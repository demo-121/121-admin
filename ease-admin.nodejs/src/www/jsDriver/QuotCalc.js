var getPremTerm = function(quotation, planDetail, planInfo) {
    var term = 0;
    if (planInfo.premTerm) {
        term = parseInt(planInfo.premTerm, 10);
    } else if (planDetail.inputConfig.defaultPremTerm) {
        if (planDetail.inputConfig.defaultPremTerm == "999") {
            if (planDetail.wholeLifeInd && planDetail.wholeLifeAge) {
                term = planDetail.wholeLifeAge - quotation.iAge;
                planInfo.premTerm =  "999";
            } else if (planDetail.inputConfig.premTermList) { // show never call this
                var term = planDetail.inputConfig.premTermList[planDetail.inputConfig.premTermList.length - 1].value
                term = parseInt(term, 10);
                planInfo.premTerm = term + "";
            }
        } else {
            term = parseInt(planDetail.inputConfig.defaultPremTerm, 10);
            planInfo.premTerm = term + "";
        }
    }

    return term;
};

var getPolicyTerm = function(quotation, planDetail, planInfo) {
    var term = 0;
    if (planInfo.policyTerm) {
        term = parseInt(planInfo.policyTerm, 10);
    } else if (planDetail.inputConfig.defaultPolicyTerm) {
        if (planDetail.inputConfig.defaultPolicyTerm == "999") {
            if (planDetail.wholeLifeInd && planDetail.wholeLifeAge) {
                term = planDetail.wholeLifeAge - quotation.iAge;
                planInfo.policyTerm = "999";
            } else if (planDetail.inputConfig.policyTermList) { // show never call this
                var term = planDetail.inputConfig.policyTermList[planDetail.inputConfig.policyTermList.length - 1].value
                term = parseInt(term, 10);
                planInfo.policyTerm = term + "";
            }
        } else {
            term = parseInt(planDetail.inputConfig.defaultPolicyTerm, 10);
            planInfo.policyTerm = term + "";
        }
    }

    return term;
};

var getPaymentMode = function(mode, planDetail) {
    for (var i in planDetail.payModes) {
        var payment = planDetail.payModes[i];
        if (payment.mode == mode) {
            return payment;
        }
    }
    return null;
}

var getAnnPrem = function(quotation, planInfo, planDetail) {
    if (planInfo.premium && quotation.paymentMode) {
        if (quotation.paymentMode == 'L' || quotation.paymentMode == 'A') {
            return planInfo.premium;
        } else {
            var payment = getPaymentMode(quotation.paymentMode, planDetail);
            if (payment) {
                return math.number(math.divide(math.bignumber(prem), payment.factor));
            }
        }
    }
    return 0;
}

var getSAScale = function(quotation, planDetail) {
    for (var s in planDetail.saInput) {
        var saInput = planDetail.saInput[s];
        if (saInput.ccy == quotation.ccy) {
            return saInput.scale
        }
    }
    return 1;
}

var updateQuotationPremium = function(quotation, planInfo, planDetail) {
    var annPrem = getAnnPrem(quotation, planInfo, planDetail);

    for (var i in planDetail.payModes) {
        var payment = planDetail.payModes[i];
        prem = math.number(math.round(math.multiply(math.bignumber(annPrem), payment.factor), 2));

        if (payment.mode == 'L' || payment.mode == 'A') {
            planInfo.yearPrem = prem;
            quotation.totYearPrem = math.number(math.add(math.bignumber(quotation.totYearPrem), prem));
        } else if (payment.mode == 'S') {
            planInfo.halfYearPrem = prem;
            quotation.totHalfyearPrem = math.number(math.add(math.bignumber(quotation.totHalfyearPrem), prem));
        } else if (payment.mode == 'Q') {
            planInfo.quarterPrem = prem;
            quotation.totQuarterPrem = math.number(math.add(math.bignumber(quotation.totQuarterPrem), prem));
        } else if (payment.mode == 'M') {
            planInfo.monthPrem = prem;
            quotation.totMonthPrem = math.number(math.add(math.bignumber(quotation.totMonthPrem), prem));
        }
    }
}

var calcQuotPlan = function(quotation, planInfo, planDetails) {

    var planDetail = planDetails[planInfo.covCode];
    var basicPlan = planDetails[quotation.plans[0].covCode];

    var premTerm = getPremTerm(quotation, planDetail, planInfo);
    var benefitTerm = getPolicyTerm(quotation, planDetail, planInfo);

    var saFunc = planDetail.formulas && planDetail.formulas.saFunc;
    var premFunc = planDetail.formulas && planDetail.formulas.premFunc;

    var cQuot = Object.assign({}, quotation);
    var cPlanInfo = Object.assign({}, planInfo);

    if ((planInfo.calcBy == 'sumAssured' || !planInfo.calcBy) && isNumber(planInfo.sumInsured) && premFunc) {

        for (var i in planDetail.payModes) {
            var payment = planDetail.payModes[i];
            cQuot.paymentMode = payment.mode;
            var prem = runFunc(planDetail.formulas.premFunc, cQuot, cPlanInfo, planDetail);

            // reset premium for decimal by currency
            let premInput = planDetail.premInput;
            if(premInput &&  premInput.length > 0) {
                for (var i in premInput) {
                    var {
                        ccy,
                        decimal
                    } = premInput[i];
                    if (ccy === quotation.ccy) {
                        var scale = math.pow(10,  decimal);
                        prem = math.number(math.divide(math.floor(math.multiply(math.bignumber(prem), scale)), scale));
                        break;
                    }
                }
            }

            if (payment.mode == quotation.paymentMode) {
                planInfo.premium = prem;
                quotation.premium = math.number(math.add(math.bignumber(quotation.premium), prem));
                quotation.sumInsured = math.number(math.add(math.bignumber(quotation.sumInsured), planInfo.sumInsured));
            }

            if (payment.mode == 'L' || payment.mode == 'A') {
                planInfo.yearPrem = prem;
                quotation.totYearPrem = math.number(math.add(math.bignumber(quotation.totYearPrem), prem));
            } else if (payment.mode == 'S') {
                planInfo.halfYearPrem = prem;
                quotation.totHalfyearPrem = math.number(math.add(math.bignumber(quotation.totHalfyearPrem), prem));
            } else if (payment.mode == 'Q') {
                planInfo.quarterPrem = prem;
                quotation.totQuarterPrem = math.number(math.add(math.bignumber(quotation.totQuarterPrem), prem));
            } else if (payment.mode == 'M') {
                planInfo.monthPrem = prem;
                quotation.totMonthPrem = math.number(math.add(math.bignumber(quotation.totMonthPrem), prem));
            }
        }
    } else if ((planInfo.calcBy == 'premium' || !planInfo.calcBy) && isNumber(planInfo.premium) && saFunc) {

        var sas = [];
        var lastSA = null;
        var closestPrem = null;
        var closestSA = null;

        // var para = {
            // age: quotation.iAge,
            // gender: quotation.iGender,
            // smoke: quotation.iSmoke, // ?'Y':'N',
            // payMod: quotation.paymentMode,
            // ccy: quotation.ccy,
        //     prem: planInfo.premium || 0,
        //     premTerm: premTerm,
        //     covClass: planInfo.classType,
        //     premAdjAmt: 1
        // };

        cPlanInfo.premAdjAmt = 1;

        if (planDetail.premAdj) {
            for (var i = 0; i < planDetail.premAdj.length; i++) {
                cPlanInfo.premAdjAmt = planDetail.premAdj[i].adjRate;
                var sa = runFunc(planDetail.formulas.saFunc, quotation, cPlanInfo, planDetail);
                cPlanInfo.sumInsured = sa;

                if (premFunc) {
                    var p = runFunc(planDetail.formulas.premFunc, quotation, cPlanInfo, planDetail);
                    var saMultiple = planDetail.saMultiple || 500;
                    if (p == cPlanInfo.premium) {
                        sa = math.number(math.multiply(math.add(math.floor(math.divide(sa, saMultiple)), sa % saMultiple == 0 ? 0 : 1), saMultiple));
                        cPlanInfo.sumInsured = sa;
                        closestPrem = runFunc(planDetail.formulas.premFunc, quotation, cPlanInfo, planDetail);;
                        closestSA = sa;
                    }
                } else {
                    closestSA = sa;
                    closestPrem = planInfo.premium;
                }
            }
        } else {
            closestSA = runFunc(planDetail.formulas.saFunc, quotation, cPlanInfo, planDetail);
            cPlanInfo.sumInsured = closestSA;

            if (premFunc) {
                closestPrem = runFunc(planDetail.formulas.premFunc, quotation, cPlanInfo, planDetail);
            } else {
                closestPrem = planInfo.premium;
            }
        }

        planInfo.premium = closestPrem;
        planInfo.sumInsured = closestSA;

        quotation.premium = math.number(math.add(math.bignumber(quotation.premium), closestPrem));
        quotation.sumInsured = math.number(math.add(math.bignumber(quotation.sumInsured), closestSA));

        updateQuotationPremium(quotation, planInfo, planDetail);

    } else if (planInfo.calcBy && planDetail.formulas[planInfo.calcBy]) {

        cPlanInfo = runFunc(planDetail.formulas[planInfo.calc], quotation, planInfo, planDetails);

        var prem = planInfo.premium = cPlanInfo.premium;
        planInfo.sumInsured = cPlanInfo.sumInsured;

        quotation.premium = math.number(math.add(math.bignumber(quotation.premium), prem));
        quotation.sumInsured = math.number(math.add(math.bignumber(quotation.sumInsured), planInfo.sumInsured));

        updateQuotationPremium(quotation, planInfo, planDetail);

    } else if (isFinite(planInfo.premium)) {
        var prem = planInfo.premium;
        quotation.premium = math.number(math.add(math.bignumber(quotation.premium), prem));

        updateQuotationPremium(quotation, planInfo, planDetail);
    } else {
        // error handle
        prem = planInfo.premium;
        quotation.premium += prem;
    }
};
