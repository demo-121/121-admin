function(parameterDic) {
    try {
        eval('var parameterDic=' + parameterDic);
        var inputList = parameterDic['inputList'],
        suitableProducts = parameterDic['suitableProducts'],
        groupBy = parameterDic['groupBy'],
        cnaModel = parameterDic['cnaModel'],
        cnaFormModel = parameterDic['cnaFormModel'],
        clientProfiles = parameterDic['clientProfiles'],
        isJustMapping = parameterDic['isJustMapping'];
        var clientOutputDic = {},
        finDatas = cnaFormModel.needsLandingContent[0].finaData,
        finCompleted = cnaModel.financialEvalCompletedDate;
        
        if (finCompleted) {
            var finAnwsers = cnaModel.financialEvaluation;
            for (var index in clientProfiles) {
                var clientProfile = clientProfiles[index],
                otherList = [],
                suitableList = [],
                group = {};
                
                for (var i in inputList) {
                    var inputProductId = inputList[i],
                    key = '';
                    if (inputProductId in suitableProducts) {
                        var suitableProduct = suitableProducts[inputProductId],
                        suitability = suitableProduct.suitability,
                        pass = !0,
                        info = [];
                        for (var qId in suitability) {
                            if (qId in finAnwsers) {
                                key += qId;
                                var suitabilityItem = suitability[qId],
                                compulsory = suitabilityItem.compulsory,
                                compulsoryOr = suitabilityItem.compulsoryOr,
                                finAnwer = finAnwsers[qId],
                                selectedOptions = finAnwer.selectedOptions,
                                selectedOptionDesc = [],
                                finDataOptions;
                                for (var j in finDatas) {
                                    var finData = finDatas[j];
                                    if (finData.id == qId) {
                                        finDataOptions = finData.options;
                                        break
                                    }
                                }
                                if (compulsory)
                                    for (var k in compulsory) {
                                        for (var a in finDataOptions) {
                                            var finDataOption = finDataOptions[a];
                                            finDataOption.id == compulsory[k] && selectedOptionDesc.push(finDataOption.title)
                                        }
                                        if (key += compulsory[k], -1 == selectedOptions.indexOf(compulsory[k])) {
                                            pass = !1;
                                            break
                                        }
                                        debug.finDataOptions = finDataOptions;
                                    }
                                if (compulsoryOr && pass) {
                                    pass = !1;
                                    for (var k in compulsoryOr)
                                        if (-1 != selectedOptions.indexOf(compulsoryOr[k])) {
                                            pass = !0;
                                            for (var a in finDataOptions) {
                                                var finDataOption = finDataOptions[a];
                                                finDataOption.id == compulsoryOr[k] && selectedOptionDesc.push(finDataOption.title)
                                            }
                                            key += compulsoryOr[k]
                                        }
                                }
                                debug.passs0 = pass;
                                if ('targetProtectionTimeLength' == qId) {
                                    var clientGender = clientProfile.gender.toLowerCase(),
                                    limitation = suitabilityItem[clientGender];
                                    debug.limitation = limitation;
                                    debug.selectedOptions = selectedOptions[0];
                                    debug.age = clientProfile.age;
                                    for (var a in finDataOptions) {
                                        var finDataOption = finDataOptions[a];
                                        if (finDataOption.id == selectedOptions[0]) {
                                            var minValue = finDataOption.minValue,
                                            maxValue = finDataOption.maxValue;
                                            debug.minValue = (clientProfile.age + minValue );
                                            if ((parseInt(clientProfile.age) + parseInt(minValue)) > parseInt(limitation)) {
                                                pass = !1;
                                                selectedOptionDesc.push(finDataOption.title);
                                            }
                                            debug.passs = pass;
                                        }
                                    }
                                }
                                for (var groupByIndex in groupBy) {
                                    var groupByItem = groupBy[groupByIndex];
                                    groupByItem.id == qId && info.push({
                                                                       title: groupByItem.title,
                                                                       items: selectedOptionDesc
                                                                       })
                                }
                            } else pass = !1;
                            if (!pass) break
                                }
                        pass ? (key in group || (group[key] = {
                                                 info: [],
                                                 products: []
                                                 }), group[key].info = info, group[key].products.push(inputProductId)) : otherList.push(inputProductId)
                    } else otherList.push(inputProductId)
                }
                
                for (var groupKey in group) {suitableList.push(group[groupKey]);}
                var clientId = ('cust_' + clientProfile.cid) ;
                clientOutputDic[clientId] = {
                                                   suitableList: suitableList,
                                                   otherList: otherList
                };
            }
        }
    } catch (ex) {
        debug.ex = ex
    }
    return returnResult({clientOutputDic: clientOutputDic})
};

