var ErrCode = {
    missingSA:                  'js.err.sa_missing',
    wrongClassType:             'js.err.wrong_class_type',
    wrongWPClassFix:            'js.err.wrong_wp_class_fix',
    wrongWPClassBP:             'js.err.wrong_wp_class_basic',
    invalidSumInsured:          'js.err.invalid_sum_insured',
    invalidPremium:             'js.err.invalid_premium',
    invalidSAMult:              'js.err.invalid_sa_mult',
    invalidBenelim:             'js.err.invalid_benelim',
    invalidPremlim:             'js.err.invalid_premlim',
    invalidCoexistShould:       'js.err.invalid_coexist_should',
    invalidCoexistShouldNot:    'js.err.invalid_coexist_shouldnot',
    invalidDA:                  'js.err.invalid_da',
    overBasicSARange:           'js.err.basicSA_over_range'
}

// validate benefit limit
var validateSALimit = function(quotation, planInfo, planDetail) {
    if (planDetail && planInfo && planDetail.inputConfig) {
        if (planDetail.inputConfig.benlim && isNumber(planInfo.sumInsured) && 
            (planDetail.inputConfig.benlim.min > planInfo.sumInsured || planDetail.inputConfig.benlim.max < planInfo.sumInsured)) {
            var err = {
                code: ErrCode.invalidBenelim,
                msgPara: []
            }
            err.msgPara.push(planInfo.covName);
            err.msgPara.push(planDetail.inputConfig.benlim.min);
            err.msgPara.push(planDetail.inputConfig.benlim.max);
            err.msgPara.push(planInfo.sumInsured);
            addError(err);
        }
    } else {
        addError({
            code: "Internal Error (validatePreimumLimit)"
        })

    }
};

// validate premium limit
var validatePreimumLimit = function(quotation, planInfo, planDetail) {
    if (planDetail && planInfo && planDetail.inputConfig) {
        if (planDetail.inputConfig.premlim && isNumber(planInfo.premium) && 
            (planDetail.inputConfig.premlim.min > planInfo.premium || planDetail.inputConfig.premlim.max < planInfo.premium)) {
            var err = {
                code: ErrCode.invalidPremlim,
                msgPara: []
            }
            err.plan = planInfo.code;
            err.msgPara.push(planInfo.covName);
            err.msgPara.push(planDetail.inputConfig.premlim.min);
            err.msgPara.push(planDetail.inputConfig.premlim.max);
            err.msgPara.push(planInfo.premium); 
            addError(err);
        }
    } else {
        addError({
            code: "Internal Error (validatePreimumLimit)"
        })
    }
};

// validate SA Multiple
var validateSAMultiple = function(quotation, planInfo, planDetail) {
    if (isNumber(planDetail.saMultiple) && isNumber(planInfo.sumInsured)) {
        var remain = math.mod(planInfo.sumInsured, planDetail.saMultiple);
        if (remain > 0) {
            addError({
                code: ErrCode.invalidSAMult,
                msgPara: [planInfo.covName, planDetail.saMultiple, planInfo.sumInsured]
            });
        }
    }
};

// validate plan coexist rule
var validateCoexist = function(quotation, planInfos, planDetails) {
    var basicPlanDetail = planDetails[quotation.baseProductCode];
    var planInfos = quotation.plans;

    if (basicPlanDetail.coexist) {
        for (var c in basicPlanDetail.coexist) {
            var coex = basicPlanDetail.coexist[c];
            if ((!coex.country || coex.country == '*' || coex.country == quotation.iResidence) &&
                (!coex.dealerGroup || coex.dealerGroup == '*' || coex.dealerGroup == quotation.dealerGroup) &&
                (!coex.gender || coex.gender == '*' || coex.gender == quotation.iGender) &&
                (!coex.smoke || coex.smoke == '*' || coex.smoke == quotation.iSmoke) &&
                (!coex.ageFr || coex.ageFr <= quotation.iAge) &&
                (!coex.ageTo || coex.ageTo >= quotation.iAge)) {

                for (var i = 1; i < planInfos.length; i++) {
                    var gA = coex.groupA.indexOf(planInfos[i].covCode);
                    for (var j = 1; j < planInfos.length; j++) {
                        if (i == j) continue;
                        var gB = coex.groupB.indexOf(planInfos[j].covCode);

                        if (gA >= 0 && gB >= 0 && !coex.shouldCoExist) {
                            addError({
                                code: ErrCode.invalidCoexistShouldNot,
                                msgPara: [planInfos[i].covName, planInfos[j].covName]
                            });
                        }

                        if (((gA >= 0 && gB < 0) || (gB >= 0 && gA < 0)) && coex.shouldCoExist) {
                            addError({
                                code: ErrCode.invalidCoexistShould,
                                msgPara: [planInfos[i].covName, planInfos[j].covName]
                            });
                        }
                    }
                }
            }
        }
    }
};

// validateDA = function(quotation, planInfos, planDetails, validate, err) {
//     if (!err) err = {};
//     if (!err.msgPara) err.msgPara = [];
//     // validate Death Aggregation
//     if (validate.deathAggregate) {
//         for (var k = 0; k < validate.deathAggregate.length; k++) {
//             var deathAggregate = validate.deathAggregate[k];
//             if (deathAggregate.ccy == quotation.ccy && deathAggregate.ageFr <= quotation.iAge && deathAggregate.ageTo >= quotation.iAge) {
//                 var daLim = deathAggregate.maxSumIns;
//                 var daSum = 0;
//                 for (var j = 0; j < planInfos.length; j++) {
//                     var planInfo = planInfos[j];
//                     var planDetail = planDetails[planInfo.docId];
//                     if (planDetail.deathAggregateSeq) {
//                         var para = {
//                             sumInsured: planInfo.sumInsured
//                         };
//                         var riskSA = runFunc(planDetail.deathAggregateFunc, planDetail, para);
//                         daSum = daSum + riskSA;
//                     }
//                 }
//                 if (daSum > daLim) {
//                     err.isInvalidDA = true;
//                 }
//             }
//         }
//     }
// };

var validateMandatoryFields = function(quotation, planInfo, planDetail) {
    if (planDetail.inputConfig) {
        var config = planDetail.inputConfig;
        if (config.canEditPolicyTerm) {
            if (planInfo.policyTerm == null) {
                addError({
                    code: ErrCode.mandatoryFieldMissing,
                    msgPara: [planDetail.covName, 'quotation.policy_term']
                });
            }
        }
        if (config.canEditPremTerm) {
            if (planInfo.premTerm == null) {
                addError({
                    code: ErrCode.mandatoryFieldMissing,
                    msgPara: [planDetail.covName, 'quotation.premium_term']
                });
            }
        }
        if (config.canEditClassType) {
            if (planInfo.classType == null) {
                addError({
                    code: ErrCode.mandatoryFieldMissing,
                    msgPara: [planDetail.covName, 'quotation.class']
                });
            }
        }
        
        if (config.canEditSumAssured) {
            if (!isNumber(planInfo.sumInsured)) {
                addError({
                    code: ErrCode.mandatoryFieldMissing,
                    msgPara: [planDetail.covName, 'quotation.sum_assured']
                });
            }
        }
        if (config.canEditPremium) {
            if (!isNumber(planInfo.premium)) {
                addError({
                    code: ErrCode.mandatoryFieldMissing,
                    msgPara: [planDetail.covName, 'quotation.premium']
                });
            }
        }
    }
}

var validateGlobal = function(quotation, planInfos, planDetails) {
    // validateDA(quotation, planInfos, planDetails, validate, err);
    validateCoexist(quotation, planInfos, planDetails);
};

var validatePlanAfterCalc = function(quotation, planInfo, planDetail) {
    validateSAMultiple(quotation, planInfo, planDetail);
    validateSALimit(quotation, planInfo, planDetail);
    validatePreimumLimit(quotation, planInfo, planDetail);
};

var validatePlanBeforeCalc = function(quotation, planInfo, planDetails) {
    var basicPlanInfo = quotation.plans[0]
    var planDetail = planDetails[planInfo.covCode];

    validateMandatoryFields(quotation, planInfo, planDetails);

    // validateSAMultiple(quotation, planInfo, planDetail);
    // validateSALimit(quotation, planInfo, planDetail);
    // validatePreimumLimit(quotation, planInfo, planDetail);

    var basicPlanDetails = planDetails[basicPlanInfo.covCode];
    var riderList = basicPlanDetails.inputConfig.riderList;
    var planDetail = planDetails[planInfo.covCode];

    // for rider, 
    if (planDetail.planType == "R") {
        // check for attachable rider list rule on basic plan
        for (var r in riderList) {
            var rider = riderList[r];
            if (rider.covCode == planInfo.covCode) {
                // check if basic SA over the limited range
                if ((isNumber(rider.basicSAMin) && rider.basicSAMin > basicPlanInfo.sumInsured) || 
                    (isNumber(rider.basicSAMax) && rider.basicSAMax < basicPlanInfo.sumInsured)) {
                    addError({
                        code: errCode.overBasicSARange,
                        msgPara: [planDetail.covName, rider.basicSAMin, rider.basicSAMax]
                    })
                }

                // check class rule for follow Basic plan
                if (rider.classRule && rider.classRule == "B" && basicPlanInfo.classType) {
                    if (planInfo.classType != basicPlanInfo.classType) {
                        addError({
                            code: errCode.wrongClassType,
                            msgPara: [planDetail.covName, basicPlanInfo.covName]
                        })
                    }
                }

                // check class rule for fix type
                if (rider.classRule && rider.classRule == "F" && rider.classFix) {
                    if (planInfo.classType != rider.classFix) {
                        addError({
                            code: errCode.wrongClassType,
                            msgPara: [planDetail.covName, rider.classFix]
                        })
                    }
                }

                break;
            }
        }
    }
};

prepMsg = function(msg, para) {
    var omsg = msg;
    if (para.length) {
        for (k = 0; k < para.length; k++) {
            omsg = omsg.replace('[' + k + ']', para[k]);
        }
    }
    return omsg;
};
