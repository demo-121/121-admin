var driver = new Object();

driver.getProducts = function(param) {
    var paramJSON = JSON.parse(param);
    var cid = paramJSON.cid;
    var suitability = paramJSON.suitability;
    var cna = paramJSON.cna;
    var checkObject = paramJSON.checkObject;
    var profiles = paramJSON.profiles;
    var recommendedProducts = paramJSON.recommendedProducts;
    var productList = paramJSON.productList;

    var suitableProducts = suitability.products;
    var groupBy = suitability.groupBy;
    var questionnaireRequiredFunc = suitability.questionnaireRequiredFunc;
    var suitabilityFunc = suitability.suitabilityFunc;
    var validateRecommendedProductsFunc = suitability.validateRecommendedProductsFunc;
    var resultObj = {};
    try {
        var cnaDic = {
            "financialEval": cna.financialEvalCompletedDate != null,
            "NeedsAnalysis": cna.needsAnalysisCompletedDate != null,
            "RiskAssess": cna.riskAssessCompletedDate != null
        }
        var p1 = JSON.stringify({"suitableDict": suitableProducts,"cnaDic": cnaDic, "checkObject": checkObject});
        var r1 = JSON.parse(runFunc(questionnaireRequiredFunc, p1));

        var notDisplayList = r1.notDisplayList != null ? Object.keys(r1.notDisplayList) : [];
        productList = productList.filter(function (p) {
           return notDisplayList.indexOf(p._id)<0;
        });
        resultObj["productList"] = productList;

        if (cnaDic.financialEval == false) {
            if (productList.length > 0) {
                resultObj["status"] = "ByProductLine";
            } else {
                resultObj["status"] = "NoData";
                resultObj["reason"] = "NotSuitable";
            }
        } else {
            var inputList = [];
            for (i in productList) {
                inputList.push(productList[i]._id)
            }
            var p2 = JSON.stringify({"inputList": inputList, "suitableProducts": suitableProducts, "groupBy": groupBy, "cnaModel": cna, "clientProfiles": profiles});
            var r2 = JSON.parse(runFunc(suitabilityFunc, p2));

            var suitableList = r2.clientOutputDic != null ? r2.clientOutputDic[cid].suitableList : [];
            var otherList = r2.clientOutputDic != null ? r2.clientOutputDic[cid].otherList : [];
            resultObj["status"] = "ByGroupNoWarn";
            resultObj["suitableList"] = suitableList;
            resultObj["otherList"] = otherList;
            debug.otherList = otherList;

            var financialEvaluation = cna.financialEvaluation;
            var suitableProductIdList = [];
            for (i in suitableList) {
                var item = suitableList[i];
                var products = item.products != null ? item.products : [];
                suitableProductIdList = suitableProductIdList.concat(products);
            }
            var r3 = JSON.parse(runFunc(validateRecommendedProductsFunc, JSON.stringify(financialEvaluation), JSON.stringify(suitableProducts), JSON.stringify(recommendedProducts), JSON.stringify(suitableProductIdList)));
            var errorArray = r3.errorArray != null ? r3.errorArray : [];
            if (errorArray.length > 0) {
                resultObj["status"] = "ByGroupWithWarn";
                resultObj["errorArray"] = errorArray;
            }
        }

    } catch(ex) {
        debug.exception = {
        message: ex.message,
        stack: ex.stack
        };
    }
    return returnResult(resultObj);
}

driver.calculateQuotation = function(quotationJson, planDetailsJson) {
    var resultObj = {};
    try {
        var quotation = 0, planDetails = 0;
        // Initialize variables based on the input parameters
        if (typeof quotationJson == 'string') {
            if (JSON && typeof JSON.parse === 'function') {
                quotation = JSON.parse(quotationJson);
                planDetails = JSON.parse(planDetailsJson);
            } else {
                eval('quotation = ' + quotationJson);
                eval('planDetails = ' + planDetailsJson);
            }
        } else if (typeof quotationJson == 'object') {
            quotation = quotationJson;
            planDetails = planDetailsJson;
        }
        resultObj.error = null;

        if (quotation && planDetails) {
            var retVal = validateQuotation(quotation, planDetails);
            resultObj.quotation = retVal.quotation || quotation;

            planDetails = retVal.planDetails || planDetails;
            if (planDetails) {
                resultObj.planDetails = {};
                for (var p in quotation.plans) {
                    var plan = quotation.plans[p];
                    resultObj.planDetails[plan.covCode] = {
                    covCode: plan.covCode,
                    inputConfig: planDetails[plan.covCode].inputConfig
                    }
                }
            }
        } else {
            resultObj.error = {
                message: "Quotation or Plan details missing."
            };
        }

        // debug.DoneQuotation = resultObj.quotation;
    } catch (ex) {
        debug.exception = {
          message: ex.message,
          stack: ex.stack
        };
        debug.quot = quotation;
        debug.pds = Object.keys(planDetails);
    }
    return returnResult(resultObj);
}

// generate illurstration and report data
/*  parameter:
 *      quotation               : Object, quotation
 *      planDetails             : Object, map of covCode vs Product Configuration
 *      extraPara               : Object
 *          BI Options          : Object, key value map of BI options
 *          illustProps         : Object, map of covCode vs Illustration properties and rates
 *          templateFuncs       : Object, map of covCode vs PDF template formulas ( prepareReportDataFunc and prepareIlluDataFunc )
 *          requireReportData   : bool, indicate whether need to generate report data
 *  return: Object
 *      quotation               : quotation with illursation data
 *      reportData              : report data for Proposal
 */
driver.generateIllustrationData = function(quotationJson, planDetailsJson, extraParaJson) {
    var resultObj = {};
    try {
        // Initialize variables based on the input parameters
        var quotation = 0, planDetails = 0, extraPara = 0;

        if (typeof quotationJson == 'string') {
            if (JSON && typeof JSON.parse === 'function') {
                quotation = JSON.parse(quotationJson);
                planDetails = JSON.parse(planDetailsJson);
                extraPara = JSON.parse(extraParaJson);
                // var globalValidation = JSON.parse(validationJson);
            } else {
                eval('quotation = ' + quotationJson);
                eval('planDetails = ' + planDetailsJson);
                eval('extraPara = ' + extraParaJson);
                // eval('var globalValidation = ' + validationJson);
            }
        } else if (typeof quotationJson == 'object') {
            quotation = (quotationJson);
            planDetails = (planDetailsJson);
            extraPara = (extraParaJson);
            // var globalValidation = (validationJson);
        }

        resultObj.error = null;

        if (quotation && planDetails) {
            try {
                var retVal = validateQuotation(quotation, planDetails);
                quotation = retVal.quotation || quotation;
                planDetails = retVal.planDetails || planDetails;

            } catch (ex) {
                ex.message = 'warning: failure to recal the quotation'
                throw ex;
            }

            // generate illustration
            // var illustrationList = [];
            retVal = generateIllustration(quotation, planDetails, extraPara);

            if (retVal.isIllustrationGenerated) {
                resultObj.illustrations = retVal.illustrations;
                // resultObj.quotation = quotation;

                if (extraPara.requireReportData) {
                    extraPara.illustrations = retVal.illustrations;
                    try {
                        // if (extraPara.prepareDataFunc) {
                        //   resultObj.reportData = runFunc(extraPara.prepareDataFunc, quotation, planDetails, extraPara);
                        // } else {
                        resultObj.reportData = prepareReportData(quotation, planDetails, extraPara);
                        // }
                    } catch (ex) {
                        ex.message = 'Failed to prepare data for PDF'
                        throw ex;
                    }
                }
            } else {
                // Failed to generate illustration
                var ex = {
                    message: 'Failed to generate PDF due to generating illustration failure'
                };
                debug.err = retVal;
                throw ex;
            }
        }
    } catch (ex) {
        debug.exception = {
            message: ex.message,
            stack: ex.stack
        };
    }
    return returnResult(resultObj);
}

var assignExtraPropertiesToPlanDetail = function(planDetail, quotation, planDetails) {
    planDetail.inputConfig = {};

    if (!quotation.paymentMode && planDetail.payModes) {
        for (var m in planDetail.payModes) {
            var mode = planDetail.payModes[m];
            if (mode.default && mode.default == 'Y') {
                quotation.paymentMode = mode.mode;
            }
        }
        if (!quotation.paymentMode) {
            quotation.paymentMode = planDetail.payModes[0].mode;
        }
    }

    if (!quotation.ccy) {
        quotation.ccy = planDetail.currencies[0].ccy[0];
    }

    if (planDetail.formulas && planDetail.formulas.preparePolicyOptions) {
        planDetail = runFunc(planDetail.formulas.preparePolicyOptions, planDetail, quotation, planDetails);
    } else {
        preparePolicyOptions(planDetail, quotation, planDetails);
    }

    if (planDetail.formulas && planDetail.formulas.prepareClassConfig) {
        planDetail = runFunc(planDetail.formulas.prepareClassConfig, planDetail, quotation, planDetails);
    } else {
        prepareClassConfig(planDetail, quotation, planDetails);
    }

    if (planDetail.formulas && planDetail.formulas.prepareTermConfig) {
        planDetail = runFunc(planDetail.formulas.prepareTermConfig, planDetail, quotation, planDetails);
    } else {
        prepareTermConfig(planDetail, quotation, planDetails);
    }

    if (planDetail.formulas && planDetail.formulas.prepareSaPremConfig) {
        planDetail = runFunc(planDetail.formulas.prepareSaPremConfig, planDetail, quotation, planDetails);
    } else {
        prepareAmountConfig(planDetail, quotation, planDetails);
    }

    if (planDetail.planInd == "B") {
        if (planDetail.formulas && planDetail.formulas.prepareAttachableRider) {
			runFunc(planDetail.formulas.prepareAttachableRider, planDetail, quotation, planDetails);
        } else {
            prepareAttachableRider(planDetail, quotation, planDetails);
        }
    }

    return planDetail;
}

var preparePolicyOptions = function(planDetail, quotation, planDetails) {

    var policyOptions = [];

    for (var p in planDetail.policyOptions) {
        var po = planDetail.policyOptions[p];
        if (po.type == 'datepicker' ||
            (po.type == 'text' && (po.subType == 'currency' || po.subType == 'number'))) {
            if (po.max || po.max === 0) {
                po.max = parseInt(po.max, 10)
            }
            if (po.min || po.min === 0) {
                po.min = parseInt(po.min, 10)
            }
        }

        if (po.type == 'picker' && po.options) {
            if (!quotation.policyOptions[po.id] && po.value) {
                for (var i in po.options) {
                    var opt = po.options[i];
                    if (opt.value == po.value) {
                        quotation.policyOptions[po.id] = po.value;
                        break;
                    }
                }
            }
            if (!quotation.policyOptions[po.id] && po.options.length) {
                if (po.value == '999') {
                    quotation.policyOptions[po.id] = po.options[po.options.length-1].value;
                } else {
                    quotation.policyOptions[po.id] = po.options[0].value;
                }
            }
        } else {
            if (!quotation.policyOptions[po.id] && (po.value || po.value === 0 || po.value === '')) {
                if (po.type == 'text' && (po.subType == 'currency' || po.subType == 'number')) {
                    quotation.policyOptions[po.id] = parseFloat(po.value);
                } else {
                    quotation.policyOptions[po.id] = po.value;
                }
            }
        }
        policyOptions.push(po);
    }
    planDetail.inputConfig.policyOptions = policyOptions;
    return planDetail;
}


var preparePremLimit = function(planDetail, quotation) {

    var planInfo = {
        classType: planDetail.inputConfig.defaultClassType || '',
        policyTerm: planDetail.inputConfig.defaultPolicyTerm || '',
        premTerm: planDetail.inputConfig.defaultPremTerm || ''
    }

    if (quotation.plans) {
        for (var p in quotation.plans) {
            if (quotation.plans[p].covCode == planDetail.covCode) {
                planInfo = Object.assign(planInfo, quotation.plans[p]);
            }
        }
    }


    if (planDetail.premInputInd && planDetail.premInputInd == 'Y') {
        var premlim = [];
        if (!planDetail.premlim) {
            // calc from benlim and put to input config
            if (planDetail.benlim && planDetail.benlim.length && planDetail.formulas && planDetail.formulas.saFunc) {
                for (var b in planDetail.benlim) {
                    var bl = planDetail.benlim[b];

                    if ((!bl.country || bl.country == '*' || bl.country == quotation.iResidence )
                        && (!bl.classType || bl.classType == '*' || bl.classType == planInfo.classType)
                        && quotation.iAge > bl.ageFr && quotation.iAge < bl.ageTo) {

                        var limits = [];
                        for (var l in bl.limits) {
                            var lim = bl.limits[l];
                            if (lim.ccy == quotation.ccy && lim.payMode == quotation.paymentMode) {
                                premlim = calcMinMaxPrem(planDetail, quotation, planInfo, lim);
                                break;
                            }
                        }
                    }
                }
            }
        } else {
            // massage premlim and put to input config
            if (planDetail.premlim.length) {
                for (var p in planDetail.premlim) {
                    var pl = planDetail.premlim[p];

                    if ((!pl.country || pl.country == '*' || pl.country == quotation.iResidence )
                        && (!pl.classType || pl.classType == '*' || pl.classType == planInfo.classType)
                        && quotation.iAge > pl.ageFr && quotation.iAge < pl.ageTo) {
                        for (var l in pl.limits) {
                            var lim = pl.limits[l];
                            if (lim.ccy == quotation.ccy && lim.payMode == quotation.paymentMode) {
                                premlim = {
                                max: lim.max,
                                min: lim.min
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
        planDetail.inputConfig.premlim = premlim;
    }

    if (planDetail.saInputInd && planDetail.saInputInd == 'Y') {
        var benlim = [];
        if (!planDetail.benlim) {
            // calc from benlim and put to input config
            if (planDetail.premlim && planDetail.premlim.length && planDetail.formulas && planDetail.formulas.saFunc) {
                for (var p in planDetail.premlim) {
                    var pl = planDetail.premlim[p];

                    if ((!pl.country || pl.country == '*' || pl.country == quotation.iResidence )
                        && (!pl.classType || pl.classType == '*' || pl.classType == planInfo.classType)
                        && quotation.iAge > pl.ageFr && quotation.iAge < pl.ageTo) {

                        var limits = [];
                        for (var l in pl.limits) {
                            var lim = pl.limits[l];
                            if (lim.ccy == quotation.ccy && lim.payMode == quotation.paymentMode) {
                                benlim = calcMinMaxSa(planDetail, quotation, planInfo, lim);
                                break;
                            }
                        }
                    }
                }
            }
        } else {
            // massage premlim and put to input config
            if (planDetail.benlim.length) {
                for (var b in planDetail.benlim) {
                    var bl = planDetail.benlim[b];

                    if ((!bl.country || bl.country == '*' || bl.country == quotation.iResidence )
                        && (!bl.classType || bl.classType == '*' || bl.classType == planInfo.classType)
                        && quotation.iAge > bl.ageFr && quotation.iAge < bl.ageTo) {

                        for (var l in bl.limits) {
                            var lim = bl.limits[l];
                            if (lim.ccy == quotation.ccy && lim.payMode == quotation.paymentMode) {
                                benlim = {
                                max: lim.max,
                                min: lim.min
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }

        planDetail.inputConfig.benlim = benlim;
    }
}

var calcMinMaxSa = function(planDetail, quotation, planInfo, limit) {

    planInfo.premium = limit.min;
    var minSA = runFunc(planDetail.saFunc || (planDetail.formulas && planDetail.formulas.saFunc), quotation, planInfo, planDetail);

    planInfo.premium = limit.max;
    var maxSA = runFunc(planDetail.saFunc || (planDetail.formulas && planDetail.formulas.saFunc), quotation, planInfo, planDetail);

    return {
        min : minSA,
        max : maxSA
    };
}

var calcMinMaxPrem = function(planDetail, quotation, planInfo, limit) {

    planInfo.sumInsured = limit.min
    var minPrem = runFunc(planDetail.premFunc || (planDetail.formulas && planDetail.formulas.premFunc), quotation, planInfo, planDetail);

    planInfo.sumInsured = limit.max;
    var maxPrem = runFunc(planDetail.premFunc || (planDetail.formulas && planDetail.formulas.premFunc), quotation, planInfo, planDetail);

    return {
        min : minPrem,
        max : maxPrem
    };
}

var prepareAmountConfig = function(planDetail, quotation) {
    var canEditSumAssured = false;
    var canEditPremium = false;
    if (planDetail.saInputInd && planDetail.saInputInd == 'Y') {
        canEditSumAssured = true;
    }
    if (planDetail.premInputInd && planDetail.premInputInd == 'Y') {
        canEditPremium = true;
    }
    planDetail.inputConfig.canEditSumAssured = canEditSumAssured;
    planDetail.inputConfig.canEditPremium = canEditPremium;

    preparePremLimit(planDetail, quotation);
}

var prepareClassConfig = function(planDetail, quotation) {
    if (planDetail.classList) {
        var classList = planDetail.classList;
        planDetail.inputConfig.canEditClassType = classList.length > 0 && !planDetail.isOccupClass;

        var classTypeList = [];
        for (var i = 0; i < classList.length; i++) {
            var classType = classList[i];
            if (classType.default && classType.default == "Y") {
                planDetail.inputConfig.defaultClassType = classType.covClass
            }
            classTypeList.push({
              "value": classType.covClass,
              "title": classType.className
            })
        }
        if (classTypeList.length > 0) {
            planDetail.inputConfig.classTypeList = classTypeList;
        }
    } else {
        planDetail.inputConfig.canEditClassType = false;
    }
}

var prepareAttachableRider = function(planDetail, quotation, planDetails) {
    var riderList = [];
    if (planDetail.riderList && planDetail.riderList.length) {
        var validRider = 0;
        for (var r in planDetail.riderList) {
            var rider = planDetail.riderList[r];

            if (rider.condition && rider.condition.length) {
                for (var c in rider.condition) {
                    var cond = rider.condition[c];

                    if ((!quotation.iResidence || cond.country == '*' || quotation.iResidence == cond.country) &&
                        (!quotation.dealerGroup || cond.dealerGroup == '*' || quotation.dealerGroup == cond.dealerGroup) &&
                        (!cond.endAge || quotation.iAge <= cond.endAge) &&
                        (!cond.staAge || quotation.iAge >= cond.staAge)) {

                        var riderDetail = planDetails[rider.covCode];

                        if (riderDetail) {
                            // check pay mode
                            var paymodes = riderDetail.payModes;
                            var foundPayMode = false;
                            for (var p in paymodes) {
                              var pm = paymodes[p];
                              if (pm.mode == quotation.paymentMode) {
                                foundPayMode = true;
                                break;
                              }
                            }
                            if (!foundPayMode) {
                              continue;
                            }

                            validRider = {}
                            for (var rd in rider) {
                                if (rd != 'condition') {
                                    validRider[rd] = rider[rd];
                                }
                            }

                            for (var c in cond) {
                                if (c != 'country' && c != 'dealerGroup' && c != 'staAge'
                                    && c != 'endAge' && c != 'amount') {
                                    validRider[c] = cond[c];
                                }
                            }
                            if (cond.amount) {
                                for (var a in cond.amount) {
                                    var amt = cond.amount[a];
                                    if (quotation.ccy == amt.ccy) {
                                        validRider = Object.assign(validRider, amt);
                                        break;
                                    }
                                }
                            }

                            if (validRider) {
                                riderList.push(validRider);
                            }
                        }
                    }
                }
            }
        }
        planDetail.inputConfig.riderList = riderList;
    }
}

var polTermsFunc = function(planDetail, quotation) {
    var policyTermList = null;

    var foundDefault = false;
    if (planDetail.polMaturities) {
        policyTermList = [];
        for (var p in planDetail.polMaturities) {
            var mat = planDetail.polMaturities[p];
            if (!mat.country || mat.country == '*' || mat.country == quotation.residence && mat.minTerm && mat.maxTerm) {
                var interval = mat.interval || 1;
                for (var m = mat.minTerm;
                    m <= mat.maxTerm && (!mat.maxAge || !quotation.iAge || (m + quotation.iAge) <= mat.maxAge)
                        && (!mat.ownerMaxAge || !quotation.pAge || (m + quotation.pAge) <= mat.ownerMaxAge);
                    m += interval) {
                    foundDefault |= (mat.default === m);
                    policyTermList.push({
                        "value": m+"",
                        "title": m+(quotation.iAge?"(@"+(quotation.iAge+m)+")":""),
                        "default": mat.default === m
                    });
                }
                if (mat.default === 0) {
                    policyTermList[0].default = true;
                } else if (mat.default === 999 || !foundDefault) {
                    policyTermList[policyTermList.length - 1].default = true;
                }
            }
        }
    } else if (planDetail.wholeLifeInd && planDetail.wholeLifeInd == 'Y' && planDetail.wholeLifeAge) {
        return [{
            "value": "999",
            "title": "term.swhole_life",
            "default": true
        }]
    }
    return policyTermList;
}

var premTermsFunc = function(planDetail, quotation) {
    var premTermList = null;
    var foundDefault = false;
    if (planDetail.premMaturities) {
        premTermList = [];
        for (var p in planDetail.premMaturities) {
            var mat = planDetail.premMaturities[p];
            if (!mat.country || mat.country == '*' || mat.country == quotation.residence && mat.minTerm && mat.maxTerm) {
                var interval = mat.interval || 1;
                for (var m = mat.minTerm;
                    m <= mat.maxTerm && (!mat.maxAge || !quotation.iAge || (m + quotation.iAge) <= mat.maxAge)
                        && (!mat.ownerMaxAge || !quotation.pAge || (m + quotation.pAge) <= mat.ownerMaxAge);
                    m += interval) {
                    foundDefault |= (mat.default === m);
                    premTermList.push({
                        "value": m+"",
                        "title": m+(quotation.iAge?"(@"+(quotation.iAge+m)+")":""),
                        "default": mat.default === m
                    });
                }

                if (mat.default === 0) {
                    premTermList[0].default = true;
                } else if (mat.default === 999 || !foundDefault) {
                    premTermList[premTermList.length - 1].default = true;
                }

            }
        }
    }
    return premTermList;
}

var prepareTermConfig = function(planDetail, quotation, planDetails) {

    var isChangeTogether = false;
    var policyTermList = [];
    var premTermList = [];

    if (planDetail.premMatSameAsBasic && planDetail.premMatSameAsBasic == 'Y') {
        var basicDetail = planDetails[quotation.baseProductCode];
        premTermList = basicDetail.inputConfig.premTermList;
    } else {
        if (planDetail.formulas && planDetail.formulas.premTermsFunc) {
            premTermList = runFunc(planDetail.formulas.premTermsFunc, planDetail, quotation);
        } else {
            premTermList = premTermsFunc(planDetail, quotation);
        }
    }

    if (planDetail.policyMatSameAsBasic && planDetail.policyMatSameAsBasic == 'Y') {
        var basicDetail = planDetails[quotation.baseProductCode];
        policyTermList = basicDetail.inputConfig.policyTermList;
    } else {
        if (planDetail.formulas && planDetail.formulas.polTermsFunc) {
            policyTermList = runFunc(planDetail.formulas.polTermsFunc, planDetail, quotation);
        } else {
            policyTermList = polTermsFunc(planDetail, quotation);
        }
    }

    if (planDetail.polMatSameAsPremMat && planDetail.polMatSameAsPremMat == 'Y') {
        policyTermList = premTermList;
    } else if (planDetail.premMatSameAsPolMat && planDetail.premMatSameAsPolMat == 'Y') {
        premTermList = policyTermList;
    }

    if (policyTermList && policyTermList.length) {
        planDetail.inputConfig.defaultPolicyTerm = policyTermList[0].value;
        for (var p in policyTermList) {
            if (policyTermList[p].default) {
                planDetail.inputConfig.defaultPolicyTerm = policyTermList[p].value;
                break;
            }
        }
        planDetail.inputConfig.canEditPolicyTerm = (planDetail.polTermInd && planDetail.polTermInd == 'Y');
        planDetail.inputConfig.policyTermList = policyTermList;
    }

    if (premTermList && premTermList.length) {
        planDetail.inputConfig.defaultPremTerm = premTermList[0].value;
        for (var p in premTermList) {
            if (premTermList[p].default) {
                planDetail.inputConfig.defaultPremTerm = premTermList[p].value;
                break;
            }
        }
        planDetail.inputConfig.canEditPremTerm = (planDetail.premTermInd && planDetail.premTermInd == 'Y');
        planDetail.inputConfig.premTermList = premTermList;
    }
    // planDetail.inputConfig.termChangeTogether = planDetail.polTermSameAsPremTerm || planDetail.premTermSameAsPolTerm;
}

var validateQuotation = function(quotation, planDetails) { //, planInfoList, globalValidation, basicPlanInfo) {
    var retVal = {};

    if (quotation.baseProductCode && planDetails[quotation.baseProductCode]) {

        var basicPlanInfo = 0;
        var basicPlanCode = '';
        var planInfoList = 0;
        var basicPlanDetail = 0;

        // check if it is initializing
        if (quotation.plans && quotation.plans.length) {
            newQuot = false;
            basicPlanInfo = quotation.plans[0];
            basicPlanCode = basicPlanInfo.covCode;
            basicPlanDetail = planDetails[basicPlanCode];
        } else if (quotation.baseProductCode) {
            newQuot = true;
            basicPlanCode = quotation.baseProductCode;
            basicPlanDetail = planDetails[basicPlanCode];
            basicPlanInfo = {
            covCode: basicPlanDetail.covCode
            }
            quotation.plans = [];
            quotation.plans.push(basicPlanInfo);
        }
        planInfoList = quotation.plans;

        var riderList = [];

        if (basicPlanDetail) {
            // assign extra properties for basic plan first
            var details = assignExtraPropertiesToPlanDetail(basicPlanDetail, quotation, planDetails);

            if (details && details.inputConfig) {
                // check auto attach rider for new quotation and compulsory rider
                if (details.inputConfig.riderList && details.inputConfig.riderList.length) {
                    riderList = details.inputConfig.riderList;
                    for (var r in riderList) {
                        var rider = riderList[r];

                        if ((newQuot && rider.autoAttach && rider.autoAttach == 'Y') || (rider.compulsory && rider.compulsory == 'Y')) {
                            var added = false;
                            for (var p = 1; p < quotation.plans.length; p++) {
                                var planInfo = quotation.plans[p];
                                if (planInfo.covCode == rider.covCode) {
                                    added = true;
                                    break;
                                }
                            }
                            if (!added) {
                                var riderDetail = planDetails[rider.covCode];
                                planInfoList.push({
                                    covCode: riderDetail.covCode,
                                    covName: riderDetail.covName,
                                    version: riderDetail.version,
                                    productLine: riderDetail.productLine
                                });
                            }
                        }
                    }
                }

                quotation.plans[0].covName = basicPlanDetail.covName;
                quotation.plans[0].productLine = basicPlanDetail.productLine;
                quotation.plans[0].version = basicPlanDetail.version;

                if (newQuot) {
                    quotation.plans[0].policyTerm = details.inputConfig.defaultPolicyTerm;
                    quotation.plans[0].premTerm = details.inputConfig.defaultPremTerm;
                    quotation.plans[0].classType = details.inputConfig.defaultClassType;
                }

                // planDetails[basicPlanCode].inputConfig = details.inputConfig;
            }


            // assign extra properties for rider
            for (var p = 1; p < planInfoList.length; p++) {
                var plan = planInfoList[p];
                var planDetail = planDetails[plan.covCode];
                var details = assignExtraPropertiesToPlanDetail(planDetail, quotation, planDetails);

                // perform rider config. override by basic plan
                if (riderList && riderList.length) {
                    for (var r in riderList) {
                        var rider = riderList[r];
                        if (rider.covCode == plan.covCode) {
                            if (rider.maxSA) {
                                details.inputConfig.benlim.max = rider.maxSA;
                            }
                            if (rider.minSA) {
                                details.inputConfig.benlim.min = rider.minSA;
                            }
                            break;
                        }
                    }
                }
                plan.covName = planDetail.covName;
                plan.productLine = planDetail.productLine;
                plan.version = planDetail.version;

                if (details.polTermRule == "BPT") {
                    plan.policyTerm = basicPlanInfo.premTerm;
                } else if (details.polTermRule == "BBT") {
                    plan.policyTerm = basicPlanInfo.policyTerm;
                } else if (!quotation.plans[p].policyTerm) {
                    plan.policyTerm = details.inputConfig.defaultPolicyTerm;
                }

                if (details.premTermRule == "BPT") {
                    plan.premTerm = basicPlanInfo.premTerm;
                } else if (details.premTermRule == "BBT") {
                    plan.premTerm = basicPlanInfo.policyTerm;
                } else if (!quotation.plans[p].policyTerm) {
                    plan.premTerm = details.inputConfig.defaultPremTerm;
                }

                if (details.premTermRule == "BT") {
                    plan.premTerm = plan.policyTerm;
                }
                if (details.polTermRule == "PT") {
                    plan.policyTerm = plan.premTerm;
                }

                plan.classType = quotation.plans[p].classType || details.inputConfig.defaultClassType;
                // planDetails[plan.covCode].inputConfig = details.inputConfig;
            }

            for (var i = 0; i < planInfoList.length; i++) {
                var detials = planDetails[planInfoList[i].covCode];
                if (detials.formulas && detials.formulas.willValidQuotFunc) {
                    runFunc(detials.formulas.willValidQuotFunc, quotation, planInfoList[i], planDetails);
                }
            }

            // start calc quotation
            quotation.premium = 0;
            quotation.totYearPrem = 0;
            quotation.totHalfyearPrem = 0;
            quotation.totQuarterPrem = 0;
            quotation.totMonthPrem = 0;
            quotation.sumInsured = 0;

            if (basicPlanDetail.formulas && basicPlanDetail.formulas.validQuotFunc) {
                var retVal = runFunc(basicPlanDetail.formulas.validQuotFunc, quotation, planDetails);
                retVal.quotation = ret.quotation || quotation;
                retVal.planDetails = ret.planDetails || planDetails;
            } else {


                // validate the input before calc quotation
                for (var i = 0; i < planInfoList.length; i++) {
                    var detials = planDetails[planInfoList[i].covCode];

                    if (detials.formulas && detials.formulas.validBeforeCalcFunc) {
                        runFunc(detials.formulas.validBeforeCalcFunc, quotation, planInfoList[i], planDetails);
                    } else {
                        validatePlanBeforeCalc(quotation, planInfoList[i], planDetails);
                    }
                }

                retVal.planDetails = planDetails;

                var lastCalcList = {};
                var removeRiderList = [];

                for (var i = 0; i < planInfoList.length; i++) {
                    var planInfo = planInfoList[i];
                    var code = planInfo.covCode;

                    // check if it is a valid rider by find it in input config of basic plan
                    var curRiderConf = false;
                    var riderDetail = planDetails[code];
                    if (i >= 1) {
                        for (var r in riderList) {
                            var riderConf = riderList[r];
                            if (code == riderConf.covCode) {
                                curRiderConf = riderConf;
                                break;
                            }
                        }

                        // check if it has SA override rule
                        if (curRiderConf && (!planInfo.sumInsured || !riderDetail.inputConfig.canEditSumAssured)) {
                            if (curRiderConf.saRule == 'BP') {
                                planInfo.sumInsured = basicPlanInfo.premium * (curRiderConf.saRate || 1);
                            } else if (curRiderConf.saRule == 'BSA') {
                                planInfo.sumInsured = basicPlanInfo.sumInsured * (curRiderConf.saRate || 1);
                            } else if (curRiderConf.saRule == 'PP') {
                                lastCalcList[i+""] = curRiderConf;
                                continue; // skip and calc later
                            } else if (curRiderConf.saRule == 'F') {
                                planInfo.sumInsured = curRiderConf.saFix || 0;
                            }
                        }
                    }

                    if (i == 0 || curRiderConf) { // perform calc if it is basic plan or valid rider
                        if (riderDetail.formulas && riderDetail.formulas.calcQuotFunc) {
                            runFunc(riderDetail.formulas.calcQuotFunc, quotation, planInfo, planDetails);
                        } else {
                            calcQuotPlan(quotation, planInfo, planDetails);
                        }
                    } else if (!curRiderConf) { // remove invalid rider
                        removeRiderList.push(code);
                    }
                }

                // calc for post calc rider
                var policyPremium = quotation.premium;

                for (var l in lastCalcList) {
                    var planInfo = planInfoList[l];
                    var curRiderConf = lastCalcList[l];
                    if (curRiderConf && (!planInfo.sumInsured || !riderDetail.inputConfig.canEditSumAssured)) {
                        // override SA if required
                        if (curRiderConf.saRule == 'PP') {
                            planInfo.sumInsured = policyPremium * (curRiderConf.saRate || 1);
                        }
                    }

                    // recalc it
                    if (planDetails[code].formulas && planDetails[code].formulas.calcQuotFunc) {
                        runFunc(planDetails[code].formulas.calcQuotFunc, quotation, planInfo, planDetails);
                    } else {
                        calcQuotPlan(quotation, planInfo, planDetails);
                    }
                }

                // remove invalid rider
                for (var l in removeRiderList) {
                    var code = removeRiderList[l];
                    for (var p = 1; p < quotation.plans.length; p++) {
                        if (quotation.plans[p].covCode == code) {
                            quotation.plans.splice(p, 1);
                        }
                    }
                }

                basicPlanInfo = quotation.plans[0];
                quotation.sumInsured = quotation.sumInsured || basicPlanInfo.sumInsured;
                quotation.policyTerm = basicPlanInfo.benefitTerm;
                quotation.premTerm = basicPlanInfo.premTerm;

                retVal.quotation = quotation;

                // Validate each plan by calculated values
                for (var i = 0; i < planInfoList.length; i++) {
                    var details = planDetails[planInfoList[i].covCode];
                    if (details.formulas && details.formulas.validAfterCalcFunc) {
                        runFunc(details.formulas.validAfterCalcFunc, quotation, planInfoList[i], details);
                    } else {
                        validatePlanAfterCalc(quotation, planInfoList[i], details);
                    }
                }

                // Validate by global rules
                validateGlobal(quotation, planInfoList, planDetails);
            }
        }
    }
    return retVal;
}

var generateIllustration = function(quotation, planDetails, extraPara) {
    var illustrations = {};
    var isIllustrationGenerated = true;

    try {
        for (var p in quotation.plans) {
            var planInfo = quotation.plans[p];
            if (planDetails && planDetails[planInfo.covCode]) {
                var planDetail = planDetails[planInfo.covCode];
                if (planDetail.formulas && planDetail.formulas.illustrateFunc) {
                    var illust = runFunc(planDetail.formulas.illustrateFunc, quotation, planInfo, planDetails, extraPara);
                    if (illust) {
                        illustrations[planInfo.covCode] = illust;
                    } else {
                        isIllustrationGenerated = false;
                    }
                }
            }
        }
    } catch(ex) {
        isIllustrationGenerated = false;
    }

    return {
        'illustrations': illustrations,
        'isIllustrationGenerated': isIllustrationGenerated
    };
}

/// function to prepare the data for genreate PDF
///     quotation - Quotation Model
///     planDetails - Map of covCode vs ProductModel
///     extraPara = {
/// *          BI Options          : Object, key value map of BI options
/// *          templateFuncs       : Object, map of template code vs PDF template formulas ( prepareReportDataFunc )
/// *          requireReportData   : bool, indicate whether need to generate report data
/// *          illustrations       : Object, map of plan code vs plan illustration if any
///                 }
var prepareReportData = function(quotation, planDetails, extraPara) {
    var reportData = {
        root: {
            mainInfo: {},
            planInfos: [],
            fundAllocation: [],
            removeBookmark: []
        }
    };

    //expiry date
    let expiryDate = new Date();
    let months = ["January", "February", "March", "April", "May", "June", "July","August", "September", "October", "November", "Devember"];
    expiryDate = new Date(new Date(expiryDate).setMonth(expiryDate.getMonth()+1));
    expiryDate = new Date(expiryDate.getTime() - 24*60*60*1000);
    let expiryDateStr = expiryDate.getDate() + ' ' + months[expiryDate.getMonth()] + ' ' + expiryDate.getFullYear();
    // MainInfo
    reportData.root.mainInfo.username = quotation.agent.agentCode;
    reportData.root.agent = quotation.agent;

    reportData.root.mainInfo.name = quotation.iLastName + ' ' + quotation.iFirstName;
    reportData.root.mainInfo.age = quotation.iAge;
    reportData.root.mainInfo.gender = quotation.iGender;
    reportData.root.mainInfo.isSmoker = quotation.iSmoke;
    reportData.root.mainInfo.currency = quotation.ccy;
    reportData.root.mainInfo.createDate = quotation.createDate;
    reportData.root.mainInfo.expiryDate = expiryDateStr;
    reportData.root.mainInfo.occupation = quotation.occupation;
    reportData.root.mainInfo.nationality = quotation.nationality;
    reportData.root.mainInfo.residence = quotation.residence;
    reportData.root.mainInfo.dob = quotation.iDob;

    if (quotation.pLastName || quotation.pFirstName) {
        reportData.root.mainInfo.pName = quotation.pLastName + ' ' + quotation.pFirstName;
        reportData.root.mainInfo.pAge = quotation.pAge;
        reportData.root.mainInfo.pGender = quotation.pGender;
        reportData.root.mainInfo.pIsSmoker = quotation.pSmoker;
        reportData.root.mainInfo.pOccupation = quotation.pOccupation;
        reportData.root.mainInfo.pNationality = quotation.pNationality;
        reportData.root.mainInfo.pResidence = quotation.pResidence;
        reportData.root.mainInfo.pDob = quotation.pDob;
    }
    reportData.root.agentName = quotation.agent.name;
    reportData.root.sysDate = extraPara.systemDate;

    var basicPlanDetail = planDetails[quotation.baseProductCode];

    if (basicPlanDetail) {
        reportData.root.mainInfo.basicPlanName = !basicPlanDetail.covName?"":(typeof basicPlanDetail.covName == 'string'?basicPlanDetail.covName:(basicPlanDetail.covName['en'] || basicPlanDetail.covName[Object.keys(basicPlanDetail.covName)[0]]));
        reportData.root.mainInfo.basicPlanCode = basicPlanDetail.covCode;
    }
    reportData.root.mainInfo.singlePremium = getCurrency(quotation.totYearPrem, null, (quotation.totYearPrem + '').indexOf('.') > 0 ? 2 : 0);
    reportData.root.mainInfo.yearlyPremium = getCurrency(quotation.totYearPrem, null, (quotation.totYearPrem + '').indexOf('.') > 0 ? 2 : 0);
    reportData.root.mainInfo.halfYearlyPremium = getCurrency(quotation.totHalfyearPrem, null, (quotation.totHalfyearPrem + '').indexOf('.') > 0 ? 2 : 0);
    reportData.root.mainInfo.quarterlyPremium = getCurrency(quotation.totQuarterPrem, null, (quotation.totQuarterPrem + '').indexOf('.') > 0 ? 2 : 0);
    reportData.root.mainInfo.monthlyPremium = getCurrency(quotation.totMonthPrem, null, (quotation.totMonthPrem + '').indexOf('.') > 0 ? 2 : 0);
    reportData.root.mainInfo.premium = getCurrency(quotation.premium, null, (quotation.premium + '').indexOf('.') > 0 ? 2 : 0);
    reportData.root.mainInfo.sumAssured = getCurrency(quotation.sumInsured, null, (quotation.sumInsured + '').indexOf('.') > 0 ? 2 : 0);

    // Plans
    var planInfoList = quotation.plans;

    for (var i = 0; i < planInfoList.length; i++) {
        var planInfo = planInfoList[i];
        var plan = {};
        plan.planName = !planInfo.covName?"":(typeof planInfo.covName == 'string'?planInfo.covName:(planInfo.covName['en'] || planInfo.covName[Object.keys(planInfo.covName)[0]]));
        plan.sumAssured = getCurrency(planInfo.sumInsured, ' ', 2);
        plan.policyTerm = (extraPara.langMap && extraPara.langMap[planInfo.policyTerm]) || planInfo.policyTerm;
        plan.premium = getCurrency(planInfo.premium, ' ', 2);
        plan.APremium = getCurrency(planInfo.yearPrem, ' ', 2);
        plan.HPremium = getCurrency(planInfo.halfYearPrem, ' ', 2);
        plan.QPremium = getCurrency(planInfo.quarterPrem, ' ', 2);
        plan.MPremium = getCurrency(planInfo.monthPrem, ' ', 2);
        plan.LPremium = getCurrency(planInfo.yearPrem, ' ', 2);
        plan.permTerm = (extraPara.langMap && extraPara.langMap[planInfo.premTerm]) ||  planInfo.premTerm;
        plan.payMode = quotation.paymentMode;

        reportData.root.planInfos.push(plan);
    }

    // Fund Allocations
    if (quotation.fundAllocation) {
        for (var i = 0; i < quotation.fundAllocation.length; i++) {
            var fund = {};

            if (quotation.fundAllocation[i].fundAllocs != 0) {
                fund.fundName = quotation.fundAllocation[i].fundName + ' [' + quotation.fundAllocation[i].fundCode + ']' ;
                fund.fundNameOth = quotation.fundAllocation[i].fundNameOth + ' [' + quotation.fundAllocation[i].fundCode + ']' ;
                fund.fundAllocs = quotation.fundAllocation[i].fundAllocs + ' %';
                reportData.root.fundAllocation.push(fund);
            }
        }
    }

    // execute prepare report data func in template
    if (basicPlanDetail.reportTemplate && extraPara.templateFuncs) {
        var planInfo = 0;
        for (var rt in basicPlanDetail.reportTemplate) {
            var template = basicPlanDetail.reportTemplate[rt];
            var tempFunc = extraPara.templateFuncs[template.code];
            if (tempFunc) {
              if (template.covCode && template.covCode != '0' && template.covCode != 'na') {
                  var found = false
                  for (var p in planInfoList) {
                      if (planInfoList[p].covCode == template.covCode) {
                          reportData[template.code+"/"+template.covCode] = runFunc(tempFunc, quotation, planInfoList[p], planDetails, extraPara);
                          found = true;
                          break;
                      }
                  }
                  if (!found) {
                      reportData[template.code] = runFunc(tempFunc, quotation, planInfoList[0], planDetails, extraPara);
                  }
              } else {
                  reportData[template.code] = runFunc(tempFunc, quotation, planInfoList[0], planDetails, extraPara);
              }
            }
        }
    }

    return reportData;
}
