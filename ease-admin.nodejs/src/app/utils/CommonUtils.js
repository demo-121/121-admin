var ConfigConstants = require('../constants/ConfigConstants');
var ActionTypes = ConfigConstants.ActionTypes;
var AppDispatcher = require('../dispatcher/AppDispatcher');
var DialogActions = require('../actions/DialogActions');
var AppStore = require('../stores/AppStore');
let SessionAlertActions = require('../actions/SessionAlertActions.js');

// used for clone object
window.cloneObject = function(obj) {
  var newObj = {};
  if(typeof obj == "string" || typeof obj == "number" || typeof obj == "boolean"){
    return obj;
  }else if(obj instanceof Array){
    return cloneArray(obj);
  }
  for (let k in obj) {
    var value = obj[k];
    if (value instanceof Array) {
      newObj[k] = cloneArray(value);
    } else if (value instanceof Date) {
      console.debug("date", new Date(value));
      newObj[k] = new Date(value);
    } else if (typeof(value) == 'object') {
      newObj[k] = cloneObject(value);
    } else {
      newObj[k] = value;
    }
  }
  return newObj;
}

// for clone array
window.cloneArray = function(obj) {
  var newObj = [];
  for (let k = 0; k < obj.length; k++) {
    var value = obj[k];
    if (value instanceof Array) {
      newObj.push(cloneArray(value));
    }else if (value instanceof Date) {
      console.debug("date", new Date(value));
      newObj.push(new Date(value));
    }else if (typeof(value) == 'object') {
      newObj.push(cloneObject(value));
    } else {
      newObj.push(value);
    }
  }

  return newObj;
}

window.saveDynDataOnServer = function(action, data) {
  //Show Loading
  if (!AppDispatcher.isDispatching()) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.UPDATE_LOADING_STATE,
      loadingState: true
    });
  }

  var dataValue = convertObj2EncodedJsonStr(data);
  var dataLength = dataValue.length;
  var packetNum = Math.ceil(dataLength/limit);

  submitBySlice(action, dataValue, packetNum, 0);
}

window.submitBySlice = function(path, data, packetNum, offset) {
  var dataLength = data.length;
  var startdigit = offset * limit;
  var endDigit = (offset + 1) * limit;
  if(endDigit > dataLength)
    endDigit = dataLength;
  var sliceData = data.slice(startdigit, endDigit);

  var request = new XMLHttpRequest();

  var param = tokenID + "=" + tokenKey + "&packetNum="+packetNum + "&offset="+offset + "&p0="+sliceData;
  var length = sliceData.length;
  console.debug("Uploading sliced data:", offset + "/" + packetNum, endDigit + "/" + dataLength);

  request.onreadystatechange = function() {
    if (request.readyState == 4 && request.status == 200) {
      var res = JSON.parse(request.responseText);
      console.debug("response", res);
      tokenID = res.tokenID;
      tokenKey = res.tokenKey;

      var status = res.uploadStatus;
      var isComplete = !!status ? status.isComplete : false;
      var isTeminate = !!status ? status.isTeminate : true;
      var newOffset = !!status ? status.offset : null;

      if(isComplete){
        if (!AppDispatcher.isDispatching()) {
          if(res.uploadStatus){
            AppDispatcher.dispatch({
              actionType: res.action,
              data: res,
              page: res.page,
              dialog: res.dialog,
              loadingState: res.loading || false
            })
          }
        }
        console.debug("completed");
      } else {
        if(res.action && res.action=='REDIRECT'){
          console.debug('[callServer] - [Response]', res.action, res);
          AppDispatcher.dispatch({
            actionType: res.action,
            path: res.path,
            page: res.page,
            data: res
          });
        } else {
          if(isTeminate ===false){
            submitBySlice(path, data, packetNum, newOffset || offset + 1);
          } else {
            console.debug("Teminated");
            SessionAlertActions.reset();
            DialogActions.showDialog({
              title: getLocalizedText('DIALOG.TITLE.WARNING', 'Warning'),
              message: getLocalizedText('DIALOG.MSG.SAVE.FAIL', 'Save failed'),
              width: 50,
              actions: [{
                type: 'cancel',
                title: getLocalizedText('BUTTON.CANCEL', 'Cancel'),
                secondary: true,
              }]
            })
          }
        }
      }
    }
  };

  request.open("POST", ROOT_CONTEXT_PATH + path, true);
  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  request.send(param);

  return true;
}

var tokenID = null;
var tokenKey = null;
var limit = 200000;
//var limit = 10;
window.submitAttachment = function(path, data, module, id, version){
  //1.show dialog
  if (!AppDispatcher.isDispatching()) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.UPDATE_LOADING_STATE,
      loadingState: true
    });
  }
  //console.debug("submitAttachment", path, data);
  var dataValue = convertObj2EncodedJsonStr(data);
  //dataValue = "1234567891011121314151617181920";
  var success = true;
  //20971520 - > 20 mb
  //var limit = 1000000;
  //512k
  var dataLength = dataValue.length;
  var packetNum =  Math.floor(dataLength/limit);
  if(packetNum < dataLength/limit) packetNum += 1;

  var done = submitAttachmentSlice(path, module, id, version, dataValue, packetNum, 0);
}

window.submitAttachmentSlice = function(path, module, id, version, data, packetNum, offset){
  //console.debug("submitAttachmentSlice", path, data);
  //var limit = 446400;
  var dataLength = data.length;
  var startdigit = offset * limit;
  var endDigit = (offset + 1) * limit;
  if(endDigit > dataLength)
    endDigit = dataLength;
  var sliceData = data.slice(startdigit, endDigit);

  var request = new XMLHttpRequest();
  console.debug("param", tokenID + "=" + tokenKey + "&module="+module + "&id="+id + "&version="+version + "&packetNum="+packetNum + "&offset="+offset);
  var param = tokenID + "=" + tokenKey + "&module="+module + "&id="+convertObj2EncodedJsonStr(id) + "&version="+version + "&packetNum="+packetNum + "&offset="+offset + "&p0="+sliceData;

  var length = sliceData.length;
  console.debug("sliceDataLenght", length);
  request.onreadystatechange = function() {
    if (request.readyState == 4 && request.status == 200) {
      var res = JSON.parse(request.responseText);
      console.debug("response", res);
      tokenID = res.tokenID;
      tokenKey = res.tokenKey;

      var isComplete = res.isComplete;
      if(isComplete){
        if (!AppDispatcher.isDispatching()) {
          if(res.content&&res.selectPageId){
            AppDispatcher.dispatch({
              actionType: res.action,
              data: res,
              page: res.page,
              dialog: res.dialog
            })
          }
          AppDispatcher.dispatch({
            actionType: ActionTypes.UPDATE_LOADING_STATE,
            loadingState: false
          });
        }
        console.debug("completed");
      }else{
        if(res && res.action && res.action=='REDIRECT'){
          console.debug('[callServer] - [Response]', res.action, res);
          var action= {
            actionType: res.action,
            path: res.path,
            page: res.page,
            data: res
          }
          AppDispatcher.dispatch(action);
        }else{
          var isTeminate = res.isTeminate;

          if(isTeminate===false){
              var newOffset = res.offSet;
              submitAttachmentSlice(path, module, id, version, data, packetNum, newOffset);
          }else{
            console.debug("uncompleted but teminated");
            SessionAlertActions.reset();
            DialogActions.showDialog({
              title: getLocalizedText('DIALOG.TITLE.WARNING', 'Warning'),
              message: getLocalizedText('DIALOG.MSG.UPLOAD.FAIL', 'Upload failed'),
              width: 50,
              actions: [{
                type: 'cancel',
                title: getLocalizedText('BUTTON.CANCEL', 'Cancel'),
                secondary: true,
              }]
            })
          }
        }

      }
    }
  };

  request.open("POST", ROOT_CONTEXT_PATH + path, true);
  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  request.send(param);

  return true;
}
// call server with path and para in object or string
window.callServer = function(actionPath, data, callback) {
  // console.log('actionPath: ', actionPath)
  // console.log('data: ', data)
  // console.log('callback: ', callback)
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) { // done
      var action = null;
      if (xhttp.status == 200 && xhttp.responseText) {
        var res = JSON.parse(xhttp.responseText);
        console.debug("ress", res);
        if (res && res.action) {
            console.debug('[callServer] - [Response]', res.action, res);
            action = {
              actionType: res.action,
              path: res.path,
              page: res.page,
              dialog: res.dialog,
              data: res
            }

            tokenID = res.tokenID;
            tokenKey = res.tokenKey;

            SessionAlertActions.reset();
        }
        if (callback) {
          callback(res);
        }
      } else {
        if (callback) {
          callback(xhttp);
        }
        console.warn("[callServer and call back]: failure:", xhttp);
      }

      if (!action) {
        // var res = JSON.parse(xhttp.responseText);
        console.error('error !!!! ',xhttp.responseText);
        action = {
          actionType: ActionTypes.PAGE_REDIRECT,
          path: '/Error',
          data: {
            content: xhttp.responseText
          }
        }
      }
      //
      // // for Tests
      // if (callback) {
      //   var testAction = callback(actionPath, data)
      //   if (testAction) {
      //     action = testAction;
      //   }
      // }
      // // end for test

      // show dialog
       if (action.dialog) {
         DialogActions.showDialog(action.dialog);
       }

      // console.debug('call dispatch:', xhttp.readyState);
      // dispatch action to stores

      AppDispatcher.dispatch(action);

      // actionChain will be call after the dispatch, ref to BCP Back-Page funtion 
      try {
        if( typeof(res.actionChain) !== 'undefined') {
          if(res.actionChain.callServer) {
            // console.log('preform action chain');
            callServer(res.actionChain.path, res.actionChain.data.map);
            return;
          }
        }  
      } catch(e) {

      }
          
    }
  };

  console.debug('[callServer] - [Path]', actionPath);

  if (!data) {
    data = "";
  } else if (typeof(data) == 'object') {
    data = convertObj2ParaStr(data);
  } 

  // Append Token
  var param = tokenID + "=" + tokenKey + (data ? "&" + data : "");

  // console.debug('[callServer] - param', param);

  if (!AppDispatcher.isDispatching()) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.UPDATE_LOADING_STATE,
      loadingState: true
    });
  }

  xhttp.open("POST", ROOT_CONTEXT_PATH + actionPath, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(param);

  ///TODO: may need to call up loading panel
};

// call server with path and para in object or string
window.callServerAndCallback = function(actionPath, data, callback, failCallback) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) { // done
      var action = null;
      if (xhttp.status == 200 && xhttp.responseText) {
        var res = JSON.parse(xhttp.responseText);
        console.debug("response", actionPath, res);
        tokenID = res.tokenID;
        tokenKey = res.tokenKey;
        SessionAlertActions.reset();

        if (res && res.action) {
            console.debug('[callServer] - [Response]', res.action, res);
            SessionAlertActions.reset();
            AppDispatcher.dispatch({
              actionType: res.action,
              path: res.path,
              page: res.page,
              dialog: res.dialog,
              data: res
            });
        } else if (callback) {
          callback(res)
        } else {
          AppDispatcher.dispatch({
            actionType: UPDATE_LOADING_STATE,
            loadingState: false
          });
        }
      } else {
        if (failCallback) {
          failCallback(xhttp);
        }
        console.warn("[callServer and call back]: failure:", xhttp);
      }
    }
  };

  console.debug('[callServer and call back] - [Path]', actionPath);

  if (!data) {
    data = "";
  } else if (typeof(data) == 'object') {
    data = convertObj2ParaStr(data);
  }

  // Append Token
  var param = tokenID + "=" + tokenKey + (data ? "&" + data : "");

  if (!AppDispatcher.isDispatching()) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.UPDATE_LOADING_STATE,
      loadingState: true
    });
  }

  xhttp.open("POST", ROOT_CONTEXT_PATH + actionPath, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(param);
};

var getFileQueue = [];
var gettingFile = false;

window.getFile = function(url, data, callback) {

  if (!gettingFile) {
    gettingFile = true;
    var param = convertObj2ParaStr(data);
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        gettingFile = false;
        try {
          var res = JSON.parse(xhr.responseText);
          console.debug("ress", res);
          callback(res);
          if (getFileQueue.length) {
            var next = getFileQueue.pop();
            getFile(next.url, next.data, next.callback);
          }
        } catch(e) {
          console.error("getFile: Exception:", e, url);
          callback(null);
        }
      // } else {
      //   callback(null);
      }
    }
    xhr.open('POST', ROOT_CONTEXT_PATH + url, true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.send(param);
  } else {
    getFileQueue.push({
      url: url,
      data: data,
      callback: callback
    })
  }

}

window.objToString =  function (obj) {
    var str = '';
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            str += p + '::' + obj[p] + '\n';
        }
    }
    return str;
}

// call server with time out
var _delayCallback = false;
window.callServerWithWaiting = function(latency, actionPath, data, callback) {
  suspendCallServer();
  _delayCallback = setTimeout(callServer, latency, actionPath, data, callback);
};

window.suspendCallServer = function() {
  if (_delayCallback) {
    clearTimeout(_delayCallback);
  }
}

window.debugLog = function(...msg) {
  console.debug({...msg});
};

window.format = function date2str(x, y) {
    var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds()
    };
    y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
    });

    return y.replace(/(y+)/g, function(v) {
        return x.getFullYear().toString().slice(-v.length)
    });
};

window.genRecordDateBy = function(str, values) {
  var user = AppStore.getUser();
  var dateFormat = user && user.dateFormat ? user.dateFormat : ConfigConstants.dateFormat;
  var timeFormat = user && user.timeFormat ? user.timeFormat : ConfigConstants.TimeFormat;

  var props = ["createDate", "createBy", "modifyDate", "modifyBy", "approvDate", "approvBy"];
  if (!str || !values) return '';
  for (let i in props) {
    if (i%2) {
      str = str.replace("{"+i+"}", values[props[i]]);
    } else {
      if (values[props[i]]) {
        var d = new Date(values[props[i]]);
        if (d) {
          str = str.replace("{"+i+"}", d.format(dateFormat+" "+timeFormat));
        }
      }
    }
  }
  return str;
};

window.getLocalizedText = function(langKey, defaultText) {
  var langMap = AppStore.getLangMap();
  return (langMap && langMap[langKey])?langMap[langKey]:(defaultText?defaultText:langKey)
};

window.getLocalText = function(lang, text) {
  if (!lang) lang = 'en';
  return (!isEmpty(text) && typeof text == 'object')?(text[lang]||text[Object.keys(text)[0]]||""):(text||"");
};

window.arrayContains = function(array, obj) {
  for (let i in array) {
    if (obj == array[i]) {
      return true;
    }
  }
  return false;
};

// return true if two object are different by loop thu field
window.checkDiff = function(field, obj1, obj2) {
  var v1 = obj1, v2 = obj2;
  if (field.id) {
    v1 = obj1[field.id]?obj1[field.id]:obj1;
    v2 = obj2[field.id]?obj2[field.id]:obj2;
  }
  if (typeof v2 == 'object') {
    if (typeof v1 == 'object') {
      if (field.items) {
        for (let i in field.items) {
          var f = field.items[i];

          if (f.type && "CHECKBOX" == f.type.toUpperCase()) {
            var vv1 = v1[f.id]?v1[f.id]:"N";
            var vv2 = v2[f.id]?v2[f.id]:"N";
            if (vv1 != vv2) return true;
          } else if (checkDiff(f, v1, v2)) {
            return true;
          }
        }
      }
    } else {
      return true;
    }
  } else {
    if (checkChanges(v1, v2)) {
      return true;
    }
  }
  return false;
};

// return true if two objects are different
window.checkChanges = function(obj1, obj2) {
  if(isEmpty(obj2) && isEmpty(obj1))
    return false;
  if (typeof(obj1) == 'object' && typeof(obj2) == 'object') {
    if (obj1 instanceof Array && obj2 instanceof Array) {
      if (obj1.length != obj2.length) {
        return true;
      } else {
        for (let i in obj1) {
          let v1 = obj1[i];
          let v2 = obj2[i];
          if (checkChanges(v1,v2)) {
            return true;
          }
        }
      }
    } else if (obj1 instanceof Date && obj2 instanceof Date){
      if (obj1.getTime() != obj2.getTime()) {
        return true;
      }
    } else {
      if (obj1 && obj2) {
        for (let key in obj1) {
          // skip check input param changes for performance
          if (typeof obj1[key] != 'function' && key != "inputParams") {
            if (checkChanges(obj1[key], obj2[key])){
              return true;
            }
          }
        }

        for (let key in obj2) {
          // skip check input param changes for performance
          if (typeof obj2[key] != 'function' && key != "inputParams") {
            if (checkChanges(obj1[key], obj2[key])){
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  return obj1 != obj2;
};


// return error message if it is not valid
window.validate = function(field, values, pField, pKey) {
  // check mandatory
  var value = undefined;
  if(!values){
    return null;
  }
  if (field.id) {
    value = values[field.id];
  }

  if (!showCondition(field, values, values, null)) {
    return false;
  }

  if (isEmpty(value)) {
    if (field.mandatory) {
      if (pField && pKey) {
        return getLocalizedText("VALIDATE_MSG.MANDATORY_IN_LIST", "{1} is required at {2} for item {3}.")
        .replace('{1}', field.title).replace('{2}', pField.title || pField.id).replace('{3}', pKey);
      }
      if (field.type && field.type.toUpperCase() == 'CHECKBOXGROUP') {
        return getLocalizedText("VALIDATE_MSG.MANDATORY_CBGROUP", "Please check at least one box.").replace('{1}', field.title);
      }
      return getLocalizedText("VALIDATE_MSG.MANDATORY", "{1} is required.").replace('{1}', field.title);
    }
  } else if (field.type) {
    if (field.type.toUpperCase() == 'LIST') {
    }
    else if (field.type.toUpperCase() == 'MULTTEXT') {
      if (field.mandatory) {
        for (let z in field.items) {
          var sitem = field.items[z];
          var sval = value[sitem.id];
          if (!(sval || sval === 0) || sval == "") {
            return getLocalizedText("VALIDATE_MSG.MANDATORY_MISS_SUBITEM", "Missing {1}").replace('{1}', sitem.title);
          }
        }
      }
    }
    else if (field.type.toUpperCase() == 'COMPLEXLIST') {
      // check default is set, if it is required to check
      // for (let si in field.items) {
      //   var sitem = field.items[si];
      //   if (sitem.id == "default") {
      //     if (value && value instanceof Array) {
      //       var hasDefault = false;
      //       for (let svi in value) {
      //         if (value[svi].default == 'Y') {
      //           hasDefault = true;
      //           break
      //         }
      //       }
      //       if (!hasDefault) {
      //         return getLocalizedText("VALIDATE_MSG.MISS_DEFAULT", "Missing default selection.").replace('{1}', field.title);
      //       }
      //     } else {
      //       return getLocalizedText("VALIDATE_MSG.MANDATORY", "This field is required.").replace('{1}', field.title);
      //       // return "Missing " + field.title;
      //     }
      //     break;
      //   }
      // }

      // check if key field found
      var keyFieldIds = [];
      for (let si in field.items) {
        var sitem = field.items[si];

        if (sitem.trigger && sitem.trigger.type == 'optionCondition' && sitem.trigger.id) {
          for (let sii in field.items) {
            let kItem = field.items[sii];
            if (kItem.id == sitem.trigger.id) {
              keyFieldIds.push(kItem);
            }
          }
        }
      }

      // check if values of keyField has same
      if (keyFieldIds.length) {
        for (let sv in value) {
          for (let svv in value) {
            if (sv != svv) {
              let hasDiff = false;
              for (let ki in keyFieldIds) {
                let key = keyFieldIds[ki].id;
                if (value[sv][key] != value[svv][key]) {
                  hasDiff = key;
                  break;
                }
              }
              if (!hasDiff) {
                let kItem = keyFieldIds[0];
                let dvalue = value[sv][kItem.id];
                if (kItem.options) {
                  for (let oi in kItem.options) {
                    var option = kItem.options[oi];
                    if (option.value == dvalue) {
                      dvalue = option.title;
                      break;
                    }
                  }
                }
                return getLocalizedText("VALIDATE_MSG.DUPLICATE_RECORD4VALUE", "Duplicate {1} found for {2} at {3}.")
                .replace('{1}', field.title)
                .replace('{2}', dvalue )
                .replace('{3}', pField.title || pField.id);
              }
            }
          }
        }
      }
    }
    else if (field.type.toUpperCase() == 'TEXT') {
      if (value && field.subType && field.subType.toUpperCase() == 'EMAIL') {
        if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(value)) {
        } else {
          return getLocalizedText("VALIDATE_MSG.INVALID_EMAIL", "Invalid email address.");
        }
      }
      // else if (field.subType && field.subType.toUpperCase() == 'TEL') {
      //   if (/^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/.test(value)) {
      //   } else {
      //     return getLocalizedText("VALIDATE_MSG.INVALID_TEL", "Invalid telephone number.");
      //   }
      // }
      else if (field.subType && field.subType.toUpperCase() == 'NUMBER') {
        if (isNaN(value)) {
          return getLocalizedText("VALIDATE_MSG.INVALID_NUMBER", "Invalid number.").replace("{1}", field.min);
        }
        if (value < field.min) {
          return getLocalizedText("VALIDATE_MSG.INVALID_NUMBER_MAX", "Amount should be larger than or equal to {1}.").replace("{1}", field.min);
        }
        if (value > field.max) {
          return getLocalizedText("VALIDATE_MSG.INVALID_NUMBER_MIN", "Amount should be less than or equal to {1}.").replace("{1}", field.max);
        }
        if (field.interval && field.interval > 0) {
          if (value % field.interval) {
            return getLocalizedText("VALIDATE_MSG.INVALID_INTERVAL", "Amount should be divisible with {1}.").replace("{1}", field.interval);
          }
        }
      }
      else {
        if (field.min && field.min > 0) {
          if (typeof value == 'string') {
              if (value.length < field.min) {
              return getLocalizedText("VALIDATE_MSG.INVALID_LENGTH_MIN", "Length should be longer than or equal to {1}.").replace("{1}", field.min);
            }
          }
        }
        if (field.max && field.max > 0) {
          if (typeof value == 'string') {
            if (value.length > field.max) {
              return getLocalizedText("VALIDATE_MSG.INVALID_LENGTH_MAX", "Length should be shorter than or equal to {1}.").replace("{1}", field.max);
            }
          }
        }
      }
    }
    else if (field.type.toUpperCase() == 'JSFIELD') {
      if (typeof value != 'object') {
        return getLocalizedText("VALIDATE_MSG.INVALID_JSON", "Invalid JSON");
      }
    }
  }

  var itemValues = value || values || {};
  if (field.items) {
    if (itemValues instanceof Array) {
     if (typeof itemValues[0] == 'object') { // complex list and table
        for (let v in itemValues) {
          for (let i in field.items) {
            var msg = validate(field.items[i], itemValues[v], field, v)
            if (msg) return msg;
          }
        }
      }
    } else if (typeof itemValues == 'object' && (field.type.toUpperCase() == 'DETAILSLIST' || field.type.toUpperCase() == 'DETAILSTAB')) {
      for (let v in itemValues) {
        for (let i in field.items) {
          var msg = validate(field.items[i], itemValues[v], field, v)
          if (msg) return msg;
        }
      }
    } else if (typeof itemValues == 'object') {
      for (let i in field.items) {
          var msg = validate(field.items[i], itemValues, pField, pKey)
          if (msg) return msg;
        }
    }
  }

  return false;
};

// return the different of obj2 from obj1
window.foundDiff = function(obj1, obj2) {
  if(isEmpty(obj2) && isEmpty(obj1))
    return undefined;
  if (typeof(obj1) == 'object' && typeof(obj2) == 'object') {
    if (obj1 instanceof Array && obj2 instanceof Array) {
      if (obj1.length != obj2.length) {
        return obj2;
      } else {
        for (let i in obj1) {
          let v1 = obj1[i];
          let v2 = obj2[i];
          if (foundDiff(v1, v2) != undefined) {
            return obj2;
          }
        }
      }
    } else
    if (obj1 instanceof Date && obj2 instanceof Date){
      if (obj1.getTime() != obj2.getTime()) {
        return obj2
      }
    } else {
      if (obj1 && obj2) {
        var diff = {};
        for (let k1 in obj1) {
          if (typeof obj2[k1] != 'function') {
            let d = foundDiff(obj1[k1], obj2[k1]);
            if (d != undefined) {
              diff[k1] = d;
            }
          }
        }
        for (let k2 in obj2) {
          if (!diff[k2] && typeof obj2[k2] != 'function') {
            let d = foundDiff(obj1[k2], obj2[k2]);
            if (d != undefined) {
              diff[k2] = d;
            }
          }
        }
        if (Object.keys(diff).length) {
          return diff;
        }
      } else {
        return obj2;
      }
    }
  } else if (obj1 != obj2) {
    return obj2;
  }
  return undefined;
}

// prase object to para string
window.convertObj2ParaStr = function(obj) {
  var para = '';
  for (let key in obj) {
    if (obj[key]) {
      para += (para ? '&' : '') + (key + '=' + obj[key]);
    }
  }
  return para;
};

// prase obj to encoded json string
window.convertObj2EncodedJsonStr = function(obj) {
  var jsonStr = JSON.stringify(obj);
  var encodedStr = window.btoa(encodeURI(jsonStr).replace(/%([0-9A-F]{2})/g,
  function(jsonStr, h) {
    return String.fromCharCode(parseInt(h, 16))
  })).replace(" ", "+");
  console.debug('[convertObj2EncodedJsonStr]', obj);
  return encodedStr;
}

window.mergeObj = function(obj1, obj2){
  var keys = Object.keys(obj2);
  for (let i in keys){
    var key = keys[i];
    obj1[key]= cloneObject(obj2[key]);
  }
  return obj1;
}

window.mergeObject = function(obj1, obj2) {
  if (!obj1 || !(typeof obj1 == 'object')) {
    obj1 = {};
  }
  if (isEmpty(obj2)) {
    return obj1
  }

  for (var key in obj2) {
    if (!obj1[key]) {
      obj1[key] = obj2[key];
    } else {
      if (typeof obj2[key] == 'object') {
        if (obj2[key] instanceof Array) {
          obj1[key] = mergeArray(obj1[key], obj2[key]);
        } else {
          obj1[key] = mergeObject(obj1[key], obj2[key]);
        }
      } else {
        obj1[key] = obj2[key];
      }
    }
  }
  return obj1;
}


window.mergeArray = function(obj1, obj2) {
  if (!obj1 || !(obj1 instanceof Array)) {
    obj1 = [];
  }
  if (isEmpty(obj2)) {
    return obj1
  }

  for (var key = 0; key < obj2.length; key++) {
    if (obj1.length <= key) {
      obj1.push(obj2[key]);
    } else {
      if (typeof obj2[key] == 'object') {
        if (obj2[key] instanceof Array) {
          obj1[key] = mergeArray(obj1[key], obj2[key]);
        } else {
          obj1[key] = mergeObject(obj1[key], obj2[key]);
        }
      } else {
        obj1[key] = obj2[key];
      }
    }
  }
  return obj1;
}

window.alert = function(msg, title, negative, positive, action){
  var dialog = {
    title: title ? title : "",
    message: msg ? msg : "",
  };

  if (positive && action) {
    var btn_positive = {
      type: 'customButton',
      title: positive,
      primary: true,
      handleTap: action,
    };
    dialog.positive = btn_positive;
  }

  var btn_negative = {
    type: 'cancel',
    title: negative || (positive ? "CANCEL" : "OK"),
    secondary: true,
  };
  dialog.negative = btn_negative;

  DialogActions.showMessageDialog(dialog);
}
window.isNumber = function(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
window.getCurrency = function(value, sign, decimals) {
    if (!isNumber(value)) {
        return '-';
    }
    if (!isNumber(decimals)) {
        decimals = 2
    }
    var text = "" + parseFloat(value).toFixed(decimals);
    var parts = text.split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return (sign && sign.trim().length > 0 ? sign : '') + parts.join(".");
};

window.b64toBlob = function(b64Data, contentType, sliceSize) {
  contentType = contentType || '';
  sliceSize = sliceSize || 512;

  var byteCharacters = atob(b64Data);
  var byteArrays = [];

  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);

    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    var byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

  var blob = new Blob(byteArrays, {type: contentType});
  return blob;
}

window.isEmpty = function (obj) {

    // null and undefined are "empty"
    if (obj == null || obj == undefined || (typeof obj === 'string' && obj == '')) return true;

    if (typeof obj === 'string' || typeof obj === 'number' || obj instanceof Date || typeof obj === 'boolean')
      return false;

    if (obj instanceof Array && obj.length)
      return false;

    if (typeof obj === 'object') {
      var keys = Object.keys(obj);
      for(var i in keys){
        if(typeof obj[keys[i]] != 'function' && !isEmpty(obj[keys[i]]))
          return false;
      }
    }
    return true;
}

window.checkExist = function(obj, key){
  if(isEmpty(key)) return false;
  let keys = key.split(".");
  if(keys.length==1){
    return (obj && obj[key]);
  }else{
    return key.split(".").every(function(x) {
        if(typeof obj != "object" || obj === null || ! x in obj)
            return false;
        obj = obj[x];
        if(!obj)
          return false;
        return true;
    });
  }
}

window.multilineText = function(txt){
  var out=[];
  if(txt!=null){
    var txtArray=txt.split("\\n");
    for(var i =0; i<txtArray.length;i++){
      out.push(<div>{txtArray[i]}<br/></div>);
    }
  }else {
    out.push(<div> </div>);
  }

  return out;
}

window.isNumeric = function(obj) {
  return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
}

window.getUniqueId = function uniqueId () {
    // desired length of Id
    var idStrLen = 32;
    // always start with a letter -- base 36 makes for a nice shortcut
    var idStr = (Math.floor((Math.random() * 25)) + 10).toString(36) + "_";
    // add a timestamp in milliseconds (base 36 again) as the base
    idStr += (new Date()).getTime().toString(36) + "_";
    // similar to above, complete the Id using random, alphanumeric characters
    do {
        idStr += (Math.floor((Math.random() * 35))).toString(36);
    } while (idStr.length < idStrLen);

    return (idStr);
}

window.getFieldFmTemplate = function(field, path, level, tgtType) {

  if (level === 0) {
    for(var i in field.items) {
      var item = field.items[i];
      if (item.type.toLowerCase() == 'section' && item.id == path[level]) {
        return getFieldFmTemplate(item, path, 1);
      } else if (item.type.toLowerCase() == 'sectiongroup') {
        var found = getFieldFmTemplate(item, path, 0);
        if (found) {
          return found;
        }
      }
    }
  } else {
    // last
    var subPaths = null;
    for (var j in field.items) {
      var item = field.items[j];
      if (item.id == path[level]) {
        if (path.length - 1 === level && (!tgtType || item[tgtType])) {
          return item;
        } else {
          return getFieldFmTemplate(item, path, level + 1);
        }
      }
      var found = getFieldFmTemplate(item, path, level);
      if (found) {
        return found;
      }
    }
  }
  return false;
}

window.getValueFmRootByRef = function(values, referField, defValue) {
  if (!referField) return values;

  var ids = referField.split('/');
  var fvalue = values;
  for (var ii = 0; ii < ids.length; ii++) {

    var idd = ids[ii];
    var idds = idd.split('@');
    // get distinct value
    if (idds.length > 1) {
      var iddd = idds[0];
      if (fvalue[iddd]) {
        var distValue = []
        var tArr = fvalue[iddd];
        if (tArr instanceof Array) {
          for (var iii in tArr) {
            var iValue = tArr[iii]
            var tValue = iValue[idds[1]];
            if (tValue && tValue != "") {
              var tgts = tValue.split(',');
              for (var i in tgts) {
                if (tgts[i] && distValue.indexOf(tgts[i]) < 0) {
                  distValue.push(tgts[i]);
                }
              }
            }
          }
        }
        fvalue = distValue;
        break;
      } else {
        fvalue = [];
      }
    } else if (fvalue) {
      if (!fvalue[idd]) {
        fvalue = defValue;
        break;
      }
      fvalue = fvalue[idd];
    }
  }
  return fvalue;
}

window.IsJsonString = function(str) {
  try {
      JSON.parse(str);
  } catch (e) {
      console.warn("Json parse wrong  -  ", e);
      return false;
  }
  return true;
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};
