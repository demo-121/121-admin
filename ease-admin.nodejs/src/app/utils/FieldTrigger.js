let ConfigConstants = require('../constants/ConfigConstants.js')
let AppStore = require('../stores/AppStore.js')

window.performValueUpdateTrigger = function (field, orgValue, values, rootValues) {
  var value = orgValue;
  if (!field.trigger) {
    return value;
  }
  var triggers = field.trigger;

  if (!(triggers instanceof Array)) {
    triggers = [triggers];
  }

  for (var t in triggers) {
    var trigger = triggers[t];
    if (!trigger) continue;
    if (trigger.type == 'valueOverride') {
      var fulfilled = false
      var cond = trigger.id.split('=');
      if (cond.length == 2) {
        let _value = getValueFmRootByRef(values, cond[0]);
        if (!_value && rootValues) _value = getValueFmRootByRef(rootValues, cond[0]);
        if (!isEmpty(_value)) {
          if (cond.length == 1) {
            cond.push(value)
          }
          if (_value == cond[1]) {
            fulfilled = true
          }
        }
      }
      if (!fulfilled) {
        cond = trigger.id.split('<');
        if (cond.length == 2) {
          let _value = getValueFmRootByRef(values, cond[0]);
          if (!_value && rootValues) _value = getValueFmRootByRef(rootValues, cond[0]);
          if (!isEmpty(_value)) {
            if (cond.length == 1) {
              cond.push(value)
            }
            if (_value < cond[1]) {
              fulfilled = true
            }
          }
        }
      }
      if (!fulfilled) {
        cond = trigger.id.split('>');
        if (cond.length == 2) {
          let _value = getValueFmRootByRef(values, cond[0]);
          if (!_value && rootValues) _value = getValueFmRootByRef(rootValues, cond[0]);
          if (!isEmpty(_value)) {
            if (cond.length == 1) {
              cond.push(value)
            }
            if (_value > cond[1]) {
              fulfilled = true
            }
          }
        }
      }
      if (fulfilled) {
        if (trigger.value || trigger.value === 0) {
          value = trigger.value;
        } else if (trigger.reference && values[trigger.reference] != undefined) {
          value = values[trigger.reference];
        } else if (_value != undefined) {
          value = _value;
        }
        values[field.id] = value;
      }
    } else
    if (trigger.type == 'defaultValueOverride') {
      if (!(value || value === 0)) { // only apply if value is not be set
        var fulfilled = false
        var cond = trigger.id.split('=');
        var _value = getValueFmRootByRef(values, cond[0]);
        if (!_value && rootValues) _value = getValueFmRootByRef(rootValues, cond[0]);
        if (!isEmpty(_value)) {
          if (cond.length == 1) {
            cond.push(value)
          }
          if (_value == cond[1]) {
            fulfilled = true
          }
        }
        if (!fulfilled) {
          cond = trigger.id.split('<');
          var _value = getValueFmRootByRef(values, cond[0]);
          if (!_value && rootValues) _value = getValueFmRootByRef(rootValues, cond[0]);
          if (!isEmpty(_value)) {
            if (cond.length == 1) {
              cond.push(value)
            }
            if (_value < cond[1]) {
              fulfilled = true
            }
          }
        } else if (!fulfilled) {
          cond = trigger.id.split('>');
          var _value = getValueFmRootByRef(values, cond[0]);
          if (!_value && rootValues) _value = getValueFmRootByRef(rootValues, cond[0]);
          if (!isEmpty(_value)) {
            if (cond.length == 1) {
              cond.push(value)
            }
            if (_value > cond[1]) {
              fulfilled = true
            }
          }
        }
        if (fulfilled) {
          if (trigger.value || trigger.value === 0) {
            value = trigger.value;
          } else if (trigger.reference && values[trigger.reference] != undefined) {
            value = values[trigger.reference];
          } else if (_value != undefined) {
            value = _value;
          }
        }
      }
    } else
    if (trigger.type == 'stringReplace' && typeof orgValue == 'string') {
      var keys = trigger.id.split(',');
      // var types = trigger.id.split(',');
      var iValues = [];
      var user = AppStore.getUser();
      for (let i in keys) {
        if (keys[i]) {
          if (keys[i] == 'date') {
            var d = new Date(values[keys[0]]);
            iValues.push(d.format(user.dateFormat?user.dateFormat:ConfigConstants.DateFormat));
          } else
          if (keys[i] == 'time') {
            var d = new Date(values[keys[0]]);
            iValues.push(d.format(user.timeFormat?user.timeFormat:ConfigConstants.TimeFormat));
          } else
          if (keys[i] == 'dateTime') {
            var d = new Date(values[keys[0]]);
            var df = user.dateFormat?user.dateFormat:"";
            df += user.timeFormat?(" " +user.timeFormat):"";
            iValues.push(d.format(df?df:ConfigConstants.DateTimeFormat));
          } else {
            iValues.push(values[keys[0]]);
          }
        } else {
          iValues.push(values[keys[0]]);
        }
      }

      for (let i = 1; i <= keys.length; i++) {
        value = value.replace("{"+i+"}", iValues[i-1]);
      }
    }
  }
  return value;
}

window.showCondition = function(field, rootValues, values, showCondItems) {
  if (!field.trigger) {
    return true;
  }
  var triggers = field.trigger;

  if (!(triggers instanceof Array)) {
    triggers = [triggers];
  }

  var show = true;

  for (var t in triggers) {
    var trigger = triggers[t];
    if (!trigger) continue;
    var lookUpId = trigger.id;
    var lookUpValue = trigger.value && trigger.value.toUpperCase() || "";

    let _value = getValueFmRootByRef(values, lookUpId);
    if (!_value && rootValues) _value = getValueFmRootByRef(rootValues, lookUpId);

    if (trigger.type == 'showIfContains') {

      if (showCondItems) {
        if (showCondItems[trigger.id] && showCondItems[trigger.id] instanceof Array) {
        } else {
          showCondItems[trigger.id] = [];
        }
        showCondItems[trigger.id].push(field.id);
      }

      var found = false;
      if(_value && typeof _value === "string"){
        _value = _value.toUpperCase().split(",");
        // if (_value.indexOf(lookUpValue) >= 0) {
        //   found = true;
        // }
        for (let i in _value){
          if(lookUpValue == _value[i].trim()){
            // show &= true;
            found = true;
            break;
          }
        }
      }
      if (!found) {
        show &= false;
      }
    } else
    if (trigger.type == 'showIfExists') {
      if (showCondItems) {
        if (showCondItems[trigger.id] && showCondItems[trigger.id] instanceof Array) {
        } else {
          showCondItems[trigger.id] = [];
        }
        showCondItems[trigger.id].push(field.id);
      }
      if (_value && typeof _value == 'string') {
        lookUpValue = lookUpValue.split(',');
        if (lookUpValue.indexOf(_value.toUpperCase()) < 0) {
          show &= false;
        }
      } else {
        show &= false;
      }
    } else
    if (trigger.type == 'showIfEqual') {
      if (showCondItems) {
        if (showCondItems[trigger.id] && showCondItems[trigger.id] instanceof Array) {
        } else {
          showCondItems[trigger.id] = [];
        }
        showCondItems[trigger.id].push(field.id);
      }

      if(_value && typeof _value == 'string' && lookUpValue.toUpperCase() == _value.toUpperCase()) {
        // return true;
      } else if (!_value && (lookUpValue == 'N' || !lookUpValue)) {
        // return true;
      } else {
        show &= false;
      }
    } else
    if (trigger.type == 'hideIfEqual') {
      if (showCondItems) {
        if (showCondItems[trigger.id] && showCondItems[trigger.id] instanceof Array) {
        } else {
          showCondItems[trigger.id] = [];
        }
        showCondItems[trigger.id].push(field.id);
      }

      if(_value && typeof _value == 'string' && lookUpValue.toUpperCase() == _value.toUpperCase()) {
        show &= false;
      } else {
        // return true;
      }
    }
  }
  return show;
}

window.optionSkipTrigger = function(field, option, values, resetValueItems, rootValues) {
  if (!field.trigger) {
    return false;
  }
  var triggers = field.trigger;

  if (!(triggers instanceof Array)) {
    triggers = [triggers];
  }

  for (var t in triggers) {
    let trigger = triggers[t];
    if (!trigger) continue;

    if (trigger.type == 'optionCondition') {
      if (option.condition) {
        if (resetValueItems) {
          if (resetValueItems[trigger.id] && resetValueItems[trigger.id] instanceof Array) {
          } else {
            resetValueItems[trigger.id] = [];
          }
          resetValueItems[trigger.id].push(field.id);
        }

        if (trigger.id && values && option.condition != values[trigger.id]) {
          return true
        } else
        if (trigger.value && option.condition != trigger.value) {
          return true
        }
      }
    } else if (trigger.type == 'optionExistsInReference') {
      var refs = getValueFmRootByRef(rootValues, trigger.id || field.reference);
      if (option.value == '*' || option.value == 'na' || (refs && refs.indexOf(option.value) >= 0)) {
        return false;
      } else {
        return true;
      }
    }
  }
  return false;
}

window.disableTrigger = function(field, template, values, changedValues, rootValues) {

  if (!field.trigger) {
    return false;
  }
  var triggers = field.trigger;

  if (!(triggers instanceof Array)) {
    triggers = [triggers];
  }

  var result = false;

  for (var t in triggers) {
    let trigger = triggers[t];
    if (!trigger) continue;

    let _value = getValueFmRootByRef(changedValues, trigger.id);
    if (!_value && rootValues) _value = getValueFmRootByRef(rootValues, trigger.id);

    if (trigger.type == 'disableIfFailValidate' && template) {
      result |= !checkChanges(values, changedValues) || !!validate(template, changedValues);
    } else if (trigger.type == 'disableIfNotEqual') {
      if (trigger.id && trigger.value) {
        result |= _value != trigger.value;
      }
    } else if (trigger.type == 'disableIfEqual') {
      if (trigger.id && trigger.value) {
        result |= _value == trigger.value;
      }
    }
  }
  return result;
}
