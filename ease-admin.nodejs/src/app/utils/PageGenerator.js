var ReactDOM = require('react-dom');
let temDate = require('../actions/TemData');
let DynContent = require('../components/DynamicUI/DynContent.jsx');

// for page redirect by path
var redirect = function(path) {
	console.trace('path redirect:', path);

	if (path == '/Home') {
    let Homepage = require('../components/Homepage.jsx');
    ReactDOM.render(
			<Homepage />,
			document.getElementById('content')
		);
	} else if (path == '/Login/Terms') {
    let Terms = require('../components/Terms.jsx');
    ReactDOM.render(
			<Terms />,
			document.getElementById('content')
		);
	} else if (path == '/Login') {
		window.location.reload();
  } else if (path == '/Error') {
    let Error = require('../components/Error.jsx');
    ReactDOM.render(
			<Error />,
			document.getElementById('content')
		);
//   } else if (path == '/ForgotPassword') {
//         let ForgotPassword = require('../components/ForgotPassword.jsx');
//         ReactDOM.render(
//             <ForgotPassword />,
//             document.getElementById('content')
//         );
  }
}
window.redirect = redirect;

// for page generation by page id
var getPageDom = function(page) {
	//console.trace('getPageDom:', page);

	switch (page.id) {
		case '/UserRoles':
			let UserRoles = require('../components/Settings/AdminSettings/UserRoles.jsx');
			// console.trace('getPageDom success:', page, UserRoles);
		 	return <UserRoles/>
		case '/UserRoles/Details':
			let UserRoleDetails = require('../components/Settings/AdminSettings/UserRoleDetails.jsx');
			// console.trace('getPageDom success:', page, UserRoleDetails);
		 	return <UserRoleDetails/>
		case '/Users':
			let Users = require('../components/Settings/AdminSettings/Users.jsx');
			// console.trace('getPageDom success:', page, UserRoles);
		 	return <Users/>
		case '/Users/Details':
			let UserDetails = require('../components/Settings/AdminSettings/UserDetails.jsx');
			// console.trace('getPageDom success:', page, UserRoleDetails);
		 	return <UserDetails/>
		case '/MySettings':
			let Settings = require('../components/Settings/MySettings.jsx');
			// console.trace('getPageDom success:', page, Settings);
		 	return <Settings/>
		case '/Report':
			let Report = require('../components/Report/Report.jsx');
			// console.trace('getPageDom success:', page, Settings);
		 	return <Report/>
		case '/DownloadMaterial':
			 let DownloadMaterial = require('../components/DownloadMaterial/DownloadMaterial.jsx');
			 // console.trace('getPageDom success:', page, Settings);
			  return <DownloadMaterial/>
		case '/BCProcessing':
			let BCProcessing = require('../components/BCProcessing/BCProcessing.jsx');
			// console.trace('getPageDom success:', page, Settings);
			return <BCProcessing/>
	  	case '/BCProcessing/Detail':
			let BCPDetail = require('../components/BCProcessing/BCPDetail.jsx');
			return <BCPDetail />
		case '/BCProcessing/DetailError':
			let BCPDetailError = require('../components/BCProcessing/BCPDetailError.jsx');
			return <BCPDetailError />

		case '/WebSettings':
			// console.trace('getPageDom success:', page, Settings);
			return <DynContent />
		case '/Products':
			let Products = require('../components/Products/ProductList.jsx');
			// console.trace('getPageDom success:', page, Products);
			return <Products visible={true} />
		case '/Products/Detail':
			let ProductsDetail = require('../components/Products/index.jsx');
			// console.trace('getPageDom success:', page, ProductsDetail);
			return <ProductsDetail visible={true}/>
		case '/DynTemplate':
			let MasterTable = require('../components/DynTemplate/MasterTable.jsx');
			// console.trace('getPageDom success:', page, Products);
			return <MasterTable visible={true} />
		case '/DynTemplate/Detail':
			let DTDetails = require('../components/DynTemplate/Details.jsx');
			// console.trace('getPageDom success:', page, ProductsDetail);
			return <DTDetails visible={true}/>
		case '/DynTemplate/Add':
			let DTAdd = require('../components/DynTemplate/Details.jsx');
			// console.trace('getPageDom success:', page, ProductsDetail);
			return <DTAdd visible={true}/>
		case '/Tasks':
			let Tasks = require('../components/Tasks/Tasks.jsx');
			// console.trace('getPageDom success:', page, Tasks);
			return <Tasks visible={true} />
		case '/TaskRelease':
			let TasksRelease = require('../components/Tasks/TaskRelease.jsx');
			// console.trace('getPageDom success:', page, TasksRelease);
			return <TasksRelease visible={true} />
		case '/Releases':
			let Releases = require('../components/Releases/Releases.jsx');
			// console.trace('getPageDom success:', page, Releases);
			return <Releases visible={true} />
		case '/Releases/Detail':
			let ReleasesDetail = require('../components/Releases/ReleasesDetail.jsx');
			return <ReleasesDetail visible={true} />
		case '/Releases/Detail/Task':
			let ReleaseAddTask = require('../components/Releases/ReleaseAddTask.jsx');
			return <ReleaseAddTask visible={true} />
		case '/Dynamic':
			// console.trace('getPageDom success:', page, DynContent);
			return <DynContent />
		case '/About':
			// console.debug("->About");
			let About = require('../components/About.jsx');
			return <About />
		case '/Needs':
			let Needs = require('../components/Needs/Needs.jsx');
			return <Needs visible={true} />
		case '/Needs/Detail':
			let NeedsDetail = require('../components/Needs/NeedsDetail.jsx');
			return <NeedsDetail visible={true} />
		case 'TaskInfo':
			let TaskInfo = require('../components/Needs/NeedsTaskInfo.jsx');
			return <TaskInfo visible={true}/>
		case 'NeedsDetail':
			let NeedsDetailDetail = require('../components/Needs/NeedsDetailDetail.jsx');
			return <NeedsDetailDetail visible={true}/>
		case 'NeedsTab':
			let NeedsTab= require('../components/Needs/NeedsTab.jsx');
			return <NeedsTab/>
		case 'NeedsFinEval':
			let NeedsFinancialEvaluation = require('../components/Needs/NeedsFinancialEvaluation.jsx');
			return <NeedsFinancialEvaluation/>
		case 'NeedsSelection':
			let NeedsSelection = require('../components/Needs/NeedsSelection.jsx');
			return <NeedsSelection/>
		case 'NeedsPrior':
			let NeedsPrior = require('../components/Needs/NeedsPrioritization.jsx');
			return <NeedsPrior/>
		case 'NeedsConceptSelling':
			let NeedsConceptSelling = require('../components/Needs/NeedsConceptSelling.jsx');
			return <NeedsConceptSelling/>
		case 'NeedsGuide':
			let NeedsGuide = require('../components/Needs/NeedsGuide.jsx');
			return <NeedsGuide/>
		case 'NeedsAnalysis':
			//let NeedsDetail = require('../components/Needs/NeedsDetail.jsx');
			return <div>analysis</div>
		case 'NeedsRAQuest':
			let NeedsRAQuest = require('../components/Needs/NeedsRAQuest.jsx');
			return <NeedsRAQuest />
		case 'NeedsRATolerance':
			let NeedsRATolerance = require('../components/Needs/NeedsRATolerance.jsx');
			return <NeedsRATolerance/>
		case 'NeedsSummaryReport':
			//let NeedsDetail = require('../components/Needs/NeedsDetail.jsx');
			return <div>/Needs/Detail/SummaryReport</div>
		case '/BeneIllus':
			let BeneIllus = require('../components/BeneIllus/BeneIllus.jsx');
			return <BeneIllus visible={true}/>
		case '/BeneIllus/Detail':
			let BeneIllusDetail = require('../components/BeneIllus/BeneIllusDetail.jsx');
			return <BeneIllusDetail visible={true}/>
		case 'BiTaskInfo':
			let BiTaskInfo = require('../components/BeneIllus/BiTaskInfo.jsx');
			return <BiTaskInfo visible={true}/>
		case 'BiDetail':
			let BiDetail = require('../components/BeneIllus/BiDetail.jsx');
			return <BiDetail visible={true}/>
		case 'BiCssHeaderFooter':
			let BiCssHeaderFooter = require('../components/BeneIllus/BiCssHeaderFooter.jsx');
			return <BiCssHeaderFooter visible={true}/>
		case 'BiPages':
			let BiPages = require('../components/BeneIllus/BiPages.jsx');
			return <BiPages visible={true}/>
		case 'BiSign':
			let BiSign = require('../components/BeneIllus/BiSign.jsx');
			return <BiSign visible={true}/>
		case 'BiImage':
			let BiImage = require('../components/BeneIllus/BiImage.jsx');
			return <BiImage visible={true}/>
		default:
	}
}

window.getPageDom = getPageDom;
