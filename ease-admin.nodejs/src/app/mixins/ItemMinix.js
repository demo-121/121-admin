let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;

let _debug=false;
let _isMount = false;

module.exports = {
  mixins: [React.LinkedStateMixin, StylePropable],

  componentDidMount: function() {
		_isMount = true;
  },

  componentWillUnmount: function() {
		_isMount = false;
  },

  isDebug(){
    return _debug;
  },

  isMount(){
    return _isMount;
  }
}
