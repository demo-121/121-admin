let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
//let StyleResizable = mui.Mixins.StyleResizable;
let Typography = mui.Styles.Typography;
let Colors = mui.Styles.Colors;

var Events = mui.Utils.Events;//require('../utils/events');

const Orienatation = {
  PORTRAIT: 1,
  LANDSCAPE: 2,
};

module.exports = {
  _updateOrientation(){
    let width = window.innerWidth;
    let height = window.innerHeight;
    this.setState({deviceOrienatation: (width>height*1.3)?Orienatation.LANDSCAPE:Orienatation.PORTRAIT});
  },

  _updateWindowSize(){
    this.setState({windowWidth: window.innerWidth, windowHeight: window.innerHeight});
  },

  componentDidMount() {
    this._updateBase();
    this.bindResize();
  },

  componentWillUnmount() {
    this.unbindResize();
  },

  bindResize: function _bindResize() {
    Events.on(window, 'resize', this._updateBase);
  },
  unbindResize: function _unbindResize() {
    Events.off(window, 'resize', this._updateBase);
  },

  _updateBase() {
  //  this._updateDeviceSize();
    this._updateWindowSize();
    this._updateOrientation();
  },

  getInitialState() {
    return {
      deviceOrienatation: Orienatation.LANDSCAPE,
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
    };
  },

  isLandscape(){
    return (this.state.deviceOrienatation==Orienatation.LANDSCAPE)?true:false;
  },

  isMobile(){
    return (this.state.deviceSize==1)?true:false;
  },

};
