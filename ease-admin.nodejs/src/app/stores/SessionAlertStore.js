let AppDispatcher = require('../dispatcher/AppDispatcher.js');
let EventEmitter = require('events').EventEmitter;
let assign = require('object-assign');
let ConfigConstants = require('../constants/ConfigConstants');
let ActionTypes = ConfigConstants.ActionTypes;

var UPDATE_SNACKBAR = "UPDATE_SNACKBAR";

//Properties
var _before = ConfigConstants.SessionAlertBefore_Min;        // Alert before 5 minutes.
var _countdown = ConfigConstants.SessionExpiry_Min;    // Session Length is 30 minutes.
var _open = false;
var _timer = null;

var SessionAlertStore = assign({}, EventEmitter.prototype, {
    emitChange: function() {
        this.emit(UPDATE_SNACKBAR);
    },
    
    addChangeListener: function(callback) {
        this.on(UPDATE_SNACKBAR, callback);
    },
    
    removeChangeListener: function(callback) {
        this.removeListener(UPDATE_SNACKBAR, callback);
    },
    
    getCountdown: function() {
        return _countdown;
    },
    
    getMessage: function() {
        return getLocalizedText('SESSION.MSG.ALERT');
    },
    
    getButton: function() {
        return getLocalizedText('SESSION.BUTTON.EXTEND');
    },
    
    getBefore: function() {
        return _before;
    },
    
    reset: function() {
        _countdown = ConfigConstants.SessionExpiry_Min;
    },
    
    reduce: function(value) {
        _countdown = _countdown - value;
        return _countdown;
    },
    
    setTimer: function(obj) {
        _timer = obj;
    },
    
    getTimer: function() {
        return _timer;
    },
    
    setOpen: function(val) {
        _open = val;
    },
    
    getOpen: function() {
        return _open;
    },
    
});

SessionAlertStore.dispatchToken = AppDispatcher.register(function(action) {
    switch (action.actionType) {
        case ActionTypes.UPDATE_SNACKBAR:
            SessionAlertStore.setOpen(action.actionValue);
            SessionAlertStore.emitChange();
            break;
        default:
            // No Action
    }
});

module.exports = SessionAlertStore;
