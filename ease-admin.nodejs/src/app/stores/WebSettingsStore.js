var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
// var HomeStore = require('../stores/HomeStore.js')

var WEB_SETTINGS_CHANGE = 'WEB_SETTINGS_CHANGE';
var ActionTypes = ConfigConstants.ActionTypes;

var _list = [];

function _setSettingsList(list) {
  _list = list;
}

var WebSettingsStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(WEB_SETTINGS_CHANGE);
  },

  addChangeListener: function(callback) {
    this.on(WEB_SETTINGS_CHANGE, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(WEB_SETTINGS_CHANGE, callback);
  },

  getList() {
      return _list;
  },

});

WebSettingsStore.dispatchToken = AppDispatcher.register(function(action) {
    switch(action.actionType) {
        case ActionTypes.GET_WEB_SETTINGS_LIST:
            _setSettingsList(action.data.list);
            WebSettingsStore.emitChange();
            break;
        default:
	    // no action
    }
});

module.exports = WebSettingsStore;
