var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
var ActionTypes = ConfigConstants.ActionTypes;

var ProductActions = require('../actions/ProductActions.js');

// var HomeStore = require('../stores/HomeStore.js');
// var ProductMenuStore = require('../stores/ProductMenuStore.js');

var CURRENT_PAGE_CHANGE = 'ProductPageChange';

var _currentPage = '';

function _selectPage(currentPage) {
  _currentPage = currentPage;
}

var ProductStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(CURRENT_PAGE_CHANGE);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    // console.debug('home store add listener');
    this.on(CURRENT_PAGE_CHANGE, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CURRENT_PAGE_CHANGE, callback);
  },

  getCurrentPage() {
    // console.debug('getCurrentPage: ', _currentPage);
    return _currentPage;
  },

  setCurrentPage(page){
    _currentPage = page;
  },

  _convertValues(items, values, changedValues){
    if(changedValues==undefined && values!=undefined){
      return values;
    }
    for (let j in items){
      var item = items[j];
      if(item.id == undefined){
        changedValues = this._convertValues(item.items, values, changedValues);
      }else if(values[item.id] != undefined && values[item.id] != ''){
        if(item.items){
          changedValues[item.id] = this._convertValues(item.items, values[item.id], changedValues[item.id]);
        }else{
          if (values[item.id] instanceof Date) {
            changedValues[item.id] = new Date(values[item.id]).getTime();
          } else{
            changedValues[item.id] = values[item.id];
          }
        }
      }
    }
    return changedValues;
  },

  convertValues(templateItems, values, changedValues){
    var sections = new Array();
    for (let i in templateItems){
      var itemId = templateItems[i].id;
      if(changedValues && changedValues[itemId]){
        for (let j in templateItems[i].items){
          changedValues[itemId] = this._convertValues(templateItems[i].items, values, changedValues[itemId]);
        }
        if(itemId != 'ProdDetail' && itemId !='TaskInfo' && itemId != 'ProdBrochureNCover'){
          sections.push(itemId);
        }
      }
    }
    changedValues.id = values.id;
    changedValues.version = values.version;
    changedValues.selectPageId = this.getCurrentPage().id;
    changedValues.sections = sections;
    if(values.taskId && values.taskId instanceof Array)
      changedValues.taskId = values.taskId[0];
    return changedValues;
  }
});

ProductStore.dispatchToken = AppDispatcher.register(function(action) {
  // console.debug('productStore dispatching', action);

  switch(action.actionType) {
    case ActionTypes.CHANGE_PAGE:
        if(action.data && action.data.content && action.data.content.values && action.data.content.values.selectPage)
          _selectPage(action.data.content.values.selectPage);
			break;
    case ActionTypes.RESET_VALUES:
        if(action.data && action.data.content && action.data.content.values && action.data.content.values.selectPage){
            _selectPage(action.data.content.values.selectPage);
            ProductStore.emitChange();
        }
			break;
		default:
			// no action
	}
});

module.exports = ProductStore;
