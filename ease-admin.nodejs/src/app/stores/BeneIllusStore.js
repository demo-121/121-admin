var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
// var HomeStore = require('../stores/HomeStore.js')

var CURRENT_PAGE_CHANGE = 'BIToReleaseAssign';
var ActionTypes = ConfigConstants.ActionTypes;

var _currentPage = '';

var _template = [];
var _list = [];
var _fitler = [];

function _getBIList(list, template) {
  _list = list;
  _template = template;
}

function _selectPage(currentPage) {
  _currentPage = currentPage;
}

var BenefitIllustrationStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {

    this.emit(CURRENT_PAGE_CHANGE);
  },

  /**_selectPage
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    // console.debug('home store add listener');

    this.on(CURRENT_PAGE_CHANGE, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {

    this.removeListener(CURRENT_PAGE_CHANGE, callback);
  },

  getList() {
      return _list;
  },

  getTemplate() {
      return _template;
  },

  getCurrentPage() {
    return _currentPage;
  },

getTypeList(){
	let type_menu_item = [];
	type_menu_item.push({payload:"All", text:"All Type"});
	type_menu_item.push({payload:"FN.PRO.PRODS", text:"Product"});
	type_menu_item.push({payload:"illustrations", text:"Benefits illustrations"});
	type_menu_item.push({payload:"Funds", text:"Funds"});
	type_menu_item.push({payload:"Application", text:"Application forms"});
	type_menu_item.push({payload:"Campaigns", text:"Campaigns"});

	 return type_menu_item;
},
getStatusList(){
	let status_menu_item = [];
	status_menu_item.push({payload:"All", text:"All Statuses"});
	status_menu_item.push({payload:"U", text:"Unassigned"});
	status_menu_item.push({payload:"A", text:"Assigned"});
	status_menu_item.push({payload:"S", text:"Scheduled"});
	status_menu_item.push({payload:"R", text:"Released"});
	status_menu_item.push({payload:"P", text:"Suspended"});

	 return status_menu_item;
},
setFilter(intype , instatus ,insearch){
	_fitler = { type : intype , status : instatus , search : insearch};
},
getFilter(){
	return _fitler;
}
});


module.exports = BenefitIllustrationStore;
