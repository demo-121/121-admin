var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
var HomeStore = require('./HomeStore.js')
var ContentStore = require('./ContentStore.js')

var CHANGE_EVENT = 'AppBarChange';
var ActionTypes = ConfigConstants.ActionTypes;

var _currentActions = 0;
var _title = {};
var _actions = [];
var _values = {};
var _searching = false;

var _updateAppBar = function(title, actions) {
  _title = title;
  if(actions) {
    _actions = actions;
  }
  
};

var _initValues = function() {
  _currentActions = 0;
  _title = {};
  _actions = [];
  _values = {};
  _searching = false;
}

var _updateCurrentActions = function(currentAction) {
  if(_actions){
    if (!isNaN(currentAction) && _actions.length > currentAction) {
      _currentActions = currentAction;
    } else {
      _currentActions = _actions.length - 1;
    }
  }
};

var _updateValues = function(values) {
  _values = values;
};

var _updateSearching = function(flag) {
  _searching = flag;
};

var AppBarStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  getTitle: function() {
    return _title;
  },

  getActions: function() {
    if(_actions)
      return _actions[_currentActions];
    else
      return {};
  },

  getAllActions: function() {
      return _actions;
  },

  getValues() {
    return _values;
  },

  getSearching() {
    return _searching;
  }
});

AppBarStore.dispatchToken = AppDispatcher.register(function(action) {
  console.debug('[AppBarStore] - [Dispatching] - [' + action.actionType + ']', action);
  // AppDispatcher.waitFor([HomeStore.dispatchToken]);

  switch (action.actionType) {
    case ActionTypes.CONFIG_LOGOUT: 
      _initValues();
      break;
    case ActionTypes.PAGE_REDIRECT:
    case ActionTypes.CHANGE_PAGE:
      // console.debug('[AppBarStore] - [Dispatching] - [' + action.actionType + ']', action);
      AppDispatcher.waitFor([HomeStore.dispatchToken, ContentStore.dispatchToken]);

      _updateSearching(false);
      if (action.data && action.data.appBar) {
        _updateAppBar(action.data.appBar.title, action.data.appBar.actions);
      }
      if (checkExist(action, "data.appBar.values")) {
          _updateValues(action.data.appBar.values);
      }else{
        _updateValues({});
      }

      _updateCurrentActions(0);
      AppBarStore.emitChange();
      break;
    case ActionTypes.CHANGE_CONTENT:
    case ActionTypes.CHANGE_SECTION:
    case ActionTypes.PRODUCT_LIST:
      // console.debug('[AppBarStore] - [Dispatching] - [' + action.actionType + ']', action);
      AppDispatcher.waitFor([HomeStore.dispatchToken, ContentStore.dispatchToken]);
      if (checkExist(action, "data.appBar")) {
        _updateAppBar(action.data.appBar.title, action.data.appBar.actions);
        if (checkExist(action, "data.appBar.values")) {
          _updateValues(action.data.appBar.values);
        } else {
          _updateValues({});
        }
        _updateCurrentActions(0);
        AppBarStore.emitChange();
      }
      break;
    case ActionTypes.CHANGE_VALUES:
    case ActionTypes.SET_ACTION:
    case ActionTypes.MasterTable.SELECTION_CHANGE:
      // console.debug('[AppBarStore] - [Dispatching] - [' + action.actionType + ']', action);
      AppDispatcher.waitFor([HomeStore.dispatchToken, ContentStore.dispatchToken]);
      if (action.actionIndex != undefined) {
        _updateCurrentActions(action.actionIndex);
        AppBarStore.emitChange();
      }
      break;
    case ActionTypes.AppBar.UPDATE_VALUES: // for update app bar values only
      // console.debug('[AppBarStore] - [Dispatching] - [' + action.actionType + ']', action.values);

    _updateValues(action.values);
      AppBarStore.emitChange();
      break;
    case ActionTypes.TASK_APPBAR_CHANGE:
      // console.debug('[AppBarStore] - [Dispatching] - [' + action.actionType + ']', action);
      if (action.action != undefined) {
        _updateCurrentActions(action.action - 1);
      } else {
        _updateCurrentActions(0);
      }
      AppBarStore.emitChange();
      break;
    case ActionTypes.AppBar.SHOW_SEARCH_BAR:
      _updateSearching(action.value)
      AppBarStore.emitChange();
      break;
    default:
      // no action
  }
});

module.exports = AppBarStore;
