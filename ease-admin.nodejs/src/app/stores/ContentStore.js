var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
var ProductStore = require('./ProductStore.js');

var CURRENT_PAGE_CONTENT = 'PageContent';
var ActionTypes = ConfigConstants.ActionTypes;

var _content = {};

var _error_msg = {
  title: null,
  msg: null
};

var _bcpContent = {
  policyNumber: null,
  selectedType: null,
  submitDisable: true
};

var isBNC = false;

var _changedFieldId = null;

function _initValues() {
    _content = {};
    isBNC = false;
    _changedFieldId = null;
    _bcpContent = {
      policyNumber: null,
      selectedType: null,
      submitDisable: true
    };
    _error_msg = {
      title: null,
      msg: null
    };
}

function setPageContent(content) {
 if (content && _content && content.values && content.values.isMore && _content.values && _content.values.list instanceof Array) {
      _content.values.list.push.apply(_content.values.list, content.values.list);
      //for refresh publish status in release details
      let detail = (  content.values && content.values.detail ) ? content.values.detail :"";
      let _detail = (  _content.values && _content.values.detail ) ? _content.values.detail :"";
      if(  detail.isPublishing  &&  _detail.isPublishing ) {
          detail.isPublishing = _detail.isPublishing;
      }
      if(  detail.isFailed  &&  _detail.isFailed ) {
          detail.isFailed = _detail.isFailed;
      }
  } else {
    _content = content;
  }
}

function _updateChangedValues(newChangedValues){
  // _content.changedValues = cloneObject(newChangedValues);
  //setPageContent(_content);
  _content.changedValues = newChangedValues;
}

function _update_error_msg(values) {
  if(_content && _content.changedValues && _content.changedValues.result) {
    if(values == null) {
      _content.changedValues.result = null;
    }
    else {
      _content.changedValues.result = {
        title: (values.title) ? values.title : null,
        msg: (values.msg) ? values.msg : null
      }
    }
  }
} 
   
function bcpinit() {
  _bcpContent = {
    policyNumber: null,
    selectedType: null,
    submitDisable: true
  };
}


var ContentStore = assign({}, EventEmitter.prototype, {
  emitChange: function() {
    this.emit(CURRENT_PAGE_CONTENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    // console.debug('home store add listener');
    this.on(CURRENT_PAGE_CONTENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CURRENT_PAGE_CONTENT, callback);
  },

  getDefualtChangedValues(){
    return _clonedChangedValues;
  },

  getPageContent() {
    return _content;
  },

  getBcpContent() {
    return _bcpContent;
  },

  getChangedValues() {
    return _content.changedValues;
  },

  setChangeValues(changedValues) {
      _content.changedValues = cloneObject(changedValues);
  },

  getChangedFieldId() {
    return _changedFieldId;
  },

  resetChangeFieldId(value) {
    if (value) {
      _changedFieldId = value;
    } else {
      _changedFieldId = null;
    }
  },

  getErrorMsg() {
    return _error_msg;
  }
});

ContentStore.dispatchToken = AppDispatcher.register(function(action) {
  console.debug("[ContentStore] - [Dispatching] - [" + action.actionType + "]", action);
  let page=action.page;
  switch (action.actionType) {
    case ActionTypes.CONFIG_LOGOUT: 
      _initValues();
      break;
    case ActionTypes.PAGE_REDIRECT:
    case ActionTypes.CHANGE_PAGE:
    if(action.page) {
      if(action.page.id != "/BCProcessing" && action.page.id != "/BCProcessing/Detail" && action.page.id != "/BCProcessing/DetailError" ) {
        bcpinit();
      } else {
        _bcpContent.submitDisable = true;
        _bcpContent.selectedType = null;
      }
    }
      
      // always disable submit button at BCProcessing page
      // _bcpContent.submitDisable = true;


      // console.debug("[ContentStore] - changecontent [Dispatching] - [" + action.actionType + "]", action);
      if (action.data) {
        var content = action.data.content;
        setPageContent(content);
      } else {
        setPageContent(null);
      }
      break;
    case ActionTypes.CHANGE_CONTENT:
      // console.debug("[ContentStore] - changecontent [Dispatching] - [" + action.actionType + "]", action);
      if (action.data) {
        setPageContent(action.data.content);
      } else {
        setPageContent(null);
      }
      ContentStore.emitChange();
      break;
    case ActionTypes.CHANGE_VALUES:
      if (action.newValues ) {
        _updateChangedValues(action.newValues);
        if (action.changedFieldId) {
          _changedFieldId = action.changedFieldId;
        }
        ContentStore.emitChange();
      }
      break;
    case ActionTypes.AssignPage.ASSIGN_PAGE:
      break;
    case ActionTypes.RESET_VALUES:
      if(checkExist(action, 'data.content.values')){
        var newValues = action.data.content.values;
        var type = newValues.actionType;
        if(type == 'merge'){
          mergeObj(_content.changedValues, newValues);
          mergeObj(_content.values, _content.changedValues);
        }else if(type == 'change'){
          _content.values = cloneObject(newValues);
          _content.changedValues = cloneObject(newValues);
        }

        if(checkExist(action, 'data.content.template')){
          var newTemplate = action.data.content.template;
          if(type == 'merge' && checkExist(_content, 'template')){
            for(var i in newTemplate.items){
              var item = newTemplate.items[i];
              for(var j in _content.template.items){
                if(_content.template.items[j].id == item.id){
                  _content.template.items[j] = cloneObject(item);
                  break;
                }
              }
            }
          }else if(type == 'change'){
            _content.template = newTemplate;
          }

        }

        ContentStore.emitChange();
      }


      break;
    case ActionTypes.NEEDS_CHANGE_PAGE:
      let content = action.data.content;
      if(content && !isEmpty(content.values)){
        _content.values = cloneObject(content.values);
        _content.changedValues = cloneObject(content.values);
        _content.valuesBackup = cloneObject(content.values);
      }
      if(content && !isEmpty(content.template)){
        if(content.template.menu){
          _content.template.menu = cloneObject(content.template.menu);
        }
      }
      ContentStore.emitChange();
      break;
    case ActionTypes.MasterTable.UPDATE_VALUES:
      if(action.values.criteria != undefined) {
        _bcpContent.policyNumber = action.values.criteria;
      }
      ContentStore.emitChange();
      break;
    case ActionTypes.MasterTable.SELECTION_CHANGE:
      if(action.rows == undefined) {
        _bcpContent.selectedType = null;
        // _bcpContent.submitDisable = true;
      }
      else if(action.rows && !isEmpty(action.rows)) {
        _bcpContent.selectedType = action.rows;
        // _bcpContent.submitDisable = false;
      }
      ContentStore.emitChange();
      break;

    case ActionTypes.CHANGE_ERROR_MSG:
      _update_error_msg(action.values);
      ContentStore.emitChange();

    default:
      break;
      // no action
  }
});

module.exports = ContentStore;
