var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
var ActionTypes = ConfigConstants.ActionTypes;

var UPDATE_FUNC_EDITOR_EVENT = 'UPDATE_FUNC_EDITOR_EVENT';

var data = {};

var updateData = function(newData) {
  data = newData;
};

var FuncEditorStore = assign({}, EventEmitter.prototype, {

  getData: function() {
    return data;
  },

  emitChange: function() {
    this.emit(UPDATE_FUNC_EDITOR_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(UPDATE_FUNC_EDITOR_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(UPDATE_FUNC_EDITOR_EVENT, callback);
  }
});

FuncEditorStore.dispatchToken = AppDispatcher.register(function(action) {
  switch (action.actionType) {
    case ActionTypes.UPDATE_FUNC_EDITOR_DATA:
      updateData(action.data);
      FuncEditorStore.emitChange();
      break;
    default:
  }
});

module.exports = FuncEditorStore;
