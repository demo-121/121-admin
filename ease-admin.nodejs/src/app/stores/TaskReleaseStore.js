var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
// var HomeStore = require('../stores/HomeStore.js')
//var GET_RELEASE_LIST = 'TASK_RELEASE';

var ActionTypes = ConfigConstants.ActionTypes;

var _template = [];
var _list = [];
var _fitler = [];

function _getUassignedReleaseList(list, template) {
  _list = list;
  _template = template;
}

var TaskReleaseStore = assign({}, EventEmitter.prototype, {
  emitChange: function() {
    //this.emit(GET_UNASSIGNED_RELEASE_LIST);
  },

  /**_selectPage
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    // console.debug('home store add listener');
    //this.on(GET_UNASSIGNED_RELEASE_LIST, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    //this.removeListener(GET_UNASSIGNED_RELEASE_LIST, callback);
  },

  getList() {
      return _list;
  },

  getTemplate() {
      return _template;
  },

  setFilter(insearch){
	   _fitler = {search : insearch};
   },

   getFilter(){
	    return _fitler;
   }
});

TaskReleaseStore.dispatchToken = AppDispatcher.register(function(action) {
  switch(action.actionType) {
		case ActionTypes.CHANGE_PAGE:
      // AppDispatcher.waitFor([HomeStore.dispatchToken]);
      if(action.page && action.page.id == "/Tasks/Release"){
        _getUassignedReleaseList(action.data.list, action.data.template);
        // TaskReleaseStore.emitChange();
      }
		break;
		default:
			// no action
	}
});

module.exports = TaskReleaseStore;
