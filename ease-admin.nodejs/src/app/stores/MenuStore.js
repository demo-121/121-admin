var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
var ActionTypes = ConfigConstants.ActionTypes;

var MENU_STATE = 'Open';

var _menuState = true;
var _mainMenu = [];

function _initValues() {
  _mainMenu = [];
  _menuState = true;
}


function _updateMenuState(mstate) {
  if (mstate == undefined) {
    _menuState = !_menuState;
  } else {
    _menuState = mstate;
  }
}

function _updateMainMenu(mainMenu) {
  _mainMenu = mainMenu;
}

var MenuStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(MENU_STATE);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(MENU_STATE, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(MENU_STATE, callback);
  },

  getMenuState() {
    return _menuState;
  },

  getMainMenu() {
    return _mainMenu;
  }
});

MenuStore.dispatchToken = AppDispatcher.register(function(action) {
  console.debug('[MenuStore] - [Dispatching] - [' + action.actionType + ']', action);
  switch(action.actionType) {
    case ActionTypes.CONFIG_LOGOUT: 
      _initValues();
      break;
    case ActionTypes.PAGE_REDIRECT:
      // console.debug('[MenuStore] - [Dispatching] - [' + action.actionType + ']', action);
      if (action.data) {
  		  _updateMainMenu(action.data.mainMenu);
        MenuStore.emitChange();  // called on Home
      }
			break;
    case ActionTypes.UPDATE_MENU_STATE:
      // console.debug('[MenuStore] - [Dispatching] - [' + action.actionType + ']', action);
      _updateMenuState(action.menuState);
      MenuStore.emitChange();
		default:
			// no action
	}
});

module.exports = MenuStore;
