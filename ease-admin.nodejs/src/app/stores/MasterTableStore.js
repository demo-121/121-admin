var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
var ActionTypes = ConfigConstants.ActionTypes;

var CHANGE_EVENT = 'MasterTableChange';

var _sortBy = '';
var _sortDir = 'A';
var _selectedRows = [];
var _selectedRowNumbers = 0;
var _pageSize = ConfigConstants.PageSize;

var _initValues = function() {
  _sortBy = '';
  _sortDir = 'A';
  _selectedRows = [];
  _selectedRowNumbers = 0;
  _pageSize = ConfigConstants.PageSize;
}

var _updateSortInfo = function(sortInfo) {
  _sortBy = sortInfo.sortBy;
  _sortDir = sortInfo.sortDir;
}

var _updateSelectedRows = function(rows) {
  _selectedRows = rows;
}

var _updateSelectedRowNumbers = function(rowNumbers) {
  _selectedRowNumbers = rowNumbers;
}

var MasterTableStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  getSortInfo() {
    return {
      sortBy: _sortBy,
      sortDir: _sortDir
    }
  },

  getSelectedRows() {
    return _selectedRows;
  },

  getSelectedRowNumbers() {
    return _selectedRowNumbers;
  },

  getPageSize() {
    return _pageSize;
  },

  setPageSize(size) {
    _pageSize = size;
  },

});

MasterTableStore.dispatchToken = AppDispatcher.register(function(action) {
  let appBarVal = action.data && action.data.appBar && action.data.appBar.values? action.data.appBar.values:"";
  let pageVal = action.data && action.data.page && action.data.page.value? action.data.page.value:"";
  let contentValList =action.data && action.data &&  action.data.content&&action.data.content.values&&action.data.content.values.list?action.data.content.values.list:[];

  let rowNumbers=pageVal.reloadStatus? appBarVal.recordStart: appBarVal.recordStart + contentValList.length;
  switch (action.actionType) {
    case ActionTypes.CONFIG_LOGOUT: 
      _initValues();
      break;
    case ActionTypes.CHANGE_PAGE:
      // console.debug('[MasterTableStore] - [Dispatching] - [' + action.actionType + ']', action);
      if (action.data && action.data.appBar && action.data.appBar.values) {
        _updateSortInfo(action.data.appBar.values);
      } else {
        _updateSortInfo({
          sortBy: '',
          sortdir: 'A'
        });
      }
      _updateSelectedRows([]);
      if(appBarVal)
        _updateSelectedRowNumbers(rowNumbers)
      break;
    case ActionTypes.CHANGE_CONTENT:
      // console.debug('[MasterTableStore] - [Dispatching] - [' + action.actionType + ']', action);
      if (action.data && action.data.appBar && action.data.appBar.values) {
        _updateSortInfo(action.data.appBar.values);
        if(appBarVal)
          _updateSelectedRowNumbers(rowNumbers)

        if (action.data.appBar.values.useDefault) {
          _updateSelectedRows([]);
        }
      }
      // if (!(action.data && action.data.content && action.data.content.values && action.data.content.values.isMore)
      // && !(action.data && action.data.appBar && action.data.appBar.values && action.data.appBar.values.sorting)) {
      //   _updateSelectedRows([]);
      // }
      MasterTableStore.emitChange();
      break;
    case ActionTypes.MasterTable.SELECTION_CHANGE:
      // console.debug('[MasterTableStore] - [Dispatching] - [' + action.actionType + ']', action);
      if(appBarVal)
        _updateSelectedRowNumbers(rowNumbers)

      _updateSelectedRows(action.rows)
      MasterTableStore.emitChange();
      break;
    case ActionTypes.MasterTable.SORT_TARGET_CHANGE:
      // console.debug('[MasterTableStore] - [Dispatching] - [' + action.actionType + ']', action);
      _updateSortInfo(action);
      MasterTableStore.emitChange();
      break;
    case ActionTypes.TASK_ASSIGN:
    case ActionTypes.ADD_TASK:
    case ActionTypes.RELEASE_TASK_CHANGE:
    case ActionTypes.TASK_APPBAR_CHANGE:
      // console.debug('[MasterTableStore] - [Dispatching] - [' + action.actionType + ']', action);
      if(appBarVal)
        _updateSelectedRowNumbers(rowNumbers)
      _updateSelectedRows(action.rows)
      break;
    default:
      // no action
  }
});

module.exports = MasterTableStore;
