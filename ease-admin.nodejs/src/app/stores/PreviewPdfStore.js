var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');


// var HomeStore = require('../stores/HomeStore.js')

var CURRENT_PAGE_CHANGE = 'PreviewPage';
var ActionTypes = ConfigConstants.ActionTypes;

var _htmlString = null;

function _genPdf(html){
  _htmlString = html;
}

var PreviewPdfStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {

    this.emit(CURRENT_PAGE_CHANGE);
  },

  /**_selectPage
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    // console.debug('home store add listener');

    this.on(CURRENT_PAGE_CHANGE, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {

    this.removeListener(CURRENT_PAGE_CHANGE, callback);
  },

  getPreviewHtml(){
    return _htmlString;
  },

});

 PreviewPdfStore.dispatchToken = AppDispatcher.register(function(action) {
   switch(action.actionType) {
     case ActionTypes.CHANGE_PAGE:
     _genPdf(null);
     PreviewPdfStore.emitChange();
     break;
    case ActionTypes.PREVIEW_PDF:
      _genPdf(checkExist(action,'data.content.values.html')?action.data.content.values.html: null);
      PreviewPdfStore.emitChange();
      break;
 		default:
 			// no action
 	}
});

module.exports = PreviewPdfStore;
