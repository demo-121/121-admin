var React = require('react');
var mui = require('material-ui')

let Paper = mui.Paper;
let TextField = mui.TextField;
let RaisedButton = mui.RaisedButton;
let CaptchaImg = require('./CustomView/CaptchaImg.jsx');
var ConfigActions = require('../actions/ConfigActions.js');

var AppStore = require('../stores/AppStore.js');
var ContentStore = require('../stores/ContentStore.js');
let Loading = require('./CustomView/LoadingBlock.jsx');

import DropDownMenu from 'material-ui/lib/DropDownMenu';
import MenuItem from 'material-ui/lib/menus/menu-item';
import appTheme from '../theme/appBaseTheme.js';
let Typography = mui.Styles.Typography;
import Snackbar from 'material-ui/lib/snackbar';

function getCookie(w){
	var cName = "";
	var pCOOKIES = new Array();
	var pCOOKIES = document.cookie.split('; ');
	for (let bb = 0; bb < pCOOKIES.length; bb++){
		var NmeVal  = new Array();
		NmeVal  = pCOOKIES[bb].split('=');
		if(NmeVal[0] == w){
			cName = unescape(NmeVal[1]);
		}
	}
	return cName;
}

function callClear() {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", ROOT_CONTEXT_PATH + "/Clear", true);
    xhttp.send();
}

var Welcome = React.createClass({

  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: appTheme.getTheme(),
    };
  },

  componentDidMount: function() {
    AppStore.addChangeListener(this.onChange);
    if (getCookie("sessionexpiry")=="Y") {
			setTimeout(callClear, 1000)
    }
		this.onChange();
  },

  componentWillUnmount: function() {
    AppStore.removeChangeListener(this.onChange);
  },

	onChange: function() {
		if (!this.state.initing) {
      console.log('app initing');
			this.setState({
        initing: true
      })
			ConfigActions.init();
		} else {
			var path = AppStore.getPath();
      var saml = AppStore.getSamlPath();
      var loggedOn = AppStore.getLoggedOn();
      window.goSaml = false;
      if (path && path != '/' && path != '/Login') {
        console.log('app initing: redirect path:' + path);
        redirect(path);
      } else {
        if (saml) { 
          console.log('app initing: redirect saml login');
          window.goSaml = true;
          window.location = saml;
        } else {
          console.log('app initing: local login');
          let content = ContentStore.getPageContent();
          if (content) {
            if (content.failMessage) {
              this.setState({
                error: content.failMessage
              })
            } else {
              this.setState({
                content: content
              })
            }
          } else {
            ConfigActions.reloadLogin('en');
          }
        }
      }
		}
	},

	getInitialState: function() {
    let content = ContentStore.getPageContent();
		return {
			loginid: '',
      password: '',
      initing: false,
      path: 0,
      value:3,
      error: content?content.failMessage:'',
      content: content,
      snackbarOpen: false
		};
	},

  handleChange: function(e, index, value){
    //this.setState({value:value});
    this.setState({error:null});
    ConfigActions.reloadLogin(value);
  },

	render() {
    var self = this;
    var {
      content,
      initing
    } = this.state;
    
    if(this.state.content == null){
      content = ContentStore.getPageContent();
    }
    let theme=this.getChildContext().muiTheme;

    if (isEmpty(content) || !content.template) {
      if (initing) {
        return <div> Initializing ... </div>
      } else {
        return <div> Redirecting ... </div>
      }
    }

    var style = {
      width: '400px',
      textAlign: 'center'
    };

    let titleStyle= {
      fontSize: 25,
      paddingTop: '32px',
      paddingBottom: '20px',
    };

    let subTitleStyle= {
      fontSize: 20,
      paddingTop: '8px',
      color: Typography.textLightBlack,
    };

    let imageStyle= {
      margin: 'auto',
      width: '120px',
      height: 'auto',
      marginTop: '32px',
    };

    let textFieldStyle= {
      width:'100%',
			height:'80px'
    };

    let inputStyle= {
      margin: '0 auto',
      display: 'table',
    }

    let buttonStyle={
      margin:'25px'
    };

    let dropDownMenuStyle={
      position: 'absolute',
      top: 0,
      right: 0,
      zIndex:'1',
      fontSize:'125%'
    };

    let pCenterStyle={
      zIndex:'0',
      overflow: 'auto'
    };

    let hyperLinkStyle={
      fontSize:15,
      display: 'block',
      cursor: 'pointer'
    };

    let errorMessageStyle = {
      color: 'red',
      width: '100%',
      display: 'block'
    };

    var errorMessage = null
    if (this.state.error) {
      errorMessage = <span style={errorMessageStyle}>{this.state.error}</span>
    } else {
      errorMessage = <span style={errorMessageStyle}>&nbsp;</span>
    }

    if (content.template) {
      var langOptions = content.template.items[0].options;
      var options  = [];
      for (let i = 0; i<langOptions.length; i++){
        var option = langOptions[i];
        options.push(<MenuItem key={"mi"+i} value={option.value} primaryText={option.title}  />);
      }
      var values = content.values;
		  return (
        <div className="pCenter" style={pCenterStyle} >
          <DropDownMenu key="langPicker" value={values['lang']} onChange={this.handleChange} style={dropDownMenuStyle} >
            {options}
          </DropDownMenu>
          
    			<Paper key="loginPaper" zDepth={0} style={style}>
            <div style={titleStyle}>
                    {values['WELCOME_BACK']}
            </div>
            <div style={subTitleStyle}>
              {values['SIGN_IN_ADMIN']}
            </div>
            <img style={imageStyle} src= {'web/img/login_icon.png'}/>
            <div style={inputStyle}>
              <TextField
                label={values['USER.NAME']}
                key="username"
                ref="username"
                hintText={values['INPUT.HINT.USER.NAME']}
                onEnterKeyDown = { this.handleLogin }
                onChange = { this.loginIdChange }
                value= {this.state.loginid }
                floatingLabelText={values['USER.NAME']}
								errorStyle = {{lineHeight:'16px'}}
								floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
								underlineStyle = { {bottom:'22px'} }
                style={textFieldStyle}/>
							<div style={{display:'none'}} className="hidden">
					        <input type="password"/>
					      </div>
              <TextField
                label={values['PASSWORD']}
                key="password"
                ref="password"
                type={this.state.password==""?"text":"password"}
                floatingLabelText={values['PASSWORD']}
								errorStyle = {{lineHeight:'16px'}}
								floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
								underlineStyle = { {bottom:'22px'} }
                onEnterKeyDown = { this.handleLogin }
                onChange = { this.passwordChange }
                value= { this.state.password }
                style={textFieldStyle}/>
            </div>
            {errorMessage}
            <RaisedButton
              key="loginButton"
              label={values['SIGN_IN']}
              style={buttonStyle}
              primary={true}
              onTouchTap={self.handleLogin}  />
          </Paper>
         
          <Loading/>
          <Snackbar open={this.state.snackbarOpen} message={values['INPUT.ERROR.SESSION_EXPIRY']} autoHideDuration={4000} onRequestClose={self.handleSnackbarRequestClose} />
        </div>
  		);
    }
	},

  loginIdChange(e) {
    this.setState({ loginid: e.target.value, error: ""});
  },

  passwordChange(e) {
    this.setState({ password: e.target.value, error: ""});
  },

  handleLogin(e) {
    var {
      loginid,
      password
    } = this.state;
    this.setState({loginid: "", password: ""});
    this.refs.username.focus();
    var currentLang = this.state.content.values['lang'];
    ConfigActions.login(loginid, password, currentLang);
  },

  isEmpty(obj) {
    for (let prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }
    return true;
  },

	handleSnackbarRequestOpen() {
		this.setState({
			snackbarOpen: true,
		});
	},

	handleSnackbarRequestClose() {
		this.setState({
			snackbarOpen: false,
		});
	}
});

module.exports = Welcome;
