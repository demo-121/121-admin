/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let CurrentColumn = require('../DynamicUI/DynEditableColumn.jsx');

let FlatButton = mui.FlatButton;
let sectionId = 'TaskInfo';

let NeedsTaskInfo = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  componentDidMount() {
    // console.debug('homepage did mount');
    ContentStore.addChangeListener(this.onContentChange);
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onContentChange);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    if(this.state.taskStatusCode != nextState.taskStatusCode){
      return true;
    }
    //if task version changedValues
    if(this.state.id != nextState.id || this.state.version != nextState.version){
      return true;
    }

    return false;
  },

  onPageChange(){
    let section = this.getSection(this.state.template);
    this.setState({section: section});
  },


  onContentChange() {
    var {
      template,
      values,
      changedValues
    } = ContentStore.getPageContent();

    var refresh = changedValues?true:false;
    if(!changedValues){
      changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);
    }

    var taskStatusCode = checkExist(changedValues,sectionId)? changedValues[sectionId]['statusCode']:'';

    this.setState({
      id: values.needCode?values.needCode:"",
      version: values.version?values.version:1,
      template:template,
      values: values,
      changedValues: changedValues,
      taskStatusCode: taskStatusCode,
      section: this.getSection(template)
    })
    if(refresh){
      this.forceUpdate();
    }
  },

  getInitialState() {
    var {
      template,
      values
    } = ContentStore.getPageContent();

    let section = this.getSection(template);

    //initial values
    if(values && sectionId && !values[sectionId]){
      values[sectionId]={};
    }
    var currPage = NeedDetailStore.getCurrentPage();
    var changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);

    var taskStatusCode = checkExist(changedValues, sectionId+'.statusCode')? changedValues[sectionId]['statusCode']:'';

    ContentStore.getPageContent().valuesBackup = values;

    return {
      id: values.needCode?values.needCode:"",
      version: values.version?values.version:1,
      currentPage: currPage,
      template: template,
      values: values,
      changedValues: changedValues,
      taskStatusCode: taskStatusCode,
      section: section
    };
  },

  getSection(template){
    var templateItems = template.items;
    for (let i in templateItems) {
      var section = templateItems[i];
      if (section.id == sectionId) {
        return section;
      }
    }
    return template.items[0];
  },

  render: function() {
    var self = this;
    var {
      section,
      values,
      changedValues
    } = this.state;

    let dataContainerStyle = {
      width:'100%',
      height:"calc(100vh - 58px)",
      overflowY:"auto",
    };

    return (
      <CurrentColumn
        style={dataContainerStyle}
        show = {true}
        template = {section}
        values = { values[sectionId] }
        changedValues = { changedValues[sectionId] }
        id = "current"
        ref = "current" />
    );
  }
});

module.exports = NeedsTaskInfo;
