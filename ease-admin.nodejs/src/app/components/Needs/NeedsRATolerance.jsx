let React = require('react');
let mui = require('material-ui');
let NeedPageMinix = require('./NeedPageMinix.js');
let ItemMinix = require('../../mixins/ItemMinix.js');

let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');

let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');
let DialogActions = require('../../actions/DialogActions.js');

let SortableList = require('../CustomView/SortableList.jsx');
let NeedsRAToleranceItem = require('./NeedsRAToleranceItem.jsx');
let RaisedButton = mui.RaisedButton;
let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let Divider = mui.Divider;
let Transitions = mui.Styles.Transitions;

//let sectionId = 'NeedsRATolerance';

let NeedsRATolerance = React.createClass({
	mixins: [NeedPageMinix],

	onUpdate(){
    this.forceUpdate();
  },

	getInitialState() {
		var content = ContentStore.getPageContent();
		var sectionId = NeedDetailStore.getCurrentPage();
		var {
			values,
			changedValues,
			template
		} = content;

		if(!values[sectionId]){
			values[sectionId]={
				titleText:[]
			};
		}else if(!values[sectionId].titleText){
			values[sectionId].titleText = [];
		}

		if(!isEmpty(values) && isEmpty(changedValues)){
			changedValues = content.changedValues = cloneObject(values);
		}

		if(!changedValues[sectionId]){
			changedValues[sectionId]={
				titleText:[]
			};
		}else if(!changedValues[sectionId].titleText){
			changedValues[sectionId].titleText = [];
		}

		let validation = function(changedValues){
			let errorMsg = false;
			if(changedValues && changedValues.titleText){
				for(let i in changedValues.titleText){
					let _text = changedValues.titleText[i];
					if(isEmpty(_text.riskTxt.en)){
            errorMsg = "ristText is empty";
            break;
          }
          if(isEmpty(_text.scoreFrom) || isEmpty(_text.scoreTo)){
            errorMsg = "score to and score form should not empty";
          }
          if(_text.scoreFrom && _text.scoreTo && _text.scoreTo < _text.scoreFrom){
            errorMsg = "score to should greater or equal to score from";
          }
				}
			}
			return errorMsg;
		};

		if(!template.items){
			template.items = [];
		}

		let foundTemplate = false
		for(let j in template.items){
			if(template.items[j].id == sectionId){
				template.items[j].validate = validation;
				let foundTemplate = true;
			}
		}

		if(!foundTemplate){
			template.items.push({id:sectionId, validate:validation});
		}

		return {
			sectionId: sectionId,
			isLock: NeedDetailStore.isLock(),
			values: values[sectionId],
			changedValues: changedValues[sectionId],
			validation: validation
		}
	},

	getStyles(){
		return(
				{
					contentStyle:{
							transition: Transitions.easeOut(),
							width:'100%',
							height: 'calc(100vh - 58px)',
							position: 'relative',
							overflowY: 'hidden'
					},
					headerStyle:{
						width:'100%',
						height:'58px',
						lineHeight:'58px',
						paddingLeft:'24px',
						paddingRight:'24px'
					},
					footerStyle:{
						width:'100%',
						height:'58px',
						lineHeight:'58px',
						paddingLeft:'24px',
						paddingRight:'24px'
					},
					listStyle:{
					  minHeight: 'calc(100vh - 232px)',
						paddingLeft: '24px',
						paddingRight: '24px'
					},
					placeholderStyle:{
						fontStyle: 'italic',
						color: '#AAAAAA'
					}
				}
		)

	},

	openCFPanel(){
    let self = this;
		let {
			changedValues
		} = this.state;

		let {
			titleText,
			minCode,
			maxCode
		} = changedValues;

		let index = changedValues.titleText.length;

		let seq = (index+1) * 100;

		let newRisk = {
			id: this.getNextId(titleText),
			label: 'Risk Level',
			riskTxt: NeedDetailStore.genLangCodesObj(),
      desc: NeedDetailStore.genLangCodesObj(),
      scoreFrom: index==0?0:titleText[index-1].scoreTo?Number(titleText[index-1].scoreTo)+1:'',
      scoreTo:'',
			isMandatory: false,
			seq: seq
		};


		let onItemUpdate = function(){
			self.refs["nrat-"+newRisk.id].onUpdate();
		};

		let validation = function(){
			let errorMsg = !isEmpty(newRisk.riskTxt.en)?false:"Some mandatory data do not input yet, if you click OK button, this question would be deleted.";

			if(!errorMsg){
			}else{
				let removeDialog = {
					id: "delete",
					title: getLocalizedText("DELETE","DELETE"),
					message: errorMsg,
					negative: {
						id: "delete_cancel",
						title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
						type: "button"
					},
					positive:{
						id: "delete_confirm",
						title: getLocalizedText("BUTTON.OK","OK"),
						type: "customButton",
						handleTap: function(){
							titleText.splice(titleText.length-1, 1);
							self.onUpdate();
							DynConfigPanelActions.closePanel(true);
						}
					}
				}

				DialogActions.showDialog(removeDialog);
			}

			return errorMsg;
		}

		titleText.push(newRisk);
		self.onUpdate();
		DynConfigPanelActions.openPanel(titleText[titleText.length-1].riskTxt, 'question', {onUpdate: onItemUpdate, validation: validation});
	},

	getNextId(titleText){
		let nextId = "r1";
		//get the latest id
		for(let i in titleText){
			let _id = titleText[i].id;
			if(_id){
				_id = _id.substring(1, _id.length);
				if(isNumeric(_id) && Number(_id) >= Number(nextId.substring(1, nextId.length)))
					nextId= "r"+(Number(_id)+1);
			}
		}
		return nextId;
	},

	genItems(titleText){
		let {
			sectionId
		} = this.state;
		let its = [];
		let self = this;
		for(let i in titleText){
			let risk = titleText[i];
			its.push(
				<NeedsRAToleranceItem
					key={"nrat-"+risk.id}
					ref={"nrat-"+risk.id}
					index={Number(i)}
					maxIndex={titleText.length-1}
					changedValues={titleText}
					onListUpdate={self.onUpdate}
					sectionId={sectionId}/>
			);
		}
		return its;
	},



	render: function() {
		if(this.isDebug()){
			console.log("render-NRAT");
		}

		var self = this;
		let styles = this.getStyles();

    let {
      values,
      changedValues,
			cfValues,
			cfCValues,
			deleteAction,
			isLock
    } = this.state;
		let items = this.genItems(changedValues.titleText);

		let header = (
			<div key={'nrat-header'} style={{height: '58px', display: 'block', overflow:'hidden'}}>
        <Toolbar key="nrat-hToolbar" style={{padding:"0px", position:'relative', background: '#FFFFFF'}} >
          <ToolbarGroup key={'nrat-hToolbar-left'} style={{display:'flex'}}>
            <ToolbarTitle text={getLocalizedText("MENU.RISK_ASSESS.RISK_TOLERANCE","Risk Tolerance")} style={{color:'#000000', paddingLeft: '24px'}}/>
          </ToolbarGroup>
          <ToolbarGroup key={'nrat-hToolbar-right'}  float="right" style={{display:'flex'}}>
						{isLock?null:
							<RaisedButton
				        key={'nrat-btn-add-question'}
				        style={{display:'inline-block', float: 'right', marginRight: '24px'}}
				        onClick={this.openCFPanel}
				        label={getLocalizedText("BUTTON.ADD_LEVEL","ADD LEVEL")}
				      />
						}

          </ToolbarGroup>
        </Toolbar>
				<hr/>
      </div>
		);


		return (
			<div key={"nrat-content"} style={styles.contentStyle}>
				<div style={{height:'calc(100vh - 58px)', overflowY: 'hidden'}}>
					{header}
					<Divider/>
					<div style={{height: 'calc(100vh - 116px)', overflowY: 'auto'}}>
						<SortableList
							id="nratList"
							items={items}
							style={styles.listStyle}
							values={values.titleText}
							changedValues={changedValues.titleText}
							handleSort={this.onUpdate}/>
					</div>
				</div>

			</div>
		);
	},
});

module.exports = NeedsRATolerance;
