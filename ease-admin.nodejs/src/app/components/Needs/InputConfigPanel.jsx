let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let Transitions = mui.Styles.Transitions;
let Tab = mui.Tab;
let Tabs = mui.Tabs;
let IconButton = mui.IconButton;
let RaisedButton = mui.RaisedButton;
let FontIcon = mui.FontIcon;
let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let SelectField = require('../CustomView/SelectField.jsx');

let ConfigPanel = require('../CustomView/ConfigPanel.jsx');
let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let EABTextField = require('../CustomView/TextField.jsx');
let EABPickerField = require('../CustomView/PickerField.jsx');
let EABViewOnlyField = require('../CustomView/ViewOnlyField.jsx');
let SwitchField = require('../CustomView/SwitchField.jsx');
let CheckboxGroup = require('../CustomView/CheckBoxGroup.jsx');

let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');
let AppBarActions = require('../../actions/AppBarActions.js');
let DialogActions = require('../../actions/DialogActions.js');
let DynConfigPanelStore = require('../../stores/DynConfigPanelStore.js');
let InputTypeDialog = require('./InputTypeDialog.jsx');

import appTheme from '../../theme/appBaseTheme.js';
let debug = false;
let _isMount=false;

let Topbar = React.createClass({

  update(){
    if(_isMount)
      this.forceUpdate();
  },

  render(){
    if(debug){
      console.log('render-configpanel-topbar');
    }

    let self = this;
    let {
      values,
      changedValues,
      actions,
      isLock,
      id
    }= this.props;

    let {
      deleteAction
    } = actions;

    let title = null;
    let _title = (changedValues.label)? changedValues.label: "";
    if(typeof _title == "string"){
      title = <ToolbarTitle text={_title} style={{color:'#000000'}}/>
    }

    let deleteBtn = (deleteAction && !isLock)?(
      <RaisedButton
        key={'cp-btn-delete'}
        style={{display:'inline-block'}}
        onClick={deleteAction}
        labelColor="#FF0000"
        label={getLocalizedText("BUTTON.DELETE","Delete")}
      />
    ):null;

    let variableIdItem = {id:'id', title:'Variable ID', type:'TEXT'};
    let variableIdField = (
      <div style={{display: 'inline-block', width: deleteAction? 'calc(100% - 122px)': '100%'}}>
      <EABTextField
        ref = {variableIdItem.id}
        key = {"ctn_"+ variableIdItem.id}
        template = { variableIdItem }
        values = { values }
        changedValues = { changedValues }
        handleChange = {this.update}
        />
    </div>
    );

    return(
      <div key={id+'-topbar'} style={{height: '58px', display: 'block', overflow:'hidden'}}>
        <Toolbar ref="appToolBar" key="appToolBar" style={{padding:"0px", position:'relative', background: '#FFFFFF'}} >
          <ToolbarGroup key={id+'-topbar-left'} style={{width:'65%', display:'flex'}}>
            <span>
              <FontIcon
                style = {{padding: '0px 15px 0 24px', height:'58px', lineHeight: '58px', cursor:'pointer'}}
                color = {"#000000"}
                hoverColor={"#000000"}
                className="material-icons"
                onClick={function(){DynConfigPanelActions.closePanel();} } >
                close
              </FontIcon>
            </span>
            {title}
          </ToolbarGroup>
          <ToolbarGroup key={id+'-topbar-right'} style={{width:'35%', display:'flex'}}>
            {variableIdField}
            {deleteBtn}

          </ToolbarGroup>
        </Toolbar>
      </div>
    );
  }
});

let LeftPanel = React.createClass({

  update(){
    if(_isMount)
      this.forceUpdate();
  },

  _genLeftPanelTabs(tabValues,langCodes,changedValues, isLock){
    let {
      actions
    }= this.props;

    let {
      onUpdate
    } = actions;

    let titleItems=[];
    let descItems=[];
    let tabItems=[];
    for( var h=0; h<tabValues.length;h++){
      let tabValue=tabValues[h];
      let fieldItems=[];
      for(let i in langCodes){
        let langCode = langCodes[i];
        let item={id:langCode, title:getLocalizedText("LANG."+langCode.toUpperCase(),"LANG."+langCode.toUpperCase()), type:'TEXT', max:100,mandatory:langCode=="en"?true:false, disabled: isLock, multiLine: true};
        fieldItems.push(
          <EABTextField
            ref = {item.id}
            key = {"ctn_"+tabValue.key+"_"+ item.id}
            template = { item }
            changedValues  = { checkExist(changedValues, tabValue.key)?changedValues[tabValue.key]:changedValues }
            handleChange = {onUpdate}
            multiLine={true}
            />
        );
      }
      tabItems.push(
        <Tab label={tabValue.label} >
          {fieldItems}
        </Tab>
      )
    }
    return tabItems;
  },


  render(){
    if(debug){
      console.log('render-configpanel-leftpanel');
    }
    let {
      values,
      changedValues,
      actions,
      height,
      isLock,
      id
    }= this.props;

    let {
      onUpdate
    } = actions;

    let langCodes = NeedDetailStore.getLangCodes();
    let items = [];

    if(checkExist(changedValues, 'title') && checkExist(changedValues, 'desc')){
      //add tab
      let tabValues=[
        {label:getLocalizedText("CONFIG_PANEL.NEED_SELECT_TAB_NAME", "NAME"), key:"title"}
      , {label:getLocalizedText("CONFIG_PANEL.NEED_SELECT_TAB_DESC", "DESCRIPTION") , key:"desc"}
      ];
      let tabItems=this._genLeftPanelTabs(tabValues ,langCodes,changedValues, isLock );
      items.push(
       <Tabs>
        {tabItems}
       </Tabs>
      );

    }else{

      let _type = DynConfigPanelStore.getType();
      for(let i in langCodes){
        let langCode = langCodes[i];
        let item={id:langCode, title:getLocalizedText("LANG."+langCode.toUpperCase(),"LANG."+langCode.toUpperCase()), type:'TEXT', disabled: isLock, multiLine: true, mandatory: (i==0?true:false)};
        items.push(
          <EABTextField
            ref = {item.id}
            key = {"ctn_"+ item.id}
            template = { item }
            values = { (_type=='otherSelection' && checkExist(values, 'otherTitle'))?values.otherTitle:checkExist(values, 'title')?values.title:values }
            changedValues = { (_type=='otherSelection' && checkExist(changedValues, 'otherTitle'))?changedValues.otherTitle:checkExist(changedValues, 'title')?changedValues.title:changedValues }
            handleChange = {onUpdate}
            multiLine = {true}
            />
        )

      }
    }


    return(
      <div ref="leftPanel" style={{ display: 'inline-block', position: 'relative', width:'calc(65% - 1px)', height: (height-65)+'px', overflowY: 'auto'}}>
        {items}
      </div>
    );
  }
});

let RightPanel = React.createClass({
  update(){
    if(_isMount)
      this.forceUpdate();
  },

  render(){
    if(debug){
      console.log('render-configpanel-rightpanel');
    }
    let self = this;
    let {
      values,
      changedValues,
      actions,
      height,
      isLock,
      id
    }= this.props;

    let {
      onUpdate,
      section
    } = actions;

    let langCodes = NeedDetailStore.getLangCodes();
    let items = [];
    let inputType=changedValues.type;

    if(!isEmpty(section) || !isEmpty(inputType)){
      //if section is not set and input type exist, find section in NeedDetailStore
      if(isEmpty(section))
        section = cloneObject(NeedDetailStore.getInputTypeSections(inputType));
      if(checkExist(section, 'items')){
        for(let i in section.items){
          let item = section.items[i];
          if(isLock){
            item.disabled = isLock;
          }
          switch(item.type){
            case 'TEXT':
              items.push(
                <EABTextField
                  style={{width:'100%'}}
                  template = { item }
                  values = { values }
                  errorText = { typeof(item.validation)=='function'?item.validation(changedValues):undefined}
                  changedValues = { changedValues }
                  handleChange = { function(){self.update();onUpdate();} }
                  />
              );
              break;
            case 'PICKER':
              items.push(
                <EABPickerField
                  template = { item }
                  values = { values }
                  changedValues = { changedValues }
                  handleChange = { function(){self.update();onUpdate();} }
                  />
              );
              break;
            case 'SWITCH':
              let onToggle = null;
              let _id = changedValues.id;
              if(item.id=='otherSelection'){
                onToggle = function(value, id){
                  if(value){
                    let otherValue = {title:NeedDetailStore.genLangCodesObj(), isOther: true, id: 'others'};
                     if(inputType=='imageSelection' || inputType=='iconSelection'){
                       otherValue.image = '';
                     }
                    changedValues.options.push(otherValue);
                  }else{
                    for(let m in changedValues.options){
                      if(changedValues.options[m].isOther){
                        changedValues.options.splice(m,1);
                      }
                    }
                  }
                  changedValues[id]=value;
                }
              }else if(item.id=='isRelationNeedsAly'){
                onToggle = function(value, id){
                  changedValues[id] = value;
                  if(!value){
                    for(let e in changedValues.options){
                      changedValues.options[e].needAlyRelationIds = undefined;
                    }
                  }
                }
              }else if(item.id=='allowAdding' && inputType=='attachment'){
                onToggle = function(value, id){
                  changedValues.attachmentSetting.allowAdding = value;
                }
              }else{
                onToggle = function(value, id){
                  changedValues[id] = value;
                }
              }
              items.push(
                <SwitchField
                  disabled = {item.disabled}
                  template = {item}
                  mode = "landscape"
                  style={{height:'36px'}}
                  values = {values}
                  requestChange = {
                    function(value, id){
                      if(onToggle){
                        onToggle(value, id);
                      }
                      self.update();
                      onUpdate();
                    }
                  }
                  changedValues = {changedValues}/>
              );
              break;
            case 'NOTE':
              items.push(
                <div className={"DetailsItem"}>
                  <span>{item.value}</span>
                </div>
              )
              break;
            case 'CHECKBOXGROUP':
              let _handleChange = null;
              if(item.id=='attachOpts'){
                _handleChange = function(id, value){
                  let setting = changedValues.attachmentSetting;
                  setting.options = [];
                  if(!isEmpty(value)){
                      let opts = value.split(",");
                      for(let i in opts){
                        let orgOptions = item.options;
                        let title = "";
                        for(let j in orgOptions){
                          if(opts[i] == orgOptions[j].value){
                            title = orgOptions[j].title;
                          }
                        }
                        setting.options.push({title:{en:title}, type:opts[i]});
                      }
                  }
                }
              }
              items.push(
                <CheckboxGroup
                  template = { item }
                  values = { values }
                  changedValues = { changedValues }
                  disabled = {item.disabled}
                  handleChange = {
                    function(id, value){
                      typeof _handleChange=='function' && _handleChange(id, value);
                      onUpdate();
                    }
                  }
                  />
              );
              break;
          }
        }
      }
    }

    let inputDialog = (
      <InputTypeDialog
        onChange={function(){self.update();onUpdate();}}
        values={values}
        changedValues={changedValues}
        disabled={isLock}/>
    );
    return(
      <div ref="rightPanel" style={{ display: 'inline-block', position: 'relative', width:'calc(35% - 1px)', height: (height-65)+'px', overflowY: 'auto'}}>
        {typeof inputType == 'string'?inputDialog:null}
        {items}
        <div className={"DetailsItem"}>
          <RaisedButton
            key={'cp-btn-add-rules'}
            style={{display:'inline-block', width: '100%', color: '4A90E2'}}
            onClick={null}
            label={getLocalizedText("CONFIG_PANEL.ADD_RULES_AND_VALIDATIONS","Add Rules And Validations")}
          />
        </div>
      </div>
    );
  }
});

let DynConfigPanel = React.createClass({

  mixins: [React.LinkedStateMixin, StylePropable],

  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },


  propTypes:{
    id: React.PropTypes.string.isRequired,
    children: React.PropTypes.node,
    onPanelClose: React.PropTypes.func,
    onPanelOpen: React.PropTypes.func,
    onRequestChange: React.PropTypes.func,
    onHandleDelete: React.PropTypes.func,
    height:React.PropTypes.number,
    style: React.PropTypes.object,
    changedValues: React.PropTypes.object,
    values: React.PropTypes.object
  },

  componentDidMount() {
    // console.debug('homepage did mount');
    _isMount=true;
    DynConfigPanelStore.addChangeListener(this.onPanelChange);
  },

  componentWillUnmount: function() {
    _isMount=false;
    DynConfigPanelStore.removeChangeListener(this.onPanelChange);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    //reflesh when open or close config panel
    if(this.state.open != nextState.open)
      return true;

    if(DynConfigPanelStore.isReflesh()){
      return true;
    }
    return false;
  },

  refleshPanel(){
    DynConfigPanelActions.updateInputType();
  },

  onPanelChange(){
    let open = DynConfigPanelStore.getPanelState();
    let changedValues = DynConfigPanelStore.getChangedValues();
    let actions = DynConfigPanelStore.getActions();
    let type = DynConfigPanelStore.getType();

    if(_isMount) this.setState({id: changedValues.id, inputType: changedValues.type, open:open, changedValues:changedValues, type:type, actions:actions});

  },

  getDefaultProps() {
    return {
      //open: false
    }
  },

  componentWillReceiveProps(nextProps){

  },

  getInitialState: function() {
    DynConfigPanelStore.updatePanel(false);
    let changedValues = DynConfigPanelStore.getChangedValues();
    let type = DynConfigPanelStore.getType();
    let actions = DynConfigPanelStore.getActions();
    return {
      open: false,
      muiTheme: this.context.muiTheme ? this.context.muiTheme : appTheme.getTheme(),
      changedValues:changedValues?changedValues:{},
      type: type?type:"",
      actions: actions?actions:{},
      isLock:ContentStore.getPageContent().values.readOnly
    }
  },

  onPanelClose(){

      let {
        onPanelClose,
        validation
      } = this.state.actions;

      //if change section or close panel, call close panel function if exist.
      let errorMsg = false;

      if(typeof(validation) == 'function'){
        errorMsg = validation();
        console.log("validation", errorMsg);
      }
      if(!errorMsg && typeof(onPanelClose) == 'function'){
        onPanelClose();
        console.log("onPanelClose");
      }else if(!errorMsg){
        let content = ContentStore.getPageContent();
        let changedValues = content.changedValues;
        changedValues['panelSave'] = true;
        let url = "/Needs/Detail/Save";
        AppBarActions.submitChangedValues(url);
        DynConfigPanelStore.clearData();
        return false;
      }
      return errorMsg;
  },


  getStyles() {
    let height = this.props.height?this.props.height:500;
    return {
      contentStyle: {
        color: '#0000000',
        top: '0px'
      },
      panelBodyStyle:{
        display:'flex',
        position:'relative',
        height:(height-65)+'px',
        width:'100%'
      },
      borderStyle:{
        display: 'inline-block',
        position: 'relative',
        width:'2px',
        height: (height-65)+'px',
        background: '#AAAAAA'
      },
      pickerStyle: {
        width: 'auto',
        height: '56px',
      }
    }
  },

  handleSelectionChange(id, value) {
    //AppBarActions.updateFilter(id, value);
  },


  hidePanel(){
    //this.refs["dcp-"+this.props.id].close();
    DynConfigPanelActions.closePanel();
  },

  render: function() {
    var self = this;
    var {
      id,
      height,
      ...others
    } = this.props;

    var {
      open,
      value,
      changedValues,
      actions,
      isLock
    } = this.state;

    if(debug){
      console.log("render - inputconfigPanel");
    }

    let styles = this.getStyles();

    return (
      <ConfigPanel key={"dcp-"+id} ref={"dcp-"+id} id={id} open={open} height={height?height:500} onPanelClose={this.onPanelClose}>
        <Topbar id={id} changedValues={changedValues} isLock={isLock} value={value} actions={actions}/>
        <div key={"dcp-"+id+"-panel"} style={styles.panelBodyStyle}>
          <LeftPanel changedValues={changedValues} isLock={isLock} value={value} actions={actions} height={height}/>
          <div style={styles.borderStyle}/>
          <RightPanel changedValues={changedValues} isLock={isLock} value={value} actions={actions} height={height}/>
        </div>
      </ConfigPanel>
    );
  }

});

module.exports = DynConfigPanel;
