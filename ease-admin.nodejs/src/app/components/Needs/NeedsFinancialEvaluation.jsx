let React = require('react');
let mui = require('material-ui');
let ReactCSSTransitionGroup = require('react-addons-css-transition-group');

let NeedPageMinix = require('./NeedPageMinix.js');
let ItemMinix = require('../../mixins/ItemMinix.js');

let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');

let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');
let DialogActions = require('../../actions/DialogActions.js');
let AppBarActions  = require('../../actions/AppBarActions.js');

let RaisedButton = mui.RaisedButton;
let FontIcon = mui.FontIcon;
let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let Divider = mui.Divider;
let SortListPage = require('./SortListPage.jsx');
let NeedsQuestionItem = require('./NeedsQuestionItem.jsx');

let HFTitle = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

	openHFPanel(titleId){
		let {
			changedValues
		} = this.props;

		let self = this;

		if(!checkExist(changedValues, titleId+'.en')){
			changedValues[titleId]=NeedDetailStore.genLangCodesObj();
		}

		//self.setState({cfCValues:changedValues[type]});
		DynConfigPanelActions.openPanel(

			changedValues[titleId],
			'question',
			{
				onUpdate: self.onUpdate/*,
				onPanelClose: function(){
					AppBarActions.submitChangedValues('/Needs/Detail/Save')
				}*/
			});
	},

	getTitleId(type){
		return (type=='header')?'headerTitle':'footerTitle';
	},

	render(){
		if(this.isDebug()){
			console.log("render - title");
		}

		let self = this;
		let {
      changedValues,
			type
    } = this.props;

		let titleId = this.getTitleId(type.toLowerCase());
		let title = (checkExist(changedValues, titleId+'.en') && !isEmpty(changedValues[titleId].en))?
				multilineText(changedValues[titleId].en):
				<span style={{color:'(0, 0, 0, 0.298039)'}}>
					{multilineText(getLocalizedText(type.toUpperCase(),"Please click here to add " + type.toLowerCase()))}
				</span>;

		return(
			<div  className="configurable" style={{cursor:'pointer', width:'100%', height:'58px', paddingTop:'12px', paddingLeft:'24px', paddingRight:'24px'}} onClick={function(){self.openHFPanel(titleId)}}>
				{title}
			</div>
		);
	}
});


let NeedsFinancialEvaluation = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

	componentDidMount: function() {
		ContentStore.addChangeListener(this.onContentChange);
    NeedDetailStore.addChangeListener(this.onPageChange);
  },

  componentWillUnmount: function() {
		ContentStore.removeChangeListener(this.onContentChange);
    NeedDetailStore.removeChangeListener(this.onPageChange);

  },

  onPageChange(){
		var content = ContentStore.getPageContent();
		var self = this;
		if(content){
			var {
				template
			} = content;

			var {
				validation,
				sectionId
			} = this.state;

			let foundTemplate = false;

			if(!template.items){
				template.items = [];
			}

			for(let j in template.items){
				if(template.items[j].id == sectionId){
					template.items[j].validate = validation;
					let foundTemplate = true;
				}
			}

			if(!foundTemplate){
				template.items.push({id:sectionId, validate:validation});
			}
			this.onContentChange();
		}
	},

	onContentChange() {
		let content = ContentStore.getPageContent();
		let {
			values,
			changedValues
		} = content;
		let {
			sectionId
		} = this.state;

    let currSid = NeedDetailStore.getCurrentPage();
		if(currSid!=sectionId && !values[currSid]){
			values[currSid]={
				questions:[]
			};
		}else if(currSid!=sectionId && !values[currSid].questions){
			values[currSid].questions = [];
		}
		if(currSid!=sectionId && !changedValues[currSid]){
			changedValues[currSid]={
				questions:[]
			};
		}else if(currSid!=sectionId && !changedValues[currSid].questions){
			changedValues[currSid].questions = [];
		}

		if(changedValues && currSid && changedValues[currSid])
			this.setState({
        sectionId: currSid,
				values: values[currSid],
				changedValues: changedValues[currSid]
			});
	},

  shouldComponentUpdate: function(nextProps, nextState) {
		if(NeedDetailStore.isReflesh()){
			return true;
		}
			if(nextState.currentPage != this.state.currentPage){
				return true;
			}
			if(nextState.sectionId != this.state.sectionId){
				return true;
			}
			return false;
	},


	getInitialState() {

		let self = this;
		var content = ContentStore.getPageContent();
		var sectionId = NeedDetailStore.getCurrentPage();
		var {
			//template,
			values,
			changedValues,
			template
		} = content;

		let validation = function(changedValues){
			let error = false;
			var errQuest = "";
			if(changedValues && changedValues.questions){
				for(let i in changedValues.questions){
					let question = changedValues.questions[i];
					//common checking of input type
					if(isEmpty(question.title.en)){
						error = true;
						if(errQuest.length!=0) errQuest+=",";
						errQuest+=Number(i)+1;
						break;
					}else if(isEmpty(question.type)){
						error=true;
						if(errQuest.length!=0) errQuest+=",";
						errQuest+=Number(i)+1;
						break;
					}else{
						let items = NeedDetailStore.getInputTypeSections(question.type).items;
						for(let j in items){
							if(items[j].mandatory && isEmpty(question[items[j].id])){
								error = true;
								if(errQuest.length!=0) errQuest+=",";
								errQuest+=Number(i)+1;
								break;
							}
						}
					}

				}
			}
			if(error){
				return "Please Enter All data of Question(s)";// + errorMsg;
			}
		};

		if(!template.items){
			template.items = [];
		}

		if(!values[sectionId]){
			values[sectionId]={
				questions:[]
			};
		}else if(!values[sectionId].questions){
			values[sectionId].questions = [];
		}

		if(!isEmpty(values) && isEmpty(changedValues)){
			changedValues = content.changedValues = cloneObject(values);
		}

		if(!changedValues[sectionId]){
			changedValues[sectionId]={
				questions:[]
			};
		}else if(!changedValues[sectionId].questions){
			changedValues[sectionId].questions = [];
		}

		let foundTemplate = false
		for(let j in template.items){
			if(template.items[j].id == sectionId){
				template.items[j].validate = validation;
				let foundTemplate = true;
			}
		}

		if(!foundTemplate){
			template.items.push({id:sectionId, validate: validation});
		}

		return {
			values: values[sectionId],
			changedValues: changedValues[sectionId],
			isLock: NeedDetailStore.isLock(),
			sectionId: sectionId,
			validation: validation
		}
	},

	getNextId(questions){
		let nextId = "r1";
		//get the latest id
		for(let i in questions){
			let _id = questions[i].id;
			if(_id){
				_id = _id.substring(1, _id.length);
				if(isNumeric(_id) && Number(_id) >= Number(nextId.substring(1, nextId.length)))
					nextId= "r"+(Number(_id)+1);
			}
		}
		return nextId;
	},

	getStyles(){
		return(
				{
					contentStyle:{
							width:'100%',
							height: 'calc(100vh - 58px)',
							position: 'relative',
							overflowY: 'hidden'
					},
					headerStyle:{
						width:'100%',
						height:'58px',
						lineHeight:'58px',
						paddingLeft:'24px',
						paddingRight:'24px'
					},
					footerStyle:{
						width:'100%',
						height:'58px',
						lineHeight:'58px',
						paddingLeft:'24px',
						paddingRight:'24px'
					},
					listStyle:{
					  minHeight: 'calc(100vh - 232px)'
					},
					placeholderStyle:{
						fontStyle: 'italic',
						color: '#AAAAAA'
					}
				}
		)

	},

	openCFPanel(){
		let {
			changedValues
		} = this.state;
		let self = this;
		let {
			questions
		} = changedValues;

		let seq = (changedValues.questions.length+1) * 100;
		let langCodesItem = NeedDetailStore.genLangCodesObj();

		let newQuestion = {
			id: self.getNextId(questions),
			type: '',
			label: 'Question',
			title: langCodesItem,
			isMandatory: false,
			seq: seq
		};

		questions.push(newQuestion);
		let onUpdate = function(){
			self.refs["nqi-"+questions[questions.length-1].id].onUpdate();
		};

		let validation = function(){
			let type = newQuestion.type;
			let errorMsg = false;
			if(!checkExist(questions[questions.length-1], 'title.en')){
				errorMsg = "Some mandatory data do not input yet, if you click OK button, this question would be deleted.";
			}else if(type){

				let section = NeedDetailStore.getInputTypeSections(type);
				for(let i in section.items){
					let _error = validate(section.items[i], questions[questions.length-1]);

					if(_error){
						errorMsg = "Some mandatory data do not input yet, if you click OK button, this question would be deleted.";
						break;
					}
				}
			}
			if(!errorMsg){
				console.log("validate");
			}else{
				console.log(errorMsg);
				let removeDialog = {
					id: "delete",
					title: getLocalizedText("DELETE","DELETE"),
					message: errorMsg,
					negative: {
						id: "delete_cancel",
						title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
						type: "button"
					},
					positive:{
						id: "delete_confirm",
						title: getLocalizedText("BUTTON.OK","OK"),
						type: "customButton",
						handleTap: function(){
							questions.splice(questions.length-1, 1);
							self.onUpdate();
							DynConfigPanelActions.closePanel(true);
						}
					}
				}

				DialogActions.showDialog(removeDialog);
			}

			return errorMsg;
		};

		DynConfigPanelActions.openPanel(
			questions[questions.length-1],
			'question',
			{
				onUpdate:onUpdate,
				validation: validation
			});
		self.onUpdate();
	},

	genItems(questions){
		let its = [];
		let self = this;
		let {
			sectionId
		} = this.state;
		for(let i in questions){
			let question = questions[i];
			its.push(
				<NeedsQuestionItem key={"nqi-"+question.id} ref={"nqi-"+question.id} id={question.id} index={Number(cloneObject(i))} changedValues={questions} sectionId={sectionId} onListUpdate={function(){self.forceUpdate();}}/>
			);
		}
		return its;
	},

	render: function() {

		var self = this;
		let styles = this.getStyles();

    let {
      values,
      changedValues,
			deleteAction,
			sectionId,
			isLock
    } = this.state;
		if(this.isDebug()){
			console.log("render nfe");
			console.log("NFE - changedValues.questions", changedValues?changedValues.questions:undefined);
		}
		let items = this.genItems(changedValues?changedValues.questions:[]);

		let header = (
			<div key={'nfe-header'} style={{height: '58px', display: 'block', overflow:'hidden'}}>
        <Toolbar key="nfe-hToolbar" style={{padding:"0px", position:'relative', background: '#FFFFFF'}} >
          <ToolbarGroup key={'nfe-hToolbar-left'} style={{display:'flex'}}>
            <ToolbarTitle text={sectionId=='NeedsFinEval'?getLocalizedText("MENU.FIN_EVAL","Financial Evalation"):getLocalizedText("QUESTIONNAIRE","Questionnaire")} style={{color:'#000000', paddingLeft: '24px'}}/>
          </ToolbarGroup>
          <ToolbarGroup key={'nfe-hToolbar-right'}  float="right" style={{display:'flex'}}>
						{isLock?null:<span style={{marginTop:'16px', cursor: 'pointer'}}><FontIcon
							key={'nfe-btn-sort-list'}
			        style={{display:'inline-block', float: 'right', padding: '0px 15px 0 24px'}}
							color = {"#000000"}
							hoverColor={"#000000"}
							onClick={function(){self.refs["slPage"].show();}}
							className="material-icons">
							format_line_spacing
						</FontIcon></span>}
						{isLock?null:<RaisedButton
			        key={'nfe-btn-add-question'}
			        style={{display:'inline-block', float: 'right', marginRight: '24px'}}
							labelStyle={{textTransform: 'none'}}
			        onClick={this.openCFPanel}
			        label={getLocalizedText("BUTTON.ADD_QUESTION","ADD QUESTION")}
			      />}
          </ToolbarGroup>
        </Toolbar>
				<hr/>
      </div>
		);


		return (
			<div key={"nfe-content"} style={styles.contentStyle}>
				<SortListPage ref="slPage" changedValues={changedValues?changedValues.questions:[]} onListUpdate={self.onUpdate} disabled={isLock}/>
				<div style={{height:'calc(100vh - 58px)', overflowY: 'hidden'}}>
					{header}
					<Divider/>
					<div style={{height: 'calc(100vh - 116px)', overflowY: 'auto'}}>
						<HFTitle changedValues={changedValues} type="header"/>
						{items}
						<HFTitle changedValues={changedValues} type="footer"/>
					</div>
				</div>
			</div>
		);
	},
});

module.exports = NeedsFinancialEvaluation;
