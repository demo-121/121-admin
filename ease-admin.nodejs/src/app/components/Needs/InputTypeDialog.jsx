var React = require('react');
let mui = require('material-ui');

let StylePropable = mui.Mixins.StylePropable;
let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;

let IconButton = mui.IconButton;
let RaisedButton = mui.RaisedButton;
let FontIcon = mui.FontIcon;
let FlatButton = mui.FlatButton
let RadioButton = mui.RadioButton;
let RadioButtonGroup = mui.RadioButtonGroup;
let Dialog = mui.Dialog;

let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let ContentStore= require('../../stores/ContentStore.js');

let EABTextField = require('../CustomView/TextField.jsx');

let three_coins = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADEAAAAcCAYAAADIrlf0AAAAAXNSR0IArs4c6QAABK5JREFUWAnVmF1oHUUUx/+7uYlJTBtjSs2Hn0FaQQpaLO2DFUUEK2okDSLFB0VB0hfx60moQhHFT0SwSH0QpFVsGsRCUUte6oNWRUFRVEpaP2JCSYxtkyYa7x1/Z+7e7b25u0mUTWkPzM7Mmf/MmTNzzsyZDbQIcv26VqHuktNGBeok7yBvWkTXxUOc8oCPMe7v5D+poH2k/cHdOr7QIMF8ALdXvQz6DJhV8+GWrM1pFvlvsWhPBZs1kiYnUQn3nq5QjXYzwIao4xgDHaL8BWmUFfqDtpm0Qf8X37HXUjPfC8mvYvz15GtINaRTyN+GIi9RrqIqJdwe3Ui3fpCtpHE672bSgwxaqOq91IxA7ci9FzHXkwLmsksn9GBwf+UCVijhFQj1MfBav/JOL9N5eqnnuuD4gdYxp8fANTKvD/Ge2/EV8yFPtoWeIhPqjxTYB9h8IVkB2/TzQS5EWeEcZuz0BOImkHor0l8oFx0r4X3ATMhs32lnOWhueVTqU05bdd7clsp6pjinXzHoZ5EwixKPuAGUicivZnQK7YE3DnArefIOrFRPfkz3mafFVMOxeLGe0xEdjnlZ4+KBKQTqJj1A6Vt9o2uCp1UoKjGgH2CuZgdeIx0o71MqTyzX4y2TuqFUr8prdaf+Yr8zxlXJKWADOb0Ov40F3xL06p2QXVgLYzVpDOZgVSdjXKamWIFl+mi2SQdFDv9Rj79EW0yBzHF+8DmfUP8wz72eG+gey0O/PVYyX0g5RvNhhfXvGGnU96zGhDehBrb3Z03aEFnjbMwU+gy+Hfm3uDfUGDL5jRHQLrJEwuhOu0GL1rQVVA+neNlNMUJEWeNK4ybkFoocZtEbtELX5Sh0RiAOk2Rykxo3T2+w5l+0vc7yen3HvT7IbsSxTdY4EzMPWRiyiiXstJ3o8EALJVKo7pjyDZerL75dDDejqzGjtzHID9SllcbKGmdjppKL5huo4/Q9kYqOGoY0zFZ0mzPP2LFaTkf1Jsds8dbIGlcup7JcvGydnDm2hb7m4hZ4zUs1Zv/cB5OtGiAQP6iL9GrcoZYjL6KscaVxK/JALVF9xMxpOKrEk6gAR5W8hRBtxFSQM8eu1xDB8WB8KzpdYG1Z42zMFGr3/EDD5tifULmJtI70lW+Y88kTJ/05pV2tJ4mYutTXOE50mWfnmtXecDICB/ota9ycaZRXm6lcyQZMc7t9meO0fZ813oYy6ynvJK8KuaeWaVPrNAoYHdWOYsHXip86jOyIxqfadFuWuDIJlcXQv3PMnw8ED+lUyLX9NZUfSStQ5uZKdLG2fFT7zf7/Tmps0busyMPWlDUuSRxLnIO/2bc5ZEP/LQC0HpjWcSLYZsdZdUIvGiuRssaVhKQFgNZODPUpKm1gVS0Ut7dEOhFL+cYo3EgFZo2zZ2vo52YHzKaghwcSVDxrKaBEF7XPKdqbwh5F874prPMZpktRYDsyWzCpV3CDYvAJI77seIQPceL0Mnn7w3AH6UnK9Wd4osni7Hka6nmvgD1PC/6VF2PjnShxzvkfBbEiZlriz8LZ9MtG/HvqST5IqnaipIjl0bPVnPzc+3lWrkikzFp2pRsfOSt/Y/4L2scUuCIBqB8AAAAASUVORK5CYII=';
let one_coin = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAYAAAByDd+UAAAAAXNSR0IArs4c6QAABHhJREFUSA29VkuIW1UY/s69eUwmr5lk2s501KqbLkSKwmhdVBcqxZWPVhdduShIqyCKK3GniAupCOIguBOLj46KC2HQWWgXthaULhQGpK3iTMa2mSaTpJ1Mcu/1+/8kNzfJTaaCeCC55/zn/7/v/I/zMLiJ5p3CPbDwBDwcgMEsv7vVzGCV/RXKTsPFV+YwftkOzoxS8BZwmGBvUGfvKL3A3DIX8Jo5hFMBWU83lND7DHfAxkmS7RftegPl9QrOl6r4ueFi1Xi4InLPYEfUwu6JFO7NpbEvHkVW5CQ9AwdHzDO4qOPA3wAhyR5CBAvUyTeaKBfW8WXlOhZzWdRTEbixKNx4rIVQ3wK2GrCqTVjrZcTT4zg4k8OT0YgSF9HEIZJ+H+CjD4GmZDa+pTRaruE8yd6ZnUApnWSGbqJVarBWSpgg6UvZJPbR0wY9fTRI6hNqGCM4R9x8sYLvajfwwa4M6h1veviyLKEm4Wr89TXx+u8NxFMJPMcwP8Jp8XSuE17L15eckay2yUrbwvxtU0PIqLQGHGPYjyPuW/sdWaDYesRQLGJqPbQ11MN2NX7edFC6soEXZrLY8BGCnZ14yrmKZ+2gzMZl3IK3WB6/B8XSL5SR2ZHBexEbE4zF01K9LQ9bpQ+u6NNcIpzsWgavoJ9MUB3sxB84EeatYAmmqLW3Fyzd1NxnjoN1y8ViaM72IDVZxYNqmMZiI4UfwC/24GWV3YojqGuv50+wBFOwObFXuCw9QTiqOzjHamz2WLQHjtWz/vnCOH5jDq9pGBN4nB5Ww+xEJpiCrfM8rSzG9oAOPPyk35A/1zDtnTaJu6ddjFGyqaKQSu2o+t8ONrkijO2sTrgo+Ap9Ha+K4g3KEiL/E6/rvh/DrzyPluhluU99cNjBJpd4qAdxBCyJIS12GU7idhxzgvObuIuh/Igp+Rp3snBGNB+bXP4+jOmyR1hd4K0g+WKhbMpWCLZL+JBbI2RXtpR8bAPPYkhXVWwhF8QI69uSL+63ah5fQCp1F9719aKY9vv9nS52QUK6IvOuO8KA844cZ9OIiq4nRTOGC8z6kuRWm8fNPaT52OQSD0+LnufhviH6cJIwpRo+ZiAXmK/Z8TpmcJ05zOJ5LSQxNPhrmL2PTS5LbmpRtCMk9OhFSKul8VjeRVKnLmE+WcFBEs5BvtJiDPRFHtJhjZiKLXPkstrPgmUOp0j3cJhNZg3fSL54EQy2SXzChb44ONGWtDCnOFoWrp7Dm8IiV3GcXz81bbPuh+Et86bIeqzZDbzdnQjtJejE+5zJc1F6eHfvwwX8yDzsZ7zPGgdvUnHgrvMhebZqf8SRxoUbz8arxuB+Ip3hTfGA2HRzxjcIx0VVsHFUDBQ07E+ItiHj0XdUsSRqLWxF6gENPjG4qrP8naDW8PCGLUZOQMNbpOXZ8CdGxzb4iKKsSNKT9HaJAKPfNVLhUiBGI5VX2+0eUQHSnmci5VfVY+g1s0Zqud8kIXI6yQkzpx5JpUv7N89ENWj//W8P4SCp9P/Lp/4/PMuKpQQVJZIAAAAASUVORK5CYII=';

import appTheme from '../../theme/appBaseTheme.js';

var InputTypeDialog = React.createClass({
	mixins: [React.LinkedStateMixin, StylePropable],

	 childContextTypes: {
		 muiTheme: React.PropTypes.object,
	 },

	 getChildContext() {
		 return {
			 muiTheme: this.state.muiTheme,
		 };
	 },

	 contextTypes: {
		 muiTheme: React.PropTypes.object
	 },

	 propTypes:{
		 onChange: React.PropTypes.func
   },

	 componentWillReceiveProps(nextProps){
     this.setState({
			 changedValues: nextProps.changedValues
		 })
   },


	 _sections: NeedDetailStore.getInputTypeSections(),

	componentDidMount: function() {

  },

  componentWillUnmount: function() {

  },


	getInitialState() {
		let {
			disabled,
			changedValues
		} = this.props;
		let cValue = changedValues?changedValues['type']:'';
		return {
      selectedValue: cValue,
			changedValues: changedValues,
			open: false,
			disabled: disabled
    }
	},

	getStyles() {
    let theme = this.state.muiTheme;
    let height = this.props.height?this.props.height:500;
    return {
      topBarTitleStyles:{
        paddingRight:'16px',
        verticalAlign: 'super',
        fontSize: '24px',
        display: 'inline-block',
        position: 'relative'
      },
      panelBodyStyle:{
        display:'flex',
        position:'relative',

        width:'100%'
      },
      leftPanelStyle:{
        display: 'inline-block',
        position: 'relative',
        width:'calc(30% - 1px)',

        overflowY: 'hidden',
				padding: 16,
      },
      borderStyle:{
        display: 'inline-block',
        position: 'relative',
        width:'1px',
        background: appTheme.palette.borderColor,
      },
      rightPanelStyle:{
        display: 'inline-block',
        position: 'relative',
        width:'calc(70% - 1px)',
        overflowY: 'auto',
				textAlign:'center',
      },
			radioButton:{
				padding: '16px 8px 16px 8px'
			},
			RadioButtonGroup:{
				padding: '16px 0px 16px 0px'
			},
			titleStyle:{
				color: '#2696CC',
				padding: '16px 8px 16px 8px',
			},
			imageStyle:{
				maxWidth:'100%'
			}
    }
  },

	hidePanel(){
		this.setState({open:false});
	},

	_genTopBar(){
    let styles = this.getStyles();

    return (
			<Toolbar style={{padding:"0px", position:'relative', borderBottom:'1px solid '+appTheme.palette.borderColor}} >
				<ToolbarGroup style={{width:'65%', display:'flex'}}>
					<FontIcon
						key='closeBtn'
						style = {{padding: '0px 15px 0 24px'}}
						color = "#000000"
						className="material-icons"
						onClick={this.hidePanel} >
						close
					</FontIcon>
					<ToolbarTitle text='Input Type' style={{color:'#000000'}}/>
				</ToolbarGroup>
				<ToolbarGroup float="right" >
					<FlatButton label="Select" onClick={this.updateValue} />
				</ToolbarGroup>
			</Toolbar>
    );
  },

	getTitle(value){
		let {
			changedValues
		} = this.state;

		let cValue = checkExist(changedValues,'type')? changedValues['type']:'';
		if(!isEmpty(cValue)){
				let item = NeedDetailStore.getInputTypeSections(cValue);
				return checkExist(item,'title')?item.title:'';
		}
		return '';
	},

  _genLeftPanel(){
    let styles = this.getStyles();
		let self = this;
		let {
			selectedValue
		} = this.state;

		let items = [];
		for(let i=0; i<this._sections.length; i++){
			let section = this._sections[i];
			let title = <label key={'t-'+section.id} className="Title" style={styles.titleStyle}>{section.title}</label>
			let rbItems = [];

			for(let j=0; j<section.items.length; j++){
				let rbItem = section.items[j];
				rbItems.push(
					<RadioButton
						key={'rbItems'+rbItem.value}
						value={rbItem.value}
						label={rbItem.title}
						style={styles.radioButton}
						iconStyle={{fill:rbItem.value==selectedValue?appTheme.palette.primary2Color:'black'}}
						disabled={rbItem.disabled}
					/>
				)
			}

			let rbGroup = (
				<RadioButtonGroup name="rbSelection"
					key={'rbg-'+section.id}
					 valueSelected={selectedValue}
					 style={styles.RadioButtonGroup}
					 onChange={
						function(e, value) {
							self.itemSelected(value);
						}
				}>
					{rbItems}
				</RadioButtonGroup>
			);

			items.push(title);
			items.push(rbGroup);
		}


    return(
      <div style={styles.leftPanelStyle}>
					{items}
      </div>
    );
  },

	_getImage(value){
		let {
			selectedValue
		} = this.state;

		if(!isEmpty(selectedValue)){
				let item = NeedDetailStore.getInputTypeSections(selectedValue);
				return checkExist(item,'src')?item.src:'';
		}
	},

  _genRightPanel(){
    let styles = this.getStyles();
		let imagePath = "web/img/input_type/";
		let {
			selectedValue
		} = this.state;

		let selectedImg = this._getImage(selectedValue);
		let img = !isEmpty(selectedImg)?<img style={styles.imageStyle} src={imagePath+selectedImg}/>:null;
    return(
      <div style={styles.rightPanelStyle}>
				{img}
      </div>
    );
  },

	itemSelected: function(value){
		this.setState({
			selectedValue: value
		});
  },

	updateValue: function(){
			let {
				selectedValue,
				changedValues
			} = this.state;

			let cValues = ContentStore.getPageContent().changedValues;

			//remove values
			if(!isEmpty(changedValues['type'])){
					let _item = NeedDetailStore.getInputTypeSections(changedValues['type']);
				 	if(_item.items){
						for(let i in _item.items){
							let item = _item.items[i];
							changedValues[item.id] = undefined;
						}
						if(changedValues.options){
							for(let i in changedValues.options){
								let option = changedValues.options[i];
								if(option.image && cValues["Attachments"]){
									cValues["Attachments"][option.image] = undefined;
								}
							}
						}
						if(checkExist(changedValues, 'attachmentSetting.image') && cValues["Attachments"]){
							cValues["Attachments"][changedValues.attachmentSetting.image]=undefined;
						}

						changedValues['options'] = undefined;
						changedValues['attachmentSetting'] = undefined;
					}
			}

			changedValues['type'] = selectedValue;

			//initial values
			if(!isEmpty(selectedValue)){
					let _item = NeedDetailStore.getInputTypeSections(selectedValue);
				 	if(checkExist(_item,'items')){
						for(let i in _item.items){
							let item = _item.items[i];
							switch(item.type){
								default:
									changedValues[item.id] = (item.value)?item.value:(item.subType=='number')?0:'';
							}
						}
					}
					if(selectedValue=='attachment'){
						changedValues['attachmentSetting']={
							allowAdding:true,
							image: 'adding',
							options: [],
							title:NeedDetailStore.genLangCodesObj()
						}
					}else if(selectedValue=='normalSlider'){
						if(!cValues['Attachments']) cValues['Attachments']={};
						cValues['Attachments']['one_coin'] = one_coin;
						cValues['Attachments']['three_coins'] = three_coins;
					}
			}

			if(this.props.onChange){
				this.props.onChange();
			}

			this.closeDialog();
	},

	openDialog: function(){
		let {
			changedValues
		} = this.state;

		let cValue=changedValues['type'];

		this.setState({
			open: true,
			selectedValue: cValue
		});
	},

	closeDialog: function(){
		this.setState({
			open: false
		});
	},



	render: function() {

		var self = this;

		let {
			open,
			disabled,
			selectedValue,
			changedValues
		} = this.state;



    let styles = this.getStyles();
    var topBar = this._genTopBar();
    var leftPanel = this._genLeftPanel();
    var rightPanel = this._genRightPanel();
		var customContentStyle = {
	      padding: '0px',
	    };

		let inputTypeItem={id:'type', title:getLocalizedText("CONFIG_PANEL.INPUT_TYPE","Input Type"), type:'TEXT', mandatory:true, disabled: disabled};
		var titleValue = {
			type: checkExist(changedValues,'type')?this.getTitle(changedValues['type']):''
		}
    return (
			<div style={{cursor: 'pointer'}}>
				<EABTextField
					key = {"itd_"+ inputTypeItem.id}
					template = { inputTypeItem }
					changedValues = { titleValue }
					onClick={!disabled?this.openDialog:null}
					/>
				<Dialog
					autoDetectWindowHeight={true}
					title={topBar}
		      actions={null}
		      modal={false}
		      open={open}
					titleStyle={styles.topBarStyle}
					contentStyle={{maxWidth:'2048px'}}
		      bodyStyle={customContentStyle}>
					<div style={styles.panelBodyStyle}>
						{leftPanel}
						<div style={styles.borderStyle}/>
						{rightPanel}
					</div>
				</Dialog>
			</div>
    );
	}
});

module.exports = InputTypeDialog;
