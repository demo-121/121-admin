let React = require('react');
let mui = require('material-ui');

let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');

let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');
let DialogActions = require('../../actions/DialogActions.js');
let NeedPageMinix = require('./NeedPageMinix.js');
let ItemMinix = require('../../mixins/ItemMinix.js');

let RaisedButton = mui.RaisedButton;
let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;

let SortableList = require('../CustomView/SortableList.jsx');
let SelectionBoxSection = require('../CustomView/SelectionBoxSection.jsx');
let NeedsSelectionItem = require('./NeedsSelectionItem.jsx');

let HFTitle = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

	openHFPanel(titleId){
		let {
			changedValues
		} = this.props;

		let self = this;

		if(!checkExist(changedValues, titleId+'.en')){
			changedValues[titleId]=NeedDetailStore.genLangCodesObj();
		}

		let configItem=changedValues[titleId];
		let validation = function(){
			let errorMsg = false;

			if(!configItem.en)
				errorMsg= "Some mandatory data do not input yet, if you click OK button, the configuration panel would be closed.";
			if(!errorMsg){
				console.log("validate");
			}else{
				console.log(errorMsg);
				let removeDialog = {
					id: "delete",
					title: getLocalizedText("DELETE","DELETE"),
					message: errorMsg,
					negative: {
						id: "delete_cancel",
						title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
						type: "button"
					},
					positive:{
						id: "delete_confirm",
						title: getLocalizedText("BUTTON.OK","OK"),
						type: "customButton",
						handleTap: function(){
							DynConfigPanelActions.closePanel(true);
						}
					}
				}
				DialogActions.showDialog(removeDialog);
			}

			return errorMsg;
		};



		DynConfigPanelActions.openPanel(configItem, 'question', { Update: self.onUpdate});
	},
	getTitleId(type){
		return (type=='header')?'header':'header';//TODO
	},


	render(){
		let self = this;
		let {
      changedValues,
			type
    } = this.props;

		if(this.isDebug()){
			console.log("render -NeedSelection-title-" + type, changedValues);
		}

		let titleId = type;//this.getTitleId(type.toLowerCase());
		let title = (checkExist(changedValues, titleId+'.en') && !isEmpty(changedValues[titleId].en))?
				multilineText(changedValues[titleId].en):
				<span style={{color:'(0, 0, 0, 0.298039)'}}>
					{getLocalizedText(type.toUpperCase(),"Please click here to add " + type.toLowerCase())}
				</span>;

		return(
			<div className="configurable" style={{textAlign:'center',
			fontSize:'16px',
			paddingBottom:'8px',
			paddingTop:'32px',
			cursor: 'pointer'}} onClick={function(){self.openHFPanel(titleId)}}>
				{title}
			</div>
		);
	}
});

let NeedSelection = React.createClass({
	mixins: [NeedPageMinix],

	onUpdate(){
    this.forceUpdate();
  },

	getInitialState() {
		var content = ContentStore.getPageContent();
		var {
			//template,
			values,
			changedValues,
			template
		} = content;

		if(!values[sectionId]){
			values[sectionId]={
				selections:[]
			};
		}else if(!values[sectionId].selections){
			values[sectionId].selections = [];
		}

		if(!isEmpty(values) && isEmpty(changedValues)){
			changedValues = content.changedValues = cloneObject(values);
		}

		if(!changedValues[sectionId]){
			changedValues[sectionId]={
				selections:[]
			};
		}else if(!changedValues[sectionId].selections){
			changedValues[sectionId].selections = [];
		}

		var sectionId = NeedDetailStore.getCurrentPage();

		let validation = function(changedValues){
			let error = false;
			if(changedValues && changedValues.selections){
				for(let i in changedValues.selections){
					let selection = changedValues.selections[i];
					//common checking of sections

				}
			}
			if(error){
				return "Please Enter All data";
			}
		};

		if(!template.items){
			template.items = [];
		}

		let foundTemplate = false
		for(let j in template.items){
			if(template.items[j].id == sectionId){
				template.items[j].validate = validation;
				let foundTemplate = true;
			}
		}

		if(!foundTemplate){
			template.items.push({id:sectionId, validate:validation});
		}


		return {
			values: values[sectionId],
			changedValues: changedValues[sectionId],
			isLock: NeedDetailStore.isLock(),
			sectionId: sectionId,
			validation: validation
		}
	},

	getStyles(){
		return(
				{
					contentStyle:{
							width:'100%',
							//height: 'calc(100vh - 90px)',//58+32
							position: 'relative',
							overflowY: 'hidden'
					},
					listStyle:{
					  minHeight: 'calc(100vh - 232px)'
					},
					placeholderStyle:{
						fontStyle: 'italic',
						color: '#AAAAAA'
					}
				}
		)

	},



	getNextId(selections){
		let nextId = "r1";
		//get the latest id
		for(let i in selections){
			let _id = selections[i].id;
			if(_id){
				_id = _id.substring(1, _id.length);
				if(isNumeric(_id) && Number(_id) >= Number(nextId.substring(1, nextId.length)))
					nextId= "r"+(Number(_id)+1);
			}
		}
		return nextId;
	},


	render: function() {

		var self = this;
		let styles = this.getStyles();

    let {
      values,
      changedValues,
			sectionId,
			deleteAction
    } = this.state;

		if(this.isDebug()){
				console.log("NeedsSelection - changedValues.selections", changedValues? changedValues.selections: undefined);
		}
		return (
			<div key={"nedsSelection-content"} style={styles.contentStyle}>
				<div style={{height:'calc(100vh - 58px)', overflowY: 'auto'}}>
					<HFTitle changedValues={changedValues} type="header"/>
						<div  style={{textAlign:'center'  }}>
							<div  style={{textAlign:'left',width:831 ,margin:'0 auto'}} >
				 				<SelectionBoxSection
									 changedValues={(changedValues && changedValues.selections && changedValues.selections[0])?changedValues.selections[0]:{}}
									 pickerArray={(changedValues && changedValues.selections && changedValues.selections[0] && changedValues.selections[0].pickerArray)?changedValues.selections[0].pickerArray:[]}
									 isSortable={true}
									 sectionId={sectionId}
									 disabled={this.state.isLock}/>
							</div>
						</div>
				</div>

			</div>
		);
	},
});

module.exports = NeedSelection;
