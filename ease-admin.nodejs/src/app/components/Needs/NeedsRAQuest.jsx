let React = require('react');
let mui = require('material-ui');

let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');

let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');
let DialogActions = require('../../actions/DialogActions.js');

let NeedPageMinix = require('./NeedPageMinix.js');
let ItemMinix = require('../../mixins/ItemMinix.js');

let FontIcon = mui.FontIcon;
let RaisedButton = mui.RaisedButton;
let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let Divider = mui.Divider;
let Transitions = mui.Styles.Transitions;

let SortListPage = require('./SortListPage.jsx');
let NeedsRAQuestionItem = require('./NeedsRAQuestionItem.jsx');
let sectionId = 'NeedsRAQuest';


let HFTitle = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

	openHFPanel(titleId){
		let {
			changedValues
		} = this.props;

		let self = this;

		if(!checkExist(changedValues, titleId+'.en')){
			changedValues[titleId]=NeedDetailStore.genLangCodesObj();
		}

		//self.setState({cfCValues:changedValues[type]});
		DynConfigPanelActions.openPanel(changedValues[titleId], 'question', {onUpdate: self.onUpdate});
	},

	getTitleId(type){
		return (type=='header')?'riskAssessHeader':'riskAssessFooter';
	},

	render(){
		let self = this;
		let {
      changedValues,
			type
    } = this.props;

		if(this.isDebug()){
			console.log("render -rqa-title-" + type, changedValues);
		}

		let titleId = this.getTitleId(type.toLowerCase());
		let title = (checkExist(changedValues, titleId+'.en') && !isEmpty(changedValues[titleId].en))?
				multilineText(changedValues[titleId].en):
				<span style={{color:'(0, 0, 0, 0.298039)'}}>
					{multilineText(getLocalizedText(type.toUpperCase(),"Please click here to add " + type.toLowerCase()))}
				</span>;

		return(
			<div className="configurable" key={"nraq-hf-"+titleId} style={{width:'100%', height:'58px', paddingTop:'12px', paddingLeft:'24px', paddingRight:'24px'}} onClick={function(){self.openHFPanel(titleId)}}>
				{title}
			</div>
		);
	}
});

let NeedsRAQuest = React.createClass({
	mixins: [NeedPageMinix],

	onUpdate(){
    this.forceUpdate();
  },

	getInitialState() {

		var content = ContentStore.getPageContent();
		var sectionId = NeedDetailStore.getCurrentPage();
		var {
			values,
			changedValues,
			template
		} = content;

		if(!values[sectionId]){
			values[sectionId]={
				questions:[]
			};
		}else if(!values[sectionId].questions){
			values[sectionId].questions = [];
		}

		if(!isEmpty(values) && isEmpty(changedValues)){
			changedValues = content.changedValues = cloneObject(values);
		}

		if(!changedValues[sectionId]){
			changedValues[sectionId]={
				questions:[]
			};
		}else if(!changedValues[sectionId].questions){
			changedValues[sectionId].questions = [];
		}

		var sectionId = NeedDetailStore.getCurrentPage();

		let validation = function(changedValues){
			let error = false;
			if(changedValues && changedValues.questions){
				for(let i in changedValues.questions){
					let question = changedValues.questions[i];
					if(isEmpty(question.title.en)){
						error = true;
					}
					if(question.options){
						for(let j in question.options){
							let option = question.options[j];
							if(isEmpty(option.text.en)){
								error = true;
							}
							if(isEmpty(option.code)){
								error = true;
							}
						}
					}

				}
			}
			if(error){
				return "Please Enter All data";
			}
		};

		if(!template.items){
			template.items = [];
		}

		let foundTemplate = false
		for(let j in template.items){
			if(template.items[j].id == sectionId){
				template.items[j].validate = validation;
				let foundTemplate = true;
			}
		}

		if(!foundTemplate){
			template.items.push({id:sectionId, validate:validation});
		}

		return {
			isLock: NeedDetailStore.isLock(),
			values: values[sectionId],
			changedValues: changedValues[sectionId],
			sectionId: sectionId,
			validation: validation
		}
	},

	getStyles(){
		return(
				{
					contentStyle:{
							transition: Transitions.easeOut(),
							width:'100%',
							height: 'calc(100vh - 58px)',
							position: 'relative',
							overflowY: 'hidden'
					},
					headerStyle:{
						width:'100%',
						height:'58px',
						lineHeight:'58px',
						paddingLeft:'24px',
						paddingRight:'24px'
					},
					footerStyle:{
						width:'100%',
						height:'58px',
						lineHeight:'58px',
						paddingLeft:'24px',
						paddingRight:'24px'
					},
					listStyle:{
					  minHeight: 'calc(100vh - 232px)',
						paddingLeft: '24px',
						paddingRight: '24px'
					},
					placeholderStyle:{
						fontStyle: 'italic',
						color: '#AAAAAA'
					}
				}
		)

	},

	openCFPanel(){
		let self = this;
		let {
			changedValues
		} = this.state;

		let {
			questions
		} = changedValues;

		let seq = (changedValues.questions.length+1) * 100;

		let newQuestion = {
			id: this.getNextId(questions),
			label: 'Question',
			title: NeedDetailStore.genLangCodesObj(),
			isMandatory: false,
			inputType: 'multiSelect',
			seq: seq
		};

		questions.push(newQuestion);
		let onItemUpdate = function(){
			self.refs["nraq-"+newQuestion.id].onUpdate();
		};

		let validation = function(){
			let errorMsg = !isEmpty(newQuestion.title.en)?false:"Some mandatory data do not input yet, if you click OK button, this question would be deleted.";

			if(!errorMsg){
				console.log("validate");
			}else{
				console.log(errorMsg);
				let removeDialog = {
					id: "delete",
					title: getLocalizedText("DELETE","DELETE"),
					message: errorMsg,
					negative: {
						id: "delete_cancel",
						title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
						type: "button"
					},
					positive:{
						id: "delete_confirm",
						title: getLocalizedText("BUTTON.OK","OK"),
						type: "customButton",
						handleTap: function(){
							questions.splice(questions.length-1, 1);
							self.onUpdate();
							DynConfigPanelActions.closePanel(true);
						}
					}
				}

				DialogActions.showDialog(removeDialog);
			}

			return errorMsg;
		};

		DynConfigPanelActions.openPanel(questions[questions.length-1], 'question', {onUpdate: onItemUpdate, validation: validation});
		if(this.isMount()) this.onUpdate();
	},

	getNextId(questions){
		let nextId = "r1";
		//get the latest id
		for(let i in questions){
			let _id = questions[i].id;
			if(_id){
				_id = _id.substring(1, _id.length);
				if(isNumeric(_id) && Number(_id) >= Number(nextId.substring(1, nextId.length)))
					nextId= "r"+(Number(_id)+1);
			}
		}
		return nextId;
	},

	genItems(questions){
		let its = [];
		let self = this;
		let {
			sectionId,
			isLock
		} = this.state;
		for(let i in questions){
			let question = questions[i];
			its.push(
				<NeedsRAQuestionItem key={"nraq-item-"+i} ref={"nraq-"+question.id} id={question.id} index={Number(i)} changedValues={questions} onListUpdate={function(){self.forceUpdate();}} sectionId={sectionId} disabled={isLock}/>
			);
		}
		return its;
	},

	render: function() {
		if(this.isDebug()){
			console.log("render-NRAQ");
		}

		var self = this;
		let styles = this.getStyles();

    let {
      values,
      changedValues,
			deleteAction,
			isLock
    } = this.state;
		let items = this.genItems(changedValues.questions);

		let header = (
			<div key={'nraq-header'} style={{height: '58px', display: 'block', overflow:'hidden'}}>
        <Toolbar key="nraq-hToolbar" style={{padding:"0px", position:'relative', background: '#FFFFFF'}} >
          <ToolbarGroup key={'nraq-hToolbar-left'} style={{display:'flex'}}>
            <ToolbarTitle text={getLocalizedText("QUESTIONNAIRE","Questionnaire")} style={{color:'#000000', paddingLeft: '24px'}}/>
          </ToolbarGroup>
          <ToolbarGroup key={'nraq-hToolbar-right'}  float="right" style={{display:'flex'}}>
						{isLock?null:<FontIcon
							key={'nfe-btn-sort-list'}
			        style={{display:'inline-block', float: 'right', padding: '0px 15px 0 24px'}}
							color = {"#000000"}
							hoverColor={"#000000"}
							onClick={function(){self.refs["slPage"].show();}}
							className="material-icons">
							format_line_spacing
						</FontIcon>}
						{isLock?null:
							<RaisedButton
				        key={'nraq-btn-add-question'}
				        style={{display:'inline-block', float: 'right', marginRight: '24px'}}
				        onClick={this.openCFPanel}
				        label={getLocalizedText("BUTTON.ADD_QUESTION","ADD QUESTION")}
				      />
						}
          </ToolbarGroup>
        </Toolbar>
				<hr/>
      </div>
		);


		return (
			<div key={"nraq-content"} style={styles.contentStyle}>
				<SortListPage ref="slPage" changedValues={changedValues.questions} onListUpdate={self.onUpdate} disabled={isLock}/>
				<div style={{height:'calc(100vh - 58px)', overflowY: 'hidden'}}>
					{header}
					<Divider/>
					<div style={{height: 'calc(100vh - 116px)', overflowY: 'auto'}}>
						<HFTitle changedValues={changedValues} type="header"/>
						{items}
            <HFTitle changedValues={changedValues} type="footer"/>
					</div>
				</div>

			</div>
		);
	},
});

module.exports = NeedsRAQuest;
