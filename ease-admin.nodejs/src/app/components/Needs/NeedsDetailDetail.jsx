/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let CurrentColumn = require('../DynamicUI/DynEditableColumn.jsx');

let FlatButton = mui.FlatButton;
let sectionId = 'NeedsDetail';

let NeedsDetailDetail = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  componentDidMount() {
    // console.debug('homepage did mount');
    ContentStore.addChangeListener(this.onContentChange);
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onContentChange);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    return true;
  },

  onPageChange(){
    let section = this.getSection(this.state.template);
    this.setState({section: section});
  },


  onContentChange() {
    var {
      template,
      values,
      changedValues
    } = ContentStore.getPageContent();

    this.setState({
      template:template,
      values: values,
      changedValues: changedValues,
    })
    /*template.validate = this._validate;
    template.checkChanges = this._checkChanges;
    var orgValues = this.state.values;
    if (checkChanges(values.id, orgValues.id) || checkChanges(values.version, orgValues.version)){
      var changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);
      this.setState({
        template:template,
        values: values,
        changedValues: changedValues
      });
    } else {
      this.setState({
        template:template,
      });
    }*/
  },

  getInitialState() {
    var {
      template,
      values
    } = ContentStore.getPageContent();

    let section = this.getSection(template);

    //initial values
    if(!checkExist(values, sectionId)){
      values[sectionId]={};
    }
    var currPage = NeedDetailStore.getCurrentPage();
    var changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);

    ContentStore.getPageContent().valuesBackup = values;
  
    return {
      template: template,
      values: values,
      changedValues: changedValues,
      section: section
    };
  },

  getSection(template){
    var templateItems = template.items;
    for (let i in templateItems) {
      var section = templateItems[i];
      if (section.id == sectionId) {
        return section;
      }
    }
    return template.items[1];
  },

  render: function() {
    var self = this;
    var section = this.state.section;

    let dataContainerStyle = {
      width:'100%',
      height:"calc(100vh - 58px)",
      overflowY:"auto",
    };

    return (
      <CurrentColumn
        style={dataContainerStyle}
        show = {true}
        template = {section}
        values = { this.state.values[sectionId] }
        changedValues = { this.state.changedValues[sectionId] }
        id = "current"
        ref = "current" />
    );
  }
});

module.exports = NeedsDetailDetail;
