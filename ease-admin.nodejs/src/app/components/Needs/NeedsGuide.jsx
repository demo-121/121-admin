let React = require('react');
let mui = require('material-ui');

let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');

let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');

let NeedPageMinix = require('./NeedPageMinix.js');
let ItemMinix = require('../../mixins/ItemMinix.js');

let RaisedButton = mui.RaisedButton;
let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let Divider = mui.Divider;

let SortableList = require('../CustomView/SortableList.jsx');
let NeedsGuideItem = require('./NeedsGuideItem.jsx');

let NeedsGuide = React.createClass({

	componentDidMount: function() {
		ContentStore.addChangeListener(this.onContentChange);
    NeedDetailStore.addChangeListener(this.onPageChange);
  },

  componentWillUnmount: function() {
		ContentStore.removeChangeListener(this.onContentChange);
    NeedDetailStore.removeChangeListener(this.onPageChange);

  },

  onPageChange(){
		var content = ContentStore.getPageContent();
    if(content){
  		var self = this;
  		var {
  			template
  		} = content;

  		var {
  			validation,
  			sectionId
  		} = this.state;

  		let foundTemplate = false;

  		if(!template.items){
  			template.items = [];
  		}



  		for(let j in template.items){
  			if(template.items[j].id == sectionId){
  				template.items[j].validate = validation;
  				let foundTemplate = true;
  			}
  		}

  		if(!foundTemplate){
  			template.items.push({id:sectionId, validate:validation});
  		}
  		this.onContentChange();
    }
	},

	onContentChange() {
		var content = ContentStore.getPageContent();
		var {
			values,
			changedValues
		} = content;

    var currSid = NeedDetailStore.getCurrentPage();

    if(!changedValues){
			changedValues = content.changedValues = {};
			changedValues[currSid]={guide:[]};
		}

		this.setState({
			sectionId: currSid,
			values: values[currSid],
			changedValues: changedValues[currSid]
		});

	},

	onUpdate(){
    this.forceUpdate();
  },

	getInitialState() {

		let self = this;
		var content = ContentStore.getPageContent();
		var sectionId = NeedDetailStore.getCurrentPage();
		var {
			//template,
			values,
			changedValues,
			template
		} = content;

		let validation = function(changedValues){
			let error = false;
			if(changedValues && changedValues.guide){
				for(let i in changedValues.guide){
					let _guide = changedValues.guide[i];
					//common checking of input type

				}
			}
			if(error){
				return "Please Enter All data";
			}
		};

		if(!template.items){
			template.items = [];
		}

		if(!values[sectionId]){
			values[sectionId]={
				guide:[]
			};
		}else if(!values[sectionId].guide){
			values[sectionId].guide = [];
		}

		if(!isEmpty(values) && isEmpty(changedValues)){
			changedValues = content.changedValues = cloneObject(values);
		}

		if(!changedValues[sectionId]){
			changedValues[sectionId]={
				guide:[]
			};
		}else if(!changedValues[sectionId].guide){
			changedValues[sectionId].guide = [];
		}

		let foundTemplate = false
		for(let j in template.items){
			if(template.items[j].id == sectionId){
				template.items[j].validate = validation;
				let foundTemplate = true;
			}
		}

		if(!foundTemplate){
			template.items.push({id:sectionId, validate:validation});
		}

		return {
			values: values[sectionId],
			changedValues: changedValues[sectionId],
			isLock: NeedDetailStore.isLock(),
			sectionId: sectionId,
			validation: validation
		}
	},

	getNextId(guide){
		let nextId = "r1";
		//get the latest id
		for(let i in guide){
			let _id = guide[i].id;
			if(_id){
				_id = _id.substring(1, _id.length);
				if(isNumeric(_id) && Number(_id) >= Number(nextId.substring(1, nextId.length)))
					nextId= "r"+(Number(_id)+1);
			}
		}
		return nextId;
	},

	getStyles(){
		return(
				{
					contentStyle:{
							width:'100%',
							height: 'calc(100vh - 58px)',
							position: 'relative',
							overflowY: 'hidden'
					},
					headerStyle:{
						width:'100%',
						height:'58px',
						lineHeight:'58px',
						paddingLeft:'24px',
						paddingRight:'24px'
					},
					footerStyle:{
						width:'100%',
						height:'58px',
						lineHeight:'58px',
						paddingLeft:'24px',
						paddingRight:'24px'
					},
					listStyle:{
					  minHeight: 'calc(100vh - 232px)'
					},
					placeholderStyle:{
						fontStyle: 'italic',
						color: '#AAAAAA'
					}
				}
		)

	},

	openCFPanel(){
		let {
			changedValues
		} = this.state;
		let self = this;
		let {
			guide
		} = changedValues;

		let seq = (changedValues.guide.length+1) * 100;
		let langCodesItem = NeedDetailStore.genLangCodesObj();

		let newGuide = {
			id: self.getNextId(guide),
			label: 'Guide',
			title: NeedDetailStore.genLangCodesObj(),
			seq: seq,
			image: 'ngi-'+self.getNextId(guide),
			content: NeedDetailStore.genLangCodesObj()
		};

		guide.push(newGuide);
		let onUpdate = function(){
			self.refs["nqi-"+guide[guide.length-1].id].onUpdate();
			//self.forceUpdate();
		};
		DynConfigPanelActions.openPanel(guide[guide.length-1], 'question', {onUpdate:onUpdate});
		this.onUpdate();
	},

	genItems(guide){
		let its = [];
		let self = this;
		let {
			sectionId
		} = this.state;
		for(let i in guide){
			let _guide = guide[i];
			its.push(
				<NeedsGuideItem key={"nag-"+_guide.id} ref={"nqi-"+_guide.id} index={Number(i)} changedValues={guide} sectionId={sectionId} onListUpdate={this.onUpdate}/>
			);
		}
		return its;
	},

	render: function() {

		var self = this;
		let styles = this.getStyles();

    let {
      values,
      changedValues,
			sectionId,
			isLock
    } = this.state;
		let items = this.genItems(changedValues?changedValues.guide:[]);

		let header = (
			<div key={'nag-header'} style={{height: '58px', display: 'block', overflow:'hidden'}}>
        <Toolbar key="nag-hToolbar" style={{padding:"0px", position:'relative', background: '#FFFFFF'}} >
          <ToolbarGroup key={'nag-hToolbar-left'} style={{display:'flex'}}>
            <ToolbarTitle text={getLocalizedText("MENU.GUIDE","Guide")} style={{color:'#000000', paddingLeft: '24px'}}/>
          </ToolbarGroup>
          <ToolbarGroup key={'nag-hToolbar-right'}  float="right" style={{display:'flex'}}>
						{isLock?null:<RaisedButton
			        key={'nag-btn-add-guide'}
			        style={{display:'inline-block', float: 'right', marginRight: '24px'}}
			        onClick={this.openCFPanel}
			        label={getLocalizedText("BUTTON.ADD_SECTION","ADD SECTION")}
			      />}
          </ToolbarGroup>
        </Toolbar>
				<hr/>
      </div>
		);


		return (
			<div key={"nag-content" + sectionId} style={styles.contentStyle}>
				<div style={{height:'calc(100vh - 58px)', overflowY: 'hidden'}}>
					{header}
					<Divider/>
					<div style={{height: 'calc(100vh - 116px)', overflowY: 'auto'}}>
						<SortableList
							id={"nagList"+sectionId}
							key={"nagList"+sectionId}
							disabled={isLock}
							items={items}
							style={styles.listStyle}
							values={values?values.guide:[]}
							changedValues={changedValues?changedValues.guide:[]}
							handleSort={this.onUpdate}/>
					</div>
				</div>

			</div>
		);
	},
});

module.exports = NeedsGuide;
