/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');

let ProfileStore = require('../../stores/ProfileStore.js');

let StylePropable = mui.Mixins.StylePropable;

let Colors = mui.Styles.Colors;
// let Toolbar = mui.Toolbar;
// let ToolbarGroup = mui.ToolbarGroup;
// let FontIcon = mui.FontIcon;
// let IconButton = mui.IconButton;

let CurrentColumn = require('../DynamicUI/DynViewOnlyColumn.jsx');
let ChangedColumn = require('../DynamicUI/DynEditableColumn.jsx');
let Toolbar = require('../DynamicUI/DynToolbar.jsx');

let FlatButton = mui.FlatButton;

let Profile = React.createClass({

  mixins: [React.LinkedStateMixin, StylePropable],

  getInitialState: function() {
    if ( !window.curProfile ) {
      window.curProfile = {
        firstname: "Sam",
        lastname: "Lam",
        gender: "m",
        dob: new Date('1988-01-12'),
        smoking: true,
        initial: "SL",
        residCountry: 'HK',
        othername: 'Sen',
        profilePic: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAkCAYAAABIdFAMAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAHhJREFUeNo8zjsOxCAMBFB/KEAUFFR0Cbng3nQPw68ArZdAlOZppPFIBhH5EAB8b+Tlt9MYQ6i1BuqFaq1CKSVcxZ2Acs6406KUgpt5/LCKuVgz5BDCSb13ZO99ZOdcZGvt4mJjzMVKqcha68iIePB86GAiOv8CDADlIUQBs7MD3wAAAABJRU5ErkJggg%3D%3D"
      };
      console.log(window.curProfile);
    }

    return {
      profile: window.curProfile,
      showEdit: false
    };
  },

  handleChange: function(newValue) {
    this.setState({ profile: newValue });
  },

  handleEdit: function() {
    this.setState( {showEdit: true} );
  },

  handleClose: function() {
    this.setState( {showEdit: false} );
  },

  handleUndo: function() {
    this.setState( {showEdit: false} );
  },

  handleDelete: function() {
    // this.setState( )
  },

  render: function() {
    var self = this;

    let containerStyle = {
      textAlign: 'left',
      display: 'flex'
    };

    let initial = 0;

    let template = {
      items: [{
        type: "section",
        id: 's0',
        items:[{
          type:"text",
          title: "First Name",
          mandatory: true,
          id:"firstname"
        }, {
          type:"text",
          title: "Last Name",
          id:"lastname"
        }]
      }, {
        type: "section",
        id: 's1',
        title: "Personal Details",
        items: [{
          type:"text",
          title: "Other Name",
          id:"othername"
        }, {
          type: "picker",
          title: "Gender",
          id: "gender",
          defaultValue: 'm',
          mandatory: true,
          options: [{
            id: 'm',
            title: 'Male'
          }, {
            id: 'f',
            title: 'Female'
          }]
        }, {
          type: "datepicker",
          title: "Date of Birth",
          id: "dob"
        }, {
          type: "switch",
          title: "Smoking Hobit",
          id: "smoking",
          options: [{
            id: true,
            title: 'Smoker',
          }, {
            id: false,
            title: 'Non-Smoker'
          }]
        }, {
          type: "picker",
          title: "Country of Residence",
          id: "residCountry",
          options: [{
            id: 'HK',
            title: 'Hong Kong',
          }, {
            id: 'MA',
            title: 'Macao',
          }, {
            id: 'CN',
            title: 'China'
          }]
        }]
      }]
    };

    var profile = this.state.profile;

    var toolBar = null;

    if (!this.state.showEdit) {
       toolBar = <Toolbar
         leftGroup={null}
         rightGroup={[ {
             id:'editBtn',
             type: 'iconButton',
             title: 'Edit',
             icon: 'edit',
             handle: self.handleEdit
           }, {
             id:'delBtn',
             type: 'iconButton',
             title: 'Delete',
             icon: 'delete',
             handle: self.handleDelete
           }
         ]} />
    } else {
      toolBar = <Toolbar
        leftGroup={[{
          id:'backBtn',
          title: 'Back',
          type: 'iconButton',
          icon: 'arrow_back',
          handle: self.handleClose
        }]}
        rightGroup={[ {
            id:'undoBtn',
            title: 'Undo',
            type: 'iconButton',
            icon: 'undo',
            handle: self.handleClose
          }, {
            id:'saveBtn',
            title: 'Save',
            type: 'iconButton',
            icon: 'save',
            handle: self.handleSave
          }
        ]} />
    }

    return (
      <div>
        {toolBar}
        <div style={containerStyle} className={this.state.showEdit?'Compact':'Regular'}>
          <CurrentColumn
            show = {true}
            values = {this.state.profile}
            template = {template}
            id = "current"
            ref = "current" />
          <ChangedColumn
            show = {this.state.showEdit}
            values = {this.state.profile}
            template = {template}
            id = "changed"
            ref = "ProfileEdit" />
        </div>
      </div>
    );
  }
});

module.exports = Profile;
