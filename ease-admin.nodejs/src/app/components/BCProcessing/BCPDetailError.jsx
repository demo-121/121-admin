/** Business Continuous Processing Detail Error page */
let React = require('react');
let mui = require('material-ui');


let DynList = require("../DynamicUI/DynList.jsx");

let BCProcessingActions = require('../../actions/BCProcessingActions.js');
let DynActions = require('../../actions/DynActions.js');
let MasterTableActions = require('../../actions/MasterTableActions.js');
let MenuActions = require('../../actions/MenuActions.js');
let ContentStore = require('../../stores/ContentStore.js');
let AppBarStore = require('../../stores/AppBarStore.js');
let MenuItem = mui.MenuItem;

let BCPDetailError = React.createClass({

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function () {
    ContentStore.removeChangeListener(this.onChange);
  },

  gettingContent: false,

  onChange() {
    this.gettingContent = false;
    this.setState({ pageContent: ContentStore.getPageContent() })
  },

  getInitialState() {
    var pageContent = ContentStore.getPageContent();
    var changedValues = pageContent ? pageContent.changedValues : null;
    return {
      selectedRow: -1,
      pageContent: pageContent,
      changedValues: changedValues,
      open: false
    };
  },

  getStyles() {
    return {
        subheaderstyle : {
            fontWeight: 'bold', 
            fontSize: 18,
        }
    }
  },

  render: function () {
  
    
    var pageContent = this.state.pageContent;
    var contentInStore = ContentStore.getPageContent();
    pageContent = contentInStore;
    if (!pageContent) return null;

    
    const styles = this.getStyles();
    const values = pageContent.changedValues;
      return (
        <div className="settings">
          <DynList
            list={values.list}
            subheaderStyle = {styles.subheaderstyle}
             />
        </div>
      );
    }
  
});

module.exports = BCPDetailError;
