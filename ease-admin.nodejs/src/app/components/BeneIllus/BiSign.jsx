/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let BeneIllusDetailStore = require('../../stores/BeneIllusDetailStore.js');
let ContentStore = require('../../stores/ContentStore.js');

let FlatButton = mui.FlatButton;
let Divider = mui.Divider;
let Tabs = mui.Tabs;
let Tab = mui.Tab;
let EABTextField = require('../CustomView/TextField.jsx');
let DialogActions = require('../../actions/DialogActions.js');

let pageIds = ['BiClientSign', 'BiAgentSign'];

let _items = [
  {id: 'page', type: 'TEXT', subType: 'number', title: getLocalizedText('PAGE_NUMER', 'Page Number'), detailSeq: 100},
  {id: 'signId', type: 'TEXT', title: getLocalizedText('SIGNATURE_ID', 'Signature ID'),  detailSeq: 200},
  {id: 'x', type: 'TEXT', subType: 'number', title: getLocalizedText('X_AXIS', 'x-axis'), detailSeq: 300},
  {id: 'y', type: 'TEXT', subType: 'number', title: getLocalizedText('Y_AXIS', 'y-axis'),  detailSeq: 400},
  {id: 'width', type: 'TEXT', subType: 'number', title: getLocalizedText('WIDTH', 'width'), detailSeq: 500},
  {id: 'height', type: 'TEXT', subType: 'number', title: getLocalizedText('HEIGHT', 'height'),  detailSeq: 600}
];

let BiSign = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  componentDidMount() {
    ContentStore.addChangeListener(this.onContentChange);
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onContentChange);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    return true;
  },

  onPageChange(){
  },


  onContentChange() {
    var content = ContentStore.getPageContent();

    var {
      template,
      values,
      changedValues
    } = content;

    if(!changedValues){
      changedValues = content.changedValues = cloneObject(values);
    }

    var {
      page
    }=this.state;

    var newPageId = changedValues.selectPage.id;
    var newState = {
      template:template,
      values: values,
      changedValues: changedValues,
      page: newPageId
    };

    if(page!=newPageId){
      newState.tabIndex = 0;
      if(!values[newPageId]){
        values[newPageId]={signFields:[]};
      }
      newState.values = values;
      newState.changedValues = content.changedValues = cloneObject(values);
    }

    this.setState(newState);
  },

  getInitialState() {
    var {
      template,
      values
    } = ContentStore.getPageContent();
    var page = values.selectPage.id;
    if(!values[page] && pageIds.indexOf(page)>=0){
      values[page]={signFields:[]};
    }
    var changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);

    ContentStore.getPageContent().valuesBackup = values;

    var isLock = BeneIllusDetailStore.isLock();
    return {
      template: template,
      values: values,
      changedValues: changedValues,
      page: page,
      isLock: isLock,
      tabIndex: 0
    };
  },


  handleChangeTabs(value){
    this.setState({tabIndex: value});
  },

  _genTabs(){
    var {
      page,
      changedValues
    } = this.state;

    var section = changedValues[page] || {};
    var signFields = section.signFields || [];
    var items = [];
    for(var i=0; i<signFields.length; i++){
      items.push(<Tab label={<span style={{paddingLeft: '12px', paddingRight: '12px'}}>{getLocalizedText('SIGNATURE', 'SIGNATURE').toUpperCase() + " " + (Number(i)+1)}</span>} value={i}/>);
    }
    return items;
  },

  addPage(){
    var {
      page,
      changedValues
    } = this.state;

    var {
      signFields
    } = changedValues[page];
    signFields.push({});
    this.setState({tabIndex: signFields.length-1});
  },

  _genItems(){
    var self = this;
    var {
      page,
      tabIndex,
      changedValues,
      isLock
    } = this.state;

    if(pageIds.indexOf(page)<0 || (changedValues[page] && !changedValues[page]['signFields'].length)){
      return (
        <div key= {"hbox"+tabIndex} style={{width: '100%', height: 'calc(100% - 58px)', display: 'table'}}>
            <div style={{textAlign: 'center', verticalAlign: 'middle', display: 'table-cell'}}>
              There is no values in this page.
            </div>
        </div>
      );
    }
    var items = [];

    for (var i in _items){
      items.push(
        <EABTextField
          ref = {_items[i].id}
          key = {"ctn_"+ _items[i].id}
          template = { self.mergeAndPrefix(_items[i], {disabled: isLock}) }
          changedValues = { changedValues[page]['signFields'][tabIndex] }
          />
      )
    }

    var removeSign = function(e){
      let removeDialog = {
        id: "delete",
        title: getLocalizedText("DELETE","DELETE"),
        message: getLocalizedText("BI_SIGN_DEL_MSG", "Are you confirm delete this signature?"),
        negative: {
          id: "delete_cancel",
          title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
          type: "button"
        },
        positive:{
          id: "delete_confirm",
          title: getLocalizedText("BUTTON.OK","OK"),
          type: "customButton",
          handleTap: function(){
            changedValues[page]['signFields'].splice(tabIndex,1);
            if(tabIndex == changedValues[page]['signFields'].length && tabIndex>0){
              self.state.tabIndex = tabIndex-1;
            }
            self.forceUpdate();
          }
        }
      }

      DialogActions.showDialog(removeDialog);
    };

    return (
      <div>
        <div key= {"hbox"+tabIndex} className={"hBoxContainer column2"} style={{paddingTop: '24px'}}>
          {items}
        </div>
        {isLock? null:<FlatButton label={<span style={{color: '#FF0000'}}>{getLocalizedText("BUTTON.DELETE", "DELETE")}</span>} onClick={removeSign}/>}
      </div>
    )
  },

  render: function() {
    var self = this;
    var {
      tabIndex,
      values,
      changedValues,
      isLock,
      page
    } = this.state;

    let dataContainerStyle = {
      width:'100%',
      height:"calc(100vh - 58px)",
      overflowY:"auto",
    };

    return (
      <div>
        <div>
          <div style={{width: 'calc(100% - 100px)', display: 'inline-block'}}>
            <div style={{display: 'table'}}>
              <div style={{display: 'table-cell'}}>
                <Tabs onChange={this.handleChangeTabs} value={tabIndex||0}>
                  {this._genTabs()}
                </Tabs>
              </div>
            </div>
          </div>
          {isLock?null:<FlatButton style={{width: '100px', display: 'inline-block', verticalAlign: 'text-bottom'}} label="Add" onClick={this.addPage}/>}
        </div>
        <div>
          {this._genItems()}
        </div>
      </div>
    );
  }
});

module.exports = BiSign;
