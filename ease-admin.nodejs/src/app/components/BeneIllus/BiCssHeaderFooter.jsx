/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let BeneIllusDetailStore = require('../../stores/BeneIllusDetailStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let HtmlEditor = require('../CustomView/HtmlEditor.jsx');
let TextArea = require('../CustomView/TextArea.jsx');


let FlatButton = mui.FlatButton;
let Divider = mui.Divider;
let Tabs = mui.Tabs;
let Tab = mui.Tab;
let sectionId = 'BiCssHeaderFooter';

let CssEditor = React.createClass({
  beauty(){
    var {
      changedValues
    } = this.props;
    var source = changedValues[sectionId]['style'];
    var output = '';
    var opts = {};

    opts.indent_char = opts.indent_size == 1 ? '\t' : ' ';
    opts.preserve_newlines = opts.max_preserve_newlines !== "-1";

    changedValues[sectionId]['style'] = css_beautify(source, opts);
    this.forceUpdate();
  },

  render(){
    var self = this;
    var {
      changedValues,
       isLock
    } = this.props;
    var cValue=changedValues && checkExist(changedValues, sectionId+'.style')?changedValues[sectionId]['style']: '';
    return(
      <div style={{padding: '24px'}}>
        <div style={{textAlign: 'right', height: '36px'}}>
          {isLock?null:<FlatButton label="beautify" onClick={this.beauty}/>}
        </div>
        <TextArea
          template={{id: 'style', type: 'TEXT', disabled: isLock}}
          changedValues={changedValues[sectionId]}/>
      </div>
    )
  }
});

let BiCssHeaderFooter = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  componentDidMount() {
    ContentStore.addChangeListener(this.onContentChange);
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onContentChange);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    return true;
  },

  onPageChange(){
  },


  onContentChange() {
    var content = ContentStore.getPageContent();

    var {
      template,
      values,
      changedValues
    } = content;

    if(!changedValues){
      changedValues = content.changedValues = cloneObject(values);
    }

    this.setState({
      template:template,
      values: values,
      changedValues: changedValues,
    })
  },

  getInitialState() {
    var {
      template,
      values
    } = ContentStore.getPageContent();

    var isLock = BeneIllusDetailStore.isLock();

    //initial values
    if(!checkExist(values, sectionId)){
      values[sectionId]={};
    }
    var currPage = BeneIllusDetailStore.getCurrentPage();
    var changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);

    ContentStore.getPageContent().valuesBackup = values;
    return {
      template: template,
      values: values,
      changedValues: changedValues,
      isLock: isLock,
      tabIndex: 0
    };
  },


  handleChangeTabs(value){
    this.setState({tabIndex: value});
  },

  getHtmlEditor(type){
    var {
      isLock,
      changedValues
    } = this.state;

    return (
      <HtmlEditor
        key={type + 'Editor'}
        ref={type + 'Editor'}
        template={{id:type, type:'TEXT', disabled:isLock}}
        changedValues={changedValues[sectionId]}/>
    );
  },

  render: function() {
    var self = this;
    var {
      tabIndex,
      isLock,
      changedValues
    } = this.state;

    let dataContainerStyle = {
      width:'100%',
      height:"calc(100vh - 58px)",
      overflowY:"auto",
    };

    return (
      <div>
        <div style={{display: 'table'}}>
          <div style={{display: 'table-cell'}}>
            <Tabs onChange={this.handleChangeTabs} value={tabIndex||0}>
              <Tab label={<span style={{paddingLeft: '12px', paddingRight: '12px'}}>{getLocalizedText('CSS', 'CSS').toUpperCase()}</span>} value={0}/>
              <Tab label={<span style={{paddingLeft: '12px', paddingRight: '12px'}}>{getLocalizedText('HEADER', 'HEADER').toUpperCase()}</span>} value={1}/>
              <Tab label={<span style={{paddingLeft: '12px', paddingRight: '12px'}}>{getLocalizedText('FOOTER', 'FOOTER').toUpperCase()}</span>} value={2}/>
            </Tabs>
          </div>
        </div>
        <Divider/>
        {
          tabIndex==0?<CssEditor key="CssEditor" changedValues={changedValues} isLock={isLock}/>:
          tabIndex==1?this.getHtmlEditor('header'):
          this.getHtmlEditor('footer')
        }
      </div>
    );
  }
});

module.exports = BiCssHeaderFooter;
