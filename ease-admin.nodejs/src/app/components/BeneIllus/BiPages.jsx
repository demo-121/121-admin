/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let BeneIllusDetailStore = require('../../stores/BeneIllusDetailStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let HtmlEditor = require('../CustomView/HtmlEditor.jsx');
var AppBarActions = require('../../actions/AppBarActions.js');
let DialogActions = require('../../actions/DialogActions.js');
import appTheme from '../../theme/appBaseTheme.js';

let FlatButton = mui.FlatButton;
let RaisedButton = mui.RaisedButton;
let Divider = mui.Divider;
let Tabs = mui.Tabs;
let Tab = mui.Tab;
let sectionId = 'BiPages';
let cssId = 'BiCssHeaderFooter';
let arrayId = 'pages';

let BiPages = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  componentDidMount() {
    ContentStore.addChangeListener(this.onContentChange);
    ContentStore.addChangeListener(this.onPageChange);
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onContentChange);
    ContentStore.removeChangeListener(this.onPageChange);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    return true;
  },

  onPageChange(){
    var content = ContentStore.getPageContent();

    var {
      template,
      values,
      changedValues
    } = content;

    if(!changedValues){
      changedValues = content.changedValues = cloneObject(values);
    }

    this.setState({
      template:template,
      values: values,
      changedValues: changedValues,
    })
  },

  onContentChange() {
    var content = ContentStore.getPageContent();

    var {
      template,
      values,
      changedValues
    } = content;

    if(!changedValues){
      changedValues = content.changedValues = cloneObject(values);
    }

    this.setState({
      template:template,
      values: values,
      changedValues: changedValues,
    })
  },

  getInitialState() {
    var {
      template,
      values
    } = ContentStore.getPageContent();

    var isLock = BeneIllusDetailStore.isLock();

    //initial values
    if(!checkExist(values, sectionId)){
      values[sectionId]={};
    }
    var currPage = BeneIllusDetailStore.getCurrentPage();
    var changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);

    ContentStore.getPageContent().valuesBackup = values;
    return {
      template: template,
      values: values,
      changedValues: changedValues,
      isLock: isLock,
      tabIndex: 0,
    };
  },


  handleChangeTabs(value){
    this.setState({tabIndex: value});
  },

  deletePage(){
    var self = this;
    let removeDialog = {
      id: "delete",
      title: getLocalizedText("DELETE","DELETE"),
      message: getLocalizedText("BI_PAGE_DEL_MSG", "Are you confirm delete this page?"),
      negative: {
        id: "delete_cancel",
        title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
        type: "button"
      },
      positive:{
        id: "delete_confirm",
        title: getLocalizedText("BUTTON.OK","OK"),
        type: "customButton",
        handleTap: function(){
          let {
            changedValues,
          } = self.state;
          let biPages = changedValues[sectionId];
          let {
            pages,
            selectedPage,
          } = biPages ;

           if(selectedPage > -1 && pages[selectedPage]){
               pages.splice(selectedPage, 1);
               selectedPage = Math.min(selectedPage, pages.length-1);
               biPages.selectedPage=selectedPage;
               AppBarActions.submitChangedValues('/BeneIllus/Detail/Save');
               self.forceUpdate();

           }
        }
      }
    }

    DialogActions.showDialog(removeDialog);


  },



  render: function() {
    var self = this;
    var {
      tabIndex,
      isLock,
      changedValues,
    } = this.state;
    let selectedPage=changedValues[sectionId].selectedPage?changedValues[sectionId].selectedPage:0;
    let dataContainerStyle = {
      width:'100%',
      height:"calc(100vh - 58px)",
      overflowY:"auto",
    };

    return (
      <div>
        {
          changedValues[sectionId][arrayId].length>0?
          <div>
            <div style={{width:'100%', height: 64, fontSize:24, borderBottom:'2px solid  rgba(0, 0, 0, 0.1)' }}>
              <div style={{float:'left ',width:'80%', textAlign: 'left', margin:20}}>{"Page " +  (selectedPage+1)} </div>
              {isLock?null:<RaisedButton   disabled={isLock} labelStyle={{color: 'red'}} style={{float:'right', margin:12}} key="bipage-delPag" label="Delete" onClick={this.deletePage}/>}
            </div>
            <div>
              <HtmlEditor
                key="pageEditor"
                ref="pageEditor"
                template={{id:'page', type:'TEXT', disabled:isLock}}
                changedValues={changedValues[sectionId][arrayId][selectedPage]}/>
            </div>
          </div>:
          null
        }
      </div>
    );
  }
});

module.exports = BiPages;
