let React = require('react');
let mui = require('material-ui');

let Checkbox = mui.Checkbox;

import EABFieldBase from './EABFieldBase.js';

let CheckboxGroup = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    checkBoxLabelStyle: {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      width: 'calc(100% - 40px)',
      fontSize: '16px'
    }
  },

  getCheckBoxValues: function(cbValues){
    var str = "";
    if (cbValues && cbValues.length) {
      for (let i = 0; i < cbValues.length; i++){
        if (cbValues[i]) {
          str += (str?",":"") + cbValues[i];
        }
      }
    }
    return str;
  },

  render: function() {
    var self = this;
    var {
      template,
      changedValues,
      handleChange,
      disabled,
      ...others
    } = this.props;

    var {
      id,
      title,
      value,
      subType,
      placeholder,
      presentation
    } = template;

    var {
      options
    } = this.state;

    var cValue = self.getValue("");

    var valueArr = [];
    if (cValue && typeof cValue == 'string') {
      valueArr = cValue.split(",");
    }

    var cbGroup = [];

    console.debug('using checkbox group!', subType);

    if (subType && 'WITH_ALL' == subType.toUpperCase()) {
      cbGroup.push(<Checkbox
        key={ 'ALL' }
        className = {"CheckBox"}
        style = {{width: null, display:'inline-block'}}
        label = { getLocalizedText("OPTION.ALL") }
        labelStyle = {this.styles.checkBoxLabelStyle}
        checked = {valueArr.indexOf('*') > -1}
        onCheck = {
          function() {
            // var cbValues = cValue.split(",");
            var index = valueArr.indexOf('*');
            if (index > -1) {
              valueArr = [];
            } else {
              valueArr = ['*'];
            }
            self.requestChange(self.getCheckBoxValues(valueArr));
          }.bind()
        }
        />);
    }

    for (let i = 0; i < options.length; i++) {
      var option = options[i];

      if (optionSkipTrigger(template, option, changedValues, this.props.resetValueItems)) {
        continue;
      }
      var otitle = getLocalText("en", option.title);

      cbGroup.push(<Checkbox
        key={ option.value }
        className = {"CheckBox"}
        style = {{width: null, display:'inline-block'}}
        label = {otitle}
        disabled = {disabled}
        labelStyle = {this.styles.checkBoxLabelStyle}
        checked = {valueArr.indexOf(option.value) > -1}
        onCheck = {
          function() {
            // var cbValues = cValue.split(",");
            var index = valueArr.indexOf(this.value);
            if (index > -1) {
              valueArr.splice(index, 1);
            } else {
              valueArr.push(this.value);
            }
            var all = valueArr.indexOf('*')
            if (all > -1) {
              valueArr.splice(all, 1);
            }
            self.requestChange(self.getCheckBoxValues(valueArr));
          }.bind({ value: option.value })
        }/>);
    }

    if (cbGroup.length == 0) {
      cbGroup.push(<label key={"placeholder"} className="CheckBox">{placeholder?placeholder:"No Option"}</label>)
    }

    var errorMsg = validate(template, changedValues);
    errorMsg = errorMsg?<div className="ErrorLabel" style={{
      color: this.state.muiTheme.textField.errorColor,
      height: '16px',
      fontSize: '12px',
      position: 'absolute',
      left: '24px',
      top: null,
      bottom: '4px'
    }}>{errorMsg}</div>:null;

    return (<div
        className={"CheckboxGroup"+ (presentation?" "+presentation:"") + this.checkDiff()}>
        {this.getFloatingTitle()}
        {errorMsg}
        {cbGroup}
    </div>);
  }
});
module.exports = CheckboxGroup;
