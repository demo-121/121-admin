let React = require('react');
let ReactDom = require('react-dom');
let mui = require('material-ui');
let CircularProgress = mui.CircularProgress;
let StylePropable = mui.Mixins.StylePropable;
var OverLay = mui.Overlay;

let LoadingStore = require('../../stores/LoadingStore.js');
let WindowListenable = mui.Mixins.WindowListenable;

import appTheme from '../../theme/appBaseTheme.js';

let LoadingBlock = React.createClass({
  mixins: [StylePropable,WindowListenable],

  getInitialState() {
    return {
      open: false,
      muiTheme: appTheme.getTheme()
    };
  },

  componentDidMount() {
    LoadingStore.addChangeListener(this.onChange);
    this._position();
  },

  componentDidUpdate: function componentDidUpdate() {
    this._position();
  },

  componentWillUnmount() {
    LoadingStore.removeChangeListener(this.onChange);
  },

  onChange() {
    this.setState({open: LoadingStore.getState()});
  },

  windowListeners: {
    resize: '_handleResize'
  },

  _handleResize: function _handleResize() {
    if (this.state.open) {
      this._position();
    }
  },

  _position() {
      var clientHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
      var container = ReactDom.findDOMNode(this);
      var loader = ReactDom.findDOMNode(this.refs.loader);
      var minPaddingTop = 16;
      var loaderHeight = loader.offsetHeight;
      var paddingTop = (clientHeight - loaderHeight) / 2;
      if (paddingTop < minPaddingTop) paddingTop = minPaddingTop;

      //Vertically center the dialog window, but make sure it doesn't
      //transition to that position.
      if (!container.style.paddingTop) {
        container.style.paddingTop = paddingTop + 'px';
      }
  },

  getStyles() {
    var muiTheme = this.state.muiTheme;
    return {
      contentStyle: {
        zIndex: muiTheme.zIndex.dialog + 100,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        textAlign: 'center'
      },
      overlayStyle: {
        zIndex: muiTheme.zIndex.dialogOverlay + 100,
        backgroundColor: "rgba(255,255,255,0)"
      }
    }
  },

  render() {
    var {
      open
    } = this.state;
    var {
      contentStyle,
      overlayStyle
    } = this.getStyles()

    return (
      <div key="LoadingBlock" style={ this.mergeStyles(contentStyle, {display: open?'block':'none'} )}>
        <CircularProgress key="loader" ref="loader"/>
        <OverLay
          show={open}
          style={overlayStyle} />
      </div>
    );
  }
});

module.exports = LoadingBlock;
