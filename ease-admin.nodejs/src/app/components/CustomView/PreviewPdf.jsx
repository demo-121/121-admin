let React = require('react');
let mui = require('material-ui');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');
let PreviewPdfStore = require('../../stores/PreviewPdfStore.js');
let MenuStore = require('../../stores/MenuStore.js');
let StylePropable = mui.Mixins.StylePropable;
import appTheme from '../../theme/appBaseTheme.js';
let Transitions = mui.Styles.Transitions;

let PreviewPageActions = require('../../actions/PreviewPageActions.js');

let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let ToolbarSeparator = mui.ToolbarSeparator;
let FontIcon = mui.FontIcon

let PreviewPdf = React.createClass({
  mixins: [StylePropable],

  getStyles() {
    return {
      contentStyle: {
        color: appTheme.palette.alternateTextColor,
        top: '0px'
      },
      previewPageStyle :{
        backgroundColor: "#FFFFFF",
        position: "absolute",
        zIndex: '200',
        width: '100%'
      },
      iconStyle: {
        padding: '0px 12px',
        margin: '4px',
        lineHeight: '48px'
      },
      flatButtonStyle:{
        padding: '10px',
        margin: '0 20px 0 0',
        minWidth: '100px',
        color: appTheme.palette.alternateTextColor
      },
      pickerStyle: {
        width: 'auto',
        height: '56px',
      },
      actionsPaneStyle: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%'
      },
      seperatorStyle: {
        float:null,
        lineHeight:'56px',
        fontSize:'30px',
        color: appTheme.palette.alternateTextColor
      },
      labelStyle: {
        paddingLeft: '3px',
        lineHeight: '56px',
        fontSize: '20px',
        fontWeight: 'normal',
        display: 'inline-block',
        position: 'relative',
        float: null,
        color: appTheme.palette.alternateTextColor
      },
      buttonStyle: {
        fontSize: '20px',
        fontWeight: 'normal',
        textTransform: 'none',
        color: appTheme.palette.alternateTextColor,
        float: null,
        padding:0,
        margin:0
      }
    }
  },

  componentDidMount() {
    PreviewPdfStore.addChangeListener(this.onChange);
  },

  componentWillUnmount() {
    PreviewPdfStore.removeChangeListener(this.onChange);
  },

  onChange() {
    var html = PreviewPdfStore.getPreviewHtml();
    this.setState({
      html: html
    })
  },

  getInitialState() {
    return {
    };
  },

  back(){
    PreviewPageActions.back();
  },


  render: function() {
    var self = this;
    var windowHeight = window.innerHeight;
    var styles = self.getStyles();
    var {html} = this.state;

    var {style} = this.props;
    var menuState = MenuStore.getMenuState();

    var content = html?
    (<div key="previewPdf" style={this.mergeAndPrefix(this.props.style, styles.previewPageStyle)}>
      <Toolbar style={{padding:"0px", boxShadow:"0px 2px 2px rgba(0,0,0,.33)", position:'relative'}} >
        <ToolbarGroup style={{ width: 'calc(100% - 300px)', display:'flex'}}>
          <FontIcon
            key="backBtn"
            tooltip="Back"
            style= { styles.iconStyle }
            className="material-icons"
            onClick={this.back}>
            arrow_back
          </FontIcon>
          <ToolbarTitle text={"Preview"} />
        </ToolbarGroup>
      </Toolbar>
      <div style={{width: menuState?'calc(100% - 280px)':'100%', height: 'calc(100vh - 56px)', overflowY: 'auto', background: '#DDDDDD'}}>
        <div style={{width: '744px', margin: 'auto', background: '#FFFFFF'}} dangerouslySetInnerHTML={{__html: html}}/>
      </div>
    </div>):null;

      return (
        <ReactCSSTransitionGroup transitionName="assignPage" transitionEnterTimeout={500} transitionLeaveTimeout={500}>
          {content}
        </ReactCSSTransitionGroup>

      )

  }
});

module.exports = PreviewPdf;
