let React = require('react');
let mui = require('material-ui');

let FontIcon = mui.FontIcon;
let Slider = mui.Slider;

import EABFieldBase from './EABFieldBase.js';
import appTheme from '../../theme/appBaseTheme.js';

let DropZone = require('./DropZone.jsx');
let ContentStore = require('../../stores/ContentStore.js');

module.exports = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    titleStyle:{
      color: '#2696CC',
      fontSize: '20px'
    },
    sliderStyle:{
      display: 'inline-block',
      width:'70%',
      paddingLeft: '24px',
      paddingRight: '24px',
    },
    iconStyle:{
      marginTop: '80px',
      height: 'auto',
      maxHeight: '28px',
      width: 'auto',
      maxWidth: '50px',
      cursor: 'pointer'
    }
  },

  getInitialState: function() {
    let {
      id
    } = this.props;

    let cValue = ContentStore.getPageContent().changedValues;
    if(!cValue.Attachments){
      cValue.Attachments = {};
    }


    return {
      id: id,
      attachments: cValue.Attachments
    }
  },


  componentWillReceiveProps(nextProps){
    let {
      id
    } = nextProps;

    let cValue = ContentStore.getPageContent().changedValues;
    if(!cValue.Attachments){
      cValue.Attachments = {};
    }

    this.setState({
      id: id,
      attachments: cValue.Attachments
    });
  },

  updateImg(newFile){
    let {
      changedValues,
      attachments
    } = this.state;

    if(!attachments){
      attachments = this.state.attachments = {};
    }

    changedValues[newFile.type] = newFile.id;
    attachments[newFile.id]=newFile.url;
    this.forceUpdate();
  },

  removeAttachment(id){
    var {
      attachments
    } = this.state;

    if(attachments){
      attachments[id] = undefined
    }

    this.forceUpdate();
  },

  getIcon(type){
    var {
      id,
      changedValues,
      attachments
    } = this.state;

    var coins = attachments[changedValues[type]];
    return (
      <DropZone
        onDrop={function(res) {
          var invalid = [];
          //File type checking
          var actualFileType = res.file.type;
          if (actualFileType == 'image/png' || actualFileType == 'image/jpeg') {
            if ( res.file.size > 2*1048576) {
               invalid.push(getLocalizedText("drop_zone.exceed", "exceed ") + "2 MB.");
            }
          }else{
              invalid.push(getLocalizedText("drop_zone.invalid_type", "invalid type"));
          }
          if (invalid.length == 0) {
            this.updateImg({
              id: getUniqueId(),
              type: this.type,
              url: res.imageUrl,
            });
          } else {
            var errorMsg = "";
            for (var i = 0; i < invalid.length; i++) {
              errorMsg += invalid[i] + "\n";
            }
            alert(errorMsg, "Import failed");
          }
        }.bind({updateImg: this.updateImg, type: type})}
        className="dropzone"
        style={{verticalAlign: 'middle', cursor: 'pointer'}}>
          <img style={this.styles.iconStyle} src={coins} />
      </DropZone>
    )

  },

  render() {
    var self = this;
    var {
      disabled,
      ...others
    } = this.props;

    var {
      changedValues,
      tempValue,
    } = this.state;

    var {
      maxValue,
      minValue,
      defaultValue,
      format,
      interval,
    } = changedValues;

    let cValue = tempValue||defaultValue||minValue||0;
    let oneCoin = this.getIcon('leftImage');
    let threeCoins = this.getIcon('rightImage');
    //<img style={this.styles.iconStyle} src="web/img/one_coin.png" />
    //<img style={this.styles.iconStyle} src="web/img/three_coins.png"/>
    return (
      <div style={{width:'100%', textAlign: 'center'}}>
        {oneCoin}
        <div style={this.styles.sliderStyle}>
          <div style={this.styles.titleStyle}>
            {format=='currency'?'$'+ cValue.toLocaleString() :cValue}
          </div>
          <Slider
            defaultValue={Number(defaultValue||minValue||0)}
            name="normalSlider"
            min={minValue?Number(minValue):0}
            step={Number(interval||1)}
            max={Number(maxValue||100)}
            value={cValue}
            disableFocusRipple={true}
            onChange={function(e, value){
              self.state.tempValue = value;
              self.forceUpdate();
            }}
            />
        </div>
        {threeCoins}
      </div>
    )
  }
});
