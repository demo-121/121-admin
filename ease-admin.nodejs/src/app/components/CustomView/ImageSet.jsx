let React = require('react');
let mui = require('material-ui');
let RaisedButton = mui.RaisedButton;
let ContentStore = require('../../stores/ContentStore.js');
let Dropzone = require('../CustomView/DropZone.jsx');
//let DynActions = require('../../actions/DynActions.js');
let PDF = require('react-pdf');

let ImageSet = React.createClass({
  propTypes: {
    parent: React.PropTypes.string,
    id:  React.PropTypes.string,
    type: React.PropTypes.string,
    title: React.PropTypes.string,
    desc: React.PropTypes.string,
    button: React.PropTypes.string,
  },

  getInitialState: function() {
    var changedValues = ContentStore.getPageContent().changedValues;
    var id = this.props.id;
    var type = this.props.type;
    var parent = this.props.parent;
    var data = changedValues[parent][id][type];
    return {
      changedValues: changedValues,
      data: data,
    }
  },


  componentDidMount() {
    ContentStore.addChangeListener(this.onValuesChange);
  },

  componentWillUnmount() {
    ContentStore.removeChangeListener(this.onValuesChange);
  },

  onValuesChange() {
    var content = ContentStore.getPageContent();
    var changedValues = content.changedValues;
    if(!checkChanges(changedValues, this.state.changedValues)){
      this.state.changedValues = changedValues;

    }
  },

  updateImg(newFile) {

    var id = this.props.id;
    var type = this.props.type;
    var parent = this.props.parent;
    var newValues = ContentStore.getPageContent().changedValues;
    newValues[parent][id][type] = newFile.url;
    //DynActions.changeValues(newValues);
    this.setState({data:newFile.url})
  },

  removeFile() {

    var id = this.props.id;
    var type = this.props.type;
    var parent = this.props.parent;
    var newValues = ContentStore.getPageContent().changedValues;
    newValues[parent][id][type] = "";
    //DynActions.changeValues(newValues);

    this.setState({data:""})
  },

  getStyle() {
    var style = {
      'buttonBgColor':'#FAFAFA',
      'buttonGreen':'#2DB154',
      'buttonRed':'#F1453D',
    }
    return style;
  },

  render: function() {
    var {
      id,
      parent,
      title,
      desc,
      button,
      buttonAdd,
      disabled,
      type,
      require
    } = this.props;

    //var changedValues = ContentStore.getPageContent().changedValues;
    var changedValues = this.state.changedValues;
    var data = this.state.data;
    /*
    if (changedValues[parent] && changedValues[parent][id] && changedValues[parent][id][type]) {
      data = changedValues[parent][id][type];
    }
*/
    //console.debug("[ImageSet] - [render] - data", data);
    var content = null;

    if (data && data != '') {
      var buttonOb = (disabled)?<div style={{height:"24px"}}/>:(
        <RaisedButton
          label={button}
          style={{marginTop:'15px', marginBottom:'15px'}}
          backgroundColor = {this.getStyle().buttonBgColor}
          labelColor = {this.getStyle().buttonRed}
          onClick={this.removeFile} />
      );

      var actualType = data.substring(data.search(":") + 1, data.search(";"));

      if (!actualType || actualType.indexOf('pdf') < 0) {
        content = (
          <div className="bncItem">
            <div className="bncItem">
              <img className="productCoverImg"
                key={id + "img"}
                src={data}
                onClick={function() {
                  window.open(this);
                }.bind(data)} />
            </div>
            {buttonOb}
          </div>
        );
      } else {
        var encodedStr = data.substring(data.search(",") + 1, data.length);
        content = (
          <div className="bncItem">
            <div className="bncItem">
              <PDF content={encodedStr}
                page={1}
                scale={0.4}
                loading={(<span>Loading...</span>)}
                onClick={function(pages) {
                  var blob = b64toBlob(this.b64, this.actualType);
                  var blobUrl = URL.createObjectURL(blob);
                  window.open(blobUrl);
                }.bind({b64: encodedStr, actualType: actualType})} />
            </div>
            {buttonOb}
          </div>
        );
      }
    } else {
      content = (
        <Dropzone
          onDrop={function(res) {
            if (this.require) {
              var requireWidth = this.require.width;
              var requireHeight = this.require.height;
            }
            var invalid = [];

            var requireFiletype=this.require.fileType;
            //File type checking
            var actualFileType = res.file.type;
            if (actualFileType == requireFiletype) {
              var image = new Image();
              image.src = res.imageUrl;
              var width = image.width;
              var height = image.height;
              // Width & Height checking
              if ((requireWidth || requireHeight) && res.imageUrl && res.file.type.indexOf('image') != -1) {
                if ((width && width != requireWidth) || (height && height != requireHeight)) {
                  invalid.push("Your image is " + width + " x " + height + ". Required " + requireWidth + " x " + requireHeight + ".");
                }
              }
              // File size checking
              if ( res.file.size >this.require.sizeLimit) {
                 invalid.push("Your file size exceed " + this.require.sizeLimit/(1024*1024) + " MB.");
              }

            }else{
                invalid.push("Your file is not " + requireFiletype.substring(requireFiletype.indexOf('/')+1, requireFiletype.length).toUpperCase()+ " type.");
            }
            if (invalid.length == 0) {
              this.updateImg({
                name: res.file.name,
                size: res.file.size,
                altText: '',
                caption: '',
                file: res.file,
                url: res.imageUrl,
              });
            } else {
              var errorMsg = "";
              for (var i = 0; i < invalid.length; i++) {
                errorMsg += invalid[i] + "\n";
              }
              alert(errorMsg, "Import failed");
            }
          }.bind({require: require, updateImg: this.updateImg})}
          className="dropzone"
          accept={type == "cover" ? "image/*" : "application/pdf"}>
          {disabled?<div style={{height:"24px"}}/>:<RaisedButton
              label={buttonAdd}
              style={{marginTop:'15px', marginBottom:'15px'}}
              backgroundColor = {this.getStyle().buttonBgColor}
              labelColor = {this.getStyle().buttonGreen} />}
        </Dropzone>
      );
    }

    return (
      <div className="PageContent">
        <div className="DetailsItem"><h2>{title}</h2></div>
        <div className="DetailsItem">{desc}</div>
        <div className="DetailsItem">{content}</div>
      </div>
    );
  },

});

module.exports = ImageSet;
