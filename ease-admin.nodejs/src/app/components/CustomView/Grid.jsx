let React = require('react');
let mui = require('material-ui');

let FlatButton = mui.FlatButton;
let SelectField = mui.SelectField;
let MenuItem  = mui.MenuItem ;

let Dropzone = require('../CustomView/DropZone.jsx');
let HandsonTable = require('../CustomView/HandsonTable.jsx');

let ContentStore = require('../../stores/ContentStore.js');

import EABFieldBase from './EABFieldBase.js';
import appTheme from '../../theme/appBaseTheme.js';

let EABTextField = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    buttonStyle: {
      marginLeft: 16,
      color: appTheme.palette.primary2Color,
      backgroundColor:appTheme.palette.background2Color,
      borderWidth: 1,
      borderStyle: 'solid',
      borderColor: appTheme.palette.borderColor
    }
  },

  validation() {
    return this.refs[this.props.template.id].validation();
  },

  render() {
    var self = this;
    var {
      template,
      width,
      handleChange,
      disabled,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      type,
      insertable,
      value,
    } = template;

    var cValue = self.getValue("");


    if(id=="fund"){
      let cv=ContentStore.getPageContent().changedValues;
      let selFund=cv.fundList.selectedFund;
      if( selFund ){
        let fundList=cv["fundList_"+selFund];
        if(fundList && fundList.fund){
          cValue=fundList.fund;
        }
      }
    }

    // Import
    var import_ = (disabled)?null:(
      <Dropzone
        key= {"dz"+id}
        onDrop={
          function(res) {
            // console.debug("import res", res);
            var content = res.imageUrl;
            // console.debug("import content", content);
            var encodedStr = content.substring(content.search(",") + 1, content.length);
            // console.debug("import encodedStr", encodedStr);
            if (encodedStr.length > 0) {
              var decodedStr = window.atob(encodedStr);
              var _csvStr = decodedStr.replace(/(\r\n)/gm, "\n");
              var csvStr = _csvStr.replace(/(\r)/gm, "\n");
              // console.debug("import csvStr", csvStr);
              var data = $.csv.toObjects(csvStr);
              // console.debug("import data", data);
              if (data.length > 0) {
                var grid = self.refs[this.id];
                var hot = grid.hot;

                data = grid.trimData(data, true);

                if (data) {
                  hot.loadData(data);

                  // Validation
                  grid.isValidateCells = true;
                  hot.validateCells(
                    function(valid) {
                      this.isValidateCells = false
                      if (valid) {
                        // Save
                        if (this.props.onChange) {
                          this.props.onChange(this.getData());
                        }
                      }
                    }.bind(grid)
                  );
                }
              } else {

              }
            }
          }.bind(template)
        }
        accept=".csv" >
        <FlatButton key= {"impBtn"+id} style={this.styles.buttonStyle} label={getLocalizedText("HANDSONTABLE_BUTTON.IMPORT","Import")} />
      </Dropzone>
    );

    // Export
    var export_= (
      <FlatButton
        key= {"expbtn"+id}
        style={this.styles.buttonStyle}
        label={getLocalizedText("HANDSONTABLE_BUTTON.EXPORT","Export")}
        onTouchTap={
          function(e, target) {
            var hot = self.refs[this.id].hot;
            var fields = self.refs[this.id].fields;
            var data = self.refs[this.id].getData();

            if (data && data.length > 0) {
              json2csv({ data: data, fields: fields }, function(err, csv) {
              //  if (err) console.log(err);

                var uri = 'data:text/csv;charset=utf-8,' + escape(csv);

                var downloadLink = document.createElement("a");
                downloadLink.href = uri;
                downloadLink.download = "export.csv";

                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
              });
            }
          }.bind(template)
        } />
    );
    //



    return (
      <div style={{height: '100%'}}>
        {
          id=="fund"?
          null:
          <div key="subActions" style={{textAlign:'right', padding:'14px 24px'}}>
            {import_}
            {export_}
          </div>
        }
        <HandsonTable
          key= {"ht"+id}
          ref={id}
          height="100%"
          template={template}
          gridData={cValue}
          disabled={disabled}
          pageData={ContentStore.getPageContent().changedValues}
          insertable={insertable?insertable:true}
          onChange={
            function(value) {
              //console.debug("[DynEditableColumn] - [Grid] - [onChange]");
              self.requestChange(value);
            }
          } />
      </div>
    );
  }
});

module.exports = EABTextField;
