let React = require('react');
let mui = require('material-ui');
let RaisedButton = mui.RaisedButton;
let CircularProgress = mui.CircularProgress;
let ContentStore = require('../../stores/ContentStore.js');
let UploadStore = require('../../stores/UploadStore.js');
let Dropzone = require('../CustomView/DropZone.jsx');
let FileUploadActions = require('../../actions/FileUploadActions.js');
let PDF = require('react-pdf');

let FileUpload = React.createClass({
  propTypes: {
    template:React.PropTypes.object,
    changedValues: React.PropTypes.object
  },

  getInitialState: function() {
    var {
      template,
      changedValues
    } = this.props;

    var {
      id,
      subType,
      presentation
    } = template

    var defRequire = presentation || "image/jpg,image/jpeg,image/png,application/pdf|1024,1024,340,480|5";
    var reqs = defRequire.split('|');

    var data = null;
    var loadingState = 1;
    if (subType == "base64") {
      data = changedValues[id];
      loadingState = 0;
    }

    return {
      data: data,
      fileType: reqs && reqs.length > 0 && reqs[0]?reqs[0]:"image/*",
      size: reqs && reqs.length > 1 && reqs[1]?reqs[1].split(','):[1024,1024,340,480],
      fileSize: reqs && reqs.length>2 && reqs[2]?parseFloat(reqs[2]):5,
      state: loadingState,
      presentation: presentation
    }
  },

  componentDidMount() {
    var self = this;
    var {
      template,
      rootValues,
      changedValues
    } = this.props;

    var {
      id,
      subType
    } = template

    UploadStore.addChangeListener(self.stateUpdate);

    var data = null;
    if (subType == "attachment") {
      FileUploadActions.getAttachment(rootValues.id, rootValues.version, id, function(data) {
        self.setState({
          data: data,
          state: 0,
          update: false
        })
      });
    }
  },

  componentWillUnmount() {
    // save the file if it is attachement
    UploadStore.removeChangeListener(this.stateUpdate);
  },

  stateUpdate() {
    var state = UploadStore.getState();

    this.setState({
      state: state
    });
  },

  updateImg(newFile) {
    var {
      template,
      handleChange,
      changedValues
    } = this.props;

    var {
      id,
      subType
    } = template;

    if (subType == 'base64') {
      changedValues[id] = newFile.url
    } else {
      // just put a dummy value as it will save as attachment later
      changedValues[id] = newFile.url.length
    }

    if (handleChange) {
      handleChange(id, null);
    }
    this.setState({
      data: newFile.url,
      state: newFile.state || 0,
      update: true
    })
  },

  removeFile() {
    var self = this;
    var {
      template,
      handleChange,
      changedValues,
      rootValues
    } = this.props;



    var {
      id,
      subType,
    } = template;

    if (subType == 'base64') {
      changedValues[id] = null;
      this.setState({
        data:"",
        state: 0,
        update: true
      })
       if(handleChange) {
          handleChange(id, null);
        }
    } else {
      FileUploadActions.removeFile(rootValues.id, rootValues.version, id, function(resp){
        if(handleChange) {
          handleChange(id, null);
        }
        if(!resp.err){
          changedValues[id] = 0;
          self.setState({
            data:"",
            state: 0,
            update: true
          })
        }

      });
    }
  },

  getStyle() {
    var style = {
      'buttonBgColor':'#FAFAFA',
      'buttonGreen':'#2DB154',
      'buttonRed':'#F1453D',
    }
    return style;
  },

  render: function() {
    // var {
    //   id,
    //   parent,
    //   title,
    //   desc,
    //   button,
    //   buttonAdd,
    //   disabled,
    //   type,
    //   require
    // } = this.props;
    var self = this;
    var {
      template,
      rootValues,
    } = this.props;

    var buttonAdd = "Add";
    var buttonDelete = "Delete";

    var {
      id,
      title,
      hints,
      disabled,
      type,
      subType,
      presentation
    } = template;

    var {
      state,
      fileType,
      fileSize,
      size,
      imgWidth,
      imgHeight
    } = this.state

    var maxSize = 5;

    var changedValues = this.state.changedValues;
    var data = this.state.data;

    var content = null;

    var save = function(invalid, res) {
      if (invalid.length == 0) {
        if (subType == "attachment") {
          self.updateImg({
            name: res.file.name,
            size: res.file.size,
            altText: '',
            caption: '',
            file: res.file,
            url: res.imageUrl,
            state: 2,
          });
          FileUploadActions.saveFile(rootValues.id, rootValues.version, id, self.state.data);
        } else {
          self.updateImg({
            name: res.file.name,
            size: res.file.size,
            altText: '',
            caption: '',
            file: res.file,
            url: res.imageUrl
          });
        }
      } else {
        var errorMsg = "";
        for (var i = 0; i < invalid.length; i++) {
          errorMsg += invalid[i] + "\n";
        }
        alert(errorMsg, "Import failed");
      }
    }

    if (typeof data === "string" && !isEmpty(data)) {
      var buttonOb = (disabled)?<div style={{height:"24px"}}/>:(state == 0?
        <RaisedButton
          label={buttonDelete}
          style={{marginTop:'15px', marginBottom:'15px'}}
          backgroundColor = {this.getStyle().buttonBgColor}
          labelColor = {this.getStyle().buttonRed}
          onClick={this.removeFile} />:<span> Uploading... </span>)
      var actualType = data.substring(data.search(":") + 1, data.search(";"));

      if (!actualType || actualType.indexOf('pdf') < 0) {
        if(!!presentation){
          imgWidth = size[2]+'px';
          imgHeight = size[3]+'px';
        }
        content = (
          <div className="bncItem">
            <div className="bncItem">
              <img className="productCoverImg"
                key={id + "img"}
                src={data}
                style={{
                  width:imgWidth,
                  height:imgHeight
                }}
                onClick={function() {
                  window.open(this);
                }.bind(data)} />
            </div>
            {buttonOb}
          </div>
        );
      } else {
        var encodedStr = data.substring(data.search(",") + 1, data.length);
        content = (
          <div className="bncItem">
            <div className="bncItem">
              <PDF content={encodedStr}
                page={1}
                scale={0.2}
                loading={(<span>Loading...</span>)}
                onClick={function(pages) {
                  var blob = b64toBlob(this.b64, this.actualType);
                  var blobUrl = URL.createObjectURL(blob);
                  window.open(blobUrl);
                }.bind({b64: encodedStr, actualType: actualType})} />
            </div>
            {buttonOb}
          </div>
        );
      }
    } else if (state === 1) {
      content = <CircularProgress key="loader" ref="loader"/>
    } else {
      content = (
        <Dropzone
          onDrop={function(res) {
            var requireWidth = parseInt(size[0]);
            var requireHeight = parseInt(size[1]);
            var invalid = [];

            //File type checking
            var actualFileType = res.file.type;
            if (fileType.indexOf(actualFileType) > -1) {
              if ((requireWidth || requireHeight) && res.imageUrl && res.file.type.indexOf('image') != -1) {
                var image = new Image()
                image.onload = function() {
                  var width = image.width;
                  var height = image.height;
                  if(!presentation){
                    self.state.imgWidth = width;
                    self.state.imgHeight = height;
                  }
                  // Width & Height checking
                    if (!!presentation && ((width && width != requireWidth) || (height && height != requireHeight))) {
                      invalid.push("Your image is " + width + " x " + height + ". Required " + requireWidth + " x " + requireHeight + ".");
                    }
                  // File size checking
                  if ( res.file.size / 1048576.0 > fileSize) {
                     invalid.push("Your file size exceed " + fileSize + " MB.");
                  }
                  save(invalid, res);
                }
                image.src = res.imageUrl;
              } else {
                // save pdf
                save(invalid, res);
              }
            } else {
              invalid.push("Your file is not correct type.");
              save(invalid, res);
            }

          }.bind(this.state)}
          className="dropzone"
          accept={fileType}>
          {disabled?<div style={{height:"24px"}}/>:<RaisedButton
              label={buttonAdd}
              style={{marginTop:'15px', marginBottom:'15px'}}
              backgroundColor = {this.getStyle().buttonBgColor}
              labelColor = {this.getStyle().buttonGreen} />}
        </Dropzone>
      );
    }

    return (
      <div className="PageContent">
        { title ? <span style={{
          fontSize: '20px',
          lineHeight: '40px',
          fontWeight: 300,
          paddingLeft: '24px'
        }}>{title}</span>:null }
        { hints ? <span style={{
          fontSize: '14px',
          paddingLeft: '24px'
        }}>{hints}</span>:null }
        <div className="DetailsItem">{content}</div>
      </div>
    );
  },

});

module.exports = FileUpload;
