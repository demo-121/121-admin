let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;


import EABFieldBase from './EABFieldBase.js';

module.exports = React.createClass({

  mixins: [EABFieldBase],


  onUpdate(){
      this.forceUpdate();
  },

  render() {
    var {
      template,
      editorStyle
    } = this.props;

    var {
      changedValues
    } = this.state;

    var {
      id,
      disabled
    } = template;

    var self = this;

    var cValue = self.getValue('');

    if(disabled){
      return (
        <div>
          <div style={this.mergeAndPrefix({border: 'solid 1px', borderRadius: '2px', width: '100%', height: 'calc(100vh - 208px)'}, editorStyle)}>
            {cValue}
          </div>
        </div>
      )
    }else{
      return (
        <div>
          <textarea
            key="textarea"
            ref="textarea"
            value={cValue}
            style={this.mergeAndPrefix({resize: 'none', width: '100%', height: 'calc(100vh - 208px)'}, editorStyle)}
            onChange={function(e){
              changedValues[id] = e.target.value;
              self.forceUpdate();
            }}
          />
        </div>
      );
    }
  }
});
