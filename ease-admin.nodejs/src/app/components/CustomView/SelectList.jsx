let React = require('react');
let mui = require('material-ui');

let FlatButton = mui.FlatButton;
let FontIcon = mui.FontIcon;

import EABFieldBase from './EABFieldBase.js';
import ReadOnlyField from './ReadOnlyField.jsx';
import DialogActions from '../../actions/DialogActions.js';

import appTheme from '../../theme/appBaseTheme.js';

module.exports = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    boxStyle: {
      backgroundColor: appTheme.palette.borderColor,
      borderRadius: '16px',
      display: 'inline-block',
      cursor:'pointer',
      width: 'auto',
      height: '32px',
      margin: '8px 12px 8px 0'
    },
    labelStyle: {
      fontSize: '14px',
      lineHeight: '32px',
      display: 'inline-block',
      width: '40px',
      textAlign: 'right'
    },
    iconStyle: {
      padding: '4px',
      float: 'right',
      color: 'rgba(0,0,0,.2)'
    }
  },

  render() {
    var self = this;
    var {
      template,
      width,
      values,
      handleChange,
      compact,
      disabled,
      style,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      type,
      title,
      hints,
      placeholder,
      presentation,
      items,
      value,
      min,
      max,
      interval,
      subType
    } = template;
    var {
      options
    } = this.state

    var cValue = self.getValue("");

    var sValues = null;
    if(typeof cValue === "string") {
      sValues = cValue.split(",");
    } else {
      sValues = [];
    }

    var optionList = [];

    for (let i in options){
      var option = options[i];

      if (sValues.indexOf(option.value) >= 0) {

        var otitle = getLocalText("en", option.title)

        optionList.push(<div key={"o"+i} style={this.styles.boxStyle} >
          <span key="ot" style={this.styles.labelStyle} >{otitle}</span>
          <FontIcon
            key="oi"
            className="material-icons"
            style={this.styles.iconStyle}
            onTouchTap={
              (disabled)?null:
              function() {
                var result = '';

                var j = sValues.indexOf(this.value)
                if (j>=0) {
                  sValues.splice(j,1);

                  if (this.item.trigger && this.item.trigger.type=='removeIfContains') {
                    if (this.item.trigger.value) {
                      var target = changedValues[this.item.trigger.id];
                      var subItemId = this.value.toLowerCase() + this.item.trigger.value
                      if (target && subItemId) {
                        target[subItemId] = null;
                      }
                    } else if (changedValues && this.item.trigger.id) {
                      changedValues[this.item.trigger.id] = null;
                    }
                  }
                }

                for (let i in sValues){
                  result += (result?",":"") + sValues[i];
                }
                changedValues[id] = result;
                self.handleChange(changedValues);
              }.bind({
                item: template,
                value: option.value
              })
            }>
            cancel
          </FontIcon>
        </div>);
      }
    }

    var subItem = null;

    if (items && items[0]) {
      subItem = items[0];
    } else {
      subItem = {
        type: 'button',
        title: 'Add',
        dialog: {
          id: 'dlg'+id,
          title: getLocalText('en', title),
          message: getLocalText('en', hints),
          positive: {
            id: 'save',
            type: 'customButton',
            title: 'OK'
          },
          negative: {
            id: 'cancel',
            type: 'button',
            title: 'Cancel'
          },
          items: [{
            id: id,
            type: 'CheckBoxGroup',
            options: options
          }]
        }
      }
    }

    var errorMsg = validate(template, changedValues);
    errorMsg = errorMsg?<div style={{
      color: this.state.muiTheme.textField.errorColor,
      height: '16px',
      fontSize: '12px',
      position: 'absolute',
      left: '24px',
      top: title?'42px':null,
      bottom: title?null:'4px'
    }}>{errorMsg}</div>:null;

    return (
      <div className={"hBoxContainer" + self.checkDiff()} style={ style || compact?{padding:"0"}:{} }>
        {!compact?this.getHeaderTitle():null}
        <div style={{paddingLeft:'24px', paddingRight:'24px'}}>
          {optionList}
          {  (disabled)?null:
            <div key= {"add"+id}
              style={ this.mergeStyles(this.styles.boxStyle, { color: appTheme.palette.themeGreen }) }
              onClick={
                function(){
                  this.dialog.values = changedValues;
                  this.dialog.positive.type = 'customButton';
                  this.dialog.positive.handleTap = function(cValues) {
                    self.state.changedValues[id] = cValues[id];
                    self.handleChange(cValues);
                  }
                  DialogActions.showDialog(this.dialog);
                }.bind(subItem)
              }>
              {subItem.title?<span key= {"label"+id} style={this.styles.labelStyle}>{subItem.title}</span>:null}
              <FontIcon
                key= {"icon"+id}
                className="material-icons"
                style={this.mergeStyles(this.styles.iconStyle, { color: appTheme.palette.themeGreen }) }
                >add_circle</FontIcon>
            </div>
          }
        </div>
        {errorMsg}
      </div>
    )
  }
});
