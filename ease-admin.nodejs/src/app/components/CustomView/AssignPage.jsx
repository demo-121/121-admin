let React = require('react');
let mui = require('material-ui');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');
let AssignContentStore = require('../../stores/AssignContentStore.js');
let HomeStore = require('../../stores/HomeStore.js');
let DynMasterTable = require('../DynamicUI/DynMasterTable.jsx');
let StylePropable = mui.Mixins.StylePropable;
import appTheme from '../../theme/appBaseTheme.js';
let Transitions = mui.Styles.Transitions;

let AssignPageActions = require('../../actions/AssignPageActions.js');
let DialogActions = require('../../actions/DialogActions.js');

let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let ToolbarSeparator = mui.ToolbarSeparator;

let FontIcon = mui.FontIcon;
let IconButton = mui.IconButton;
let FlatButton = mui.FlatButton;
let IconMenu = mui.IconMenu;
let TextField = mui.TextField;
let DropDownMenu = mui.DropDownMenu;
let MenuItem = mui.MenuItem;

let AssignPage = React.createClass({
  mixins: [StylePropable],

  getStyles() {
    return {
      contentStyle: {
        color: appTheme.palette.alternateTextColor,
        top: '0px'
      },
      assignPageStyle :{
        backgroundColor: "#FFFFFF",
        position: "absolute",
        zIndex: '200',
      },
      iconStyle: {
        padding: '0px 12px',
        margin: '4px',
        lineHeight: '48px'
      },
      flatButtonStyle:{
        padding: '10px',
        margin: '0 20px 0 0',
        minWidth: '100px',
        color: appTheme.palette.alternateTextColor
      },
      pickerStyle: {
        width: 'auto',
        height: '56px',
      },
      actionsPaneStyle: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%'
      },
      seperatorStyle: {
        float:null,
        lineHeight:'56px',
        fontSize:'30px',
        color: appTheme.palette.alternateTextColor
      },
      labelStyle: {
        paddingLeft: '3px',
        lineHeight: '56px',
        fontSize: '20px',
        fontWeight: 'normal',
        display: 'inline-block',
        position: 'relative',
        float: null,
        color: appTheme.palette.alternateTextColor
      },
      buttonStyle: {
        fontSize: '20px',
        fontWeight: 'normal',
        textTransform: 'none',
        color: appTheme.palette.alternateTextColor,
        float: null,
        padding:0,
        margin:0
      }
    }
  },

  componentDidMount() {
    AssignContentStore.addChangeListener(this.onChange);
  },

  componentWillUnmount() {
    AssignContentStore.removeChangeListener(this.onChange);
  },

  onChange() {
    this.setState({
      content: AssignContentStore.getPageContent(),
      hint: AssignContentStore.getHint(),
      actions: AssignContentStore.getActions(),
      show: AssignContentStore.isShow(),
    })
  },

  getInitialState() {
    var content =  AssignContentStore.getPageContent();
    return {
      content: content,
      hint: AssignContentStore.getHint(),
      actions: AssignContentStore.getActions(),
    };
  },

  back(){
    AssignPageActions.back();
  },

  handleCriteriaEnter(e, e2) {
    var value = e.target.value;
    AssignPageActions.updateCriteria(value, false);
  },

  handleCriteriaChange(e) {
    var value = e.target.value;
    AssignPageActions.updateCriteria(value, true);
  },

  cleanCriteria() {
    AssignPageActions.updateCriteria('', false);
  },

  handleTap(e, target) {
    // for icon button
    var path = e.target.id;
    // for button
    if (path == "") {
      path = target.id;
    }
    var page = AssignContentStore.getCurrentPage();
    var data = {
      p1: page.module
    }

    console.debug('handle path =', path);
    callServer(path, data)
  },

  handleDialogTap(e, target) {
    console.debug("[DynToolbar] - [handleDialogTap]", target);
    if (target) {
      if (target.dialog) {
        var dialog = target.dialog;

        if(dialog.positive)
          dialog.positive.type='customButton';

        var id = dialog.positive.id;

        var values = {};
        values['rows'] = AssignContentStore.getSelectedRows();
        var content = AssignContentStore.getPageContent();
        var page = AssignContentStore.getCurrentPage();

        if (content.values.id != null) {
          values['keyList'] = content.values.id;
        }

        var page = HomeStore.getCurrentPage();
        page["actionID"] = id;
        dialog.positive.handleTap = function(actionItem){
          var data = {
            p0: convertObj2EncodedJsonStr(values),
            p1: page.module,
            p10: convertObj2EncodedJsonStr(page),
          }
          console.debug("[DynDialog] - [callServer]", values);
          callServer('/Tasks/Detail/Assign', data);
        }

        DialogActions.showDialog(dialog);
      }
    }
  },

  // handle submit button tap
  handleSubmitTap(e, target) {
    console.debug("handleSubmitTap", target);
    if (target) {
      AssignPageActions.submitCurrentId(target.id)
    }
  },

  // handle submitfull button tap
  handleSubmitChangedTap(e, target) {
    console.debug("submitButton", target);
    if (target) {
      AssignPageActions.submitChangedValues(target.id)
    }
  },

  handleSubmitSelectionTap(e, target) {
    if (target) {
      AssignPageActions.submitSelected(target.id)
    }
  },


  _genItem(item) {
    var values = this.state.values;
    var styles = this.getStyles()

    switch (item.type) {
      case 'label':
        return (
          <ToolbarTitle text={item.title} />
        );
      break;
      case 'iconButton':
        return (
          <FontIcon
            key={item.id}
            id={item.id}
            tooltip={item.title}
            style= { styles.iconStyle }
            color = { styles.contentStyle.color }
            hoverColor = { styles.contentStyle.color }
            className="material-icons"
            onClick={this.handleTap}>
            {item.value}
          </FontIcon>
        );
      break;
      case 'submitCurrentButton': // for Edit / Delete / Revert / Approve
        return (
          <FlatButton
            key={item.id}
            id={item.id}
            target={item}
            style={styles.flatButtonStyle}
            onTouchTap={this.handleSubmitTap}>
            {item.title}
          </FlatButton>
        );
        break;
      case 'submitChangedButton': // for Save
        return (
          <FlatButton
            key={item.id}
            id={item.id}
            target={item}
            style={styles.flatButtonStyle}
            onTouchTap={this.handleSubmitChangedTap}>
            {item.title}
          </FlatButton>
        );
        break;
      case 'submitSelectedButton': // for clone muliple selected row
        return (
          <FlatButton
            key={item.id}
            id={item.id}
            target={item}
            style={styles.flatButtonStyle}
            onTouchTap={this.handleSubmitSelectionTap}>
            {item.title}
          </FlatButton>
        );
        break;
      case 'button':
        return (
          <FlatButton
            key={item.id}
            id={item.id}
            target={item}
            style={styles.flatButtonStyle}
            onTouchTap={this.handleTap}>
            {item.title}
          </FlatButton>
        );
        break;
      case 'dialogButton':
        return (
          <FlatButton
            key={item.id}
            id={item.id}
            target={item}
            style={styles.flatButtonStyle}
            onTouchTap={this.handleDialogTap} >
            {item.title}
          </FlatButton>
        );
        break;
      case 'iconDialogButton':
        var handleTap = function() {
            if (this.dialog) {
              DialogActions.showDialog(this.dialog);
            }
        }.bind(item)

        return (
          <FontIcon
            key={item.id}
            id={item.id}
            tooltip={item.title}
            target={item}
            style= { styles.iconStyle }
            color = { styles.contentStyle.color }
            hoverColor = { styles.contentStyle.color }
            className="material-icons"
            onTouchTap={handleTap} >
            {item.value}
          </FontIcon>
        );
      case 'picker':
        return (
          <SelectField
            ref = {item.id}
            key = {item.id}
            template = {item}
            value = {values[item.id]?values[item.id]:item.value}
            style = { styles.pickerStyle }
            iconStyle={{ fill: styles.contentStyle.color}}
            labelStyle = { this.mergeStyles(styles.contentStyle, { paddingRight: '28px'}) }
            underlineStyle={{display:'none'}}
            handleChange={this.handleSelectionChange}/>
        );
        case 'versionPicker':
        return (
          <SelectField
            ref = {item.id}
            key = {item.id}
            template = {item}
            value = {values[item.id]?values[item.id]:item.value}
            style={{width: 'auto', height: '56px'}}
            labelStyle = {styles.contentStyle}
            underlineStyle={{display:'none'}}
            handleChange={this.handleVersionChange}/>
        );
        break;
      case 'iconMenu':
        return(
          <IconMenu
            targetOrigin={{vertical:"top", horizontal:"right"}}
            iconButtonElement={
              <IconButton touch={true} style={styles.iconStyle}>
                <FontIcon
                  color= {appTheme.palette.alternateTextColor}
                  className="material-icons">
                  more_vert
                </FontIcon>
              </IconButton>
            }>
            {this._genMenuItems(item.items)}
          </IconMenu>
        );
        break;
      default:
        return null
    }
  },

  _genItems: function(group) {
    var items = [];
    for (let i in group) {
      var item = group[i];
      var view = this._genItem(item);
      if (view) {
        items.push(view)
      }
    }
    return items;
  },



  render: function() {
    var self = this;
    var content = self.state.content;
    var windowHeight = window.innerHeight;
    var styles = self.getStyles();
    var values = AssignContentStore.getValues();

    var {style} = this.props;

    var rightItems = (
      <ToolbarGroup key="right" float="right" style={styles.actionsPaneStyle}>
        <FontIcon
          key="cancelBtn"
          tooltip="Cancel"
          style= { styles.iconStyle }
          className = "material-icons"
          onClick = {self.cleanCriteria}>
          cancel
        </FontIcon>
        {this._genItems(this.state.actions)}
      </ToolbarGroup>
    );
    var content = this.state.show?
    (<div key="assignPage" style={this.mergeAndPrefix(this.props.style, styles.assignPageStyle)}>
      <Toolbar style={{padding:"0px", boxShadow:"0px 2px 2px rgba(0,0,0,.33)", position:'relative'}} >
        <ToolbarGroup style={{ width: 'calc(100% - 300px)', display:'flex'}}>
          <FontIcon
            key="backBtn"
            tooltip="Back"
            style= { styles.iconStyle }
            className="material-icons"
            onClick={this.back}>
            arrow_back
          </FontIcon>
          <TextField
            key = "criteria"
            ref = "criteria"
            style = {{ width: 'calc(100% - 100px)', lineHeight:'56px'}}
            inputStyle = {styles.contentStyle}
            hintText = {(this.state.hint)?this.state.hint:"Search by code or name..."}
            hintStyle = {styles.contentStyle}
            value = {values["criteria"]}
            onEnterKeyDown = { self.handleCriteriaEnter }
            onChange = {self.handleCriteriaChange}
            underlineStyle={{display:'none'}}
            />
        </ToolbarGroup>
        {rightItems}
      </Toolbar>
      <DynMasterTable
        id = {content.template.id}
        list = {content.values.list}
        total = {content.values.total}
        template = {content.template}
        assignPage = {true}
        show = {true}
        multiSelectable = {content.selectType=="MULTI_SELECTABLE"?true:false}
        selectable = {content.selectType=="SELECTABLE"?true:false}
        height = { (windowHeight - 170) + "px"}/>
      <div style={{height:"16px", width:"100%"}}/>
    </div>):null;

      return (
        <ReactCSSTransitionGroup transitionName="assignPage" transitionEnterTimeout={500} transitionLeaveTimeout={500}>
          {content}
        </ReactCSSTransitionGroup>

      )

  }
});

module.exports = AssignPage;
