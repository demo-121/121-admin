let React = require('react');
let mui = require('material-ui');

let Checkbox = mui.Checkbox;

import EABFieldBase from './EABFieldBase.js';

module.exports = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    checkBoxLabelStyle: {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      width: 'calc(100% - 40px)',
      fontSize: '16px'
    }
  },

  render() {
    var self = this;
    var {
      template,
      values,
      width,
      compact,
      disabled,
      handleChange,
      rootValues,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      title,
      value
    } = template;

    // var cValue = ((id && changedValues && changedValues[id])?changedValues[id]:(value?value:"N")) == "Y";
    var cValue = (self.getValue("N") === "Y")

    disabled = disabled || template.disabled || disableTrigger(template, null, values, changedValues, rootValues)

    return <div
      className={"DetailsItem" + this.checkDiff() }>
      <Checkbox
          key={ id }
          label = {!compact?this.getTitle():""}
          labelStyle = {this.styles.checkBoxLabelStyle}
          className = {"CheckBox"}
          disabled = {disabled}
          style={{ width:width?width:'100%', display:'inline-block', backgroundColor:null }}
          defaultChecked= { cValue }
          onCheck = {
            function(e, val) {
              self.requestChange(!cValue);
            }
          }
          {...others}
          />
      </div>
  }
});
