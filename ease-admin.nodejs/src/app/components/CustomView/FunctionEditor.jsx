let React = require('react');
let mui, {Dialog, FlatButton, Toolbar, ToolbarTitle,
  ToolbarGroup, FontIcon, IconButton, CircularProgress} = require('material-ui');

let EABJSBoxField = require('../CustomView/JSBoxField.jsx');

import appTheme from '../../theme/appBaseTheme.js';
import EABFieldBase from './EABFieldBase.js';

let FuncEditorStore = require('../../stores/FuncEditorStore.js')
let DialogActions = require('../../actions/DialogActions.js');
let DynActions = require('../../actions/DynActions.js');

const FUNCTION_START = "function([para]) {";
const FUNCTION_END = "}";

let EditorPanelHeader = React.createClass({
  getInitialState() {
    return {
      editing: false,
      hasError: false,
    };
  },

  render() {
    var self = this;

    var {
      title,
      canReset,
      id,
      index,
      template,
      rootValues,
      editingValues,
      handlePlay,
      handleClose,
      handleReset,
      handleChange
    } = this.props;

    var cValue = editingValues[template.id];


    return <Toolbar key={"toolbar"}
        className={ "Toolbar" }>
        <ToolbarTitle text={title || "Function Editor"} style={{
              lineHeight:'56px',
              display:'inline-block', verticalAlign: 'top',
              paddingLeft: 24,
              fontSize: 20,
              color: appTheme.palette.alternateTextColor,
              whiteSpace: 'nowrap',
              textOverflow: 'ellipsis',
              overflow: 'hidden'
            }}/>
        <ToolbarGroup style={{float: 'right'}}>
          <FlatButton
            label={"Cancel"}
            secondary={ true }
            onTouchTap={handleClose} />
          {canReset?<FlatButton
            label={"Reset"}
            secondary={ true }
            disabled={!cValue}
            onTouchTap={ handleReset }/>:null}
          <FlatButton
            label={"Test"}
            secondary={ true }
            disabled={self.state.hasError || self.state.editing}
            onTouchTap={ handlePlay }/>
          <FlatButton
            label={ "Done" }
            secondary={ true }
            disabled={self.state.hasError || self.state.editing}
            onTouchTap={ function() {
              handleChange(editingValues);
            } }/>
        </ToolbarGroup>
      </Toolbar>
  }
})

let EditorPanel = React.createClass({

  getInitialState() {
    return {
      editing: false,
      hasError: false,
    };
  },

  render() {
    var self = this;

    var {
      title,
      id,
      index,
      template,
      rootValues,
      editingValues,
      updateParaFunc,
      handlePlay,
      handleClose,
      handleReset,
      handleChange
    } = this.props;

    var {
      dataDict
    } = FuncEditorStore.getData();

    if (!editingValues) return "";

    title = title || getLocalText("en", editingValues.title);
    if (!isEmpty(title)) {
      title += " ("+editingValues.id+")";
    } else {
      title = editingValues.id;
    }

    return <div key={"floatingPage"} className={ "FloatingPage" }
      style={{
        display: 'flex'
      }}>
      <EditorPanelHeader
        ref={"eph"}
        canReset={editingValues.default}
        title={title}
        id={id}
        index={index}
        rootValues={rootValues}
        template={template}
        editingValues={ editingValues }
        handleClose={ handleClose}
        handleReset={ handleReset}
        handlePlay={ handlePlay}
        handleChange={ handleChange }
        />
      <div key={'content'}
        className={ "Content" }>
        {dataDict[id]?<div style={{display:'block', fontSize: 12, color:appTheme.baseTheme.palette.primary3Color}}>{dataDict[id]}</div>:null}
        <EABJSBoxField
          key = {"jsbox_"+ id}
          ref = {"jsbox_"+ id}
          template = { template }
          index = { index }
          changedValues = { editingValues }
          rootValues = { rootValues }
          updateParaFunc = { updateParaFunc }
          onJSEditorFocus = {
            function() {
              if (self.refs.eph) {
                self.refs.eph.state.editing = true;
                self.refs.eph.forceUpdate();
              }
            }
          }
          handleChange = {
            function(id, value, hasError) {
              if (self.refs.eph) {
                self.refs.eph.state.editing = false;
                self.refs.eph.state.hasError = hasError;
                self.refs.eph.forceUpdate();
              }
            }
          }/>
      </div>
    </div>
  }
})

let FunctionEditor = React.createClass({
  mixins: [EABFieldBase],

  getInitialState() {
    return {
      runState: 0,
      paraUpdated: false,
      open: false
    };
  },

  handleClose() {
    this.setState({open: false})
  },

  resetValueItems: null,

  updatePara(changingValues) {

    var data = FuncEditorStore.getData();
    if (!data) {
      console.error('error: failure to get defParams from server')
      return changingValues;
    }

    var defParams = data.defParams;
    if (defParams) {
      var paras = changingValues.inputParams;
      for (var i in paras) {
        var para = paras[i];
        if (para.type == 'quotation' && defParams[para.type]) {
          para.value = !isEmpty(para.value)?mergeObject(cloneObject(defParams.quotation), para.value):cloneObject(defParams.quotation);
        } else if (para.type == 'planInfo' && defParams[para.type]) {
          para.value = !isEmpty(para.value)?mergeObject(cloneObject(defParams.planInfo), para.value):cloneObject(defParams.planInfo);
        } else if (para.type == 'extraPara' && defParams[para.type]) {
          if (!isEmpty(para.value)) {
            for (var v in para.value) {
              if (v == 'planIllustProp' || v == 'templateFuncs') {
                // para.value[v] = defParams.extraPara[v];
                para.value[v] = "/* Not configurable */";
              } else if (!isEmpty(para.value[v])) {
                // para.value[v] = mergeObject(cloneObject(defParams.extraPara[v]), para.value[v]);
                para.value[v] = mergeObject(defParams.extraPara[v], para.value[v]);
              } else {
                // para.value[v] = cloneObject(defParams.extraPara[v]);
                para.value[v] = defParams.extraPara[v];
              }
            }
          } else {
            para.value = defParams.extraPara;
          }
        } else if (para.type == 'planDetails' && defParams[para.type]) {
          // para.value = defParams.planDetails;
          para.value = "/* Not configurable */";
        } else if (para.type == 'planDetail' && defParams[para.type]) {
          // para.value = defParams.planDetail;
          para.value = "/* Not configurable */";
        } else if (para.type == 'object') {
          para.value = para.value || {}
        } else if (para.type == 'array') {
          para.value = para.value || []
        }
      }
    }
    return changingValues;
  },

  onUpdatePara() {
    FuncEditorStore.removeChangeListener(this.onUpdatePara);
    var {
      changedValues,
      editingValues,
    } = this.state;

    var {
      rootValues
    } = this.props;

    var changingValues = this.updatePara(editingValues || changedValues)

    if (this.state.open) {
      this.setState({
        editingValues: editingValues || cloneObject(changingValues),
        // paraUpdated: true
      })
    } else {
      // this.setState({
      //   paraUpdated: true
      // })
      this.forceUpdate();
    }
    // this.setState({
    //   paraUpdated: true
    // })
  },

  onClickPlay() {
    var self = this;


    var testValues = this.state.open?this.state.editingValues:this.state.changedValues;

    if (!this.state.runState && testValues.func) {
      FuncEditorStore.addChangeListener(this.onUpdatePara);

      this.state.runState = 1;

      DynActions.getFunctionPara(testValues.inputParams);
    } else {
      DialogActions.showMessageDialog({
        title: "Execution error",
        message: "Empty function body.",
        contentStyle: {
          width: '200px'
        },
        actions: [{
          type: 'customButton',
          title: "Cancel",
          handleTap: function() {
            return true;
          }
        }]
      });
    }
  },

  onClickEdit() {
    var self = this;
    var {
      changedValues
    } = this.state;

    // this.setState({
    //   editingValues: cloneObject(changedValues),
    //   open: true
    // });
    this.state.open = true;
    FuncEditorStore.addChangeListener(this.onUpdatePara);
    DynActions.getFunctionPara(changedValues.inputParams);
  },

  componentDidUpdate() {
    var {
      runState,
      open,
      changedValues,
      editingValues
    } = this.state;

    var {
      subType
    } = this.props.template

    var self = this;

    var {
      funcData,
      defParams
    } = FuncEditorStore.getData();
    // if (!this.state.paraUpdated && this.props.inline) {
    //   FuncEditorStore.addChangeListener(this.onUpdatePara);
    //   DynActions.getFunctionPara(changedValues.inputParams, "P");
    //   return;
    // }

    if (runState == 1) {
      var count = 0;
      var testValues = open?editingValues:changedValues;

      var checkReady = function() {
        var contentWindow = document.getElementById("testFrame").contentWindow;
        if (contentWindow.runDriver) {
          console.log('Ready!', contentWindow.runDriver);

          DialogActions.showMessageDialog({
            title: "Execution Result",
            message: "Running...",
            contentStyle: {
              width: '200px'
            },
            actions: [{
              type: 'customButton',
              title: "Cancel",
              handleTap: function() {
                self.setState({
                  runState: 0
                })
                return true;
              }
            }]
          });
          setTimeout(function() {
            var result = null;

            var input = [];

            for (var i in testValues.inputParams) {
              var para = testValues.inputParams[i];
              var param = {
                id: para.id,
                type: para.type,
                value: {}
              }
              if (para.type == 'extraPara' && defParams[para.type]) {
                for (var v in para.value) {
                  if (v == 'planIllustProp' || v == 'templateFuncs') {
                    param.value[v] = defParams.extraPara[v];
                  } else {
                    // para.value[v] = cloneObject(defParams.extraPara[v]);
                    param.value[v] = para.value[v];
                  }
                }
              } else if ((para.type == 'planDetails' || para.type == 'planDetail') && defParams[para.type]) {
                param.value = defParams[para.type];
              } else {
                param.value = para.value;
              }
              input.push(param);
            }

            if (subType == 'driver') {
              result = contentWindow.runDriver(testValues.func, input);
            } else if (testValues.default) {
              result = contentWindow.testFunction(testValues.func, input);
            } else {
              result = contentWindow.testFunction(testValues.func, input);
            }
            if (typeof result == 'object') {
              result = JSON.stringify(result);
            }

            DialogActions.showMessageDialog({
              title: "Execution Result",
              message: result,
              contentStyle: {
                width: '80%'
              },
              actions: [{
                type: 'customButton',
                title: "Done",
                handleTap: function() {
                  self.setState({
                    runState: 0
                  })
                  return true;
                }
              }]
            });
          }, 500)
        } else {
          if (count < 10) {
            count++;
            console.log('not ready!');
            setTimeout(checkReady, 1000)
          }
        }
      }

    setTimeout(function() {
      DialogActions.showMessageDialog({
        title: "Execution Result",
        message: "Initializing...",
        contentStyle: {
          width: '200px'
        },
        actions: [{
          type: 'customButton',
          title: "Cancel",
          handleTap: function() {
            self.setState({
              runState: 0
            })
            return true;
          }
        }]
      });
    }, 100);
      setTimeout(checkReady, 1000)
    }
  },

  resetFunc() {
    var self = this;
    var {
      template
    } = this.props;

    var {
      changedValues,
      editingValues,
      editing
    } = this.state;

    var {
      id
    } = template;
    DialogActions.showMessageDialog({
      title: "Reset Function",
      message: "Are you sure to reset the function to resume default routine?",
      contentStyle: {
        width: '80%'
      },
      actions: [{
        type: "customButton",
        title: "Confirm",
        handleTap: function() {
          editingValues[id] = changedValues[id] = "";
          editingValues.funcBody = changedValues.funcBody = "";
          self.setState({
            open: false
          })
        }
      }, {
        type: 'cancel',
        title: "Cancel"
      }]
    });
  },

  getFullFunction(changedValues) {
    var para = ""
    if (changedValues.inputParams) {
      for (var i in changedValues.inputParams) {
        if (i != '0') para += ", ";
        para += changedValues.inputParams[i].id;
      }
      return FUNCTION_START.replace('[para]', para) + "\n"
        + changedValues.funcBody + "\n"
        + FUNCTION_END
    }
    return "";
  },

  render() {
    var self = this;

    var {
      muiTheme
    } = this.context;

    var {
      template,
      width,
      handleChange,
      rootValues,
      disabled,
      index,
      inline,
      ...others
    } = this.props;

    var {
      editingValues,
      changedValues,
      muiTheme,
      paraList,
      open,
      runState,
      paraUpdated,
      activeListItem,
      expandedListItems
    } = this.state;

    var {
      id,
      type,
      placeholder,
      value,
      min,
      max,
      interval,
      subType
    } = template;

    var cValue = self.getValue("");
    var dtitle = getLocalText("en", template.title || template.hints);

    self.state.hasError //validate(template, changedValues);

    var content = [];

    if (open) {
      content.push()
    }

    if (inline) {
      // if (!paraUpdated) {
      //   return <div style={{
      //       width: "100%"
      //     }}> Loading <CircularProgress key={"loader"+id} size={1}/> </div>
      // }

      return <div style={{
          width: "100%"
        }}>
        <EABJSBoxField
          key = {"jsbox_"+ id}
          ref = {"jsbox_"+ id}
          template = { template }
          index = { index }
          updateParaFunc = { self.updatePara }
          changedValues = { changedValues }
          rootValues = { rootValues }
          style={{
            maxWidth: "calc(100% - 50px)"
          }}
          inline={true}/>
        {runState?<iframe ref="testFrame" height="0" width="0" borer="0" id="testFrame" src="./web/jsDriver/FunctionTest.html"></iframe>:
          <IconButton style={{display:'inline-block', float: "right", margin:"0"}}
            onClick={ self.onClickPlay}>
            <FontIcon className="material-icons">play_arrow</FontIcon>
          </IconButton>}
      </div>
    } else {
      var func = this.getFullFunction(changedValues);

      return <div className={"DetailsItem"}>
        <div style={{height: '44px', width: '100%'}}>
          <div style={{fontSize: '20px', fontWeight:300, display:'inline-block', lineHeight:'36px'}}>{dtitle}</div>
          <FlatButton
            label={"Test"}
            disabled={self.state.hasError}
            style={{
              float: 'right',
              marginBottom: '8px'
            }}
            onTouchTap={ self.onClickPlay }/>
          <FlatButton
              label={"Edit"}
              style={{
                float: 'right',
                marginRight: '24px',
                marginBottom: '8px'
              }}
              onTouchTap={ self.onClickEdit }/>
          {changedValues.default?<FlatButton
              label={"Reset"}
              style={{
                float: 'right',
                marginRight: '24px',
                marginBottom: '8px'
              }}
              onTouchTap={ self.resetFunc }/>:null}
        </div>
        {cValue?<div className={"FullScript"}>{func}</div>:
        (changedValues.default?
          <span style={{ color: muiTheme.baseTheme.palette.text1Color}}>Create function to override the default routine by click Edit</span>:
          <span style={{ color: muiTheme.baseTheme.palette.text1Color}}>Click Edit to create custom function.</span>)}
        {runState?<iframe ref="testFrame" height="0" width="0" borer="0" id="testFrame" src="./web/jsDriver/FunctionTest.html"></iframe>:null}
        {open?<EditorPanel
            title={dtitle}
            id={id}
            index={index}
            rootValues={rootValues}
            cValue={cValue}
            template={template}
            editingValues={ editingValues }
            updateParaFunc= {self.updatePara}
            handleClose={self.handleClose}
            handleReset={self.reset}
            handlePlay={self.onClickPlay}
            handleChange={function(values) {
              for (var v in values) {
                changedValues[v] = values[v];
              }
              self.handleClose();
              self.props.handleChange();
            }}/>:null}
      </div>
    }
  }
});

module.exports = FunctionEditor;
