let React = require('react');
let mui = require('material-ui');

let IconButton = mui.IconButton;
let Paper = mui.Paper;
import AddCircleOutline from 'material-ui/lib/svg-icons/content/add-circle-outline';
import Add from 'material-ui/lib/svg-icons/content/add';

import EABFieldBase from './EABFieldBase.js';
let Dropzone = require('../CustomView/DropZone.jsx');
let debug = false;
let _isMount = false;

let IconBox = React.createClass({

  mixins: [EABFieldBase],

  propTypes: {
    onClickBody: React.PropTypes.func,
    onImageChange: React.PropTypes.func,
    onDrop: React.PropTypes.func,
    isAdd: React.PropTypes.bool,
    isOther: React.PropTypes.bool,
    type: React.PropTypes.string.isRequired,
    size: React.PropTypes.number
  },

  getInitialState: function() {
    let {
      image,
      title
    } = this.props;

    return {
      image: image,
      title
    }
  },

  componentWillReceiveProps(nextProps){
    let {
      image,
      title
    } = nextProps;

    if(_isMount) this.setState({image: image, title: title});
  },

  componentWillUnmount(){
    _isMount=false;
  },

  componentDidMount(){
    _isMount=true;
  },


  onUpdate(){
    if(_isMount)
      this.forceUpdate();
  },

  setImg(newFile) {
    this.setState({image: newFile.url});
    if(this.props.onDrop){
      this.props.onDrop(newFile.url);
    }
  },

  removeFile() {
    this.setState({image: ""});
    if(this.props.onDrop){
      this.props.onDrop("");
    }
  },

  getSize(size){
    let height = (size)? size: 200;
    let width = (size)? size: 200;

    return({height: height, width: width});
  },

  getStyle() {
    var style = {
      'buttonBgColor':'#FAFAFA',
      'buttonGreen':'#2DB154',
      'buttonRed':'#F1453D',
    }
    return style;
  },

  genItem(type){
    let items = [];
    let {
      id,
      isAdd,
      isOther
    } = this.props;
    let {
      changedValues,
      title
    } = this.state;

    switch(type){
      case "imageSelection":
      case "iconSelection":
        items.push(<div key={"ic-desc-"+id} style={{paddingLeft:24, paddingRight:24}}>{isAdd?getLocalizedText('ADD_SECTION', 'Add Section'):! isEmpty(title)? multilineText(title.en): (isOther)?getLocalizedText('OTHERS', 'Others'):getLocalizedText('ADD_TITLE', 'Add title')}</div>)
        break;
      case "attachments":
        items.push(<div key={"ic-desc-"+id} style={{paddingLeft:24, paddingRight:24}}>{!isEmpty(title)? multilineText(title.en):getLocalizedText('ADD_TITLE', 'Add title')}</div>)
        break;
      default:
        break;
    }
    return items;
  },

  render: function() {
    //var changedValues = ContentStore.getPageContent().changedValues;
    if(debug){
      console.log("render - iconbox");
    }

    var {
      changedValues,
      image
    } = this.state;

    let {
      isAdd,
      size,
      type,
      style,
      paperStyle,
      imageStyle,
      dropZoneStyle,
      descStyle,
      disabled
    } = this.props;

    var dropZ = '';

    //let imageSize = this.getSize(size);

    let require = {
      width: size,
      height: size,
      fileType:'image/png',
      sizeLimit:1*1048576,// 1MB:2MB in byte
    }
    let imgHeight = 148;
    switch (size) {
      case 150:
          imgHeight=113;
          break;
      case 192:
          imgHeight=148;
          break;
      case 261:
          imgHeight=196;
          break;
    }

    let iconObj = (
      <div style={{cursor:'pointer', width:size+'px', height: imgHeight+'px', textAlign: 'center', verticalAlign:'middle', display:'table-cell'}}>
        <img style={this.mergeAndPrefix({width: 'auto', height: 'auto', maxWidth: type=='imageSelection'?size:'100px', maxHeight: type=='imageSelection'?imgHeight:'100px', verticalAlign:'center'}, imageStyle)} src={image}/>
      </div>
    );

    let imageObj = (
      <img
        style={this.mergeAndPrefix({cursor: 'pointer', width: '100%',height: 'auto'},imageStyle)}
        src={image}/>
    );

    let imgObj = iconObj;
    let addImgBtn = (
      <div  style={{verticalAlign:'middle', background: '#A8B6C0'}}>
       <IconButton
           iconStyle={{width: 48, height: 48,  WebkitFilter:'invert(100%)'}}
           style={{width: size+'px', height: imgHeight+'px'}}
           >
           <Add/>
       </IconButton>
     </div>
    )
    let addIconBtn = (
      <div style={{height:imgHeight+'px', width:'100%', cursor: 'pointer'}}>
        <IconButton
            iconStyle={{width: '28px', height: '28px',  WebkitFilter:'invert(100%)'}}
            style={{ marginTop: ((imgHeight-76)/2)+'px', width: '76px', height: '76px', borderRadius:'38px', background:'#A8B6C0'}}
            >
            <Add/>
        </IconButton>
      </div>
    );


    dropZ = (
      <Dropzone
        style={this.mergeAndPrefix({width: '100%', margin: 'auto'}, dropZoneStyle)}
        onDrop={function(res) {
          if (this.require) {
            var requireWidth = this.require.width;
            var requireHeight = this.require.height;
          }
          var invalid = [];

          var requireFiletype=this.require.fileType;
          //File type checking
          var actualFileType = res.file.type;
          if (actualFileType == requireFiletype) {
            var image = new Image();
            image.src = res.imageUrl;
            var width = image.width;
            var height = image.height;
            // Width & Height checking
            /*if ((requireWidth || requireHeight) && res.imageUrl && res.file.type.indexOf('image') != -1) {
              if ((width && width != requireWidth) || (height && height != requireHeight)) {
                invalid.push("Your image is " + width + " x " + height + ". Required " + requireWidth + " x " + requireHeight + ".");
              }
            }*/
            // File size checking
            if ( res.file.size >this.require.sizeLimit) {
               invalid.push("Your file size exceed " + this.require.sizeLimit/(1024*1024) + " MB.");
            }

          }else{
              invalid.push("Your file is not " + requireFiletype.substring(requireFiletype.indexOf('/')+1, requireFiletype.length).toUpperCase()+ " type.");
          }


          if (invalid.length == 0) {
            this.updateImg({
              name: res.file.name,
              size: res.file.size,
              altText: '',
              caption: '',
              file: res.file,
              url: res.imageUrl,
            });
          } else {
            var errorMsg = "";
            for (var i = 0; i < invalid.length; i++) {
              errorMsg += invalid[i] + "\n";
            }
            alert(errorMsg, "Import failed");
          }
        }.bind({require: require, updateImg: this.setImg})}
        className="dropzone"
        accept={"image/*"} >
        {image?imgObj:(type=="imageSelection")?addImgBtn:addIconBtn}
      </Dropzone>
    );

    let disableZone = (
      <div style={this.mergeAndPrefix({width: '100%', margin: 'auto'}, dropZoneStyle)}>
        {image?imgObj:(type=="imageSelection")?addImgBtn:addIconBtn}
      </div>
    )

    let height = ((size-imgHeight)+'px');
    let items = this.genItem(type);

    return (
      <div className="drag-area" style={this.mergeAndPrefix({cursor:'pointer', textAlign: 'center', width:size, height: size, display: 'inline-block', marginRight:'24px', marginBottom:'24px', verticalAlign:'top'}, style)}>
        <Paper style={this.mergeAndPrefix({background: '#FAFAFA'}, paperStyle)}>
          {disabled?disableZone:dropZ}
          <div className={isAdd?"":"configurable"}  style={this.mergeAndPrefix({height:type=='attachments'?'0px':height, width:size, verticalAlign:'middle', display:'table-cell'}, descStyle)} onClick={this.props.onClickBody}>
            {type!='attachments'?items:null}
          </div>
        </Paper>
        <div style={this.mergeAndPrefix({height:type=='attachments'?height:'0px', width:'100%', marginTop:'12px'}, descStyle)} onClick={this.props.onClickBody}>
          {type=='attachments'?items:null}
        </div>
      </div>
    );
  },

});

module.exports = IconBox;
