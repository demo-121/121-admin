let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;

let TextField = mui.TextField
let Dialog = mui.Dialog;
let FlatButton = mui.FlatButton;
let DatePicker = mui.DatePicker;
let DialogStore = require('../../stores/DialogStore.js');
let DialogActions = require('../../actions/DialogActions.js');
let ContentStore = require('../../stores/ContentStore.js');

var _values ={};
var defaultvalue = false;

let CustomDialog = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  propTypes: {

  },
  componentDidMount() {
    DialogStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function() {
    DialogStore.removeChangeListener(this.onChange);
  },

  onChange() {
    this.setState({
      values: DialogStore.getValues(),
    });
  },
  getInitialState() {
    this.state = {
      open: true,
      values : DialogStore.getValues()
    };

    return this.state;
  },

  submit: function(){
    this.props.content.fields.map(function(field) {
      switch (field.type) {
        case "textField":
          if(this.refs[field.id].getValue != null){
            _values[field.id] = this.refs[field.id].getValue()
          }
          break;

        case "date":
          if(this.refs[field.id].getDate != null){
            _values[field.id] = this.refs[field.id].getDate()
          }
          break;

          case "hidden":
            _values[field.id] = field.value;
            break;

        default:

      }
    }.bind(this))

    var data = {
      p0: convertObj2EncodedJsonStr(_values),
    }
    DialogStore.setClose();
    callServer(this.props.content.action, data);
  },
  handleOpen(){
    this.setState({open: true});
  },
  handleClose(){
    DialogStore.setClose();
    this.setState({open: false});
    ContentStore.getPageContent().dialog = null;
  },
  ontextFieldChange(event){
    this.state.values[event.target.id] = event.target.value ? event.target.value : "";
    DialogActions.updatevalues(this.state.values)
    //this.setState({values : _values})
  },
  render() {
    let content = this.props.content;
    let open = DialogStore.getOpen();
    let _values = this.state.values;

    let actions = [];
    content.actions.map(function(action) {
      switch (action.type) {
        case "submit":
          actions.push(
                <FlatButton
                  label={action.label}
                  primary={action.primary}
                  keyboardFocused={action.keyboardFocused}
                  onTouchTap={this.submit}/>
              )
          break;

          case "cancel":
            actions.push(
                  <FlatButton
                    label={action.label}
                    primary={action.primary}
                    keyboardFocused={action.keyboardFocused}
                    onTouchTap={this.handleClose}/>
                )
            break;
        default:

      }
    }.bind(this))

    if(!defaultvalue){
      content.fields.map(function(field) {
        switch (field.type) {
          case "textField":
            this.state.values[field.id] = field.value
            break;

          case "date":
            this.state.values[field.id] = new Date(field.value)
            break;

          default:
            break;
          }
      }.bind(this))
      DialogStore.setValues(this.state.values);
      defaultvalue =true;
    }
    return(
      <Dialog
        title={content.title}
        actions={actions}
        modal={false}
        open={open}
        onRequestClose={this.handleClose}
      >
        {
          content.fields.map(function(field) {
            switch (field.type) {
              case "textField":
                return (
                        <TextField
                          id = {field.id}
                          ref={field.id}
                          hintText={field.hint}
                          floatingLabelText={field.floatText}
                          multiLine = {field.multiLine}
                          value = {this.state.values[field.id]}
                          onChange = {this.ontextFieldChange} />
                      );
                break;

                case "date":
                  var dfDateValue = {
                    id: field.id,
                    value: this.state.values[field.id],
                    requestChange: function(value) {
                      this.state.values[field.id] = value;
                      DialogActions.updatevalues(this.state.values);
                    }.bind(this)
                  };

                  if (this.state.values[field.id] != 'Invalid Date') {
                    return (
                            <DatePicker
                              id = {field.id}
                              ref= {field.id}
                              hintText={field.hint}
                              floatingLabelText={field.floatText}
                              valueLink = { dfDateValue } />
                          );
                  } else {
                    return (
                            <DatePicker
                              id = {field.id}
                              ref= {field.id}
                              hintText={field.hint}
                              floatingLabelText={field.floatText}
                               />
                          );
                  }

                  break;
              default:
                break;
            }
          }.bind(this))
        }

      </Dialog>
    );
  }
});

module.exports = CustomDialog;
