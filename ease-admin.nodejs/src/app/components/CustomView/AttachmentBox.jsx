let React = require('react');
let mui = require('material-ui');

import EABFieldBase from './EABFieldBase.js';
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');
let DialogActions = require('../../actions/DialogActions.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let IconBox = require('./IconBox.jsx');
let SortableList = require('./SortableList.jsx');
let debug = true;
let _isMount = false;


module.exports = React.createClass({

  mixins: [EABFieldBase],

  componentWillUnmount(){
    _isMount=false;
  },

  componentDidMount(){
    _isMount=true;
  },

  propTypes:{

  },

  onUpdate(){
    if(_isMount)
      this.forceUpdate();
  },

  _genItem(changedValues){
    let items = [];
    let self = this;

    let {
      disabled
    } = this.props;

    let cValue = ContentStore.getPageContent().changedValues;
    if(!cValue.Attachments){
      cValue.Attachments = {};
    }

    let attachments = cValue.Attachments;

    let size = (checkExist(changedValues, 'boxRegularWidth'))? Number(changedValues.boxRegularWidth): 150;
    let id = this.props.id + '-ic-attachment';

    //let imageId = changedValues.image;
    let imageId = changedValues.image;
    if(!imageId){
      imageId = changedValues.image = getUniqueId();
    }

    if(!attachments[imageId]){
      attachments[imageId]="";
    }

    let openPanel = function(){
      //parent.setState({cfValues: options[i], deleteAction: delAction});
      DynConfigPanelActions.openPanel(changedValues, 'question', {onUpdate: self.onUpdate});
    }

    let changeImage = function(url){
      attachments[imageId] = url;
      changedValues.image = imageId;
    }

    items.push(
			<IconBox
        id={id}
        key={id}
        ref={id}
        image={attachments[imageId]}
        title={changedValues.title}
        size={150}
        disabled={disabled}
        type={"attachments"}
        onClickBody={openPanel}
        onDrop={changeImage}/>
    )
    return items;
  },


  render() {
    if(debug){
      console.log("render - attachments");
    }
    var self = this;
    let {
      changedValues
    } = this.state;

    let items = this._genItem(changedValues);

    return (
      <div>
        {items}
      </div>
    );
  }
});
