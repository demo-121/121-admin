/** In this file, we create a React component which incorporates components provided by material-ui */
let React = require('react');

let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let ToolTip = mui.Tooltip;
import appTheme from '../../theme/appBaseTheme.js';

let ReadOnlyField = React.createClass({

  mixins: [StylePropable],

  contextTypes: {
    muiTheme: React.PropTypes.object,
  },

  propTypes: {
    floatingLabelText: React.PropTypes.string,
    floatingLabelStyle: React.PropTypes.object,
    underline: React.PropTypes.bool,
    underlineStyle: React.PropTypes.object,
    labelStyle: React.PropTypes.object,
    id: React.PropTypes.string,
    autoWidth: React.PropTypes.bool,
    tooltip: React.PropTypes.array,
    target: React.PropTypes.object,
    onClick: React.PropTypes.func,
    defaultValue: React.PropTypes.string,
    icon: React.PropTypes.object,
    subType: React.PropTypes.string,
    mandatory: React.PropTypes.bool,
  },

  //for passing default theme context to children
  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme,
    };
  },

  getDefaultProps() {
    return {
      fullWidth: false,
      target: null,
      onClick: null,
      defaultValue: '-',
      underline: false
    };
  },

  getContextProps() {
    const theme = this.context.muiTheme;

    return {
      isRtl: theme.isRtl,
    };
  },

  getInitialState() {
    let props = (this.props.children) ? this.props.children.props : this.props;
    return {
      showTooltips: false,
      muiTheme: this.context.muiTheme ? this.context.muiTheme : appTheme.getTheme(),
    }
  },

  getStyles() {
    let theme = this.state.muiTheme;

    var compact = this.props.compact && !this.props.floatingLabelText

    let styles = {
      root: {
        height: compact?60:80,
        position: 'relative',
        fontSize: 16,
        top: compact?1:(!this.props.floatingLabelText?-8:0)
      },
      label: {
        paddingLeft: 0,
        lineHeight: compact?'60px':'80px',
        display: 'block',
        position: 'relative',
        color: theme.textField.textColor,
        width: '100%',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis'
      },
      underline: {
        marginTop: 42,
        borderTop: 'solid 1px',
        borderColor: theme.textField.borderColor
      },
      titleStyle: {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      },
      floatingLabel: {
        lineHeight: '24px',
        top: compact?18:28,
        bottom: 'none',
        opacity: 1,
        color: theme.textField.fixedLabelColor,
        transform: 'perspective(1px) scale(0.875) translate3d(0px, -28px, 0)',
        transformOrigin: 'left top',
        position: 'absolute'
      }
    };
    return styles;
  },

  render() {
    var self = this;
    let styles = this.getStyles();
    let {
      id,
      className,
      style,
      labelStyle,
      underline,
      underlineStyle,
      floatingLabelText,
      floatingLabelStyle,
      defaultValue,
      fullWidth,
      tooltip,
      icon,
      compact,
      subType,
      mandatory
    } = this.props;

    // var star = mandatory?<span style={ {color:'red'} }>&nbsp;*</span>:null;
    let labelTextElement = floatingLabelText ? (
      <label key="label"
        style={ this.mergeAndPrefix(styles.floatingLabel, floatingLabelStyle) } >
        <span style={ styles.titleStyle }>{floatingLabelText}</span>
      </label>
    ) : null;

    if(subType && subType == 'password'){
      defaultValue = '********';
    }

    if (!defaultValue) defaultValue = "-";

    let valueTextElement = defaultValue ? (
      icon ? (
      <label key="value"
        style={ this.mergeAndPrefix(styles.label,{ display:'inline-flex',
          alignItems:'center'}) } >
        <span key="inner" style={ this.mergeAndPrefix(styles.label, {top: null}) }>{defaultValue}</span>
        { icon }
      </label>
      ):(
      <label key="value"
        style={ this.mergeAndPrefix(styles.label) } >
        {defaultValue}
      </label>)
    ) : null;

    let underlineElement = underline?<hr key="hr" style={this.mergeAndPrefix(styles.underline, underlineStyle)}/>:null;

    var handleTap = function() {
      if (self.props.onClick && typeof self.props.onClick == 'function') {
        self.props.onClick();
      }
    }

    var tooltipBlock = null;
    if (tooltip) {
      tooltipBlock = <ToolTip
        key = {"tt_"+id}
        label = {tooltip}
        show = {this.state.showTooltips}
        />
    }

    var _handleHoverExit = function (e) {
      this.parent.setState({showTooltips: false});
    }

    var _handleHover = function (e) {
      this.parent.setState({showTooltips: true});
    }

    return (
      <div key="item"
        className={className}
        style={this.mergeAndPrefix(styles.root, style)}
        onClick={handleTap}
        onMouseLeave = {tooltip?_handleHoverExit.bind({parent: self}):null}
        onMouseEnter = {tooltip?_handleHover.bind({parent: self}):null}
        >
        {tooltipBlock}
        {labelTextElement}
        {valueTextElement}
        {underlineElement}
      </div>
    );
  },
});

module.exports = ReadOnlyField;
