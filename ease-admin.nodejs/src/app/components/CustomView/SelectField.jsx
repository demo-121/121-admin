let React = require('react');
let mui = require('material-ui');

let SelectFieldView = mui.SelectField;
let MenuItem = mui.MenuItem;
let StylePropable = mui.Mixins.StylePropable;

let SelectField = React.createClass({
  mixins: [StylePropable],
  styles: {
    optionItemStyle: {
      fontSize:'14px',
      display: 'inline-block',
      width: '100%'
    }
  },

  handleChange: function(e, index, value){
    if (this.props.handleChange) this.props.handleChange(this.props.template.id, value);
  },

  init() {
    this.addOption();
  },

  addOption(){
    var {
      lang,
      langMap,
      optionsMap
    } = this.context;

    var {
      template,
      changedValues,
      options,
      rootValues,
      resetValueItems
    } = this.props;

    var {
      id,
      min,
      max,
      value,
      subType,
      interval,
      placeholder,
      reference,
      setAll,
      mandatory
    } = template;

    options = options || template.options;

    if (typeof options == 'string' && optionsMap && optionsMap[options]) {
      options = optionsMap[options].options
    }

    if (!options) {
      options = [];
    }

    if (options.length == 0 && (min || min === 0) && max) {
      if (!interval) interval = 1;

      min = parseInt(min);
      max = parseInt(max);

      for (let i = min; i <= max; i+=interval) {
        options.push({
          title: ""+i,
          value: i
        })
      }
    }
    var rbs = [];

    var filteredOptions = [];

    // for (let i in options){
    //   var opt = options[i];
    //   if (optionSkipTrigger(template, opt, changedValues, resetValueItems, rootValues)) {
    //     continue;
    //   }
    //   filteredOptions.push(opt);
    // }

    options = options.filter(function(opt) {
      return !optionSkipTrigger(template, opt, changedValues, resetValueItems, rootValues)
    })

    if (setAll === true || setAll === 'Y') {
      if (!options.find(function(ele) { return ele.value == '*'})) {
        options.splice(0, 0, {
          title: "ALL",
          value: "*"
        })
      }
    } else {
      options = options.filter(function(ele) {
        return ele.value != '*';
      })
    }

    if (mandatory === false || mandatory === 'N') {
      if (!options.find(function(ele) { return ele.value == '0'})) {
        options.splice(0, 0, {
          title: " - ",
          value: "0"
        })
      }
    } else {
      options = options.filter(function(ele) {
        return ele.value != '0';
      })
    }

    for (var i in options) {
      var option = options[i];
      var _label = option.label;

      if (_label != undefined)
        _label = _label.replace('{0}', option.title);

      var otitle = getLocalText("en", option.title);

      rbs.push(<MenuItem
        key={option.value}
        className="inline-blk"
        label={_label}
        value={option.value}
        primaryText={otitle}
        style={this.mergeAndPrefix(this.styles.optionItemStyle, option.highLight?{color:'#006600'}:{})}></MenuItem>);
    }

    this.optionsItems = rbs
  },

  componentWillMount: function() {
    this.init();
  },

  render: function() {

    var {
      template,
      title,
      value,
      compact,
      changedValues,
      handleChange,
      options,
      ...others
    } = this.props;

    var {
      id,
      min,
      max,
      interval,
      mandatory,
      setAll,
      placeholder
    } = template;

    //refresh options when options changed
   this.addOption();

    if (isEmpty(value) && id && changedValues) {
      value = changedValues[id] || template.value || (mandatory?(setAll === true || setAll === 'Y'?"*":""):"na");
    }
    changedValues[id] = value;

    var errorText = validate(template, changedValues);

    return (<SelectFieldView
      name={id}
      floatingLabelText = { !compact?getLocalText('en', title || template.title):null }
      value= { value}
      hintText= { placeholder }
      hintStyle= {{ bottom: '24px'}}
      errorText = { errorText }
      autoWidth = {true}
      onChange={this.handleChange}
      {...others}>{ this.optionsItems }</SelectFieldView>);
  }
});
module.exports = SelectField;
