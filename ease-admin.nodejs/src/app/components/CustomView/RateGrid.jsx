let React = require('react');
let mui = require('material-ui');

let FlatButton = mui.FlatButton;

let Dropzone = require('../CustomView/DropZone.jsx');
let RateHandsonTable = require('../CustomView/RateHandsonTable.jsx');

let ContentStore = require('../../stores/ContentStore.js');

import EABFieldBase from './EABFieldBase.js';
import appTheme from '../../theme/appBaseTheme.js';

let EABRateGrid = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    buttonStyle: {
      marginLeft: 16,
      color: appTheme.palette.primary2Color,
      backgroundColor:appTheme.palette.background2Color,
      borderWidth: 1,
      borderStyle: 'solid',
      borderColor: appTheme.palette.borderColor
    }
  },


  importFunc(resp){
    var {
      template
    } = this.props;

    var {
      id
    } = template;

    // console.debug("import res", res);
    var content = resp.imageUrl;
    // console.debug("import content", content);
    var encodedStr = content.substring(content.search(",") + 1, content.length);
    // console.debug("import encodedStr", encodedStr);
    if (encodedStr.length > 0) {
      var grid = this.refs[id];

      var decodedStr = window.atob(encodedStr);
      var _csvStr = decodedStr.replace(/(\r\n)/gm, "\n");

      //detect direction
      var table = _csvStr.split("\n");
      var tableLength = table.length;
      if(tableLength > 1){
        var firstRow = table[1].split(",");
        var firstCell = firstRow[0];
        //if vertical
        if(firstCell === 0 || isNumber(firstCell)){
          for(var i=0; i<tableLength; i++){
            table[i] = table[i].split(",");
          }
          var _keys = table[0];
          var keySize = _keys.length;
          var tempTable = [];
          for(var i=0; i<keySize; i++){
            var tempStr = _keys[i];
            for(var j=1; j<tableLength; j++){
              var cellValue = table[j][i];
              if(cellValue === 0 || isNumber(cellValue)){
                tempStr += "," + cellValue;
              }else if(cellValue != null && cellValue != ""){
                alert("invalid data", "error");
              }
            }
            tempTable.push(tempStr);
          }
          _csvStr = tempTable.join("\n");
        }
      }

      var data = $.csv.toObjects(grid.colHeadersId.join(",") + "\n" +_csvStr);
      this.requestChange(data);
    }
  },

  exportFunc(e, target){
    var {
      template,
      changedValues
    } = this.props;

    var{
      id
    } = template;

    var grid = this.refs[id];

    var {
      colHeadersId
    } = grid;

    var data = cloneArray(changedValues[id]);

    if (data && data.length > 0) {
      grid.exportCsv({ data: data, fields: colHeadersId, hasCSVColumnTitle: false });
    }
  },

  onValueChange(value){
    var {
      template,
      changedValues
    } = this.props;

    changedValues[template.id] = value;
  },

  componentWillMount(){
    var {
      template,
      changedValues
    } = this.props;

    var {
      id
    } = template;

    if(isEmpty(changedValues[id])){
      changedValues[id] = [{}];
    }
  },

  render() {
    var self = this;
    var {
      template,
      width,
      height,
      handleChange,
      disabled,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      type,
      insertable,
      title,
      value,
    } = template;


    // Import
    var import_ = (disabled)?null:(
      <Dropzone
        key= {"dz"+id}
        onDrop={this.importFunc}
        accept=".csv" >
        <FlatButton key= {"impBtn"+id} style={this.styles.buttonStyle} label={getLocalizedText("HANDSONTABLE_BUTTON.IMPORT","Import")} />
      </Dropzone>
    );

    // Export
    var export_= (
      <FlatButton
        key= {"expbtn"+id}
        style={this.styles.buttonStyle}
        label={getLocalizedText("HANDSONTABLE_BUTTON.EXPORT","Export")}
        onTouchTap={this.exportFunc} />
    );
    //



    return (
      <div style={{height: '100%'}}>
        {
          id=="fund"?
          null:
          <div key="subActions" style={{width:'100%', height:'66px'}}>
            { title ? <span className="fixWidthSpan" style={{
              fontSize: '20px',
              lineHeight: '66px',
              padding: '0 24px',
              width: '300px',
              fontWeight: 300
            }}>{title}</span>:null }
            <div style={{float:'right', padding:'14px 24px'}}>
              {import_}
              {export_}
            </div>
          </div>
        }
        <RateHandsonTable
          key= {"ht"+id}
          ref={id}
          height= {height || "100%"}
          template={template}
          gridData={changedValues[id]}
          disabled={disabled}
          onChange={this.onValueChange} />
      </div>
    );
  }
});

module.exports = EABRateGrid;
