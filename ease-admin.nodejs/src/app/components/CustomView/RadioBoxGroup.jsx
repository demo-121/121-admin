let React = require('react');
let mui = require('material-ui');

let RadioButton = mui.RadioButton;
let RadioButtonGroup = mui.RadioButtonGroup;

import EABFieldBase from './EABFieldBase.js';

let CheckboxGroup = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    checkBoxLabelStyle: {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      width: 'calc(100% - 40px)',
      fontSize: '16px'
    }
  },

  getCheckBoxValues: function(cbValues){
    var str = "";
    if (cbValues && cbValues.length) {
      for (let i = 0; i < cbValues.length; i++){
        if (cbValues[i]) {
          str += (str?",":"") + cbValues[i];
        }
      }
    }
    return str;
  },

  render: function() {
    var self = this;
    var {
      template,
      changedValues,
      handleChange,
      disabled,
      ...others
    } = this.props;

    var {
      id,
      title,
      value,
      subType,
      placeholder,
      disabled,
      presentation
    } = template;
    
    var {
      options
    } = this.state

    var cValue = self.getValue("");

    var errorMsg = validate(template, changedValues);
    errorMsg = errorMsg?<div style={{
      color: this.state.muiTheme.textField.errorColor,
      height: '16px',
      fontSize: '12px',
      position: 'absolute',
      left: '24px',
      top: title?'42px':null,
      bottom: title?null:'4px'
    }}>{errorMsg}</div>:null;

    if (options && options.length > 0) {
      var radios = [];
      for (let j = 0; j < options.length; j++) {
        var option = options[j];
        var otitle = getLocalText("en", option.title);
        radios.push(
          <RadioButton
            className = {"CheckBox"}
            value={option.value}
            label={otitle}
            disabled={disabled} />
        );
      }
    }

    return (
      <div className={this.checkDiff()} style={{position:'relative'}}>
        {this.getHeaderTitle()}
        <RadioButtonGroup
          name = {id}
          className={"RadioBoxGroup"+ (presentation?" "+presentation:"")}
          defaultSelected={cValue}
          onChange={
            function(e, value) {
              self.requestChange(value);
            }
          } >
          {radios}
        </RadioButtonGroup>
      {errorMsg}
    </div>
    );
  }

});
module.exports = CheckboxGroup;
