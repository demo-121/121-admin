let React = require('react');
let mui = require('material-ui');

let FlatButton = mui.FlatButton;
let FontIcon = mui.FontIcon;
let IconButton = mui.FontIcon;

let EABFieldBase = require('./EABFieldBase.js');
let ReadOnlyField = require('./ReadOnlyField.jsx');
let DialogActions = require('../../actions/DialogActions.js');

let EABTable = require('./Table.jsx');
let EABSection = require('./Section.jsx');
let appTheme = require('../../theme/appBaseTheme.js');

module.exports = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    subLineStyle: {
      backgroundColor: appTheme.palette.borderColor,
      height:1,
      margin:'20px 0'
    },
    boxStyle: {
      backgroundColor: appTheme.palette.borderColor,
      borderRadius: '16px',
      cursor:'pointer',
      fontSize: '14px',
      border: "1px solid white"
    }
  },

  getDefaultProps() {
    return {
      allowAdd: true
    }
  },

  render() {
    var self = this;
    var {
      template,
      width,
      values,
      inDialog,
      handleChange,
      rootValues,
      allowAdd,
      index,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      type,
      title,
      presentation,
      mandatory,
      items,
      value,
      min,
      max,
      interval,
      subType
    } = template;

    // var cValue = (id && changedValues && changedValues[id])?changedValues[id]:(value?value:{});

    var cValue = self.getValue([]);

    var oValue = (id && values && !isEmpty(values[id]))?values[id]:[];

    // var label = "add";
    var cancelBtn = getLocalizedText('BUTTON.CANCEL', "Cancel");
    var addBtn = getLocalizedText('BUTTON.ADD', "Add");
    var saveBtn = getLocalizedText('BUTTON.SAVE', "Save");
    var deleteBtn = getLocalizedText('BUTTON.DELETE', "Delete");

    var subItems = [];

    var sortedItems = items.sort(function(a,b) {
      var aa = a.detailSeq?a.detailSeq:0;
      var bb = b.detailSeq?b.detailSeq:0;
      return (aa < bb) ? -1 : (aa > bb) ? 1 : 0;
    });

    var handleTap = function() {


      var dialogValues = {}
      if (cValue && this.index != undefined && typeof cValue[this.index] == 'object') {
        dialogValues = cloneObject(cValue[this.index]);
      } else {
        // if it is already in dialog, just add row
        if (inDialog) {
          cValue.push(dialogValues);
          changedValues[id] = cValue;
          self.forceUpdate()
          return;
        }
      }
    //console.debug('on handleTap add:', self.state.changedValues, self.props.rootValues);

      // construct the dialog
      DialogActions.showDialog({
        title: getLocalText('en', template.title || template.hints),
        message: this.message,
        items: this.items,
        values: dialogValues,
        rootValues: rootValues,
        handleChange: function() {

        },
        actions: [{
          type: 'customButton',
          float: 'left',
          title: deleteBtn,
          disabled: !self.props.allowAdd,
          hide: this.index == undefined,
          handleTap: function() {
            if (this.index) {
              cValue.splice(this.index, 1);
            }
            self.handleChange(self.state.changedValues);
          }.bind(this)
        }, {
          type: 'cancel',
          title: cancelBtn
        }, {
          type: 'customButton',
          title: this.doneTitle,
          handleTap: function( values ) {
            // if set it as default, reset others
            if (values.default == 'Y') {
              for (let i in cValue) {
                if (i != this.index) {
                  cValue[i].default = 'N';
                }
              }
            }
            // check if key value already exists
            var keys = [];
            for (var i in items) {
              if (items[i].isKey) {
                keys.push(items[i]);
              }
            }
            if (keys && keys.length) {
              for (var i in cValue) {
                if ( i != this.index ) {
                  var allSame = true;
                  for (var k in keys) {
                    var key = keys[k].id;
                    if (cValue[i][key] != values[key]) {
                      allSame = false;
                    }
                  }

                  if (allSame) {
                    alert("It is having duplicate key with item "+i+".", "Warning");
                    return false;
                  }
                }
              }
            }
            // update new values
            if (this.index) {
              cValue[this.index] = values;
            } else {
              cValue.push(values);
            }
            changedValues[id] = cValue;
            self.handleChange(changedValues);
            return true;
          }.bind(this)
        }]
      });
    }
    var disabled = disableTrigger(template, null, null, subValues, rootValues);

    if (!subType || "FIELD" == subType.toUpperCase()) {
      if (cValue instanceof Array && cValue.length > 0) {
        var showField = presentation?presentation.toUpperCase():null;

        // generate sub field title
        var subTitle = "";
        var errMsg = "";
        for (let si in sortedItems) {
           var sItem = sortedItems[si];
           if (!showField || showField.indexOf(sItem.id.toUpperCase()) >= 0) {
               if (sItem.id != 'default') {
                 subTitle += (subTitle?", ":"") + sItem.title;
               }
           }
        }
        var self = this;

        var goThruItem = function(sItem, values, valueStr) {
          var subV = self._getDisplayValue(sItem, values[sItem.id], values);
          if (!showField || showField.indexOf(sItem.id.toUpperCase()) >= 0) {
            if (subV == undefined) subV = '-';
            if (sItem.id == 'default') {
              valueStr += subV;
            } else {
              valueStr += (valueStr?", ":"") + subV;
            }
          } else {
            // sp handle for checkbox
            if (sItem.type.toUpperCase() == 'CHECKBOXGROUP') {
              var cbValues = subV.split(",");
              var ttp = ""
              for (let cbi in cbValues) {
                var cbl = self._getDisplayValue(sItem, cbValues[cbi]);
                ttp += (ttp?", ":"") + cbl;
              }

              if (ttp) {
                tooltip = sItem.title + ": " + ttp;
              }
            }
          }
          if (sItem.items) {
            for (let sii in sItem.items) {
              var siitem = sItem.items[sii];
              valueStr = goThruItem(siitem, values, valueStr);
            }
          }
          return valueStr
        }

        // generate sub field for each cValue
        for (let si in cValue) {
          var subValues = cValue[si];
          var valueStr = "";
          var tooltip = null;

          // concate the subValues
          for (let sii in sortedItems) {
            var sItem = sortedItems[sii];
            valueStr = goThruItem(sItem, subValues, valueStr);
          }

          var diffC = false;
          if (self.props.requireShowDiff) {
            if (!oValue) {
              diffC = true
            } else {
              var hasSame = false;
              for (let oi in oValue) {
                if (!checkDiff(template, oValue[oi], subValues)) {
                  hasSame = true;
                  break;
                }
              }
              diffC = !hasSame;
            }
          }


          subItems.push(
            <ReadOnlyField
              key = { id+si }
              className = {"DetailsItem" + (diffC?" diff":"")}
              style= {{width: width?width:null, backgroundColor:null}}
              floatingLabelText = {subTitle}
              underline = {true}
              tooltip = {tooltip}
              defaultValue = { valueStr }
              onClick = {
                disabled?null:handleTap.bind({items: sortedItems, values: cValue, index: si, doneTitle: saveBtn })
              }/>
          )
        }
      }
    } else
    if ("INLINE" == subType.toUpperCase()){
        var rows = []
        if (cValue instanceof Array && cValue.length > 0) {

          var goThruItem = function(cell, values, retValue) {
            var cvalue = values[cell.id];
            if (cell.options) {
              for (var o in cell.options) {
                if (cell.options[o].value == cvalue) {
                  cvalue = getLocalText('en', cell.options[o].title)
                }
              }
            }
            if (cell.subType && cell.subType == 'currency') {
              cvalue = getCurrency(cvalue, "$", 2);
            }
            retValue = retValue.replace('['+cell.id+']', cvalue);

            if (cell.items) {
              for (var ci in cell.items) {
                var scell = cell.items[ci];
                retValue = goThruItem(scell, sv, retValue);
              }
            }
            return retValue;
          }

          for (var c in cValue) {
            var sv = cValue[c];
            var valPresent = presentation;

            for (var ci in sortedItems) {
              var cell = sortedItems[ci];
              valPresent = goThruItem(cell, sv, valPresent);
            }

            // if (!header) {
            //   header = <div key={"hr"} className="row">{hcells}</div>
            // }
            if (c != "0") {
              rows.push(<span style={{display: "inline-block",
                verticalAlign: "top"}}>, </span>)
            }

            rows.push(<span key={"r"+c} className="inlineCell" style={self.styles.boxStyle} onClick={
              disabled?null:handleTap.bind({items: sortedItems, values: cValue, index: c, doneTitle: saveBtn })
            }>{valPresent}</span>)
          }
        }
        if (allowAdd) {
          rows.push(<IconButton
            style = { self.styles.boxStyle }
            onClick = { handleTap.bind({items: sortedItems, values: cValue, doneTitle: addBtn}) }
            ><FontIcon className="material-icons" style={{color:appTheme.palette.themeGreen}}>add</FontIcon>
          </IconButton>)
        }
        return <div key={"icl_"+id} className="inlineCompList">{rows}</div>
    } else if ("TABLE" == subType.toUpperCase()){
      var iii = 1;
      subItems.push(<EABTable
        key = {"table_" + id}
        template = { template }
        values = { values }
        rootValues = { rootValues }
        changedValues = { changedValues }
        deleteAble = { true }
        handleUpdateRow = { disabled?null:function(row) {
          handleTap.bind({items: sortedItems, values: cValue, index: row, doneTitle: saveBtn })()
        }}
        handleChange = {function() {
          self.forceUpdate();
        }}
        />)
    }

    else if ("SECTION" == subType.toUpperCase()){
      subItems.push(<EABSection
        key = {"section_" + id}
        ref = {"section_" + id}
        template = { template }
        values = { values }
        rootValues = { rootValues }
        changedValues = { changedValues }
        deleteAble = { true }
        sortable = { true }
        genItemsFunc = { this.props.genItemsFunc }
        />)
    }

    var errorMsg = validate(template, changedValues);
    errorMsg = errorMsg?<div style={{
      color: this.state.muiTheme.textField.errorColor,
      height: '16px',
      fontSize: '12px',
      width: '100%',
      paddingLeft: "24px"
    }}>{errorMsg}</div>:null;

    var valueLen = oValue?oValue.length:0;
    var cValueLen = cValue?cValue.length:0;
    var dd = valueLen - cValueLen;
    if (dd < 0) dd = 0;
    subItems.push(
      <div key={id+"empty"}
        className="DetailsItem diff"
        style={{
          height: (dd * 80)+'px',
          paddingTop: '0px'
        }}/>)

    return (
      <div className="ListField">
        {!subType?<div key={"sps_"+id} className="DetailsItem" style={{paddingRight: 0}}>
          <div style={ this.styles.subLineStyle }></div>
        </div>:null}
        <div key= {"ctn"+id} style={{position:'relative'}}>
          {subItems}
          {errorMsg}
          {(allowAdd === undefined || allowAdd)?<FlatButton
            key={id+"btn"}
            labelPosition = "after"
            className = { "DetailsItem" }
            style={ !subType || subType.toUpperCase()!='TABLE'?{ margin: '6px 24px' }:{ margin: '6px 24px', width: 'calc(100% - 48px)'} }
            onClick = { (subType && subType.toUpperCase()=='SECTION')?()=>{this.refs['section_'+id].addRow()}:handleTap.bind({items: sortedItems, values: cValue, doneTitle: addBtn}) }
            >
            {!subType?<div style={{display:'flex', alignItems:'center'}}>
              <FontIcon className="material-icons" style={{color:appTheme.palette.text1Color}}>add</FontIcon>
              <span style={{padding: '0 15px'}}>{addBtn}</span>
            </div>:((subType.toUpperCase() == 'SECTION' || subType.toUpperCase()=='TABLE')?<span style={{padding: '0 15px', width: '100%'}}>{addBtn}</span>:null)}
          </FlatButton>:null}
        </div>
        {!subType?<div key={"spe_"+id} className="DetailsItem" style={{paddingRight: 0, paddingTop: 0}}>
          <div style={ this.styles.subLineStyle }></div>
        </div>:null}
      </div>
    )
  }
});
