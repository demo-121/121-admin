let React = require('react');
let mui = require('material-ui');

let TextArea = require('./TextArea.jsx');
import EABFieldBase from './EABFieldBase.js';

let FlatButton = mui.FlatButton;

let CssEditor = React.createClass({
  mixins: [EABFieldBase],

  beauty(){
    var {
      changedValues
    } = this.state;


    var cValue = this.getValue("")
    var output = '';
    var opts = {};

    opts.indent_char = opts.indent_size == 1 ? '\t' : ' ';
    opts.preserve_newlines = opts.max_preserve_newlines !== "-1";

    this.requestChange(css_beautify(cValue, opts));
  },

  render(){
    var self = this;
    var {
      changedValues
    } = this.state;

    var {
      template,
      values,
      disabled,
      rootValues
    } = this.props;

    var {
      id
    } = template;


    disabled = disabled || template.disabled || disableTrigger(template, null, values, changedValues, rootValues)

    return(
      <div style={{padding: '24px'}}>
        <div style={{textAlign: 'right', height: '36px'}}>
          {disabled?null:<FlatButton label="beautify" onClick={this.beauty}/>}
        </div>
        <TextArea
          template={{id: id, type: 'TEXT', disabled: disabled}}
          changedValues={changedValues}/>
      </div>
    )
  }
});

module.exports = CssEditor;
