let TinyMCE = require('react-tinymce');
let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;

let TextArea = require('./TextArea.jsx');
import EABFieldBase from './EABFieldBase.js';

let Tabs = mui.Tabs;
let Tab = mui.Tab;
let FlatButton = mui.FlatButton;
let Divider = mui.Divider;

/*
  rs, res : searchValue of start, end
  _rs, _res: newvalue of start, end
  sf, sb: skipValue of front, end
  af, ab: addValue of front, end
  afIf, abIf: condition of af, ab

  note:
    if sf and sb exist, and both rs and res inside sf and sb, the string between rs and sf, res and sb would be remove,
    if rs and res inside afIf and abIf, add af before rs and ab behind res
*/
let src2Html = [
  {rs:'<xsl:value-of select', _rs:'{{value-of', res:'/>', _res:'}}'},
  {rs:'<xsl:if test', _rs:'{{if:test', res:'>', _res:'}}'},
  {rs:'</xsl:if>', _rs:'{{if:end}}'},
  {rs:'<xsl:choose>', _rs:'{{choose}}', res:'</xsl:choose>', _res:'{{choose:end}}'},
  {rs:'<xsl:when', _rs:'{{when:test', res:'>', _res:'}}'},
  {rs:'</xsl:when>', _rs:'{{when:end}}'},
  {rs:'<xsl:otherwise>', _rs:'{{otherwise}}', res:'</xsl:otherwise>', _res:'{{otherwise:end}}'},
  {rs:'<xsl:for-each select', _rs:'{{for-each', res:'>', _res:'}}', af:'<tr><td>', ab:'</td></tr>', afIf:'<table', abIf:'</table>'},
  {rs:'</xsl:for-each>', _rs:'{{for-each:end}}', af:'<tr><td>', ab:'</td></tr>', afIf:'<table', abIf:'</table>'}
];
let html2Src = [
  {_rs:'<xsl:value-of select', rs:'{{value-of', _res:'/>', res:'}}'},
  {_rs:'<xsl:if test', rs:'{{if:test', _res:'>', res:'}}'},
  {_rs:'</xsl:if>', rs:'{{if:end}}'},
  {_rs:'<xsl:choose>', rs:'{{choose}}', _res:'</xsl:choose>', res:'{{choose:end}}'},
  {_rs:'<xsl:when', rs:'{{when:test', _res:'>', res:'}}'},
  {_rs:'</xsl:when>', rs:'{{when:end}}'},
  {_rs:'<xsl:otherwise>', rs:'{{otherwise}}', _res:'</xsl:otherwise>', res:'{{otherwise:end}}'},
  {_rs:'<xsl:for-each select', rs:'{{for-each', _res:'>', res:'}}', sf:'<tr', sb:'</tr>'},
  {_rs:'</xsl:for-each>', rs:'{{for-each:end}}', sf:'<tr>', sb:'</tr', af:'<tr><td>'}
];

let convert = function(html, convArr){
  if(!(!isEmpty(html) && convArr instanceof Array && convArr.length>0)){
    return html;
  }

  let result = cloneObject(html);

  for(var i in convArr){
    var convObj = convArr[i];

    if(isEmpty(convObj)){

    }else{
      var {
        rs, _rs, res, _res, sf, sb, af, ab, afIf, abIf
      } = convObj;
      if(res && _res){
        var tempStr = '';
        var _htmls = result.split(rs);
        if(_htmls.length>1){
          var index = -1;
          for(var i=1; i<_htmls.length; i++){
            if(_htmls[i].indexOf(res)>-1){
              if(sf && sb){
                if(_htmls[i-1].indexOf(sf) > _htmls[i-1].indexOf(sb) && _htmls[i].indexOf(sb)>_htmls[i].indexOf(res)){
                  _htmls[i-1] = _htmls[i-1].substring(0, _htmls[i-1].indexOf(sf));
                  _htmls[i] = _htmls[i].substring(0, _htmls[i].indexOf(res) + res.length) + _htmls[i].substring(_htmls[i].indexOf(sb) + sb.length, _htmls[i].length);
                }
              }
              if(af && ab){
                var isAdd = true;
                if(afIf && abIf && !(_htmls[i-1].indexOf(afIf)>-1 && _htmls[i].indexOf(abIf)>_htmls[i].indexOf(res))){
                  isAdd=false;
                }
                if(isAdd){
                  _htmls[i-1] = _htmls[i-1] + af;
                  _htmls[i] = _htmls[i].substring(0, _htmls[i].indexOf(res)+res.length)+ ab + _htmls[i].substring(_htmls[i].indexOf(res)+res.length, _htmls[i].length);
                }
              }
              _htmls[i] = _htmls[i].replace(res, _res);

            }else{
              index = i;
            }
          }
          if(index > -1){
            result = _htmls[0];
            for(var i=1; i<_htmls.length; i++){
              result += (i<=index?_rs:rs) + _htmls[i];
            }
          }else{
            result = _htmls.join(_rs);
          }
        }
      }else{
        result = result.replaceAll(rs, _rs);
      }

    }
  }
  return result;

};

let Editor = React.createClass({
  mixins:[StylePropable],
    onUpdate(){
    var {
      template,
      changedValues
    } = this.props;

    var {
      id
    } = template;

    var self = this;

    var cValue = changedValues[id];
    if(!cValue){
      cValue = changedValues[id] = '<p></p>';
    }
    cValue = convert(cValue, src2Html);
    if(this.editor)this.editor.setContent(cValue);
  },

  getInitialState: function() {
    return({init: false});
  },

  setUpFunc(editor){
    this.editor = editor;
    editor.addButton('xslButton', {
      type: 'menubutton',
      text: 'XSL',
      icon: false,
      menu:[
        {
          text: 'value-of',
          onClick: function(){
            editor.insertContent('{{value-of="@@value"}}');
          }
        },
        {
          text: 'if',
          onClick: function(){
            editor.insertContent('{{if:test="@@condition"}}@@case{{if:end}}');
          }
        },
        {
          text: 'choose',
          onClick: function(){
            editor.insertContent('{{choose}}{{when:test="@@condition"}}@@case{{when:end}}{{otherwise}}@@case{{otherwise:end}}{{choose:end}}');
          }
        },
        {
          text: 'for-each:start',
          onClick: function(){
            editor.insertContent('{{for-each="@@value"}}');
          }
        }
        ,
        {
          text: 'for-each:end',
          onClick: function(){
            editor.insertContent('{{for-each:end}}');
          }
        }
      ]

    })
  },

  componentWillReceiveProps(nextProps){
    var {
      template,
      changedValues
    } = nextProps;

    var {
      id,
      disabled
    } = template;
    if(!this.state.init){
      this.state.init = true;
    }else if(this.editor){
      var cValue = changedValues[id];
      if(!cValue){
        cValue = changedValues[id] = '<p></p>'
      }
      if(this.editor.getContent() != cValue){
        this.editor.setContent(convert(cValue, src2Html));
      }
    }
  },

  onChange(value){
    var {
      template,
      changedValues
    } = this.props;

    var {
      id
    } = template;

    value = value.replaceAll(/&nbsp;/g,' ');

    changedValues[id] = convert(value, html2Src);

  },

  render() {
    var {
      template,
      editorStyle,
      contentStyle,
      changedValues
    } = this.props;

    var {
      id,
      disabled
    } = template;

    var self = this;

    var cValue = changedValues[id];
    if(!cValue){
      cValue = changedValues[id] = '<p></p>'
    }
    //
    // if(this.editor){
    //   this.editor.setContent(cValue);
    // }
    if(disabled){
      return (
        <div>
          <div style={this.mergeAndPrefix({border: 'solid 1px', borderRadius: '2px', width: '100%'}, editorStyle)} dangerouslySetInnerHTML={{ __html: cValue }}/>
        </div>
      )
    }else{
      return (
        <div>
          <TinyMCE
            content={cValue}
            config={{
              plugins: ['autolink link image lists preview', 'searchreplace visualblocks', 'insertdatetime media table contextmenu paste code textcolor colorpicker'],
              toolbar: 'undo redo | forecolor backcolor | bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | link image | xslButton',
              resize: false,
              height: (editorStyle && editorStyle.height)?editorStyle.height: 'calc(100vh - 300px)',
              content_style: contentStyle,
              setup: this.setUpFunc
            }}
            onChange={function(e){
              self.onChange(e.target.getContent());
            }}
          />
        </div>
      );
    }
  }
});



let HtmlEditor = React.createClass({
  mixins: [EABFieldBase],

  getInitialState() {
    return {
      tabIndex: 0
    }
  },

  handleChangeTabs(value){
    this.setState({tabIndex: value});
  },

  beauty(){
    var {
      template
    } = this.props;

    var {
      changedValues
    } = this.state;

    var {
      id
    } = template;

    var source = changedValues[id];
    var output = '';
    var opts = {};

    opts.indent_char = opts.indent_size == 1 ? '\t' : ' ';
    opts.preserve_newlines = opts.max_preserve_newlines !== "-1";

    changedValues[id] = html_beautify(source, opts);
    this.refs.tEditor.onUpdate();
  },


  render(){
    var self = this;
    var {
      template,
      contentStyle
    } = this.props;

    var {
      changedValues
    } = this.state;

    var {
      id,
      disabled
    } = template;

    var {
      tabIndex
    } = this.state;

    var hEditor = (
      <div>
        <div style={{textAlign:'right', height: '36px'}}/>
        <Editor
          ref="hEditor"
          editorStyle={{height: disabled?'calc(100vh - 256px)':'calc(100vh - 348px)'}}
          template={template}
          changedValues={changedValues}
          contentStyle={(contentStyle?contentStyle:'')+'position:relative;'}/>
      </div>
    );

    var tEditor = (
      <div>
        <div style={{textAlign:'right', height: '36px'}}>
          {disabled?null:<FlatButton label="beautify" onClick={this.beauty}/>}
        </div>
        <TextArea
          ref="tEditor"
          editorStyle={{height: disabled?'calc(100vh - 256px)':'calc(100vh - 240px)'}}
          template={template}
          changedValues={changedValues}/>
      </div>
    )
    return(
      <div style={{padding: '24px'}}>
        <div style={{width: '50%'}}>
          <Tabs onChange={this.handleChangeTabs} value={tabIndex||0}>
            <Tab label="html" value={0}/>
            <Tab label="source" value={1}/>
          </Tabs>
        </div>
        <Divider/>
        {
          tabIndex==0?hEditor:tEditor
        }
      </div>
    )
  }
});

module.exports = HtmlEditor;
