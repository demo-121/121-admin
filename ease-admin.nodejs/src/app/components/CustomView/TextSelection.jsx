let React = require('react');
let mui = require('material-ui');

let RaisedButton = mui.RaisedButton;
import EABFieldBase from './EABFieldBase.js';
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');
let DialogActions = require('../../actions/DialogActions.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let SortableList = require('./SortableList.jsx');

let _isMount = false;

let OtherTitle = React.createClass({

  onUpdate(){
    this.forceUpdate();
  },

  render(){
    let self = this;
    let {
      changedValues
    } = this.props;

    //if not other selection
    if(!changedValues.otherSelection)
      return null;

    //other option should be the last index of options
    let otherOption = changedValues.options[changedValues.options.length-1];
    if(!otherOption.otherTitle){
      otherOption.otherTitle = NeedDetailStore.genLangCodesObj();
    }

    var _othInputTypeOption = [{title:'Text', value: 'text'}, {title:'Currency', value: 'currency'}];
    var _othInputType = {id: 'otherInputType', placeholder: 'Format', type:'PICKER', options: _othInputTypeOption};


    let openPanel = function(){
      DynConfigPanelActions.openPanel(otherOption, 'otherSelection', {onUpdate: self.onUpdate, section: {items:[_othInputType]}});
    }

    return(
      <div onClick={openPanel}>
        {isEmpty(otherOption.otherTitle.en)?isEmpty(otherOption.title.en)?"Others":otherOption.title.en:otherOption.otherTitle.en}:
      </div>
    );
  }
});

module.exports = React.createClass({

  mixins: [EABFieldBase],

  componentWillUnmount(){
    _isMount=false;
  },

  componentDidMount(){
    _isMount=true;
  },

  getInitialState: function() {
    let section = NeedDetailStore.getInputTypeSections('textSelection');
    let subItems = null;
    if(section){
      subItems = section.subItems;
    }

    return {
      section: {items: subItems}
    }
  },

  _genItem(changedValues){
    let {
      disabled
    } = this.props;

    let items = [];
    let self = this;
    if(changedValues && !changedValues.options){
      changedValues.options =[];
    }
    let options = changedValues.options;
    let labStyle={textTransform: 'none'};
    for(let i in options){
      let openPanel = function(){
        self.openCFPanel(options, i);
      }
      items.push(
        <RaisedButton labelStyle={labStyle} className="drag-area" label={<span style={{color:'#20A84C'}}>{!isEmpty(options[i].title.en)?options[i].title.en:options[i].isOther?'Others':'Add title'}</span>} style={{marginRight:'24px', marginBottom: '12px', verticalAlign: 'top'}} onClick={openPanel}/>
      )
    }
    if(!disabled){
      items.push(
        <RaisedButton labelStyle={labStyle} style={{marginBottom: '12px', verticalAlign: 'top'}} label={<span style={{color:'#FF0000'}}>{"Add Selection"}</span>} onClick={this.addSelection}/>
      );
    }

    return items;
  },

  openCFPanel(options, index){

    let self = this;
    let section = cloneObject(this.state.section);
    var _textInput = {id:'textInput', title:getLocalizedText("CONFIG_PANEL.TEXT_INPUT","Text Input"), type:'SWITCH'};

    if(options[index].isOther)  section.items.push(_textInput);


    let delAction = function(){
      let removeDialog = {
        id: "delete",
        title: getLocalizedText("DELETE","DELETE"),
        message: getLocalizedText("DELETE_ITEM","Delete this item?"),
        negative: {
          id: "delete_cancel",
          title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
          type: "button"
        },
        positive:{
          id: "delete_confirm",
          title: getLocalizedText("BUTTON.OK","OK"),
          type: "customButton",
          handleTap: function(){
            if(options[index].isOther){
              changedValues.otherSelection=false;
            }
            options.splice(Number(index),1);
            for(let j in options){
              options[j].seq = (Number(j)+1)*100;
            }
            DynConfigPanelActions.closePanel();
            self.onUpdate();
          }
        }
      }
      DialogActions.showDialog(removeDialog);

    }

    //parent.setState({cfValues:changedValues[i], deleteAction: delAction});
    DynConfigPanelActions.openPanel(options[index], 'textSelection', {deleteAction: delAction, onUpdate: self.onUpdate, section: section});
  },

  addSelection(){
    let {
      changedValues
    } = this.state;

    let self = this;

    if(changedValues && !changedValues.options){
      changedValues.options =[];
    }
    var options = changedValues.options;

    let langCodesItems = NeedDetailStore.genLangCodesObj();
    let section = cloneObject(this.state.section);

    //check other selection exist or not
    let _i = 0;
    if(changedValues.otherSelection){
      _i = 1;
    }

    options.splice(options.length-_i, 0, {id:this.getNextId(options), title:langCodesItems});
    self.onUpdate();
    let validation = function(){
      let errorMsg = !isEmpty(options[options.length-_i-1].title.en)?false:"Some mandatory data do not input yet, if you click OK button, this question would be deleted.";

			if(!errorMsg){
			}else{
				let removeDialog = {
					id: "delete",
					title: getLocalizedText("DELETE","DELETE"),
					message: errorMsg,
					negative: {
						id: "delete_cancel",
						title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
						type: "button"
					},
					positive:{
						id: "delete_confirm",
						title: getLocalizedText("BUTTON.OK","OK"),
						type: "customButton",
						handleTap: function(){
							options.splice(options.length-_i-1, 1);
							self.onUpdate();
							DynConfigPanelActions.closePanel(true);
						}
					}
				}

				DialogActions.showDialog(removeDialog);
			}

			return errorMsg;
    };
    DynConfigPanelActions.openPanel(options[options.length-1-_i], 'textSelection', {onUpdate: self.onUpdate, validation:validation, section: section});
  },

  getNextId(options){
		let nextId = "t1";
		//get the latest id
		for(let i in options){
			let _id = options[i].id;
			if(_id){
				_id = _id.substring(1, _id.length);
				if(isNumeric(_id) && Number(_id) >= Number(nextId.substring(1, nextId.length)))
					nextId= "t"+(Number(_id)+1);
			}
		}
		return nextId;
	},

  onUpdate(){
    if(_isMount)
      this.forceUpdate();
  },

  render() {
    var self = this;
    let {
      changedValues
    } = this.state;

    let {
      isSortable,
      disabled
    } = this.props;

    let items = this._genItem(changedValues);

    //index of items which cannot sort
    let disabledItems = [];
    if(changedValues.otherSelection){
      disabledItems.push(items.length-2);
    }
    disabledItems.push(items.length-1);

    let sortableItems = (
      <SortableList
        id={this.props.id}
        items={items}
        disabled={disabled}
        changedValues={changedValues.options}
        horizontal={true}
        disabledItems={disabledItems}
        handleSort={this.onUpdate}/>
    )

    return (
      <div>
        {isSortable?sortableItems:items}
        <OtherTitle changedValues = {changedValues}/>
      </div>
    );
  }
});
