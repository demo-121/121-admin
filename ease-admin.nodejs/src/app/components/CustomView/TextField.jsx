let React = require('react');
let mui = require('material-ui');

let TextField = mui.TextField;

import EABFieldBase from './EABFieldBase.js';

let EABTextField = React.createClass({
  mixins: [EABFieldBase],

  render() {
    var self = this;
    var {
      template,
      values, rootValues,
      width,
      disabled,
      compact,
      handleChange,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      type,
      placeholder,
      value,
      min,
      max,
      multiLine,
      title,
      interval,
      subType
    } = template;

    var cValue = self.getValue("")

    disabled = disabled || template.disabled || disableTrigger(template, null, values, changedValues, rootValues)

    compact |= !title;

    var errorText = (typeof(template.validation)=='function')?template.validation(changedValues):validate(template, changedValues);
    return <div
      className={"DetailsItem" + this.checkDiff() }>
        <TextField
          key = {id}
          ref = {id}
          type = {subType || "text"}
          className = { "ColumnField" }
          disabled = {disabled}
          hintText= { placeholder }
          errorText= { errorText }
          floatingLabelText = { !compact?this.getTitle():null }
          style= {{width: width?width:'100%', height:compact?'60px':(multiLine?'72px':'80px'), backgroundColor:null}}
          errorStyle = {{lineHeight:'16px', bottom:compact?'16px':'20px'}}
          floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
          underlineStyle = { {bottom:compact?'16px':(multiLine?'12px':'22px')} }
          underlineDisabledStyle = { this.getStyles().disabledUnderline }
          inputStyle={multiLine?{height: '64px'}:null}
          focusColor = { muiTheme.textField.focusColor }
          value = { cValue }
          min = {subType == 'number'?min:null}
          max = {subType == 'number'?max:null}
          step = {subType == 'number'?interval:null}
          multiLine = {false}
          onChange = {
            function(e) {
              self.requestChange(e.target.value);
            }
          }
          onBlur = {
            function(e) {
              self.handleBlur(e.target.value);
            }
          }
          {...others}
          />
      </div>
  }
});

module.exports = EABTextField;
