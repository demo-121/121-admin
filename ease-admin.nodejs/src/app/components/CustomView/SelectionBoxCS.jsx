let React = require('react');
let mui = require('material-ui');
let IconButton = mui.IconButton;
let FlatButton = mui.FlatButton;
let ContentStore = require('../../stores/ContentStore.js');
let Dropzone = require('../CustomView/DropZone.jsx');
//let DynActions = require('../../actions/DynActions.js');
let Paper = mui.Paper;
import AddCircleOutline from 'material-ui/lib/svg-icons/content/add-circle-outline';
import Add from 'material-ui/lib/svg-icons/content/add';
import Info from 'material-ui/lib/svg-icons/action/info';
import EABFieldBase from './EABFieldBase.js';
import Search from 'material-ui/lib/svg-icons/action/search';
import Done from 'material-ui/lib/svg-icons/action/done';
var AppBarActions = require('../../actions/AppBarActions');

let PDF = require('react-pdf');
let Dialog = mui.Dialog;
let openType={};


let uploadZoneWidth=241;
let uploadZoneHeight=196;

let IconBox = React.createClass({

  mixins: [EABFieldBase],

  propTypes: {
    onClickBody: React.PropTypes.func,
    onDrop: React.PropTypes.func,
    isAdd: React.PropTypes.bool,
    type: React.PropTypes.string.isRequired,
    size: React.PropTypes.number
  },

  getInitialState: function() {

    let image = this.getImage();
    return {
      fileURL:'default',
      openDialog:false,
      openType:'image/PNG',
      fileName:'',
      sacleImgWidth:150,
      sacleImgHeight:150,
      isSelected:false,
    }
  },

  getImage(){
    var changedValues = ContentStore.getPageContent().changedValues;
    if(!checkExist(changedValues, "Attachments")){
      changedValues["Attachments"]={};
    }

    let image ="";
    let cValue = this.props.changedValues;
    if(checkExist(cValue, 'image')){
      let fileName = cValue.image;
      if(checkExist(changedValues, "Attachments."+fileName)){
        image=changedValues["Attachments"][fileName];

        if(checkExist(this.state, 'fileURL'))
          if(this.state.fileURL!=image)
            this.setState({fileURL: image});

        let type = image.substring(image.search(":") + 1, image.search(";"));
        if(checkExist(this.state, 'openType'))
          if(this.state.openType!=type)
            this.setState({openType:type});


      }
    }

    return image;
  },

  setImg(newFile) {
    if(this.props.onDrop){
      this.props.onDrop(newFile.url);
    }else{
      var changedValues = ContentStore.getPageContent().changedValues;
      if(!checkExist(changedValues, "Attachments")){
        changedValues["Attachments"]={};
      }

      let cValue = this.props.changedValues;
      if(checkExist(cValue, 'image')){
        let fileName = cValue.image;
        changedValues["Attachments"][fileName] = newFile.url;
        this.setState({fileURL: newFile.url});
        AppBarActions.submitChangedValues("/Needs/Detail/Save");

      }
    }
    this.props.forceRefresh();
  },

  updateOpenType( _openType){
    this.setState({openType:_openType})
  },

  removeFile() {
    var changedValues = ContentStore.getPageContent().changedValues;
    if(!checkExist(changedValues, "Attachments")){
      changedValues["Attachments"]={};
    }

    let cValue = this.props.changedValues;
    if(checkExist(cValue, 'image')){
      let fileName = cValue.image;
      changedValues["Attachments"][fileName] = "";
    }
  },

  getSize(size){
    let height = (size)? size: 200;
    let width = (size)? size: 200;

    return({height: height, width: width});
  },

  getStyle() {
    var style = {
      'buttonBgColor':'#FAFAFA',
      'buttonGreen':'#2DB154',
      'buttonRed':'#F1453D',
    }
    return style;
  },
  openFile(){

    //window.open(this.state.fileURL);
    if(this.state.openType=="application/pdf"){
      let image=this.state.fileURL;

      let encodedStr= image.substring(image.search(",") + 1, image.length);
      let pdfType = image.substring(image.search(":") + 1, image.search(";"));

      var blob = b64toBlob(encodedStr, pdfType);
      var blobUrl = URL.createObjectURL(blob);
      window.open(blobUrl);
    }else
      this.setState({openDialog: true});
  },
  handleClose (){
   this.setState({openDialog: false});
 },


 setTitle(imageId, langCode, title){
   let ncs = ContentStore.getPageContent().changedValues.NeedsConceptSelling;
   var options = ncs.pages[ncs.selectedPage].selections[langCode].options;
   for(let j in options){
      if(options[j].id==imageId){
        let option=options[j];
        option.title['en']=title;
      }
   }


 },
/* multiDel
 setSelectState(){
     this.setState({isSelected:this.props.deleteFile(this.props.imageId)});
     <IconButton  style={{position:'absolute', top:0, left:0, display:this.state.isSelected?'':'none' }}    iconStyle={{fill:"rgba(0, 0, 0, 0.54)", width:24, height:24}}>
       <Done  />
     </IconButton>
 },
 */
  render: function() {
    //var changedValues = ContentStore.getPageContent().changedValues;
    var {
      changedValues,
      sacleImgWidth,
      sacleImgHeight,
    } = this.state;

    let image = this.getImage();
    let self=this;
    let {
      isAdd,
      size,
      type,
      isDragable,

      txtContent,
      txtTitelContent,

      gridZoneStyle,
      dragZoneStyle,
      imgZoneStyle,
      addZoneStyle,
      descZoneStyle,

      addIcBtn,
      infoIcBtn,
      disabled,

      imageId,
      langCode,
    } = this.props;

    let   style=gridZoneStyle.style?gridZoneStyle.style:"";
    let   paperStyle=gridZoneStyle.paperStyle?gridZoneStyle.paperStyle:"";

    let   dzStyle=dragZoneStyle.dzStyle?dragZoneStyle.dzStyle:"";
    let   viewDImageStyle=dragZoneStyle.viewDImageStyle?dragZoneStyle.viewDImageStyle:"";

    let   icBtnStyle=addZoneStyle.icBtnStyle?addZoneStyle.icBtnStyle:"";
    let   icBtnIconStyle=addZoneStyle.icBtnIconStyle?addZoneStyle.icBtnIconStyle:"";
    let   addBtnStyle=addZoneStyle.addBtnStyle?addZoneStyle.addBtnStyle:"";


    let   descOuterStyle=descZoneStyle.descOuterStyle?descZoneStyle.descOuterStyle:"";
    let   descInnerStyle=descZoneStyle.descInnerStyle?descZoneStyle.descInnerStyle:"";

    var dropZ = '';

    //let imageSize = this.getSize(size);

    let require = {
      width: size,
      height: size,
      fileType:'image/png',
      sizeLimit:1*1048576,// 1MB:2MB in byte
    }
    let imgStyle = {
      cursor: 'pointer',
      width: '100%',
      height: 'auto'
    }
    let pdfObj=null;
    let iframeObj=null;
    let imageObj=null;
    let bigImgObj=null;
    let openFileType=image.substring(image.search(":") + 1, image.search(";"));
    if(openFileType=='application/pdf'){
      let encodedStr= image.substring(image.search(",") + 1, image.length);
      let blobStr=b64toBlob(encodedStr, openFileType)?b64toBlob(encodedStr, openFileType):'';
      let blobURL=URL.createObjectURL(blobStr);
      pdfObj=(  <div  style={imgZoneStyle}><PDF content={encodedStr} page={1} scale={0.3} loading={(<span>Loading...</span>)}/></div>);
      iframeObj=(<div><iframe src={blobURL} style={{width:'718px',  height:'700px'}} frameborder="0"></iframe></div> )
    }else{
      imageObj=(
        <div style={{cursor:'pointer', width:'100%', height: uploadZoneHeight+'px', textAlign: 'center', verticalAlign:'middle', display:'table-cell'}}>
          <img style={{
                    width: 'auto',
                    height: 'auto',
                    maxWidth:uploadZoneWidth+ 'px',
                    maxHeight: uploadZoneHeight+'px',
                    verticalAlign:'center'
                  }}
                src={image}/>
        </div>
      )
      bigImgObj=(<div><img  style={{ maxWidth:'100%'}}src={image}></img> </div>);
      let newImg=new Image();
      newImg.src = image;
    }
    let imgHeight = (type=='imageSelection')?(size*(236/314)+'px'):(148+'px');
    let addButton = (
      <div style={addBtnStyle} key="addBtn">
        {addIcBtn}
      </div>
    );


    dropZ = (
      <Dropzone
        style={dzStyle}
        onDrop={function(res) {
          var invalid = [];
          var image = new Image();
            image.src = res.imageUrl;
          var width = image.width;
          var height = image.height;

          let widthLimit=2048;
          let heightLimit=1536;
          var actualFileType = res.file.type;
          var sizeLimit=2*1024*1024;
          if (actualFileType.startsWith("image/")||actualFileType == "application/pdf"){
            if(actualFileType.startsWith("image/")){
              if ((width && width != widthLimit) || (height && height != heightLimit)) {
                invalid.push("Your image is " + width + " x " + height + ". Required " + widthLimit + " x " + heightLimit + ".");
              }
            }
            if ( res.file.size >sizeLimit) {
                invalid.push("Your file size exceed " + sizeLimit/(1024*1024) + " MB.");
            }
          }else {
              invalid.push("Your file is not pdf, jpg, png type.");
          }
          if(invalid.length == 0){
            if (this.require) {
              var requireWidth = this.require.width;
              var requireHeight = this.require.height;
            }
            var requireFiletype=this.require.fileType;
            this.updateOpenType( actualFileType );
            self.setState({fileName: res.file.name})
            self.setTitle( imageId, langCode, res.file.name);
        }

          if (invalid.length == 0) {
            this.updateImg({
              name: res.file.name,
              size: res.file.size,
              altText: '',
              caption: '',
              file: res.file,
              url: res.imageUrl,
            });
            //this.setState({fileURL:res.imageUrl});
          } else {
            var errorMsg = "";
            for (var i = 0; i < invalid.length; i++) {
              errorMsg += invalid[i] + "\n";
            }
            alert(errorMsg, "Import failed");
          }
        }.bind({require: require, updateImg: this.setImg, updateOpenType:this.updateOpenType})}
        className="dropzone"
          >
        <IconButton  style={{position:'absolute', top:0, left:0, display:this.state.isSelected?'':'none' }}    iconStyle={{fill:"rgba(0, 0, 0, 0.54)", width:24, height:24}}>
          <Done  />
        </IconButton>
        {image?this.state.openType=="application/pdf"?pdfObj:imageObj:addButton}
      </Dropzone>
    );


    let height = (type=='imageSelection')?((size-size*(236/314))+'px'):((size-148)+'px');
    let disableZone = (
      <div>
        {image?this.state.openType=="application/pdf"?pdfObj:imageObj:addButton}
      </div>
    )
    const actions = [
     <FlatButton
       label="Close"
       primary={true}
       onTouchTap={this.handleClose}
     />,

   ];
   let openObj=[];
   let fileURL=self.state.fileURL;
   if(this.state.openType=="application/pdf"){
     openObj.push( <div style={{textAlign:'center'}}>{iframeObj} </div>);
   }
    else {
      openObj.push( <div style={{textAlign:'center'}}>{bigImgObj} </div>);
    }

   let fileDialog=(
      <Dialog
        actions={actions}
        modal={false}
        open={this.state.openDialog }
      >
        <div style={{textAlign:'center'}}>
         {openObj}
        </div>
      </Dialog>
      );
    return (
      <div  onClick={this.setSelectState} className={isDragable?"drag-area":""} style={this.mergeAndPrefix({width:size, height: size, display: 'inline-block' , verticalAlign:'top'}, style)}>
        <Paper style={ paperStyle }>
          <div style={this.mergeAndPrefix({ verticalAlign:'middle', display:'table-cell', height:uploadZoneHeight,  minWidth:uploadZoneWidth, maxWidth:uploadZoneWidth, minHeight:uploadZoneHeight, maxHeight:uploadZoneHeight,  overflow:'hidden'} , viewDImageStyle)}>
            {disabled?disableZone:dropZ}
          </div>
          {infoIcBtn}
          <IconButton  style={{float:'right'}}  onTouchTap={this.openFile}  iconStyle={{fill:"rgba(0, 0, 0, 0.54)", width:24, height:24}}>
            <Search  />
          </IconButton>
          <div style={this.mergeAndPrefix({backgroundColor:this.state.isSelected?'white':'white'},descOuterStyle)} onClick={this.props.onClickBody}>
            <div style={descInnerStyle}>{ this.state.fileName?this.state.fileName:txtContent }</div>
          </div>
        </Paper>
       {fileDialog}
      </div>
    );
  },

});

module.exports = IconBox;
