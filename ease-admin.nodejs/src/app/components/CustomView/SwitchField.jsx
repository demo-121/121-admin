/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');

let Toggle = mui.Toggle;
import EABFieldBase from './EABFieldBase.js';
import appTheme from '../../theme/appBaseTheme.js';

let SwitchField = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    root: {
      height: 72,
      position: 'relative',
      fontSize: 16
    },
    label: {
      paddingLeft: 0,
      top: 4
    }
  },
  render() {
    let self = this;
    let styles = this.getStyles();
    let {
      className,
      style,
      template,
      values, rootValues,
      width,
      disabled,
      handleChange,
      labelStyle,
      options,
      ...other,
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      value
    } = template;

    var cValue = self.getValue(false);
    disabled = disabled || template.disabled || disableTrigger(template, null, values, changedValues, rootValues)

    return (<div
      className={"DetailsItem" + this.checkDiff() }>
        <div key="item" className={"ColumnField"}
          style = {this.mergeAndPrefix(this.styles.root, style)}>

          <Toggle key="toggle"
            defaultToggled = { cValue }
            label={this.getFloatingTitle()}
            style={{width: '100%'}}
            elementStyle={{float:'right'}}
            iconStyle={{float:'right'}}
            inputStyle={{float:'right'}}
            disabled = {disabled}
            onToggle = {
              function(e, value) {
                //handleChange(value);
                self.requestChange(value);
              }
            }
            {...other} />
        </div>
      </div>
    );
  },
});

module.exports = SwitchField;
