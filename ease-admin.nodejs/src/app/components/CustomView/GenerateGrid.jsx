let React = require('react');
let mui = require('material-ui');

let FlatButton = mui.FlatButton;

let Dropzone = require('../CustomView/DropZone.jsx');
let GenerateHandsonTable = require('../CustomView/GenerateHandsonTable.jsx');

let ContentStore = require('../../stores/ContentStore.js');

let actions = require("../../actions/DynActions");

import EABFieldBase from './EABFieldBase.js';
import appTheme from '../../theme/appBaseTheme.js';
var json2csv = require('json2csv');

let EABGenerateGrid = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    buttonStyle: {
      marginLeft: 16,
      color: appTheme.palette.primary2Color,
      backgroundColor:appTheme.palette.background2Color,
      borderWidth: 1,
      borderStyle: 'solid',
      borderColor: appTheme.palette.borderColor
    }
  },


  validation() {
    let self = this;
    var {
      template,
      changedValues
    } = this.props;

    var{
      id,
      validateFunc
    } = template;

    var {
      titles,
      values
    } = changedValues[id];

    if (validateFunc) {
      if (validateFunc.indexOf('function')>=0) {

      } else {
        actions.validate(validateFunc, changedValues[id], (values)=>{
          setTimeout(()=> {              
            self.state.changedValues[id] = values;
            self.forceUpdate();
          }, 1000)
        });
      }
      return;
    }

    var grid = this.refs[id];
    var data = cloneArray(values);
    var size = data.length;
    if(size<2){
      alert("title", "not enough data");
      return;
    }else{
      var titleSize = titles.length;
      var _keys = data[0];
      //validate keys
      for(var j in _keys){
        for(var z in _keys){
          if(j != z && _keys[j] == _keys[z]){
            alert("duplicate keys", "error");
            return;
          }
        }
      }
      for(var i=0; i<titleSize; i++){
        var key = _keys[i.toString()];
        if(!key){
          alert("title", "not enough keys");
          return;
        }else{
          let item = grid.searchItemByKey(key, template);
          if(item){
            let {
              mandatory,
              type,
              options
            } = item;
            var optionSize = options?options.length:0;

            for(var j=1; j<size; j++){
              var cellValue = data[j][i];
              if(mandatory && !cellValue){
                alert("please fill in mandatory fields", "Error");
                return;
              }else if(type.toUpperCase() == "PICKER"){
                var found = false;
                for(var z=0; (z<optionSize && !found); z++ ){
                  if(cellValue.toUpperCase() == options[z].title.toUpperCase()){
                      found = true;
                  }
                }
                if(!found){
                  alert("invalid value", "Error");
                  return;
                }
              }
            }
          }
        }
      }

    }
    alert("pass", "pass");
  },

  componentWillMount(){
    var {
      template,
      changedValues
    } = this.props;

    var {
      id
    } = template;

    if(isEmpty(changedValues[id])){
      changedValues[id] = {
        titles:[],
        values:[]
      }
    }else if(typeof changedValues[id] != "object"){
      changedValues[id] = {
        titles:[],
        values:[]
      }
    }else{
      if(isEmpty(changedValues[id].titles)){
        changedValues[id].titles=[];
      }
      if(isEmpty(changedValues[id].values)){
        changedValues[id].values=[];
      }
    }
  },

  onValueChange(value){
    var {
      template,
      changedValues
    } = this.props;

    changedValues[template.id].values = value;
  },

  exportFunc(e, target){

    var {
      template,
      changedValues
    } = this.props;

    var{
      id
    } = template;

    var {
      values,
      titles
    } = changedValues[id];

    var grid = this.refs[id];
    var {
      colHeadersId
    } = grid;

    var data = cloneArray(values);

    data.splice(0,1);


    if (data && data.length > 0) {
      grid.exportCsv({ data: data, fields: colHeadersId,  fieldNames: titles,  quotes: '' })
    }
  },

  importFunc(resp){
    var self = this;
    var {
      template,
      changedValues
    } = this.props;

    var {
      id,
      validateFunc
    } = template;

    // console.debug("import res", res);
    var content = resp.imageUrl;
    // console.debug("import content", content);
    var encodedStr = content.substring(content.search(",") + 1, content.length);
    // console.debug("import encodedStr", encodedStr);
    if (encodedStr.length > 0) {
      var grid = this.refs[id];

      var decodedStr = window.atob(encodedStr);
      var _csvStr = decodedStr.replace(/(\r\n)/gm, "\n");

      //handle headers
      var _csvArr = _csvStr.split("\n");
      var titles = _csvArr[0].split(",");
      var tSize = titles.length;
      var _keys="";
      var colHeadersId = "";

      var orgValues = changedValues[id].values;
      var cnt = 0;
      if(!isEmpty(orgValues[0])){
        var orgKeys = orgValues[0];

        for(var i in orgKeys){
          if(cnt < tSize){
            _keys += (cnt === 0?"":",") + orgKeys[i];
            colHeadersId += (cnt++===0?"":",") + i.toString();
          }

        }
      }
      for(var i=cnt; i<tSize; i++){
        _keys += (i===0?"":",") + grid.searchKey(titles[i], template);
        colHeadersId += (i===0?"":",") + i.toString();
      }
      _csvArr.splice(0,1,_keys).join("\n");

      //import data
      var data = $.csv.toObjects(colHeadersId + "\n" + _csvArr.join("\n"));

      
      if (validateFunc) {
        if (validateFunc.indexOf('function')>=0) {

        } else {
          actions.validate(validateFunc, {
            titles: titles,
            values: data
          }, (values)=>{
            setTimeout(()=> {              
              self.state.changedValues[id] = values;
              self.forceUpdate();
            }, 1000)
          })
        }
      }
      // } else {
      this.requestChange({titles: titles, values: data});
      // }
    }
  },

  componentWillReceiveProps(newProps) {
    var {
      template,
      changedValues
    } = this.props;

    var{
      id,
      validateFunc
    } = template;

    var {
      titles,
      values
    } = changedValues[id];
    let self = this;

    setTimeout(function() {
      self.requestChange({titles: titles, values: values});
    }, 500)
    
  },


  render() {
    var self = this;
    var {
      template,
      width,
      handleChange,
      disabled,
      height,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      type,
      subType,
      insertable,
      title,
      value,
      title
    } = template;

    var cValue = changedValues[id];


    // Import
    var import_ = (disabled)?null:(
      <Dropzone
        key= {"dz"+id}
        onDrop={this.importFunc}
        accept=".csv" >
        <FlatButton key= {"impBtn"+id} style={this.styles.buttonStyle} label={getLocalizedText("HANDSONTABLE_BUTTON.IMPORT","Import")} />
      </Dropzone>
    );

    // Export
    var export_= (
      <FlatButton
        key= {"expbtn"+id}
        style={this.styles.buttonStyle}
        label={getLocalizedText("HANDSONTABLE_BUTTON.EXPORT","Export")}
        onTouchTap={this.exportFunc} />
    );
    //

    //validation
    var valid_ = (
      <FlatButton
        key= {"validBtn"+id}
        style={this.styles.buttonStyle}
        label={getLocalizedText("HANDSONTABLE_BUTTON.VALIDATE","Validate")}
        onTouchTap={self.validation}/>
    );


    return (
      <div style={{height: '100%'}}>
        {
          id=="fund"?
          null:
          <div key="subActions" style={{width:'100%', height:'66px'}}>
            { title ? <span className="fixWidthSpan" style={{
              fontSize: '20px',
              lineHeight: '66px',
              padding: '0 24px',
              width: '300px',
              fontWeight: 300
            }}>{title}</span>:null }
            <div style={{float:'right', padding:'14px 24px'}}>
              {export_}
              {import_}
            </div>
          </div>
        }
        <GenerateHandsonTable
          key= {"ht"+id}
          ref={id}
          height= {height || "100%"}
          template={template}
          colHeaders={cValue.titles}
          gridData={cValue.values}
          disabled={disabled || subType == 'readonly'}
          onChange={this.onValueChange} />
      </div>
    );
  }
});

module.exports = EABGenerateGrid;
