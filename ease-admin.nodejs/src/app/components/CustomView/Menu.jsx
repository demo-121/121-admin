let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;

import LeftNav from 'material-ui/lib/left-nav';
import Divider from 'material-ui/lib/divider';
import List from 'material-ui/lib/lists/list';
import ListItem from 'material-ui/lib/lists/list-item';
import { SelectableContainerEnhance } from 'material-ui/lib/hoc/selectable-enhance';
import appTheme from '../../theme/appBaseTheme.js';

let SelectableList = SelectableContainerEnhance(List);

var ConfigActions = require('../../actions/ConfigActions.js');
var DynActions = require('../../actions/DynActions.js');
let TemData = require('../../actions/TemData.js');
let MenuStore = require('../../stores/MenuStore.js');
let AppStore = require('../../stores/AppStore.js');

let MenuActions = require('../../actions/MenuActions.js');

let Menu = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],



  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: appTheme.getTheme(),
    };
  },

  getStyles() {
    return {
      // Styles
      headerStyle: {
        fontSize: '20px',
        paddingLeft: '16px'
      },
      subheaderStyle: {
        paddingLeft: '16px',
        color: "gray"
      },
      dividerStyle: {
        marginTop: '8px',
        marginBottom: '8px'
      },
      selectionListStyle: {
        //overflowY: this.state.menuState?"auto":'hidden',
        height: "calc(100% - 56px)",
        backgroundColor:'transparent',
        overflowY: 'auto'
      },
      selectedItemStyle: {
        backgroundColor: 'transparent',
        color: appTheme.palette.primary1Color
      }
    }
  },

  menuMap: [],

  propTypes:{
    defaultOpen: React.PropTypes.bool,
    overlay: React.PropTypes.bool
  },

  getDefaultProps() {
    return {
      defaultOpen: true,
      overlay: false
    }
  },

  getInitialState() {

    var rows = this.genMenu(MenuStore.getMainMenu());
    var sysName = AppStore.getUser().sysName;
    var compName = AppStore.getUser().compName;
    var menuState = MenuStore.getMenuState();

    return {
      selectedIndex: 1,
      rows: rows,
      sysName: sysName,
      compName: compName,
      overlay: this.props.overlay,
      menuState: this.props.defaultOpen
    };
  },

  componentDidMount() {
    MenuStore.addChangeListener(this.onMenuChange);
  },

  componentWillUnmount: function() {
    MenuStore.removeChangeListener(this.onMenuChange);
  },

  // componentWillReceiveProps() {
  //   this.setState({menuState: this.props.defaultOpen, overlay: this.props.overlay});
  // },

  onMenuChange() {
    var rows = this.genMenu(MenuStore.getMainMenu());
    var sysName = AppStore.getUser().sysName;
    var compName = AppStore.getUser().compName;
    var menuState = MenuStore.getMenuState();
    // console.debug('onMenuChange', menuState);
    this.setState({
      rows: rows,
      sysName: sysName,
      compName: compName,
      menuState: menuState});
  },

  handleItemTap(index) {
    var self = this;
    MenuActions.changePage(this.menuMap[index], function() {
      self.setState({
        selectedIndex: index
      })
    })
  },

  genMenu(mainMenu) {
    var valueIndex = 1;
    var rows = [];
    var curSection = null;
    var styles = this.getStyles();
    if (mainMenu) {
      for (let i = 0; i < mainMenu.length; i++) {
        var item = mainMenu[i];
        var id = item.id;
        var type = item.type;
        var title = item.title;
        var child = item.subItems;
        var section = (''+item.seq)[0];

        if (curSection != section) {
          if (curSection) {
            rows.push(<Divider key={'i'+i} style={styles.dividerStyle} />);
          }
          curSection = section;
        }

        var childRow = [];

        if (child) {
          for (let c = 0; c < child.length; c++) {
            var subRow = child[c];
            if (subRow) {
              var subRowId = subRow.id;
              var subRowType = subRow.type;
              var subRowTitle = subRow.title;
              childRow.push(<ListItem key={"i"+i+"_"+c} value={valueIndex} primaryText={subRowTitle} />);

              this.menuMap[valueIndex] = subRow;
              valueIndex++;
            }
          }
          rows.push(<ListItem key={"i"+i} primaryText={title} primaryTogglesNestedList={true} nestedItems={childRow} />);
        } else {
          rows.push(<ListItem key={"i"+i} value={valueIndex} primaryText={title} />);

          this.menuMap[valueIndex] = item;
          valueIndex++;
        }
      }
    }
    return rows;
  },

  render() {
    var styles = this.getStyles();
    var sysName = AppStore.getUser().sysName;
    var compName = AppStore.getUser().compName;
    var version = AppStore.getVersion();

    return (
      <LeftNav
        key="leftMenu"
        open={this.state.menuState}
        style={{overflow: 'visible',
          width: this.state.menuState?'280px':'0px',
          backgroundColor:appTheme.palette.background2Color}}>
        <div style={{height: "56px", paddingTop: "8px", display:'block', overflow: 'hidden'}}>
          <span style={styles.headerStyle}>{sysName}</span>
          <br/>
          <span style={styles.subheaderStyle}>{compName}</span>
        </div>
        <Divider key="dvd" />
        <SelectableList 
          valueLink = ""
          key="menuList"
          value = { this.state.selectedIndex }
          selectedItemStyle={ styles.selectedItemStyle }
          onItemTap={ this.handleItemTap }
          style={ styles.selectionListStyle }>
          {this.state.rows}
        </SelectableList>
        <div style={{
            position: "absolute",
            bottom: "0px",
            width: "100%",
            textAlign: "center"
          }}>{version}</div>
      </LeftNav>
    );
  },

});

module.exports = Menu;
