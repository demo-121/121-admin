/** In this file, we create a React component which incorporates components provided by material-ui */
let React = require('react');

let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let ColorManipulator = mui.Utils.ColorManipulator; //require('./utils/color-manipulator');
//let ThemeManager = mui.Styles.ThemeManager;
//let DefaultRawTheme = mui.Styles.LightRawTheme;
import appTheme from '../../theme/appBaseTheme.js';
import AceEditor from 'react-ace';
import 'brace/mode/javascript';
import 'brace/theme/github';

let JavascriptField = React.createClass({

  mixins: [StylePropable],

  contextTypes: {
    muiTheme: React.PropTypes.object,
  },

  propTypes: {
    id: React.PropTypes.string,
    mode: React.PropTypes.string,
    height: React.PropTypes.string,
    width: React.PropTypes.string,
    defaultValue: React.PropTypes.string,
    onBlur: React.PropTypes.func,
    onChange: React.PropTypes.func,
    onBeforeLoad: React.PropTypes.func,
  },

  //for passing default theme context to children
  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme,
    };
  },


  getContextProps() {
    const theme = this.context.muiTheme;

    return {
      isRtl: theme.isRtl,
    };
  },

  _handleBlur(){
    var value = this.state.defaultValue;
    //this.state.errorMsg = this.validate();
    if (this.props.onBlur) this.props.onBlur(value, this.state.errorMsg);
  },

  _handleChange(value){
    this.state.defaultValue = value;
    if(this.props.onChange) this.props.onChange(value);
  },

  getInitialState() {
    let defaultValue = this.props.defaultValue || ""
    return {
      expandedListItems: [],
      activeListItem: 0,
      muiTheme: this.context.muiTheme ? this.context.muiTheme : appTheme.getTheme(),
      defaultValue: defaultValue
    }
  },

  getStyles() {
    let theme = this.state.muiTheme;

    let styles = {
      root:{
        width: "100%",
        border: "1px solid " + appTheme.palette.borderColor
      }
    };
    return styles;
  },

  validate() {
    console.log('validate javascirpt:', this.refs.jsEditor);

    if (this.props.mode == 'json') {
      try {
        var temp = JSON.parse(this.state.defaultValue);
      } catch (e) {
        return e.message;
      }
    } else if (this.refs.jsEditor){
      var ans = this.refs.jsEditor.editor.getSession().getAnnotations();

      if (ans && ans.length) {
        // for (var i in ans) {
        var row = ans[0];
          // if (row.type == 'error') { // only trace error
        return "Line:" + row.row + " Column:"+ row.column + " Error:"+  row.text;
          // }
        // }
      }
    }
    return false;
  },

  render() {
    let styles = this.getStyles();
    let {
      id,
      className,
      style,
      height,
      width,
      onBlur,
      onBeforeLoad,
      disabled,
      mode,
      ...others
    } = this.props;

    var {
      errorMsg
    } = this.state;

    var muiTheme = this.context.muiTheme;

    return (
      <div key={id} style={Object.assign(style, {
          width: width || "100%"
        })}>
        <div className={className}
          style={Object.assign(styles.root, {
            height: height || "100%",
            width: '100%'
          })}>
          <AceEditor
            key={"edit"+id}
            name={"editor"+id}
            ref="jsEditor"
            mode={ "javascript" }
            theme="github"
            width="100%"
            height="100%"
            style={{
              display: "inline-block"
            }}
            value={this.state.defaultValue || ""}
            onBlur={this._handleBlur}
            onChange={this._handleChange}
            readOnly={disabled}
            showPrintMargin={false}
            {...others}
            />
        </div>
        {errorMsg&&mode=='json'?<div style={{
            color: muiTheme.textField.errorColor,
            height: '16px',
            fontSize: '12px'
          }}>{errorMsg}</div>:null}
      </div>
    );
  },
});

module.exports = JavascriptField;
