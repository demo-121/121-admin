let React = require('react');
let mui = require('material-ui');

let FlatButton = mui.FlatButton;
let FontIcon = mui.FontIcon;

import EABFieldBase from './EABFieldBase.js';
import ReadOnlyField from './ReadOnlyField.jsx';
import DialogActions from '../../actions/DialogActions.js';
import EABDetail from './Detail.jsx';

import appTheme from '../../theme/appBaseTheme.js';

module.exports = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    subLineStyle: {
      backgroundColor: appTheme.palette.borderColor,
      height:1,
      margin:'20px 0'
    },
  },

  render() {
    var self = this;
    var {
      template,
      width,
      values,
      handleChange,
      rootValues,
      collapsed,
      inDialog,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      type,
      title,
      presentation,
      allowAdd,
      mandatory,
      items,
      value,
      min,
      max,
      interval,
      subType
    } = template;

    // var cValue = (id && changedValues && changedValues[id])?changedValues[id]:(value?value:{});
    console.log('render DetailsList');
    rootValues = rootValues || changedValues;
    var cValue = self.getValue({});

    var oValue = (id && values && !isEmpty(values[id]))?values[id]:{};

    // var label = "add";
    var cancelBtn = getLocalizedText('BUTTON.CANCEL', "Cancel");
    var addBtn = getLocalizedText('BUTTON.ADD', "Add");
    var saveBtn = getLocalizedText('BUTTON.SAVE', "Save");
    var deleteBtn = getLocalizedText('BUTTON.DELETE', "Delete");

    var subItems = [];

    var sortedItems = items.sort(function(a,b) {
      var aa = a.detailSeq?a.detailSeq:0;
      var bb = b.detailSeq?b.detailSeq:0;
      return (aa < bb) ? -1 : (aa > bb) ? 1 : 0;
    });

    var handleTap = function() {

      var dialogValues = {}
      if (cValue && this.index != undefined && typeof cValue[this.index] == 'object') {
        dialogValues = cValue[this.index];
      } else {
        // if it is already in dialog, just add row
        if (inDialog && cValue) {
          cValue[this.index] = dialogValues;
          changedValues[id] = cValue;
          this.forceUpdate();
          return;
        }
      }

      //console.debug('on handleTap add:', this, dialogValues);
      // construct the dialog
      DialogActions.showDialog({
        title: getLocalizedText(getLocalText('en', title || template.hints)),
        message: this.message,
        items: [{
          "id": "id",
          "type": "text",
          "isKey": true,
          "detailSeq": 1,
          "mandatory": true,
          "title": {
            "en": "ID"
          }
        }, {
          "id": "title",
          "title": {
            "en": "Description"
          },
          "type": "multText",
          "detailSeq": 2,
          "items": [{
            "id":"en",
            "title": "English"
          }, {
            "id":"zh-tw",
            "title": "中文"
          }]
        }],
        values: dialogValues,
        rootValues: rootValues,
        actions: [{
          type: 'customButton',
          float: 'left',
          title: deleteBtn,
          hide: this.index == undefined,
          handleTap: function() {
            if (this.index) {
              delete cValue[this.index];
            }
            self.handleChange(self.state.changedValues);
          }.bind(this)
        }, {
          type: 'cancel',
          title: cancelBtn
        }, {
          type: 'customButton',
          title: this.doneTitle,
          handleTap: function( values ) {
            // if set it as default, reset others
            if (values.default == 'Y') {
              for (let i in cValue) {
                if (i != this.index) {
                  cValue[i].default = 'N';
                }
              }
            }
            if (this.index) { // exsiting record
              // update record including id
              if (this.index != values.id && cValue[values.id]) { // updated id duplicate
                alert("ID duplicate, please use another one", "Warning");
                return false;
              } else {
                delete cValue[this.index];
              }
            } else if (cValue[values.id]) { // new record but existing id
              alert("ID duplicate, please use another one", "Warning");
              return false;
            }

            // new record
            cValue[values.id] = values;
            changedValues[id] = cValue;
            self.handleChange(changedValues);
            return true;
          }.bind(this)
        }]
      });
    }

    var keys = Object.keys(cValue)

    // check if value has set
    if (keys.length > 0 && cValue[keys[0]].seq) {
      keys = keys.sort(function(a,b) {
        var aa = cValue[a].seq || 0;
        var bb = cValue[b].seq || 0;
        return (aa < bb) ? -1 : (aa > bb) ? 1 : 0;
      })
    }

    if (keys.length > 0) {
      for (var i in keys) {
        var cVal = cValue[keys[i]];
        if (typeof cVal == 'object'){
          subItems.push(<EABDetail
            key = {"detail_" + id + "_"+i}
            template = { template }
            values = { cVal }
            changedValues = { cVal }
            rootValues = { rootValues }
            deleteAble = {true}
            collapsed = {collapsed}
            index = {keys[i]}
            handleUpdateRow = {
              handleTap.bind({items: sortedItems, values: cValue, index: keys[i], doneTitle: addBtn})
            }
            handleDeleteRow = { function(row) {
              delete cValue[row];
              self.forceUpdate();
            }}
            handleChange = {function() {
              self.forceUpdate();
            }}
            />)
        }
      }
    } else {
      subItems.push(<div style={{
        lineHeight: '48px',
        marginLeft: '24px'
      }}>No records found.</div>)
    }

    // var errorMsg = validate(template, changedValues);
    // errorMsg = errorMsg?<div style={{
    //   color: this.state.muiTheme.textField.errorColor,
    //   height: '16px',
    //   fontSize: '12px',
    //   position: 'absolute',
    //   left: '24px',
    //   top: '42px'
    // }}>{errorMsg}</div>:null;

    var valueLen = oValue?oValue.length:0;
    var cValueLen = cValue?cValue.length:0;
    var dd = valueLen - cValueLen;
    if (dd < 0) dd = 0;

    return (
      <div key= {"ctn"+id} className="ListField" style={{position:'relative'}}>
        {this.getHeaderTitle()}
        {subItems}
        {(allowAdd === undefined || allowAdd)?<FlatButton
          key={id+"btn"}
          labelPosition = "after"
          className = { "DetailsItem" }
          style={ { margin: '6px 24px' } }
          onClick = { handleTap.bind({items: sortedItems, values: cValue, doneTitle: addBtn}) }
          >
          <div style={{display:'flex', alignItems:'center'}}>
            <FontIcon className="material-icons" style={{color:appTheme.palette.text1Color}}>add</FontIcon>
            <span style={{padding: '0 15px'}}>{addBtn}</span>
          </div>
        </FlatButton>:null}
      </div>
    )
  }
});
