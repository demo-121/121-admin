var React = require('react');
let mui = require('material-ui');
let Paper = mui.Paper;
var AppStore = require('../stores/AppStore.js');
var ContentStore = require('../stores/ContentStore.js');
let RaisedButton = mui.RaisedButton;

let MenuActions = require('../actions/MenuActions.js');

var Error = React.createClass({

	componentDidMount: function() {
    AppStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function() {
    AppStore.removeChangeListener(this.onChange);
  },

	getInitialState() {
		return {
			title: "Error",
			error: "The file concerned may have been damaged, moved, deleted, or a bug may have caused the error. Alternatively, the file simply might not exist, or the user has mistyped its name."
		}
	},
	loginPage(){
		MenuActions.changePage("/Logout");
		document.location=ROOT_CONTEXT_PATH;
	},

	render: function() {
		let imageStyle = {
      width: '120px',
      height: 'auto',
      marginBottom: '32px',
			display: 'flex',
			flexDirection: 'column',
    };

		let textStyle = {
			width: '50%',
			textAlign: 'center',
		};

		// <h1>{this.state.title}</h1>
		return (
			<div className="pCenter" style={{display: 'flex', flexDirection: 'column'}} >
				<img style={imageStyle} src= {'web/img/error_icon.png'} />
				<h1 style={textStyle}>{this.state.error}</h1>
				 <RaisedButton
                  label={getLocalizedText('ERROR.BTN.TITLE','LOGIN 121 ADMIN')}
                  secondary={true}
                  onTouchTap={this.loginPage} />
			</div>


		);
	},

	onChange() {
		var title = ContentStore.getPageContent().title;
		var error = ContentStore.getPageContent().error;

		if (title || error) {
			this.setState({
				title: title,
				error: error
			});
		}
	}
});

module.exports = Error;
