let React = require('react');
let mui, {Dialog, FlatButton, Toolbar, ToolbarTitle} = require('material-ui');

let EABTextField = require('../CustomView/TextField.jsx')
let EABPickerField = require('../CustomView/PickerField.jsx')
let EABDatePickerField = require('../CustomView/DatePickerField.jsx')
let EABTimePickerField = require('../CustomView/TimePickerField.jsx')
let EABCheckbox = require('../CustomView/CheckBox.jsx');
let EABCheckboxGroup = require('../CustomView/CheckBoxGroup.jsx');
let EABRadioBoxGroup = require('../CustomView/RadioBoxGroup.jsx');
let EABMultipleTextField = require('../CustomView/MultipleTextField.jsx');
let EABTable = require('../CustomView/Table.jsx');
let EABJSBoxField = require('../CustomView/JSBoxField.jsx');
let EABComplexList = require('../CustomView/ComplexList.jsx');
let EABDetailList = require('../CustomView/ComplexList.jsx');

let DialogActions = require('../../actions/DialogActions.js');
let DynActions = require('../../actions/DynActions.js');

let AppStore = require('../../stores/AppStore.js');
let MsgDialogStore = require('../../stores/MsgDialogStore.js');
let HomeStore = require('../../stores/HomeStore.js');
let MasterTableStore = require('../../stores/MasterTableStore.js');
let ContentStore = require('../../stores/ContentStore.js');

import appTheme from '../../theme/appBaseTheme.js';

let DynMsgDialog = React.createClass({

  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme,
    };
  },

  contextTypes: {
    muiTheme: React.PropTypes.object
  },

  getInitialState() {
    var dialog = MsgDialogStore.getDialog();
    return {
      dialog: dialog,
      open: MsgDialogStore.getOpen(),
      muiTheme: this.context.muiTheme ? this.context.muiTheme : appTheme.getTheme()
    };
  },

  componentDidMount() {
    MsgDialogStore.addChangeListener(this.onChange);
  },

  componentWillUnmount() {
    MsgDialogStore.removeChangeListener(this.onChange);
  },

  onChange() {
    var dialog = MsgDialogStore.getDialog();
    if (dialog) {
      this.setState({
        dialog: dialog,
        open: MsgDialogStore.getOpen()
      });
    }
  },

  handleClose(e) {
    var event = e || window.event;
    if (event) {
      if (event.stopPropagation) {
        event.stopPropagation();
      } else {
        event.cancelBubble = true;
      }
      if (event.preventDefault) {
        event.preventDefault();
      }
    }
    DialogActions.closeMsgDialog();
  },

  getStyles() {
    let theme = this.state.muiTheme;
    return {
      titleStyle : {
        display: 'block',
        fontSize: '20px',
        lineHeight: '64px'
      },
      dialogTitleStyle: {
        padding: '20px 24px',
        lineHeight: '24px',
        fontSize: 20,
        borderBottom: '1px solid',
        borderColor: appTheme.palette.borderColor
      },
      actionsContainerStyle: {
        borderTop: '1px solid',
        borderColor: appTheme.palette.borderColor
      }
    }
  },

  resetValueItems: null,

  render() {
    var self = this;
    var values = this.state.values;
    var dialog = this.state.dialog;
    var orgValues = dialog ? dialog.values : null;

    if (!dialog || !this.state.open) {
      return null;
    }

    this.resetValueItems = {};

    console.debug('[DynMsgDialog] - [render]', this.state);
    var styles = this.getStyles();
    this.fullScreen = false;

    // Start of Buttons
    const actions = [];

    if (!dialog.actions) {
      if (dialog.negative) {
        actions.push(this._genCancelButton(dialog.negative));
      }
      if (dialog.positive) {
        actions.push(this._genActionButton(self, dialog, dialog.positive, values));
      }
    } else {
      for (let ai in dialog.actions) {
        var action = dialog.actions[ai];
        if (!action.hide) {
          if (action.type == 'cancel') {
            actions.push(this._genCancelButton(action));
          } else {
            actions.push(this._genActionButton(self, dialog, action, values));
          }
        }
      }
    }
    // End of Buttons

    const customContentStyle = {
      width: '300px',
      maxWidth: null
    };

    var dtitle = getLocalText('en', dialog.title);

    return (
      <Dialog
        title={dtitle}
        className="Dialog"
        actions={actions}
        modal={false}
        actionsContainerStyle={styles.actionsContainerStyle}
        titleStyle={styles.dialogTitleStyle}
        bodyStyle={ {padding: '24px 20px',
          maxHeight: dtitle?"calc(100vh - 225px)":"calc(100vh - 160px)",
          overflowY: 'auto'} }
        contentStyle={dialog.contentStyle?Object.assign(customContentStyle, dialog.contentStyle):customContentStyle}
        open={this.state.open} >
        {dialog.message}
      </Dialog>
    );

  },

  _genCancelButton(actionItem) {
    return <FlatButton
      label={actionItem.title}
      primary={actionItem.primary}
      onTouchTap={this.handleClose} />
  },

  _genActionButton(self, dialog, actionItem, values) {
    var handlePositiveTap = null;

    if (actionItem.type == "dialogButton" && actionItem.dialog) {
      handlePositiveTap = function() {
        DialogActions.showDialog(actionItem.dialog);
      };
    } else if (actionItem.type == "customButton") {
      handlePositiveTap = function() {
        var result = true;
        if (actionItem.handleTap && typeof actionItem.handleTap == 'function') {
          result = actionItem.handleTap(values);
        }
        if (result || result == undefined) {
          self.handleClose();
        }
      };
    } else if (actionItem.type == "submitButton") {
      handlePositiveTap = function() {
        var content = ContentStore.getPageContent();
        var page = HomeStore.getCurrentPage();
        var rows = [];

        if (self.state.dialog.inhertValue && self.state.values && self.state.values.rows) {
          rows = self.state.values.rows;
        } else {
          rows = MasterTableStore.getSelectedRows();
        }

        if (rows.length == 0 && content.values.id) {
          rows.push(content.values.id);
        }

        DialogActions.submitSelected(actionItem.id, values, rows, content, page);
        self.handleClose();
      };
    } else if (actionItem.type == "saveValueButton") {
      handlePositiveTap = function() {

        var content = ContentStore.getPageContent();
        var changedValues = content.changedValues?content.changedValues:cloneObject(content.values);

        // changedValues
        var keyList = Object.keys(values);
        for (let i in keyList){
          var key = keyList[i];
          changedValues[key] = values[key];
        }
        DynActions.changeValues(changedValues);
        self.handleClose();
      };
    } else {
      handlePositiveTap = function() {
        self.handleClose();
      }
    }

    return (
      <FlatButton
        label={ actionItem.title }
        style={ actionItem.float?{
          float: actionItem.float
        }: {}}
        primary={actionItem.primary}
        keyboardFocused={actionItem.keyboardFocused}
        onTouchTap={function(e) {
          var event = e || window.event;
          if (event) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else {
              event.cancelBubble = true;
            }
            if (event.preventDefault) {
              event.preventDefault();
            }
          }
          if (this.beforePositive && typeof this.beforePositive == 'function') {
            this.beforePositive();
          }
          this.handlePositiveTap();
          if (this.afterPositive && typeof this.afterPositive == 'function') {
            this.afterPositive();
          }
        }.bind({handlePositiveTap: handlePositiveTap, beforePositive: dialog.beforePositive, afterPositive: dialog.afterPositive}) } />
    );
  }

});

module.exports = DynMsgDialog;
