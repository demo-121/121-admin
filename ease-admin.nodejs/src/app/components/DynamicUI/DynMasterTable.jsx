/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let ReactDOM = require('react-dom');

let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
import appTheme from '../../theme/appBaseTheme.js';
let brace = require('brace');

let MasterTableStore = require('../../stores/MasterTableStore.js');
let MasterTableActions = require('../../actions/MasterTableActions.js');

let AssignPageActions = require('../../actions/AssignPageActions.js');
let AssignContentStore = require('../../stores/AssignContentStore.js');


let ConfigConstants = require('../../constants/ConfigConstants.js');
let DynActions = require('../../actions/DynActions.js');
var DialogActions = require('../../actions/DialogActions')
let IconButton = mui.IconButton;
let IconMenu = mui.IconMenu;
let MenuItem = mui.MenuItem;
let FontIcon = mui.FontIcon;
let Colors = mui.Styles.Colors;
let FlatButton = mui.FlatButton;
let ToolTip = mui.Tooltip;

import Table from 'material-ui/lib/table/table';
import TableBody from 'material-ui/lib/table/table-body';
import TableFooter from 'material-ui/lib/table/table-footer';
import TableHeader from 'material-ui/lib/table/table-header';
import TableHeaderColumn from 'material-ui/lib/table/table-header-column';
import TableRow from 'material-ui/lib/table/table-row';
import TableRowColumn from 'material-ui/lib/table/table-row-column';

let DynMasterTable = React.createClass({

  mixins: [React.LinkedStateMixin, StylePropable],

  contextTypes: {
    muiTheme: React.PropTypes.object
  },

  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: this.context.muiTheme?this.context.muiTheme:appTheme.getTheme(),
    };
  },

  propTypes:{
    id: React.PropTypes.string.isRequired,
    height: React.PropTypes.string,
    list: React.PropTypes.array.isRequired,
    template: React.PropTypes.object.isRequired,
    total: React.PropTypes.number.isRequired,
    selectable: React.PropTypes.bool,
    multiSelectable: React.PropTypes.bool,
    contextMenu: React.PropTypes.array,
    show: React.PropTypes.bool,
    handleTapRow: React.PropTypes.func,
    handleRowSelection: React.PropTypes.func,
    assignPage: React.PropTypes.bool,
    sort: React.PropTypes.bool
  },

  getDefaultProps() {
    return {
      selectable: false,
      multiSelectable: false,
      contextMenu: null,
      show: true,
      height: '500px',
      assignPage: false,
    }
  },

  getInitialState() {
    let template = this.props.template;
    var sortInfo = (this.props.assignPage)?AssignContentStore.getSortInfo():MasterTableStore.getSortInfo();
    var selectedRows = (this.props.assignPage)?AssignContentStore.getSelectedRows():MasterTableStore.getSelectedRows();
    var selectedRowNumbers = (this.props.assignPage)?AssignContentStore.getSelectedRowNumbers():MasterTableStore.getSelectedRowNumbers();
    return {
      muiTheme: this.context.muiTheme,
      sortedCol: sortInfo.sortBy,
      sortedDirection: sortInfo.sortDir,
      selectedRows: selectedRows,
      fields: this.initFields(template),
      tooltipState: {},
      selectedRowNumbers: selectedRowNumbers,
    }
  },

  getStyles() {
    return {
      tableHeaderColumnStyle: {
        fontWeight:'bold',
        paddingRight: '0px',
        cursor: 'pointer',
        overflowX: 'hidden',
        color: appTheme.palette.text1Color
      },
      tableColumnStyle: {
        paddingRight: '0px',
        fontSize: '14px',
        position: 'relative',
        overflow: 'visible'  // for tooltip
      },
      cellStyle: {
        width: '100%',
        display: 'inline-block',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
      }
    }
  },

  initFields(template) {
    var fields = [];
    for (let i in template.items) {
      var item = template.items[i];
      if (item.listSeq && item.title) {
        fields.push(item);
      }
    }

    fields = fields.sort(function(a,b) {
      var aa = a.listSeq;
      var bb = b.listSeq;
      return (aa < bb) ? -1 : (aa > bb) ? 1 : 0;
    });
    return fields;
  },

  componentDidMount() {
    // console.debug('homepage did mount');
    if(this.props.assignPage){
      AssignContentStore.addChangeListener(this.onChange);
    }else{
      MasterTableStore.addChangeListener(this.onChange);
    }
  },

  componentWillUnmount() {
    if(this.props.assignPage){
      AssignContentStore.removeChangeListener(this.onChange);
    }else{
      MasterTableStore.removeChangeListener(this.onChange);
    }

  },

  onChange() {
    var sortInfo = (this.props.assignPage)?AssignContentStore.getSortInfo():MasterTableStore.getSortInfo();
    var selectedRows = (this.props.assignPage)?AssignContentStore.getSelectedRows():MasterTableStore.getSelectedRows();
    var selectedRowNumbers = (this.props.assignPage)?AssignContentStore.getSelectedRowNumbers():MasterTableStore.getSelectedRowNumbers();
    this.setState({
      selectedRows: selectedRows,
      sortedCol: sortInfo.sortBy,
      sortedDirection: sortInfo.sortDir,
      selectedRowNumbers : selectedRowNumbers })
  },

  componentWillReceiveProps(nextProps) {
    this.setState({ fields: this.initFields(nextProps.template)})
  },

  handleTapHeader(e, row, col) {
    var direction = this.state.sortedDirection;
    var multiSelectable = !!this.props.multiSelectable;
    if (multiSelectable && col == 1) return;
    var column = col - (multiSelectable?2:1);
    var fid = this.state.fields[column].id;

    if (this.state.sortedCol == fid) {
      if (direction == 'A') {
        direction = 'D';
      } else {
        direction = 'A';
      }
    } else {
      direction = 'A';
    }

    if(this.props.assignPage){
      AssignPageActions.updateSortInfo(fid, direction);
    }else{
      if(this.props.sort !== "undefined") {
        if(this.props.sort !== false) {
          MasterTableActions.updateSortInfo(fid, direction);
        }
      } else {
        MasterTableActions.updateSortInfo(fid, direction);
      }
    }

    //var sortedList = this._sortList(this.state.sortedList, fid, direction);
    //this.setState( {sortedList: sortedList, sortedCol: column, sortedDirection: direction});
  },

  // _sortList(list, fid, dir) {
  //   if (dir == 'a') {
  //     return list.sort(function(a,b) {
  //       var aa = a[fid];
  //       var bb = b[fid];
  //       return (aa < bb) ? -1 : (aa > bb) ? 1 : 0;
  //     } );
  //   } else {
  //     return list.sort(function(a,b) {
  //       var aa = a[fid];
  //       var bb = b[fid];
  //       return (aa < bb) ? 1 : (aa > bb) ? -1 : 0;
  //     } );
  //   }
  // },

  handleTapRow(e, row) {
    let {
      list,
      id,
    } = this.props;
    if((id == "releaseDetails" ||id == "tasks")  && !list[row].accessible){
      DialogActions.showDialog({
        title: getLocalizedText('DIALOG.TITLE.WARNING', 'Warning'),
        message: getLocalizedText('DIALOG.MSG.ACCESS_DENIED', 'dsadsad'),
        width: 50,
        actions: [{
          type: 'OK',
          title: getLocalizedText('BUTTON.OK', 'OK'),
          secondary: true,
        }]
      });
      return null;
    }
    if(!this.props.assignPage){
      if (this.props.handleTapRow){
        this.props.handleTapRow(e, row);
      } else {
        MasterTableActions.selectRow(row);
      }
    }
  },

  // only the checkbox field
  handleRowSelection(rows) {

    if (this.props.handleRowSelection) {
      if(this.props.assignPage){
        AssignPageActions.updateSelectedRow(rows, this.props.list, this.props.handleRowSelection(rows));
      }else{
        MasterTableActions.updateSelectedRow(rows, this.props.list, this.props.handleRowSelection(rows));
      }
    } else {
      if(this.props.assignPage){
        AssignPageActions.updateSelectedRow(rows, this.props.list, false);
      }else{
        MasterTableActions.updateSelectedRow(rows, this.props.list, false);
      }
    }
  },

  handleNextTap() {
    var {
      list
    } = this.props;
    if(this.props.assignPage){
      AssignPageActions.refreshMasterTable(list.length);
    }else{
      MasterTableActions.refreshMasterTable(list.length);
    }
  },

  _getSortIcon(dir) {
    var color = appTheme.palette.text1Color;
    return <FontIcon
      className="material-icons"
      style={{
        fontSize:'14px',
        top:'3px',
        color:color,
        marginLeft:'5px'
      }}>
        {dir=='A'?'arrow_downward':'arrow_upward'}
      </FontIcon>
  },

  _getTableHeader: function() {
    var fields = this.state.fields;
    var sortedCol = this.state.sortedCol;
    var direction = this.state.sortedDirection;
    var {
      template,
      selectable,
      multiSelectable
    } = this.props;

    var hasContextMenu = !!this.props.contextMenu;
    var styles = this.getStyles();
    var headerColumns = [];
    for (let i in fields) {
      var item = fields[i];
      var dirIcon = (item.id == sortedCol)? this._getSortIcon(direction):null;
      headerColumns.push(
        <TableHeaderColumn
          key={"hc"+i}
          ref={"hc"+i}
          style={ this.mergeStyles(styles.tableHeaderColumnStyle, { width: item.colWidth }) }>
          {item.title}
          {dirIcon}
        </TableHeaderColumn>
        );
    }

    if (hasContextMenu) {
      headerColumns.push(
        <TableHeaderColumn
          key={"actions"+i}
          ref={"actions"}
          style={ { width: '50px' } }>
        </TableHeaderColumn>
        );
    }
    return <TableHeader ref="header"
        adjustForCheckbox={multiSelectable || selectable}
        displaySelectAll={multiSelectable}>
        <TableRow
          onCellClick={this.handleTapHeader}>
          {headerColumns}
        </TableRow>
      </TableHeader>;
  },

  _getTableFooter: function() {
    var rows = this.state.selectedRows;
    var rowNumbers = this.state.selectedRowNumbers;
    var total = this.props.total;
    var selectCtn = 0;
    if (typeof(rows) == 'object') {
      selectCtn = Object.keys(rows).length
    } else {
      selectCtn = (rows == 'all')?total:0;
    }
    var text = "";
    if (rowNumbers && rowNumbers<total) {
      text = getLocalizedText("TABLE.DISPLAY_COUNT","{2} / {1} records");
      text = text.replace("{1}", total).replace("{2}", Math.min(rowNumbers, total));
    }else {
      text = getLocalizedText("TABLE.RECORD_COUNT","{1} records");
      text = text.replace("{1}", total).replace("{2}", selectCtn);
    }
    if (selectCtn) {
      text = getLocalizedText("TABLE.SELECTED_COUNT","{2} of {1} selected");
      text = text.replace("{1}", total).replace("{2}", selectCtn);
    }
    return <div key="footer" className="TableFixFooter"> {text} </div>;
  },

  _getMoreButton() {
    let{
      list,
      total,
      multiSelectable,
      emptyMessage,
      template,
      selectable,
      page
    } = this.props;

    var maxSize = ConfigConstants.TableMaxSize;
    // add 'more' row
    if (list && total > list.length && list.length < maxSize) {
      return (
        <TableFooter
            ref="tableFooter"
            adjustForCheckbox={false}>
          <TableRow key={"lastRow"}
            displayRowCheckbox={false}>
            <TableRowColumn
              key={"actions"}
              ref={"actions"}
              colSpan={this.state.fields.length + (this.props.multiSelectable?1:0) + (this.props.selectable?1:0)}>
              <FlatButton
                key={'next'}
                id={'next'}
                style={ { width: '100%', textAlign: 'center' } }
                onTouchTap={this.handleNextTap}>
                { getLocalizedText('BUTTON.MORE', 'More')}
              </FlatButton>
            </TableRowColumn>
          </TableRow>
        </TableFooter>);
    } else if (total == 0) {
        return (
          <TableFooter
              adjustForCheckbox={false}>
            <TableRow key={"lastRow"}
              displayRowCheckbox={false}>
              <TableRowColumn
                key={"actions"}
                ref={"actions"}
                style={ { textAlign: 'center' } }
                colSpan={this.state.fields.length + (this.props.multiSelectable?1:0) + (this.props.selectable?1:0)}>
                {template.emptyMessage?template.emptyMessage:getLocalizedText("TABLE.NO_RECORD")}
              </TableRowColumn>
            </TableRow>
          </TableFooter>);
    } else if (total != 0 && page == 'bcp') {
      let style = {
        margin : 12,
        border: "1px solid #C5C5C5",
        borderRadius: 5,
        textAlign: "center",
        // color: "#20A84C"
      }
        return (
          <TableFooter
            ref="tableFooter"
            adjustForCheckbox={false}>
          <TableRow key={"lastRow"}
            displayRowCheckbox={false}>
          <TableRowColumn
              key={"actions"}
              ref={"actions"}
              colSpan={this.state.fields.length + (this.props.multiSelectable?1:0) + (this.props.selectable?1:0)}>
              
              <FlatButton
                key={'resubmit'}
                id={'resubmit'}
                style = {style}
                onTouchTap = {this.props.handleReSubmit}
                disabled={this.props.submitDisable}
                // style={ { width: '100%', textAlign: 'center' } }

                // onTouchTap={this.handleNextTap}
                >
                Submit
                {/* { getLocalizedText('BUTTON.MORE', 'More')} */}
              </FlatButton>
          </TableRowColumn>
          </TableRow>
          </TableFooter>
         
        );
    }
    return null;

  },

  _getTableRows() {
    let{
      list,
      contextMenu,
      total,
      multiSelectable
    } = this.props;

    if (!list) {
      return null;
    }

    var self = this;
    var styles = this.getStyles();
    var fields = this.state.fields;
    var selectedRows = this.state.selectedRows;
    var columns = null;
    var rows = [];
    var _handleHoverExit = function (e) {
      var tooltipState = this.parent.state.tooltipState;
      if (tooltipState[this.key]) {
        tooltipState[this.key] = false;
      }
      this.parent.setState({tooltipState: tooltipState});
    }

    var _handleHover = function (e) {
      var tooltipState = this.parent.state.tooltipState;
      tooltipState[this.key] = true;
      this.parent.setState({tooltipState: tooltipState});
    }

    for (let i in list) {
      var item = list[i];
      columns = [];
      for (let j in fields) {
        var field = fields[j];
        var value = item[field.id];
        value = performValueUpdateTrigger(field, value, item);

        if (field.options) {
          for (let idx in field.options) {
            var op = field.options[idx];
            if (op.value === value) {
              value = op.title;
              break;
            }
          }
        }

        

        var tooltip = null;
        if (field.tooltipId && item[field.tooltipId]) {
          let tooltipTextHandler = function(data) {
            if((typeof data).toUpperCase() == "STRING") {
              return data;
            }
            
            if((typeof data).toUpperCase() == "OBJECT") {
              let text_array = [];
              for(let key in data) {
                if(data[key].eleType) {
                  let optional = {};
                  // console.warn('optional', data[key].optional);
                  if(data[key].optional) {
                    if(IsJsonString(data[key].optional)) {
                      optional = JSON.parse(data[key].optional);
                    }
                    
                  }
                  text_array.push(React.createElement(data[key].eleType, optional, data[key].text));
                }
              }
              return text_array;
            }
            
            console.warn("You have create Tooltip but render incorrectly, check DynMasterTable Line 550");
            return null;
          }

          tooltip = <ToolTip
            key = {"tt_"+i+"_"+j}
            // label = {item[field.tooltipId]}
            label = {tooltipTextHandler(item[field.tooltipId])}
            show = {!!this.state.tooltipState[i+"-"+j]}
            />
        }
        value = <span style={ styles.cellStyle }>{value}</span>

        var icon = null;
        if(item[field.icon]){
          var color = eval(item[field.icon].color)
          value = <div style={{display:'flex', alignItems:'center'}}>
            {value}
            <FontIcon className="material-icons" style={{marginLeft:8}} color={color} >{item[field.icon].icon}</FontIcon>
          </div>
        }

        columns.push( <TableRowColumn
          key={"c_"+i+"_"+j}
          onHoverExit = {tooltip?_handleHoverExit.bind({parent: self, key: i+"-"+j}):null}
          onHover = {tooltip?_handleHover.bind({parent: self, key: i+"-"+j}):null}
          style={ this.mergeStyles(styles.tableColumnStyle, { width: field.colWidth }) }>{tooltip}{value}</TableRowColumn> )
      }

      if (contextMenu && contextMenu.length) {
        var menuItems = [];
        var handleIconTap = function(e) {
          console.debug('tap tap');
        }

        var iconButtonElement = <IconButton key={"actionBtn"+i}
          iconClassName="material-icons"
          onTouchTap={handleIconTap}>
          more_vert
        </IconButton>

        for (let m = 0; m<contextMenu.length; m++) {
          var mitem = contextMenu[m];
          menuItems.push(<MenuItem key={"mi"+j+"-"+m} primaryText={mitem.title} />)
        }

        columns.push(
          <TableRowColumn
            key={"actions"}
            ref={"actions"}
            style={ { width: '50px' } }>
            <IconMenu
              key={"cm"+i}
              iconButtonElement={iconButtonElement}
              anchorOrigin={{vertical:'top',horizontal:'right'}}
              targetOrigin={{vertical:'top',horizontal:'right'}}>
              {menuItems}
            </IconMenu>
          </TableRowColumn>
          );
      }

      var selected = selectedRows? ((selectedRows == 'all') || arrayContains(selectedRows, item.id)): false;

      rows.push( <TableRow key={"r"+i}
        selected={ selected }
        style={{cursor:'pointer', height: 48}}
      >{columns}</TableRow> )
    }

    return {
      rows: rows,
      colCount: columns?columns.length:0
    }
  },

  resetWidth4Scrollbar() {
    var self = this;
    if (this.resetSize) {
      clearTimeout(this.resetSize);
    }

    this.resetSize = setTimeout(function() {
      var st = self.refs["header"];
      var ci = self.refs["tableFooter"];
      if (st && ci) {
        st = ReactDOM.findDOMNode(st);
        ci = ReactDOM.findDOMNode(ci);
        st = st.parentNode.parentNode;
        if (st.style) {
          st.style.paddingRight =  (st.clientWidth - ci.clientWidth) + "px"
          st.style.overflow = "hidden"
        }
      }
      self.resetSize = false;
    }, 100)
  },

  render: function() {
    var self = this;
    let {
      id,
      height,
      show,
      selectable,
      multiSelectable,
      list,
      template,
      assignPage,
      contextMenu,
      ...other
    } = this.props;

    if (!show) return <div key={id} className="DetailsColumn" style={{ flex:0 }}></div>;


    // set the page size
    // var maxRows = height / 48;
    // MasterTableStore.setPageSize(maxRows);

    var header = this._getTableHeader();
    var footer = this._getTableFooter();
    var moreButton = this._getMoreButton();
    var {
      rows,
      colCount
    } = this._getTableRows();

    this.resetWidth4Scrollbar();
    if (header && rows && footer) {
      return (
        <div style={{height:'100%'}}>
          <Table
            fixedHeader={true}
            fixedFooter={false}
            height={height}
            onRowSelection={this.handleRowSelection}
            selectable = {multiSelectable || selectable}
            allRowsSelected = { this.state.selectedRows == 'all' }
            multiSelectable={multiSelectable} >
            {header}
            <TableBody
              key="body"
              showRowHover={true}
              displayRowCheckbox={multiSelectable || selectable}
              onRowClick={this.handleTapRow}
              deselectOnClickaway={false}
              preScanRows={true}
              contextMenuIconColumn={contextMenu?colCount+(multiSelectable?1:0):0}
              ref="tableBody">
              {rows}
            </TableBody>
            {moreButton}
          </Table>
          {footer}
        </div>)
    }
    return null;
  }
});

module.exports = DynMasterTable;
