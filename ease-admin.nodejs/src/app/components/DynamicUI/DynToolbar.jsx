/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
import appTheme from '../../theme/appBaseTheme.js';
let Transitions = mui.Styles.Transitions;

let AppBarStore = require('../../stores/AppBarStore.js');
let HomeStore = require('../../stores/HomeStore.js');
let MenuStore = require('../../stores/MenuStore.js');
let ContentStore = require('../../stores/ContentStore.js')

let MenuActions = require('../../actions/MenuActions.js')
let AppBarActions = require('../../actions/AppBarActions.js')
let DialogActions = require('../../actions/DialogActions.js')

let MasterTableActions = require('../../actions/MasterTableActions.js')

let SelectField = require('../CustomView/SelectField.jsx')

let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let ToolbarSeparator = mui.ToolbarSeparator;

let FontIcon = mui.FontIcon;
let IconButton = mui.IconButton;
let FlatButton = mui.FlatButton;
let IconMenu = mui.IconMenu;
let TextField = mui.TextField;
let DropDownMenu = mui.DropDownMenu;
let MenuItem = mui.MenuItem;

let DynToolbar = React.createClass({

  mixins: [StylePropable],

  PropTypes: {
  },

  getStyles() {
    return {
      contentStyle: {
        color: appTheme.palette.alternateTextColor,
        top: '0px'
      },
      iconStyle: {
        padding: '0px 12px',
        margin: '4px',
        lineHeight: '48px'
      },
      flatButtonStyle:{
        padding: '10px',
        margin: '0 20px 0 0',
        minWidth: '100px'
      },
      pickerStyle: {
        width: 'auto',
        height: '56px',
      },
      actionsPaneStyle: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%'
      },
      seperatorStyle: {
        float:null,
        lineHeight:'56px',
        fontSize:'30px',
        color: appTheme.palette.alternateTextColor
      },
      labelStyle: {
        lineHeight: '56px',
        fontSize: '20px',
        fontWeight: 'normal',
        display: 'inline-block',
        position: 'relative',
        float: null,
        color: appTheme.palette.alternateTextColor
      },
      buttonStyle: {
        fontSize: '20px',
        fontWeight: 'normal',
        textTransform: 'none',
        float: null,
        padding:0,
        margin:0
      }
    }
  },

  searchBar: null,

  getDefaultProps: function() {
    return {
    }
  },

  getInitialState: function() {
    
    return {
      searching: AppBarStore.getSearching(),
      values: AppBarStore.getValues(),
      title: AppBarStore.getTitle(),
      actions: AppBarStore.getActions()
    };
  },

  componentDidMount() {
    AppBarStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function() {
    AppBarStore.removeChangeListener(this.onChange);
  },

  componentDidUpdate: function() {
    if (this.state.searching) {
      if (this.refs.criteria) {
        this.refs.criteria.focus();
      }
    }
  },

  onChange: function() {
    var values = AppBarStore.getValues()
    let bcpvalues = ContentStore.getBcpContent();
    values = values || (bcpvalues.policyNumber) ? {criteria: bcpvalues.policyNumber} : null;

    let searching = AppBarStore.getSearching() || (bcpvalues.policyNumber) ? true : false;
    this.setState({
      searching: searching,
      values: values,
      title: AppBarStore.getTitle(),
      actions: AppBarStore.getActions()
    })
  },

  changeMenuState(){
    MenuActions.updateMenuState();
  },

  showSearchBar() {
    // this.setState({searching: true})
    AppBarActions.showSearchBar(true);
  },

  hideSearchBar() {
    // this.setState({searching: false})
    AppBarActions.showSearchBar(false);
  },

  cleanCriteria() {
    AppBarActions.updateCriteria('', false);
  },

  handleBack(e, target) {
    AppBarActions.back(target, 2);
  },

  levelThreehandleBack(e, target) {
    AppBarActions.back(target, 3);
  },

  handleSelectionChange(id, value) {
    AppBarActions.updateFilter(id, value);
  },

  handleVersionChange(action, version) {
    AppBarActions.changeVersion(action, version)
  },

  handleCriteriaChange(e) {
    var value = e.target.value;
    AppBarActions.updateCriteria(value, true);
    this.clearselection();
  },

  handleCriteriaEnter(e, e2) {
    var value = e.target.value;
    AppBarActions.updateCriteria(value, false);
    this.clearselection();
  },

  clearselection() {
    if(this.props.assignPage){
      // AssignPageActions.updateSelectedRow(null, null, null);
    }else{
      MasterTableActions.updateSelectedRow([], null, null);
    }    
  },

  handleSearchFieldChange(e) {
    var value = e.target.value;
    AppBarActions.submitSearchField(this.searchFieldTarget, value);
  },

  handleTap(e, target) {
    // for icon button
    var path = e.target.id;
    // for button
    if (path == "") {
      path = target.id;
    }
    var page = HomeStore.getCurrentPage();
    var data = {
      p1: page.module,
      p2: convertObj2EncodedJsonStr(page.value)
    }

    console.debug('handle path =', path);
    callServer(path, data)
  },

  handleDialogTap(dialog) {
    // console.debug("[DynToolbar] - [handleDialogTap]", target);
    if (dialog) {
      return function (e) {
        DialogActions.showDialog(dialog);
      };
    }
  },

  // handle submit button tap
  handleSubmitTap(e, target) {
    console.debug("handleSubmitTap", target);
    if (target) {
      AppBarActions.submitCurrentId(target.id)
    }
  },

  // handle submitfull button tap
  handleSubmitChangedTap(e, target) {
    console.debug("submitButton", target);
    if (target) {
      AppBarActions.submitChangedValues(target.id)
    }
  },

  handleSubmitChangedTapWithConfirm(e, target) {
    console.debug("submitButtonWithConfirm", target);
    if (target) {
      AppBarActions.submitCurrentIdWithConfirm(target.id)
    }
  },

  handleSubmitSelectionTap(e, target) {
    if (target) {
      AppBarActions.submitSelected(target.id)
    }
  },

  _genSearchBar: function(hints) {
    var self = this;
    var values = this.state.values;
    var menuState = MenuStore.getMenuState();
    var styles = this.getStyles()

    return (
      <Toolbar key="searchBar" style={{
          position: 'absolute',
          width: (window.innerWidth - (menuState?280:0)) + 'px',
          height: this.state.searching?56:0,
          overflow: this.state.searching?'visible':'hidden',
          // transition: Transitions.easeOut(),
          // backgroundColor: appTheme.palette.hoverBackground,
          top:'0px'
      }}>
      <ToolbarGroup style={{ width: 'calc(100% - 100px)', display:'flex'}}>
        <FontIcon
          key="backBtn"
          tooltip="Back"
          style= { styles.iconStyle }
          className="material-icons"
          onClick={this.hideSearchBar}>
          arrow_back
        </FontIcon>
        <TextField
          key = "criteria"
          ref = "criteria"
          style = {{ width: 'calc(100% - 100px)', lineHeight:'56px'}}
          inputStyle = {styles.contentStyle}
          hintText = {hints?hints:"Search by code or name..."}
          hintStyle = {styles.contentStyle}
          value = {values['criteria']?values['criteria']:""}
          onEnterKeyDown = { this.handleCriteriaEnter }
          onChange = {this.handleCriteriaChange}
          underlineStyle={{display:'none'}}
          />
      </ToolbarGroup>
      <ToolbarGroup float="right">
        <FontIcon
          key="cancelBtn"
          tooltip="Cancel"
          style= { styles.iconStyle }
          className = "material-icons"
          onClick = {self.cleanCriteria}>
          cancel
        </FontIcon>
      </ToolbarGroup>
    </Toolbar>)
  },

  _genItem(item) {
    var values = this.state.values;
    var styles = this.getStyles()
    var content = ContentStore.getPageContent();
    var disabled = false;
    if (content) {
      var {
        template,
        valuesBackup,
        changedValues
      } = content;
      disabled = disableTrigger(item, template, valuesBackup, changedValues);
    }

    switch (item.type) {
      case 'label':
        return (
          <ToolbarTitle text={item.title} />
        );
      break;
      case 'iconButton':
        return (
          <FontIcon
            key={item.id}
            id={item.id}
            tooltip={item.title}
            disabled = {disabled}
            style= { styles.iconStyle }
            color = { styles.contentStyle.color }
            hoverColor = { styles.contentStyle.color }
            className="material-icons"
            onClick={(item.disabled)?null:this.handleTap}>
            {item.value}
          </FontIcon>
        );
      break;
      case 'searchButton':
        this.searchBar = this._genSearchBar(item.value);
        return (
          <FontIcon
            key={item.id}
            tooltip={item.title}
            style= { styles.iconStyle }
            className="material-icons"
            onClick={this.showSearchBar}>
            search
          </FontIcon>
        );
      case 'searchField':
        this.searchFieldTarget = item.value;
        return (<TextField
          key = "criteria"
          ref = "criteria"
          style = {{ width: '500px', lineHeight:'56px' }}
          inputStyle = {styles.contentStyle}
          hintText = {item.title?item.title:"Search by code or name..."}
          hintStyle = {styles.contentStyle}
          onChange = {this.handleSearchFieldChange}
          underlineStyle= {{display:'none'}}
          />);
        break;
      case 'submitCurrentButton': // for Edit / Delete / Revert / Approve
        return (
          <FlatButton
            key={item.id}
            id={item.id}
            target={item}
            secondary = { true }
            disabled = {disabled}
            style={styles.flatButtonStyle}
            onTouchTap={this.handleSubmitTap}>
            {item.title}
          </FlatButton>
        );
        break;
      case 'submitChangedButton': // for Save
        return (
          <FlatButton
            key={item.id}
            id={item.id}
            target={item}
            secondary = { true }
            disabled = {disabled}
            style={styles.flatButtonStyle}
            onTouchTap={this.handleSubmitChangedTap}>
            {item.title}
          </FlatButton>
        );
        break;

        case 'submitChangedButtonWithConfirm': // for Save
          return (
            <FlatButton
              key={item.id}
              id={item.id}
              target={item}
              secondary = { true }
              disabled = {disabled}
              style={styles.flatButtonStyle}
              onTouchTap={this.handleSubmitChangedTapWithConfirm}>
              {item.title}
            </FlatButton>
          );
          break;

      case 'submitSelectedButton': // for clone muliple selected row
        return (
          <FlatButton
            key={item.id}
            id={item.id}
            target={item}
            secondary = { true }
            disabled = {disabled}
            style={styles.flatButtonStyle}
            onTouchTap={this.handleSubmitSelectionTap}>
            {item.title}
          </FlatButton>
        );
        break;
      case 'button':
        return (
          <FlatButton
            key={item.id}
            id={item.id}
            target={item}
            secondary = { true }
            disabled = {disabled}
            style={styles.flatButtonStyle}
            onTouchTap={this.handleTap}>
            {item.title}
          </FlatButton>
        );
        break;
      case 'dialogButton':
        return (
          <FlatButton
            key={item.id}
            id={item.id}
            target={item}
            secondary = { true }
            style={styles.flatButtonStyle}
            onTouchTap={this.handleDialogTap(item.dialog)} >
            {item.title}
          </FlatButton>
        );
        break;
      case 'iconDialogButton':
        var handleTap = function() {
            if (this.dialog) {
              DialogActions.showDialog(this.dialog);
            }
        }.bind(item)

        return (
          <FontIcon
            key={item.id}
            id={item.id}
            tooltip={item.title}
            target={item}
            style= { styles.iconStyle }
            color = { styles.contentStyle.color }
            hoverColor = { styles.contentStyle.color }
            className="material-icons"
            onTouchTap={handleTap} >
            {item.value}
          </FontIcon>
        );
      case 'picker':
        return (
          <SelectField
            ref = {item.id}
            key = {item.id}
            template = {item}
            changedValues = {values}
            style = { styles.pickerStyle }
            iconStyle={{ fill: styles.contentStyle.color}}
            labelStyle = { this.mergeStyles(styles.contentStyle, { paddingRight: '28px'}) }
            underlineStyle={{display:'none'}}
            handleChange={this.handleSelectionChange}/>
        );
        case 'versionPicker':
        return (
          <SelectField
            ref = {item.id}
            key = {item.id}
            template = {item}
            value = {values[item.id]?values[item.id]:item.value}
            style={{width: 'auto', height: '56px'}}
            iconStyle={{ fill: styles.contentStyle.color}}
            labelStyle = {styles.contentStyle}
            underlineStyle={{display:'none'}}
            handleChange={this.handleVersionChange}/>
        );
        break;
      case 'iconMenu':
        return(
          <IconMenu
            key = {item.id}
            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
            targetOrigin={{horizontal: 'right', vertical: 'top'}}
            iconButtonElement={
              <IconButton touch={true} style={styles.iconStyle}>
                <FontIcon
                  color= {appTheme.palette.alternateTextColor}
                  className="material-icons">
                  more_vert
                </FontIcon>
              </IconButton>
            } >
            {this._genMenuItems(item.items)}
          </IconMenu>
        );
        break;
      default:
        return null
    }
  },

  _genItems: function(group) {
    var items = [];
    for (let i in group) {
      var item = group[i];
      var view = this._genItem(item);
      if (view) {
        items.push(view)
      }
    }
    return items;
  },

  _genMenuItems: function(menuItems){
    var items = [];
    for (let i in menuItems) {
      var mi = menuItems[i];
      items.push(
        <MenuItem
          key={"mi"+i}
          primaryText={mi.title}
          onTouchTap={mi.dialog ? this.handleDialogTap(mi.dialog) : undefined} />
      );
    }
    return items;
  },

  _genTitle: function(title) {
    var views = [];
    var styles = this.getStyles();
    switch (title.type) {
      case 'label':
        return (
          <span key="pTitle" style={styles.labelStyle}>{title.primary}</span>
        );
      break;
      case 'levelTwo':
        var backPage = HomeStore.getBackPage();
        views.push(
          <FlatButton
            key={title.id}
            id={title.id}
            style={styles.buttonStyle}
            secondary = { true }
            target={title}
            onTouchTap={this.handleBack}>
            {title.primary?title.primary:(backPage && backPage.title)?backPage.title:null}
          </FlatButton>
        )
        views.push(
          <span
            key="sp"
            style={ styles.seperatorStyle }
            className="material-icons"
            >keyboard_arrow_right</span>
        )
        if(title.secondary.type){
          views.push(
            this._genItem(title.secondary)
          );
        }else{
          views.push(
            <span key="sTitle" style={styles.labelStyle}>{title.secondary}</span>
          );
        }

        break;
      case 'levelThree' :
        let self = this;
        var backPage = HomeStore.getBackPage();
        let flatbutton = function(data, appBarLevel) {
          return <FlatButton
            key={data.id}
            id={data.id}
            style={styles.buttonStyle}
            secondary = { true }
            target={data}
            onTouchTap={(appBarLevel == 2) ? self.handleBack : self.levelThreehandleBack}>
            {data.primary? data.primary:
              data.title? data.title:
              (backPage && backPage.title)?backPage.title:null}
          </FlatButton>;
        }
        let arrow_right = function (key) {
          return <span
            key={"sp" + key}
            style={ styles.seperatorStyle }
            className="material-icons"
            >keyboard_arrow_right</span>;
        }

        views.push(flatbutton(title, 3))
        views.push(arrow_right("1"))
        views.push(flatbutton(title.secondary, 2))
        // views.push(
        //   this._genItem(title.secondary)
        // )
        views.push(arrow_right("2"))
        views.push(
          this._genItem(title.tertiary)
        )
        break;
      default:
        var view = this._genItem(title);
        if (view) {
          views.push(view)
        }
    }
    return views;
  },

  render: function() {

    var title = null, rightItems = null;
    var styles = this.getStyles();
    this.searchBar = null;

    if (this.state.title) {
      title = this._genTitle(this.state.title);
    }

    rightItems = (
      <ToolbarGroup key="right" float="right" style={styles.actionsPaneStyle}>
        {this._genItems(this.state.actions)}
      </ToolbarGroup>
    );
    return (
      <div key="appBar" style={{ height: '58px', display: 'block', overflow:'hidden' }}>
        <Toolbar key="appToolBar" style={{padding:"0px", boxShadow:"0px 2px 2px rgba(0,0,0,.33)", position:'relative'}} >
          <ToolbarGroup key="left" style={{display:'flex'}}>
            <FontIcon
              key='menuBtn'
              tooltip='Main Menu'
              style = {{padding: '0px 15px 0 24px'}}
              className="material-icons"
              onClick={this.changeMenuState} >
              menu
            </FontIcon>
            {title}
          </ToolbarGroup>
          {rightItems}
        </Toolbar>
        {this.searchBar}
      </div>
    );
  }
});


module.exports = DynToolbar;
