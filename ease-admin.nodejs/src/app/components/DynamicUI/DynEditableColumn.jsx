let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let Transitions = mui.Styles.Transitions;

let Colors = mui.Styles.Colors;
let FlatButton = mui.FlatButton;
let Tabs = mui.Tabs;
let Tab = mui.Tab;

let Dialog = require('../CustomView/Dialog.jsx');

let EABTextField = require('../CustomView/TextField.jsx');
let EABPickerField = require('../CustomView/PickerField.jsx');
let EABDatePickerField = require('../CustomView/DatePickerField.jsx');
let EABSwitchField = require('../CustomView/SwitchField.jsx');
let EABCheckBox = require('../CustomView/CheckBox.jsx');
let EABCheckBoxGroup = require('../CustomView/CheckBoxGroup.jsx');
let EABViewOnlyField = require('../CustomView/ViewOnlyField.jsx');
let EABMultipleTextField = require('../CustomView/MultipleTextField.jsx');
let EABCheckBoxDialog = require('../CustomView/CheckBoxDialog.jsx');
let EABRadioGroup = require('../CustomView/RadioGroup.jsx');
let EABDetailsList = require('../CustomView/DetailsList.jsx');
let EABDetailsTab = require('../CustomView/DetailsTab.jsx');
let EABComplexList = require('../CustomView/ComplexList.jsx');
let EABSelectList = require('../CustomView/SelectList.jsx');
let EABGrid = require('../CustomView/Grid.jsx');
let EABRateGrid = require('../CustomView/RateGrid.jsx');
let EABGenericGrid = require('../CustomView/GenerateGrid.jsx');
let EABHtmlEditor = require('../CustomView/HtmlEditor.jsx');
let EABCssEditor = require('../CustomView/CssEditor.jsx');
let EABJSBoxField = require('../CustomView/JSBoxField.jsx');
let JavascriptField = require('../CustomView/JavascriptField.jsx');
let EABSelectAllButton = require('../CustomView/SelectAllButton.jsx');
let EABTable = require('../CustomView/Table.jsx');
let EABFileUpload = require('../CustomView/FileUpload.jsx');
let ContentStore = require('../../stores/ContentStore.js');
let AppBarActions = require('../../actions/AppBarActions.js');
let DialogActions = require('../../actions/DialogActions.js');

var beautify = require('js-beautify').js_beautify;

import appTheme from '../../theme/appBaseTheme.js';

let DynEditableColumn = React.createClass({

  mixins: [React.LinkedStateMixin, StylePropable],

  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme,
    };
  },

  contextTypes: {
    muiTheme: React.PropTypes.object
  },

  propTypes:{
    id: React.PropTypes.string.isRequired,
    show: React.PropTypes.bool.isRequired,
    changeActionIndex: React.PropTypes.func,
    switchAction: React.PropTypes.bool,
    style: React.PropTypes.object,
    template: React.PropTypes.object,
    changedValues: React.PropTypes.object,
    rootValues: React.PropTypes.object,
    showDiff: React.PropTypes.bool,
    dateBy: React.PropTypes.string
  },

  getDefaultProps() {
    var content = ContentStore.getPageContent();

    return {
      showDiff: false
    }
  },

  componentWillReceiveProps(nextProps){
    let content = ContentStore.getPageContent();
    this.state.values = nextProps.values?nextProps.values:(content.values?content.values:{});
    this.state.changedValues = nextProps.changedValues?nextProps.changedValues:(content.changedValues?content.changedValues:null);
    if (!this.state.changedValues && !nextProps.changedValues) {
      this.state.changedValues = content.changedValues = cloneObject(this.state.values);
    }
    this.state.template = nextProps.template ? nextProps.template : content.template;
  },

  getInitialState: function() {
    var content = ContentStore.getPageContent();
    var changedValues = this.props.changedValues?this.props.changedValues:(content.changedValues?content.changedValues:{});
    var values = this.props.values?this.props.values:(content.values?content.values:cloneObject(changedValues));

    return {
      values: values,
      changedValues: changedValues,
      template: this.props.template ? this.props.template : content.template,
      muiTheme: this.context.muiTheme ? this.context.muiTheme : appTheme.getTheme()
    }
  },

  handleSubmitTap(e, target) {
    AppBarActions.submitCurrentId(target.id);
  },

  handleAssignSubmitTap(e, target) {
    if (target.dialog) {
      var dialog = target.dialog;
      dialog.positive.handleTap = function(values){
        AppBarActions.submitCurrentTaskId(target.id);
      };
      DialogActions.showDialog(dialog);
    } else {
      AppBarActions.submitCurrentTaskId(target.id);
    }
  },

  getStyles() {
    let theme = this.state.muiTheme;

    return {
      floatingLabel : {
        lineHeight: '22px',
        top: '38px',
        bottom: 'none',
        fontSize: '16px',
        opacity: 1,
        color: theme.textField.fixedLabelColor,
        transform: 'perspective(1px) scale(0.875) translate3d(0px, -28px, 0)',
        transformOrigin: 'left top',
        position: 'absolute',
        zIndex: 100
      },
      titleStyle : {
        display: 'block',
        fontSize: '20px',
        lineHeight: '64px'
      },
      dateBy: {
        color: theme.textField.fixedLabelColor
      },
      buttonStyle: {
        marginLeft: 16,
        color: appTheme.palette.primary2Color,
        backgroundColor:appTheme.palette.background2Color,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: appTheme.palette.borderColor
      }
    }
  },

  runningId: 0,
  showCondItems: null,
  resetValueItems: null,

  _genItem(its, item, values, changedValues, handleChange, index, showCondItems) {
    var self = this;
    var value, cValue;

    var rootValues = this.props.rootValues || this.state.changedValues || this.state.values;
    // current value
    if (!values) {
      values = this.state.values;
    }
    value = item.id && values? values[item.id] : values;

    var lang = "en";

    // changed value
    if (!changedValues) {
      changedValues = this.state.changedValues;
    }
    cValue = item.id ? changedValues[item.id] : changedValues;

    // if it is a new change get default value if changed value is empty
    if (changedValues.isNew && !cValue && item.value) {
      cValue = item.value
      changedValues[item.id] = item.value
    }

    // check show condition
    if (!showCondition(item, rootValues, changedValues, showCondItems)) {
      return;
    }

    var styles = this.getStyles();
    var viewType = this.state.template.type;

    var title = null;
    if (item.title) {
      title = getLocalText(lang, item.title);

      title = (
        <div>
          <span style={styles.floatingTitle}>{title}</span>
          {item.mandatory?<span style={ {color:'red'} }>&nbsp;*</span>:null}
        </div>)
    };
    if (!item.type || item.type.toUpperCase() == 'READONLY' || item.type.toUpperCase() == 'ROWKEY') {
      its.push(
        <EABViewOnlyField
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { changedValues }
          rootValues = { rootValues } />)
    }
    if (!item.type || item.type.toUpperCase() == 'HIDDEN') {
      // its.push(
      //   <EABViewOnlyField
      //     ref = {item.id}
      //     key = {"ctn_"+ item.id}
      //     template = { item }
      //     index = { index }
      //     values = { changedValues }
      //     rootValues = { rootValues } />)
      if (!changedValues[item.id] && item.value) {
        changedValues[item.id] = item.value;
      }
    }
    else if (item.type.toUpperCase() == 'TEXT' || item.type.toUpperCase() == 'TEXTFIELD') {
      its.push(
        <EABTextField
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          changedValues = { changedValues }
          requireShowDiff = { this.props.showDiff }
          changeActionIndex = { this.props.changeActionIndex }
          rootValues = { rootValues }
          handleChange= { handleChange }/>
      );
    }
    else if (item.type.toUpperCase() == 'PICKER') {
      its.push(
        <EABPickerField
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          changedValues = { changedValues }
          resetValueItems = { this.resetValueItems }
          requireShowDiff = { this.props.showDiff  }
          changeActionIndex = { this.props.changeActionIndex }
          width = {item.colWidth?item.colWidth:''}
          callback= {item.id=="selectedFund"?function(){
              self.forceUpdate();
          }:""}
          handleChange = { handleChange }
          rootValues = { rootValues } />
      );
    }
    else if (item.type.toUpperCase() == 'DATEPICKER') {
      its.push(
        <EABDatePickerField
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          disabled = {item.disabled}
          changedValues = { changedValues }
          requireShowDiff = { this.props.showDiff  }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues } />
      );
    }
    else if (item.type.toUpperCase() == 'SWITCH') {
      its.push(
          <EABSwitchField
            ref = {item.id}
            key = {"ctn_"+ item.id}
            template = { item }
            index = { index }
            values = { values }
            mode = "landscape"
            changedValues = { changedValues }
            requireShowDiff = { this.props.showDiff  }
            changeActionIndex = { this.props.changeActionIndex }
            handleChange = { handleChange }
            rootValues = { rootValues }  />
      );
    }
    else if (item.type.toUpperCase() == 'CHECKBOX') {
      its.push(
        <EABCheckBox
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          changedValues = { changedValues }
          requireShowDiff = { this.props.showDiff }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues } />
      )
    }
    else if (item.type.toUpperCase() == 'CHECKBOXGROUP' && item.options) {
      its.push(
        <EABCheckBoxGroup
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          changedValues = { changedValues }
          resetValueItems = { this.resetValueItems }
          requireShowDiff = { this.props.showDiff }
          changeActionIndex = { this.props.changeActionIndex }
          disabled = {item.disabled}
          handleChange = { handleChange }
          rootValues = { rootValues } />)
    }
    else if (item.type.toUpperCase() == 'RADIOGROUP') {
      its.push(
        <EABRadioGroup
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          disabled = { item.disabled }
          changedValues = { changedValues }
          requireShowDiff = { this.props.showDiff }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues }  />)
    }
    else if (item.type.toUpperCase() == 'MULTTEXT') {
      its.push(
        <EABMultipleTextField
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          changedValues = { changedValues }
          disabled = {item.disabled}
          requireShowDiff = { this.props.showDiff }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues } />)
    }
    else if (item.type.toUpperCase() == 'CHECKBOXDIALOG') {
      its.push(
        <EABCheckBoxDialog
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          changedValues = { changedValues }
          requireShowDiff = { this.props.showDiff }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues } />)
    }
    else if (item.type.toUpperCase() == 'COMPLEXLIST') {
      // this.showCondItems[item.id] = item;
      if (this.showCondItems[item.id] instanceof Array) {
      } else {
        this.showCondItems[item.id] = [];
      }
      this.showCondItems[item.id].push(item);

      its.push(
        <EABComplexList
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          changedValues = { changedValues }
          requireShowDiff = { this.props.showDiff }
          changeActionIndex = { this.props.changeActionIndex }
          rootValues = { rootValues }
          handleChange = { handleChange }
          genItemsFunc = { this._genItem }/>
      );
    }
    else if (item.type.toUpperCase() == 'DETAILSLIST') {
      // this.showCondItems[item.id] = item;
      if (this.showCondItems[item.id] instanceof Array) {
      } else {
        this.showCondItems[item.id] = [];
      }
      this.showCondItems[item.id].push(item);
      its.push(
        <EABDetailsList
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          changedValues = { changedValues }
          collapsed = { true }
          allowAdd = { item.allowAdd }
          requireShowDiff = { this.props.showDiff }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues } />
      );
    }
    else if (item.type.toUpperCase() == 'DETAILSTAB') {
      // this.showCondItems[item.id] = item;
      if (this.showCondItems[item.id] instanceof Array) {
      } else {
        this.showCondItems[item.id] = [];
      }
      this.showCondItems[item.id].push(item);
      its.push(
        <EABDetailsTab
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          changedValues = { changedValues }
          collapsed = { true }
          allowAdd = { item.allowAdd }
          requireShowDiff = { this.props.showDiff }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues }
          genItemsFunc = { this._genItem }/>
      );
    }
    else if (item.type.toUpperCase() == 'SELECTLIST'){
      its.push(<EABSelectList
        ref = {item.id}
        key = {"ctn_"+ item.id}
        template = { item }
        index = { index }
        values = { values }
        changedValues = { changedValues }
        disabled = { item.disabled }
        requireShowDiff = { this.props.showDiff }
        changeActionIndex = { this.props.changeActionIndex }
        handleChange = { handleChange }
        rootValues = { rootValues } />)
    }
    else if (item.type.toUpperCase() == 'FILEUPLOAD' || item.type.toUpperCase() == 'IMAGES') {
      its.push(
        <EABFileUpload
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          changedValues = { changedValues }
          requireShowDiff = { this.props.showDiff }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues } />)
    }
    else if (item.type.toUpperCase() == 'JSBOX') {
      its.push(
        <EABJSBoxField
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          disabled = {item.disabled}
          changedValues = { changedValues }
          requireShowDiff = { this.props.showDiff }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues } />
      );
    }
    else if (item.type.toUpperCase() == 'TABLE') {
      its.push(
        <EABTable
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          changedValues = { changedValues }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues } />
      );
    }
    else if (item.type.toUpperCase() == 'GRID') {
      its.push(
        <EABGrid
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          disabled = { item.disabled }
          changedValues = { changedValues }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues }/>
      );
    }
    else if (item.type.toUpperCase() == 'RATEGRID') {
      its.push(
        <EABRateGrid
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          disabled = { item.disabled }
          changedValues = { changedValues }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues }/>
      );
    }
    else if (item.type.toUpperCase() == 'CSSEDITOR'){
      its.push(
        <EABCssEditor
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          disabled = { item.disabled }
          changedValues = { changedValues }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues }/>
      )
    }
    else if (item.type.toUpperCase() == 'HTMLEDITOR'){
      its.push(
        <EABHtmlEditor
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          disabled = { item.disabled }
          changedValues = { changedValues }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          rootValues = { rootValues }/>
      )
    }
    // js editor
    else if ("JSFIELD" == item.type.toUpperCase()){
      this.fullScreen = true;
      var disabled = item.disabled;

      var cVal = cValue;
      if (cValue && typeof cValue == 'object') {
        cVal = JSON.stringify(cValue);
      }
      if (cVal) {
        cVal = beautify(cVal, { indent_size: 4});
      }
      var title = item.title;
      its.push(
        <div className={"DetailsItem"}>
          {title?<label key="label" className="Title" style={styles.titleStyle}>{title}</label>:null}
          <JavascriptField
            id = {item.id}
            key = {item.id}
            ref = {item.id}
            className = { "ColumnField" }
            disabled = {disabled}
            height = "300px"
            handleChange = { handleChange }
            style = { {
              display: 'block'
            } }
            onBlur = {
              function(value, errorMsg) {
                self.state.editing = false;
                if (!errorMsg) {
                  changedValues[this.id] = JSON.parse(value);
                } else {
                  changedValues[this.id] = value;
                }
                self.forceUpdate();
              }.bind(item)
            }
            onFocus = {
              function() {
                self.setState({
                  editing: true
                });
              }
            }
            mode = "json"
            defaultValue = { cVal }
            />
        </div>
      );
    }
    else if (item.type.toUpperCase() == 'GENERICGRID') {
      its.push(
        <EABGenericGrid
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          index = { index }
          values = { values }
          disabled = { item.disabled }
          changedValues = { changedValues }
          changeActionIndex = { this.props.changeActionIndex }
          handleChange = { handleChange }
          height = { "100%" }
          rootValues = { rootValues }/>
      );
    }
    else if (item.type.toUpperCase() == 'LIST') {
      var subItems = [];

      if (!cValue) {
          if (!changedValues) {
            cValue = this.state.changedValues[item.id] = {};
          } else {
            cValue = changedValues[item.id] = {};
          }
      }

      for (let si in item.items) {
        var subItem = item.items[si];
        subItem.parentType = item.type;
        this._genItem(subItems, subItem, value, cValue, handleChange, si, this.showCondItems);
      }

      its.push(
        <div key={"list_"+ (this.runningId++)}
          className="ListField">
          {title?<label key="label" className="Title" style={ styles.titleStyle }> {title} </label>:null}
          {subItems}
        </div>)
    }
    else if (item.type.toUpperCase() == 'HBOX' || item.type.toUpperCase() == 'SECTION') {

      // either column2, column3, column4
      var additionalPresentClass = item.presentation?" "+item.presentation:"";
      if (!additionalPresentClass && item.type.toUpperCase() == 'SECTION') {
        additionalPresentClass = 'column1';
      }
      if (item.items.length) {
        var titleNode = null;
        if (title) {
          if (item.parentType && item.parentType.toUpperCase() == 'LIST') {
            titleNode = (<label key="label" className="Label" style={styles.floatingLabel}>{title}</label>)
          } else {
            titleNode = (<label key="label" className="Title" style={styles.titleStyle}>{title}</label>)
          }
        }

        if (!cValue && item.id) {
          if (!changedValues) {
            cValue = this.state.changedValues[item.id] = {};
          } else {
            cValue = changedValues[item.id] = {};
          }
        }

        var subItems = [];
        for (let si in item.items) {
          var _item = item.items[si];
          this._genItem(subItems, _item, value, cValue, handleChange, si, this.showCondItems);
        }

        if (subItems.length) {
          its.push(
            <div key= {"hbox"+(this.runningId++)} className={"hBoxContainer"+additionalPresentClass}>
              {titleNode}
              {subItems}
            </div>)
        }
      }
    }
    else if (item.type.toUpperCase() == 'SELECTALLBUTTON') {
      its.push(<EABSelectAllButton
        key={ "btn"+this.runningId }
        template = {item}
        index = { index }
        changedValues = { changedValues }
        changeActionIndex = { this.props.changeActionIndex }
        requestChange = { function(keys) {
          for (let k in keys) {
            var key = keys[k];
            self.refs[key].forceUpdate();
          }
        }.bind(item)} />)
    }
    else if (item.type.toUpperCase() == 'TABS') {
      if (item.items) {
        var tabsItems = [];

        for (let i = 0; i < item.items.length; i++) {
          var item_ = item.items[i];
          var tabItems = [];

          // tab items
          if (item_.type.toUpperCase() == 'tab'.toUpperCase() && item_.items) {
            for (let j = 0; j < item_.items.length; j++) {
              var item__ = item_.items[j];
              this._genItem(tabItems, item__, value, cValue, handleChange, j, this.showCondItems);
            }

            var ititle = getLocalText(lang, item_.title)

            tabsItems.push(
              <Tab
                key= {"tab"+i}
                label={ititle}
                value={i}
                style={{height: '100%'}} >
                {tabItems}
              </Tab>
            );
          }
        }

        its.push(
          <Tabs
            key= {"tabs"+this.runningId}
            style={{ height: "100%"}}
            contentContainerStyle = {{height: 'calc(100% - 48px)'}}
            value={0} >
            {tabsItems}
          </Tabs>
        );
      }
    }
  },

  _genHeader(template) {
    var viewType = template.type;
    var self = this;
    var styles = this.getStyles();

    var actionStyle={
      padding: '5px'
    }

    var actionsPaneStyle = {
      position: 'absolute',
      top: '16px',
      right: '0px',
      padding: '0 24px'
    }

    var headerStyle = {
      position: 'relative',
      display: 'inline-block',
      width: '100%'
    }

    var actions = [];
    for (let _ai in template.items){
      var _item = template.items[_ai];
      if(_item.type == 'action'){
        var actions = [];
        for (let ai in _item.items) {
          var action = _item.items[ai];
          if(showCondition(action, null, this.state.changedValues)){
            if(action.type=='assign'){
              actions.push(
                <FlatButton
                  key={"a"+ ai}
                  style= {styles.buttonStyle}
                  target={action}
                  label={action.title}
                  onTouchTap={self.handleAssignSubmitTap} />
              );
            }
          }
        }
      }
    }

    if (actions.length || (viewType == 'section' && template.title)) {
      var ttitle = getLocalText('en', template.title);
      return (
        <div key="header" style={ headerStyle }>
          {viewType == 'section' && ttitle?
            (<h2 key={'changeheader'}
               className="DetailsItem">
               {ttitle}
             </h2>) : null
          }
          {actions.length?
            (<div key="actions" style={ actionsPaneStyle }>
              {actions}
            </div>) : null
          }
        </div>
      );
    } else {
      return null;
    }
  },

  _genFooter() {
    var styles = this.getStyles()
    if (!this.state.changedValues.isNew && this.props.dateBy) {
      return <div className="DateBy" style={ styles.dateBy } dangerouslySetInnerHTML={{__html: genRecordDateBy(this.props.dateBy, this.state.changedValues)}}></div>
    }
    return null;
  },

  render: function() {
    console.debug('[DynEditableColumn] - [render]');

    var self = this;

    if (!this.props.show)
      return <div
        key={this.props.id}
        style={{ width:0 }}>
      </div>;

    // var content = ContentStore.getPageContent();
    // this.state.template = this.props.template? this.props.template: content.template;
    // this.state.values = this.props.changedValues?{}:content.values;
    // this.state.changedValues = this.props.changedValues?this.props.changedValues:(content.changedValues?content.changedValues:cloneObject(content.values));

    var {
      template,
      values,
      changedValues
    } = this.state;

    var fields = [];
    this.runningId = 0;

    this.showCondItems = {};
    this.resetValueItems = {};

    if (template.items.length > 0) {
      var sortedItems = template.items.sort(function(a,b) {
        var aa = a.detailSeq || 0;
        var bb = b.detailSeq || 0;
        return (aa < bb) ? -1 : (aa > bb) ? 1 : 0;
      });

      for (let i in sortedItems) {
        var item = sortedItems[i];
        if (item.detailSeq) {
          this._genItem(fields, item, values, changedValues, null, i, this.showCondItems);
        }
      }
    }

    return (
      <div key={this.props.id}
        style={ this.mergeStyles( this.props.style, {transition: Transitions.easeOut() }) }
        className={this.props.className || "DetailsColumn"}>
        {this._genHeader(template)}
        <div key={"fields"} style={{ padding:'0 0 24px' }}>
          {fields}
          {this._genFooter()}
        </div>
      </div>
    );
  }

});

module.exports = DynEditableColumn;
