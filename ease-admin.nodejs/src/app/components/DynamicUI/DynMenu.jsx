let React = require('react');
let mui = require('material-ui');

let ContentStore = require('../../stores/ContentStore.js');

let DynMenuActions = require('../../actions/DynMenuActions.js');
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');
let DynConfigPanelStore = require('../../stores/DynConfigPanelStore.js');
let DialogActions = require('../../actions/DialogActions.js');
let ItemMinix = require('../../mixins/ItemMinix.js');

let FontIcon = mui.FontIcon;
let IconButton = mui.IconButton;
let BeneIllusDetailStore = require('../../stores/BeneIllusDetailStore.js');
let SortableList = require('../CustomView/SortableList.jsx');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let StylePropable = mui.Mixins.StylePropable;
import Divider from 'material-ui/lib/divider';
import List from 'material-ui/lib/lists/list';
import ListItem from 'material-ui/lib/lists/list-item';
import { SelectableContainerEnhance } from 'material-ui/lib/hoc/selectable-enhance';
let SelectableList = SelectableContainerEnhance(List);
import appTheme from '../../theme/appBaseTheme.js';
import Add from 'material-ui/lib/svg-icons/content/add';
let _nestedMenu = ['N','D'];
let _sortableMenu = ['S'];

let DynMenu = React.createClass({
  //mixins: [React.LinkedStateMixin, StylePropable],
  mixins: [ItemMinix],

  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: appTheme.getTheme(),
    };
  },

  getStyles() {
    return {
      // Styles
      menuHeaderStyle:{
        width:'100%',
        height:'58px',
        lineHeight:'58px',
        display:'flex'
      },
      headerStyle: {
        fontSize: '20px',
        paddingLeft: '16px'
      },
      subheaderStyle: {
        paddingLeft: '16px',
        color: "gray"
      },
      dividerStyle: {
        marginTop: '4px',
        marginBottom: '4px'
      },
      selectionListStyle: {

        backgroundColor:'transparent'
      },
      selectedItemStyle: {
        backgroundColor: 'transparent',
        color:this.state.isCsClicked?'#000000': appTheme.palette.primary1Color
      },

      sortableItemStyle: {
        marginLeft: '0px',
        paddingLeft: '16px',
        paddingRight: '16px',
        height: '48px',
        lineHeight: '48px',
        position: 'relative'
      },
      sortableItemBottomStyle: {
        marginLeft: '0px',
        height: '48px',
        lineHeight: '48px',
        position: 'absolute',
        bottom: 0,
        zIndex:100,
        width:'100%',
        textAlign:'center',
      },
      addButtonCsStyle: {
        verticalAlign:'middle',
        float:'right'
      },
      addButtonBiStyle: {
        verticalAlign:'middle',
        width:'100%',
        backgroundColor:appTheme.palette.background2Color
      },
      subMenuListStyle:{
        position: 'absolute',
        left: '0px',
        top: '0px',
        height: "calc(100vh - 56px)",
        paddingTop: '0px',
        width: "278px",
        backgroundColor: '#FFFFFF',
        color: appTheme.palette.primary1Color
      },
      body:{
        height: "calc(100vh - 56px)",
        position:"relative",
        width:"280px",
        overflowY: 'auto',
        display: "inline-block"
      },
      border:{
        position:"absolute",
        right:"0px",
        top:"0px",
        width:"2px",
        height:"calc(100vh - 56px)",
        backgroundColor:"#DDDDDD"
      },
    }
  },

  getInitialState() {
    var isLockBi = BeneIllusDetailStore.isLock();
    let content=ContentStore.getPageContent();
    let biPages={};

    let {
      items
    } = this.props;

    let sortSectionId = items && items[1] && items[1][0]? items[1][0].id : '';
    if(content.values[sortSectionId])
    biPages=content.values[sortSectionId];
    return {
      selectedIndex: items && items[0] && items[0][0]? items[0][0].id : '',//(this.props.parents)?0:1,
      show:false,
      changedValues:this.props.items,
      lastSelMenuId:'',
      isCsClicked: false,
      isPanelClosed:true,
      biPages:biPages ,
      isLockBi:isLockBi,
    };
  },

  handleBIMenu(selIdx){
    if(!this.state.isPanelClosed)
    return null;
    //indicate chaneg to cs menu
    this.state.isCsClicked=true;
    this.setState({selectedIndex: -1});

    let item=this.state.changedValues[1][0];//TODO:hardcoded postion in the array([1][0])
    //if(!isEmpty(item)&&checkExist(item, 'items')&&item.items.length>0 && selIdx <item.items[0].length){
    let content=ContentStore.getPageContent();
    let selectedId=content.changedValues[item.id].pages[selIdx].id;
    content.changedValues[item.id]['selectedPage']=selIdx;//selectedId;

    this.setState({
      lastSelMenuId:selectedId,
      disableAddBi: true,
      biPages: content.changedValues[item.id]
    });
    content.changedValues['selectPage']['id'] = item.id;
    DynMenuActions.changePage(this.props.module, item.id);

    this.forceUpdate();

  },

  handleBIMenuWithoutClick(){
    let item=this.state.changedValues[1][0];//TODO:hardcoded postion in the array([1][0])

    //if(!isEmpty(item)&&checkExist(item, 'items') &&item.items.length>0 ){//}&&newIdx <item.items[0].length){

    let content=ContentStore.getPageContent();
    let selectedId=this.checkLastSelMenuId(content.changedValues[item.id].pages);
    let selIdx="";
    if(selectedId)
    selIdx=content.changedValues[item.id].pages.map(function(e) { return e.id; }).indexOf(this.state.lastSelMenuId);
    if(selIdx>-1){
      content.changedValues[item.id]['selectedPage']=selIdx;
      this.setState({
        lastSelMenuId:selectedId,
        disableAddBi: true,
        biPages: content.changedValues[item.id]
      });
      content.changedValues['selectPage']['id'] = item.id;
      DynMenuActions.changePage(this.props.module, item.id);

      this.forceUpdate();
    }

    //  }

  },

  checkLastSelMenuId(sortableItems){

    for(let j=0; j<sortableItems.length; j++){
      let menuItem=sortableItems[j];
      if(menuItem.id==this.state.lastSelMenuId)
      return this.state.lastSelMenuId;
    }
    //select first item if not found
    if(sortableItems.length>0&&sortableItems[0].id){
      this.state.lastSelMenuId=sortableItems[0].id
      return sortableItems[0].id;
    }

    return null;

  },


  createNewBiPage(){
    this.state.isCsClicked=true;
    var id;

    let content=ContentStore.getPageContent();
    let sectionId= this.props.items[1][0].id;//'NeedsConceptSelling'
    if(!content.changedValues[sectionId]){
      content.changedValues[sectionId]={
        pages:[]
      }
    }
    let pages=content.changedValues[sectionId].pages;

    let prefix="";
    if(sectionId=="NeedsConceptSelling")
    prefix="CSr";
    else
    prefix="BIr";

    var max=0;
    for (var i = 0; i < pages.length; i++) {
      var currId=Number(pages[i].id.replace(prefix, ""));
      if( max < currId) {
        max=currId;
      }
    }

    var id=max+1;

    var newObj={
      "perm_id": prefix+id,
      "id": prefix+id,
      "seq": id*100,
      "title": {
      },
    };
    newObj.title=NeedDetailStore.genLangCodesObj();
    newObj.title.en=(sectionId=="NeedsConceptSelling")? "" :"Page "+id;
    pages.push(newObj);
    let self = this;

    //this.state.lastSelMenuId= newObj.id;
    let lastPage=pages[pages.length-1];
    if(sectionId=="NeedsConceptSelling"){
      let validation = function(){
        let errorMsg = false;

        if(!newObj.title.en)
        errorMsg= "Some mandatory data do not input yet, if you click OK button, this category would be deleted.";
        if(!errorMsg){
          console.log("validate");
        }else{
          console.log(errorMsg);
          let removeDialog = {
            id: "delete",
            title: getLocalizedText("ALERT","ALERT"),
            message: errorMsg,
            negative: {
              id: "delete_cancel",
              title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
              type: "button"
            },
            positive:{
              id: "delete_confirm",
              title: getLocalizedText("BUTTON.OK","OK"),
              type: "customButton",
              handleTap: function(){
                pages.splice(pages.length-1, 1);
                // self.state.lastSelMenuId=oldSelMenuId;
                DynConfigPanelActions.closePanel(true);
              }
            }
          }
          DialogActions.showDialog(removeDialog);
        }

        return errorMsg;
      };
      DynConfigPanelActions.openPanel(lastPage , 'question' , {
        validation: validation,
        onPanelClose: function(){
          if(newObj.title.en)
          self.handleBIMenu(pages.length-1);
        }
      });
    }else{
      if(newObj.title.en)
      self.handleBIMenu(pages.length-1);
    }
  },

  genSortMenuHeading(sortMenuId){
    let styles = this.getStyles();
    let {
      isLockBi,
      disableAddBi
    } =this.state;
    switch (sortMenuId) {
      case "NeedsConceptSelling":
      return(
        <div style={  styles.sortableItemStyle }>
          <div  >
            <IconButton   disabled={  !this.state.isPanelClosed   } style={ styles.addButtonCsStyle  } iconStyle={{color:'grey'}} className="material-icons"  onTouchTap={this.createNewBiPage}>
              <Add/>
            </IconButton>
          </div>
          <div>
            <span style={{verticalAlign:'middle' ,color:'grey', fontSize:'15px'}}>{ "Categories" }</span>
          </div>
        </div>
      )
      break;
      case "BiPages":
      return(
        <div  >
          <div style={ styles.sortableItemBottomStyle}>
            <IconButton   disabled={(isLockBi||disableAddBi)} style={styles.addButtonBiStyle } iconStyle={{color:'grey'}} className="material-icons"  onTouchTap={this.createNewBiPage}>
              <Add/>
            </IconButton>
          </div>
          <div style={  styles.sortableItemStyle}>
            <span style={{verticalAlign:'middle' ,color:'grey', fontSize:'15px'}}>{ 'Pages'}</span>
          </div>
        </div>
      )
      break;
    }
  },



  genMenu(level, menuList) {
    let rows = [];
    let subRows = [];
    let styles = this.getStyles();
    let self = this;
    let menuIndex = 1;
    let {
      isLockBi,
      disableAddBi
    } =this.state;

    var getSubMenu = function(item, index) {
      if (item && (item.type == 'L' || item.type == '') && item.items && item.items.length) {
        var listRows = [];
        for (let c = 0; c < item.items[0].length; c++) {
          var listRow = item.items[0][c];
          var lrTitle = getLocalText('en', listRow.title);

          var nItems = getSubMenu(listRow, menuIndex + ',' + c);

          if (nItems) {
            listRows.push(
              <ListItem
                key={listRow.id}
                value={ listRow.id }
                primaryTogglesNestedList={true}
                primaryText={lrTitle}
                disabled={listRow.disabled}
                nestedItems={nItems}  />
            );
          } else {
            listRows.push(
              <ListItem
                key={listRow.id}
                value={ listRow.id }
                primaryText={lrTitle}
                disabled={listRow.disabled}/>
            );
          }
        }
        return listRows;
      }
      return null;
    }


    if (menuList) {
      for (let i = 0; i < menuList.length; i++) {
        let sections = menuList[i];
        if(i>0){
          rows.push(<Divider/>);
        }
        for(let j = 0; j < sections.length; j++){
          let item = sections[j];
          let id = item.id;
          let type = item.type;
          let title = checkExist(item, 'title.en')?item.title.en:item.title ;//item.title;
          let disabled = item.disabled;
          let _level = level+'-'+j;
          let sortMenuId="";
          if(menuList[1]&& menuList[1][0] &&  menuList[1][0].id)
          sortMenuId=menuList[1][0].id;
          let subMenuHeight = (sortMenuId=="NeedsConceptSelling")?216: 448;
          if (_sortableMenu.indexOf(type)>-1) {
            let listItem =(type!='S')? (
              <ListItem
                value={ item.id }
                primaryText={title}
                disabled={disabled}
                style={{color:disabled?"#AAAAAA":"#000000"}}>
              </ListItem>
            ):
            (
              self.genSortMenuHeading(sortMenuId)
            );
            rows.push(listItem);
            let menuItems = [];
            //suppose sortable menu only has 1 section
            let biPages=this.state.biPages;
            let pages=[];
            if(biPages && biPages.pages)
            pages=biPages.pages;
            if(pages&&pages.length>0){
              if(pages&&pages.length>0){
                let  selIdx=biPages.selectedPage;
                for (let a= 0 ; a< pages.length; a++) {
                  let biPage = pages[a];
                  menuItems.push(
                    <div className="hightLight" onClick={this.handleBIMenu.bind(this, a)} style={this.mergeAndPrefix({ backgroundColor:this.state.isCsClicked && a==selIdx?'#2696CC':''},styles.sortableItemStyle)} >
                      <FontIcon  style={{verticalAlign:'middle', cursor:'pointer',}} className={ this.state.isPanelClosed && this.state.isCsClicked&&!isLockBi?"material-icons drag-area":"material-icons   "} >reorder</FontIcon>
                      <span style={{verticalAlign:'middle', paddingLeft: '16px'}}>{biPage.title.en}</span>
                    </div>
                  );
                }
              }
              let submenu = (
                <div style={{maxHeight:'calc(100vh - '+ subMenuHeight +'px)',  overflowY:'auto', overflowX:'hidden'}}>
                  <SortableList
                    key={"dsm"+_level+id}
                    ref={"dsm"+_level+id}
                    items={menuItems}
                    changedValues={pages?pages:""}
                    handleSort={this.handleBIMenuWithoutClick}/>
                </div>
              );
              rows.push(submenu);
            }
          }
          else if(_nestedMenu.indexOf(type)>-1){
            let listItem = (
              <ListItem
                value={ item.id }
                primaryText={title}
                disabled={disabled}
                style={{color:disabled?"#AAAAAA":"#000000"}}
                onClick={
                  function(){
                    this.refs["dsm"+_level+id].showMenu();

                  }.bind(this)
                }/>
            );
            let parents = this.props.parents?this.props.parents:[];
            // let sindex = cloneObject(menuIndex);
            let onSelectFunc = function(){
              self.setState({selectedIndex: this.id});
              if(self.props.onSelected){
                self.props.onSelected();
              }
            }.bind({id: item.id});
            parents.push(item.id);
            let submenu = (
              <DynMenu
                key={"dsm"+_level+id}
                ref={"dsm"+_level+id}
                parents={parents}
                level={_level}
                module={this.props.module}
                title={ checkExist(item, 'title.en')?item.title.en:item.title }
                items={item.items}
                onSelected={onSelectFunc}
                selected={this.state.selectedIndex==menuIndex?true:false}/>
            );
            rows.push(listItem);
            subRows.push(submenu);

          }else if(type=='L'){
            var listRows = getSubMenu(item, menuIndex);
            var listItem = (
              <ListItem
                key={id}
                primaryText={title}
                primaryTogglesNestedList={true}
                nestedItems={listRows}
                disabled={disabled}
                style={{color:(disabled)?"#AAAAAA":"#000000"}} />
            );
            rows.push(listItem);
          }else {
            rows.push(
              <ListItem
                value={ item.id }
                primaryText={title}
                disabled={disabled}
                style={{color:(disabled)?"#AAAAAA":"#000000"}} />
            );
          }
          menuIndex++;
        }
      }
    }
    return {
      mainMenu:rows,
      subMenu: subRows
    };
  },

  componentDidMount() {
    DynConfigPanelStore.addChangeListener(this.onPanelChange);
    ContentStore.addChangeListener(this.onContentChange);
  },

  componentWillUnmount: function() {
    DynConfigPanelStore.removeChangeListener(this.onPanelChange);
    ContentStore.removeChangeListener(this.onContentChange);
  },


  onContentChange(){

    let content=ContentStore.getPageContent();
    var newState = {};
    var {
      changedValues
    } = this.state;
    let item= changedValues && changedValues[1]? changedValues[1][0]: null;
    if(checkExist(content, 'changedValues.BiPages')){
      newState.disableAddBi = false;
      newState.biPages = content.changedValues.BiPages;
    }
    if(checkExist(content, 'changedValues.selectPage.id') || checkExist(content, 'values.selectPage.id')){
      let selectPageId = checkExist(content, 'changedValues.selectPage.id')?content.changedValues.selectPage.id:content.values.selectPage.id;
      let items =this.state.changedValues;// this.props.items;
      // let selectedIndex = this.getSelectedIndex(items, selectPageId);
      let selectedIndex = selectPageId;
      if(selectedIndex != null){
        newState.selectedIndex = selectedIndex;
      }

      if(checkExist(item, 'id') && checkExist(content, 'changedValues.'+item.id)){
        this.setState({
          disableAddBi: false,
          biPages: content.changedValues[item.id]
        });
        this.forceUpdate();
      }
    }
    if(!isEmpty(newState)) this.setState(newState);
  },

  getSelectedIndex(menuList, selectPageId){
    var menuIndex = 1;
    for(var z in menuList){
      var items = menuList[z];
      for(var i in items){
        var _item = items[i];
        if(selectPageId == _item.id){
          return Number(i)+menuIndex;
        }
        var type = _item.type;
        if(type){
          switch(type){
            case 'N':
            case 'D':
            for(var j in _item.items){
              if(this.getSelectedIndex(_item.items, selectPageId)){
                return Number(i)+menuIndex;
              }
            }
            break;
            case 'L':
            for(var j in _item.items[0]){
              if(selectPageId == _item.items[0][j].id){
                return (Number(i) + menuIndex) + "," + j;
              }
            }
            break;
            default:
          }
        }
      }
      menuIndex = menuIndex + items.length;
    }

    return null;
  },

  onPanelChange(){
    let open = !DynConfigPanelStore.getPanelState();
    if(this.state.isPanelClosed!=open){
      this.state.isPanelClosed=open;
      this.forceUpdate();
    }
  },

  componentWillReceiveProps: function(nextProps)
  {
    var newState = {changedValues:nextProps.items};
    if(nextProps.parents && !nextProps.selected){
      newState.selectedIndex = -1;
    }
    this.setState(newState);
  },

  onMenuChange() {
    if(this.props.onMenuChange){
      this.props.onMenuChange();
    }
  },

  handleUpdateSelectedIndex(e, value) {
    if(!this.state.isPanelClosed&&this.state.isCsClicked)
    return null;
    //indicate chaneg to cs menu
    this.state.isCsClicked=false;
    //update selected index before call server
    // let callback = function(){
      var allowChange = true;
      if(this.props.onSelected){
        allowChange = this.props.onSelected();
      }
      if (!allowChange) {
        // this.setState({selectedIndex: index});
        // if(this.props.changePage){
        //   this.props.changePage();
        // }
        return;
      }
    // }.bind(this);

    DynMenuActions.changePage(this.props.module, value);

    // let items =this.state.changedValues;// this.props.items;
    // let item = undefined;
    // var target = undefined;
    // var secondTarget = undefined;
    // if(typeof index == "number"){
    //   target = cloneObject(index-1);
    // }else{
    //   var indexArr = index.split(",");
    //   target = cloneObject(Number(indexArr[0])-1);
    //   secondTarget = cloneObject(Number(indexArr[1]));
    // }
    // for(let i in items){
    //   if(target < items[i].length ){
    //     let _item = items[i][target];
    //     let type =null
    //     if( checkExist(_item,"type"))
    //     type = _item.type;
    //     if(!type){
    //       item=_item;
    //     }else{
    //       switch(type){
    //         case 'S':
    //         item=_item;
    //         break;
    //         case 'L':
    //         item=_item.items[0][secondTarget];
    //         break;
    //         default:
    //       }
    //     }
    //     break;
    //   }else{
    //     target=target-items[i].length;
    //   }
    // }
    //
    //
    // if(!isEmpty(item)){
    //   DynMenuActions.changePage(this.props.module, item.id);
    // }
  },

  propTypes:{
    module: React.PropTypes.string.isRequired,
    items: React.PropTypes.array.isRequired,
    onMenuChange: React.PropTypes.func,
    onChangePage: React.PropTypes.func,
    style: React.PropTypes.object,
    parents: React.PropTypes.array

  },

  getDefaultProps() {
    return {
      level: ''
    }
  },

  showMenu(){
    this.setState({show:true});
  },

  hideMenu(){
    this.setState({show:false});
  },

  render() {
    var {
      style,
      level,
      items,
      title,
      parents,
      module,
      ...others,
    } = this.props;
    let styles = this.getStyles();
    let menu = this.genMenu(this.props.level?this.props.level:'',this.state.changedValues);
    if(this.props.parents){
      return (
        <SelectableList
          ref={"sl"+this.props.level?this.props.level:''}
          selectedItemStyle={styles.selectedItemStyle }
          valueLink={{value: this.state.selectedIndex, requestChange: this.handleUpdateSelectedIndex}}
          style={ this.mergeAndPrefix(styles.subMenuListStyle,{display: (this.state.show)?'block':'none'}) }>
          <div style={styles.menuHeaderStyle}>
            <span><IconButton iconClassName="material-icons" onClick={this.hideMenu}>arrow_back</IconButton></span>
            <span style={{fontSize: '20px'}}>{title}</span>
          </div>
          <Divider/>
          {menu.mainMenu}
          {menu.subMenu}
        </SelectableList>
      );
    }else{
      return (
        <div style={this.mergeAndPrefix(styles.body, style)} {...others}>
          <SelectableList
            valueLink={{value: this.state.selectedIndex, requestChange: this.handleUpdateSelectedIndex}}
            selectedItemStyle={styles.selectedItemStyle}
            style={ styles.selectionListStyle }>
            {menu.mainMenu}
            {menu.subMenu}
          </SelectableList>
          <div style={styles.border}>
          </div>
        </div>
      );
    }

  }
});

module.exports = DynMenu;
