/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let Checkbox = mui.Checkbox;
let Colors = mui.Styles.Colors;
let ContentStore = require('../../stores/ContentStore.js');
let ReadOnlyField = require('../CustomView/ReadOnlyField.jsx');
let DialogActions = require('../../actions/DialogActions.js');
let CompntBase = require('../../mixins/EABComponentBase.js');
let ConfigConstants = require('../../constants/ConfigConstants.js')

import appTheme from '../../theme/appBaseTheme.js';

let DynViewOnlyColumn = React.createClass({

  mixins: [React.LinkedStateMixin, StylePropable, CompntBase],

  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme,
    };
  },

  contextTypes: {
    muiTheme: React.PropTypes.object
  },

  propTypes:{
    id: React.PropTypes.string.isRequired,
    // values: React.PropTypes.object,
    // template: React.PropTypes.object.isRequired,
    show: React.PropTypes.bool,
    title: React.PropTypes.string,
    style: React.PropTypes.object,
  },

  getStyles() {
    let theme = this.state.muiTheme;

    return {
      floatingLabel : {
        lineHeight: '22px',
        top: '38px',
        bottom: 'none',
        fontSize: '16px',
        opacity: 1,
        color: theme.textField.fixedLabelColor,
        transform: 'perspective(1px) scale(0.875) translate3d(0px, -28px, 0)',
        transformOrigin: 'left top',
        position: 'absolute',
        zIndex: 100
      },
      checkBoxLabelStyle: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        width: 'calc(100% - 40px)',
        fontSize: '16px',
      },
      titleStyle : {
        display: 'block',
        fontSize: '20px',
        lineHeight: '64px'
      },
      groupTitle:{
        fontSize: '16px',
      },
      dateBy: {
        color: theme.textField.fixedLabelColor
      },
      subLineStyle: {
        backgroundColor: appTheme.palette.borderColor,
        height:1,
        margin:'20px 0'
      }
    }
  },

  getInitialState() {
    var content =  ContentStore.getPageContent();
    return {
      muiTheme: this.context.muiTheme?this.context.muiTheme:appTheme.getTheme(),
      template: content.template,
      values: content.values
    };
  },

  handleCbValues:function(parentID, values){
    var OrgValues = values?values:this.state.values;
    var value = OrgValues[parentID];
    if (value){
      return value.split(",");
    }
    return [];
  },

  runningId: 0,

  _genItem(its, item, values) {
    var value;
    if (!values) {
      values = this.state.values
    }
    value = item.id?values[item.id]:values;

    var styles = this.getStyles();
    if (!item.type) {
    }
    else if (item.type.toUpperCase() == 'LIST') {
      var subItems = [];
      var titleNode = null;
      if (item.title) {
        titleNode = (<label key="label" className="Title" style={ styles.titleStyle }>{item.title}</label>);
      }

      for (let si in item.items) {
        var subItem = item.items[si];
        subItem.parentType = item.type;
        this._genItem(subItems, subItem, value)
      }

      its.push(<div key={"div_"+ item.id} className="ListField">
        {titleNode}
        {subItems}
      </div>)
    }
    else if (item.type.toUpperCase() == 'HBOX') {
      if (item.items.length) {
        var titleNode = null;
        if (item.title) {
          if (item.parentType && item.parentType.toUpperCase() == 'LIST') {
            titleNode = (<label key="label" className="Label" style={styles.floatingLabel}>{item.title}</label>)
          } else {
            titleNode = (<label key="label" className="Title" style={styles.titleStyle}>{item.title}</label>)
          }
        }
        var subItems = [];
        for (let si in item.items) {
          var _item = item.items[si];
          this._genItem(subItems, _item, value);
        }

        // either column2, column3, column4
        var additionalPresentClass = item.presentation?" "+item.presentation:"";

        its.push(
          <div key={"hbox"+(this.runningId++)} className={"hBoxContainer"+additionalPresentClass}>
            {titleNode}
            {subItems}
          </div>)
      }
    }
    else if (item.type.toUpperCase() == 'CHECKBOXDIALOG') {
      // generate sub field title
      var valueStr = "";

      // generate sub field for each value
      var subV = this._getValue(item, values[item.id]);
      if (subV == undefined) subV = '-';
      var cbValues = this.handleCbValues(item.id, values);
      var ttp = ""
      for (let cbi in cbValues) {
        var cbl = this._getValue(item, cbValues[cbi]);
        valueStr += (valueStr?", ":"") + cbl;
      }

      its.push(
        <ReadOnlyField
          key = {item.id }
          id = { item.id }
          className="DetailsItem"
          floatingLabelText = {item.title}
          tooltip = {tooltip}
          defaultValue = { valueStr } />
      )
    }
    else if (item.type.toUpperCase() == 'COMPLEXLIST') {
      var subItems = [];
      var titleNode = null;
      if (item.title) {
        titleNode = (<label key={item.id} className="Title" style={ styles.titleStyle }>{item.title}</label>);
      }
      if (!value || !(value instanceof Array)) {
        value = values[item.id] = [];
      }

      if (value instanceof Array && value.length > 0) {
        // generate sub field title
        var showField = item.presentation?item.presentation.toUpperCase():null

        var sortedItems = item.items.sort(function(a,b) {
          var aa = a.detailSeq?a.detailSeq:0;
          var bb = b.detailSeq?b.detailSeq:0;
          return (aa < bb) ? -1 : (aa > bb) ? 1 : 0;
        });

        var subTitle = "";
        for (let si in sortedItems) {
          var sItem = sortedItems[si];
          if (!showField || showField.indexOf(sItem.id.toUpperCase()) >= 0) {
            if (sItem.id != 'default') {
              subTitle += (subTitle?", ":"") + sItem.title;
            }
          }
        }

        // generate sub field for each value
        for (let si in value) {
          var subValues = value[si];
          var valueStr = "";
          var tooltip = null;
          // concate the sub value
          for (let sii in sortedItems) {
            var sItem = sortedItems[sii];
            var subV = this._getValue(sItem, subValues[sItem.id]);
            if (subV == undefined) subV = '-';
            if (!showField || showField.indexOf(sItem.id.toUpperCase()) >= 0) {
                if (sItem.id == 'default') {
                  valueStr += subV;
                } else {
                  valueStr += (valueStr?", ":"") + subV;
                }
            } else {
              // sp handle for checkbox
              if (sItem.type.toUpperCase() == 'CHECKBOXGROUP') {
                var cbValues = this.handleCbValues(sItem.id, subValues);
                var ttp = ""
                for (let cbi in cbValues) {
                  var cbl = this._getValue(sItem, cbValues[cbi]);
                  ttp += (ttp?", ":"") + cbl;
                }

                if (ttp) {
                  tooltip = sItem.title + ": " + ttp;
                }
              }
            }
          }

          subItems.push(
            <ReadOnlyField
              key = {item.id+si}
              className="DetailsItem"
              floatingLabelText = {subTitle}
              tooltip = {tooltip}
              defaultValue = { valueStr } />
          )
        }
      } else {
        var errorMsg = item.placeholder;
      }
      var content =  ContentStore.getPageContent();
      var cValue = content.changedValues?content.changedValues[item.id]:null;

      var valueLen = value?value.length:0;
      var cValueLen = cValue?cValue.length:0;
      var dd = cValueLen - valueLen;
      if (dd < 0) dd = 0;
      subItems.push(
        <div style={ {display:'block',
          height: dd * 80,
        }}></div>);

      var addButton = <div style={ {
          display:'block',
          height: '48px',
          fontSize: '12px'
        }}>{errorMsg?errorMsg:""}</div>

      its.push(<div key={"sps_"+item.id} className="DetailsItem" style={{paddingRight: 0}}>
        <div style={ styles.subLineStyle }> </div>
      </div>)
      its.push(<div className="ListField">
        {titleNode}
        {subItems}
        {addButton}
      </div>)
      its.push(<div key={"spe_"+item.id} className="DetailsItem" style={{paddingRight: 0}}>
        <div style={ styles.subLineStyle }></div>
      </div>)
    }
    else if (item.type.toUpperCase() == 'CHECKBOX') {

      its.push(<div className={"DetailsItem"}>
          <Checkbox
            key = { item.id }
            label = {item.title}
            className={"CheckBox"}
            style={{width:null, display:'inline-block'}}
            labelStyle = {styles.checkBoxLabelStyle}
            defaultChecked = {value == "Y"}
            labelDisabledColor = {this.state.muiTheme.checkbox.labelColor}
            boxDisabledColor = {this.state.muiTheme.checkbox.boxColor}
            checkDisabledColor = {this.state.muiTheme.checkbox.checkedColor}
            disabled = {true}
          />
        </div>)
    }
    else if (item.type.toUpperCase() == 'CHECKBOXGROUP'){
      var cbGroup = [];

      var itemValues = this.handleCbValues(item.id);
      var label = null;
      if (item.title) {
        label = (
          <label key={"l_"+item.id} className="Title" style={ styles.titleStyle }>{item.title}</label>
        );
      }
      for (let i in item.options){
        var option = item.options[i];
        cbGroup.push(<Checkbox
            key={ option.value }
            className={"CheckBox"}
            style={{width:null, display:'inline-block'}}
            labelStyle = {styles.checkBoxLabelStyle}
            label = {option.title}
            defaultChecked={arrayContains(itemValues,option.value)}
            labelDisabledColor = {this.state.muiTheme.checkbox.labelColor}
            boxDisabledColor = {this.state.muiTheme.checkbox.boxColor}
            checkDisabledColor = {this.state.muiTheme.checkbox.checkedColor}
            disabled = {true}
            />)
      }
      its.push(
        <div
          className={"CheckboxGroup"+ (item.presentation?" "+item.presentation:"")}>
          {label}{cbGroup}
        </div>
      );
    }else if(item.type.toUpperCase() == 'MULTTEXT' ){
      var txtValue = (value[this.state.values.lang] != null) ? value[this.state.values.lang] : "";
      its.push(
        <div key={"d_"+ item.id } className="DetailsItem"> <ReadOnlyField
          key = { item.id }
          id = { item.id }
          className = "ColumnField"
          floatingLabelText = {item.title}
          subType= {item.subType}
          defaultValue = { txtValue } /></div>
      );
    }else if (item.type.toUpperCase() != 'SELECTALLBUTTON' ){
      if (typeof(value) == 'string') {
      } else {
        value = undefined;
      }

      if (value) {
        value = this._getValue(item, value);
      }

      its.push(
        <div key={"d_"+ item.id } className="DetailsItem"> <ReadOnlyField
          key = { item.id }
          id = { item.id }
          className = "ColumnField"
          floatingLabelText = {item.title}
          subType= {item.subType}
          defaultValue = { value ? value : item.value ? item.value : "-"} /></div>
      );
    }
  },

  _getValue(item, value) {
    if (item.options && value) {
      for (let idx in item.options) {
        var op = item.options[idx];
        if (op.value.toUpperCase() == value.toUpperCase()) {
          return op.title;
        }
      }
    } else if (item.type == 'datepicker' && value instanceof Date) {
      return value.format(ConfigConstants.DateFormat);
    } else if (item.type.toUpperCase() == 'CHECKBOX') {
      if (item.id == "default") {
        if (value == "Y") {
          return " (Default)";
        } else {
          return "";
        }
      }
    }
    return value;
  },

  render: function() {
    var self = this;
    if (!this.props.show) return <div></div>;

    var styles = this.getStyles();

    let {
      title
    } = this.props;

    // update the state anyway
    var content = ContentStore.getPageContent()
    this.state.template = content.template;
    this.state.values = content.values;

    var {
      template,
      values
    } = this.state;

    var its = [];

    //console.debug('render view only column!');

    //   fields += <div>;
    var subHeader = null;
    if (!!title && template.type == "edit") {
      subHeader = (
        <div key="curtitle" className="colTitle">
          <p>{title}</p>
        </div>
      );
    } else {
      subHeader = (<div key="curtitle"></div>)
    }

    if (template.items.length > 0) {
      var sortedItems = template.items.sort(function(a,b) {
        var aa = a.detailSeq?a.detailSeq:0;
        var bb = b.detailSeq?b.detailSeq:0;
        return (aa < bb) ? -1 : (aa > bb) ? 1 : 0;
      });

      for (let i in sortedItems) {
        var item = sortedItems[i];
        if (item.detailSeq) {
          this._genItem(its, item);
        }
      }
    }


    return (
      <div key={this.props.id} className="DetailsColumn" style={ this.props.style }>
        <div key={"fields"} style={{ padding:'0 0 24px' }}>
          {its}
          <div className="DateBy" style={styles.dateBy} dangerouslySetInnerHTML={ {__html: genRecordDateBy(this.props.dateBy, values)} }></div>
        </div>
      </div>
    );
  }
});

module.exports = DynViewOnlyColumn;
