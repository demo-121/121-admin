let React = require('react');
let mui, {Dialog, FlatButton, Toolbar, ToolbarTitle} = require('material-ui');

let EABTextField = require('../CustomView/TextField.jsx')
let EABPickerField = require('../CustomView/PickerField.jsx')
let EABDatePickerField = require('../CustomView/DatePickerField.jsx')
let EABTimePickerField = require('../CustomView/TimePickerField.jsx')
let EABCheckbox = require('../CustomView/CheckBox.jsx');
let EABCheckboxGroup = require('../CustomView/CheckBoxGroup.jsx');
let EABRadioBoxGroup = require('../CustomView/RadioBoxGroup.jsx');
let EABMultipleTextField = require('../CustomView/MultipleTextField.jsx');
let EABTable = require('../CustomView/Table.jsx');
let EABJSBoxField = require('../CustomView/JSBoxField.jsx');
let EABComplexList = require('../CustomView/ComplexList.jsx');
let EABDetailsList = require('../CustomView/DetailsList.jsx');
let JavascriptField = require('../CustomView/JavascriptField.jsx');
let EABViewOnlyField = require('../CustomView/ReadOnlyField.jsx');
let DialogActions = require('../../actions/DialogActions.js');
let DynActions = require('../../actions/DynActions.js');

let AppStore = require('../../stores/AppStore.js');
let DialogStore = require('../../stores/DialogStore.js');
let HomeStore = require('../../stores/HomeStore.js');
let MasterTableStore = require('../../stores/MasterTableStore.js');
let ContentStore = require('../../stores/ContentStore.js');

var beautify = require('js-beautify').js_beautify;

import appTheme from '../../theme/appBaseTheme.js';

let DynDialog = React.createClass({

  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme,
    };
  },

  contextTypes: {
    muiTheme: React.PropTypes.object
  },

  getInitialState() {
    var dialog = DialogStore.getDialog();
    var values = {};
    if (dialog) {
      values = dialog.values ? cloneObject(dialog.values) : dialog.inhertValue ? this.state.values : {};
    }
    this.count = 0;
    return {
      dialog: dialog,
      open: dialog ? true : false,
      values: values,
      muiTheme: this.context.muiTheme ? this.context.muiTheme : appTheme.getTheme()
    };
  },

  componentDidMount() {
    console.trace('mount DynDialog:', ++this.count);
    DialogStore.addChangeListener(this.onChange);
  },

  componentWillUnmount() {
    console.trace('unmount DynDialog:', --this.count);
    DialogStore.removeChangeListener(this.onChange);
  },

  onChange() {
    var dialog = DialogStore.getDialog();
    if (dialog) {
      var values = dialog.values? cloneObject(dialog.values): (dialog.inhertValue ? this.state.values : {})
      this.setState({
        dialog: dialog,
        open: DialogStore.getOpen(),
        rootValues: dialog.rootValues|| values,
        values: values
      });
    }
  },

  handleClose(e) {
    var event = e || window.event;
    if (event) {
      if (event.stopPropagation) {
        event.stopPropagation();
      } else {
        event.cancelBubble = true;
      }
      if (event.preventDefault) {
        event.preventDefault();
      }
    }
    this.setState({open: false})
  },

  getStyles() {
    let theme = this.state.muiTheme;

    return {
      floatingLabel : {
        lineHeight: '22px',
        top: '38px',
        bottom: 'none',
        fontSize: '16px',
        opacity: 1,
        color: theme.textField.fixedLabelColor,
        transform: 'perspective(1px) scale(0.875) translate3d(0px, -28px, 0)',
        transformOrigin: 'left top',
        position: 'absolute',
        zIndex: 100
      },
      floatingTitle: {
        whiteSpace: 'nowrap'
      },
      checkBoxLabelStyle: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        width: 'calc(100% - 40px)',
        fontSize: '16px',
      },
      titleStyle : {
        display: 'block',
        fontSize: '20px',
        lineHeight: '64px'
      },
      disabledUnderline: {
        borderBottom: '1px solid',
        borderColor: appTheme.palette.borderColor
      },
      dateBy: {
        color: theme.textField.fixedLabelColor
      },
      subLineStyle: {
        backgroundColor: appTheme.palette.borderColor,
        height:1,
        margin:'20px 0'
      },
      dialogTitleStyle: {
        padding: '20px 24px',
        lineHeight: '24px',
        fontSize: 20,
        borderBottom: '1px solid',
        borderColor: appTheme.palette.borderColor
      },
      actionsContainerStyle: {
        borderTop: '1px solid',
        borderColor: appTheme.palette.borderColor
      }
    }
  },

  resetValueItems: null,

  _genItem(items, item, orgValues, values, handleChange) {

    var self = this;
    var styles = self.getStyles();
    var rootValues = this.state.rootValues || this.state.changedValues;

    if (!values) {
      values = this.state.changedValues;
    }

    cValue = item.id ? values[item.id] : values;

    // if it is a new change get default value if changed value is empty
    if (cValue === undefined) {
      cValue = values[item.id] = item.value || ""
    }

    if (!showCondition(item, orgValues, values)) {
      return;
    }

    var requestChange = function(val, id) {
      values[id] = val;
      self.setState({values: values});
    }

    var type = item.type || ""

    // Label
    if (type.toUpperCase() == "LABEL") {
      items.push(
        <div style= {{ padding: '24px', height: '8px'} }>
          <span style={{paddingLeft: item.indent + "em"}} key={item.id} dangerouslySetInnerHTML={{__html: item.title}} />
        </div>
      );
    }
    // Label
    if (type.toUpperCase() == "PARA") {
      items.push(
        <p style={item.style || {}} key={item.id} dangerouslySetInnerHTML={{__html: item.title}} />
      );
    }
        // TextField
    else if (type.toUpperCase() == "TEXT") {
      items.push(
        <EABTextField
          key = {"ctn"+item.id}
          template = {item}
          changedValues = {values}
          fullWidth= {true}
          requestChange = { requestChange }
          handleChange = { handleChange }
          rootValues = { rootValues }
          />
      );
    }
    else if (type.toUpperCase() == 'MULTTEXT') {
      items.push(
        <EABMultipleTextField
          key = {"ctn_"+ item.id}
          template = { item }
          changedValues = { values }
          requestChange = { requestChange }
          handleChange = { handleChange }
          rootValues = { rootValues }
        />)
    }
    // DatePicker
    else if (type.toUpperCase() == "DATEPICKER") {
      items.push(
        <EABDatePickerField
          key = {"ctn_"+item.id}
          template = {item}
          changedValues = { values }
          requestChange = { requestChange }
          handleChange = { handleChange }
          rootValues = { rootValues }
          />
      );
    }
    // TimePicker
    else if (type.toUpperCase() == "TIMEPICKER") {
      items.push(
        <EABTimePickerField
          key = {"ctn_"+item.id}
          template = {item}
          changedValues = { values }
          requestChange = { requestChange }
          handleChange = { handleChange }
          rootValues = { rootValues }
          />
      );
    }
    // Checkbox group
    else if ("CHECKBOXGROUP" == type.toUpperCase() && item.options) {
      items.push(
        <EABCheckboxGroup
          key = {"ctn_"+item.id}
          template = {item}
          resetValueItems = { this.resetValueItems }
          changedValues = { values }
          requestChange = { requestChange }
          handleChange = { handleChange }
          rootValues = { rootValues }
          />
      );
    }
    // Checkbox
    else if ("CHECKBOX" == type.toUpperCase()){
      items.push(
        <EABCheckbox
          key={ "ctn_"+item.id }
          template = {item}
          resetValueItems = { this.resetValueItems }
          changedValues = { values }
          requestChange = { requestChange }
          handleChange = { handleChange }
          rootValues = { rootValues }
        />)
    }
    // RadioButton
    else if (type.toUpperCase() == "RADIOBUTTON") {
      items.push(
        <EABRadioBoxGroup
          key = {"ctn_"+item.id}
          template = {item}
          resetValueItems = { this.resetValueItems }
          changedValues = { values }
          requestChange = { requestChange }
          handleChange = { handleChange }
          rootValues = { rootValues }
          />
      );
    }
    // picker
    else if ('PICKER' == type.toUpperCase()) {
      items.push(
        <EABPickerField
          key = {"ctn_"+item.id}
          template = { item }
          changedValues = { values }
          resetValueItems = { this.resetValueItems }
          requestChange = { requestChange }
          handleChange = { handleChange }
          rootValues = { rootValues }
          />
      );
    }
    else if (type.toUpperCase() == 'HBOX' || "COLUMN" == type.toUpperCase() || "COLUMNGROUP" == type.toUpperCase() ) {
      if (item.items.length) {
        var titleNode = null;
        var title = item.title;
        if ("COLUMN" != type.toUpperCase()) {
          if (title) {
            titleNode = (<label key="label" className="Title" style={styles.titleStyle}>{title}</label>)
          }
        }

        var subItems = [];
        for (let si in item.items) {
          var _item = item.items[si];
          this._genItem(subItems, _item, orgValues, cValue, handleChange);
        }

        // either column2, column3, column4
        var additionalPresentClass = item.presentation?" "+item.presentation:"";
        if ("COLUMNGROUP" == type.toUpperCase() ) {
          additionalPresentClass = "column1"
        }
        if ("COLUMN" == type.toUpperCase() ) {
          additionalPresentClass = "column2"
        }
        items.push(
          <div key= {"hbox"+(this.runningId++)} className={"hBoxContainer "+additionalPresentClass}>
            {titleNode}
            {subItems}
          </div>)
      }
    }
    // table
    else if ("TABLE" == type.toUpperCase()){
      item.title = null;
      items.push(<EABTable
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          inDialog = { true }
          resetValueItems = { this.resetValueItems }
          changedValues = { values }
          rootValues = { rootValues }
          handleChange = {function(id, value) {
            if (handleChange && typeof handleChange == 'function') {
              handleChange(id, value);
            }
            self.forceUpdate();
          }}
          />
      );
    }
    else if (type.toUpperCase() == 'COMPLEXLIST') {
      items.push(
        <EABComplexList
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          inDialog = { true }
          resetValueItems = { this.resetValueItems }
          changedValues = { values }
          rootValues = { rootValues }
          handleChange = {
            function(id, value) {
              if (handleChange && typeof handleChange == 'function') {
                handleChange(id, value);
              }
              self.forceUpdate();
            }
          }
          />
      );
    }
    else if (type.toUpperCase() == 'DETAILSLIST') {
      this.fullScreen = true;
      items.push(
        <EABDetailsList
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          collapsed = { true }
          inDialog = { true }
          resetValueItems = { this.resetValueItems }
          changedValues = { values }
          rootValues = { rootValues }
          handleChange = {
            function(id, value) {
              if (handleChange && typeof handleChange == 'function') {
                handleChange(id, value);
              }
              self.forceUpdate();
            }
          }
          />
      );
    }
    // js editor
    else if ("JSFIELD" == type.toUpperCase()){
      this.fullScreen = true;
      var cValue = values[item.id];
      var disabled = item.disabled || disableTrigger(item, self.state.dialog, orgValues, values, rootValues);

      var cVal = cValue
      if (cValue && typeof cValue == 'object') {
        cVal = JSON.stringify(cValue);
      }
      if (cVal) {
        cVal = beautify(cVal, { indent_size: 4});
      }
      var title = item.title;
      items.push(
        <div className={"DetailsItem"}>
          {title?<label key="label" className="Title" style={styles.titleStyle}>{title}</label>:null}
          <JavascriptField
            id = {item.id}
            key = {item.id}
            ref = {item.id}
            className = { "ColumnField" }
            disabled = {disabled}
            height = "300px"
            handleChange = { handleChange }
            style = { {
              display: 'block'
            } }
            onBlur = {
              function(value, errorMsg) {
                self.state.editing = false;
                if (!errorMsg) {
                  this.values[this.id] = JSON.parse(value);
                } else {
                  this.values[this.id] = value;
                }
                self.forceUpdate();
              }.bind({id: item.id, item: item, values: values})
            }
            onFocus = {
              function() {
                self.setState({
                  editing: true
                });
              }
            }
            mode = "json"
            defaultValue = { cVal }
            />
        </div>
      );
    }
    // PDF
    else if ("PDF" == type.toUpperCase() || "SELECTLIST" == type.toUpperCase()){

    } else {
      items.push(
        <EABViewOnlyField
          id = {item.id}
          ref = {item.id}
          key = {item.id}
          template = { item }
          changedValues = { values }/>)
    }
  },

  render() {
    var self = this;
    var values = this.state.values;
    var dialog = this.state.dialog;
    var orgValues = dialog ? dialog.values : null;

    if (!dialog || !this.state.open) {
      return null;
    }

    this.runningId = 0;

    this.resetValueItems = {};

    console.debug('[DynDialog] - [render]', this.state);
    var styles = this.getStyles();
    this.fullScreen = false;

    // Start of Items
    var items = [];
    if (dialog.items && dialog.items.length > 0) {

      // Sort items
      var sortedItems = dialog.items.sort(function(a,b) {
        var aa = a.detailSeq ? a.detailSeq : 0;
        var bb = b.detailSeq ? b.detailSeq : 0;
        return (aa < bb) ? - 1 : (aa > bb) ? 1 : 0;
      });

      for (let i = 0; i < sortedItems.length; i++) {
        var item = sortedItems[i];
        this._genItem(items, item, orgValues, values, dialog.handleChange);
      }
    }
    // End of Items

    // Start of Buttons
    const actions = [];

    if (!dialog.actions) {
      if (dialog.negative) {
        actions.push(this._genCancelButton(dialog.negative));
      }
      if (dialog.positive) {
        actions.push(this._genActionButton(self, dialog, dialog.positive, values));
      }
    } else {
      for (let ai in dialog.actions) {
        var action = dialog.actions[ai];
        if (!action.hide) {
          if (action.type == 'cancel') {
            actions.push(this._genCancelButton(action));
          } else {
            actions.push(this._genActionButton(self, dialog, action, values));
          }
        }
      }
    }
    // End of Buttons

    const customContentStyle = {
      width: dialog.width ? dialog.width + '%' : '60%',
      maxWidth: null,
    };

    var dtitle = getLocalText('en', dialog.title);

    if (this.fullScreen) {
      return <div key={"floatingPage"} className={ "FloatingPage" }
        style={{
          display: this.state.open?'flex':'none'
        }}>
        <Toolbar key={"toolbar"}
          className={ "Toolbar" }>
          <ToolbarTitle text={<div style={{
                lineHeight:'56px',
                display:'inline-block', verticalAlign: 'top',
                paddingLeft: 24,
                fontSize: 20,
                color: appTheme.palette.alternateTextColor,
                width: 'calc(100% - 64px)',
                whiteSpace: 'nowrap',
                textOverflow: 'ellipsis',
                overflow: 'hidden'
              }}>{dtitle}</div>
            } style={{
              width: '100%'
            }}/>
        </Toolbar>
        <div key={'content'}
          className={ "Content" }
          style={{
            paddingTop: appTheme.spacing.desktopGutter,
            paddingBottom: appTheme.spacing.desktopGutter,
        //    paddingLeft: appTheme.spacing.desktopGutter,
            paddingRight: 0, overflowY:'auto'}}>
          {items}
        </div>
        <div style={{height:'52px', textAlign:'right',padding: '8px', borderTop:"1px solid " + appTheme.palette.borderColor}}>
          {actions}
        </div>
      </div>
    } else {
      return (
        <Dialog
          title={dtitle}
          className="Dialog"
          actions={actions}
          modal={false}
          actionsContainerStyle={styles.actionsContainerStyle}
          titleStyle={styles.dialogTitleStyle}
          bodyStyle={ {padding: '24px 20px',
            maxHeight: dtitle?"calc(100vh - 265px)":"calc(100vh - 200px)",
            overflowY: 'auto'} }
          contentStyle={customContentStyle}
          open={this.state.open} >
          {dialog.message?<span>{dialog.message}</span>:null}
          {items}
        </Dialog>
      );
    }
  },

  _genCancelButton(actionItem) {
    return <FlatButton
      label={actionItem.title}
      primary={actionItem.primary}
      onTouchTap={this.handleClose} />
  },

  _genActionButton(self, dialog, actionItem, values) {
    var handlePositiveTap = null;

    if (actionItem.type == "dialogButton" && actionItem.dialog) {
      handlePositiveTap = function() {
        DialogActions.showDialog(actionItem.dialog);
      };
    } else if (actionItem.type == "customButton") {
      handlePositiveTap = function() {
        var result = true;
        if (actionItem.handleTap && typeof actionItem.handleTap == 'function') {
          result = actionItem.handleTap(values);
        }
        if (result || result == undefined) {
          self.handleClose();
        }
      };
    } else if (actionItem.type == "submitButton") {
      handlePositiveTap = function() {
        var content = ContentStore.getPageContent();
        var page = HomeStore.getCurrentPage();

        if (content.template && content.template.type == 'table') {
          var rows = [];

          if (self.state.dialog.inhertValue && self.state.values && self.state.values.rows) {
            rows = self.state.values.rows;
          } else {
            rows = MasterTableStore.getSelectedRows();
          }

          if (rows.length == 0 && content.values.id) {
            rows.push(content.values.id);
          }

          DialogActions.submitSelected(actionItem.id, values, rows, content, page);
        } else {
          DialogActions.submitEditValues(actionItem.id, values, content, page);
        }
        self.handleClose();
      };
    } else if (actionItem.type == "saveValueButton") {
      handlePositiveTap = function() {

        var content = ContentStore.getPageContent();
        var changedValues = content.changedValues?content.changedValues:cloneObject(content.values);

        // changedValues
        var keyList = Object.keys(values);
        for (let i in keyList){
          var key = keyList[i];
          changedValues[key] = values[key];
        }
        DynActions.changeValues(changedValues);
        self.handleClose();
      };
    } else {
      handlePositiveTap = function() {
        self.handleClose();
      }
    }

    // mandatory checking
    var errorMsg = false;
    if (dialog.items) {
      for (let it = 0; it < dialog.items.length; it++) {
        errorMsg = validate(dialog.items[it], values);
        if (errorMsg) {
          break;
        }
      }
    }

    return (
      <FlatButton
        label={ actionItem.title }
        style={ actionItem.float?{
          float: actionItem.float
        }: {}}
        primary={actionItem.primary}
        keyboardFocused={actionItem.keyboardFocused}
        disabled={!!errorMsg || actionItem.disabled || this.state.editing}
        onTouchTap={function(e) {
          var event = e || window.event;
          if (event) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else {
              event.cancelBubble = true;
            }
            if (event.preventDefault) {
              event.preventDefault();
            }
          }
          if (this.beforePositive && typeof this.beforePositive == 'function') {
            this.beforePositive();
          }
          this.handlePositiveTap();
          if (this.afterPositive && typeof this.afterPositive == 'function') {
            this.afterPositive();
          }
        }.bind({handlePositiveTap: handlePositiveTap, beforePositive: dialog.beforePositive, afterPositive: dialog.afterPositive}) } />
    );
  }

});

module.exports = DynDialog;
