let React = require("react");
let mui = require("material-ui");
let StylePropable = mui.Mixins.StylePropable;
let DynMasterTable = require("../DynamicUI/DynMasterTable.jsx");
let ContentStore = require('../../stores/ContentStore.js');
let AppBarActions = require('../../actions/AppBarActions.js');

let TaskRelease = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  propTypes: {

  },

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onChange);
  },

  onChange() {
    this.setState({
      content: ContentStore.getPageContent(),
    });
  },

  getInitialState() {
    return {
      content: ContentStore.getPageContent(),
    };
  },

  handleTapRow(e, row) {
  },

  handleRowSelection(rows) {
    if (rows instanceof Array && rows.length > 0 && rows != 'none') {
        return 1;
    } else {
        return 0;
    }
  },

  render() {
    var windowHeight = window.innerHeight;
    var content = this.state.content;
    return(
      <div className="PageContent">
        <DynMasterTable
          id = {'taskRelease'}
          list = {content.values.list}
          total = {content.values.total}
          template = {content.template}
          show = {true}
          selectable = {false}
          height = { (windowHeight - 170) + "px"}
          handleTapRow = {this.handleTapRow}
          handleRowSelection = {this.handleRowSelection} />
      </div>
    );
  }
});

module.exports = TaskRelease;
