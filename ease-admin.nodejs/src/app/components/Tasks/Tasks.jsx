let React = require("react");
let mui = require("material-ui");
let StylePropable = mui.Mixins.StylePropable;
let DynMasterTable = require("../DynamicUI/DynMasterTable.jsx");
let TaskStore = require('../../stores/TaskStore.js');
let TaskActions = require('../../actions/TaskActions.js');
let ContentStore = require('../../stores/ContentStore.js');

let Tasks = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  propTypes: {

  },

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);

    if (this.state.content == null || this.state.content.template == null || this.state.content.values == null)
      this._reload();
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onChange);
  },

  componentDidUpdate() {
    if (this.state.content == null || this.state.content.template == null || this.state.content.values == null)
      this._reload();
  },

  _reload() {
    if (!this.gettingContent) {
      this.gettingContent = true;
      TaskActions.getTaskList();
    }
  },

  gettingContent: false,
  onChange() {
    this.gettingContent = false;
    this.setState({
      content: ContentStore.getPageContent(),
    });
  },

  getInitialState() {
    return {
      content: ContentStore.getPageContent(),
    };
  },

  handleTapRow(e, row) {
    var content = this.state.content;
    TaskActions.pageRedirect(content.values.list[row].id, 'FN.TASKS');
  },

  handleRowSelection(rows) {
    var content = this.state.content;
    var statusList = [];

    if (rows == 'all'){
      for (let i = 0; i < content.values.list.length; i++) {
        var status = content.values.list[i].statusCode;

        if (status != undefined)
          statusList.push(status);
      }
    } else if (rows.length > 0) {
      for (let i = 0; i < rows.length; i++) {
        if (content.values.list[rows[i]] != undefined)
          statusList.push(content.values.list[rows[i]].statusCode);
      }
    }

    var action;
    var hasUnassigned = statusList.indexOf("U") != -1 ? true : false;
    var hasAssigned = statusList.indexOf("A") != -1 ? true : false;
    var hasSuspended = statusList.indexOf("D") != -1 ? true : false;
    var hasReleased = statusList.indexOf("R") != -1 ? true : false;
    var hasScheduled = statusList.indexOf("S") != -1 ? true : false;
    var hasPublishing = statusList.indexOf("L") != -1 ? true : false;

    if (hasPublishing)
        action = 1;
    else if (hasUnassigned && !hasAssigned && !hasSuspended && !hasReleased && !hasScheduled)
        action = 2;
    else if (hasAssigned && !hasUnassigned && !hasSuspended && !hasReleased && !hasScheduled)
        action = 3;
    else if (hasSuspended && !hasAssigned && !hasUnassigned && !hasReleased && !hasScheduled)
        action = 4;
    else if (statusList.length > 0 && hasAssigned && hasUnassigned && !hasSuspended && !hasReleased && !hasScheduled)
        action = 5;
    else
        action = 1;

    if (rows != 'none' && statusList.length > 0)
      return action - 1;
    else
      return 0;
  },

  render() {
    var windowHeight = window.innerHeight;
    var content = this.state.content;
    var type_list = TaskStore.getTypeList();
    var status_list = TaskStore.getStatusList();

    if (content == null)
      this.state.content = content = ContentStore.getPageContent();

    if(content == null || content.template == null){
      return <div></div>;
    } else {
      
      return(
        <div className="PageContent">
          <DynMasterTable
            id = {'tasks'}
            list = {content.values.list}
            total = {content.values.total}
            template = {content.template}
            show = {true}
            multiSelectable = {true}
            height = { (windowHeight - 170) + "px"}
            handleTapRow = {this.handleTapRow}
            handleRowSelection = {this.handleRowSelection} />
        </div>
      );
    }
  }
});

module.exports = Tasks;
