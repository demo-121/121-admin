/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let brace = require('brace');

let CurrentColumn = require('../../DynamicUI/DynViewOnlyColumn.jsx');
let ContentStore = require('../../../stores/ContentStore.js');
let ChangedColumn = require('../../DynamicUI/DynEditableColumn.jsx');

let CompntBase = require('../../../mixins/EABComponentBase.js')
import appTheme from '../../../theme/appBaseTheme.js';

let UserRoleDetails = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable, CompntBase],

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onChange);
  },

  gettingContent : false,

  shouldComponentUpdate: function(nextProps, nextState) {
    if (!nextState.template) {
      if (!this.gettingContent) {
        this.gettingContent = true;
        DynActions.reflesh();
      }
      return false;
    } else {
      var id = nextState.values ? nextState.values.id : "";
      var oid = this.state.values ? this.state.values.id : "";
      if (id != oid) {
        return true;
      }

      if (this.refs.changed) {
        var showCondItems = this.refs.changed.showCondItems;
        var changedId = ContentStore.getChangedFieldId();

        if (showCondItems && changedId && showCondItems[changedId]) {
          return true;
        }
      }
    }
    return checkChanges(this.state.template, nextState.template);
  },

  onChange() {
    var content = ContentStore.getPageContent();
    this.gettingContent = true;
    if (content && !content.valuesBackup) {
      content.valuesBackup = cloneObject(content && content.changedValues?content.changedValues:{});
    }
    this.setState({
      template: content?content.template:null,
      values: content?(content.values?content.values:null):null,
      changedValues: content?(content.changedValues?content.changedValues:{}):null
    });
  },

  getInitialState() {
    var content = ContentStore.getPageContent();
    if (content) {
      content.valuesBackup = cloneObject(content && content.changedValues?content.changedValues:{});
    }

    return {
      template: content?content.template:null,
      values: content?(content.values?content.values:null):null,
      changedValues: content?(content.changedValues?content.changedValues:{}):null
    };
  },

  resetWidth4Scrollbar() {
    var self = this;
    setTimeout(function() {
      var st = self.refs["subtitle"];
      var ci = self.refs["contentInner"];
      if (st && ci) {
        st.style.paddingRight =  (st.clientWidth - ci.clientWidth) + "px"
      }
    }, 500)
  },

  render: function() {
    var self = this;

    var {
      template,
      values,
      changedValues
    } = this.state;

    if ((!values && !changedValues) || template == null) {
      return <div key="UserRoleDetails"></div>;
    }

    if (!template) return null;
    var type = template.type;
    var changed = null;

    var dateBy = getLocalizedText('DYN.DELETE.DATEBY', "<p><b>Record Removed</b> on <b>{2}</b> by <b>{3}</b></p>");
    var currentDateBy = getLocalizedText('DYN.CURRENT.DATEBY', "<p><b>Record Created</b> on <b>{0}</b> by <b>{1}</b></p>"
      + "<p><b>Last Updated</b> on <b>{2}</b> by <b>{3}</b></p>"
      + "<p><b>Last Approved</b> on <b>{4}</b> by <b>{5}</b></p>");
    var currentTitle = getLocalizedText('DYN.CURRENT', "Current");
    var changedDateBy = getLocalizedText('DYN.CHANGED.DATEBY', "<p><b>Updated</b> on <b>{2}</b> by <b>{3}</b></p>");
    var changedTitle = getLocalizedText('DYN.CHANGED', "Changed");

    if (type == "delete") {
      changed = <div
        key={"deletedCol"}
        className="DetailsColumn">
        <div className="DateBy" dangerouslySetInnerHTML={{__html: genRecordDateBy(dateBy, changedValues)}}></div>
      </div>
    } else {
      var changeActionIndex = function() {
        var orgValues = ContentStore.getPageContent().valuesBackup;

        if(!(orgValues.isNew && type == 'add') && checkChanges(orgValues, changedValues)){
          return 1;
        } else {
          return 0;
        }
      };

      changed = <ChangedColumn
          show = {type != 'details'}
          dateBy = {changedDateBy}
          showDiff = { type == 'edit' }
          changeActionIndex = { changeActionIndex }
          title = {type=="edit"?changedTitle:null}
          id = "changed"
          ref = "changed" />
    }

    //   fields += <div>;
    var subHeader = null;
    if (type == "edit" || type=="delete") {
      subHeader = (
        <div key="subtitle" ref="subtitle" style={{display:'flex', height: '40px'}}>
          <div key="stitle1" className="colTitle" style={ {width: '50%', display:'inline-block', borderRight:'1px solid', borderColor: appTheme.palette.borderColor}}>
            <p>{currentTitle}</p>
          </div>
          <div key="stitle2" className="colTitle grey" style={ {width: '50%', display:'inline-block'}}>
            <p>{changedTitle}</p>
          </div>
        </div>
      );
    } else {
      subHeader = (<div key="subtitle" ref="subtitle"></div>)
    }
    this.resetWidth4Scrollbar();

    return (
      <div key="UserRoleDetails" className={"PageContent " + (type=="edit"?"Compact":"Regular")}>
        <span className={"PageHeader"}>{template.title}</span>
        {subHeader}
        <div style={{height: this.state.windowHeight - (type=="edit"||type=="delete"?156:116) + 'px', overflowY: 'auto' }}>
          <div style={{display:'flex', paddingBottom: '40px'}} ref="contentInner">
            <CurrentColumn
               show = {type != 'add'}
               dateBy = {currentDateBy}
               title = {type=="edit"||type=="delete"?currentTitle:null}
               style = {type=="edit"||type=="delete"?{borderRight:'1px solid', borderColor: appTheme.palette.borderColor}:{}}
               id = "current"
               ref = "current" />
             {changed}
          </div>
        </div>
      </div>
    );
  }
});

module.exports = UserRoleDetails;
