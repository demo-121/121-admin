let React = require("react");
let mui = require("material-ui");
let StylePropable = mui.Mixins.StylePropable;

let DynMasterTable = require("../../DynamicUI/DynMasterTable.jsx");
let AppBarStore = require('../../../stores/AppBarStore.js');
let ContentStore = require('../../../stores/ContentStore.js');
let MasterTableActions = require('../../../actions/MasterTableActions.js')

module.exports = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  getInitialState() {
    return {
      content: ContentStore.getPageContent(),
    };
  },

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);

    if (this.state.content == null || this.state.content.template == null || this.state.content.values == null) {
      this._reload();
    }
  },

  componentWillUnmount() {
    ContentStore.removeChangeListener(this.onChange);
  },

  componentDidUpdate() {
    if (this.state.content == null || this.state.content.template == null || this.state.content.values == null) {
      this._reload();
    }
  },

  _reload() {
    if (!this.gettingContent) {
      this.gettingContent = true;
      MasterTableActions.refreshMasterTable(0, null, false);
    }
  },

  gettingContent: false,

  onChange() {
    this.gettingContent = false;
    this.setState({
      content: ContentStore.getPageContent(),
    });
  },

  handleRowSelection(rows) {
    var action = 0;
    if (rows == 'all') {
      var appbarValues = AppBarStore.getValues();
      if (appbarValues.statusFilter) {
        if (appbarValues.statusFilter == 'A') {
          action = 1;
        }
      }
    } else if (rows == 'none') {

    } else if (rows instanceof Array && rows.length > 0) {
      var list = this.state.content.values.list;
      var hasNew = false;
      for (let i in rows) {
        if (list[i].action == 'N') {
          hasNew = true;
          break;
        }
      }
      action = hasNew?2:1;
    }
    return action;
  },

  render() {
    if (this.state.content == null || this.state.content.template == null || this.state.content.values == null) {
      this.state.content = ContentStore.getPageContent();
    }
    if (this.state.content == null || this.state.content.template == null || this.state.content.values == null) {
      return <div className="PageContent"></div>;
    } else {
      var windowHeight = window.innerHeight;

      var {
        template,
        values
      } = this.state.content

      return(
        <div className="PageContent">
          <DynMasterTable
            id = {template.id}
            list = {values.list}
            total = {values.total}
            template = {template}
            show = {true}
            multiSelectable = {true}
            handleRowSelection = {this.handleRowSelection}
            height = { (windowHeight - 170) + "px"}
            />
        </div>
      );
    }
  }
});
