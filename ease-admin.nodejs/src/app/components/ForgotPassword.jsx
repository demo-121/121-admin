// var React = require('react');
// var mui = require('material-ui');
// let TextField = mui.TextField;
// let RaisedButton = mui.RaisedButton;
// let Paper = mui.Paper;
// var PasswordActions = require('../actions/PasswordActions.js');
// var PasswordStore = require('../stores/PasswordStore.js');
// var AppStore = require('../stores/AppStore.js');
// let Loading = require('./CustomView/LoadingBlock.jsx');
// import appTheme from '../theme/appBaseTheme.js';
// let Typography = mui.Styles.Typography;

// var ForgotPassword = React.createClass({
//     childContextTypes: {
//         muiTheme: React.PropTypes.object
//     },

//     getChildContext() {
//         return {
//             muiTheme: appTheme.getTheme()
//         }
//     },

//     propTypes: {
// 		userCode: React.PropTypes.string
// 	},
//   componentDidMount() {
//     PasswordStore.addChangeListener(this.onChange);
//   },
//   gettingContent: false,
//   componentWillUnmount: function() {
//     PasswordStore.removeChangeListener(this.onChange);
//   },
//     getInitialState: function() {
//         return {
//           result:[PasswordStore.getResult()],
//             userCode: this.props.userCode,
//             showMsg : false,
//             sentMsg: ''
//         }
//     },
//     onChange() {
//       this.setState({
//         result: PasswordStore.getResult(),
//         template : PasswordStore.getTemplate()
//       });
//     },

//     handleChange: function(e, index, value) {
//         this.setState({value:value});
//     },

//     render() {
//         var self = this;
//         let theme=this.getChildContext().muiTheme;
//         var message = null;
//         var title = null;
//         var subtitle = null;
//         var icon = null;
//         var textfield1 = null;
//         var btn1 = null
//         var btn2 = null

//         let style = {
//            width: '500px',
//            textAlign: 'center'
//         };
//         let titleStyle= {
//             fontSize: 25,
//             paddingTop: '32px',
//             paddingBottom: '20px',
//         };
//         let subTitleStyle= {
//             fontSize: 20,
//             paddingTop: '8px',
//             color: Typography.textLightBlack,
//         };
//         let messageStyle = {
//             fontSize: 16,
//             color: 'red',
//             width: '100%'
//         };
//         let pCenterStyle={
//             zIndex:'0'
//         };
//         let inputStyle= {
//             margin: '0 auto',
//             display: 'table',
//         }
//         let textFieldStyle= {
//             width:'350px'
//         };
//         let buttonStyle={
//             float:'top' ,
//             paddingTop:'25px',
//             paddingBottom:'25px'
//         };

//         let hyperLinkStyle={
//           fontSize:15,
//           display: 'block',
//           cursor: 'pointer'
//         };        

//         if(this.state.template == null){
//           if (!this.gettingContent) {
//             this.gettingContent = true;
//             PasswordActions.sendRequest("");
//           }
//           return <div></div>;
//         } else {
//           title = (
//             <div style={titleStyle}>{this.state.template.title}</div>
//           )

//           subtitle = (
//             <div style={subTitleStyle}>{this.state.template.subtitle}</div>
//           )

//           btn2 = (
//             <div style={buttonStyle}>
//               <a onTouchTap={self.loginPage} style={hyperLinkStyle}>{this.state.template.btn2}</a>
//             </div>
//           )
//           if(!this.state.result.result){
//             textfield1 = (
//               <div style={inputStyle}>
//                   <TextField
//                       label={this.state.template.floatUser}
//                       key="username"
//                       ref="username"
//                       hintText={this.state.template.hintsUser}
//                       hintStyle = { {top:'28px', lineHeight:'24px'} }
//                       floatingLabelText={this.state.template.floatUser}
//                       floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
//                       underlineStyle = { {bottom:'22px'} }
//                       style={textFieldStyle} />
//               </div>
//             )
//             btn1 = (
//               <div style={buttonStyle}>
//                   <RaisedButton
//                       label={this.state.template.btn}
//                       primary={true}
//                       onTouchTap={self.handleClick} />
//               </div>
//             )
//           }

//           if (this.state.result.message) {
//             if(this.state.showMsg){
//               message = <span style={messageStyle}>{this.state.result.message}</span>
//             }
//           }


//       }
//         return (
//             <div>
//                 <div className="pCenter" style={pCenterStyle} >
//                     <div style={{float:'top'}}>
//                         <Paper zDepth={0} style={style}>
//                             {title}
//                             {subtitle}
//                             <br/>
//                             <br/>
//                             {textfield1}
//                             {message}
//                             {btn1}
//                             {btn2}
//                         </Paper>
//                         <Loading/>
//                     </div>
//                 </div>
//             </div>
//         );
//     },

//     loginPage() {
//         document.location="";
//     },

//     handleClick() {
//         this.state.showMsg = true;
//         this.handleSend();
//     },
//     handleSend() {
//         var username = this.refs.username.getValue();
//         PasswordActions.sendRequest(username);
//     }


// });

// module.exports = ForgotPassword;
