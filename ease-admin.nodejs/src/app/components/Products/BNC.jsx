//brochure and cover
let React = require('react');
let mui = require('material-ui');
let ContentStore = require('../../stores/ContentStore.js');
let ImageSet = require('../CustomView/ImageSet.jsx');
let DynActions = require('../../actions/DynActions.js');

let BNC = React.createClass({
  propTypes:{
    template: React.PropTypes.object
  },

  getInitialState: function() {
    var content = ContentStore.getPageContent();
    var values = content.values;
    var changedValues = content.changedValues?content.changedValues:cloneObject(values);
    var template = this.props.template;
    return {
      changedValues: changedValues
    }
  },

  componentDidMount() {
    ContentStore.addChangeListener(this.onValuesChange);
  },

  componentWillUnmount() {
    ContentStore.removeChangeListener(this.onValuesChange);
  },

  onValuesChange() {
    var content = ContentStore.getPageContent();
    var values = content.values;
    var changedValues = content.changedValues?content.changedValues:cloneObject(values);
    if(!checkChanges(changedValues, this.state.changedValues)){
      this.setState({
        changedValues:changedValues,
      })
    }
    /*
    this.setState({
      changedValues:changedValues
    })
    */
  },

  render: function() {
    var content = ContentStore.getPageContent();
    var values = content.values;

    if (content.changedValues === 'undefined' || content.changedValues == null) {
      //DynActions.changeValues(values);
      ContentStore.setChangeValues(cloneObject(values));
    }

    var changedValues = content.changedValues ? content.changedValues : cloneObject(values);
    var template = this.props.template;
    var items = template.items;
    var id = template.id;

    var imgSets = [];
    for (let i = 0; i < items.length; i++) {
      var item = items[i];
      var subSet = [];
      for (let si in item.items) {
        var field = item.items[si];
        var thisFileType = '';
        var thisSizeLimit = 0;
        switch (field.type) {
          case 'image':
            thisFileType = 'image/png';
            thisSizeLimit = 5*1048576;
            break;
          case 'pdf':
            thisFileType = 'application/pdf';
            thisSizeLimit = 5*1048576;
          default:

        }
        subSet.push(<ImageSet
          parent = {id}
          key ={item.id + "_" + field.id}
          type = {field.id}
          id = {item.id}
          title ={field.title}
          desc = {field.placeholder}
          button = {getLocalizedText("BUTTON.DELETE")}
          buttonAdd = {getLocalizedText("BUTTON.ADD")}
          disabled = {item.disabled}
          require={{
            width: 1224,
            height: 600,
            fileType:thisFileType,
            sizeLimit:thisSizeLimit,// 5MB in byte

          }}

          />)

      }

      imgSets.push(
        <div className="bot-line">
          <div className="DetailsItem">
            <h2>{item.title}</h2>
          </div>
          {subSet}
        </div>
      );
    }

    return <div className="content">
      <div className="bnc">{imgSets}</div></div>;
  },
});

module.exports = BNC;
