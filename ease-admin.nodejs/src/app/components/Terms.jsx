var React = require('react');
let mui = require('material-ui');
let Paper = mui.Paper;
let RaisedButton = mui.RaisedButton;
let ErrorPage = require('./Error.jsx');
var ConfigActions = require('../actions/ConfigActions.js');
var AppStore = require('../stores/AppStore.js');
var ContentStore = require('../stores/ContentStore.js');
let Loading = require('./CustomView/LoadingBlock.jsx');
import appTheme from '../theme/appBaseTheme.js';

var Terms = React.createClass({
  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: appTheme.getTheme(),
    };
  },

  propTypes: {
    title: React.PropTypes.string,
    content: React.PropTypes.string,
    lblAccept: React.PropTypes.string,
    lblDecline: React.PropTypes.string
  },

  getInitialState() {
    // var data = ConfigActions.retrieveContent();
    var content = ContentStore.getPageContent();
    return {
      title: content.title,
      contents: content.details,
      lblAccept: content.accept,
      lblDecline: content.decline
    }
  },

  componentDidMount: function() {
    AppStore.addChangeListener(this.onChange);
    ContentStore.addChangeListener(this.onContentChange);
  },

  componentWillUnmount: function() {
    AppStore.removeChangeListener(this.onChange);
    ContentStore.removeChangeListener(this.onContentChange);
  },

	render: function() {
    var {
      title,
      contents,
      lblAccept,
      lblDecline
    } = this.state;

    var style = {
      width: '50%',
      minWidth: '500px',
      textAlign: 'center'
    };

		return (
      <div className="pCenter">
        <Paper zDepth={0} style={style}>
          <h3 dangerouslySetInnerHTML={{__html: title}} />
          <p dangerouslySetInnerHTML={{__html: contents}} />
          <div style={{display: 'flex'}}>
            <RaisedButton
              label={lblAccept}
              primary={true}
              onTouchTap={this.handleAccept} />
            <div style={{flex:1}}></div>
            <RaisedButton
              label={lblDecline}
              secondary={true}
              onTouchTap={this.handleDecline} />
          </div>
        </Paper>
        <Loading/>
      </div>
    );
	},

  handleAccept: function(e) {
    ConfigActions.termsAccept()
  },

  handleDecline: function(e) {
    //back to login page
    ConfigActions.termsDecline()
  },

  onContentChange() {
    var content = ContentStore.getPageContent();
    var path = AppStore.getPath();
    if (path == '/Login/Terms' && content && content.title) {
      this.setState({
        title: content.title,
        contents: content.details,
        lblAccept: content.accept,
        lblDecline: content.decline
      })
    }
  },

  onChange() {
    var path = AppStore.getPath();
    if (path) {
      redirect(path);
    }
  }
});

module.exports = Terms;
