let React = require('react');
let mui = require('material-ui');

let Snackbar = mui.Snackbar;
let SessionAlertStore = require('../../stores/SessionAlertStore.js');
let SessionAlertActions= require('../../actions/SessionAlertActions.js');

let SessionAlert = React.createClass({
    childContextTypes: {
        muiTheme: React.PropTypes.object,
    },

    getChildContext() {
        return {
            muiTheme: this.state.muiTheme,
        };
    },

    contextTypes: {
        muiTheme: React.PropTypes.object
    },
    
    getInitialState() {
        var message = SessionAlertStore.getMessage();
        var button = SessionAlertStore.getButton();
        var countdown = SessionAlertStore.getCountdown();
        var open = SessionAlertStore.getOpen();
        return {
            message: message,
            button : button,
            countdown : countdown,
            open : open
        };
    },
    
    componentDidMount() {
        SessionAlertStore.addChangeListener(this.handleShow);
    },

    componentWillUnmount() {
        SessionAlertStore.removeChangeListener(this.handleShow);
    },
    
    handleShow() {
        this.setState({open : true});
    },
    
    handleClose() {
        this.setState({open: false})
    },
    
    handleKeepAlive() {
        SessionAlertActions.extend();
    },
    
    render() {
        var self = this;
        
        // var message = self.state.message;
        var button = SessionAlertStore.getButton();
        var message = SessionAlertStore.getMessage();
        var countdown = SessionAlertStore.getCountdown();
        var open = SessionAlertStore.getOpen();
        if (message) {
            message = message.replace("XXX", countdown);
            message = message.replace("YYY", button);
        }
        
        return (
            <Snackbar
                open={open}
                message={message}
                action={button}
                autoHideDuration={600000}
                onActionTouchTap={self.handleKeepAlive}
                onRequestClose={self.handleClose}
            />
        );
    },
    
});

module.exports = SessionAlert;
