let React = require('react');
let mui = require('material-ui');

import FlatButton from 'material-ui/lib/flat-button';
import Dialog from 'material-ui/lib/dialog';
import RaisedButton from 'material-ui/lib/raised-button';
import AppBar from 'material-ui/lib/app-bar';
import appTheme from '../theme/appBaseTheme.js';

let ConfigActions = require('../actions/ConfigActions.js');
let ContentStore = require('../stores/ContentStore.js');

let About = React.createClass({
  gettingContent: false,

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onChange);
  },

  onChange() {
    this.gettingContent = false;
    this.setState({pageContent: ContentStore.getPageContent()})
  },

  getInitialState() {
    return {
      pageContent: ContentStore.getPageContent(),
    };
  },

  getStyle(){
    return{
      center: {textAlign:'center'},
    }
  },

  render() {
    var pageContent = this.state.pageContent;
    if (pageContent == null) {
      pageContent = ContentStore.getPageContent();
    }
    if (pageContent == null || pageContent.template == null) {
      if (!this.gettingContent) {
        this.gettingContent = true;
        ConfigActions.getAbout();
      }
      return <div className="PageContent"></div>;
    } else {
      console.debug("about->content:", pageContent);
      if (!pageContent) return null;
      var template = pageContent.template;
      var values = pageContent.values;
      var items = template.items;
      var section1 = [];
      var section2 = [];
      var section3 = [];
      for (let i = 0; i < items.length; i++) {
        var item = items[i];
        var view = '';

        if (item.type == 'viewOnly') {
          // Section 2
          section2.push(
            <div key={"div_"+ item.id } className="DetailsItem aboutView">
              <label className="grey">{item.title}</label>
              <div>
                <b key={"div_inner_"+ item.id} dangerouslySetInnerHTML={{__html: values[item.id] ? values[item.id] : "N/A"}} />
              </div>
            </div>
          );
        }
        else if (item.type == 'text') {
          // Section 3
          section3.push(
            <div
              key={"div_"+ item.id}
              className="DetailsItem aboutView grey"
              dangerouslySetInnerHTML={{__html: item.title}} />
          );
        } else {
          // Section 1
          switch (item.id) {
            case "logo":
              var logo = (
                <div key={"div_"+ item.id } className="DetailsItem" style={this.getStyle().center}>
                  <img key={item.id} src={item.title} />
                </div>
              );
              section1.push(logo);
              break;
            case 'title':
              section1.push(
                <p
                  key={item.id}
                  style={this.getStyle().center}
                  className="DetailsItem bold">{item.title}
                </p>
              );
              break;
            case 'desc':
              section1.push(
                <p
                  key={item.id}
                  className="DetailsItem aboutView grey"
                  dangerouslySetInnerHTML={{__html: item.title}} />
              );
              break;
          }
        }
      }

      return (
        <div key={template.id} className="about">
          <div className="bot-line">{section1}</div>
          <div className="bot-line abtMid">{section2}</div>
          {section3}
        </div>
      );
    }
  }
});

module.exports = About;
