let React = require("react");
let mui = require("material-ui");
let StylePropable = mui.Mixins.StylePropable;
let FontIcon = mui.FontIcon;
let IconButton = mui.IconButton;
let Colors = mui.Styles.Colors;

let DynMasterTable = require("../DynamicUI/DynMasterTable.jsx");
let ReleaseStore = require('../../stores/ReleaseStore.js');
let CustomDialog = require('../../components/CustomView/CustomDialog.jsx');
let ReleaseDetailActions  = require('../../actions/ReleaseDetailActions.js');
let ContentStore = require('../../stores/ContentStore.js');
let AppStore = require('../../stores/AppStore.js');
let DialogStore = require('../../stores/DialogStore.js');
let ReadOnlyField = require('../CustomView/ReadOnlyField.jsx');
let DialogActions = require('../../actions/DialogActions.js');
let TaskActions = require('../../actions/TaskActions.js');
let MasterTableActions = require('../../actions/MasterTableActions.js');
import Table from 'material-ui/lib/table/table';
import TableBody from 'material-ui/lib/table/table-body';
import TableFooter from 'material-ui/lib/table/table-footer';
import TableHeader from 'material-ui/lib/table/table-header';
import TableHeaderColumn from 'material-ui/lib/table/table-header-column';
import TableRow from 'material-ui/lib/table/table-row';
import TableRowColumn from 'material-ui/lib/table/table-row-column';
import appTheme from '../../theme/appBaseTheme.js';

let TextField = mui.TextField
let Dialog = mui.Dialog;
let FlatButton = mui.FlatButton;
let DatePicker = mui.DatePicker;

let ReleasesDetail = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  propTypes: {

  },
  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);
  },
  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onChange);

    if (this.releaseDetailTimeFunc) {
      clearTimeout(this.releaseDetailTimeFunc)
    }
  },
  onChange() {
    this.setState({
      content: ContentStore.getPageContent(),
      showActions: false,
    });
  },
  getInitialState() {
    return {
      showActions: false,
      content: ContentStore.getPageContent(),
      tooltipState: {}
    };
  },
  handleRowSelection(rows) {
     var content = this.state.content;
     var total = content.values.list.length;

    if ((rows != 'all' && rows.length > 0 && rows != 'none') || (rows == 'all' && total !=0) ){
         this.setState({showActions: true});
    } else {
         this.setState({showActions: false});
    }
  },
  handleTapRow(e, row) {
    var content = this.state.content;
    TaskActions.pageRedirect(content.values.list[row].id, 'FN.RELEASE.DETAIL');
  },

  getStyles() {
    let theme = this.state.muiTheme;

    return {
      buttonStyle: {
        marginLeft: 16,
        color: appTheme.palette.primary2Color,
        backgroundColor:appTheme.palette.background2Color,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: appTheme.palette.borderColor
      }
    }
  },


  refreshStatus() {
      var self = this;
      MasterTableActions.refreshMasterTable(self.state.content.values.list.length, null, true, true);
  },
  render() {
    var windowHeight = window.innerHeight;
    var content = this.state.content;
    var dialogbox = null;
    var isRecordLocked = content.values.isRecordLocked;
    if(!content.values.detail){
      return (null)
    }

    var statusCode = content.values.detail.statusCode;
    var isFailed = content.values.detail.isFailed;
    var isPublishing = content.values.detail.isPublishing;
    var styles = this.getStyles();

    if(content==null){
      return(null);
    }else{
      if (content.dialog != null){
        dialogbox = (<CustomDialog refs="dialog" content={content.dialog}/>);
        DialogStore.setOpen();
      }

      var addfunction = function(){
        ReleaseDetailActions.addTasks(content.values.detail.releaseID)
      }
      var unassignfunction = function(){
        DialogActions.showDialog(content.values.unassignDialog);
      }
      var reassignfunction = function(){
        DialogActions.showDialog(content.values.reassignDialog);
      }
      var suspendfunction = function(){
        DialogActions.showDialog(content.values.suspendDialog);
      }
      var editfunction = function(){
        DialogActions.showDialog(content.values.editReleaseDialog);
      }

      var detailItems = [];

      for (let dii in content.template.detailItems) {
        var item = content.template.detailItems[dii];
        var icon = null;
        var tooltip = null;

        if (item.id == 'status') {
          icon = (isPublishing == "Y") ? (
            <IconButton  iconClassName="material-icons" onTouchTap={this.refreshStatus} style={{padding : 0}} iconStyle={{color : Colors.green500}}>cloud_upload</IconButton>) :
               ((isFailed =='Y') ? (<FontIcon className="material-icons" style={{marginLeft:8}} color={Colors.red500}>error</FontIcon>)
               :null)
          tooltip = (isFailed =='Y') ? content.values.detail.tooltipId : null;
        }
        console.debug('details value : ', item.id, content.values.detail[item.id], icon, tooltip)
        detailItems.push(
          <ReadOnlyField
              key = { item.id }
              id = { item.id }
              className = "DetailsItem"
              floatingLabelText = {item.title}
              style = {{width: null}}
              icon = {icon}
              tooltip = {tooltip}
              defaultValue = {content.values.detail[item.id]} />
        )
      }
      //this.refreshStatus();
      return(
        <div className="PageContent">
          <div style={{position:'relative'}}>
            <div style={{position:'absolute', top:0, right: 0, zIndex: 100, padding: '14px 24px'}}>
                {(statusCode=='P' || statusCode=='T') && isPublishing == 'N' && isRecordLocked == 'N'?<FlatButton style={ styles.buttonStyle } label={content.values.text.BUTTON_EDIT} onTouchTap={editfunction} />:null}
            </div>
            <h2
              style={{ position: 'relative'}}
              key={'header'}
              className="DetailsItem">
              {content.values.text.RELEASE_DETAILS}
            </h2>
          </div>
          <div className="hBoxContainer" >
            {detailItems}
          </div>
          <div style={{position:'relative'}}>
            <div style={{position:'absolute', top:0, right: 0, zIndex: 100, padding: '14px 24px'}}>
              {this.state.showActions?<FlatButton style={styles.buttonStyle} label={content.values.text.BUTTON_UNASSIGN} onTouchTap={unassignfunction}/>:null}
              {this.state.showActions?<FlatButton style={styles.buttonStyle} label={content.values.text.BUTTON_REASSIGN} onTouchTap={reassignfunction}/>:null}
              {(statusCode=='P' || statusCode=='T') && isPublishing == 'N' &&  isRecordLocked == 'N'?<FlatButton style={styles.buttonStyle } label={content.values.text.BUTTON_ADD} onTouchTap={addfunction} />:null}
            </div>
            <h2 key={'header'}
              className="DetailsItem">
              {content.values.text.MENU_TASKS}
            </h2>
          </div>
          <DynMasterTable
            id = {'releaseDetails'}
            list = {content.values.list}
            total = {content.values.total}
            template = {content.template}
            show = {true}
            multiSelectable = {(statusCode =='P' || statusCode=='T') && isPublishing == 'N' && isRecordLocked == 'N'? true:false}
            height = { (windowHeight - 438) + "px"}
            handleTapRow = {this.handleTapRow}
            handleRowSelection = {this.handleRowSelection} />
        { dialogbox }
      </div>
      );
    }
  }
});

module.exports = ReleasesDetail;
