let React = require("react");
let mui = require("material-ui");
let StylePropable = mui.Mixins.StylePropable;
let DynMasterTable = require("../DynamicUI/DynMasterTable.jsx");
let ReleaseStore = require('../../stores/ReleaseStore.js');
let CustomDialog = require('../../components/CustomView/CustomDialog.jsx');
let ReleaseActions = require('../../actions/ReleaseActions.js');
let ContentStore = require('../../stores/ContentStore.js');

let AppStore = require('../../stores/AppStore.js');
let AppBarStore = require('../../stores/AppBarStore.js');
let DialogStore = require('../../stores/DialogStore.js');
let DialogActions = require('../../actions/DialogActions.js');

let TextField = mui.TextField
let Dialog = mui.Dialog;
let FlatButton = mui.FlatButton;
let DatePicker = mui.DatePicker;

let Releases = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  propTypes: {

  },

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);

    if(content == null || content.template == null){
      if (!this.gettingContent) {
        setTimeout(function() {
          this.gettingContent = true;
          ReleaseActions.getReleasesList();
        }, 10)
      }
    }
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onChange);
  },

  gettingContent: false,

  onChange() {
    if (ContentStore.getPageContent())
      this.gettingContent = false;

    this.setState({
      content: ContentStore.getPageContent(),
    });
  },

  getInitialState() {
    return {
      content: ContentStore.getPageContent(),
    };
  },

  handleTapRow(e, row) {
    ReleaseActions.getReleasesDetail(this.state.content.values.list[row].release_ID);
  },

  handleOpen (){
    this.setState({open: true});
  },
  handleClose(){
    this.setState({open: false});
  },



  handleRowSelection(rows) {
    //TODO:john:if base  version different is found, go action 8 to  alert user  and disable push
    var content = this.state.content;
    var statusList = [];
    var publishingList = [];
    //var baseDiffList = [];
    var taskCount = 0;

    if (rows == 'all') {
      for (let i = 0; i < content.values.list.length; i++) {
        var status = content.values.list[i].statusCode;
        var publishing = content.values.list[i].isPublishing;
        //var isBaseDiff = content.values.list[i].isBaseDiff;

        if (status != undefined)
          statusList.push(status);

        if (publishing != undefined)
          publishingList.push(publishing);

        //if (isBaseDiff != undefined)
        //  baseDiffList.push(isBaseDiff);
      }
    } else if (rows.length > 0) {
      for (let i = 0; i < rows.length; i++) {
        if (content.values.list[rows[i]] != undefined) {
          statusList.push(content.values.list[rows[i]].statusCode);
          publishingList.push(content.values.list[rows[i]].isPublishing);
          //baseDiffList.push(content.values.list[rows[i]].isBaseDiff);
          taskCount += content.values.list[rows[i]].task_count;
        }
      }
    }

    var action;
    var hasPending = statusList.indexOf("P") != -1 ? true : false;
    var hasTesting = statusList.indexOf("T") != -1 ? true : false;
    var hasScheduled = statusList.indexOf("S") != -1 ? true : false;
    var hasReleased = statusList.indexOf("R") != -1 ? true : false;
    var hasPublishing = publishingList.indexOf("Y") != -1 ? true : false;
    //var hasBaseDiff = baseDiffList.indexOf(true) != -1 ? true : false;

    if (hasPublishing) {
        action = 1;
    } else if (hasPending && !hasTesting && !hasScheduled && !hasReleased && statusList.length == 1 && taskCount > 0) {
        action = 2;
    } else if (hasTesting && !hasPending && !hasScheduled && !hasReleased && statusList.length == 1 && taskCount > 0) {
        action = 3;
    } else if (hasScheduled && !hasTesting && !hasPending && !hasReleased && statusList.length == 1 && taskCount > 0) {
        action = 4;
        AppBarStore.getAllActions()[3][0].dialog.items[1].value = content.values.list[rows[0]].scheduledDate;
        AppBarStore.getAllActions()[3][0].dialog.items[2].value = content.values.list[rows[0]].scheduledTime;
    } else if (hasReleased && !hasPending && !hasTesting && !hasScheduled) {
        action = 5;
    } else if (statusList.length > 0 && hasPending && hasTesting && !hasScheduled && !hasReleased) {
        action = 6;
    } else if ((hasTesting || hasPending) && !hasScheduled && !hasReleased && !hasPublishing) {
        action = 7;
    } else {
        action = 1;
    }

    //if(hasBaseDiff){
    //  action = 8;
    //}

    if (rows != 'none' && statusList.length > 0)
      return action - 1;


    else
      return 0;
  },

  render() {
    var windowHeight = window.innerHeight;
    var content = this.state.content;
    var dialogbox = null;

    if (content == null)
      this.state.content = content = ContentStore.getPageContent();

    if(content == null || content.template == null){
      return <div></div>;
    } else {
      return(
        <div className="PageContent">
          <DynMasterTable
            id = {'release'}
            list = {content.values.list}
            total = {content.values.total}
            template = {content.template}
            show = {true}
            multiSelectable = {true}
            height = { (windowHeight - 170) + "px"}
            handleTapRow = {this.handleTapRow}
            handleRowSelection = {this.handleRowSelection} />
        </div>
      );
    }
  }
});

module.exports = Releases;
