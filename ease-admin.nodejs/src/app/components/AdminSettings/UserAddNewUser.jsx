let React = require('react');
let mui = require('material-ui');
let DropDownMenu = mui.DropDownMenu;
let MenuItem = mui.MenuItem;
let TextField = mui.TextField;
let IconButton = mui.IconButton;
let UserAddNewUser = React.createClass({
  render: function() {
    return(
    <div className="new-user">
      <div>
      <TextField hintText="User Name" className="fullWidth"></TextField>
      </div>
      <div className="inlineblock">
       </div>
         <DropDownMenu value={3} className="fullWidth">
           <MenuItem style={{padding:'0px',}} value={1} primaryText="All Broadcasts" />
           <MenuItem style={{padding:'0px',}} value={2} primaryText="All Voice" />
           <MenuItem style={{padding:'0px',}} value={3} primaryText="Department" />
           <MenuItem style={{padding:'0px',}} value={4} primaryText="Complete Voice" />
           <MenuItem style={{padding:'0px',}} value={5} primaryText="Complete Text" />
           <MenuItem style={{padding:'0px',}} value={6} primaryText="Active Voice" />
           <MenuItem style={{padding:'0px',}} value={7} primaryText="Active Text" />
         </DropDownMenu>
       <div>
      <DropDownMenu value={3} className="fullWidth">
        <MenuItem value={1} primaryText="All Broadcasts" />
        <MenuItem value={2} primaryText="All Voice" />
        <MenuItem value={3} primaryText="position" />
        <MenuItem value={4} primaryText="Complete Voice" />
        <MenuItem value={5} primaryText="Complete Text" />
        <MenuItem value={6} primaryText="Active Voice" />
        <MenuItem value={7} primaryText="Active Text" />
      </DropDownMenu>
    </div>
    <div>
      <TextField hintText="Email Address"></TextField>
      </div>
      <div>
      <TextField hintText="Phone no."></TextField></div>

      <p>Access Rights</p>
      <div><TextField hintText="Company, Channel, User Role"></TextField></div>

      <IconButton className="material-icons" color="#fff" hoverColor="#fff" style={{color:'#fff'}}>add</IconButton>
      <TextField floatingLabelText="Activated" value="Yes"></TextField>
  </div>
  );
}
});
module.exports = UserAddNewUser;
