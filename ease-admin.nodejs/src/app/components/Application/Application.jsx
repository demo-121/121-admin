/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;

let Application = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],



  render: function() {

    return (
      <div>
      </div>
    );
  }
});

module.exports = Application;
