/** Report page */
let React = require('react');
let mui = require('material-ui');

let FlatButton = mui.FlatButton;
let TextField = mui.TextField;
let Dialog = mui.Dialog; 
let RadioGroup = require('../CustomView/DynRadioGroup.jsx');

 
let ReportActions = require('../../actions/ReportActions.js');
let ContentStore = require('../../stores/ContentStore.js');
let DynActions = require('../../actions/DynActions.js');
let DatePicker = mui.DatePicker;
let SelectField = mui.SelectField;
let MenuItem = mui.MenuItem; 

let Report = React.createClass({

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onChange);
  },

  gettingContent: false,
  
  onChange() {
    this.gettingContent = false;
    this.setState({pageContent: ContentStore.getPageContent()})
  },

  setValues(newValues){
    DynActions.changeValues(newValues);
  },

  getInitialState() {
    var pageContent = ContentStore.getPageContent();
    var changedValues = pageContent ? pageContent.changedValues : null;
    return {
      pageContent: pageContent,
      changedValues: changedValues,
      open: false
    };
  },
  
  handleSubmitTap(e, target) {
    var values = ContentStore.getPageContent().changedValues;
    var self = this;
    if (target) {

       var cb = function(res){
        if(res && res.content&& res.content.values){
          var values = res.content.values;
          if(values.error){ 
            self.state.open=true;       
          }else if(values.report ) { 
            self.download(values.report.fileName,values.report.file);		
          }

          if (values.resultStr){
            self.state.open=true;
          }
        }
         
	   }
      ReportActions.reportSearch("/Report/Generate", cb);
    }
  },  

	download: function (filename, text) {
	  var pom = document.createElement('a');
	  pom.setAttribute('href',  text);
	  pom.setAttribute('download', filename);

	  if (document.createEvent) {
	      var event = document.createEvent('MouseEvents');
	      event.initEvent('click', true, true);
	      pom.dispatchEvent(event);
	  }
	  else {
	      pom.click();
	  }
	}, 
  
  getStyle(){
    var style = {
      'buttonBgColor':'#FAFAFA',
      'buttonGreen':'#2DB154',
      'buttonRed':'#F1453D',
    }
    return style;
  },  

  changeValues(id, value){

    var values = ContentStore.getPageContent().changedValues;
    var newValues = cloneObject(values);
    newValues[id] = value;
    this.setValues(newValues);
  },

   

  render: function() {
    var self            = this;
    var pageContent     = this.state.pageContent;
    var contentInStore  = ContentStore.getPageContent();
    pageContent         = contentInStore;

    if (pageContent == null || pageContent.template == null) {
      if (!this.gettingContent) {
        this.gettingContent = true;
        ReportActions.getReport();
      }
      return <div className="PageContent"></div>;
    } else {
      if (!pageContent) return null;
      
      var template  = pageContent.template;
      var title     = template.title;
      var values    = pageContent.changedValues;

      var leng      = template.items.length;
      var content   = [];
      // this.refs["bt-"+i].getValue()
      
      var menuItems = [];
      for (var j =0; j<3;j++){  
        menuItems.push(<MenuItem
          value     ={j}
          key       ={j}
          primaryText={j} />
          );          
      }

      for (let i=0; i<leng; i++){
        var item = template.items[i];

        switch(item.type){
          case "radioGroup":
            var onRGChange = function(e, newSelection){
              var newValues = cloneObject(values);
              newValues[this.id] = newSelection;
              self.setValues(newValues);
            }.bind( { id: item.id} )
            
            var value = values[item.id];
            content.push(<h2 key={item.title}>{item.title}</h2>);
            content.push(<RadioGroup onChange={onRGChange} key={"rg"+item.id} id={item.id} ref={item.id} template={item} value={value} />);
            break;
          
          case "selectField":

            var oItems = [];
            for (let j in item.options){
              var option  = item.options[j];
              var otitle  = getLocalText('en', option.title)
              var oItem   = <MenuItem
                            value={option.value}
                            key={option.value}
                            primaryText={otitle} />
              oItems.push(oItem);
            }

           
            var onSFChange = function(e, index, newSelection){
              var newValues = cloneObject(values);
              newValues[this.id] = newSelection;
              self.setValues(newValues);
              if(this.id == 'reportId'){
                ReportActions.reportSearch("/Report");
              }
            }.bind( { id: item.id} )
            
            var value = values[item.id];
            content.push(<h2 key={item.title}>{item.title}</h2>);
            content.push(
            	<SelectField 
            	onChange={onSFChange} 
            	key={"rg"+item.id} 
            	id={item.id} 
            	ref={item.id}  
            	value={value} 
            	children={oItems}/>
            	);
            break;
            
          case "datePicker":
            var onDPChange = function(e, newSelection){
              var newValues = cloneObject(values);
              newValues[this.id] = newSelection.getDate()+"/"+(newSelection.getMonth()+1)+"/"+newSelection.getFullYear();
              self.setValues(newValues);
            }.bind( { id: item.id} )
            
            var dateValue = null; 
            if(values[item.id]){
              var parts = values[item.id].split('/');
              dateValue = new Date(parts[2],parts[1]-1,parts[0])
            }
           
            content.push(<h2 key={item.title}>{item.title}</h2>);
            content.push(
            	<DatePicker 
            	onChange={onDPChange} 
            	key={"rg"+item.id} 
            	id={item.id} 
            	ref={item.id}  
              value={dateValue}
              formatDate = {
                function (date) {
                  if (date && date instanceof Date) {
                    return date.format("dd-mmmm-yyyy"); 
                  }
                  return ""
                }
              }
              mode="landscape" />
            	);
            break; 

          case "textField":  
            content.push(<h2 key={item.title}></h2>);
            var value = values[item.id];
            var onTFChange = function(e){
              var newValues = cloneObject(values);
              newValues[this.id] = e.target.value; 
              self.setValues(newValues);
            }.bind( { id: item.id} )             
            
            var tf =
              (<TextField
                key           = {item.id}
                ref           = {item.id}
                style         = { {width: '240px', marginRight:'20px', display:'inline-block' } }
                floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
                underlineStyle = { {bottom:'22px'} }
                floatingLabelText = {item.title}
                value         = { value }
                onChange      ={onTFChange}
              />) 
              content.push(tf);  
            break;
          
          case "button":
            var button =
            (<FlatButton
              key={"bt_"+item.id}
              labelStyle={{color:this.getStyle().buttonGreen}}
              backgroundColor={this.getStyle().buttonBgColor}
              style={{color:this.getStyle().buttonGreen}}
              id={item.id}
              target={item}
              onTouchTap={this.handleSubmitTap}>
              {item.title}
            </FlatButton>);
            content.push(<div className="saveBt">{button}</div>);
            break;
        }

      }

       
      var dialog = [];
      
      if(values.error) {
        dialog.push(
          <Dialog
            open={this.state.open}
            actions={<FlatButton
              label="OK"
              primary={true} 
              onClick={function(){
                self.setState({open: false})
              }}
            />}
            modal={false}
            onRequestClose={ function(){
              this.setState({open: false})
            }}
            title={values.error.title}
          >
            {values.error.message}
          </Dialog>
        )
      }

      if (values.resultStr){
        dialog.push(
          <Dialog
            modal  
            open      ={this.state.open}
            actions   ={<FlatButton
              label   ="OK"
              primary ={true} 
              onClick ={() => this.setState({ open: false })}
            />}
            
            onRequestClose ={function(){
              this.setState({open: false})
            }}
            title     ={'Business Continuous Processing'}
          >
            {values.resultStr}
          </Dialog>
        )
      }

      return (
            <div className="settings">
              {content}
              {dialog}
            </div>
      );

    }
  }
});

module.exports = Report;
