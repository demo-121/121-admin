let HomeStore = require('../stores/HomeStore.js');
let AppBarStore = require('../stores/AppBarStore.js');
let MasterTableStore = require('../stores/MasterTableStore.js');
let ContentStore = require('../stores/ContentStore.js');
let AppDispatcher = require('../dispatcher/AppDispatcher.js');
let ConfigConstants = require('../constants/ConfigConstants.js');

let ActionTypes = ConfigConstants.ActionTypes;


module.exports = {
  updateSelectedRow(rows, list, actionIndex) {


    var selected;
    var hasNew = false;
    if (rows == 'all')  {
    //   for (let i = 0; i < list.length; i++) {
    //     var item = list[i];
    //     selected[item.id] = true;
    // //     refinedRows.push(i);
    //   }
      selected = 'all'
      hasNew = true;
    } else if (rows == 'none') {
      selected = ''
    } else if (rows.length) {
      selected = [];
      for (let i = 0; i < rows.length; i++) {
        if (rows[i] != undefined) {
          var item = list[rows[i]]
          selected.push(item.id);
          if (item.action == 'N') {
            hasNew = true;
          }
        }
      }
    }

    AppDispatcher.dispatch({
      actionType: ActionTypes.MasterTable.SELECTION_CHANGE,
      hasNew: hasNew,
      rows: selected,
      actionIndex: actionIndex
    })
  },

  selectRow(row) {
    var page = HomeStore.getCurrentPage();
    var list = ContentStore.getPageContent().values.list;
    var item = list[row];
    var para = {
      p0: item.id,
      p1: page.module,
      p10: convertObj2EncodedJsonStr(page)
    }
    callServer(page.id+"/Details", para)
  },

  updateSortInfo(sortBy, sortDir) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.MasterTable.SORT_TARGET_CHANGE,
      sortBy: sortBy,
      sortDir: sortDir
    });

    this.refreshMasterTable(0, {
      sortInfo: {
        sortBy: sortBy,
        sortDir: sortDir
      }
    });
  },

  refreshMasterTable(start, condition, delay, reloadStatus) {
    var filters = condition?condition.filters:null;
    var sortInfo = condition?condition.sortInfo:null;
    var content = ContentStore.getPageContent();

    if (!filters) {
      filters = AppBarStore.getValues();
    }
    var para = cloneObject(filters);

    if (!sortInfo) {
      sortInfo = MasterTableStore.getSortInfo();
    } else {
      para.sorting = true;
    }

    if (!start) {
      start = 0;
    }

    if (content) {
      if (content.values) {
          para['id'] = content.values.id;
      }
    }

    var page = HomeStore.getCurrentPage();
    // refine criteria
    var criteria = filters.criteria;
    para['criteria'] = criteria && criteria.length >= ConfigConstants.SearchMinLenght ? criteria : "";

    // set the sort info
    for (let k in sortInfo) {
      para[k] = sortInfo[k];
    }

    // set record start and page size
    para['recordStart'] = start;
    para['pageSize'] = MasterTableStore.getPageSize();
    para['reloadStatus'] = reloadStatus;
    // Commented by Sam
    //Ask Sam if you want to reopen it.
    // var content = ContentStore.getPageContent();
    // if (content) {
    //   para['rows'] = content.values.id;
    // }

    var data = {
      p0: convertObj2EncodedJsonStr(para),
      p1: page.module,
      p10: convertObj2EncodedJsonStr(page)
    }

    if (delay) {
      callServerWithWaiting(ConfigConstants.SearchLatency, page.id, data);
    } else {
      suspendCallServer();
      callServer(page.id, data);
    }
  }
}
