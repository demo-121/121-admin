let MasterTableActions = require('../actions/MasterTableActions.js');
let AssignContentStore = require('../stores/AssignContentStore.js');
let AppDispatcher = require('../dispatcher/AppDispatcher.js');
let ConfigConstants = require('../constants/ConfigConstants.js');
let ActionTypes = ConfigConstants.ActionTypes;

module.exports = {
  updateSelectedRow(rows, list, actionIndex) {

    console.debug("handleRowSelection", rows);

    var selected;
    var hasNew = false;
    if (rows == 'all')  {
    //   for (let i = 0; i < list.length; i++) {
    //     var item = list[i];
    //     selected[item.id] = true;
    // //     refinedRows.push(i);
    //   }
      selected = 'all'
      hasNew = true;
    } else if (rows == 'none') {
      selected = ''
    } else if (rows.length) {
      selected = [];
      for (let i = 0; i < rows.length; i++) {
        if (rows[i] != undefined) {
          var item = list[rows[i]]
          selected.push(item.id);
          if (item.action == 'N') {
            hasNew = true;
          }
        }
      }
    }

    AppDispatcher.dispatch({
      actionType: ActionTypes.AssignPage.SELECTION_CHANGE,
      hasNew: hasNew,
      rows: selected,
      actionIndex: actionIndex
    })
  },

  updateSortInfo(sortBy, sortDir) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.AssignPage.SORT_TARGET_CHANGE,
      sortBy: sortBy,
      sortDir: sortDir
    });

    this.refreshMasterTable(0, {
      sortInfo: {
        sortBy: sortBy,
        sortDir: sortDir
      }
    });
  },

  refreshMasterTable(start, condition, delay) {
    var filters = condition?condition.filters:null;
    var sortInfo = condition?condition.sortInfo:null;
    var content = AssignContentStore.getPageContent();

    if (!filters) {
      filters = AssignContentStore.getValues();
    }
    var para = cloneObject(filters);

    if (!sortInfo) {
      sortInfo = AssignContentStore.getSortInfo();
    } else {
      para.sorting = true;
    }

    if (!start) {
      start = 0;
    }

    if (content) {
      if (content.values) {
          para['id'] = content.values.id;
      }
    }

    var page = AssignContentStore.getCurrentPage();
    // refine criteria
    var criteria = filters.criteria;

    para['criteria'] = criteria && criteria.length >= ConfigConstants.SearchMinLenght ? criteria : "";

    // set the sort info
    for (let k in sortInfo) {
      para[k] = sortInfo[k];
    }

    // set record start and page size
    para['recordStart'] = start;
    para['pageSize'] = AssignContentStore.getPageSize();

    // Commented by Sam
    //Ask Sam if you want to reopen it.
    // var content = ContentStore.getPageContent();
    // if (content) {
    //   para['rows'] = content.values.id;
    // }

    var data = {
      p0: convertObj2EncodedJsonStr(para),
    }

    if (delay) {
      callServerWithWaiting(ConfigConstants.SearchLatency, page.id, data);
    } else {
      suspendCallServer();
      callServer(page.id, data);
    }
  },
  updateCriteria(value, delay) {
    var values = AssignContentStore.getValues();
    values["criteria"] = value;

    AppDispatcher.dispatch({
      actionType: ActionTypes.AssignPage.UPDATE_VALUES,
      values: values
    });

    if (!value || value.length >= ConfigConstants.SearchMinLenght || value.length == 0) {
      this.refreshMasterTable(0, {
        filters: values
      }, delay);
    }
  },




  updateFilter(id, value) {
    var values = AssignContentStore.getValues();
    values[id] = value;

    AppDispatcher.dispatch({
      actionType: ActionTypes.AssignPage.UPDATE_VALUES,
      values: values
    })
    AssignPageActions.refreshMasterTable(0, {
      filters: values
    });
  },

  reloadPageFromServer() {
    AssignPageActions.refreshMasterTable(0);
  },


  // submit action with selected row
  submitSelected(actionItem) {
    var values = {};
    values['rows'] = AssignContentStore.getSelectedRows()

    var content = AssignContentStore.getPageContent();

    if (content.values.id != null) {
      values['keyList'] = content.values.id;
    }

    var data = {
      p0: convertObj2EncodedJsonStr(values),
    }

    console.debug("[DynDialog] - [callServer]", values);
    callServer(actionItem.id, data);

    self.handleClose();
  },

  // submit action and record id to server
  submitCurrentId(action) {
    var page = AssignContentStore.getCurrentPage();
    var content =  AssignPageActions.getPageContent();
    var recordId = null;
    if (content.values) {
      recordId = content.values.id;
    }

    if (action) {
      var para = {
        p0: recordId,
        p1: page.module,
        p2: page.id
      }
      callServer(action, para)
    }
  },
  submitCurrentTaskId(action) {
    var page = AssignContentStore.getCurrentPage();
    var rows = {rows:AssignContentStore.getPageContent().values.taskId};
    if (rows && action) {
      var para = {
        p0: convertObj2EncodedJsonStr(rows),
        p1: page.module,
        p2: page.id,
      }
      callServer(action, para)
    }

  },

  selectRow(row) {
    /*var page = HomeStore.getCurrentPage();
    var list = ContentStore.getPageContent().values.list;
    var item = list[row];
    var para = {
      p0: item.id,
      p1: page.module,
      p10: convertObj2EncodedJsonStr(page)
    }
    callServer(page.id+"/Details", para)*/
  },


  back(){
    AppDispatcher.dispatch({
      actionType: ActionTypes.AssignPage.BACK
    })
  }

}
