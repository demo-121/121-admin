var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypes = ConfigConstants.ActionTypes;

var ContentStore = require('../stores/ContentStore');
var DialogActions = require('../actions/DialogActions');
var AppBarActions = require('../actions/AppBarActions');

var DynMenuActions = {
  updateMenuState: function(mstate) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.UPDATE_MENU_STATE,
      menuState: mstate
    })
  },

  changePage(module, id, callback) {
    let content = ContentStore.getPageContent();
    let {
      values,
      changedValues
    } = content;


    changedValues['selectPageId'] = id;
    let url = module + "/Save";
    AppBarActions.submitChangedValues(url, true);
  }
}
module.exports = DynMenuActions;
