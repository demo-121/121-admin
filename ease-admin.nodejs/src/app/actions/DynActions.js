var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypes = ConfigConstants.ActionTypes;
var TemData = require('../actions/TemData');
var MasterTableActions = require('../actions/MasterTableActions');
var ConfigActions = require('../actions/ConfigActions');
let AppBarStore = require('../stores/AppBarStore.js');
let ContentStore = require('../stores/ContentStore.js');
let HomeStore = require('../stores/HomeStore.js');

var DynActions = {
	reflesh() {
		MasterTableActions.refreshMasterTable(0, {}, false);
	},

	goDynDetail(item, backPage) {
		var page = HomeStore.getCurrentPage();
		var data = {
			id: item.id
		};
		callServer("/DynTemplate/Detail", {
			p0: convertObj2EncodedJsonStr(data),
			p1: page.module
		});
  	},

	getFunctionPara(paras) {
		var self = this;
		var page = HomeStore.getCurrentPage();
		var values = ContentStore.getPageContent().changedValues;
		var quot = null;

		var requireParas = [];
		var baseCode = "";
		var planCode = "";
		var baseVer = 1;
		var planVer = 0;

		for (var i in paras) {
			var para = paras[i];
			// if (para.type == 'quotation') {
			// 	quot = para.value;
			// 	break;
			// }
			if (para.type == 'quotation') {
				if (para.value && para.value.baseProductCode) {
					baseCode = para.value.baseProductCode;
				}
				if (para.value && para.value.plans && para.value.plans[0]) {
					if (para.value.plans[0].covCode) {
						baseCode = para.value.plans[0].covCode;
					}
					if (para.value.plans[0].version) {
						baseVer = para.value.plans[0].version;
					}
				}
				if (!planCode && baseCode) planCode = baseCode;
				if (!planVer && baseVer) planVer = baseVer;
				requireParas.push({
					type: para.type,
					code: para.code || baseCode,
					version: para.version || baseVer
				})
			} else if (para.type == 'planInfo') {
				if (para.value && para.value.covCode) {
					planCode = para.value.covCode
				}
				if (para.value && para.value.version) {
					planVer = para.value.version
				}
				requireParas.push({
					type: para.type,
					code: para.code || planCode,
					version: para.version || planVer
				})
			} else if (para.type == 'planDetail') {
				requireParas.push({
					type: para.type,
					code: para.code || planCode,
					version: para.version || planVer
				})
			} else {
				requireParas.push({
					type: para.type,
					code: para.code || baseCode,
					version: para.version || baseVer
				})
			}
		}

		if (this.gettingFunctionPara) {
			if (this.calling) {
				return;
			} else {
				clearTimeout(this.gettingFunctionPara);
			}
		}

		this.gettingFunctionPara = setTimeout(function() {
			this.calling = true;
			callServerAndCallback("/DynTemplate/GetFunctionPara", {
				p0: convertObj2EncodedJsonStr({
					docCode: values.id,
					version: values.version,
					params: requireParas
				}),
				p1: page.module
			}, function(res) {
				self.gettingFunctionPara = false;
				self.calling = false;
				AppDispatcher.dispatch({
					actionType: ActionTypes.UPDATE_FUNC_EDITOR_DATA,
					data: res.data
				})
			}, function(res) {
				self.calling = false;
				self.gettingFunctionPara = false;
			});
		}, 500);
	},

	changeValues(newValues, actionIndex, changedFieldId) {
		/*
		console.debug("changeValues->newValues", newValues);
		AppDispatcher.dispatch({
			actionType: ActionTypes.CHANGE_VALUES,
			newValues: newValues
		})
		*/
		var values = ContentStore.getPageContent().values;
		if (!newValues) newValues = ContentStore.getPageContent().changedValues;

		AppDispatcher.dispatch({
			actionType: ActionTypes.CHANGE_VALUES,
			values: values,
			newValues: newValues,
			actionIndex: actionIndex,
			changedFieldId: changedFieldId
		})

		// if(this.isEquivalent(newValues, values)){
		// 		this.changeAppBar(0, newValues);
		// }else{
		// 		this.changeAppBar(1, newValues);
		// }

	},

	validate(validPath, data, cb) {
		var page = HomeStore.getCurrentPage();
		var data = {
			values: data
		};
		callServer(validPath, {
			p0: convertObj2EncodedJsonStr(data),
			p1: page.module
		}, function(resp) {
			if (cb && resp.content) {
				cb(resp.content.values);
			}
		});
	}
};




module.exports = DynActions;
