var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var HomeStore = require('../stores/HomeStore.js');
var ActionTypes = ConfigConstants.ActionTypes;

var TaskActions = {
  getTaskList() {
     var data = {
 			action:'getTaskList'
 		 };
     var page = HomeStore.getCurrentPage();
     var param = {
       p0:convertObj2EncodedJsonStr(data),
       p10: convertObj2EncodedJsonStr(page)
     };

 	   callServer("/Tasks", param);
  },

  updateSelectedRow(rows) {
    var _rows = {
      rows: rows
    };

    AppDispatcher.dispatch({
      actionType: ActionTypes.TASK_APPBAR_CHANGE,
      rows: _rows
    })
  },

  pageRedirect(taskID, backFuncCode) {
     var data = {
      taskID: taskID,
      backFuncCode: backFuncCode
     };
     var page = HomeStore.getCurrentPage();
     var param = {
       p0:convertObj2EncodedJsonStr(data),
       p10: convertObj2EncodedJsonStr(page)
     };

     callServer("/Tasks/Redirect", param);
  },
}

module.exports = TaskActions;
