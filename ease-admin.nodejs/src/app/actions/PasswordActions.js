var PasswordActions = {
    sendRequest: function(username) {
        callServer('/Password/ForgotRequest','uid='+username);
    },

    sendNewPassword: function(password, confrimPassword, p3) {
        password = encryptPassword(password);
        confrimPassword = encryptPassword(confrimPassword);
        callServer('/Password/ResetAuth','pwd01='+ password + '&pwd02=' + confrimPassword + '&p3=' + p3 );
    },

    sendcheckPassword: function(password, confrimPassword, p3) {
        password = encryptPassword(password);
        confrimPassword = encryptPassword(confrimPassword);
        callServer('/Password/ResetCheck','pwd01='+ password + '&pwd02=' + confrimPassword + '&p3=' + p3  );
    }
};

module.exports = PasswordActions;
  