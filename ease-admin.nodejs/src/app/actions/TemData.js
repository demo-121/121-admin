
var TemDate = {
  userDetailTemplate: {
    type: "detail",
    value: {
      email: "$$$@gmail.com",
      phone: "23222231",
      right_defualt: "m,f,d",
      right: "m,f,d",
      activated: "Yes",
      createDate: "",
      lastUpDate: '',

    },
    items: [{
      type: "section",
      id: 's0',
      items:[{
        type:"text",
        title: "Email Address",
        mandatory: true,
        id:"email"
      }, {
        type:"text",
        title: "Phone no.",
        id:"phone"
      }]
    }, {
      type: "section",
      id: 's1',
      title: "Acess Right",
      items: [{
        type:"text",
        title: "Company,Channel, User Role(Defualt)",
        id:"right_defualt"
      }, {
        type:"text",
        title: "Company,Channel, User Role",
        id:"right"
      },  ]
    },
    {
     type: "section",
     id: 's2',
     title: "",
     items: [{
       type:"text",
       title: "Activated",
       id:"activated"
     }, {
       type:"text",
       title: "REeord created on 2014-01-01 by AMY",
       id:"createDate"
     },
     {
       type:"text",
       title: "Last Updated on 2014-01-01 by AMY",
       id:"lastUpDate"
     }]
   }],
 },
 usersList: {
   title: "Edit User",
   type: "edit",
   value: {
     email: "$$$@gmail.com",
     phone: "23222231",
     right_defualt: "m,f,d",
     right: "m,f,d",
     activated: "Yes",
     createDate: "",
     lastUpDate: '',
   },
   items: [{
     type: "section",
     id: 's0',
     items:[{
       type:"text",
       title: "Email Address",
       mandatory: true,
       id:"email"
     }, {
       type:"text",
       title: "Phone no.",
       id:"phone"
     }]
   }, {
     type: "section",
     id: 's1',
     title: "Acess Right",
     items: [{
       type:"text",
       title: "Company,Channel, User Role(Defualt)",
       id:"right_defualt"
     }, {
       type:"text",
       title: "Company,Channel, User Role",
       id:"right"
     },  {
       type:"dialog",
       title:"Access Right",
       id:"dialog",
       dialog:{
         values:{
           channel: "agency",
           company: "eabhk",
           role:"readonly",

         },
         items:[
          {
           type:"picker",
           title: "Company",
           id:"Company",
           mandatory:true,
           options:[{
             title: "EAB HK",
             value:"eabhk",
           },
           {
             title: "EAB SZ",
             value:"eabsz",
           },
           {
             title: "EAB SH",
             value:"eabsh",
           },],
         },
         {
           type:"picker",
           title: "Channel",
           id:"channel",
          mandatory:true,
           options:[{
             title: "Agency",
             value:"agency",
           },
           {
             title: "Agency 1",
             value:"agency1",
           },
           {
             title: "Agency 2",
             value:"agency2",
           },],
         },
         {
           type:"picker",
           title: "User Role",
           id:"role",
           mandatory:true,
           options:[{
             title: "Full Access",
             value:"fullaccess",
           },
           {
             title: "Partly Access",
             value:"partaccess",
           },
           {
             title: "Read Only",
             value:"readonly",
           },],
         },

         ]
       }
     }]
   },
   {
    type: "section",
    id: 's2',
    title: "",
    items: [{
      type:"text",
      title: "Activated",
      id:"activated"
    }, {
      type:"text",
      title: "REeord created on 2014-01-01 by AMY",
      id:"createDate"
    },
    {
      type:"text",
      title: "Last Updated on 2014-01-01 by AMY",
      id:"lastUpDate"
    }]
  }],
},
addUser: {
  title: "Add User",
  type: "add_new",
  value: {
    email: "",
    phone: "",
    right_defualt: "",
    right: "",
    activated: "",
    createDate: "",
    lastUpDate: '',
  },
  items: [{
    type: "section",
    id: 's0',
    items:[{
      type:"text",
      title: "Email Address",
      mandatory: true,
      id:"email"
    }, {
      type:"text",
      title: "Phone no.",
      id:"phone"
    }]
  }, {
    type: "section",
    id: 's1',
    title: "Acess Right",
    items: [{
      type:"text",
      title: "Company,Channel, User Role(Defualt)",
      id:"right_defualt"
    }, {
      type:"text",
      title: "Company,Channel, User Role",
      id:"right"
    },  ]
  },
  {
   type: "section",
   id: 's2',
   title: "",
   items: [{
     type:"text",
     title: "Activated",
     id:"activated"
   }, {
     type:"text",
     title: "REeord created on 2014-01-01 by AMY",
     id:"createDate"
   },
   {
     type:"text",
     title: "Last Updated on 2014-01-01 by AMY",
     id:"lastUpDate"
   }]
 }],
},

_usersList : {
  type: "table",
  list: [{
    id:'1',
    usertype: "SysADmin0",
    usercode: "amin0",
    username: "Nathan Chan 0",
    remarks: "new user",
    status: "Pending",
    path:"user/detail",
  }, {
    id:'2',
    usertype: "SysADmin1",
    usercode: "amin1",
    username: "Nathan Chan 1",
    remarks: "new user",
    status: "Activated",
    path:"user/detail",

  }, {
    id:'3',
    usertype: "SysADmin2",
    usercode: "amin2",
    username: "Nathan Chan 2",
    remarks: "new user",
    status: "true",
    path:"user/detail",
  }],
  header: {
    id: 'headrow',
    items:[{
      title: "User Type",
      mandatory: true,
      id:"usertype",
      listSeq : 1,
      colWidth : '50%',
      detailSeq : 1
    }, {
      title: "User Code",
      id:"usercode",
      listSeq : 2,
      colWidth : '50%',
      detailSeq : 2
    },{
      title: "User Name",
      id:"username",
      listSeq : 3,
      colWidth : '50%',
      detailSeq : 3
    },{
      title: "Status",
      id:"status",
      listSeq : 4,
      colWidth : '50%',
      detailSeq : 3
    },{
      title: "Remarks",
      id:"remarks",
      listSeq : 4,
      colWidth : '50%',
      detailSeq : 3
    },
  ]
  }
},
}
module.exports = TemDate;
