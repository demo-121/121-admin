var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypes = ConfigConstants.ActionTypes;
var MasterTableStore = require('../stores/MasterTableStore.js');
var HomeStore = require('../stores/HomeStore.js');
var ReleaseDetailActions = {

  addTasks(releaseID){
    var rows = [];
    rows.push(releaseID);
    var data = {
     rows : rows
    };
    var page = HomeStore.getCurrentPage();
    var param = {
      p0:convertObj2EncodedJsonStr(data),
      p10: convertObj2EncodedJsonStr(page)
    };

    callServer("/Releases/Detail/Task", param);

  },

  editRelease(releaseID){
    var data = {
     releaseID : releaseID
    };
    var page = HomeStore.getCurrentPage();
    var param = {
      p0:convertObj2EncodedJsonStr(data),
      p10: convertObj2EncodedJsonStr(page)
    };

    callServer("/Releases/Detail/Edit", param);
  },

  getReleasesDetail(row){
    var data = {
     action:'getReleasesList',
     releaseID : row
    };
    var page = HomeStore.getCurrentPage();
    var param = {
      p0:convertObj2EncodedJsonStr(data),
      p10: convertObj2EncodedJsonStr(page)
    };

    callServer("/TaskRelease", param);
  },

  updateSelectedRow(_rows, statusList) {
    var data = {
     rows: _rows
    };

    AppDispatcher.dispatch({
      actionType: ActionTypes.RELEASE_TASK_CHANGE,
      rows: data
    })
  },

  unassignTasks(releaseID){
    var data = {
     rows: MasterTableStore.getSelectedRows(),
     releaseID: releaseID
    };
    var page = HomeStore.getCurrentPage();
    var param = {
      p0:convertObj2EncodedJsonStr(data),
      p10: convertObj2EncodedJsonStr(page)
    };

    callServer("/Releases/Detail/Unassign", param);
  },

  suspendTasks(releaseID){
    var data = {
     rows: MasterTableStore.getSelectedRows(),
     releaseID: releaseID
    };
    var page = HomeStore.getCurrentPage();
    var param = {
      p0:convertObj2EncodedJsonStr(data),
      p10: convertObj2EncodedJsonStr(page)
    };

    callServer("/Releases/Detail/Suspend", param);
  },

  reassignTasks(releaseID){
    var data = {
     rows : MasterTableStore.getSelectedRows(),
     releaseID : releaseID
    };
    var page = HomeStore.getCurrentPage();
    var param = {
      p0:convertObj2EncodedJsonStr(data),
      p10: convertObj2EncodedJsonStr(page)
    };

    callServer("/Releases/Detail/Reassign", param);
  },

}

module.exports = ReleaseDetailActions;
