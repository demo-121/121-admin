var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypes = ConfigConstants.ActionTypes;
var AppStore = require('../stores/AppStore');

var UserRoleActions = {
  goProductDetail(product) {
    var data = {
      id: product.prodCode,
      version: product.version
    };
    callServer("/Products/Detail", {
      p0: convertObj2EncodedJsonStr(data)
    });
  },

  productChangePage(page) {
    var selectPage = {
      id: page
    }
    AppDispatcher.dispatch({
      actionType: ActionTypes.PRODUCT_CHANGE_PAGE,
      data: {
        selectPage: selectPage
      }
    })
  }

}

module.exports = UserRoleActions;
