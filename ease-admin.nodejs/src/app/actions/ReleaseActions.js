var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var HomeStore = require('../stores/HomeStore.js');
var ActionTypes = ConfigConstants.ActionTypes;

var ReleaseActions = {
  getReleasesList() {
     var data = {
 			action:'getReleasesList',
      pageSize: 10
 		 };
     var page = HomeStore.getCurrentPage();
     var param = {
       p0:convertObj2EncodedJsonStr(data),
       p10: convertObj2EncodedJsonStr(page)
     };

 	   callServer("/Releases", param);
  },

  getReleasesDetail(row){
    var data = {
     releaseID : row
    };
    var page = HomeStore.getCurrentPage();
    var param = {
      p0:convertObj2EncodedJsonStr(data),
      p10: convertObj2EncodedJsonStr(page)
    };

    callServer("/Releases/Detail", param);
  },

  updateSelectedRow(rows) {
    // var action;
    //
    // var hasPending = statusList.indexOf("P") != -1 ? true : false;
    // var hasTesting = statusList.indexOf("T") != -1 ? true : false;
    // var hasScheduled = statusList.indexOf("S") != -1 ? true : false;
    // var hasReleased = statusList.indexOf("R") != -1 ? true : false;
    //
    // if (hasPending && !hasTesting && !hasScheduled && !hasReleased) {
    //     action = 2;
    // } else if (hasTesting && !hasPending && !hasScheduled && !hasReleased) {
    //     action = 3;
    // } else if (hasScheduled && !hasTesting && !hasPending && !hasReleased) {
    //     action = 4;
    // } else if (hasReleased && !hasPending && !hasTesting && !hasScheduled) {
    //     action = 5;
    // } else if (statusList.length > 0 && hasPending && hasTesting && !hasScheduled && !hasReleased) {
    //     action = 6;
    // } else {
    //     action = 5;
    // }

    var _rows = {
     rows: rows
    };

    AppDispatcher.dispatch({
      actionType: ActionTypes.TASK_APPBAR_CHANGE,
      //action: action,
      rows: rows
    })
  },

}

module.exports = ReleaseActions;
