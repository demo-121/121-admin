var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypes = ConfigConstants.ActionTypes;
let TaskReleaseStore = require('../stores/TaskReleaseStore.js');
var AppStore = require('../stores/AppStore');
var searchValue="";

var TaskReleaseActions = {
  taskAssign(rows, releaseID) {
     var _rows = {
      rows: rows,
      releaseID: releaseID
     };

    AppDispatcher.dispatch({
      actionType: ActionTypes.TASK_ASSIGN,
      action: '',
      rows: _rows
    })
  },
}

module.exports = TaskReleaseActions;
