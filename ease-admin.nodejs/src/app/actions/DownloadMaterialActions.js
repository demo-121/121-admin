var ContentStore = require('../stores/ContentStore');
var HomeStore = require('../stores/HomeStore');
var DownloadMaterialActions = {	
	 
	getDownloadMaterial: function(){
		var page = HomeStore.getCurrentPage();
		var para = {
			p1: page.module
		}
		callServer("/DownloadMaterial", para);
	},

	downloadMaterialSearch: function(action, callback) {
		var page = HomeStore.getCurrentPage();
		var changedValues =  ContentStore.getPageContent().changedValues;
		  
		if (action) {
			var para = {
				p0: convertObj2EncodedJsonStr(changedValues),
				p1: page.module
			}
			callServer(action, para, callback);
		}
		 
	},
};

module.exports = DownloadMaterialActions;
