var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypes = ConfigConstants.ActionTypes;
let DynConfigPanelStore = require('../stores/DynConfigPanelStore.js');

var DynConfigPanelActions = {

	openPanel(changedValues, type, actions) {
		AppDispatcher.dispatch({
			actionType: ActionTypes.PANEL_UPDATE,
			show: 'Y',
			changedValues: changedValues,
			type: type,
			actions: actions,
		});
	},

	closePanel(clearData) {
		let data = {
			actionType: ActionTypes.PANEL_UPDATE,
			show: 'N'
		};
		if(clearData){
			data.changedValues = {};
			data.type = '';
			data.actions = {};
		}
		AppDispatcher.dispatch(data);
	},

	updateInputType(){
		AppDispatcher.dispatch({
			actionType: ActionTypes.PANEL_UPDATE
		})
	}
};


module.exports = DynConfigPanelActions;
