var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypes = ConfigConstants.ActionTypes;

var WebSettingsActions = {
  getWebSettingsList() {
    callServer("/WebSettings", {});
  },
}

module.exports = WebSettingsActions;
