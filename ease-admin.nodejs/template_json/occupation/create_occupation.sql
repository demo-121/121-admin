delete from sys_func_mst where module = 'occupation';
insert into sys_func_mst (func_code, name_code, func_uri, show_menu, access_ctrl, upline_func_code, disp_seq, status, module, setable)
    values('FN.OCCP', 'MENU.OCCP', '/DynTemplate', 'Y', 4, null, 2430, 'A', 'occupation', 'Y');

insert into sys_func_mst (func_code, name_code, func_uri, show_menu, access_ctrl, upline_func_code, disp_seq, status, module, setable)
    values('FN.OCCP.DETAILS', 'ACCESS.VIEW', '/DynTemplate/Details', 'N', 5, 'FN.OCCP', 2431, 'A', 'occupation', 'Y');

insert into sys_func_mst (func_code, name_code, func_uri, show_menu, access_ctrl, upline_func_code, disp_seq, status, module, setable)
    values('FN.OCCP.UPD', 'ACCESS.UPDATE', null, 'N', 5, 'FN.OCCP', 2432, 'A', 'occupation', 'Y');

insert into sys_func_mst (func_code, name_code, func_uri, show_menu, access_ctrl, upline_func_code, disp_seq, status, module, setable)
    values('FN.OCCP.ADD', 'ICON.ADD', '/DynTemplate/Add', 'N', 6, 'FN.OCCP.UPD', 2433, 'A', 'occupation', 'Y');

insert into sys_func_mst (func_code, name_code, func_uri, show_menu, access_ctrl, upline_func_code, disp_seq, status, module, setable)
    values('FN.OCCP.SAVE', 'BUTTON.ADD', '/DynTemplate/Save', 'N', 6, 'FN.OCCP.ADD', 2434, 'A', 'occupation', 'Y');


delete from DYN_TEMPLATE_UI_MST where module = 'occupation';
insert into DYN_TEMPLATE_UI_MST (MODULE, UI_NAME, DATA_TABLE, SEARCH_FIELD, PRIMARY_KEY, MASTER_VIEW, COMP_FILTER, LANG_FILTER, NAME_TABLE, TRX_SECTION_ID, PRIMARY_ID, NAME_ID) values ('occupation', 'OCCP.TITLE', 'occupation_trx', 'occupation_code, name_desc', 'occupation_code', 'v_occupation_trx', 'Y', 'Y','OCCUPATION_NAME', 'occupationDetail', 'occupationCode', 'occupationName');

insert into DYN_TEMPLATE_FIELD_MST (MODULE, FIELD_ID, FIELD_TITLE, FIELD_TYPE, FIELD_KEY,  LIST_SEQ, OPTIONS_LOOKUP, LOOKUP_FIELD, PRESENT)
   values ('occupation', 'occupationCode', 'OCCUPATION_CODE', 'text', 'occupation_code', 2, '', '', '');

insert into DYN_TEMPLATE_FIELD_MST (MODULE, FIELD_ID, FIELD_TITLE, FIELD_TYPE, FIELD_KEY,  LIST_SEQ, OPTIONS_LOOKUP, LOOKUP_FIELD, PRESENT)
   values ('occupation', 'occupationName', 'OCCUPATION_NAME', 'text', 'name_desc', 2, '', '', '');




insert into dyn_template_action_mst(MODULE, UI_TYPE, ID, SEQ, TITLE, TYPE, DEFAULT_VALUE, OPTIONS_LOOKUP, LOOKUP_FIELD, IS_TITLE, LVL)
values('occupation', 'table', 'status', 1, null, 'picker', '-R', 'sys_lookup_mst', ' PRODUCT_STATUS', 'N', 1);

insert into dyn_template_action_mst(MODULE, UI_TYPE, ID, SEQ, TITLE, TYPE, DEFAULT_VALUE, OPTIONS_LOOKUP, LOOKUP_FIELD, IS_TITLE, LVL)
values('occupation', 'table', '/DynTemplate/Search', 2, 'Search', 'searchButton', '', '', '', 'N', 1);

insert into dyn_template_action_mst(MODULE, UI_TYPE, ID, SEQ, TITLE, TYPE, DEFAULT_VALUE, OPTIONS_LOOKUP, LOOKUP_FIELD, IS_TITLE, LVL)
values('occupation', 'table', '/DynTemplate/Add', 3, 'Add', 'iconButton', 'add', '', '', 'N', 1);


insert into dyn_template_action_mst(MODULE, UI_TYPE, ID, SEQ, TITLE, TYPE, DEFAULT_VALUE, OPTIONS_LOOKUP, LOOKUP_FIELD, IS_TITLE, LVL, SUB_TYPE)
values('occupation', 'table', '/DynTemplate/NewVersion', 1, 'BUTTON.CREATE_NEW_VERSION', 'dialogButton', '', '', '', 'N', 2, 'new');

insert into dyn_template_action_mst(MODULE, UI_TYPE, ID, SEQ, TITLE, TYPE, DEFAULT_VALUE, OPTIONS_LOOKUP, LOOKUP_FIELD, IS_TITLE, LVL, SUB_TYPE)
values('occupation', 'table', '/DynTemplate/Clone', 2, 'BUTTON.CLONE', 'dialogButton', '', '', '', 'N', 2, 'clone');


insert into dyn_template_action_mst(MODULE, UI_TYPE, ID, SEQ, TITLE, TYPE, DEFAULT_VALUE, OPTIONS_LOOKUP, LOOKUP_FIELD, IS_TITLE, LVL, SUB_TYPE)
values('occupation', 'table', '/DynTemplate/Clone', 1, 'BUTTON.CLONE', 'dialogButton', '', '', '', 'N', 3, 'clone');


insert into dyn_template_action_mst(MODULE, UI_TYPE, ID, SEQ, TITLE, TYPE, DEFAULT_VALUE, OPTIONS_LOOKUP, LOOKUP_FIELD, IS_TITLE, LVL, SUB_TYPE)
values('occupation', 'table', '/DynTemplate/NewVersion', 1, 'BUTTON.CREATE_NEW_VERSION', 'dialogButton', '', '', '', 'N', 4, 'new');

insert into dyn_template_action_mst(MODULE, UI_TYPE, ID, SEQ, TITLE, TYPE, DEFAULT_VALUE, OPTIONS_LOOKUP, LOOKUP_FIELD, IS_TITLE, LVL, SUB_TYPE)
values('occupation', 'detail', '/DynTemplate', 1, 'OCCUPATION_LIST', 'text', '', '', '', 'Y', 1, '');


insert into dyn_template_action_mst(MODULE, UI_TYPE, ID, SEQ, TITLE, TYPE, DEFAULT_VALUE, OPTIONS_LOOKUP, LOOKUP_FIELD, IS_TITLE, LVL, SUB_TYPE)
values('occupation', 'add', '/DynTemplate/Detail/Save', 1, ' BUTTON.SAVE', 'submitChangedButton', '', '', '', 'N', 1, '');

insert into dyn_template_action_mst(MODULE, UI_TYPE, ID, SEQ, TITLE, TYPE, DEFAULT_VALUE, OPTIONS_LOOKUP, LOOKUP_FIELD, IS_TITLE, LVL, SUB_TYPE)
values('occupation', 'detail', '/DynTemplate/Detail/Save', 1, ' BUTTON.SAVE', 'submitChangedButton', '', '', '', 'N', 1, '');





  CREATE TABLE "OCCUPATION_TRX" 
   (
   	"COMP_CODE" VARCHAR2(10 BYTE) NOT NULL ENABLE, 
	"OCCUPATION_CODE" VARCHAR2(20 BYTE) NOT NULL ENABLE, 
	"VERSION" NUMBER(*,0) NOT NULL ENABLE, 
	"LAUNCH_DATE" DATE, 
	"EXPIRY_DATE" DATE, 
	"BASE_VERSION" NUMBER(*,0), 
	"LAUNCH_STATUS" CHAR(1 BYTE) DEFAULT 'P', 
	"STATUS" CHAR(1 BYTE) DEFAULT 'A', 
	"CREATE_DATE" DATE, 
	"CREATE_BY" VARCHAR2(50 BYTE), 
	"UPD_DATE" DATE, 
	"UPD_BY" VARCHAR2(50 BYTE), 
	"TEMPLATE_CODE" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	 PRIMARY KEY ("VERSION", "OCCUPATION_CODE", "COMP_CODE")
)


   insert into dyn_trx_field_mst (id, key, module, mandatory, status, type) values('occupationCode', 'occupation_code', 'occupation', 'Y', 'A', 'text');



CREATE OR REPLACE FORCE EDITIONABLE VIEW "V_OCCUPATION_TRX" ("COMP_CODE", "OCCUPATION_CODE", "VERSION", "LAUNCH_DATE", "BASE_VERSION", "LAUNCH_STATUS", "STATUS", "CREATE_DATE", "CREATE_BY", "UPD_DATE", "UPD_BY", "LANG_CODE", "VERSION_DESC", "NAME_DESC", "TASK_STATUS", "TASK_STATUS_CODE") AS 
  select
       a."COMP_CODE",
	   a."OCCUPATION_CODE",
	   a."VERSION",
	   a."LAUNCH_DATE",
	   a."BASE_VERSION",
	   a."LAUNCH_STATUS",
	   a."STATUS",
	   a."CREATE_DATE",
	   a."CREATE_BY",
	   a."UPD_DATE",
	   a."UPD_BY",
	   a."LANG_CODE",
	   pn1.name_desc || ' v.' || a.version as version_desc,
       pn1.name_desc name_desc,
       vtl.status task_status,
	   vtl.status_code task_status_code
  from
     (select
             vpl.*,
             slm.lang_code
         from
              (
				select b.*
					from
						DYN_PUBBLISH_MST a, occupation_trx b
					where
						a.comp_code = b.comp_code and a.DOC_CODE = b.occupation_code and a.version = b.version
				union
			SELECT
				c.* FROM occupation_trx c WHERE  c.status = 'A'
				AND c.launch_status = 'P'
				AND c.version = 1
				AND NOT EXISTS (SELECT NULL
                               FROM   DYN_PUBBLISH_MST mst
                               WHERE  mst.comp_code = c.comp_code
                                      AND mst.doc_code = c.occupation_code)
                                      ) vpl,
              sys_lang_mst slm
         where vpl.status = 'A'
         and slm.status = 'A'
     ) a
  left join dyn_name_mst pn1 on
       a.comp_code = pn1.comp_code and
       a.occupation_code=pn1.doc_code and
       a.version=pn1.version and pn1.module='occupation' and
       pn1.lang_code = a.lang_code  and
       pn1.status = 'A'
  left join v_task_list vtl on
       a.comp_code = vtl.comp_code and
       a.occupation_code = vtl.item_code and
       a.version = vtl.ver_no and
       vtl.func_code = 'FN.OCCP'
;


insert into sys_name_mst (name_code, lang_code, name_desc) values ('OCCUPATION_CODE', 'en', 'OCCUPATION CODE');
insert into sys_name_mst (name_code, lang_code, name_desc) values ('OCCUPATION_NAME', 'en', 'OCCUPATION NAME');
insert into sys_name_mst (name_code, lang_code, name_desc) values ('MENU.OCCP', 'en', 'Occupation Loading');

