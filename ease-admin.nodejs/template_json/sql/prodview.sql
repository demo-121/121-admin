
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ICONFIG_SIT3"."V_PROD_TRX" ("COMP_CODE", "PROD_CODE", "VERSION", "PROD_LINE", "LAUNCH_DATE", "EXPIRY_DATE", "BASE_VERSION", "LAUNCH_STATUS", "STATUS", "CREATE_DATE", "CREATE_BY", "UPD_DATE", "UPD_BY", "LANG_CODE", "PLAN_TYPE", "VERSION_DESC", "NAME_DESC", "TASK_STATUS", "TASK_STATUS_CODE") AS 
  select
        a."COMP_CODE",
       a."PROD_CODE",
       a."VERSION",
       a."PROD_LINE",
       a."LAUNCH_DATE",
       a."EXPIRY_DATE",
       a."BASE_VERSION",
       a."LAUNCH_STATUS",
       a."STATUS",
       a."CREATE_DATE",
       a."CREATE_BY",
       a."UPD_DATE",
       a."UPD_BY",
       a."LANG_CODE",
       a."PLAN_TYPE",
       pn1.name_desc || ' v.' || a.version as version_desc,
       pn1.name_desc as name_desc,
       vtl.status task_status, vtl.status_code task_status_code
  from
     (select
             vpl.*,
             slm.lang_code
         from
              (select b.* from DYN_PUBBLISH_MST a, prod_trx b where a.comp_code = b.comp_code and a.DOC_CODE = b.PROD_CODE and a.version = b.version
union
SELECT c.* FROM prod_trx c WHERE  c.status = 'A'
               AND c.launch_status = 'P'
               AND c.version = 1
               AND NOT EXISTS (SELECT NULL
                               FROM   DYN_PUBBLISH_MST mst
                               WHERE  mst.comp_code = c.comp_code
                                      AND mst.doc_code = c.PROD_CODE)
                                      ) vpl,
              sys_lang_mst slm
         where vpl.status = 'A'
         and slm.status = 'A'
     ) a
  left join dyn_name_mst pn1 on
       a.comp_code = pn1.comp_code and
       a.PROD_CODE=pn1.doc_code and
       a.version=pn1.version and pn1.module='product' and
       pn1.lang_code = a.lang_code  and
       pn1.status = 'A'

  left join v_task_list vtl on
       a.comp_code = vtl.comp_code and
       a.PROD_CODE = vtl.item_code and
       a.version = vtl.ver_no and
       vtl.func_code = 'FN.PRO.PRODUCTS'
;
