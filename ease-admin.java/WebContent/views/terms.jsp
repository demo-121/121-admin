<%@page import="com.eab.common.Token"%>
<%@page import="com.eab.common.Log"%>
<%@page import="com.eab.common.Constants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	Log.info("{JSP} /views/login.jsp");
	String appkey = Constants.APP_STARTUP_KEY;
	String errorMessage = (String) request.getAttribute("errorMessage");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>${title}</title>

<script src="<%=request.getContextPath()%>/web/js/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/web/js/crypto/jsbn.js"></script>
<script src="<%=request.getContextPath()%>/web/js/crypto/jsbn2.js"></script>
<script src="<%=request.getContextPath()%>/web/js/crypto/prng4.js"></script>
<script src="<%=request.getContextPath()%>/web/js/crypto/rng.js"></script>
<script src="<%=request.getContextPath()%>/web/js/crypto/rsa.js"></script>
<script src="<%=request.getContextPath()%>/web/js/crypto/rsa2.js"></script>

<link href="<%=request.getContextPath()%>/web/js/mui/css/mui.min.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/web/css/style.css" rel="stylesheet" type="text/css" />
<script src="<%=request.getContextPath()%>/web/js/mui/js/mui.min.js"></script>

<script language="javascript">
function doSubmit() {
	var formObj = document.getElementById("termform");
	var decision = document.getElementById('decision').value;
	
	if (formObj && decision == "ACCEPT") {
		formObj.action = "<%=request.getContextPath()%>/Login/Terms/Accept";
	} else {
		formObj.action = "<%=request.getContextPath()%>/Login/Terms/Reject";
	}
	
	return true;
}
</script>

</head>
<body>
	<header class="mui-appbar mui--z1">
		<div class="mui-container">
			<table width="100%">
				<tr class="mui--appbar-height">
					<td class="mui--text-title">121 Configurator</td>
					<td align="right">
						<ul class="mui-list--inline mui--text-body2">
						<li><a href="#">About</a></li>
						</ul>
					</td>
				</tr>
			</table>
		</div>
	</header>
	<div id="content-wrapper" class="mui--text-center">
		<div class="mui--appbar-height"></div>
		<div class="mui-container-fluid">
			<div class="mui-row">
				<div class="mui-col-sm-10 mui-col-sm-offset-1">
					<div class="space-top"></div>
					<div class="mui-panel">
						<form id="termform" method="post" onSubmit="doSubmit();">
							<legend>Terms &amp; Conditions</legend>
							<p>
							God's in his heaven.  All's right with the world.
							</p>
							<button type="submit" class="mui-btn mui-btn--raised" onClick="document.getElementById('decision').value='ACCEPT';">Accept</button>
							&nbsp;
							<button type="submit" class="mui-btn mui-btn--raised" onClick="document.getElementById('decision').value='REJECT';">Decline</button>
							<input type="hidden" name="<%=Token.CsrfID(session) %>" value="<%=Token.CsrfToken(session) %>" >
							<input type="hidden" id="decision" value="ACCEPT" >
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<div class="mui-container mui--text-center">
			Made with by <a href="http://www.eabsystems.com">EAB R&D Team</a> | &copy;2016 EAB Systems Hong Kong Limited
		</div>
	</footer>
</body>
<script language="javascript">

</script>
</html>