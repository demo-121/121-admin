<%@page import="com.eab.common.Token"%>
<%@page import="com.eab.common.Log"%>
<%@page import="com.eab.common.Constants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	Log.info("{JSP} /ease/index.jsp");
	String appkey = Constants.APP_STARTUP_KEY;
	String errorMessage = (String) request.getAttribute("errorMessage");
	String contextPath = request.getContextPath();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="icon" type="image/png" href="<%=request.getContextPath()%>/web/img/favicon.png" />
<link rel="apple-touch-icon" href="<%=request.getContextPath()%>/web/img/favicon.png" />

<title>EASE Admin</title>

<!-- jQuery -->
<script src="<%=request.getContextPath()%>/web/js/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/web/js/jquery/jquery.csv.min.js"></script>

<!-- crypto -->
<script src="<%=request.getContextPath()%>/web/js/crypto/jsbn.js"></script>
<script src="<%=request.getContextPath()%>/web/js/crypto/jsbn2.js"></script>
<script src="<%=request.getContextPath()%>/web/js/crypto/prng4.js"></script>
<script src="<%=request.getContextPath()%>/web/js/crypto/rng.js"></script>
<script src="<%=request.getContextPath()%>/web/js/crypto/rsa.js"></script>
<script src="<%=request.getContextPath()%>/web/js/crypto/rsa2.js"></script>


<!-- HandsonTable -->
<script src="<%=request.getContextPath()%>/web/js/handsontable/dist/handsontable.full.js"></script>
<link rel="stylesheet" media="screen" href="<%=request.getContextPath()%>/web/js/handsontable/dist/handsontable.full.css">

<!-- PDF.js -->
<script src="<%=request.getContextPath()%>/web/js/pdfjs/build/pdf.js"></script>

<!-- Material UI -->
<script src="<%=request.getContextPath()%>/web/js/mui/js/mui.min.js"></script>
<link href="<%=request.getContextPath()%>/web/js/mui/css/mui.min.css" rel="stylesheet" type="text/css" />

<!-- main.css -->
<link href="<%=request.getContextPath()%>/web/main.css" rel="stylesheet" type="text/css" />

<!-- tinymce -->
<script src="<%=request.getContextPath()%>/web/js/tinymce/tinymce.min.js"></script>

<!-- beauty -->
<script src="<%=request.getContextPath()%>/web/js/js-beautify/beautify-css.js"></script>
<script src="<%=request.getContextPath()%>/web/js/js-beautify/beautify-html.js"></script>
<script src="<%=request.getContextPath()%>/web/js/js-beautify/beautify.js"></script>

<script language="javascript">
var ROOT_CONTEXT_PATH = "<%=request.getContextPath()%>";
var publicKey = "${publicKey}";
var IEversion = detectIE();

function encryptPassword(password) {
//	var rsa = new RSAKey();
//	rsa.setPublic(publicKey, '10001');
//	var res = rsa.encrypt(password);
//	return res;
	return password;
}

if (IEversion !== false) {
	if (IEversion >= 12) {
		window.onunload=function(event){
			if (!window.goSaml) { 
				window.location = "<%=request.getContextPath()%>/Logout"
			}
		};
	} else {
		window.onbeforeunload=function(event){
			if (!window.goSaml) { 
				window.location = "<%=request.getContextPath()%>/Logout"
			}
			event.returnValue = "Your browser session is clear.  Please close the browser.";
		};
	}
} else {
	window.onunload=function(event){
		if (!window.goSaml) { 
			window.location = "<%=request.getContextPath()%>/Logout"
		}
	};
}

function detectIE() {
	var ua = window.navigator.userAgent;
	// Test values; Uncomment to check result …
	// IE 10
	// ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
	// IE 11
	// ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
	// IE 12 / Spartan
	// ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
	// Edge (IE 12+)
	// ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

	var msie = ua.indexOf('MSIE ');
	if (msie > 0) {
		// IE 10 or older => return version number
		return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
	}

	var trident = ua.indexOf('Trident/');
	if (trident > 0) {
		// IE 11 => return version number
		var rv = ua.indexOf('rv:');
		return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
	}

	var edge = ua.indexOf('Edge/');
	if (edge > 0) {
		// Edge (IE 12+) => return version number
		return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
	}

	// other browser
	return false;
}

</script>

</head>
<body onbeforeunload="if (window.loggedIn) { return 'This action will end your current session. Do you wish to proceed.';}"
    >
	<section id="content"></section>
	<script src="<%=request.getContextPath()%>/web/app.js"></script>
</body>
</html>