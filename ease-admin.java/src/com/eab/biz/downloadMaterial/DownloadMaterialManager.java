package com.eab.biz.downloadMaterial;

import com.eab.common.*;
import com.eab.json.model.*;
import com.eab.model.bcp.AppIdMap;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.JsonObject;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.eab.dao.report.Report.getChannels;
import static com.eab.dao.report.Report.getPickerOptions;


public class DownloadMaterialManager {
	HttpServletRequest request;

	public DownloadMaterialManager(HttpServletRequest request) {
		this.request = request;
	}

	public Response getSession(JSONObject paramJSON, boolean isInit) {
		//UserPrincipalBean principal = Function.getPrincipal(request);
		JsonObject values = new JsonObject();
		Response resp = null;

		SimpleDateFormat sdf = new SimpleDateFormat(  Constants.DATETIME_PATTERN_AS_ADMIN_DATE);

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		String endDate  = sdf.format(cal.getTime());
		String startDate=  sdf.format(cal.getTime());
		String channel = "all";
		String dateType = "appStart";
		boolean isGenExcel = false;

		// values
		try {
			if (paramJSON != null  && !isInit){
				if(paramJSON.has("channel") && paramJSON.get("channel") instanceof String)
					channel = paramJSON.getString("channel");
				if(paramJSON.has("dateType") && paramJSON.get("dateType") instanceof String)
					dateType = paramJSON.getString("dateType");
				if(paramJSON.has("startDate") && paramJSON.get("startDate") instanceof String)
					startDate = paramJSON.getString("startDate");
				if(paramJSON.has("endDate") && paramJSON.get("endDate") instanceof String)
					endDate = paramJSON.getString("endDate");
				if(paramJSON.has("channel")  && paramJSON.has("startDate")&& paramJSON.has("endDate")&& paramJSON.has("dateType"))
					isGenExcel = true;
			}

			values.addProperty("channel", channel);
			values.addProperty("dateType", dateType);
			values.addProperty("startDate", startDate);
			values.addProperty("endDate", endDate);

			// AppBar - Title
			List<Option> downloadMaterialFunctions =  new ArrayList<>();
			Option optSession = new Option("Session", "session_value");
			Option optSubSession = new Option("Sub-Session", "sub_session_value");
			Option optMaterial = new Option("Material", "material");
			downloadMaterialFunctions.add(optSession);
			downloadMaterialFunctions.add(optSubSession);
			downloadMaterialFunctions.add(optMaterial);
			AppBarTitle appBarTitle = new AppBarTitle(null, AppBarTitle.Types.LABEL, "", "MENU.DOWNLOADMATERIAL", downloadMaterialFunctions, null);
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, null);
			Page pageObject = new Page("/DownloadMaterial", "");


			// create template
			List<Field> fields = new ArrayList<>();
			List<Option> dateTypeOptons = getPickerOptions("REPORT.DATETYPE");

			// Search fields
			Field startDateField = new Field("startDate", "datePicker", startDate, null, "REPORT.STARTDATE");
			Field endDateField = new Field("endDate", "datePicker", endDate, null, "REPORT.ENDDATE");

			List<Option> channelOptions = getChannels();
			for(int i = channelOptions.size()-1; i >= 0 ;i--){
				Option channelOption = channelOptions.get(i);
				if(channelOption.getValue().equals("GIAGENCY") || channelOption.getValue().equals("BANCA"))
					channelOptions.remove(i);
			}

			Field channelField = new Field("channel", "selectField", channel, channelOptions, "REPORT.CHANNEL");
			Field dateTypeField = new Field("dateType", "selectField", dateType, dateTypeOptons, "REPORT.DATETYPE");
			Field generateBtn = new Field("gen", "button", "BUTTON.GENERATE");

			fields.add(dateTypeField);
			fields.add(startDateField);
			fields.add(endDateField);
			fields.add(channelField);
			fields.add(generateBtn);

			// translation
			Template template = new Template("report", fields, "", "Reports");
			List<String> nameList = new ArrayList<String>();
			template.getNameCodes(nameList);
			appBar.getNameCodes(nameList);
			Map<String, String> nameMap = new HashMap<String, String>();
			nameMap = Function2.getTranslateMap(request, nameList);
			template.translate(nameMap);
			appBar.translate(nameMap);

			Content content = new Content(template, values, values);
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);

			/*
			JsonObject error = ReportUtil.dateRangeValidation(sdf.parse(startDate),sdf.parse(endDate), sdf.parse(sdf.format(new Date())));
			if(error != null && error.has("title"))
				((JsonObject)resp.getContent().getValues()).add("error", error);
			else if(isGenExcel){
				AllChannelReport allChannelReport = new AllChannelReport();
				String b64 = allChannelReport.genAllChannelReport(principal.getUserName(), startDate, endDate,dateType, channel, principal.getUserCode());
				if(b64!= null && !b64.isEmpty()){
					JsonObject report = new JsonObject();
					report.addProperty("file", "data:application/vnd.ms-excel;base64,"+b64);
					report.addProperty("fileName", "All_Channel_Report.xlsx");
					((JsonObject)resp.getContent().getValues()).add("report", report);
				}
			}
			*/
			// get template
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;
	}

}
