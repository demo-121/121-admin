package com.eab.biz.bcp;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.biz.bcp.BcpRequest;
import com.eab.biz.common.CommonManager;
import com.eab.common.*;
import com.eab.couchbase.CBServer;
import com.eab.dao.common.Functions;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DataType;

import org.json.JSONObject;

import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.SearchCondition;
import com.eab.json.model.Template;
import com.eab.model.MenuList;
import com.eab.model.bcp.BcpBean;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.JsonObject;

import static com.eab.dao.report.Report.getPickerOptions;
import static com.eab.dao.report.Report.getChannels;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class BcpDetail extends BcpCommon {

	public BcpDetail(HttpServletRequest request) {
		super(request);
	}

	public Response getBCPDetail(HttpServletRequest request, String type, String policyNumber) {
		Response resp 		= null;
		
		
		if(type != null && isValidPolicyNumberFormat(policyNumber)) {
			try {
				Map<String,Object> appbardata = new HashMap<String,Object>();
				
				Map<String,Object> level1_obj = new HashMap<String,Object>();
				level1_obj.put("id", "/BCProcessing");
				level1_obj.put("types", AppBarTitle.Types.LABEL);
				level1_obj.put("value", "/BCProcessing/" + type);
				level1_obj.put("primary", "BCP." + type);
				appbardata.put("level1", level1_obj);
				
				Map<String,Object> level2_obj = new HashMap<String,Object>();
				level2_obj.put("id", "/BCProcessing/Detail");
				level2_obj.put("types", AppBarTitle.Types.LABEL);
				level2_obj.put("value", "/BCProcessing/Detail/" + type);
				level2_obj.put("title", "BCP.DETAIL." + type);
				level2_obj.put("pol_num", " (" + policyNumber + ")");
				appbardata.put("level2", level2_obj);
				AppBar appBar = getAppBar(appbardata);
				
//				AppBar appBar = getAppBar("/BCProcessing", AppBarTitle.Types.LABEL, "BCP.DETAIL." + type, "/BCProcessing/Detail/" + type, "level2");
				JsonParser parser = new JsonParser();
				JsonObject pagevalues = parser.parse("{\"type\":" + type + ", \"policyNumber\":" + policyNumber +"}").getAsJsonObject();
				Page pageObject 	= new Page("/BCProcessing/Detail", "BCPDetail", pagevalues);
				
				//Prepare Table template
				JSONObject template_obj = new JSONObject();
				
				// List id, List name, List type, List width
				template_obj.put("List1", Arrays.asList("seq", "SEQ", "text", "auto"));
				template_obj.put("List2", Arrays.asList("status", "STATUS", "text", "150px", "tooltipId"));
				template_obj.put("List3", Arrays.asList("updateBy", "UPD_BY", "text", "210px"));
				template_obj.put("List4", Arrays.asList("upd_date", "LAST_UPDATED", "text", "200px"));
				
				List<Field> tableTemplate = getTableTemplate(template_obj);
				
				List<Field> fields 	= new ArrayList<>();
				
				Template template = new Template("action", tableTemplate, "table", "BCP.FUNC.ACTION");
				template.setDetailItems(fields);

				//prepare table values			
				
				BcpRequest br = new BcpRequest(request);
				List<Map<String,Object>> actionStatusList = br.getHistoryLog(policyNumber, type);
				

				// translation
				List<String> nameList = new ArrayList<String>();
				template.getNameCodes(nameList);
				appBar.getNameCodes(nameList);
				
				String resultStr = "";
				BcpBean values = new BcpBean(actionStatusList, resultStr);
				Content content = new Content(template, values, values);

				String tokenID 	= Token.CsrfID(request.getSession());
				String tokenKey = Token.CsrfToken(request.getSession());

				resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, 
						pageObject, tokenID, tokenKey, appBar, content);

			} catch (Exception e) {
				Log.error(e);
				resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
			}
		}
		return resp;
	}
	

	
	
	
	
}
