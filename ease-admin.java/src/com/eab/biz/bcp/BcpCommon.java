package com.eab.biz.bcp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.biz.common.CommonManager;
import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.common.Functions;
import com.eab.database.jdbc.DBManager;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Field;
import com.eab.json.model.SearchCondition;
import com.eab.model.profile.UserPrincipalBean;

public class BcpCommon {
	Connection conn = null;
	StringBuffer sb = new StringBuffer();
	DBManager dbm = new DBManager();
	ResultSet rs = null;
	
	HttpServletRequest request;
	HttpSession session;
	UserPrincipalBean principal;
	Language langObj;

	public BcpCommon(HttpServletRequest request) {
		this.request = request;
		this.session = request.getSession();
		this.principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		this.langObj = new Language(principal.getUITranslation());
	}
	
	public JSONArray getTooltipValues(ResultSet result_item) throws SQLException{
		result_item.beforeFirst();
		JSONObject tooltip_field = new JSONObject();
		JSONArray tooltip_arr = new JSONArray();
		String batch_num = "";
		Boolean init = true;
		 String optional = "{\"className\": \"testing\", \"style\": { \"fontSize\" : \"18px\" } }";
//		String optional = "";
		
		// default values
		tooltip_field.put("Line1", Arrays.asList("p", optional, "eApp - success") );
		tooltip_field.put("Line2", Arrays.asList("p", optional, "FNA - success") );
		tooltip_field.put("Line3", Arrays.asList("p", optional, "eBI - success") );
		tooltip_field.put("Line4", Arrays.asList("p", optional, "Upload document - success") );
		
        if(result_item != null) {
        	while(result_item.next()) {
        		if(init) {
        			batch_num = result_item.getString("TAKEN_BY_BATCH_NO");
        			init = false;
        		}
        		if(batch_num.equals(result_item.getString("TAKEN_BY_BATCH_NO"))) {
        			String doc_name = result_item.getString("DOC_NAME");
            		if(doc_name.equals("appPdf")) {
            			tooltip_field.put("Line1", Arrays.asList("p", optional, "eApp - Failed") );
            		}
            		else if(doc_name.equals("fnaReport")) {
            			tooltip_field.put("Line2", Arrays.asList("p", optional, "FNA - Failed") );
            		}
            		else if(doc_name.equals("proposal")) {
            			tooltip_field.put("Line3", Arrays.asList("p", optional, "eBI - Failed") );
            		}
            		else if(doc_name != null && !doc_name.isEmpty()) {
            			tooltip_field.put("Line4", Arrays.asList("p", optional, "Upload document - Failed") );
            		}
        		}
        		
        	}
        		
        }
        
        // ordering 
		tooltip_arr.put(tooltip_field.getJSONArray("Line1"));
		tooltip_arr.put(tooltip_field.getJSONArray("Line2"));
		tooltip_arr.put(tooltip_field.getJSONArray("Line3"));
		tooltip_arr.put(tooltip_field.getJSONArray("Line4"));
		return tooltip_arr;
	}
	
	// create a Object that use to perform chain-action
	public Map<String, Object> actionChain(JSONObject value) {
		Map<String, Object> result = new HashMap<String, Object>();
		Iterator<?> keys = value.keys();
		while(keys.hasNext()) {
			String key = (String) keys.next();
			if(key.toUpperCase().equals("CALLSERVER")) {
				// should be boolean
				result.put("callServer", value.getBoolean(key));
			}else if(key.toUpperCase().equals("PATH")) {
				// should be string
				result.put("path", value.getString(key));
			}else if(key.toUpperCase().equals("DATA")) {
				// should be JSONObject
				result.put("data", value.getJSONObject(key));
			}
		}
		return result;
	}
	
	protected List<Field> getTableTemplate(JSONObject fieldlist) throws Exception {
		List<Field> template = new ArrayList<>();
		if(fieldlist != null && fieldlist instanceof JSONObject) {
			Iterator<?> keys = fieldlist.keys();
			int seq = 0;
			while( keys.hasNext() ) {
				Field field1 = null;
				
			    String key = (String)keys.next();
			    JSONArray arr= fieldlist.getJSONArray(key);
			    if(arr.length() == 4) {
				    field1 = new Field(arr.getString(0), langObj.getNameDesc(arr.getString(1)), arr.getString(2), seq + 1, seq + 1,  arr.getString(3));
			    }

			    if(arr.length() == 5) {
				    field1 = new Field(arr.getString(0), langObj.getNameDesc(arr.getString(1)), arr.getString(2), seq + 1, seq + 1,  arr.getString(3), arr.getString(4));
			    }
			    seq ++;
			    template.add(field1);
			}
		}
		
	    return template;
	}
	
	protected Map<String,Object> createTooltip(JSONObject tooltip_list) throws Exception {
		Map<String,Object> tooltip = new HashMap<String,Object>();
		
		if(tooltip_list != null && tooltip_list instanceof JSONObject) {
			Iterator<?> keys = tooltip_list.keys();
			int seq = 0;
			while( keys.hasNext() ) {
				Map<String,Object> tooltip_level2 = new HashMap<String,Object>();
			    String key = (String)keys.next();
			    JSONArray arr= tooltip_list.getJSONArray(key);
			    tooltip_level2.put("eleType", arr.getString(0));
			    tooltip_level2.put("optional", arr.getString(1));
			    tooltip_level2.put("text", arr.getString(2));
			    tooltip.put(String.valueOf(seq), tooltip_level2);
			    seq++;
		    }

		}
		
		
//		Map<String,Object> tooltip_level2 = new HashMap<String,Object>();
//		tooltip_level2.put("eleType", "p");
//		tooltip_level2.put("text", "testing");
//		tooltip.put("0", tooltip_level2);
//		Map<String,Object> tooltip_level22 = new HashMap<String,Object>();
//		tooltip_level22.put("eleType", "p");
//		tooltip_level22.put("text", "testing222222");
//		tooltip.put("1", tooltip_level22);
		
		return tooltip;
	}
	
	protected Map<String,Object> createTooltip(JSONArray tooltip_list) throws Exception {
		Map<String,Object> tooltip = new HashMap<String,Object>();
		
		if(tooltip_list != null && tooltip_list instanceof JSONArray) {
//			Iterator<?> keys = tooltip_list.keys();
//			int seq = 0;
//			while( keys.hasNext() ) {
//				Map<String,Object> tooltip_level2 = new HashMap<String,Object>();
//			    String key = (String)keys.next();
//			    JSONArray arr= tooltip_list.getJSONArray(key);
//			    tooltip_level2.put("eleType", arr.getString(0));
//			    tooltip_level2.put("optional", arr.getString(1));
//			    tooltip_level2.put("text", arr.getString(2));
//			    tooltip.put(String.valueOf(seq), tooltip_level2);
//			    seq++;
//		    }
			int seq = 0;
			for(int i=0; i<tooltip_list.length(); i++) {
				Map<String,Object> tooltip_level2 = new HashMap<String,Object>();
				JSONArray tooltip_arr = tooltip_list.getJSONArray(i);
			    tooltip_level2.put("eleType", tooltip_arr.getString(0));
			    tooltip_level2.put("optional", tooltip_arr.getString(1));
			    tooltip_level2.put("text", tooltip_arr.getString(2));
			    tooltip.put(String.valueOf(seq), tooltip_level2);
				seq++;
			}
			

		}
		
		
//		Map<String,Object> tooltip_level2 = new HashMap<String,Object>();
//		tooltip_level2.put("eleType", "p");
//		tooltip_level2.put("text", "testing");
//		tooltip.put("0", tooltip_level2);
//		Map<String,Object> tooltip_level22 = new HashMap<String,Object>();
//		tooltip_level22.put("eleType", "p");
//		tooltip_level22.put("text", "testing222222");
//		tooltip.put("1", tooltip_level22);
		
		return tooltip;
	}
	
	protected AppBar getAppBar(Map<String, Object> appbardata) throws Exception {
		Object primary = null;
		Object secondary = null;
		Object tertiary = null;
		List<Field> appBarAction = new ArrayList<>();
		AppBarTitle appBarTitle = null;
		String level = "1";
		if(appbardata.get("level1") != null) {
			Map<String,Object> level1 = (Map<String, Object>) appbardata.get("level1");
//			primary = new Field(level1.get("id").toString(), level1.get("types").toString(), level1.get("value").toString(), null, langObj.getNameDesc(level1.get("title").toString()));
			
			
			appBarTitle = new AppBarTitle(level1.get("id").toString(), level1.get("types").toString(), level1.get("value").toString(), level1.get("primary").toString(), null, null);
			if(level1.get("appbar_action") != null) {
				Map<String,Object> appbar_action = (Map<String, Object>) level1.get("appbar_action");
				appBarAction.add(new Field(appbar_action.get("id").toString(), appbar_action.get("type").toString(), appbar_action.get("value").toString(), appbar_action.get("title").toString(), null));
			}
		}
		if(appbardata.get("level2") != null) {
			level = "2";
			Map<String,Object> level2 = (Map<String, Object>) appbardata.get("level2");
			secondary = new Field(level2.get("id").toString(), level2.get("types").toString(), level2.get("value").toString(), null, langObj.getNameDesc(level2.get("title").toString()) + level2.get("pol_num").toString());
		}
		if(appbardata.get("level3") != null) {
			level = "3";
			Map<String,Object> level3 = (Map<String, Object>) appbardata.get("level3");
			tertiary = new Field(level3.get("id").toString(), level3.get("types").toString(), level3.get("value").toString(), null, langObj.getNameDesc(level3.get("title").toString()) + level3.get("pol_num").toString());
		}
		
		
		if(level == "2") {
			appBarTitle = new AppBarTitle(AppBarTitle.Types.LEVEL_TWO, null, null, secondary);
		}
		if(level == "3") {
			appBarTitle = new AppBarTitle(AppBarTitle.Types.LEVEL_THREE, null, null, secondary, tertiary);
		}
		//AppBar
		
		//AppBar Actions
//		List<Field> appBarAction1 = new ArrayList<>();
//		appBarAction1.add(new Field("searchString", "searchButton",langObj.getNameDesc("BCP.FUNC.POLICY_NUMBERS").toString(), langObj.getNameDesc("SEARCH").toString(), null));
		
		List<List<Field>> appBarActionsCollection = new ArrayList<>();
		appBarActionsCollection.add(appBarAction);
		
		AppBar appBar = new AppBar(appBarTitle, appBarActionsCollection);
				
//				SearchCondition cond = SearchCondition.RetrieveSearchCondition(request);
//				appBar.setValues(cond);
				
		return appBar;
	}
	
	protected boolean isValidPolicyNumberFormat(String policyNumStr) {
		
		String curPolicyNum = policyNumStr.trim();
		
		if (curPolicyNum == null){return false;}
		if (curPolicyNum.length() == 0){return false;}
		
		if (curPolicyNum.length() != 11){return false;}
		
		String[] parts = curPolicyNum.split("-");
		if (parts.length != 2){return false;}
		if (parts[0].length() != 3){return false;}
		if (parts[1].length() != 7){return false;}
		
		try {Integer.parseInt(parts[0]);}
		catch(Exception e){ return false;}
		
		try {Integer.parseInt(parts[1]);}
		catch(Exception e){ return false;}
		
		return true;
	}
}
