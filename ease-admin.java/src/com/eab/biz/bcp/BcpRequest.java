package com.eab.biz.bcp;



import java.sql.ResultSet;

import com.eab.common.Constants;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.eab.common.EnvVariable;
import com.eab.common.Log;
import com.eab.couchbase.CBServer;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DataType;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


import org.json.JSONArray;
import org.json.JSONObject;

public class BcpRequest extends BcpCommon{

	public BcpRequest(HttpServletRequest request) {
		super(request);
	}
	
	
	private final String USER_AGENT = "Mozilla/5.0";

	
	private String selectStatusStatement(String policyNumber, String type) throws Exception {
		type = type.toUpperCase();
		String sql = "";
		switch(type) {
			case "RLS":
				sql = "select * from ("
						+ " select t2.POL_NUM, to_char( cast(t1.CREATE_DATE as timestamp) at time zone 'UTC', 'yyyy-MM-dd\"T\"hh24:mi:ss\"+00:00\"') as CREATE_DATE, t1.EAPP_NO, t1.CREATE_BY_BATCH_NO, t1.RLS_SUBMISSION_STATUS, t1.CREATED_BY"
						+ " FROM BATCH_SUBMISSION_REQUEST t1 join EAPP_MST t2"
						+ " ON (t2.EAPP_NO = t1.EAPP_NO) WHERE t2.POL_NUM IN (" + policyNumber + ")"
						+ " AND t1.RLS_SUBMISSION_STATUS != '(null)'"
						+ " ORDER BY CREATE_BY_BATCH_NO DESC"
						+ " ) WHERE ROWNUM = 1";

//				sql = "SELECT POL_NUM, STATUS, CREATE_DT" 
//				+ " FROM API_RLS_REC_APP_TRX" 
//				+ " WHERE POL_NUM in (" + policyNumber + ")" 
//				+ " ORDER BY CREATE_DT DESC";
				break;
			
			case "WFI":
				sql = "select * from ("
						+ " select t2.POL_NUM, to_char( cast(t1.CREATE_DATE as timestamp) at time zone 'UTC', 'yyyy-MM-dd\"T\"hh24:mi:ss\"+00:00\"') as CREATE_DATE, t1.EAPP_NO, t1.CREATE_BY_BATCH_NO, t1.WFI_SUBMISSION_STATUS, t1.CREATED_BY"
						+ " FROM BATCH_SUBMISSION_REQUEST t1 join EAPP_MST t2"
						+ " ON (t2.EAPP_NO = t1.EAPP_NO) WHERE t2.POL_NUM IN (" + policyNumber + ")"
						+ " AND t1.WFI_SUBMISSION_STATUS != '(null)'"
						+ " ORDER BY CREATE_BY_BATCH_NO DESC"
						+ " ) WHERE ROWNUM = 1";
				
//				sql = "SELECT POL_NUM, STATUS, CREATE_DT"
//				+ " FROM API_WFI_REC_DOC_TRX"
//				+ " WHERE POL_NUM in (" + policyNumber + ")"
//				+ " ORDER BY CREATE_DT DESC";
				break;
			
			case "PYMT":
				sql = "SELECT POL_NUM, STATUS, to_char( cast(CREATE_DT as timestamp) at time zone 'UTC', 'yyyy-MM-dd\"T\"hh24:mi:ss\"+00:00\"') as CREATE_DT, CREATED_BY" 
				+ " FROM API_RLS_REC_PYMT_TRX" 
				+ " WHERE POL_NUM in (" + policyNumber + ")"
				+ " ORDER BY CREATE_DT DESC";
				break;
		}
		
		return sql;
	}
	
	private String selectError(String policyNumber, String type, String taken_by_batch_no, String created_by) throws Exception {
		type = type.toUpperCase();
		String sql = "";
		switch(type) {
			case "WFI":
				if(taken_by_batch_no !=null) {
					sql = "SELECT * FROM WFI_EIP_EXCEPTION WHERE POL_NUM in (" + policyNumber + ") AND SUBMISSION_TYPE = '" + type + "' AND CREATE_BY_BATCH_NO in (" + Integer.parseInt(taken_by_batch_no) + ")";
				} else {
					sql = "SELECT * FROM WFI_EIP_EXCEPTION ERROR_LOG JOIN ("
							+ " SELECT * FROM BATCH_SUBMISSION_REQUEST T1 JOIN EAPP_MST T2 ON (T2.EAPP_NO = T1.EAPP_NO)" 
							+ " WHERE T2.POL_NUM IN (" + policyNumber + ") AND T1.WFI_SUBMISSION_STATUS != '(null)' ORDER BY CREATE_BY_BATCH_NO DESC) BATCH_LOG" 
							+ " ON (ERROR_LOG.CREATE_BY_BATCH_NO = BATCH_LOG.TAKEN_BY_BATCH_NO) "
							+ " WHERE ERROR_LOG.POL_NUM IN (" + policyNumber + ") AND ERROR_LOG.SUBMISSION_TYPE = '" + type
//							+ " ON (ERROR_LOG.CREATE_BY_BATCH_NO = BATCH_LOG.TAKEN_BY_BATCH_NO) WHERE ERROR_LOG.POL_NUM IN (" + policyNumber + ") AND ERROR_LOG.SUBMISSION_TYPE = '" + type
							+ "' ORDER BY CREATE_DATE DESC";
				}
//				sql = "SELECT * FROM WFI_EIP_EXCEPTION ERROR_LOG JOIN ("
//						+ " SELECT * FROM BATCH_SUBMISSION_REQUEST T1 JOIN EAPP_MST T2 ON (T2.EAPP_NO = T1.EAPP_NO)" 
//						+ " WHERE T2.POL_NUM IN (" + policyNumber + ") AND T1.WFI_SUBMISSION_STATUS != '(null)' ORDER BY CREATE_BY_BATCH_NO DESC) BATCH_LOG" 
//						+ " ON (ERROR_LOG.CREATE_BY_BATCH_NO = BATCH_LOG.TAKEN_BY_BATCH_NO) "
//						+ " WHERE ERROR_LOG.POL_NUM IN (" + policyNumber + ") AND ERROR_LOG.SUBMISSION_TYPE = '" + type + "' AND ERROR_LOG.CREATE_BY_BATCH_NO = '" + taken_by_batch_no
//						+ "' ORDER BY CREATE_DATE DESC";
				return sql;
			case "RLS":
					sql = "SELECT * FROM WFI_EIP_EXCEPTION WHERE POL_NUM in (" + policyNumber + ") AND SUBMISSION_TYPE = '" + type + "' AND CREATE_BY_BATCH_NO in (" + Integer.parseInt(taken_by_batch_no) + ")";
				return sql;
			case "PYMT":
				if(created_by.toUpperCase().equals("SYSTEM")) {
					sql = "SELECT *" 
							+ " FROM API_RLS_REC_PYMT_TRX" 
							+ " WHERE POL_NUM in (" + policyNumber + ") AND STATUS = 'F' AND (CREATED_BY = 'SYSTEM' OR CREATED_BY is null)"
							+ " ORDER BY CREATE_DT DESC";
				} else {
					sql = "SELECT RESPONSE, POL_NUM" 
							+ " FROM API_RLS_REC_PYMT_TRX" 
							+ " WHERE POL_NUM in (" + policyNumber + ") AND STATUS = 'F' AND CREATED_BY = '" + created_by + "'"
							+ " ORDER BY CREATE_DT DESC";
				}
				
			return sql;
	}
		
		return sql;
	}
	
public List<Map<String, Object>> getErrorHistoryLog(String policyNumber, String type, String taken_by_batch_no, String created_by) throws SQLException {
	Log.info("BcpRequest.getErrorHistoryLog...  PolicyNumber: " + policyNumber + ",  Type: " +type + ", TakenByBatchNo: " + taken_by_batch_no);
		
		String sql;
		String batch_num = null;
		Boolean init = true;
		
		List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
		
		try {
			this.conn = DBAccess.getConnectionApi();
			this.conn.setAutoCommit(false);
			
			String pPolicyNumber = this.dbm.param(policyNumber, DataType.TEXT);
			if(type.toUpperCase().equals("WFI") && taken_by_batch_no != null) {
//				this.dbm.param(policyNumber, DataType.TEXT);
			}
	    	
			sql = selectError(pPolicyNumber, type, taken_by_batch_no, created_by);		
			this.rs = this.dbm.select(sql, this.conn);
			
			int i = 0;
			
			
				if(this.rs != null) {
					while(this.rs.next()){
						

						Map<String,Object> Row = new HashMap<String,Object>();
		            	Row.put("pol_num", this.rs.getString("POL_NUM"));
						if(type.toUpperCase().equals("PYMT")) {
							Row.put("title", "Failed record for \" Record Payment (DCR)\"");
							Row.put("msg", this.rs.getString("RESPONSE"));
							Row.put("type", "ListItem");
							Row.put("listSeq", 1);
				        	result.add(Row);
						} else {
							if(init) {
								batch_num = this.rs.getString("CREATE_BY_BATCH_NO");
								init = false;
							}
							
							if(batch_num.equals(this.rs.getString("CREATE_BY_BATCH_NO"))) {
								if(type.equals("WFI")) {
		//							Row.put("id", type);
									String doc_name = this.rs.getString("DOC_NAME");
									if(doc_name.equals("appPdf")) {
										doc_name = "eApp";
										Row.put("listSeq", 1);
					        		}
					        		else if(doc_name.equals("fnaReport")) {
					        			doc_name = "FNA";
										Row.put("listSeq", 2);
					        		}
					        		else if(doc_name.equals("proposal")) {
					        			doc_name = "eBI";
										Row.put("listSeq", 3);
					        		}
					        		else if(doc_name != null && !doc_name.isEmpty()) {
					        			doc_name = "Upload document - " + doc_name;
										Row.put("listSeq", 4 + i);
										i++;
					        		}
											
									Row.put("title", "Failed record for \" " + doc_name + "\"");
									Row.put("type", "ListItem");
									Row.put("msg", this.rs.getString("EXCP_DESC"));
						        	result.add(Row);
								}else if(type.equals("RLS")) {
									// RLS
									
									if(this.rs != null){
//										Row.put("id", type);
										
											Row.put("title", "Failed record for \" Submission (RLS)\"");
											Row.put("msg", this.rs.getString("EXCP_DESC"));
										
										Row.put("type", "ListItem");
										Row.put("listSeq", 1);
							        	result.add(Row);
							        } 
								}
							}
						}
		        } 
			} 
			
	        
		} catch (Exception e) {
			this.sb.delete(0, this.sb.length());
			this.sb.append("BcpRequest.getErrorMsg(); error - ").append(e);
			
			Log.error(e);
			result.clear();
		} finally {
//			this.dbm = null;
			if (this.conn != null && !this.conn.isClosed()) this.conn.close();
			this.conn = null;
		}
		
		return result;
	}
	
	private String selectHistoryLog(String policyNumber, String type) throws Exception {
		type = type.toUpperCase();
		String sql = "";
		switch(type) {
			case "RLS":
				sql = " select  to_char( cast(t1.CREATE_DATE as timestamp) at time zone 'UTC', 'yyyy-MM-dd\"T\"hh24:mi:ss\"+00:00\"') as CREATE_DATE, t1.RLS_SUBMISSION_STATUS, t1.CREATED_BY, t1.TAKEN_BY_BATCH_NO, t2.POL_NUM"
						+ " FROM BATCH_SUBMISSION_REQUEST t1 join EAPP_MST t2"
						+ " ON (t2.EAPP_NO = t1.EAPP_NO) WHERE t2.POL_NUM IN (" + policyNumber + ")"
						+ " AND t1.RLS_SUBMISSION_STATUS != '(null)'"
						+ " ORDER BY CREATE_BY_BATCH_NO DESC";
//				sql = "SELECT POL_NUM, STATUS, CREATE_DT" 
//				+ " FROM API_RLS_REC_APP_TRX" 
//				+ " WHERE POL_NUM in (" + policyNumber + ")" 
//				+ " ORDER BY CREATE_DT DESC";
				break;
			
			case "WFI":
//				sql = "SELECT * FROM WFI_EIP_EXCEPTION ERROR_LOG JOIN (SELECT * FROM BATCH_SUBMISSION_REQUEST T1 JOIN EAPP_MST T2 ON (T2.EAPP_NO = T1.EAPP_NO)" + 
//						"WHERE T2.POL_NUM IN (" + policyNumber + ") AND T1.WFI_SUBMISSION_STATUS != '(null)' ORDER BY CREATE_BY_BATCH_NO DESC) BATCH_LOG" + 
//						"ON (ERROR_LOG.CREATE_BY_BATCH_NO = BATCH_LOG.TAKEN_BY_BATCH_NO) WHERE ERROR_LOG.POL_NUM IN (" + policyNumber + ") AND" + 
//						"ERROR_LOG.SUBMISSION_TYPE = 'WFI' ORDER BY CREATE_DATE DESC";
				
				sql = " select  to_char( cast(t1.CREATE_DATE as timestamp) at time zone 'UTC', 'yyyy-MM-dd\"T\"hh24:mi:ss\"+00:00\"') as CREATE_DATE, t1.WFI_SUBMISSION_STATUS, t1.CREATED_BY, t1.TAKEN_BY_BATCH_NO, t2.POL_NUM"
						+ " FROM BATCH_SUBMISSION_REQUEST t1 join EAPP_MST t2"
						+ " ON (t2.EAPP_NO = t1.EAPP_NO) WHERE t2.POL_NUM IN (" + policyNumber + ")"
						+ " AND t1.WFI_SUBMISSION_STATUS != '(null)'"
						+ " ORDER BY CREATE_BY_BATCH_NO DESC";
				
//				sql = "SELECT POL_NUM, STATUS, CREATE_DT"
//				+ " FROM API_WFI_REC_DOC_TRX"
//				+ " WHERE POL_NUM in (" + policyNumber + ")"
//				+ " ORDER BY CREATE_DT DESC";
				break;
			
			case "PYMT":
				sql = "SELECT POL_NUM, STATUS, to_char( cast(CREATE_DT as timestamp) at time zone 'UTC', 'yyyy-MM-dd\"T\"hh24:mi:ss\"+00:00\"') as CREATE_DT, CREATED_BY, POL_NUM" 
				+ " FROM API_RLS_REC_PYMT_TRX" 
				+ " WHERE POL_NUM in (" + policyNumber + ")"
				+ " ORDER BY CREATE_DT DESC";
				break;
		}
		
		return sql;
	}
	
	public List<Map<String, Object>> getHistoryLog(String policyNumber, String type) throws SQLException {
		Log.info("BcpRequest.getHistoryLog...  PolicyNumber: " + policyNumber + ",  Type: " +type );
		List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
		List<ResultSet> result_list = new ArrayList<ResultSet>();
		List<String> sql_list = new ArrayList<String>();
		String sql ="";
		String sql2 = "";
		try {
			this.conn = DBAccess.getConnectionApi();
			this.conn.setAutoCommit(false);
			
			String pPolicyNumber = this.dbm.param(policyNumber, DataType.TEXT);
	    	
	    	
			sql = selectHistoryLog(pPolicyNumber, type);	
			
			if(type.equals("WFI")) {
				sql2 = selectError(pPolicyNumber, type , null, null);
	
			} 
			
			sql_list.add(sql);
			sql_list.add(sql2);
			result_list = this.dbm.selectMultipleQuery(sql_list, this.conn);
			int i = 0;
			if(result_list != null) {
//				for(int i=0; i<result_list.size(); i++) {
					ResultSet result_item = result_list.get(0);
					while(result_item.next()) {
						Map<String,Object> Row = new HashMap<String,Object>();

		            	Row.put("pol_num", result_item.getString("POL_NUM"));
		        		Row.put("seq", ++i);
		        		Row.put("type", type);
		        		String status = null;
		        		if(type.toUpperCase().equals("PYMT")) {
		        			status = result_item.getString("STATUS");
		        		} else {
		        			status = result_item.getString((type + "_SUBMISSION_STATUS").toString());
			        		Row.put("taken_by_batch_no", result_item.getString("TAKEN_BY_BATCH_NO"));
		        		}
		        		Boolean tooltip = false;
		        		if(status.equals("C")) {
		        			status = "Success";
		        		} else if(status.equals("R")) {
//		        			status = "Ready";
		        			status = "Processing";
		        		} else if(status.equals("W")) {
		        			status = "Waiting";
		        		}  else if(status.equals("P")) {
		        			status = "Pending";
		        		} else if(status.equals("F")) {
		        			tooltip = true;
		        			status = "Failed";
		        		}
		        		Row.put("status", status );
		        		String upd_by = "";
		        		try {
		        			upd_by = result_item.getString("CREATED_BY");
		        			
		        			if(upd_by == null) {
			        			upd_by = "SYSTEM";
			        		}
		        		} catch( Exception e) {
		        			upd_by = "SYSTEM";
		        		}
		        		
		        		
		        		
		        		Row.put("updateBy", upd_by);
		        		
		        		String create_date = null;
		        		if(type.toUpperCase().equals("PYMT")) {
//		        			createdate = ZonedDateTime.parse(result_item.getString("CREATE_DT"), DateTimeFormatter.ISO_DATE_TIME);
		        			create_date = timeFormater(result_item.getString("CREATE_DT"));
		        		} else {
		        			create_date = timeFormater(result_item.getString("CREATE_DATE"));
		        		}
//		        		Row.put("upd_date", result_item.getString("CREATE_DATE"));
		        		Row.put("upd_date", create_date);
		        		if(type.equals("WFI") && tooltip) {
		        			Row.put("tooltipId", createTooltip(getTooltipValues(result_list.get(1))));
		        		}
		        		result.add(Row);
					}
//				}
				
			}

	        	
		} catch (Exception e) {
			this.sb.delete(0, this.sb.length());
			this.sb.append("BcpDetail.getHistoryLog(); error - ").append(e);
			
			Log.error(e);
			result.clear();
		} finally {
//			this.dbm = null;
			if (this.conn != null && !this.conn.isClosed()) this.conn.close();
			this.conn = null;
		}
		return result;
	}
	
	public String timeFormater (String date) {
		ZonedDateTime datetime = ZonedDateTime.parse(date, DateTimeFormatter.ISO_DATE_TIME);
		LocalDateTime createDate = LocalDateTime.ofInstant(datetime.toInstant(), ZoneId.of(Constants.ZONE_ID));
		return createDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
	}
	

	

	public List<Map<String,Object>> getApplicationApiStatus(String policyNumber) throws SQLException {
		Log.info("BcpRequest.getApplicationApiStatus...  PolicyNumber: " + policyNumber);
		
		List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
		
		
		boolean isPolicyNumValid = false;

        String gatewayURL = EnvVariable.get("GATEWAY_URL");
        String gatewayPort = EnvVariable.get("GATEWAY_PORT");
        String 	gatewayDBName = EnvVariable.get("GATEWAY_DBNAME");
        String gatewayUser = EnvVariable.get("GATEWAY_USER");
        String gatewayPW = EnvVariable.get("GATEWAY_PW");
		
        Log.info("BcpRequest.getApplicationApiStatus(); start");
        
		if (policyNumber == null || policyNumber.length() == 0) {
			return result;
		}
		
		try {
			//Oracle - DB connect
			this.conn = DBAccess.getConnectionApi();
			this.conn.setAutoCommit(false);
			
			//Couchbase - get policy Doc
	        CBServer cb = new CBServer(gatewayURL,gatewayPort,gatewayDBName,gatewayUser,gatewayPW);
	        JSONObject policyNumDoc = cb.getDoc(policyNumber);

	        if (policyNumDoc == null) {
	        	//no policy object
	        	JSONObject appList = cb.getDoc("_design/main/_view/appByPolNum?key=[\"01\",\""+policyNumber+"\"]");
	        	JSONArray appListRows = appList.getJSONArray("rows");

	        	if (appListRows != null && appListRows.length() > 0) {
	        		JSONObject appRow = (JSONObject) appListRows.get(0);
	        		JSONObject appDoc = (JSONObject) appRow.get("value");
	        		
	        		JSONObject bundleDoc = cb.getDoc(appDoc.getString("bundleId"));
	        		if (bundleDoc != null) {
	        			
	        			JSONArray bundleApplications = bundleDoc.getJSONArray("applications");
	        			String appStatus = "";
	        			
	        			for (int i = 0; i < bundleApplications.length(); i ++) {
	        				JSONObject bApp = (JSONObject) bundleApplications.get(i);
	        				if (bApp.has("applicationDocId") && bApp.has("appStatus")) {
	        					if (bApp.getString("applicationDocId").equals(appDoc.getString("id"))) {
	        						appStatus = bApp.getString("appStatus");
	        						break;
	        					}
	        				}
	        			}
	        			
		        		if (appStatus.equals("INVALIDATED_SIGNED") && appDoc.getBoolean("isInitialPaymentCompleted")) {
		        			isPolicyNumValid = true;
		        		}
	        		}

	        	}
	        } else {
	        	isPolicyNumValid = true;
	        }									
	        
	        
	        String sql;
	        if (isPolicyNumValid) {
	        	String pPolicyNumber = this.dbm.param(policyNumber, DataType.TEXT);
	        	
	        	//RLS
				sql = selectStatusStatement(pPolicyNumber, "rls");		
				this.rs = this.dbm.select(sql, this.conn);
				
				Map<String,Object> rlsRow = new HashMap<String,Object>();
				rlsRow.put("id", "rls");
				rlsRow.put("type", "rls");
				rlsRow.put("action", langObj.getNameDesc("BCP.FUNC.SUBMISSION_RLS").toString());
				boolean recordExist = true;
	            if(this.rs != null && this.rs.next()){
	            	rlsRow.put("pol_num", this.rs.getString("POL_NUM"));
	            	String status = this.rs.getString("RLS_SUBMISSION_STATUS");
	            	if(status.equals("C")) {
	        			status = "Success";
	        		} else if(status.equals("R")) {
//	        			status = "Ready";
	        			status = "Processing";
	        		} else if(status.equals("W")) {
	        			status = "Waiting";
	        		} else if(status.equals("W")) {
	        			status = "Waiting";
	        		}  else if(status.equals("P")) {
	        			status = "Pending";
	        		} else if(status.equals("F")) {
	        			status = "Failed";
	        		}
	            	rlsRow.put("status", status);
	            	String upd_by = "";
	        		try {
	        			upd_by = this.rs.getString("CREATED_BY");
	        			if(upd_by == null) {
		        			upd_by = "SYSTEM";
		        		}
	        		} catch( Exception e) {
	        			upd_by = "SYSTEM";
	        		}
	        		
	        		
	            	rlsRow.put("updateBy", upd_by);
	            	rlsRow.put("upd_date", timeFormater(this.rs.getString("CREATE_DATE")));
	            } else {
	            	recordExist = false;
	            }
	            if(recordExist) {
	            	result.add(rlsRow);
	            }
	            
	            //WFI
				sql = selectStatusStatement(pPolicyNumber, "wfi");		
				this.rs = this.dbm.select(sql, conn);
				Map<String,Object> wfiRow = new HashMap<String,Object>();
				wfiRow.put("id", "wfi");
				wfiRow.put("type", "wfi");
				wfiRow.put("action", langObj.getNameDesc("BCP.FUNC.SUBMISSION_WFI").toString());
				recordExist = true;

				if(this.rs != null && this.rs.next()){
					String status = this.rs.getString("WFI_SUBMISSION_STATUS");
					wfiRow.put("pol_num", this.rs.getString("POL_NUM"));
					if(status.equals("C")) {
	        			status = "Success";
	        		} else if(status.equals("R")) {
//	        			status = "Ready";
	        			status = "Processing";
	        		} else if(status.equals("W")) {
	        			status = "Waiting";
	        		}  else if(status.equals("P")) {
	        			status = "Pending";
	        		} else if(status.equals("F")) {
	        			status = "Failed";
	        		}
					wfiRow.put("status", status);
					String upd_by = "";
	        		try {
	        			upd_by = this.rs.getString("CREATED_BY");
	        			if(upd_by == null) {
		        			upd_by = "SYSTEM";
		        		}
	        		} catch( Exception e) {
	        			upd_by = "SYSTEM";
	        		}
	        		
	        		
	            	wfiRow.put("updateBy", upd_by);
					wfiRow.put("upd_date", timeFormater(this.rs.getString("CREATE_DATE")));
	            } else {
	            	recordExist = false;
	            }
	            if(recordExist) {
	            	result.add(wfiRow);
	            }
	            
	            
	            //PAYMENT
				sql = selectStatusStatement(pPolicyNumber, "pymt");		
				this.rs = this.dbm.select(sql, this.conn);

				Map<String,Object> pymtRow = new HashMap<String,Object>();
				pymtRow.put("id", "pymt");
				pymtRow.put("type", "pymt");
				pymtRow.put("action", langObj.getNameDesc("BCP.FUNC.SUBMISSION_PAYMENT").toString());
				recordExist = true;
	            if(rs != null && rs.next()){
	            	pymtRow.put("pol_num", this.rs.getString("POL_NUM"));
	            	String status = this.rs.getString("STATUS");
	            	if(status.equals("C")) {
	        			status = "Success";
	        		} else if(status.equals("R")) {
//	        			status = "Ready";
	        			status = "Processing";
	        		} else if(status.equals("W")) {
	        			status = "Waiting";
	        		}  else if(status.equals("P")) {
	        			status = "Pending";
	        		} else if(status.equals("F")) {
	        			status = "Failed";
	        		}
	            	pymtRow.put("status", status);
	            	String upd_by = "";
	        		try {
	        			upd_by = this.rs.getString("CREATED_BY");
	        			if(upd_by == null) {
		        			upd_by = "SYSTEM";
		        		}
	        		} catch( Exception e) {
	        			upd_by = "SYSTEM";
	        		}
	        		
	        		
	            	pymtRow.put("updateBy", upd_by);
	            	pymtRow.put("upd_date", timeFormater(this.rs.getString("CREATE_DT")));
	            } else {
	            	recordExist = false;
	            	pymtRow.put("pol_num", policyNumber);
	            	pymtRow.put("status", "PEDNING");
	            	pymtRow.put("updateBy", "");
//	            	pymtRow.put("upd_date", "");
	            }
	            
	            if(recordExist) {
	            	result.add(pymtRow);
	            }
	            
	        }
		} catch (Exception e) {
			this.sb.delete(0, this.sb.length());
			this.sb.append("BcpRequest.getApplicationApiStatus(); error - ").append(e);
			
			Log.error(e);
			result.clear();
		} finally {
//			this.dbm = null;
			if (this.conn != null && !this.conn.isClosed()) this.conn.close();
			this.conn = null;
		}
		
		return result;
	}
	
	public String resubmit(JSONObject param) {

		Log.info("BcpRequest.resubmit...  param: " + param.toString());
		String result = "";
		String policyNumber = "";
		JSONArray selectedType = null;
//		String triggeredBy = "";
		StringBuilder sb = new StringBuilder();
		

		if(param.has("selectedType") && param.get("selectedType") instanceof JSONArray) {
			
			// the selectedType is an array sent from the front-end (node.js /react BCProcessingActions -> resubmit() )
			selectedType = param.getJSONArray("selectedType");
		}
		
		
		// RLS, WFI
		if(param.has("policyNumber") && param.get("policyNumber") instanceof String) {
			policyNumber = param.getString("policyNumber");
		} 
		
		// PYMT
		else if(param.has("policyNumbers") && param.get("policyNumbers") instanceof JSONArray) {
			policyNumber = param.getJSONArray("policyNumbers").getString(0);
		}
		
//		if(param.has("triggeredBy") && param.get("triggeredBy") instanceof String) {
//			triggeredBy = param.getString("triggeredBy");
//		}
		
		if(policyNumber == null || selectedType == null) {
			Log.info("BcpRequest.resubmitWfinRls() ; error - selectedType / policyNumber is / are null");
			sb.delete(0, sb.length());
//			sb.append("BcpRequest.resubmitWfinRls(); error - apiPath/policyNumber/triggeredBy is / are null");
			sb.append("{\"status\": " + false + ",");
			sb.append("\"title\": \" BCPRequest Error \",");
			sb.append("\"msg\": \"BcpRequest.resubmitWfinRls(); error - selectedType / policyNumber is / are null\"}");
			return sb.toString();
		}
		
		
		
		String base_url = EnvVariable.get("INTERNAL_API_DOMAIN");
		if (base_url == null){
			sb.delete(0, sb.length());
//			sb.append("BcpRequest.resubmitWfinRls(); error - cannot find API base domain");
			sb.append("{\"status\": " + false + ",");
			sb.append("\"title\": \" BCPRequest Error \",");
			sb.append("\"msg\": \"BcpRequest.resubmitWfinRls(); error - cannot find API base domain\"}");
			return sb.toString();
		}
		
		
//		String url 		= base_url + "/payment-record/resubmission";
		String url 		= base_url;
		
		
		

		if(selectedType.getString(0).toUpperCase().equals("RLS") || selectedType.getString(0).toUpperCase().equals("WFI")) {
			url += "/WfiRlsSubmission/submit";
			sb.append("{\"policyNumber\":\"" + policyNumber + "\", \"triggeredBy\" : \"" + principal.getUserCode() + "\", \"selectedType\" : [\"" + selectedType.getString(0).toLowerCase() + "\"]}");
		} else if(selectedType.getString(0).toUpperCase().equals("PYMT")) {
			url += "/payment-record/resubmission";
			sb.append("{\"policyNumbers\":[\"" + policyNumber + "\"], \"triggeredBy\" : \"" + principal.getUserCode() + "\"}");
		}
		
		
		
		HttpURLConnection con;
		try{
			URL obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
	 
		    // Setting basic post request
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			con.setRequestProperty("Content-Type","application/json");
		 
		  
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(sb.toString());
			wr.flush();
			wr.close();
 
			int responseCode = con.getResponseCode();
			 
			BufferedReader in = null;
			if ((responseCode >= 200) && (responseCode < 400)){
				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			}
			else{
				in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
			}
			
			String output;
			
			
			sb.delete(0,  sb.length());
			while ((output = in.readLine()) != null) {
				sb.append(output);
			}
			in.close();
  
			String 		response_str = sb.toString();
			response_str = response_str.replaceAll("[^a-zA-Z0-9 {}:,']+", "").replaceAll("(n )", "");
			
			
			
			JSONObject 	response_json 	= new JSONObject(response_str);
			Boolean hasOtherError = false;
			
			sb.delete(0, sb.length());
			sb.append("{\"msg\": \"Record resubmit request is sent for the following policy(s):  " + policyNumber + ". ");
			if(selectedType.getString(0).toUpperCase().equals("RLS") || selectedType.getString(0).toUpperCase().equals("WFI")) {
				if(response_json.has("success") && response_json.get("success") instanceof Object) {
					if(response_json.getBoolean("success")) {
						sb.append("\",");
						sb.append("\"status\": " + true + ",");
						sb.append("\"title\": \" BCPRequest Success \"}");
					} else {
						hasOtherError = true;
					}
				} else {
					hasOtherError = false;
				}
			} else if(selectedType.getString(0).toUpperCase().equals("PYMT")) {
				if(response_json.has("result") && response_json.get("result") instanceof Object) { 
					JSONObject result_obj = response_json.getJSONObject("result");
					if(result_obj.getBoolean("success")) {
						sb.append("\",");
						sb.append("\"status\": " + true + ",");
						sb.append("\"title\": \" BCPRequest Success \"}");
					} else {
						JSONObject msg = result_obj.getJSONObject("message");
						String reason = msg.getString("reason");
						sb.append("Response : " + reason + "\",");
						sb.append("\"status\": " + false + ",");
						sb.append("\"title\": \" BCPRequest Error \"}");
					}
				} else {
					hasOtherError = true;
				}
			} else {
				hasOtherError = true;
			}
			
//			if(response_json.has("result") && response_json.get("result") instanceof Arrays) {
//				JSONObject resultArr = response_json.getJSONArray("result").getJSONObject(0);
//				if(resultArr.has("success") && resultArr.get("success") instanceof Boolean) {
//					sb.append("{\"msg\": \"Record resubmit request is sent for the following policy(s):  " + policyNumber + ". ");
//					if(resultArr.getBoolean("success") == true) {
//						sb.append("\",");
//						sb.append("\"status\": " + true + ",");
//						sb.append("\"title\": \" BCPRequest Success \"}");
//					} else {
//						sb.append("Response : " + resultArr.getString("message").replace('\"', '\'') + "\",");
//						sb.append("\"status\": " + false + ",");
//						sb.append("\"title\": \" BCPRequest Error \"}");
//					}
//				} else {
//					hasOtherError = true;
//				}
//			}
//			 else {
//				 hasOtherError = true;
//			}
			
			if(hasOtherError) {
//				sb.append("{\"status\": " + false + ",");
//				sb.append("\"title\": \" BCPRequest Error \",");
//				sb.append("\"msg\": \"Fail to proceed the record resubmit request, please check the server stablize\"}");
				sb.delete(0, sb.length());
				sb.append("{\"title\": \" BCPRequest Response \",");
				sb.append("\"msg\": \"" + response_str.replaceAll(System.lineSeparator(),"").replaceAll("\"", "'").replaceAll("[^a-zA-Z0-9 {}:,']+", "") + "\"}");
			}
			
			result = sb.toString();
			
		}
		catch (Exception e){
			
			sb.delete(0, sb.length());
			sb.append("{\"status\": " + false + ",");
			sb.append("\"title\": \" BCPRequest Error \",");
			sb.append("\"msg\": \"Fail to proceed the record resubmit request. Response: " + e.toString().replaceAll("[^a-zA-Z0-9 ]+", "") + "\"}");
//			sb.append("\"msg\": \"Fail to proceed the record resubmit request. Response: " + e.toString().replaceAll("\n", "").replaceAll("(\\|^)", "") + "\"}");
			
//			sb.append("BcpRequest.resubmitWfinRls(); error - ").append(e);
			
			result = sb.toString();
		}
		
		
		return result;
	}
	

	
	
}
