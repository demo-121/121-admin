package com.eab.biz.bcp;

import com.eab.common.Constants;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.model.bcp.BcpBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class BcpDetailError extends BcpCommon{
	public BcpDetailError(HttpServletRequest request) {
		super(request);
	}
	
	public Response getErrorMsg(HttpServletRequest request, String type, String policyNumber, String taken_by_batch_no, String created_by) {
		Response resp = null;
		
		try {
//			List<Map<String,Object>> appbardata = new ArrayList<Map<String,Object>>();
			Map<String,Object> appbardata = new HashMap<String,Object>();
			
			Map<String,Object> level1_obj = new HashMap<String,Object>();
			level1_obj.put("id", "/BCProcessing");
			level1_obj.put("types", AppBarTitle.Types.LABEL);
			level1_obj.put("value", "/BCProcessing/" + type);
			level1_obj.put("primary", "BCP." + type);
			appbardata.put("level1", level1_obj);
			
			Map<String,Object> level2_obj = new HashMap<String,Object>();
			level2_obj.put("id", "/BCProcessing/Detail");
			level2_obj.put("types", AppBarTitle.Types.LABEL);
			level2_obj.put("value", "/BCProcessing/Detail/" + type);
			level2_obj.put("title", "BCP.DETAIL." + type);
			level2_obj.put("pol_num", "");
			appbardata.put("level2", level2_obj);
			
			Map<String,Object> level3_obj = new HashMap<String,Object>();
			level3_obj.put("id", "/BCProcessing/Detail/Error");
			level3_obj.put("types", AppBarTitle.Types.LABEL);
			level3_obj.put("value", "/BCProcessing/DetailError/" + type);
			level3_obj.put("title", "BCP.DETAIL.ERROR." + type);
			level3_obj.put("pol_num", " (" + policyNumber + ")");
			appbardata.put("level3", level3_obj);
			
			AppBar appBar = getAppBar(appbardata);
//			AppBar appBar = getAppBar("/BCProcessing", AppBarTitle.Types.LABEL, "BCP.DETAIL." + type, "/BCProcessing/Detail/" + type, "level2");
			Page pageObject = new Page("/BCProcessing/DetailError", "BCPDetailError");
			
			
			
//			List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
//			int i = 1;
//			while(true) {
//				Map<String,Object> rlsRow = new HashMap<String,Object>();
//				rlsRow.put("id", "eBI");
//				rlsRow.put("title", "Failed record for \"eBI\"");
//				rlsRow.put("type", "ListItem");
//				rlsRow.put("listSeq", i);
//				rlsRow.put("msg", "{\"message\": \"Authentication failed (HTTP Basic Auth and Client Certificate Auth)\",\"reasonCode\": \"ESG_ERR-005\",\"reason\": \"Authentication failed (HTTP Basic Auth and Client Certificate Auth)\"}");
//				result.add(rlsRow);
//				i++;
//				
//
//				if(i == 3) {
//					break;
//				}
//			}
			
//			List<Map<String,Object>> actionStatusList = result;
			
			BcpRequest br = new BcpRequest(request);
			List<Map<String,Object>> actionStatusList = br.getErrorHistoryLog(policyNumber, type, taken_by_batch_no, created_by);
			
			
			String resultStr = "";
			BcpBean values = new BcpBean(actionStatusList, resultStr);
			Content content = new Content(null, values, values);
			
			
			String tokenID 	= Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());
			
			
			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, 
					pageObject, tokenID, tokenKey, appBar, content);
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}
		
		return resp;
	}
}
