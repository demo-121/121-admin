package com.eab.biz.beneIllus;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.eab.biz.dynamic.LockManager;
import com.eab.biz.tasks.TaskInfoMgr;
import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.common.Constants.ActionTypes;
import com.eab.common.Constants.PageIDs;
import com.eab.dao.beneIllus.BeneIllus;
import com.eab.dao.dynamic.Companies;
import com.eab.dao.dynamic.RecordLock;
import com.eab.dao.tasks.Tasks;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.BiListCondition;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.DialogItem;
import com.eab.json.model.Field;
import com.eab.json.model.ListContent;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.json.model.Field.Types;
import com.eab.model.beneIllus.BiBean;
import com.eab.model.dynamic.CompanyBean;
import com.eab.model.dynamic.RecordLockBean;
import com.eab.model.needs.NeedsBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.system.FuncBean;
import com.eab.security.SysPrivilegeMgr;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class BeneIllusMgr {
	
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	HttpSession session;
	Gson gson;

	public BeneIllusMgr(HttpServletRequest request) {
		super();
		this.request = request;
		session = request.getSession();
		principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public Response checkExisting(List<String> biCodes, String compCode) {
		List<String> existedBiCode = new ArrayList<>();

		BeneIllus bi = new BeneIllus();

		for (int i = 0; i < biCodes.size(); i++) {
			String biCode = biCodes.get(i);

			try {
				if (bi.checkExist(biCode, compCode, -1))
					existedBiCode.add(biCode);
			} catch (SQLException e) {
				Log.error(e);
			}
		}

		if (existedBiCode.size() > 0) {
			// Confirmation Dialog

			// Action
			String action = ActionTypes.SHOW_DIALOG;

			// Token
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			// Dialog
			Field positive = new Field("/BeneIllus/Clone", "submitButton", langObj.getNameDesc("BUTTON.OK"));
			Field negative = new Field("cancel", "button", langObj.getNameDesc("BUTTON.CANCEL"));
			Dialog dialog = new Dialog("clone_confirm", langObj.getNameDesc("BI.CLONE_DIALOG.CLONE"), langObj.getNameDesc("BI.CLONE_DIALOG.CLONE_CONFIRM_MSG"), positive, negative, true, 30);

			return new Response(action, tokenID, tokenKey, dialog);
		} else {
			// Clone
			return cloneBi(biCodes, compCode);
		}
	}
	
	public Response cloneBi(List<String> biCodes, String destCompCode) {
		BeneIllus bi = new BeneIllus();
		String newBiCode = null;
		
		for (int i = 0; i < biCodes.size(); i++) {
			String biCode = biCodes.get(i);

			try {
				newBiCode = bi.clone(principal, biCode, destCompCode);
			} catch (SQLException e) {
				Log.error(e);
				return null;
			}
		}

		// OK Dialog

		// Action
		if (biCodes.size() == 1 && destCompCode.equals(principal.getCompCode())){
			return getBiDetail(newBiCode, 1, null);
		} else {
			String action = ActionTypes.CHANGE_PAGE;
	
			// Token
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());
	
			// Page
			Page page = new Page(PageIDs.BI_LIST, "Benefit Illustration List");
	
			// Dialog
			Field negative = new Field("ok", "button", langObj.getNameDesc("BUTTON.OK"));
			Dialog dialog = new Dialog("clone_completed", langObj.getNameDesc("BI.CLONE_DIALOG.CLONE"), langObj.getNameDesc("COMPLETE"), null, negative, 30);
	
			// Build
			Response rs = new Response(action, tokenID, tokenKey, page);
			rs.setDialog(dialog);
	
			return rs;
		}
	}

	
	public Dialog cloneDialog() {
		Companies companiesDAO = new Companies();

		// Clone - Company Dialog Items
		List<Option> companyOptions = new ArrayList<>();
		
		try {
			List<CompanyBean> companyList = companiesDAO.getCompanyListByUser(request);
		
			for (int i = 0; i < companyList.size(); i++) {
				CompanyBean company = companyList.get(i);
				companyOptions.add(new Option(company.getCompName(), company.getCompCode()));
			}
		} catch (SQLException e1) {
			Log.error(e1);
		}
		
		List<Field> companyDialogItems = new ArrayList<>();
		companyDialogItems.add(new DialogItem("compCode", DialogItem.Types.RADIOBUTTON, null, true, companyOptions));

		// Clone - Company Positive
		Field companyPositive = new Field("/BeneIllus/CheckExist", "submitButton", langObj.getNameDesc("BUTTON.OK"));
		// Clone - Company Negative
		Field companyNegative = new Field("company_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL"));
		// Clone - Company Dialog
		Dialog companyDialog = new Dialog("company", langObj.getNameDesc("PROD.CLONE_DIALOG.SELECT_COMP"), langObj.getNameDesc("PROD.CLONE_DIALOG.SELECT_COMP_MSG"), companyPositive, companyNegative, companyDialogItems, 40);

		// Clone - Confirm Positive
		Field cloneConfirmPositive = new Field("confirm_ok", "dialogButton", langObj.getNameDesc("BUTTON.OK"), companyDialog);
		// Clone - Confirm Negative
		Field cloneConfirmNegative = new Field("confirm_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL"));
		// Clone - Confirm Dialog
		Dialog cloneConfirmDialog = new Dialog("confirm", langObj.getNameDesc("PROD.CLONE_DIALOG.CLONE"), langObj.getNameDesc("PROD.CLONE_DIALOG.CLONE_MSG"), cloneConfirmPositive, cloneConfirmNegative, 30);

		//return cloneConfirmDialog;
		return companyDialog;
	}

	public Dialog newVersionDialog() {
		// New Version - Confirm Positive
		Field newVersionConfirmPositive = new Field("/BeneIllus/NewVersion", "submitButton", langObj.getNameDesc("BUTTON.OK"));
		// New Version - Confirm Negative
		Field newVersionCconfirmNegative = new Field("confirm_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL"));
		// New Version - Confirm Dialog
		Dialog newVersionConfirmDialog = new Dialog("confirm", langObj.getNameDesc("PROD.CLONE_DIALOG.NEW_VERSION"), langObj.getNameDesc("PROD.CLONE_DIALOG.NEW_VERSION_CONFIRM"), newVersionConfirmPositive, newVersionCconfirmNegative, 30);

		return newVersionConfirmDialog;
	}
	
	public int newVersion(String biCode) {
        BeneIllus bi = new BeneIllus();

        if (biCode != null && !biCode.isEmpty()) {
            try {
                return bi.newVersion(principal, biCode);
            } catch (SQLException e) {
                Log.error(e);
            }
        }
        
        return -1;
    }
	
	public Response biList(BiListCondition biListCondition) {
		try{
			// biListCondition
			String criteria = biListCondition != null ? biListCondition.getCriteria() : null;
			String status = biListCondition != null ? biListCondition.getStatus() : null;
			String sortBy = biListCondition != null ? biListCondition.getSortBy() : null;
			String sortDir = biListCondition != null ? biListCondition.getSortDir() : null;
			Integer recordStart = biListCondition != null ? biListCondition.getRecordStart() : null;
			Integer pageSize = biListCondition != null ? biListCondition.getPageSize() : null;
	
			// DAO
			BeneIllus biDAO = new BeneIllus();
	
			Boolean updateContentOnly = criteria != null || status != null || (sortBy != null && sortDir != null);
	
			// Action
			String action = updateContentOnly ? ActionTypes.CHANGE_CONTENT : ActionTypes.CHANGE_PAGE;
	
			// Page
			Page page = new Page(PageIDs.BI_LIST, "Benefit Illustration List");
	
			// Token
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());
	
			// AppBar - Title
			AppBarTitle appBarTitle = new AppBarTitle(null, "label", "", langObj.getNameDesc("MENU.BENE_ILLUS").toString(), null, null);
			
			// Action1 - 1
			Field appBarAction1_1 = new Field("status", "picker", "-R", null, biDAO.getBiStatus(langObj));
			// Action1 - 2
			Field appBarAction1_2 = new Field("/BeneIllus/Search", "searchButton", null, "Search", null);
			// Action1 - 3
			Field appBarAction1_3 = new Field("/BeneIllus/Detail", "iconButton", "add", "Add", null);
			// Action1
			List<Field> appBarAction1 = new ArrayList<>();
			appBarAction1.add(appBarAction1_1);
			appBarAction1.add(appBarAction1_2);
			appBarAction1.add(appBarAction1_3);
	
			Dialog cloneDialog = cloneDialog();
			Dialog newVersionDialog = newVersionDialog();
	
			// Action2 - 1
			Field appBarAction2_1 = new Field("/BeneIllus/NewVersion", "dialogButton", langObj.getNameDesc("BUTTON.CREATE_NEW_VERSION"), newVersionDialog);
			// Action2 - 2
			Field appBarAction2_2 = new Field("/BeneIllus/Clone", "dialogButton", langObj.getNameDesc("BUTTON.CLONE"), cloneDialog);
			// Action2
			List<Field> appBarAction2 = new ArrayList<>();
			appBarAction2.add(appBarAction2_1);
			appBarAction2.add(appBarAction2_2);
	
			// Action3 - 1
			Field appBarAction3_1 = new Field("/BeneIllus/Clone", "dialogButton", langObj.getNameDesc("BUTTON.CLONE"), cloneDialog);
			// Action3
			List<Field> appBarAction3 = new ArrayList<>();
			appBarAction3.add(appBarAction3_1);
	
			// Action4 - 1
			Field appBarAction4_1 = new Field("/BeneIllus/NewVersion", "dialogButton", langObj.getNameDesc("BUTTON.CREATE_NEW_VERSION"), newVersionDialog);
			// Action4
			List<Field> appBarAction4 = new ArrayList<>();
			appBarAction4.add(appBarAction4_1);
			 
			// Actions
			List<List<Field>> appBarActions = new ArrayList<>();
			appBarActions.add(appBarAction1);
			appBarActions.add(appBarAction2);
			appBarActions.add(appBarAction3);
			appBarActions.add(appBarAction4);
		
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, appBarActions, biListCondition);
			
			// Product List Data
			List<BiBean> biList = new ArrayList<>();
	
			if (recordStart != null && recordStart != 0)
				biList = (List<BiBean>) session.getAttribute(Constants.SessionKeys.SEARCH_RESULT);
	
			// Get list
			if (biList.size() == 0) {
				try {
					biList = biDAO.searchBiList(biListCondition, principal, langObj);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					Log.error(e);
				}
	
				// Add result to session for "More" button use
				session.setAttribute(Constants.SessionKeys.SEARCH_RESULT, biList);
			}
	
			// Template
			Template template = new Template();
			List<Field> items = new ArrayList<>();
			items.add(new Field("biCode", langObj.getNameDesc("BI_LIST_COLUMN_BICODE"), Types.TEXT, 1, 1, "200px"));
			items.add(new Field("biName", langObj.getNameDesc("BI_LIST_COLUMN_BINAME"), Types.TEXT, 2, 2, "auto"));
			items.add(new Field("status", langObj.getNameDesc("BI_LIST_COLUMN_STATUS"), Types.TEXT, 3, 3, "150px"));
			template.setItems(items);
	
			// More
			int offset = recordStart != null ? recordStart : 0;
			int size = pageSize != null ? pageSize : Constants.SEARCH_RESULT_DEFAULT_PAGE_SIZE;
	
			List<Object> shortedList = new ArrayList<>();
	
			for (int j = offset; j < offset + size && j < biList.size(); j++) {
				shortedList.add(biList.get(j));
			}
	
			// Content - values
			ListContent contentValues = new ListContent(shortedList, biList.size(), offset > 0);
	
			// Content
			Content content = new Content(template, contentValues);
	
			// Build
			return new Response(action, null, page, tokenID, tokenKey, appBar, content);
		} catch(Exception e){
			Log.error(e);
		}
		
		return null;
	}
	
	public Response changePage(String selectPageId, String targetPageId, String biCode, int version) {
		// Action
		String action = ActionTypes.RESET_VALUES;

		// Token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());

		JsonObject values = new JsonObject();
		JsonObject selectPage = new JsonObject();
		selectPage.addProperty("id", targetPageId);
		
		if (targetPageId.equals(Constants.BiPage.TASK)) {
			TaskInfoMgr taskMgr = new TaskInfoMgr(request, "bi");
			taskMgr.addValues(values, -1, biCode, version, null);
		} else if (targetPageId.equals(Constants.BiPage.CLIENT_SIGN) || targetPageId.equals(Constants.BiPage.AGENT_SIGN)) {
			selectPage.addProperty("uid", "BiSign");
		}
		
		values.add("selectPage", gson.toJsonTree(selectPage));
		values.add("id", gson.toJsonTree(biCode));
		values.addProperty("actionType", "merge");
		Content content = new Content(null, values);
		
		return new Response(action, null, null, tokenID, tokenKey, null, content);
	}

	public Response getBiDetail(String biCode, int version, String selectPageId){
		try {
			
			if (biCode == null)
				biCode = "";
			
			if (selectPageId == null || selectPageId.equals(""))
				selectPageId = Constants.BiPage.TASK;
		
			boolean isNewBi = (biCode == null || biCode.equals("")) ? true : false;
			JsonObject values = new JsonObject();
			BeneIllus bi = new BeneIllus();
			
			Log.debug("---------------------[BeneIllusMgr - start check is record readOnly]--------------------------");
			boolean canAccess = false;
		
			// bi status
			Map<String, Object> biStatus = bi.getBiStatusById(biCode, version, principal);
			
			// recordLock
			Dialog lockDialog = null;
			if (!isNewBi) {
				RecordLock recordLock = new RecordLock();
				RecordLockBean recordLockBean = recordLock.newRecordLockBean("biDetail", biCode, version);
				recordLock.getRecordLock(request, recordLockBean);
				
				if (recordLockBean.canAccess()) {
					if (!recordLockBean.isCurrentUserAccess())
						recordLock.addRecordLock(request, recordLockBean);
					
					canAccess = true;
				} else {
					LockManager lm = new LockManager(request);
					lockDialog = lm.lockDialog(recordLockBean);
				}
			}
			
			boolean isReadOnly = false;
			boolean isSuspend = false;
			boolean updateRight = false;
			
			if (!isNewBi) {
				if (!canAccess) {
					isReadOnly = true;
				} else if (biStatus!=null) {
					if (biStatus.containsKey("launch_status") && biStatus.get("launch_status").toString().equals("L")) {
						isReadOnly = true;
					} else if (biStatus.containsKey("status") && biStatus.get("status").toString().equals("E")) {
						if (biStatus.containsKey("bi_ver") && ((int)biStatus.get("bi_ver"))>=version)
							isReadOnly = true;
					} else if (biStatus.containsKey("task_status")) {
						if (biStatus.get("task_status").toString().equals("S"))
							isReadOnly = true;
						
						if (biStatus.get("task_status").toString().equals("D"))
							isSuspend = true;
					}
				}
			}
			
			//check access right
			SysPrivilegeMgr privilegeMgr = new SysPrivilegeMgr();
			ArrayList<FuncBean> funcList = privilegeMgr.getFunc(principal.getRoleCode(), "en");
			
			for(FuncBean funcBean: funcList){
				if (funcBean.getCode().equals("FN.PRO.BENEF.UPD"))
					updateRight = true;
			}
			
			boolean canEdit = isSuspend || isReadOnly || !updateRight;

			Log.debug("---------------------[BeneIllusMgr - end check is record readOnly]--------------------------");
			Log.debug("---------------------[BeneIllusMgr - start get language]--------------------------");

			// Action
			String action = ActionTypes.CHANGE_PAGE;

			// Token
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());
			
			// AppBar - Title
			String pageTitle = "";
			Log.debug("---------------------[BeneIllusMgr - start building app bar]--------------------------");
			List<BiBean> biBeans = bi.getBis(biCode, principal.getCompCode(), principal);
			List<Option> secondaryOpts = new ArrayList<Option>();
			
//			for (BiBean biBean : biBeans) {
//				String _title = biBean.getBiName() + "(v." + String.valueOf(biBean.getVersion()) + ")";
//				secondaryOpts.add(new Option(_title, String.valueOf(biBean.getVersion())));
//				
//				if (version == biBean.getVersion())
//					pageTitle = _title;
//			}
			
			for (BiBean biBean : biBeans) {
				String _title = biBean.getBiName() + " v." + String.valueOf(biBean.getVersion()) + " (" + biBean.getTaskStatus().toLowerCase() + ")";
				Option opt = new Option(_title, String.valueOf(biBean.getVersion()));
				
				if (biStatus.containsKey("bi_ver") && ((int)biStatus.get("bi_ver")) == biBean.getVersion())
					opt.setHighLight(true);
					
				secondaryOpts.add(opt);
				
				if (version == biBean.getVersion())
					pageTitle = _title;
			}
			
			Object secondary = (isNewBi) ? langObj.getNameDesc("BI.NEW_BI") : new Field("/BeneIllus/Detail", Types.VERSIONPICKER, String.valueOf(version), secondaryOpts, null);
			AppBarTitle appBarTitle = new AppBarTitle(AppBarTitle.Types.LEVEL_TWO, null, null, secondary);
			
			// Action1
			List<Field> appBarAction1 = new ArrayList<>();
			
			// Action1 - 1
			if (!isReadOnly && updateRight) {
				Field appBarAction1_1 = new Field("/BeneIllus/Detail/Save", "submitChangedButton", "save", langObj.getNameDesc("BUTTON.SAVE"), null);
				appBarAction1.add(appBarAction1_1);
			}
			
			// Action1 - 2
			if (!isNewBi && updateRight) {
				Field appBarAction1_2 = new Field("/BeneIllus/Detail/Preview", "iconButton", "visibility", "Visibility", null);
				// Action1 - 3
				List<Field> appBarAction1_3Items = new ArrayList<>();
				Dialog cloneDialog = cloneDialog();
				Dialog newVersionDialog = newVersionDialog();
				
				if (!biStatus.get("status").toString().equals("T"))
					appBarAction1_3Items.add(new Field("/BeneIllus/NewVersion", "dialogButton", langObj.getNameDesc("ACTION_NEW_VERSION"), newVersionDialog));
				
				appBarAction1_3Items.add(new Field("/BeneIllus/Clone", "dialogButton", langObj.getNameDesc("ACTION_CLONE_BI"), cloneDialog));
				Field appBarAction1_3 = new Field("more", "iconMenu", appBarAction1_3Items);
				
				appBarAction1.add(appBarAction1_2);
				appBarAction1.add(appBarAction1_3);
			}

			// Actions
			List<List<Field>> appBarActions = new ArrayList<>();
			appBarActions.add(appBarAction1);

			// AppBar
			AppBar appBar = new AppBar(appBarTitle, appBarActions);

			// Page
			JsonObject pageValue = new JsonObject();
			pageValue.addProperty("id", biCode);
			pageValue.addProperty("version", version);
			pageValue.addProperty("selectPageId", selectPageId);
			Page page = new Page("/BeneIllus/Detail", pageTitle, pageValue);

			Log.debug("---------------------[BeneIllusMgr - start building template]--------------------------");

			// Template - Section
			List<Field> sections = new ArrayList<Field>();

			Log.debug("---------------------[BeneIllusMgr - start building template with values ]--------------------------");
			JsonArray menuLists = bi.getMenu(biCode, principal, version, isNewBi);
			
			TaskInfoMgr taskMgr = new TaskInfoMgr(request, "bi");
			BeneIllusDetailMgr biDetailMgr = new BeneIllusDetailMgr(request);
			
			for (int i=0; i<menuLists.size(); i++) {
				JsonArray menuList = menuLists.get(i).getAsJsonArray();
				
				for (int j=0; j<menuList.size(); j++) {
					JsonObject menuItem = menuList.get(j).getAsJsonObject();
					String _sid = menuItem.get("id").getAsString();
					
					switch(_sid) {
						case Constants.BiPage.TASK :
							Tasks tasksDao = new Tasks();
							Map<String, Object> task = tasksDao.getTaskById(-1, biCode, version, Tasks.FuncCode.BI, null, principal);
							taskMgr.addSection(task, sections, isNewBi, isReadOnly || !updateRight);
							
							if (!isNewBi) 
								taskMgr.addValues(task, values);
					
							break;
						case Constants.BiPage.DETAIL:
							biDetailMgr.addSection(sections, biCode, version, isNewBi, (isNewBi || (!isNewBi && ((int)biStatus.get("bi_ver"))==1 )), canEdit);
							
							if (!isNewBi) 
								biDetailMgr.addValues(values, biCode, version);
							
							break;
						default:
							if (!isNewBi) {
								Map<String, JSONObject> section = bi.getBiSections(biCode, principal, version, _sid);
								
								if(section!=null && section.containsKey(_sid))
									values.add(_sid, JsonConverter.convertObj(section.get(_sid)));
							}
					}
				}
			}
			
			Log.debug("---------------------[BeneIllusMgr - end building template with values]--------------------------");

			Template template = new Template("template", sections, null, null);
			template.setMenu(menuLists);

			Log.debug("---------------------[BeneIllusMgr - start building template]--------------------------");
			Log.debug("---------------------[BeneIllusMgr - start adding values]--------------------------");
			
			//attachments
			Map<String, Object> _attachs = bi.getAttachments(biCode, version, principal);
			JsonObject attachsSet = new JsonObject();
			// load image from db
			if (_attachs != null) {
				for (Entry<String, Object> entry : _attachs.entrySet()) {
					String key = entry.getKey();

					Blob blob = (Blob) _attachs.get(key);
					byte[] bdata = blob.getBytes(1, (int) blob.length());
					String b64String = new String(bdata);
					attachsSet.remove(key);
					attachsSet.add(key, gson.toJsonTree(b64String));
				}
			}

			values.add(Constants.NeedPage.ATTACHMENTS, attachsSet);			
			values.add("id", gson.toJsonTree(biCode));
			values.add("version", gson.toJsonTree(version));
			values.addProperty("isNew", isNewBi);
			values.addProperty("readOnly", canEdit);

			JsonObject selectPage = new JsonObject();
			selectPage.addProperty("id", selectPageId);
			values.add("selectPage", gson.toJsonTree(selectPage));

			Content content = new Content(template, values);
			Log.debug("---------------------[BeneIllusMgr - end adding values]--------------------------");
			Response resp = new Response(action, null, page, tokenID, tokenKey, appBar, content);
			resp.setDialog(lockDialog);
			return resp;
		} catch (Exception e) {
			Log.error(e);
		}

		return null;
	}
	
}
