package com.eab.biz.beneIllus;

import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

import com.eab.biz.dynamic.CompanyManager;
import com.eab.biz.upload.UploadManager;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.beneIllus.BeneIllus;
import com.eab.dao.tasks.Tasks;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.UploadResponse;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.tasks.TaskBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class BeneIllusAttachMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	
	public BeneIllusAttachMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	 
	public String getparam(String key){
		String value = request.getParameter(key);
		if(value == null && request.getParameterValues(key) != null){
			String[] values = request.getParameterValues(key);
			value = values[0];
		}
		return value;
	}
	
	public UploadResponse saveAttach(){
		UploadResponse response = new UploadResponse();

		String param = request.getParameter("p0");		
		String id = request.getParameter("id");
		
		if(id != null){
			byte[] decoded = Base64.decodeBase64(id.replace(" ", "+"));
			id = new String(decoded, StandardCharsets.UTF_8);
			id = id.replace("\"", "");
		}
		
		
		String module = getparam("module");
		String packetNumStr = getparam("packetNum");
		String offsetStr = getparam("offset");

		String biCode = "";
		String selectPageId = "";
		String targetPageId = "";
		int version = 1;
		PrintWriter out = null;
		TaskBean taskInfo = null;
		JSONObject mBiDetail = null;
		JSONObject attachItems = null;
		int taskId = -1;
		Map<String, JSONObject> arrMSections = new HashMap<String, JSONObject>();;
		boolean isNewBi = false;
		boolean isNewBiComplete = true;
		boolean isSkip = false;
		String[] sIds = {
				Constants.BiPage.CSS_HEADER_FOOTER,
				Constants.BiPage.CLIENT_SIGN,
				Constants.BiPage.AGENT_SIGN,
				Constants.BiPage.BI_PAGES,
				Constants.BiPage.BI_IMAGE
		};

		
		int packetNum = Function.stringToInt(packetNumStr);
		int offset = Function.stringToInt(offsetStr);
		
		UploadManager um = new UploadManager(request);  
		try{		
			if(offset != -1 && packetNum != -1 && um.isOffsetValid(module, id, version, offset)){
					if(offset == packetNum - 1){
						//insert into db
						Log.debug("biMgr: last packet");
						
						String fullData = null;
						if(packetNum == 1)
							fullData = param;
						else
							fullData = um.getAttachmentFromSession(module, id, version, offset) + param;
						
						Log.debug("version:"+version);			
						CompanyManager cm = new CompanyManager(request);
						List<String> langList = cm.getCompanyLangList(principal.getCompCode());
						byte[] decoded = Base64.decodeBase64(fullData.replace(" ", "+"));
						String paraStr = new String(decoded, StandardCharsets.UTF_8);						
						JSONObject resultJSON = new JSONObject(paraStr);
						if(resultJSON.has("taskId")){
							taskId = resultJSON.getInt("taskId");
						}
						if (resultJSON.has("id")) {
							biCode = resultJSON.getString("id");
							if (biCode == null || biCode.equals(""))
								isNewBi = true;
						}
						if (resultJSON.has("version"))
							version = resultJSON.getInt("version");
						
						if (resultJSON.has("selectPage"))
							selectPageId = resultJSON.getJSONObject("selectPage").getString("id");
						targetPageId = resultJSON.has("selectPageId")?  resultJSON.getString("selectPageId") : selectPageId;
							
						
						if (resultJSON.has(Constants.BiPage.TASK)) {
							taskInfo = new TaskBean();
							JSONObject _mTaskInfo = resultJSON.getJSONObject(Constants.ProductPage.TASK);
							if(!_mTaskInfo.has("task_desc") || (_mTaskInfo.has("task_desc") && "".equals(_mTaskInfo.get("task_desc")) 
									|| !_mTaskInfo.has("effDate") || (_mTaskInfo.has("effDate") && _mTaskInfo.get("effDate").getClass() != Long.class)))
								isNewBiComplete = false;
							else{
								taskInfo.setTaskDesc(_mTaskInfo.getString("task_desc"));
								if(_mTaskInfo.has("ref_no"))taskInfo.setRefNo(_mTaskInfo.getString("ref_no"));
								Date effDate = new Date(_mTaskInfo.getLong("effDate"));
								taskInfo.setEffDate(effDate);
							}
							
							if(!isNewBi && taskId>0){
								taskInfo.setTaskCode(String.valueOf(taskId));
							}
						}else if(isNewBi){
							isNewBiComplete = false;
						}
						
						if (resultJSON.has(Constants.BiPage.DETAIL)) {
							JSONObject _mBiDetail = resultJSON.getJSONObject(Constants.BiPage.DETAIL);
							if(!_mBiDetail.has("biCode") || (_mBiDetail.has("biCode") && "".equals(_mBiDetail.get("biCode"))) 
									|| !_mBiDetail.has("biName"))
								isNewBiComplete = false;
							else{
								mBiDetail = new JSONObject();
								for (String key : _mBiDetail.keySet()) {
									mBiDetail.put(key, _mBiDetail.get(key));
									if (key.equals("biCode"))
										biCode = _mBiDetail.get(key).toString();
								}
								mBiDetail.put("version", version);
								if(isNewBi){
									mBiDetail.put("launch_date", taskInfo.getEffDate());
									if(taskInfo != null){
										taskInfo.setItemCode(biCode);
										taskInfo.setFuncCode(Tasks.FuncCode.BI);
										taskInfo.setVerNo("1");
									}
								}
							}
						}else if(isNewBi){
							isNewBiComplete = false;
						}
						
						for(String sid: sIds){
							if (resultJSON.has(sid)) {
								arrMSections.put(sid,resultJSON.getJSONObject(sid));
							}
						}
						
						
						if(resultJSON.has(Constants.NeedPage.ATTACHMENTS))
							attachItems = resultJSON.getJSONObject(Constants.NeedPage.ATTACHMENTS);
						
						
						if(resultJSON.has("skip")){
							isSkip = resultJSON.getBoolean("skip");
						}
						
						if(isNewBi && resultJSON.has(Constants.BiPage.TASK) && resultJSON.has(Constants.BiPage.DETAIL)){
							isNewBiComplete = true;
						}
						 

						BeneIllus bi = new BeneIllus();


						String errMsg = (!isSkip &&(!isNewBi || isNewBiComplete))?bi.saveBi(biCode, version, taskInfo, mBiDetail, arrMSections, attachItems, taskId, principal):null;
						if(errMsg != null){
							Field negativeBtn = new Field("cancel", "button", langObj.getNameDesc("BUTTON.OK"));
							Dialog errDialog = new Dialog("Error", langObj.getNameDesc("ERROR"), errMsg, null, negativeBtn, 30);
							response.setDialog(errDialog);
						}
						response.setSelectPageId(targetPageId);
						response.setItemCode(biCode);
						response.setVersion(version);						
						response.setCurPageId(selectPageId);
						response.setChange((isNewBi && isNewBiComplete)?true:false);
						 
						////////////////////////////////
						
						 
						//clear session
						um.clearAttachmentFromSession(module, id, version, offset);
						
						//return done state
						response.setComplete(true);
						response.setTeminate(true);
					}else{
						Log.debug("saveAttachment:next:next packet");
					
						String currentData = um.getAttachmentFromSession(module, id, version, offset);
						String newData = null;
						if(currentData != null)
							newData = um.getAttachmentFromSession(module, id, version, offset) + param;
						else
							newData = param;
						
						um.saveAttachmentOffsetToSession(module, id, version, offset);
						um.saveAttachmentToSession(module, id, version, offset, newData);
						
						//return the next offset
						int newOffset = offset + 1;
						response.setOffSet(newOffset);
						response.setComplete(false);
						response.setTeminate(false);
					}
				
				
			}else{
				response.setComplete(false);
				response.setTeminate(true);
			}
		}catch(Exception e){
			Log.debug("saveAttachment: Exception");
			Log.error(e);
			//clear session
			um.clearAttachmentFromSession(module, id, version, offset);
			response.setComplete(false);
			response.setTeminate(true);
		}
		
		Log.debug("saveAttachment: done");
		
		//set token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		response.setTokenID(tokenID);
		response.setTokenKey(tokenKey);
		return response;
	}
	
	
	
}
