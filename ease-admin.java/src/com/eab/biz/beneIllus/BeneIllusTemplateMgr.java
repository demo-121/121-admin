package com.eab.biz.beneIllus;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Compressor;
import com.eab.common.Constants;
import com.eab.common.CryptoUtil;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.common.Constants.ActionTypes;
import com.eab.couchbase.CBServer;
import com.eab.dao.beneIllus.BeneIllus;
import com.eab.dao.releases.ReleasePublish;
import com.eab.helper.ReportsGenerator;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Response;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class BeneIllusTemplateMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	HttpSession session;
	
	private final int TYPE_IMAGE = 1;
	
	
	Gson gson;

	public BeneIllusTemplateMgr(HttpServletRequest request, String comp_code) {
		super();
		this.request = request;
		
		if (request == null) {
			principal = new UserPrincipalBean();
			principal.setCompCode(comp_code);
		} else {
			session = request.getSession();
			principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
			langObj = new Language(principal.getUITranslation());
		}
			
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public Response previewBi(String biCode, int version) throws Exception{
		
		// Action
		String action = ActionTypes.PREVIEW_PDF;
	
		// Token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		
		// AppBar - Title
		
		String pageTitle = "";
		Log.debug("---------------------[BeneIllusTemplateMgr - start building app bar]--------------------------");
		AppBarTitle appBarTitle = new AppBarTitle("/BeneIllus/Detail/Preview", AppBarTitle.Types.LABEL, "Preview", "Preview", null, null);
		// AppBar
		AppBar appBar = new AppBar(appBarTitle, null);
		
		JsonObject reportTemplate = handleBiTemplate(biCode, version);
		JsonObject commonTemplate = getCommonTemplate();
		String style = commonTemplate.get("style").getAsString();
		style = style.replace(".pageContainer{", ".pageContainer{border-bottom:solid 1px;");
		commonTemplate.addProperty("style", style);
		String xmlData = getXml();
		
		ReportsGenerator reportsGenerator = new ReportsGenerator(request);        	
    	String reportHtml = reportsGenerator.generateHtml(commonTemplate.toString(), reportTemplate.toString(), xmlData);
		
    	JsonObject values = new JsonObject();
    	values.addProperty("html", reportHtml);
    	Content content = new Content(null, values);
		
		return new Response(action, null, null, tokenID, tokenKey, appBar, content);
	}
	
	public JsonObject getCommonTemplate(){
		ReleasePublish publishdao = new ReleasePublish();
		JsonObject commonTemplate = null;
		try{
			//Get Production Config		
			List<Map<String,Object>> prodENVData = publishdao.getProductionENVData();
						
			CBServer cbServer = null;
			for(Map<String,Object> prodENV : prodENVData){
				if(prodENV.get("COMP_CODE").toString().equals(principal.getCompCode())){
					CryptoUtil crypto = new CryptoUtil();
					String password = crypto.decryptUserData(prodENV.get("COMP_CODE").toString(), prodENV.get("DB_PASSWORD").toString());
					cbServer = new CBServer(prodENV.get("HOST").toString(), prodENV.get("PORT").toString(), prodENV.get("DB_NAME").toString(), prodENV.get("DB_LOGIN").toString(), password);
					break;
				}
			}
				
			if(cbServer == null){
				throw new Exception ("Server Connection Fail");
			}else if (cbServer.checkDBConnection() == false){
				throw new Exception ("Server Connection Fail");
			}
			commonTemplate = JsonConverter.convertObj(cbServer.getDoc("commonTemplate")).getAsJsonObject();
		}catch (Exception e){
			
		}	
		return commonTemplate;
	}
	
	public JsonObject handleBiTemplate(String biCode, int version) throws Exception{
		JsonObject reportTemplate = new JsonObject();
		BeneIllus bi = new BeneIllus();
		Map<String, JSONObject> sections = bi.getBiSections(biCode, principal, version, null);
		JSONObject images = null;
		if(sections.containsKey(Constants.BiPage.BI_IMAGE)){
			images = sections.get(Constants.BiPage.BI_IMAGE);
		}
		for(String key : sections.keySet()){
			JSONObject section = sections.get(key);
			
			switch(key){
				case Constants.BiPage.CSS_HEADER_FOOTER:
					JsonObject headerFooter = new JsonObject();
					if(section.has("header")){
						headerFooter.addProperty("header", replaceXslWord(biCode, version, images, Compressor.compressHtml(section.getString("header"))));
					}
					if(section.has("footer")){
						headerFooter.addProperty("footer", replaceXslWord(biCode, version, images, Compressor.compressHtml(section.getString("footer"))));
					}
					reportTemplate.add("headerFooter", headerFooter);
					
					if(section.has("style")){
						reportTemplate.addProperty("style", Compressor.compressCss(section.getString("style"), new Compressor.Options()));
					}
					break;
				case Constants.BiPage.CLIENT_SIGN:
				case Constants.BiPage.AGENT_SIGN:
					JsonArray signFields = new JsonArray();
					if(section.has("signFields")){
						JSONArray items = section.getJSONArray("signFields");
						for(int i=0; i<items.length(); i++){
							JSONObject item = items.getJSONObject(i);
							int pageNum = item.getInt("page") -1;
							
							JsonObject location = new JsonObject();
							location.addProperty("id", item.getString("signId"));
							location.addProperty("x", item.getDouble("x"));
							location.addProperty("y", item.getDouble("y"));
							location.addProperty("width", item.getDouble("width"));
							location.addProperty("height", item.getDouble("height"));
							
							boolean found = false;
							for(int j=0; j<signFields.size(); j++){
								JsonObject _sf = signFields.get(j).getAsJsonObject();
								if(_sf.get("page").getAsInt() == pageNum){
									found = true;
									JsonArray _sl = _sf.getAsJsonArray("signLocation");
									_sl.add(location);
									break;
								}
							}
							
							if(!found){
								JsonArray signLocation = new JsonArray();
								signLocation.add(location);
								JsonObject signField = new JsonObject();
								signField.addProperty("page", pageNum);
								signField.add("signLocation", signLocation);
								signFields.add(signField);
							}
							
						}
					}
					JsonObject _signFields = reportTemplate.has("signFields")?reportTemplate.getAsJsonObject("signFields"): new JsonObject();
					_signFields.add(key.equals(Constants.BiPage.CLIENT_SIGN)?"client":"agent", signFields);
					reportTemplate.add("signFields", _signFields);
					break;
				case Constants.BiPage.BI_PAGES:
					JsonArray templates = new JsonArray();
					if(section.has("pages")){
						JSONArray pages = section.getJSONArray("pages");
						for(int i=0; i<pages.length(); i++){
							JSONObject page = pages.getJSONObject(i);
							if(page.has("page")){
								templates.add(replaceXslWord(biCode, version, images, Compressor.compressHtml(page.getString("page"))));
							}
						}
					}
					reportTemplate.add("templates", templates);
				default:
			}
		}
		
		return reportTemplate;
	}
	
	public String replaceXslWord(String biCode, int version, JSONObject images, String template) throws SQLException{
		BeneIllus bi = new BeneIllus();
		String[][] xslMap = {
			{"{{value-of=\"","<xsl:value-of select=\"", "\"}}", "\"/>"},
			{"{{if:test=\"","<xsl:if test=\"", "\"}}", "\">"},
			{"{{if:end}}", "</xsl:if>"},
			{"{{choose}}", "<xsl:choose>"},
			{"{{choose:end}}", "</xsl:choose>"},
			{"{{when:test=\"", "<xsl:when test=\"", "\"}}", "\">"},
			{"{{when:end}}", "</xsl:when>"},
			{"{{otherwise}}", "<xsl:otherwise>"},
			{"{{otherwise:end}}", "</xsl:otherwise>"},
			{"&nbsp;", " "},
			{"&lt;", "<"},
			{"&gt;",">"}
		};
		
		for(int i=0; i<xslMap.length; i++){
			template = replaceXslWord(biCode, version, images, xslMap[i], -1, template);
		}
		
		//image
		String[] imageKey = {"<img src=\"", "<img src=\"", "\"", "\""};
		template = replaceXslWord(biCode, version, images, imageKey, TYPE_IMAGE, template);
		
		for(String[] xsl : xslMap){
			template = template.replace(xsl[0], xsl[1]);
		}
		
		return template;		
	}
	
	private String replaceXslWord(String biCode, int version, JSONObject images, String[] replacePattern, int type, String template) throws SQLException{
		String result = "";
		String start = null;
		String rStart = null;
		String end = null;
		String rEnd = null;
		BeneIllus bi = new BeneIllus();
		if(replacePattern.length>=2){
			start=replacePattern[0];
			rStart=replacePattern[1];
		}
		if(replacePattern.length>=4){
			end=replacePattern[2];
			rEnd=replacePattern[3];
		}
		
		if(end == null){
			return template.replace(start, rStart);
		}else if(template.indexOf(start)>0){
			do{
				//get the string before {{image-src-s}}
				result += template.substring(0, template.indexOf(start));
				template = template.substring(template.indexOf(start)+start.length());
				
				//if no {{image-src-e}} in the string
				if(template.indexOf(end)<0){
					result += start;
					break;
				}else{
					//get the image src
					String condition = template.substring(0, template.indexOf(end));
					if(type==TYPE_IMAGE){
						condition = bi.getBiImage(biCode, version, images, condition, principal);
					}
					result += rStart + condition + rEnd;
					template = template.substring(template.indexOf(end)+end.length());
				}
			}while(template.indexOf(start)>0);
		}
		
		return result + template;
		
	}
	
	public String getXml(){
		return "<root><plan><MPremium>10,000.00</MPremium><permTerm>5</permTerm><planName>5 Year SP Endowment Plan</planName><sumAssured>10,000</sumAssured><APremium>10,000.00</APremium><premium>10,000.00</premium><policyTerm>5</policyTerm><HPremium>10,000.00</HPremium><QPremium>10,000.00</QPremium></plan><biValue><ngsvHigh>0</ngsvHigh><totaldbLow>10,100</totaldbLow><ngdbLow>0</ngdbLow><totaldbHigh>10,100</totaldbHigh><eodLow>0</eodLow><totalBasicPremPaid>10,000.00</totalBasicPremPaid><gdb>10,100</gdb><ngsvLow>0</ngsvLow><totalRiderPremPaid>0.00</totalRiderPremPaid><eodHigh>0</eodHigh><curBasicPrem>10,000.00</curBasicPrem><yearlyPremium>10,000.00</yearlyPremium><totalTdc>2,240.00</totalTdc><riderTdc>0.00</riderTdc><totalsvLow>10,100</totalsvLow><totalsvHigh>10,100</totalsvHigh><totaladbLow>0</totaladbLow><vpptdHigh>0</vpptdHigh><vpptdLow>0</vpptdLow><curRidersPrem>0.00</curRidersPrem><polYr>1</polYr><age>26</age><totalPremiumPaid>10,000.00</totalPremiumPaid><basicTdc>2,240.00</basicTdc><gsv>10,100</gsv><ngdbHigh>0</ngdbHigh><withdraw>0</withdraw><totaladbHigh>0</totaladbHigh></biValue><biValue><ngsvHigh>0</ngsvHigh><totaldbLow>11,027</totaldbLow><ngdbLow>0</ngdbLow><totaldbHigh>11,027</totaldbHigh><eodLow>0</eodLow><totalBasicPremPaid>20,000.00</totalBasicPremPaid><gdb>11,027</gdb><ngsvLow>0</ngsvLow><totalRiderPremPaid>0.00</totalRiderPremPaid><eodHigh>0</eodHigh><curBasicPrem>10,000.00</curBasicPrem><yearlyPremium>10,000.00</yearlyPremium><totalTdc>2,925.00</totalTdc><riderTdc>0.00</riderTdc><totalsvLow>10,502</totalsvLow><totalsvHigh>10,502</totalsvHigh><totaladbLow>0</totaladbLow><vpptdHigh>0</vpptdHigh><vpptdLow>0</vpptdLow><curRidersPrem>0.00</curRidersPrem><polYr>2</polYr><age>27</age><totalPremiumPaid>20,000.00</totalPremiumPaid><basicTdc>2,925.00</basicTdc><gsv>10,502</gsv><ngdbHigh>0</ngdbHigh><withdraw>0</withdraw><totaladbHigh>0</totaladbHigh></biValue><biValue><ngsvHigh>0</ngsvHigh><totaldbLow>11,301</totaldbLow><ngdbLow>0</ngdbLow><totaldbHigh>11,301</totaldbHigh><eodLow>0</eodLow><totalBasicPremPaid>30,000.00</totalBasicPremPaid><gdb>11,301</gdb><ngsvLow>0</ngsvLow><totalRiderPremPaid>0.00</totalRiderPremPaid><eodHigh>0</eodHigh><curBasicPrem>10,000.00</curBasicPrem><yearlyPremium>10,000.00</yearlyPremium><totalTdc>3,199.00</totalTdc><riderTdc>0.00</riderTdc><totalsvLow>10,763</totalsvLow><totalsvHigh>10,763</totalsvHigh><totaladbLow>0</totaladbLow><vpptdHigh>0</vpptdHigh><vpptdLow>0</vpptdLow><curRidersPrem>0.00</curRidersPrem><polYr>3</polYr><age>28</age><totalPremiumPaid>30,000.00</totalPremiumPaid><basicTdc>3,199.00</basicTdc><gsv>10,763</gsv><ngdbHigh>0</ngdbHigh><withdraw>0</withdraw><totaladbHigh>0</totaladbHigh></biValue><biValue><ngsvHigh>0</ngsvHigh><totaldbLow>11,582</totaldbLow><ngdbLow>0</ngdbLow><totaldbHigh>11,582</totaldbHigh><eodLow>0</eodLow><totalBasicPremPaid>40,000.00</totalBasicPremPaid><gdb>11,582</gdb><ngsvLow>0</ngsvLow><totalRiderPremPaid>0.00</totalRiderPremPaid><eodHigh>0</eodHigh><curBasicPrem>10,000.00</curBasicPrem><yearlyPremium>10,000.00</yearlyPremium><totalTdc>3,336.00</totalTdc><riderTdc>0.00</riderTdc><totalsvLow>11,030</totalsvLow><totalsvHigh>11,030</totalsvHigh><totaladbLow>0</totaladbLow><vpptdHigh>0</vpptdHigh><vpptdLow>0</vpptdLow><curRidersPrem>0.00</curRidersPrem><polYr>4</polYr><age>29</age><totalPremiumPaid>40,000.00</totalPremiumPaid><basicTdc>3,336.00</basicTdc><gsv>11,030</gsv><ngdbHigh>0</ngdbHigh><withdraw>0</withdraw><totaladbHigh>0</totaladbHigh></biValue><biValue><ngsvHigh>0</ngsvHigh><totaldbLow>11,868</totaldbLow><ngdbLow>0</ngdbLow><totaldbHigh>11,868</totaldbHigh><eodLow>0</eodLow><totalBasicPremPaid>50,000.00</totalBasicPremPaid><gdb>11,868</gdb><ngsvLow>0</ngsvLow><totalRiderPremPaid>0.00</totalRiderPremPaid><eodHigh>0</eodHigh><curBasicPrem>10,000.00</curBasicPrem><yearlyPremium>10,000.00</yearlyPremium><totalTdc>3,473.00</totalTdc><riderTdc>0.00</riderTdc><totalsvLow>11,303</totalsvLow><totalsvHigh>11,303</totalsvHigh><totaladbLow>0</totaladbLow><vpptdHigh>0</vpptdHigh><vpptdLow>0</vpptdLow><curRidersPrem>0.00</curRidersPrem><polYr>5</polYr><age>30</age><totalPremiumPaid>50,000.00</totalPremiumPaid><basicTdc>3,473.00</basicTdc><gsv>11,303</gsv><ngdbHigh>0</ngdbHigh><withdraw>0</withdraw><totaladbHigh>0</totaladbHigh></biValue><mainInfo><quarterlyPremium>10,000</quarterlyPremium><isSmoker>N</isSmoker><username>50000</username><basicPlanName><en>5 Year SP Endowment Plan</en><zh-Hant>5 Year SP Endowment Plan</zh-Hant></basicPlanName><age>26</age><halfYearlyPremium>10,000</halfYearlyPremium><premium>10,000</premium><sumAssured>10,000</sumAssured><currency>HKD</currency><yearlyPremium>10,000</yearlyPremium><gender>M</gender><name>David</name><monthlyPremium>10,000</monthlyPremium></mainInfo><sysDate>10/07/2016</sysDate><agentName>Kelly Chung</agentName><sig2><ANNUAL_BASIC_PREMIUM>200000</ANNUAL_BASIC_PREMIUM><ANNUAL_TOTAL_PREMIUM>200000</ANNUAL_TOTAL_PREMIUM><NET_RATE_ZERO>0</NET_RATE_ZERO><NET_RATE_LOW>3</NET_RATE_LOW><NET_RATE_MID>6</NET_RATE_MID><NET_RATE_HIGH>9</NET_RATE_HIGH><MGT_FEE_PERCENT_PLUS_NET_RATE_ZERO>1.5</MGT_FEE_PERCENT_PLUS_NET_RATE_ZERO><MGT_FEE_PERCENT_PLUS_NET_RATE_LOW>4.5</MGT_FEE_PERCENT_PLUS_NET_RATE_LOW><MGT_FEE_PERCENT_PLUS_NET_RATE_MID>7.5</MGT_FEE_PERCENT_PLUS_NET_RATE_MID><MGT_FEE_PERCENT_PLUS_NET_RATE_HIGH>10.5</MGT_FEE_PERCENT_PLUS_NET_RATE_HIGH><bpys><bpy><END_OF_POLICY_YEAR>1</END_OF_POLICY_YEAR><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>199120</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>226028</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>188159</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>213586</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>193640</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>219807</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>182679</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>207365</DB_AT_ZERO_NET_RATE></bpy><bpy><END_OF_POLICY_YEAR>2</END_OF_POLICY_YEAR><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>217779</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>243264</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>194463</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>217219</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>205956</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>230057</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>183300</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>204750</DB_AT_ZERO_NET_RATE></bpy><bpy><END_OF_POLICY_YEAR>3</END_OF_POLICY_YEAR><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>238108</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>261794</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>200912</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>220898</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>218984</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>240767</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>183863</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>202153</DB_AT_ZERO_NET_RATE></bpy><bpy><END_OF_POLICY_YEAR>4</END_OF_POLICY_YEAR><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>260249</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>281713</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>207507</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>224621</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>232759</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>251956</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>184367</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>200000</DB_AT_ZERO_NET_RATE></bpy><bpy><END_OF_POLICY_YEAR>5</END_OF_POLICY_YEAR><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>284357</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>303122</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>214249</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>228387</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>247321</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>263642</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>184783</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>200000</DB_AT_ZERO_NET_RATE></bpy><bpy><END_OF_POLICY_YEAR>10</END_OF_POLICY_YEAR><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>425654</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>446937</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>241789</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>253878</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>322095</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>338199</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>178856</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>200000</DB_AT_ZERO_NET_RATE></bpy><bpy><END_OF_POLICY_YEAR>15</END_OF_POLICY_YEAR><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>636833</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>668674</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>272865</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>286508</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>419361</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>440329</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>170341</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>200000</DB_AT_ZERO_NET_RATE></bpy><bpy><END_OF_POLICY_YEAR>20</END_OF_POLICY_YEAR><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>945798</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>993088</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>305678</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>320962</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>541997</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>569097</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>153620</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>200000</DB_AT_ZERO_NET_RATE></bpy><bpy><END_OF_POLICY_YEAR>25</END_OF_POLICY_YEAR><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>1391227</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>1460788</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>339162</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>356120</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>693797</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>728487</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>115159</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>200000</DB_AT_ZERO_NET_RATE></bpy><bpy><END_OF_POLICY_YEAR>30</END_OF_POLICY_YEAR><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>2011672</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>2112256</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>369923</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>388419</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>873028</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>916680</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>0</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>0</DB_AT_ZERO_NET_RATE></bpy><bpy><END_OF_POLICY_YEAR>35</END_OF_POLICY_YEAR><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>2831873</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>2973467</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>392802</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>412442</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>1069504</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>1122980</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>0</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>0</DB_AT_ZERO_NET_RATE></bpy><bpy><END_OF_POLICY_YEAR>40</END_OF_POLICY_YEAR><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>3830722</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>4022258</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>400802</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>420842</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>1259007</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>1321957</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>0</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>0</DB_AT_ZERO_NET_RATE></bpy><bpy><END_OF_POLICY_YEAR>41</END_OF_POLICY_YEAR><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>4044300</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>4246515</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>399947</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>419945</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>1292763</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>1357401</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>0</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>0</DB_AT_ZERO_NET_RATE></bpy></bpys><bas><ba><END_OF_POLICY_YEAR_A>31</END_OF_POLICY_YEAR_A><ILLUSTRATION_AGE>31 (at age 90)</ILLUSTRATION_AGE><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>2159488</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>2267463</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>375330</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>394097</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>911485</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>957059</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>0</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>0</DB_AT_ZERO_NET_RATE></ba><ba><END_OF_POLICY_YEAR_A>36</END_OF_POLICY_YEAR_A><ILLUSTRATION_AGE>36 (at age 95)</ILLUSTRATION_AGE><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>3019307</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>3170273</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>395838</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>415630</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>1109031</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>1164483</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>0</SV_AT_ZERO_NET_RATE><DB_AT_ZERO_NET_RATE>0</DB_AT_ZERO_NET_RATE></ba><ba><END_OF_POLICY_YEAR_A>41</END_OF_POLICY_YEAR_A><ILLUSTRATION_AGE>41 (at age 100)</ILLUSTRATION_AGE><TOTAL_PREMIUM_PAID>200000</TOTAL_PREMIUM_PAID><SV_AT_HIGH_NET_RATE>4044300</SV_AT_HIGH_NET_RATE><DB_AT_HIGH_NET_RATE>4246515</DB_AT_HIGH_NET_RATE><SV_AT_LOW_NET_RATE>399947</SV_AT_LOW_NET_RATE><DB_AT_LOW_NET_RATE>419945</DB_AT_LOW_NET_RATE><SV_AT_MID_NET_RATE>1292763</SV_AT_MID_NET_RATE><DB_AT_MID_NET_RATE>1357401</DB_AT_MID_NET_RATE><SV_AT_ZERO_NET_RATE>0</SV_AT_ZERO_NET_RATE></ba></bas></sig2></root>";
	}
	
	

}
