package com.eab.biz.beneIllus;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.beneIllus.BeneIllus;
import com.eab.json.model.Field;
import com.eab.json.model.Field.Types;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class BeneIllusDetailMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	BeneIllus bi;
	
	public BeneIllusDetailMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		bi = new BeneIllus();
	}
	
	public void addSection(List<Field> sections, String biCode, int version, boolean isNewBi, boolean canEditBiCode, boolean isReadOnly){
		try{
			Log.debug("---------------------[BeneIllusMgr - start adding section]--------------------------");
			List<Field> biDetailItems = new ArrayList<Field>();
	
			BeneIllus biDAO = new BeneIllus();			
			biDetailItems.add(new Field("biCode", Types.TEXT, null, langObj.getNameDesc("BI_CODE"), 100, "100%", null, true, (!canEditBiCode || isReadOnly), null, null, null, 20));
			List<Field> biNameLangs = new ArrayList<Field>();
			biNameLangs.add(new Field("en", langObj.getNameDesc("LANG.EN"), true));
			biNameLangs.add(new Field("zh-tw", langObj.getNameDesc("LANG.ZH-TW"), false));
			biDetailItems.add(new Field("biName", Types.MULTTEXT, null, langObj.getNameDesc("BI_NAME"), 300, "100%", null, true, isReadOnly, null, biNameLangs, null, 40));
			Field sectionProdDetail = new Field(Constants.BiPage.DETAIL, Types.SECTION, langObj.getNameDesc("DETAIL"), true, biDetailItems);
	
			sections.add(sectionProdDetail);
			
			Log.debug("---------------------[BeneIllusMgr - end adding section]--------------------------");
		}catch(Exception e){
			Log.error(e);
		}
	}
	
	public void addValues(JsonObject values, String biCode, int version) throws Exception{
		JsonObject biDetail = bi.getBi(biCode, principal, version, langObj);
		if (biDetail != null) {
			values.add(Constants.BiPage.DETAIL, biDetail);
		}
	}

}
