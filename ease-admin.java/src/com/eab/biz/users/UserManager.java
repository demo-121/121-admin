package com.eab.biz.users;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.biz.common.CommonManager;
import com.eab.biz.system.ResetPassword;
import com.eab.common.ActionFailureException;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.dynamic.DynamicDAO;
import com.eab.dao.dynamic.TemplateDAO;
import com.eab.dao.profile.User;
import com.eab.dao.users.UsersDAO;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.ListContent;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.SearchCondition;
import com.eab.json.model.Template;
import com.eab.json.model.Trigger;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.profile.UserBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.security.SysPrivilegeMgr;
import com.eab.webservice.dynamic.DynamicFunction;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class UserManager {

	String[] ids = new String[] { "userCode", "userType", "userName", "compCode", "channelCode", "depCode", "posCode", "gender", "homeNo", "officeNo", "mobileNo", "email", "status", "createDate", "createBy", "modifyDate", "modifyBy", "approveDate", "approveBy" };
	String[] keys = new String[] { "user_code", "user_type", "user_name", "comp_code", "channel_code", "dep_code", "pos_code", "gender", "home_no", "office_no", "mobile_no", "email", "status", "create_date", "create_by", "modify_date", "modify_by", "approv_date", "approv_by" };
	boolean[] mandate = new boolean[] { true, true, true, false, false, false, false, false, false };

	String[] accessIds = new String[] { "userCode", "company", "channel", "role", "status", "createDate", "createBy", "modifyDate", "modifyBy", "approveDate", "approveBy" };
	String[] accessKeys = new String[] { "user_code", "comp_code", "channel_code", "role_code", "status", "create_date", "create_by", "modify_date", "modify_by", "approv_date", "approv_by" };

	String module = "user";

	Map<String, String> idMap = null;
	Map<String, String> keyMap = null;

	Connection conn = null;
	UsersDAO dao = null;
	HttpServletRequest request = null;
	HttpSession session = null;
	UserPrincipalBean principal = null;
	
	public UserManager(HttpServletRequest request) {
		super();
		this.request = request;
		this.session = request.getSession();

		try {
			conn = DBAccess.getConnection();
			dao = new UsersDAO(conn, ids, keys, accessIds, accessKeys);
		} catch (SQLException e) {
		}

		idMap = new HashMap<String, String>();
		keyMap = new HashMap<String, String>();

		for (int i = 0; i < this.ids.length; i++) {
			idMap.put(ids[i], keys[i]);
			keyMap.put(keys[i], ids[i]);
		}
		principal = Function.getPrincipal(request);
	}

	/**
	 * @param request
	 * @return Response
	 */
	public Response getTableView() {
		SearchCondition cond = SearchCondition.RetrieveSearchCondition(request);
		Response resp = null;
		String error = null;
		DynamicDAO dynamic = new DynamicDAO();
		//filter by status first	
		String statusStr = cond.getStatusFilter();
		String sortBy = cond.getSortBy();
		String sortDir = cond.getSortDir();

		int offset = cond.getRecordStart();
		int size = cond.getPageSize();

		try {
			// get principal

			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			String compCode = principal.getCompCode();
			String templateType = "table";
			List<String> additionalNames = Arrays.asList("MT.FOOTER");
			List<String> names = new ArrayList<String>();
			names.addAll(additionalNames);

			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}

			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			}

			if (template == null || !module.equals(template.getId())) {
				tempJson = new JsonObject();
				TemplateDAO tempalteDao = new TemplateDAO();
				template = tempalteDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
			}

			template.setType(templateType);

			//			initKeyMap(tempJson);

			// convert sort by
			if (sortBy == null || sortBy.isEmpty()) {
				sortBy = "modify_date";
				sortDir = "D";
				cond.setSortBy("modifyDate");
				cond.setSortDir("D");
			} else {
				if (idMap.containsKey(sortBy)) {
					sortBy = idMap.get(sortBy);
				}
			}
			// get granded uri list to filter the app bar
			List<String> uriList = null;
			SysPrivilegeMgr spMgr = new SysPrivilegeMgr();
			new ArrayList<String>();
			spMgr.getGrandedURIList(principal.getRoleCode(), uriList);

			// get app bar
			AppBar appBar = dynamic.genAppBar(conn, ui, templateType, uriList);
			List<Field> acts = dynamic.getActions(conn, ui, "multiSelect", uriList);
			if (acts != null) {
				appBar.getActions().add(acts);
			}
			appBar.setValues(cond);

			// translate
			appBar.getNameCodes(names);
			template.getNameCodes(names);
			Map<String, String> lmap = Function2.getTranslateMap(request, names);
			appBar.translate(lmap);
			template.translate(lmap);

			JsonObject langMap = new JsonObject();
			for (String name : additionalNames) {
				langMap.addProperty(name, lmap.get(name));
			}

			// get result
			String filterKey = cond.getSearchKey();
			String curFilterKey = session.getAttribute(Constants.SessionKeys.FILTER_KEY) != null ? (String) session.getAttribute(Constants.SessionKeys.FILTER_KEY) : null;

			JsonArray result = null;
			if (offset > 0 && filterKey.equals(curFilterKey)) {
				Object temp = session.getAttribute(Constants.SessionKeys.SEARCH_RESULT);
				result = temp != null && temp instanceof JsonArray ? (JsonArray) temp : null;
			}

			if (result == null) {
//				boolean isAdmin = Function.isCurrentUserAdmin(request);
				result = dao.getUserMaster(cond, statusStr, sortBy, sortDir, tempJson.getAsJsonArray("items"), ui, compCode);
				session.setAttribute(Constants.SessionKeys.FILTER_KEY, filterKey);
				session.setAttribute(Constants.SessionKeys.SEARCH_RESULT, result);
			}

			JsonArray shortedList = new JsonArray();
			for (int j = offset; j < offset + size && j < result.size(); j++) {
				shortedList.add(result.get(j));
			}
			ListContent list = new ListContent(shortedList, result.size());
			list.setIsMore(offset > 0);

			Content content = new Content(template, list);
			content.setLangMap(langMap);

			// Page
			Page page = new Page("/Users", lmap.get("USER.TITLE"));

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			if (cond.isUseDefault()) {
				resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, appBar, content);
			} else {
				resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, page, tokenID, tokenKey, appBar, content);
			}

		} catch (Exception ex) {
			Log.error(ex);
			error = ex.getMessage();
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}

		return resp;
	}

	//	private void initKeyMap(JsonObject tempJson) {
	//		JsonArray items = tempJson.getAsJsonArray("items");
	////		
	//		String[] ids = new String[items.size()];
	//		String[] keys = new String[items.size()];
	//		
	//		idMap = new HashMap<String, String>();
	//		keyMap = new HashMap<String, String>();
	//		
	////		for (int i = 0; i<ids.length; i++) {
	//		for (int i = 0; i<items.size(); i++) {
	//			JsonObject item = items.get(i).getAsJsonObject();
	//			
	//			ids[i] = item.get("id").getAsString();
	//			
	//			if (item.has("key") && !item.get("key").isJsonNull()) {
	//				keys[i] = item.get("key").getAsString();
	//				
	//				idMap.put(ids[i], keys[i]);
	//				keyMap.put(keys[i], ids[i]);				
	//			}
	//		}
	//	}

	public Response genAddNew() {
		Response resp = null;
		String error = null;
		DynamicDAO dynamic = new DynamicDAO();
		try {

			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			String compCode = principal.getCompCode();

			String templateType = "add";

			List<String> additionalNames = Arrays.asList("BUTTON.ADD_NEW_ACCESS_RIGHT", "BUTTON.ADD", "BUTTON.SAVE", "BUTTON.CANCEL", "DIALOG.ADD_NEW_ACCESS_RIGHT.TITLE");
			List<String> names = new ArrayList<String>();
			names.addAll(additionalNames);

			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}

			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			}

			if (template == null || !module.equals(template.getId())) {
				tempJson = new JsonObject();
				TemplateDAO tempalteDao = new TemplateDAO();
				template = tempalteDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
			}

			template.setType(templateType);
			template.setTitle("USER.ADD.TITLE");

			//			initKeyMap(tempJson);
			//
			//			for (Field item : template.getItems()) {
			//				if ("access".equals(item.getId())) {
			//					
			//				}
			//			}
			
			// get granded uri list to filter the app bar
			SysPrivilegeMgr spMgr = new SysPrivilegeMgr();
			List<String> uriList = new ArrayList<String>();
			spMgr.getGrandedURIList(principal.getRoleCode(), uriList);

			// get app bar
			AppBar appBar = dynamic.genAppBar(conn, ui, templateType, uriList);

			appBar.getTitle().setSecondary("USER.NEW_USER");

			// translate
			template.getNameCodes(names);
			appBar.getNameCodes(names);
			Map<String, String> lmap = Function2.getTranslateMap(request, names);
			template.translate(lmap);
			appBar.translate(lmap);

			JsonObject langMap = new JsonObject();
			for (String name : additionalNames) {
				langMap.addProperty(name, lmap.get(name));
			}

			// get details
			Content content = new Content(template, null);
			content.setLangMap(langMap);

			JsonObject changedValues = new JsonObject();
			changedValues.addProperty("isNew", true);

			content.setChangedValues(changedValues);

			// Page
			Page page = new Page("/Users/Details", "User Master");

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, appBar, content);

		} catch (Exception ex) {
			Log.error(ex);
			error = ex.getMessage();
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}

		}

		return resp;
	}

	public Response getDetailView() {
		Response resp = null;
		String error = null;

		String uid = request.getParameter("p0");
		DynamicDAO dynamic = new DynamicDAO();
		try {
			if (uid == null || uid.isEmpty()) {
				//	Log.debug("load user user details by id:" + uid);
				throw new Exception("not user  id found: " + uid);
			}

			String templateType = null;

			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			String compCode = principal.getCompCode();

			List<String> additionalNames = Arrays.asList("DYN.DELETE.DATEBY", "DYN.CURRENT.DATEBY", "DYN.CHANGED.DATEBY", "DYN.CURRENT", "DYN.CHANGED", "BUTTON.ADD_NEW_ACCESS_RIGHT", "BUTTON.ADD", "BUTTON.SAVE", "BUTTON.CANCEL", "DIALOG.ADD_NEW_ACCESS_RIGHT.TITLE");
			List<String> names = new ArrayList<String>();
			names.addAll(additionalNames);

			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}

			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			}

			if (template == null || !module.equals(template.getId())) {
				tempJson = new JsonObject();
				TemplateDAO tempalteDao = new TemplateDAO();
				template = tempalteDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
			}

//			initKeyMap(tempJson);
			List<JsonObject> users = dao.getUserById(tempJson.getAsJsonArray("items"), uid, ui.getPrimaryKey());

			JsonObject userMst = null;
			JsonObject userTmp = null;

			String Action = null;
			String UpdBy = null;

			for (JsonObject user : users) {
				if (user.has("action") && !user.get("action").isJsonNull()) {
					String action = user.get("action").getAsString();
					if (action != null && !action.isEmpty()) {
						Action = action;
						if (user.has("updateBy") && !user.get("updateBy").isJsonNull()) {
							UpdBy = user.get("updateBy").getAsString();
							user.remove("updateBy");
						}
						if (userTmp != null) {
							throw new Exception("retrieve corrupted");
						}
						userTmp = user;
					} else {
						if (userMst != null) {
							throw new Exception("retrieve corrupted");
						}
						userMst = user;
					}
				} else {
					if (userMst != null) {
						throw new Exception("retrieve corrupted");
					}
					userMst = user;
				}
			}

			if (userMst == null && userTmp == null) {
				throw new Exception("retrieve corrupted");
			}

			// AppBar - Title
			String secondary = "USER.NEW_USER";
			if (userMst != null && userMst.has("userName") && !userMst.get("userName").isJsonNull()) {
				secondary = userMst.get("userName").getAsString();
			} else if (userTmp != null && userTmp.has("userName") && !userTmp.get("userName").isJsonNull()) {
				secondary = userTmp.get("userName").getAsString();
			}

			AppBarTitle appBarTitle = new AppBarTitle("levelTwo", "", null, secondary);
			CommonManager cMgr = new CommonManager(request);
			cMgr.setAppbarPrimary(appBarTitle, "/Users");

			List<List<Field>> appBarActions = new ArrayList<>();
			String pageTitle = "";
			
			// get granded uri list to filter the app bar
			SysPrivilegeMgr spMgr = new SysPrivilegeMgr();
			List<String> uriList = new ArrayList<String>();
			spMgr.getGrandedURIList(principal.getRoleCode(), uriList);

			// AppBar - Actions
			if (Action == null || Action.isEmpty()) {
				// TODO check update right
				templateType = "details";
				List<Field> acts = dynamic.getActions(conn, ui, templateType, uriList);
				if (acts != null) {
					appBarActions.add(acts);
				}
				pageTitle = "USER.VIEW.TITLE";
			} else if ("N".equals(Action)) {
				// TODO check update right
				templateType = "add";
				if ("A".equals(principal.getUserType()) || !principal.getUserCode().equals(UpdBy)) {
					List<Field> aacts = dynamic.getActions(conn, ui, "approve", uriList);
					if (aacts != null) {
						appBarActions.add(aacts);
					}
				}
				List<Field> acts = dynamic.getActions(conn, ui, templateType, uriList);
				if (acts != null) {
					appBarActions.add(acts);
				}
				pageTitle = "USER.ADD.TITLE";
			} else if ("E".equals(Action)) {
				// TODO check update right
				templateType = "edit";
				if ("A".equals(principal.getUserType()) || !principal.getUserCode().equals(UpdBy)) {
					List<Field> aacts = dynamic.getActions(conn, ui, "approve", uriList);
					if (aacts != null) {
						appBarActions.add(aacts);
					}
				}
				List<Field> acts = dynamic.getActions(conn, ui, templateType, uriList);
				if (acts != null) {
					appBarActions.add(acts);
				}
				pageTitle = "USER.EDIT.TITLE";
			} else if ("D".equals(Action)) {
				// TODO check update right
				templateType = "delete";
				if ("A".equals(principal.getUserType()) || !principal.getUserCode().equals(UpdBy)) {
					List<Field> aacts = dynamic.getActions(conn, ui, "approve", uriList);
					if (aacts != null) {
						appBarActions.add(aacts);
					}
				}
				List<Field> acts = dynamic.getActions(conn, ui, templateType, uriList);
				if (acts != null) {
					appBarActions.add(acts);
				}
				pageTitle = "USER.DEL.TITLE";
			}

			// refine fields 
			for (Field item : template.getItems()) {
				if ("access".equals(item.getId())) {
					if (principal.isRegional()) {
						// allow for all
					} else {
						List<Trigger> trigger = new ArrayList<Trigger>();
						trigger.add(new Trigger("disableIfNotEqual", "company", compCode));
						item.setTrigger(trigger);
					}
				}
				if ("edit".equals(templateType) && !item.isAllowUpdate()) {
					item.setDisabled(true);
				}
			}

			// AppBar
			AppBar appBar = new AppBar(appBarTitle, appBarActions);

			template.setTitle(pageTitle);
			template.setType(templateType);

			// translate
			appBar.getNameCodes(names);
			template.getNameCodes(names);
			Map<String, String> lmap = Function2.getTranslateMap(request, names);
			appBar.translate(lmap);
			template.translate(lmap);

			JsonObject langMap = new JsonObject();
			for (String name : additionalNames) {
				langMap.addProperty(name, lmap.get(name));
			}

			Log.debug("mst:" + (userMst != null && !userMst.isJsonNull() ? userMst : ""));
			Log.debug("tmp:" + (userTmp != null && !userTmp.isJsonNull() ? userTmp : ""));

			Content content = new Content(template, userMst, userTmp);
			content.setLangMap(langMap);

			// Page
			Page page = new Page("/Users/Details", "User Role Master");

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, appBar, content);

		} catch (Exception ex) {
			Log.error(ex);
			error = ex.getMessage();
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}

		return resp;
	}

	public Response genEdit() {
		Response resp = null;

		String uid = request.getParameter("p0");
		DynamicDAO dynamic = new DynamicDAO();
		try {
			if (uid == null || uid.isEmpty()) {
				//	Log.debug("load user  details by id:" + uid);
				throw new Exception("not user  id found: " + uid);
			}

			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			String compCode = principal.getCompCode();
			String templateType = "edit";

			List<String> additionalNames = Arrays.asList("DYN.DELETE.DATEBY", "DYN.CURRENT.DATEBY", "DYN.CHANGED.DATEBY", "DYN.CURRENT", "DYN.CHANGED", "BUTTON.ADD_NEW_ACCESS_RIGHT", "BUTTON.ADD", "BUTTON.SAVE", "BUTTON.CANCEL", "DIALOG.ADD_NEW_ACCESS_RIGHT.TITLE");
			List<String> names = new ArrayList<String>();
			names.addAll(additionalNames);

			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}

			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			}

			if (template == null || !module.equals(template.getId())) {
				tempJson = new JsonObject();
				TemplateDAO tempalteDao = new TemplateDAO();
				template = tempalteDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
			}

			template.setType(templateType);

			//			initKeyMap(tempJson);

			// get record

			for (Field item : template.getItems()) {
				if ("access".equals(item.getId())) {
					if (principal.isRegional()) {
						// allow for all
					} else {
						List<Trigger> trigger = new ArrayList<Trigger>();
						trigger.add(new Trigger("disableIfNotEqual", "company", compCode));
						item.setTrigger(trigger);
					}
				}
				if ("edit".equals(templateType) && !item.isAllowUpdate()) {
					item.setDisabled(true);
				}
			}

			List<JsonObject> users = dao.getUserById(tempJson.getAsJsonArray("items"), uid, ui.getPrimaryKey());

			JsonObject userMst = null;

			if (users.size() > 0) {
				userMst = users.get(0);
			}

			if (userMst == null) {
				throw new Exception("not record found:" + uid);
			}

			String secondary = "No name";
			if (userMst.has("userName") && !userMst.get("userName").isJsonNull()) {
				secondary = userMst.get("userName").getAsString();
			}

			// get granded uri list to filter the app bar
			SysPrivilegeMgr spMgr = new SysPrivilegeMgr();
			List<String> uriList = new ArrayList<String>();
			spMgr.getGrandedURIList(principal.getRoleCode(), uriList);

			// AppBar - Title
			AppBarTitle appBarTitle = new AppBarTitle("/Users", "levelTwo", "", "USER.TITLE", null, secondary);
			List<List<Field>> appBarActions = new ArrayList<>();
			appBarActions.add(dynamic.getActions(conn, ui, templateType, uriList));

			// AppBar
			AppBar appBar = new AppBar(appBarTitle, appBarActions);

			String pageTitle = "USER.EDIT.TITLE";
			template.setTitle(pageTitle);

			// translate
			appBar.getNameCodes(names);
			template.getNameCodes(names);
			Map<String, String> lmap = Function2.getTranslateMap(request, names);
			appBar.translate(lmap);
			template.translate(lmap);

			JsonObject langMap = new JsonObject();
			for (String name : additionalNames) {
				langMap.addProperty(name, lmap.get(name));
			}

			userMst.addProperty("isNew", true);

			Content content = new Content(template, userMst, userMst);
			content.setLangMap(langMap);
			// Page
			Page page = new Page("/Users/Details", "User  Master");

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, page, tokenID, tokenKey, appBar, content);
		} catch (Exception ex) {
			Log.error(ex);

			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}

		return resp;
	}

	public Response genDelete() {
		Response resp = null;
		Dialog dialog = null;
		String uid = request.getParameter("p0");

		try {
			if (uid == null || uid.isEmpty()) {
				//	Log.debug("load user user details by id:" + uid);
				throw new Exception("not user user id found: " + uid);
			}

			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			String compCode = principal.getCompCode();
			String templateType = "delete";

			List<String> additionalNames = Arrays.asList("DYN.CURRENT.TITLE", "DYN.CHANGED.TITLE");
			List<String> names = new ArrayList<String>();
			names.addAll(additionalNames);

			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				DynamicDAO dynamic = new DynamicDAO();
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}

			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			}

			if (template == null || !module.equals(template.getId())) {
				tempJson = new JsonObject();
				TemplateDAO tempalteDao = new TemplateDAO();
				template = tempalteDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
			}

			template.setType(templateType);

			//			initKeyMap(tempJson);

			if(!uid.equals(principal.getUserCode())){
				if (!dao.deleteUser(uid, principal.getUserCode()) || !dao.insertAudit(uid)) { // Action 'D' for delete
					throw new Exception("insertion error!!");
				}
			}else{
				Field positive = new Field("ok", "button", "BUTTON.OK");
				dialog = new Dialog("deleteDialog", "USER.DELETE.TITLE", "USER.DELETE.MSG.DELETE_SELF", null, positive);
			}

			// back to table view
			// Page
			Page page = new Page("/Users", null);

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);
			
			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, null, null);
			if(dialog != null){
				dialog.translate(request);
				resp.setDialog(dialog);
			}

		} catch (Exception ex) {
			Log.error(ex);

			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}

		return resp;
	}

	public Response saveAdd() {
		Response resp = null;
		try {

			String p0 = request.getParameter("p0");
			JsonObject userJson = null;
			if (p0 != null && !p0.isEmpty()) {
				p0 = new String(Base64.getDecoder().decode(p0.replace(" ", "+")), "UTF-8");
				JsonParser parser = new JsonParser();
				userJson = parser.parse(p0).getAsJsonObject();
			} else {
				throw new Exception("missing para!!!");
			}

			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				DynamicDAO dynamic = new DynamicDAO();
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}

			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			} else { // 
				throw new Exception("Invalid action.");
			}

			// valiate and refine the user json
			List<Field> fields = template.getItems();
			String defComp = null, defChannel = null;
			for (Field field : fields) {
				String id = field.getId();
				if (field.isMandatory() && !(userJson.has(id) && !userJson.get(id).isJsonNull())) {
					throw new ActionFailureException("Missing mandatary :" + id, true, true);
				}

				if ("access".equals(id)) {
					JsonArray accesses = userJson.getAsJsonArray("access");
					for (int a = 0; a < accesses.size(); a++) {
						JsonObject acc = accesses.get(a).getAsJsonObject();
						for (Field item : field.getItems()) {
							String sid = item.getId();
							Log.debug("checking access, field:" + sid);
							if (acc.has(sid) && !acc.get(sid).isJsonNull()) {
								if ("default".equals(sid)) {
									String def = acc.get(sid).getAsString();
									if ("Y".equals(def)) {
										defComp = acc.get("company").getAsString();
										defChannel = acc.get("channel").getAsString();
									}
								}
							} else if (item.isMandatory()) {
								throw new ActionFailureException("Missing mandatary :" + sid, true, true);
							}
						}
					}
					if (defComp == null || defChannel == null) {
						throw new ActionFailureException("Missing default :" + id, true, true);
					} else {
						userJson.addProperty("compCode", defComp);
						userJson.addProperty("channelCode", defChannel);
					}
				}
			}

			String uid = userJson.get(keyMap.get(ui.getPrimaryKey())).getAsString();

			boolean isNew = false;
			// isNew is set when press Add button only 
			if (userJson.has("isNew") && !userJson.get("isNew").isJsonNull()) {
				isNew = userJson.get("isNew").getAsBoolean();
			}
			
			if (Function2.hasRecord(ui.getDataTable(), ui.getPrimaryKey(), uid, dao.getManager(), conn)) {
				throw new ActionFailureException("Duplicate code found, please use any other code.", true, true);
			} else if (Function2.hasRecord(ui.getTempTable(), ui.getPrimaryKey(), uid, dao.getManager(), conn)) {
				if (isNew) { // if it is new and a temp record already exist, it is a duplicate
					throw new ActionFailureException("Duplicate code found, please use any other code.", true, true);
				} else
				// else it is editing a waiting approve new record
				if (!dao.saveUpdate(uid, userJson, principal.getUserCode(), "N") || !dao.insertAudit(uid)) {
					throw new Exception("insertion error!!");
				}
			} else {
				if (isNew) { // if it is new and not temp record found
					if (!dao.saveInsert(uid, userJson, principal.getUserCode(), null, null, "N") || !dao.insertAudit(uid)) { // N for New 
						throw new Exception("insertion error!!");
					}
				} else { // otherwise, it is a error
//					throw new Exception("insertion error!!");
					String oid = userJson.get("id").getAsString();
				
					if (Function2.hasRecord(ui.getTempTable(), ui.getPrimaryKey(), oid, dao.getManager(), conn)) {
						if (!dao.insertAudit(oid, principal.getUserCode(), "X") 
								|| !dao.removeTmp(oid)) {
							throw new Exception("changed new record id error!!");
						} else {
							if (!dao.saveInsert(uid, userJson, principal.getUserCode(), null, null, "N") || !dao.insertAudit(uid)) { // N for New 
								throw new Exception("changed new record id error!!");
							}
						}
					} else {
						throw new Exception("changed new record id error!!");
					}

				}
			}

			// back to table view			
			// Page
			Page page = new Page("/Users", null);

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, null, null);

		} catch (ActionFailureException ex) {
			Log.error(ex);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} catch (Exception ex) {
			Log.error(ex);

			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}

		return resp;
	}

	public Response saveEdit() {
		Response resp = null;
		try {

			String p0 = request.getParameter("p0");
			JsonObject userJson = null;
			if (p0 != null && !p0.isEmpty()) {
				p0 = new String(Base64.getDecoder().decode(p0.replace(" ", "+")), "UTF-8");
				JsonParser parser = new JsonParser();
				userJson = parser.parse(p0).getAsJsonObject();
			} else {
				throw new Exception("missing para!!!");
			}

			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				DynamicDAO dynamic = new DynamicDAO();
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}

			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			} else { // 
				throw new Exception("Invalid action.");
			}

			// valiate
			List<Field> fields = template.getItems();
			String defComp = null, defChannel = null;
			for (Field field : fields) {
				String id = field.getId();
				if (field.isMandatory() && !(userJson.has(id) && !userJson.get(id).isJsonNull())) {
					throw new ActionFailureException("Missing mandatary:" + id, true, true);
				}

				if ("access".equals(id)) {
					JsonArray accesses = userJson.getAsJsonArray("access");
					for (int a = 0; a < accesses.size(); a++) {
						JsonObject acc = accesses.get(a).getAsJsonObject();
						for (Field item : field.getItems()) {
							String sid = item.getId();
							if (item.isMandatory() && !(acc.has(sid) && !acc.get(sid).isJsonNull())) {
								throw new ActionFailureException("Missing mandatary :" + id, true, true);
							} else if ("default".equals(sid)) {
								if (acc.has(sid) && !acc.get(sid).isJsonNull()) {
									String def = acc.get(sid).getAsString();
									if ("Y".equals(def)) {
										defComp = acc.get("company").getAsString();
										defChannel = acc.get("channel").getAsString();
									}
								}
							}

						}
					}
					if (defComp == null) {
						throw new ActionFailureException("Missing default :" + id, true, true);
					} else {
						userJson.addProperty("compCode", defComp);
						userJson.addProperty("channelCode", defChannel);
					}
				}
			}

			boolean isNew = false;
			// isNew is set when press Edit button only 
			if (userJson.has("isNew") && !userJson.get("isNew").isJsonNull()) {
				isNew = userJson.get("isNew").getAsBoolean();
			}

			String uid = userJson.get(keyMap.get(ui.getPrimaryKey())).getAsString();

			if (Function2.hasRecord(ui.getDataTable(), ui.getPrimaryKey(), uid, dao.getManager(), conn)) {
				if (Function2.hasRecord(ui.getTempTable(), ui.getPrimaryKey(), uid, dao.getManager(), conn)) {
					if (isNew) { // if it is new and a temp record already exist, it is a duplicate
						throw new Exception("edit error!!");
					} // else update the temp record
					else if (!dao.saveUpdate(uid, userJson, principal.getUserCode(), "E") || !dao.insertAudit(uid)) {
						throw new Exception("update error!!");
					}
				} else if (isNew) { // if it is a new edit, save by insert new edit
					String createBy = (String) Function2.getSingleColumnFromRecord(ui.getDataTable(), "create_by", ui.getPrimaryKey(), uid, DataType.TEXT, dao.getManager(), conn);
					Date createDate = (Date) Function2.getSingleColumnFromRecord(ui.getDataTable(), "create_date", ui.getPrimaryKey(), uid, DataType.DATE, dao.getManager(), conn);
					if (!dao.saveInsert(uid, userJson, principal.getUserCode(), createDate, createBy, "E") || !dao.insertAudit(uid)) { // E for Edit
						throw new Exception("edit error!!");
					}
				} else {
					throw new Exception("edit error!!");
				}
			} else {
				throw new Exception("edit error!!");
			}

			// Page
			Page page = new Page("/Users", null);

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, null, null);

		} catch (ActionFailureException ex) {
			// TODO:: error message handling
			Log.error(ex);
			//			Content content = new Content();

			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} catch (Exception ex) {
			Log.error(ex);

			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}

		return resp;
	}

	public Response approve() {
		Response resp = null;

		String uid = request.getParameter("p0");
		Log.info("UserManager approve uid: " + uid);

		try {
			if (uid == null || uid.isEmpty()) {
				//	Log.debug("load user user details by id:" + uid);
				throw new Exception("not user user id found: " + uid);
			}

			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}

			// check if maker is save as checker
			String UpdateBy = (String) Function2.getSingleColumnFromRecord("sys_user_role_mst_tmp", "modify_by", "role_code", uid, DataType.TEXT, dao.getManager(), conn);
			if (!(!principal.getUserCode().equals(UpdateBy) || Constants.USERTYPE_ADMIN.equals(principal.getUserType()))) {
				throw new ActionFailureException("ERROR.INVAILD_MAC_ACCESS", true, true);
			}

			String compCode = principal.getCompCode();

			List<String> additionalNames = Arrays.asList("DYN.CURRENT.TITLE", "DYN.CHANGED.TITLE");
			List<String> names = new ArrayList<String>();
			names.addAll(additionalNames);

			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				DynamicDAO dynamic = new DynamicDAO();
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}

			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			}

			if (template == null || !module.equals(template.getId())) {
				tempJson = new JsonObject();
				TemplateDAO tempalteDao = new TemplateDAO();
				template = tempalteDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
			}

			//			initKeyMap(tempJson);

			String Action = dao.getUserAction(uid);
			if ("N".equals(Action)) {
				if (!dao.approveAdd(uid, principal.getUserCode())
						|| !dao.insertAudit(uid, principal.getUserCode(), "A") // A for approve Add
						|| !dao.removeTmp(uid)) {
					throw new Exception("insertion error!!");
				}

				//added user, set email for setting password
				Log.debug("-------->set email for setting password");
				ResetPassword rp = new ResetPassword();
				User currentUser = new User();
				UserBean ub = currentUser.get(uid, request);
				rp.apply(ub.getUserCode(), ub.getUserName(), ub.getEmail(), ub.getLangCode(), "/Users/Approve");

			} else if ("E".equals(Action)) {
				if (!dao.approveEdit(uid, principal.getUserCode())
						|| !dao.insertAudit(uid, principal.getUserCode(), "U") // U for approve update
						|| !dao.removeTmp(uid)) {
					throw new Exception("insertion error!!");
				}
			} else if ("D".equals(Action)) {
				if (!dao.approveDelete(uid, principal.getUserCode())
						|| !dao.insertAudit(uid, principal.getUserCode(), "R") // R for approve delete 
						|| !dao.removeTmp(uid)) {
					throw new Exception("insertion error!!");
				}
			} else {
				throw new Exception("insertion error!!");
			}

			// Page
			Page page = new Page("/Users", null);

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, null, null);

		} catch (Exception ex) {
			Log.error(ex);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}

		return resp;
	}

	public Response revert() {
		Response resp = null;

		String uid = request.getParameter("p0");

		try {
			if (uid == null || uid.isEmpty()) {
				// revert a new record, do nothing
			} else {

				if (principal == null) {
					throw new ActionFailureException("Session time out.", true, true);
				}
				String compCode = principal.getCompCode();

				List<String> additionalNames = Arrays.asList("DYN.CURRENT.TITLE", "DYN.CHANGED.TITLE");
				List<String> names = new ArrayList<String>();
				names.addAll(additionalNames);

				// get dynamic ui
				DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
				if (ui == null || !module.equals(ui.getModule())) {
					DynamicDAO dynamic = new DynamicDAO();
					ui = dynamic.getDynamicUI(module, conn);
					session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
				}

				// get dynamic template
				Template template = null;
				JsonObject tempJson = null;
				Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
				if (cTemp != null && cTemp instanceof JsonObject) {
					tempJson = (JsonObject) cTemp;
					Gson gson = new Gson();
					template = gson.fromJson(tempJson, Template.class);
				}

				if (template == null || !module.equals(template.getId())) {
					tempJson = new JsonObject();
					TemplateDAO tempalteDao = new TemplateDAO();
					template = tempalteDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
					session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
				}

				//			initKeyMap(tempJson);

				String Action = dao.getUserAction(uid);

				if (Action != null) {
					if (!dao.insertAudit(uid, principal.getUserCode(), "X") // X for reject 
							|| !dao.removeTmp(uid)) {
						throw new Exception("insertion error!!");
					}
				} else {
					// just close
				}
			}

			// Page
			Page page = new Page("/Users", null);

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, null, null);

		} catch (Exception ex) {
			Log.error(ex);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}

		return resp;
	}
}
