package com.eab.biz.dynTemplate;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.Function2;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.dynTemplate.DynTemplate;
import com.eab.dao.dynamic.Companies;
import com.eab.dao.dynamic.DynamicDAO;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.DialogItem;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.model.dynTemplate.DynField;
import com.eab.model.dynTemplate.DynUI;
import com.eab.model.dynamic.CompanyBean;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.webservice.dynamic.DynamicFunction;
import com.google.gson.JsonObject;

import javafx.scene.transform.Translate;

public class ListMgr extends DynMgr{
	public static String DYN_TEMPLATE = "DYN_TEMPLATE";

	public ListMgr(HttpServletRequest request) {
		super(request);
	}

	public JSONObject getTemplate(List<DynField> headers, String module) {
		String templateNameInSession = DYN_TEMPLATE + "_" + module.toUpperCase();
		if (session.getAttribute(templateNameInSession) != null) {
			return (JSONObject) session.getAttribute(templateNameInSession);
		} else {
			JSONObject template = new JSONObject();
			template.put("id", module);
			template.put("type", "table");
			int size = headers.size();
			JSONArray items = new JSONArray();
			for (int i = 0; i < size; i++) {
				JSONObject item = new JSONObject();
				DynField field = headers.get(i);
				item.put("id", field.getId());
				item.put("listSeq", field.getSeq());
				item.put("type", field.getType());
				item.put("title", field.getTitle());
				items.put(item);
			}
			template.put("items", items);
			return template;
		}
	}
	
	public void makeMstFilter(String filter, DynUI ui, DBManager dbm){
		try{
			if(ui.hasCompFilter()){
				String newFilter = " comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT);
				if("".equals(filter)){
					filter = newFilter;
				}else{
					filter = " and " + newFilter;
				}
			}
			
			if(ui.hasLangFilter()){
				String newFilter = " lang_code = " + dbm.param(principal.getCompCode(), DataType.TEXT);
				if("".equals(filter)){
					filter = newFilter;
				}else{
					filter = " and " + newFilter;
				}
			}
		}catch(Exception e){
			Log.error("getMstFilter error ");
			e.printStackTrace();
		}
	}
	
	public String getFieldKey(List<DynField> headers, String id){
		int size = headers.size();
		
		for(int i = 0; i < size; i++){
			DynField f = headers.get(i);
			if(id.equals(f.getId())){
				return f.getKey();
			}
		}
		return "";
	}
	
	/*
	implement steps
	1. get app bar
	2. get records from mst view
	3. handle filter and order parameter 
	*/
	public Response getDynList() {
		Response resp = null;
		Connection conn = null;
		DynTemplate dao = new DynTemplate();
		DynamicDAO dynamic = new DynamicDAO();
		ResultSet rs = null;
		DBManager dbm = null;
		JSONArray result = new JSONArray();
		JSONObject template = null;
		DynUI ui = null;
		String module = null;
		List<DynField> headers = new ArrayList<DynField>();
		String searchStr = "";
		String statusStr = "";
		String typeStr = "";
		String sortBy = "";
		String sortDir = "A";
		String status = "";
		int offset = 0;
		int size = 10;
		JSONObject cond = null;
		String actionType = Constants.ActionTypes.CHANGE_PAGE;
		boolean isNew = true;
		try {
			conn = DBAccess.getConnection();
			dbm = new DBManager();
			module = request.getParameter("p1");

			String p0 = request.getParameter("p0");
			if (p0 != null) {
				try {
					actionType = Constants.ActionTypes.CHANGE_CONTENT;
					p0 = new String(Base64.getDecoder().decode(p0), "UTF-8");
					Log.debug("gen table View 2:" + p0);
					cond = new JSONObject(p0);
					searchStr = cond.has("criteria") ? cond.getString("criteria") : "";
					statusStr = cond.has("statusFilter") ? cond.getString("statusFilter") : "";
					typeStr = cond.has("typeFilter") ? cond.getString("typeFilter") : "";
					sortBy = cond.has("sortBy") ? cond.getString("sortBy") : "";
					sortDir = cond.has("sortDir") ? cond.getString("sortDir").toUpperCase() : "A";
					offset = cond.has("recordStart") ? cond.getInt("recordStart") : 0;
					size = cond.has("pageSize") ? cond.getInt("pageSize") : Constants.SEARCH_RESULT_DEFAULT_PAGE_SIZE;
					isNew = offset == 0;
					status = cond.has("status") ? cond.getString("status") : "";
				} catch (Exception e) {
					cond = new JSONObject();
				}

			} else {
				cond = new JSONObject();
			}
			
			// get ui mst, to all tables
			ui = dao.getDynUI(module, conn);

			String filterKey = module + "_" + searchStr + "_" + statusStr + "_" + typeStr + "_" + sortBy + "_"
					+ sortDir;
			String curFilterKey = session.getAttribute(Constants.SessionKeys.FILTER_KEY) != null
					? (String) session.getAttribute(Constants.SessionKeys.FILTER_KEY) : null;
			if (!isNew && filterKey.equals(curFilterKey)) {
				result = session.getAttribute(Constants.SessionKeys.SEARCH_RESULT) != null
						? (JSONArray) session.getAttribute(Constants.SessionKeys.SEARCH_RESULT) : null;
			}
			
			// headers for table-like list 
			// fields to be returned
			headers = dao.getDynFields(module, conn);
			
			String selectFields = select(headers);
			if(!selectFields.contains(ui.getPrimaryKey())){
				selectFields += ", " + ui.getPrimaryKey();
			}
			
			String filter = "";
			String order = "";
			
			String filterByLike = "";
			
			if(!"".equals(searchStr)){
				String searchField = ui.getSearchField();
				String[] sfArray = searchField.split(",");
				int sfLenght = sfArray.length;
				for(int i = 0; i < sfLenght; i++){
					String sf = sfArray[i].replace(" ", "");
					String likeFilter = "UPPER(" + sf + ") like UPPER(" + dbm.param("%" + searchStr + "%", DataType.TEXT) + ")";
					if("".equals(filterByLike)){
						filterByLike = likeFilter;
					}else{
						filterByLike += " or " + likeFilter;
					}
				}
			}
			
			
			
			// order
			// default: order by UPD_DATE
			if(!"".equals(sortBy)){
				order = " order by " + getFieldKey(headers, sortBy);
				String direction = "D".equals(sortDir) ? "desc" : "";
				order = order + " " + direction;
			}
			
			if("".equals(sortBy))
				sortBy = "UPD_DATE desc";
			
			
			//handle filter para
			try{
				if(ui.hasCompFilter()){
					String newFilter = " comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT);
					if("".equals(filter)){
						filter = newFilter;
					}else{
						filter += " and " + newFilter;
					}
				}
				
				if(ui.hasLangFilter()){
					String newFilter = " lang_code = " + dbm.param(principal.getLangCode(), DataType.TEXT);
					if("".equals(filter)){
						filter = newFilter;
					}else{
						filter += " and " + newFilter;
					}
				}
				
				if(!"".equals(filterByLike)){
					if("".equals(filter)){
						filter = filterByLike;
					}else{
						filter = "(" + filterByLike + ")" + " and " + filter;
					}
				}
				
				if(!"".equals(status) && !"A".equalsIgnoreCase(status)){
					String statusFilter = " status " + ("-R".equals(status)?"<>":"=") + dbm.param(status, DataType.TEXT);
					if("".equals(filter)){
						filter = statusFilter;
					}else{
						filter += " and " + statusFilter;
					}
				}
				
				for (int i = 0; i < headers.size(); i++) {
					DynField header = headers.get(i);
					String hId = header.getId();
					if(cond.has(hId)){
						String conHid = cond.getString(hId);
						if(!"".equals(conHid)){
							if(!("picker".equalsIgnoreCase(header.getType()) && "*".equalsIgnoreCase(conHid))){
								
								if(!"-R".equalsIgnoreCase(conHid)){
									String keyFilter =  header.getKey() + " = " + dbm.param(conHid, DataType.TEXT);
									if("".equals(filter)){
										filter = keyFilter;
									}else{
										filter += " and " + keyFilter;
									}
								}else{
									String keyFilter =  header.getKey() + " <> " + dbm.param(conHid, DataType.TEXT);
									if("".equals(filter)){
										filter = keyFilter;
									}else{
										filter += " and " + keyFilter;
									}
								}
							}
						}
					}
				}
				
				
			}catch(Exception e){
				Log.error("getMstFilter error ");
				e.printStackTrace();
			}
			//end filter para
			
			if (filter != null && !filter.equals(""))
				filter = " where " + filter;

			String sql = " select " + selectFields + " from " + ui.getMstView() + filter + " " + order;
			Log.error("dynTemplate list sql : " + sql);
			rs = dbm.select(sql, conn);
			// get all selected column names
			String primaryKey = ui.getPrimaryKey();
			
			if (rs != null) {
				HashMap<String, Map<String, String>> pickers = new HashMap<String, Map<String, String>>();
				while (rs.next()) {
					JSONObject dataSet = new JSONObject();
					dataSet.put("id", rs.getString(primaryKey));
					for (int j = 0; j < headers.size(); j++) {
						DynField listItem = headers.get(j);
						String mapKey = listItem.getKey();
						String mapValue = rs.getString(mapKey);
						if ("picker".equalsIgnoreCase(listItem.getType())) {
							listItem.setMandatory("N");
							if(pickers.containsKey(listItem.getKey())){
								Map<String, String> picker = pickers.get(listItem.getKey());
								mapValue = picker.get(mapValue);
							}else{
								Map<String, String> picker = dao.getLookUpList(ui, listItem, conn, principal, null);
								pickers.put(listItem.getKey(), picker);
								mapValue = picker.get(mapValue);
							}
						}
						if (mapKey != null)
							dataSet.put(listItem.getId(), mapValue);
					}
					result.put(dataSet);
				}

				session.setAttribute(Constants.SessionKeys.SEARCH_RESULT + module, result);
			}
			
			JSONObject ret = new JSONObject();
			if (result != null) {
				template = getTemplate(headers, module);
				
				JSONObject jsonContent = new JSONObject();
				JSONArray shortedList = new JSONArray();

				for (int j = offset; j < offset + size && j < result.length(); j++) {
					shortedList.put(result.get(j));
				}

				JSONObject values = new JSONObject();
				values.put("list", shortedList);
				values.put("total", result.length());
				values.put("isMore", offset > 0);

				jsonContent.put("values", values);
				jsonContent.put("template", template);

				ret.put("content", jsonContent);

				JsonObject valuesObject = DynamicFunction.convertJsonToGson(values);
				Content content = new Content(DynamicFunction.getTemplateFromJson(request, template), valuesObject,
						valuesObject);
				Page pageObject = new Page("/DynTemplate", module, module);
				
				AppBar appBar = getAppBar(conn, ui, module, "table", null);
				appBar.setValues(DynamicFunction.convertJsonToGson(cond));
				String tokenID = Token.CsrfID(request.getSession());
				String tokenKey = Token.CsrfToken(request.getSession());

				resp = new Response(actionType, null, pageObject, tokenID, tokenKey, appBar, content);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbm.clear();
			dbm = null;
			if (conn != null) {
				try {
					if (!conn.isClosed()) {
						conn.close();
						conn = null;
					}
				} catch (SQLException e) {

				}
			}
		}
		return resp;
	}

	private String select(List<DynField> headers) {
		String str = null;
		if (headers != null) {
			int size = headers.size();
			for (int i = 0; i < size; i++) {
				DynField field = headers.get(i);
				if (str == null) {
					str = field.getKey();
				} else {
					str += ", " + field.getKey();
				}
			}
		}
		return str;
	}

}
