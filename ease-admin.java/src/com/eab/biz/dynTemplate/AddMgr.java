package com.eab.biz.dynTemplate;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.common.Constants;
import com.eab.common.Function2;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.dynTemplate.DynLayout;
import com.eab.database.jdbc.DBAccess;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.model.dynTemplate.DynUI;
import com.google.gson.JsonObject;

public class AddMgr extends DynMgr{
		public static String DYN_TEMPLATE = "DYN_TEMPLATE";
		public AddMgr(HttpServletRequest request) {
			super(request);
		}
		
		public Response getDynAdd() {
			Connection conn = null;
			DynLayout dao = new DynLayout();
			String module = request.getParameter("p1");
			try {
				conn = DBAccess.getConnection();
				
				DynUI ui = dao.getDynUI(module, conn); 
				Template template = parseTemplate(getTemplateJson(conn, ui, "ADD", null, 1));
				JsonObject deValues = new JsonObject();
				deValues.addProperty("id", "");
				deValues.addProperty("isNew", true);
				deValues.addProperty("version", 1);
				JsonObject selectPage = new JsonObject();
				selectPage.addProperty("id", "TaskInfo");
				deValues.add("selectPage", selectPage);
				Content content = new Content(template, deValues, deValues);
				// Token
				String tokenID = Token.CsrfID(request.getSession());
				String tokenKey = Token.CsrfToken(request.getSession());
				Page pageObject = new Page("/DynTemplate/Add", null);
				
				pageObject.setModule(module);
				JsonObject pageValues = new JsonObject();
				pageValues.addProperty("id", "");
				pageValues.addProperty("selectPageId", "TaskInfo");
				pageValues.addProperty("version", 1);
				pageObject.setValue(pageValues);
				ListMgr listMgr = new ListMgr(request);
	
				AppBar appBar = listMgr.getAppBar(conn, ui, module, "add", null);
				
				Response resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, pageObject, tokenID, tokenKey, appBar, content);
				return resp;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

}
