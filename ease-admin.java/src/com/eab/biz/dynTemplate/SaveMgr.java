package com.eab.biz.dynTemplate;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.util.ArrayList;
import java.sql.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.eab.common.Compressor;
import com.eab.common.Constants;
import com.eab.common.Constants.ActionTypes;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.dynTemplate.DynTemplate;
import com.eab.dao.tasks.Tasks;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Response;
import com.eab.model.dynTemplate.DynField;
import com.eab.model.dynTemplate.DynName;
import com.eab.model.dynTemplate.DynUI;
import com.eab.model.tasks.TaskBean;
import com.eab.webservice.dynamic.DynamicFunction;

public class SaveMgr extends DynMgr {
	private String PacketKey = "DTSaveDataTemp";
	private String TASK_INFO = "TASK_INFO_TEMP";
	private String MSG_ERROR = "ERROR";
	String module = null;
	String docCode = null;
	int version = -1;
	int taskId = -1;
	Connection conn = null;

	public SaveMgr(HttpServletRequest request) {
		super(request);
	}

	protected void addDataSection(String dataStr) {
		List<String> temp = (ArrayList<String>) request.getSession().getAttribute(this.PacketKey);
		temp.add(dataStr);
	}

	protected int getTaskId(JSONObject inputJson) {
		int taskId = -1;
		if (inputJson.has("taskId")) {
			Object taskIdObj = inputJson.get("taskId");
			Log.error("taskId:" + taskIdObj.toString());
			if (taskIdObj instanceof JSONArray) {
				JSONArray taskIdArr = (JSONArray) taskIdObj;
				if (taskIdArr.length() > 0)
					taskId = taskIdArr.getInt(0);
			} else {
				taskId = inputJson.getInt("taskId");
			}
		}
		return taskId;
	}

	public Response save() {
		// return
		JSONObject response = new JSONObject();
		Response resp = null;

		// db
		DBManager dbm = null;

		// dao
		DynTemplate dao = new DynTemplate();
		DynUI ui;

		// packets
		String packetNumStr = request.getParameter("packetNum");
		String offsetStr = request.getParameter("offset");
		int packetNum = Function.stringToInt(packetNumStr);
		int offset = Function.stringToInt(offsetStr);

		// doc info
		String newDocCode = null;
		String attId = null;
		String error = null;
		boolean isComplete = false;
		boolean isNew = false;
		int version = 1;
		String curSection = null, docCode = null, nextSection = null;
		String dataStr = request.getParameter("p0");

		try {
			if (offset != -1 && packetNum != -1) {

				if (offset == 0) {
					session.setAttribute(this.PacketKey, new ArrayList());
				}
				// combine data sections
				addDataSection(dataStr);

				// end
				if (offset == packetNum - 1) {
					isComplete = true;
					List<String> packets = (ArrayList<String>) session.getAttribute(this.PacketKey);
					if (packets.size() == packetNum) {
						StringBuilder sb = new StringBuilder();
						for (String pack : packets) {
							sb.append(pack);
						}

						String fullData = sb.toString();
						byte[] decoded = Base64.decodeBase64(fullData.replace(" ", "+"));
						String paraStr = new String(decoded, StandardCharsets.UTF_8);
						JSONObject dataJson = new JSONObject(paraStr);

						Log.error("data received: " + dataJson.toString().length());

						String module = dataJson.getString("module");
						docCode = dataJson.getString("docCode"); // e.g: covCode
						int taskId = getTaskId(dataJson);
						version = dataJson.has("version") ? dataJson.getInt("version") : 1;
						this.module = module;
						this.docCode = docCode;
						this.version = version;

						attId = dataJson.isNull("values") ? null
								: (dataJson.has("attId") ? dataJson.getString("attId") : null);

						if (taskId == -1) {
							isNew = true;
						}

						conn = DBAccess.getConnection();
						dbm = new DBManager();

						if (dataJson.isNull("values")) {
							// no change
							// TODO: just change page
							if (attId == null) {
								curSection = dataJson.getString("curSection");
								if (dataJson.has("nextSection"))
									nextSection = dataJson.getString("nextSection");
								else
									nextSection = curSection;
							}
						} else {
							// start save-data
							if (attId == null) {
								// start save-normal-data
								curSection = dataJson.getString("curSection");
								if (dataJson.has("nextSection"))
									nextSection = dataJson.getString("nextSection");
								else
									nextSection = curSection;

								JSONObject values = dataJson.getJSONObject("values");
								ui = dao.getDynUI(module, conn);
								String thisDocCode = getDocCode(values, ui.getPrimaryID());
								if (isNew)
									docCode = thisDocCode;
								else
									newDocCode = thisDocCode;

								// do nth if docCode does not exit
								if (docCode != null) {
									conn.setAutoCommit(false);

									if (isNew) {
										if (session.getAttribute(TASK_INFO) != null) {
											values.put(Constants.ProductPage.TASK, session.getAttribute(TASK_INFO));
											session.removeAttribute(TASK_INFO);
										}
									}

									// change doc code
									if (newDocCode != null && !newDocCode.equalsIgnoreCase(docCode)) {
										// update all docCode in all table
										// 1. task table
										Tasks task = new Tasks();
										task.updateTaskDocCode(conn, principal, taskId, newDocCode);

										// 2. dyn section
										dataDao.updateSectionDocCode(conn, principal, docCode, newDocCode, version,
												module);

										// 3. json
										dataDao.updateJsonDocCode(conn, principal, newDocCode, taskId);

										// 4. name table
										dataDao.updateDynNameCode(conn, principal.getCompCode(), module, docCode,
												newDocCode, version);

										// 5. trx table

										updateTrxDocCode(conn, ui, docCode, newDocCode, version);
										docCode = newDocCode;
									}
									// upsert task info
									if (values.has(Constants.ProductPage.TASK) && error == null) {
										error = saveTaskInfo(conn, values.getJSONObject(Constants.ProductPage.TASK),
												docCode, module, isNew, taskId, version);
										Log.error("saveTaskInfo error : " + error);
										// if it has no task id, get it from db.

										if (taskId == -1) {
											Tasks tsDao = new Tasks();
											taskId = tsDao.getTaskId(conn, principal.getCompCode(), module, docCode,
													version);
										}

									}

									// save sections
									if (taskId != -1) {

										JSONObject template = getTemplateJson(conn, ui, "NEW", docCode, version);
										this.taskId = taskId;

										JSONObject jSections = values;
										saveSections(conn, jSections, docCode, module, version);

										// make and save product json
										JSONObject dynJson = parseResultJson(conn, docCode, module, version, template);

										// save main module json & rate json
										dataDao.saveJsonFile(principal, conn, dynJson, module, docCode, docCode,
												version, taskId);

										// when create new record, save to trx
										// table
										String trxKey = ui.getTrxSectionID();
										if (values.has(trxKey)) {
											JSONObject trxObj = values.getJSONObject(trxKey);
											error = saveTrxMst(conn, docCode, module, ui, version, trxObj);

											if (error == null) {
												String nameId = ui.getNameId();
												JSONObject name = (JSONObject) getKey(trxObj, nameId);
												error = saveNames(conn, name, module, version, docCode, nameId);
											}
											if (error != null) {
												conn.rollback();
											}
										}

									}
									conn.commit();
								} else {
									// when it has no task id, its a new record
									// then save the task info into session
									if (values.has(Constants.ProductPage.TASK)) {
										session.setAttribute(TASK_INFO, values.get(Constants.ProductPage.TASK));
									}
								}
								if (error == null) {
									// error free

									if (isNew && docCode != null && !"".equals(docCode)) {
										// save done!
										DetailMgr dMgr = new DetailMgr(request);
										resp = dMgr.getDetails(module, docCode, version, curSection);
										resp.setAction(ActionTypes.CHANGE_CONTENT);
										JSONObject state = new JSONObject();
										state.put("isComplete", true);
										resp.setUploadStatus(DynamicFunction.convertJsonToGson(state));
										// have to close connection before
										// return
										if (conn != null && !conn.isClosed()) {
											conn.close();
										}

										return resp;
									}

								} else {
									// TODO handle error

								}
								// end save-normal-data
							} else {
								// save attachment by attId
								conn.setAutoCommit(false);

								String fileStr = dataJson.getString("values");
								// return action = UPDATE_UPLOAD_STATE to
								// complete the complete
								AttachmentMgr aMgr = new AttachmentMgr(request);
								error = aMgr.saveAttchment(conn, dbm, fileStr, module, docCode, attId, version, taskId);
								if (error != null) {
									conn.rollback();
								}
								conn.commit();

							}
							// end save-data

						}

					}

					session.removeAttribute(this.PacketKey);

					response.put("isComplete", true);
					response.put("isTeminate", true);

				} else {

					// return the next offset
					int newOffset = offset + 1;
					response.put("offset", newOffset);
					response.put("isComplete", false);
					response.put("isTeminate", false);
				}
			} else {
				response.put("isComplete", false);
				response.put("isTeminate", true);
			}
		} catch (Exception e) {
			Log.error(e);
			// clear session
			session.removeAttribute(this.PacketKey);

			response.put("isComplete", false);
			response.put("isTeminate", true);
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
				if (dbm != null) {
					dbm.clear();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// set token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		if (attId == null && isComplete) {
			// normal data response
			Log.error("save data done: return now");
			resp = changePage(curSection, nextSection, docCode, version);
			resp.setTokenID(tokenID);
			resp.setTokenKey(tokenKey);
			resp.setUploadStatus(DynamicFunction.convertJsonToGson(response));
		} else {
			// upload single packet response
			resp = new Response(ActionTypes.UPDATE_UPLOAD_STATE, null, null);
			resp.setTokenID(tokenID);
			resp.setTokenKey(tokenKey);
			resp.setUploadStatus(DynamicFunction.convertJsonToGson(response));
		}
		return resp;
	}

	public void updateTrxDocCode(Connection conn, DynUI ui, String docCode, String newDocCode, int version) {
		dataDao.updateTrxDocCode(conn, ui, principal.getCompCode(), docCode, newDocCode, version);
	}

	public JSONObject parseResultJson(Connection conn, String docCode, String module, int version, JSONObject template)
			throws SQLException {
		JSONObject prodJson = new JSONObject();
		JSONObject json = new JSONObject();
		Map<String, JSONObject> sectionMap = dataDao.getSections(conn, principal, module, docCode, version, null);

		// handle static case
		// handle image and css
		JSONArray images = null;
		String style = null;
		String cssSectionStr = "cssSection";
		String imageSectionStr = "imageSection";
		if (sectionMap.containsKey(imageSectionStr)) {
			JSONObject imgSection = sectionMap.get(imageSectionStr);
			sectionMap.remove(imageSectionStr);
			if (imgSection.has("images")) {
				images = imgSection.getJSONArray("images");
			}
		}

		if (sectionMap.containsKey(cssSectionStr)) {
			JSONObject cssSection = sectionMap.get(cssSectionStr);
			sectionMap.remove(cssSectionStr);
			if (cssSection.has("style")) {
				style = cssSection.getString("style");
			}
		}

		// replace img base64 string into css
		if (images != null && style != null) {
			for (int i = 0; i < images.length(); i++) {
				JSONObject image = images.getJSONObject(i);
				String code = "<img@" + image.getString("code") + ">";
				String imageStr = "url(" + image.getString("image") + ")";
				style = style.replaceAll(code, imageStr);
			}
		}

		// insert css into json
		if (style != null) {
			prodJson.put("style", Compressor.compressCss(style, new Compressor.Options()));
			// prodJson.put("style", style);
		}

		for (String key : sectionMap.keySet()) {
			JSONObject section = sectionMap.get(key);
			// loop template
			JSONObject sectionTemplate = getSectionTemplate(template, key);
			if (sectionTemplate != null)
				json.put(key, convertSection(sectionMap, section, key, sectionTemplate));
		}

		Iterator<?> keys = json.keys();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			Object sectionObj = json.get(key);
			if (sectionObj instanceof JSONObject) {
				JSONObject curSection = (JSONObject) sectionObj;
				Iterator<?> sKeys = curSection.keys();
				while (sKeys.hasNext()) {
					String sKey = (String) sKeys.next();
					prodJson.put(sKey, curSection.get(sKey));
				}
			} else {
				prodJson.put(key, sectionObj);
			}

		}

		return prodJson;
	}

	public JSONObject getSectionTemplate(JSONObject template, String key) {
		JSONObject target = null;
		if (template.has("items")) {
			JSONArray items = template.getJSONArray("items");
			int size = items.length();
			int i = 0;
			while (i < size && target == null) {
				JSONObject item = items.getJSONObject(i);
				if (item.has("id") && key.equals(item.getString("id"))) {
					target = item;
				} else {
					target = getSectionTemplate(item, key);
				}
				i++;
			}
		}
		return target;
	}

	public JSONObject convertSection(Map<String, JSONObject> sectionMap, JSONObject section, String sectionKey,
			JSONObject template) {
		Iterator<?> keys = section.keys();
		JSONObject newSection = new JSONObject();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			Object value = section.get(key);
			JSONObject thisTemplate = getSectionTemplate(template, key);
			if (thisTemplate != null)
				newSection.put(key, convertObject(sectionMap, section, sectionKey, value, thisTemplate));
		}

		// parseSections
		return newSection;
	}

	public Object convertObject(Map<String, JSONObject> sectionMap, JSONObject parentValue, String sectionKey,
			Object value, JSONObject template) {

		if (template == null)
			return value;

		if (!template.has("type"))
			return value;

		if (!validation(sectionMap, parentValue, sectionKey, value, template)) {
			Log.error("not validate");
			return null;
		}

		if (template.has("forConfigOnly"))
			return null;

		if (template.has("saveByFile")) {
			Log.error("saveByFile!!!");
			// save it to dyn json table
			// for independent rate save, obj should be an array
			JSONArray values = (JSONArray) value;
			saveRates(sectionMap, sectionKey, values, template);
		} else {
			String templateType = template.getString("type");
			if (value instanceof JSONArray) {
				// complexList
				JSONArray values = (JSONArray) value;
				if ("complexList".equalsIgnoreCase(templateType)) {
					boolean merge = false;
					int clSize = values.length();

					if (clSize > 0) {
						if (1 == Function.getSize(values.getJSONObject(0))) {
							merge = true;
						}
					}
					if (merge) {
						String thisKey = null;
						JSONObject fst = values.getJSONObject(0);
						Iterator<?> keys = fst.keys();
						if (keys.hasNext()) {
							thisKey = (String) keys.next();
						}

						if (thisKey != null) {
							Object[] objArray = new Object[clSize];
							for (int c = 0; c < clSize; c++) {
								JSONObject clValue = values.getJSONObject(c);
								objArray[c] = clValue.get(thisKey);
							}
							return objArray;
						} else {
							return values;
						}
					} else {

						JSONArray newValues = new JSONArray();
						int size = values.length();
						for (int i = 0; i < size; i++) {
							// loop every item inside complex list
							JSONObject clItemValue = values.getJSONObject(i);
							Iterator<?> keys = clItemValue.keys();
							JSONObject _clItemValue = new JSONObject();
							while (keys.hasNext()) {
								String key = (String) keys.next();
								JSONObject thisTemplate = getSectionTemplate(template, key);
								if (thisTemplate != null)
									_clItemValue.put(key, convertObject(sectionMap, clItemValue, sectionKey,
											clItemValue.get(key), thisTemplate));
								else
									_clItemValue.put(key, clItemValue.get(key));
							}
							newValues.put(_clItemValue);
						}
						return newValues;
					}
				} else if ("rateGrid".equalsIgnoreCase(templateType)) {
					return parseRate(values);
				} else {
					return value;
				}

			} else if (value instanceof JSONObject) {
				JSONObject value_ = (JSONObject) value;

				if ("detailsTab".equalsIgnoreCase(templateType) || "detailsList".equalsIgnoreCase(templateType)) {
					JSONObject newTabDetailValue = (JSONObject) value;
					Iterator<?> keys = value_.keys();

					JSONObject titleTemplate = getSectionTemplate(template, "title");
					boolean hasTitle = titleTemplate != null;
					boolean hasId = (getSectionTemplate(template, "id") != null);

					while (keys.hasNext()) {
						String key = (String) keys.next();

						JSONObject subValue = value_.getJSONObject(key);
						JSONObject subValue_ = new JSONObject(subValue.toString());

						// handle title and id
						if (!hasId)
							subValue_.remove("id");
						if (!hasTitle)
							subValue_.remove("title");

						JSONObject subValue__ = new JSONObject(subValue_.toString());
						Iterator<?> sKeys = subValue__.keys();
						while (sKeys.hasNext()) {
							String sKey = (String) sKeys.next();
							JSONObject thisTemplate = getSectionTemplate(template, sKey);
							if (thisTemplate != null && subValue_.has(sKey)) {
								Object sObj = convertObject(sectionMap, subValue__, sectionKey, subValue_.get(sKey),
										thisTemplate);
								subValue_.put(sKey, sObj);
							} else {
								if (!sKey.equals("id") && !sKey.equals("title")) {
									subValue_.remove(sKey);
								}
							}
						}

						if (1 == Function.getSize(subValue_)) {
							Iterator<?> resultKeys = subValue_.keys();
							if (resultKeys.hasNext()) {
								String resultKey = (String) resultKeys.next();
								newTabDetailValue.put(key, subValue_.get(resultKey));
							}
						} else if (Function.getSize(subValue_) > 0) {
							newTabDetailValue.put(key, subValue_);
						} else {
							newTabDetailValue.put(key, "");
						}

					}
					return newTabDetailValue;
				} else if ("multText".equalsIgnoreCase(templateType)) {
					return value_;
				} else if ("genericGrid".equalsIgnoreCase(templateType)) {
					return parseGenericGrid(value_);
				} else if ("jseditor".equalsIgnoreCase(templateType)) {
					return value_.get("func");
				} else {
					return value_;
				}

			} else if (value instanceof String) {
				if ("ccy".equalsIgnoreCase(template.getString("id"))) {
					Log.error("CCY");
				}
				if ("selectList".equalsIgnoreCase(templateType) || "checkboxgroup".equalsIgnoreCase(templateType)) {
					return stringToArray((String) value);
				} else if ("htmlEditor".equalsIgnoreCase(templateType)) {
					return parseHtmlEditor((String) value);
				} else
					return value;
			} else if (value instanceof Long) {
				if ("datePicker".equalsIgnoreCase(template.getString("type"))) {
					return Function.longToDate((Long) value);
				} else {
					return value;
				}
			} else {
				return value;
			}
		}
		return null;
	}

	public boolean validation(Map<String, JSONObject> sectionMap, JSONObject parentValue, String sectionKey,
			Object value, JSONObject template) {
		if ("premInput".equals(template.getString("id")))
			Log.error("premInput");

		boolean result = true;
		if (template.has("trigger")) {
			result = checkTrigger(sectionMap, parentValue, sectionKey, value, template);
		}
		return result;
	}

	/*
	 * 1. get trigger value for current section 2. if null, get from other
	 * sections
	 */
	public boolean checkTrigger(Map<String, JSONObject> sectionMap, JSONObject parentValue, String sectionKey,
			Object value, JSONObject template) {
		JSONArray triggers = template.getJSONArray("trigger");
		int triggersSize = triggers.length();
		boolean show = true;
		for (int i = 0; i < triggersSize; i++) {
			JSONObject trigger = triggers.getJSONObject(i);
			if (trigger.has("type")) {
				String type = trigger.getString("type");
				// TODO: handle the checking
				try {
					if ("hideIfEqual".equalsIgnoreCase(type) || "showIfContains".equalsIgnoreCase(type)
							|| "showIfEqual".equalsIgnoreCase(type) || "showIfExists".equalsIgnoreCase(type)) {
						String lookUpId = trigger.getString("id");
						String lookUpValue = trigger.getString("value");
						String _value = getTriggerValue(sectionMap, parentValue, sectionKey, lookUpId);

						if (_value != null) {
							if ("hideIfEqual".equalsIgnoreCase(type)) {
								show = show && !lookUpValue.equalsIgnoreCase(_value);
							} else if ("showIfContains".equalsIgnoreCase(type)) {
								show = show && _value.contains(lookUpValue);
							} else if ("showIfEqual".equalsIgnoreCase(type)) {
								show = show && lookUpValue.equalsIgnoreCase(_value);
							} else if ("showIfExists".equalsIgnoreCase(type)) {
								show = show && lookUpValue.contains(_value);
							}
						} else {
							if ("showIfContains".equalsIgnoreCase(type)) {
								show = show && false;
							} else if ("showIfEqual".equalsIgnoreCase(type)) {
								show = show && false;
							} else if ("showIfExists".equalsIgnoreCase(type)) {
								show = show && false;
							}
						}
					}
				} catch (Exception e) {
					Log.error("checkTrigger error");
					e.printStackTrace();
				}
				if (!show)
					break;
			}
		}

		return show;
	}

	public String getTriggerValue(Map<String, JSONObject> sectionMap, JSONObject parentValue, String sectionKey,
			String lookUpId) {
		String[] keys = lookUpId.split("/");
		JSONObject curSectionValue = sectionMap.get(sectionKey);

		String value = null;
		if (parentValue.has(lookUpId)) {
			value = parentValue.getString(lookUpId);
		}
		if (value == null)
			value = getTriggerValue(curSectionValue, keys, 0);

		if (value == null) {
			for (String key : sectionMap.keySet()) {
				if (value == null) {
					value = getTriggerValue(sectionMap.get(key), keys, 0);
				} else
					break;
			}
		}
		return value;
	}

	public String getTriggerValue(Object values, String[] keys, int index) {
		if (!(values instanceof JSONObject)) {
			return null;
		}
		String key = keys[index];

		JSONObject _values = (JSONObject) values;
		if (_values.has(key)) {
			if (index == keys.length - 1) {
				Object value = _values.get(key);
				if (value instanceof String)
					return (String) value;
			} else {
				if (_values.has(key) && _values.get(key) instanceof JSONObject) {
					index = index + 1;
					return getTriggerValue(_values.get(key), keys, index);
				} else {
					return null;
				}
			}
		}
		return null;
	}

	public String[] stringToArray(String value) {
		String[] _value = value.split(",");
		return _value;
	}

	public void saveRates(Map<String, JSONObject> sectionMap, String sectionKey, JSONArray values,
			JSONObject template) {

		String rateId = template.getString("saveByFile");
		// the template type is assumed to be complexList
		for (int i = 0; i < values.length(); i++) {
			JSONObject value_ = values.getJSONObject(i);
			Iterator<?> keys = value_.keys();
			String thisRateId = new String(rateId);
			while (keys.hasNext()) {
				String key = (String) keys.next();
				String keyStr = "[" + key + "]";
				if (thisRateId.contains(keyStr)) {
					if (value_.get(key) instanceof String)
						thisRateId = thisRateId.replace(keyStr, value_.getString(key));
					else if (value_.get(key) instanceof Integer) {
						thisRateId = thisRateId.replace(keyStr, value_.getInt(key) + "");
					}
				}
			}
			// thisRateId = principal.getCompCode() + "_" + module + "_" +
			// docCode + "_" + version + "_" + thisRateId;

			JSONArray templateItems = template.getJSONArray("items");
			for (int j = 0; j < templateItems.length(); j++) {
				JSONObject templateItem = templateItems.getJSONObject(j);
				if (templateItem.has("isValue") && value_.has(templateItem.getString("id"))) {
					JSONObject rates = (JSONObject) convertObject(sectionMap, value_, sectionKey,
							value_.get(templateItem.getString("id")), templateItem);
					value_.put(templateItem.getString("id"), rates);
					saveRates(value_, thisRateId);
				}
			}
		}
	}

	public String parseHtmlEditor(String html) {
		return Compressor.compressHtml(html);
	}

	public JSONObject parseGenericGrid(JSONObject genericGrid) {
		JSONArray values = genericGrid.getJSONArray("values");
		try {
			if (values.length() > 0) {
				JSONObject result = new JSONObject();
				JSONObject keys = values.getJSONObject(0);
				for (String key : keys.keySet()) {
					result.put(keys.getString(key), new JSONArray());
				}
				for (int i = 1; i < values.length(); i++) {
					JSONObject value = values.getJSONObject(i);
					for (String key : keys.keySet()) {
						Object dataObj = value.get(key);
						String dataStr = dataObj instanceof String ? dataObj.toString() : "";
						result.getJSONArray(keys.getString(key)).put(dataStr);
					}
				}

				return result;
			}
		} catch (Exception e) {
			Log.error(e);
		}
		return null;
	}

	public JSONObject parseRate(JSONArray rates) {
		try {
			JSONObject result = new JSONObject();
			for (int i = 0; i < rates.length(); i++) {
				JSONObject rate = rates.getJSONObject(i);
				int keyCnt = rate.keySet().size();
				JSONArray rateArr = new JSONArray();
				for (int j = 2; j <= keyCnt; j++) {
					Object data = rate.get(String.valueOf(j));
					if (data instanceof Integer || data instanceof Double || data instanceof BigDecimal) {
						rateArr.put(data);
					} else if (data instanceof String) {
						String dataStr = data.toString();
						if ("".equals(dataStr.trim())) {
							rateArr.put(0);
						} else {
							if ("".equals(dataStr))
								return result;
							else {
								try {
									if (Function.isInteger(dataStr)) {
										rateArr.put(Integer.parseInt(dataStr));
									} else {
										rateArr.put(Double.parseDouble(dataStr));
									}
								} catch (Exception e) {
									rateArr.put(dataStr);
									// e.printStackTrace();
								}
							}
						}
					}
				}
				if (rate.has("1")) {
					if (rateArr.length() == 1) {
						result.put(String.valueOf(rate.get("1")), rateArr.get(0));
					} else
						result.put(String.valueOf(rate.get("1")), rateArr);
				}
			}
			if (result.keySet().size() > 0) {
				return result;
			}
		} catch (Exception e) {
			Log.error(" rate err :" + rates.toString());
		}
		return null;

	}

	public void saveRates(JSONObject rates, String fileId) {
		// if(fileId.contains("[covCode]")){
		// fileId = fileId.replace("[covCode]", docCode);
		// }
		// if(fileId.contains("[docCode]")){
		// fileId = fileId.replace("[docCode]", docCode);
		// }

		Log.error("saveRates docCode  : " + fileId);

		rates.put("type", "nfor_tab");
		try {
			dataDao.saveJsonFile(principal, conn, rates, module, docCode, fileId, version, taskId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// to do task
	public void sectionMassage(JSONObject template, Object section) {
		String tempType = template.getString("type");
		if (section.getClass() == JSONObject.class) {
			//
			JSONObject sJson = (JSONObject) section;
			if ("formulas".equalsIgnoreCase(sJson.getString("id")) && "detailsList".equalsIgnoreCase(tempType)) {
				Iterator<?> keys = sJson.keys();
				while (keys.hasNext()) {
					String fKey = (String) keys.next();
					JSONObject func = sJson.getJSONObject(fKey);
					if (func.has("func")) {
						sJson.put(fKey, func.get("func"));
					}
				}
			}

		}
		// single field in complex list
		if ("complexList".equalsIgnoreCase(tempType)) {
			JSONArray sAry = (JSONArray) section;
			int listCount = sAry.length();
			String onlyKey = null;
			Object onlyValues = null;
			for (int i = 0; i < listCount; i++) {
				JSONObject clJson = sAry.getJSONObject(i);
				Iterator<?> keys = clJson.keys();
				int keyCount = 0;
				while (keys.hasNext()) {
					keyCount += 1;
				}
				if (keyCount == 1) {
					keys = clJson.keys();
					onlyKey = (String) keys.next();
					if (clJson.get(onlyKey).getClass() == String.class) {
						if (onlyValues == null) {
							onlyValues = new String[listCount];
						} else {
							String[] _onlyValues = (String[]) onlyValues;
							_onlyValues[i] = clJson.getString(onlyKey);
						}
					} else if (clJson.get(onlyKey).getClass() == int.class) {
						if (onlyValues == null) {
							onlyValues = new int[listCount];
						} else {
							int[] _onlyValues = (int[]) onlyValues;
							_onlyValues[i] = clJson.getInt(onlyKey);
						}
					}

				}
			}

			if (onlyValues != null) {
				section = onlyValues;
			}
		}

		// single field in details list
		// multiple fields in details list: remove key "id"
		if ("detailsList".equalsIgnoreCase(tempType)) {
			JSONObject dlSection = (JSONObject) section;
			Iterator<?> keys = dlSection.keys();
			while (keys.hasNext()) {
				String dlKey = (String) keys.next();
				JSONObject dlItemSection = dlSection.getJSONObject(dlKey);
				dlItemSection.remove("id");
				String onlyKey = null;
				Iterator<?> dltKeys = dlItemSection.keys();
				while (dltKeys.hasNext()) {
					String curKey = (String) keys.next();
					if (!dltKeys.hasNext())
						onlyKey = curKey;
				}
				if (onlyKey != null) {
					dlSection.put(dlKey, dlItemSection.get(onlyKey));
				}
			}

		}

		//
	}

	public String saveNames(Connection conn, JSONObject nameObj, String module, int version, String docCode,
			String fieldId) {
		Log.error("saveNames : " + nameObj.toString());
		Iterator<?> keys = nameObj.keys();
		String error = null;
		while (keys.hasNext() && error == null) {
			String key = (String) keys.next();
			if (nameObj.get(key) instanceof String) {
				try {
					DynName name = new DynName();
					name.setModule(module);
					name.setCompCode(principal.getCompCode());
					name.setDocCode(docCode);
					name.setVersion(version);
					name.setFieldId(fieldId);
					name.setNameDesc(nameObj.getString(key));
					name.setLangCode(key);
					boolean result = false;
					if (dataDao.isNameExist(conn, name)) {
						result = dataDao.updateDynName(name, null, conn);
					} else {
						result = dataDao.insertDynName(name, null, conn);
					}
					error = result ? null : MSG_ERROR;

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					return MSG_ERROR;
				}
			}
		}
		return null;
	}

	public void saveSections(Connection conn, JSONObject jSections, String docCode, String module, int version) {
		Iterator<?> keys = jSections.keys();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			if (jSections.get(key) instanceof JSONObject) {
				try {
					dataDao.saveSection(principal, conn, jSections.getJSONObject(key), docCode, key, version, module);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public String saveTaskInfo(Connection conn, JSONObject taskInfo, String code, String module, boolean isNew,
			int taskId, int version) {
		TaskBean taskBean = new TaskBean();
		taskBean.setTaskDesc(taskInfo.getString("task_desc"));
		if (taskInfo.has("ref_no"))
			taskBean.setRefNo(taskInfo.getString("ref_no"));
		Date effDate = new Date(taskInfo.getLong("effDate"));
		taskBean.setEffDate(effDate);

		if (taskId != -1 && !"".equals(taskId)) {
			taskBean.setTaskCode(String.valueOf(taskId));
		}
		taskBean.setItemCode(code);
		taskBean.setVerNo(Integer.toString(version));
		taskBean.setFuncCode(layoutDao.getFuncCodeByModule(conn, module));

		try {

			if (taskBean != null) {
				Tasks taskMgr = new Tasks();
				if (taskBean.getTaskCode() == null || taskBean.getTaskCode().equals("")) {
					if (!taskMgr.addTask(principal, taskBean, conn)) {
						conn.rollback();
						conn.commit();
						return "ERROR.NO_INSERT";
					}
				} else {
					if (!taskMgr.updateTask(principal, taskBean, conn)) {
						conn.rollback();
						conn.commit();
						return "ERROR.NO_UPDATE";
					}
				}
			}
		} catch (Exception e) {
			return "ERROR";
		}

		return null;
	}

	public String genTempalteCode(DynUI ui, String type, JSONObject section) {
		Log.error("genTempalteCode section: " + section.toString());
		String tempalteConcat = ui.getTemplateConcat();
		Log.error("genTempalteCode tempalteConcat : " + tempalteConcat);
		String templateCode = null;
		if (tempalteConcat != null) {
			Iterator<?> keys = section.keys();
			while (keys.hasNext()) {
				String sKey = (String) keys.next();
				Object value = section.get(sKey);
				if (tempalteConcat.contains("[" + sKey + "]") && value instanceof String) {
					tempalteConcat = tempalteConcat.replace("[" + sKey + "]", (String) value);
				}
			}
		}

		if (tempalteConcat != null)
			templateCode = type + tempalteConcat;
		else
			templateCode = type;

		return templateCode;

	}

	public String saveTrxMst(Connection conn, String docCode, String module, DynUI ui, int version,
			JSONObject section) {
		String table = ui.getDataTable();

		List<DynField> trxFields = layoutDao.getTrxFields(principal, module);
		// check exist

		DBManager dbm = new DBManager();
		try {
			String primaryKey = ui.getPrimaryKey();
			HashMap<String, Object> condMap = new HashMap<String, Object>();
			condMap.put(primaryKey, docCode);
			condMap.put("comp_code", principal.getCompCode());
			condMap.put("version", version);

			boolean exist = Function2.hasRecordByType(table, condMap, dbm, conn);

			int size = trxFields.size();
			Log.error("getTrxFields size : " + size);
			if (exist) {
				// update
				// updateSet
				String updateSet = null;
				for (int i = 0; i < size; i++) {
					DynField f = trxFields.get(i);
					Object value = new Object();
					int dateType = DataType.TEXT;

					// assume rest are text
					if (f.getId() != null && section.has(f.getId())) {
						if ("date".equals(f.getType())) {
							dateType = DataType.DATE;
							Long value_ = section.getLong(f.getId());
							value = new Date(value_);
						} else if ("number".equals(f.getType())) {
							int value_ = section.getInt(f.getId());
							value = value_;
							dateType = DataType.INT;
						} else {
							value = section.getString(f.getId());
						}
						String con = f.getKey() + " = " + dbm.param(value, dateType);
						if (updateSet == null) {
							updateSet = con;
						} else {
							updateSet += " , " + con;
						}
					}

				}
				String sql = " update " + ui.getDataTable() + "" + " set " + updateSet + " where " + " comp_code = "
						+ dbm.param(principal.getCompCode(), DataType.TEXT) + " and version = "
						+ dbm.param(version, DataType.INT) + " and " + primaryKey + " = "
						+ dbm.param(docCode, DataType.TEXT);

				dbm.update(sql, conn);
			} else {
				// insert
				// columns
				String columns = "( template_code, comp_code, version, CREATE_DATE, UPD_DATE, CREATE_BY, UPD_BY ";
				String values = " ( " + dbm.param(genTempalteCode(ui, "NEW", section), DataType.TEXT) + ", "
						+ dbm.param(principal.getCompCode(), DataType.TEXT) + ", " + dbm.param(version, DataType.INT)
						+ ", sysdate, sysdate, " + dbm.param(principal.getUserCode(), DataType.TEXT) + ","
						+ dbm.param(principal.getUserCode(), DataType.TEXT);
				boolean hasPrimaryKey = false;

				for (int i = 0; i < size; i++) {

					DynField f = trxFields.get(i);

					if (f.getKey().equalsIgnoreCase(ui.getPrimaryKey()))
						hasPrimaryKey = true;

					columns += "," + f.getKey();

					Object value = new Object();
					int dateType = DataType.TEXT;

					// assume rest are text
					if ("date".equals(f.getType())) {
						dateType = DataType.DATE;
						Long value_ = section.getLong(f.getId());
						value = new Date(value_);
					} else if ("number".equals(f.getType())) {
						int value_ = section.getInt(f.getId());
						value = value_;
						dateType = DataType.INT;
					} else {
						value = section.getString(f.getId());
					}
					values += "," + dbm.param(value, dateType);
				}
				if (!hasPrimaryKey) {
					columns += "," + primaryKey;
					values += "," + dbm.param(docCode, DataType.TEXT);
				}
				columns += ")";
				values += ")";

				String sql = " insert into " + ui.getDataTable() + " " + columns + " values " + values;
				dbm.insert(sql, conn);

			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR.CREATE.ERROR";
		} finally {
			dbm.clear();
			dbm = null;
		}
		return null;

	}
}
