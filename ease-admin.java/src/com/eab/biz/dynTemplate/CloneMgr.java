package com.eab.biz.dynTemplate;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.biz.dynamic.DynamicManager;
import com.eab.biz.products.ProductMgr;
import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.common.Constants.ActionTypes;
import com.eab.dao.dynTemplate.DynData;
import com.eab.dao.dynTemplate.DynTemplate;
import com.eab.dao.products.Products;
import com.eab.dao.tasks.Tasks;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.model.dynTemplate.DynUI;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.JsonObject;

public class CloneMgr {
	
	HttpServletRequest request;
	UserPrincipalBean principal;
	HttpSession session;
	Language langObj;
	
	public CloneMgr(HttpServletRequest request) {
		this.request = request;
		this.session = request.getSession();
		principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
	}

	public Response clone() {
		String para = request.getParameter("p0");
		byte[] decoded = Base64.decodeBase64(para.replace(" ", "+"));
		String paraStr = new String(decoded, StandardCharsets.UTF_8);	
		JSONObject paraJson = new JSONObject(paraStr);
		String module = request.getParameter("p1");
		JSONArray docCodes = paraJson.getJSONArray("rows");
		String targetCompCode = paraJson.getString("compCode");
		return clone(docCodes, targetCompCode, module);	
		
	}
	
	public Response clone(JSONArray docCodes, String targetCompCode, String module){
		DynamicManager dynMgr = new DynamicManager(request);
		Response resp = dynMgr.returnDialog("CLONE_ERROR_MSG", "cloneError", "CLONE_ERROR_TITLE");
		
		try {
			//if rows length > 1, refresh list, else goto detail
			boolean success = clone(principal, null, docCodes, module, targetCompCode);
			int docSize = docCodes.length();
			if(success && docSize > 0){
				if(docSize > 1){
					ListMgr listMgr = new ListMgr(request);
					resp = listMgr.getDynList();
				}else{
					DetailMgr detailMgr = new DetailMgr(request);
					String newDocCode = clone(principal, null, docCodes.getString(0), module, targetCompCode);
					if(newDocCode != null){
						resp = detailMgr.getDetails(module, newDocCode, 1, null);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	private boolean clone(UserPrincipalBean principal, Connection conn, JSONArray docCodes, String module, String targetCompCode) throws SQLException{
		boolean isNew = false;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
			}
			for(int i=0; i<docCodes.length(); i++){
				String docCode = docCodes.getString(i);
				if(clone(principal, conn, docCode, module, targetCompCode) == null){
					return false;
				}
			}
			if(isNew){
				conn.commit();
			}
			return true;
		}catch(Exception e){
			conn.rollback();
			Log.error("newVersions error: " + e.getMessage());	
		}finally{
			if(isNew){
				conn.close();
				conn = null;	
			}
		}
		return false;
	}
	
	private String clone(UserPrincipalBean principal, Connection conn, String docCode, String module, String targetCompCode) throws SQLException{
		boolean isNew = false;
		DBManager dbm = null;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
			}
			dbm = new DBManager();
			DynData dataDao = new DynData();
			
			//get the template version
			int version = dataDao.getVersion(principal, conn, docCode, module);
			
			//get the template data
			JSONObject data = dataDao.getDynTrxData(principal, conn, docCode, version, module);
			
			//get the latest doc_code of template
			String newDocCode = dataDao.getNewDocCode(principal, conn, docCode,  module, 1, targetCompCode);
						
			
			//insert task
			Tasks taskDao = new Tasks();
			JSONObject taskParam = new JSONObject();
			taskParam.put("VER_NO", "1");
			taskParam.put("COMP_CODE", targetCompCode);
			taskParam.put("ITEM_CODE", newDocCode);
			//
			taskDao.addTaskFromTask(principal, conn, module, docCode, version, taskParam);
			
			int taskId = taskDao.getTaskId(conn, principal.getCompCode(), module, newDocCode, 1);
			
			
			JSONObject dataParam = new JSONObject();
			dataParam.put("VERSION", 1);
			dataParam.put("COMP_CODE", targetCompCode);
			dataParam.put("DOC_CODE", newDocCode);
			
			DynUI ui = dataDao.getDynUI(module, conn);
			dataParam.put(ui.getPrimaryKey().toUpperCase(), newDocCode);
			
			//insert into name table
			dataDao.addNameFromName(principal, conn, module, docCode, version, dataParam);
			
			//insert into section table
			dataDao.addSectionFromSection(principal, conn, module, docCode, version, dataParam);
			
			//insert into data table
			dataDao.addTrxDataFromTrxData(principal, conn, module, docCode, version, dataParam);
			
			//insert into json table
			
			//dataDao.addJsonFromJson(principal, conn, module, docCode, version, dataParam);
			cloneJson(conn, module, docCode, newDocCode, version, version, taskId);
			
			//insert into attachment table
			cloneAttachment(conn, module, docCode, newDocCode, version, version, taskId);
			
			if(isNew){
				conn.commit();
			}
			return newDocCode;
		}catch(Exception e){
			conn.rollback();
			Log.error("newVersion error: " + e.getMessage());	
		}finally{
			if(dbm != null)
				dbm.clear();
			if(isNew){
				conn.close();
				conn = null;	
			}
		}
		return null;
	}
	
	
	public Response checkExist() {
		String para = request.getParameter("p0");
		byte[] decoded = Base64.decodeBase64(para.replace(" ", "+"));
		String paraStr = new String(decoded, StandardCharsets.UTF_8);	
		JSONObject paraJson = new JSONObject(paraStr);
		String module = request.getParameter("p1");
		String paramCompCode = null;
		List<String> docCodes = new ArrayList<>();
		try {
			if (paraJson.has("compCode")) {
				paramCompCode = (String) paraJson.get("compCode");
			}
			JSONArray rows = new JSONArray();
			if (paraJson.has("rows")) {
				rows = paraJson.getJSONArray("rows");
			}
			
			// Fetch rows
			for (int i = 0; i < rows.length(); i++) {
				String row = (String) rows.get(i);
				docCodes.add(row);
			}

			JSONArray existedDocCode = new JSONArray();
			String compCode = principal.getCompCode();

			for (int i = 0; i < docCodes.size(); i++) {
				String docCode = docCodes.get(i);

				try {
					DynData dynData = new DynData();
					if (dynData.checkExist(docCode, compCode, -1, module)) {
						existedDocCode.put(docCode);
					}
				} catch (SQLException e) {
					Log.error(e);
				}
			}

			if (existedDocCode.length() > 0) {
				// Confirmation Dialog

				// Action
				String action = ActionTypes.SHOW_DIALOG;

				// Token
				String tokenID = Token.CsrfID(request.getSession());
				String tokenKey = Token.CsrfToken(request.getSession());

				// Dialog
				Field positive = new Field("/DynTemplate/Clone", "submitButton", langObj.getNameDesc("BUTTON.OK"));
				Field negative = new Field("cancel", "button", langObj.getNameDesc("BUTTON.CANCEL"));
				Dialog dialog = new Dialog("clone_confirm", langObj.getNameDesc("PROD.CLONE_DIALOG.CLONE"), langObj.getNameDesc("PROD.CLONE_DIALOG.CLONE_CONFIRM_MSG"), positive, negative, true, 30);

				return new Response(action, tokenID, tokenKey, dialog);
			} else {
				// Clone
				return clone(existedDocCode, compCode, module);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean cloneJson(Connection conn, String module, String docCode, String newDocCode, int version, int newVersion, int taskId) throws SQLException{
		DBManager dbm = new DBManager();
		try{
			
			String compCode = principal.getCompCode();
			DynData dao = new DynData();
			DynUI ui = dao.getDynUI(module, conn);	
			
			//get json from db
			HashMap<String, Object> keys = new HashMap<String, Object>();
			keys.put("parent_code", docCode);
			keys.put("version", version);
			if(ui.hasCompFilter()){
				keys.put("comp_code", compCode);
			}
			JSONArray jsonArray = dao.getAllDynJson(keys, dbm, conn);
			
			//insert into data table
			int attSize = jsonArray.length();
			for(int i = 0; i < attSize; i++){
				JSONObject json = jsonArray.getJSONObject(i);
				String id = json.getString("id");
				if(docCode.equals(id)){
					id = newDocCode;
				}else{
					id.replace(docCode, newDocCode);
				}
				JSONObject file = json.getJSONObject("json");
				dao.saveJsonFile(principal, conn, file, module, newDocCode, id, newVersion, taskId);
			}
			return true;
		}catch(Exception e){
			conn.rollback();
			Log.error("addJsonFromJson error: " + e.getMessage());	
		}finally{
			dbm.clear();
			dbm = null;
		}
		return false;
	}
	
	public boolean cloneAttachment(Connection conn, String module, String docCode, String newDocCode, int version, int newVersion, int taskId) throws SQLException{
		
		DBManager dbm = new DBManager();
		try{
			
			String compCode = principal.getCompCode();
			DynData dao = new DynData();
			DynUI ui = dao.getDynUI(module, conn);	
			
			//get attachments from db
			HashMap<String, Object> keys = new HashMap<String, Object>();
			keys.put("doc_code", docCode);
			keys.put("version", version);
			if(ui.hasCompFilter()){
				keys.put("comp_code", compCode);
			}
			JSONArray attArray = dao.getAttachments(keys, dbm, conn);
			dbm.clear();
			
			//insert into data table
			int attSize = attArray.length();
			for(int i = 0; i < attSize; i++){
				JSONObject att = attArray.getJSONObject(i);
				String attId = att.getString("attId");
				String file = att.getString("attFile");
				dao.insertAttachment(taskId, compCode, module, newDocCode, newVersion, attId, file, dbm, conn);
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			dbm.clear();
			dbm = null;
		}
		return false;
	}

}
