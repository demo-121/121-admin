package com.eab.biz.dynTemplate;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.biz.dynamic.DynamicManager;
import com.eab.biz.products.ProductMgr;
import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.dynTemplate.DynData;
import com.eab.dao.dynTemplate.DynTemplate;
import com.eab.dao.tasks.Tasks;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.model.dynTemplate.DynUI;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.tasks.TaskBean;
import com.google.gson.JsonObject;

public class NewVersionMgr {
	
	HttpServletRequest request;
	UserPrincipalBean principal;
	HttpSession session;
	Language langObj;
	
	public NewVersionMgr(HttpServletRequest request) {
		this.request = request;
		this.session = request.getSession();
		principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
	}

	public Response newVersion() {
		String para = request.getParameter("p0");
		byte[] decoded = Base64.decodeBase64(para.replace(" ", "+"));
		String paraStr = new String(decoded, StandardCharsets.UTF_8);	
		JSONObject paraJson = new JSONObject(paraStr);
		String module = request.getParameter("p1");
		DynamicManager dynMgr = new DynamicManager(request);
		Response resp = dynMgr.returnDialog("NEW_VERSION_ERROR_MSG", "newVersionError", "NEW_VERSION_ERROR_TITLE");
		try {
			JSONArray rows = paraJson.getJSONArray("rows");
			DynData dynData = new DynData();
			
			
			//if rows length > 1, refresh list, else goto detail
			if(rows.length()>0){
				if(rows.length()>1){
					ListMgr manager = new ListMgr(request);
					if(newVersions(principal, null, rows, module))
						resp = manager.getDynList();
				}else{
					DetailMgr detailMgr = new DetailMgr(request);
					String docCode =  rows.getString(0);
					int version = newVersion(principal, null, docCode, module);
					if(version > 0)
						resp = detailMgr.getDetails(module, docCode, version, null);
				}
			}
			
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resp;
	}
	
	private boolean newVersions(UserPrincipalBean principal, Connection conn, JSONArray docCodes, String module) throws SQLException{
		boolean isNew = false;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
			}
			for(int i=0; i<docCodes.length(); i++){
				String docCode = docCodes.getString(i);
				if(newVersion(principal, conn, docCode, module) < 1){
					return false;
				}
			}
			if(isNew){
				conn.commit();
			}
			return true;
		}catch(Exception e){
			conn.rollback();
			Log.error("newVersions error: " + e.getMessage());	
		}finally{
			if(isNew){
				conn.close();
				conn = null;	
			}
		}
		return false;
	}
	
	private int newVersion(UserPrincipalBean principal, Connection conn, String docCode, String module) throws SQLException{
		boolean isNew = false;
		DBManager dbm;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
			}
			dbm = new DBManager();
			DynData dataDao = new DynData();
			
			//get the template version
			int version = dataDao.getVersion(principal, conn, docCode, module);
			
			//get the template data
			JSONObject data = dataDao.getDynTrxData(principal, conn, docCode, version, module);
			
			//get the latest version of template
			int newVersion = dataDao.getNextVersion(principal, conn, docCode, module);
						
			
			//insert task
			Tasks taskDao = new Tasks();
			JSONObject taskParam = new JSONObject();
			taskParam.put("VER_NO", String.valueOf(newVersion));
			taskDao.addTaskFromTask(principal, conn, module, docCode, version, taskParam);

			int taskId = taskDao.getTaskId(conn, principal.getCompCode(), module, docCode, newVersion);
			
			JSONObject dataParam = new JSONObject();
			dataParam.put("VERSION", newVersion);
			//insert into data table
			dataDao.addTrxDataFromTrxData(principal, conn, module, docCode, version, dataParam);
			
			//insert into name table
			dataDao.addNameFromName(principal, conn, module, docCode, version, dataParam);
			
			//insert into section table
			dataDao.addSectionFromSection(principal, conn, module, docCode, version, dataParam);
			
			//insert into json table
			CloneMgr clnMgr = new CloneMgr(request);
			clnMgr.cloneJson(conn, module, docCode, docCode, version, newVersion, taskId);
			
			
			//insert into attachment table
			clnMgr.cloneAttachment(conn, module, docCode, docCode, version, newVersion, taskId);
			
			
			if(isNew){
				conn.commit();
			}
			return newVersion;
		}catch(Exception e){
			conn.rollback();
			Log.error("newVersion error: " + e.getMessage());	
		}finally{
			dbm = null;
			if(isNew){
				conn.close();
				conn = null;	
			}
		}
		return -1;
	}

}
