package com.eab.biz.dynTemplate;

import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.biz.products.ProductMgr;
import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.common.Constants.ActionTypes;
import com.eab.dao.dynTemplate.DynData;
import com.eab.dao.dynTemplate.DynTemplate;
import com.eab.dao.products.Products;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.JsonObject;

public class SuspendMgr {
	
	HttpServletRequest request;
	UserPrincipalBean principal;
	HttpSession session;
	Language langObj;
	
	public SuspendMgr(HttpServletRequest request) {
		this.request = request;
		this.session = request.getSession();
		principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
	}

	public Response suspend() {
		String para = request.getParameter("p0");
		byte[] decoded = Base64.decodeBase64(para.replace(" ", "+"));
		String paraStr = new String(decoded, StandardCharsets.UTF_8);	
		JSONObject paraJson = new JSONObject(paraStr);
		String module = request.getParameter("p1");
		JSONArray docCodes = paraJson.getJSONArray("rows");
		return suspend(docCodes, module);	
		
	}
	
	public Response suspend(JSONArray docCodes, String module){
		DynData dynData = new DynData();
		Response resp = null;
		
		try {
			//if rows length > 1, refresh list, else goto detail
			if(docCodes.length()>1){
				ListMgr manager = new ListMgr(request);
				dynData.suspend(principal, null, docCodes, module);
				resp = manager.getDynList();
			}else{
				ProductMgr productManager = new ProductMgr(request);
				String docCode =  docCodes.getString(0);
				int version = dynData.suspend(principal, null, docCode, module);
				resp = productManager.getProdDetail(docCode, version, null);
			}
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

}
