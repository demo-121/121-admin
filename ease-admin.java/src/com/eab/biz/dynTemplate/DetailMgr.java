package com.eab.biz.dynTemplate;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.eab.biz.tasks.TaskInfoMgr;
import com.eab.common.Constants;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.common.Constants.ActionTypes;
import com.eab.dao.dynTemplate.DynData;
import com.eab.dao.dynTemplate.DynTemplate;
import com.eab.dao.tasks.Tasks;
import com.eab.database.jdbc.DBAccess;
import com.eab.json.model.AppBar;
import com.eab.json.model.Content;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.model.dynTemplate.DynUI;
import com.eab.webservice.dynamic.DynamicFunction;
import com.google.gson.JsonObject;

public class DetailMgr extends DynMgr{
	public DetailMgr(HttpServletRequest request) {
		super(request);
	}
	
	public Response getDetailView(){
		int version = -1;
		String docCode = null;
		try {

			String param = request.getParameter("p0");
			String module = request.getParameter("p1");
			if (param != null && !param.isEmpty()) {
				String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
				JSONObject resultJSON = new JSONObject(resultStr);
				
				if (resultJSON.has("id"))
					docCode = resultJSON.getString("id");
				
				//get target version 
				if (resultJSON.has("version")){
					Object version_ = resultJSON.get("version");
					if(version_.getClass() == String.class){
						version = Integer.parseInt(resultJSON.getString("version"));
					}else{
						version = resultJSON.getInt("version");
					}
				}
					
				String selectPageId;
				if(resultJSON.has("selectPageId"))
					selectPageId  = resultJSON.getString("selectPageId");
				else
					selectPageId = Constants.ProductPage.TASK;

				return getDetails(module, docCode, version, selectPageId);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * 1. get sections from dyn_section
	 * 2. get full layout template
	 * 3. get task id
	 * 4. set page info
	 */
	public Response getDetails(String module, String docCode, int version, String selectPageId){
		Response resp = null;
		Connection conn = null;
		DynTemplate dao = new DynTemplate();
		
		try{
			conn = DBAccess.getConnection();
			if (selectPageId == null || selectPageId.equals(""))
				selectPageId = Constants.ProductPage.TASK;
			
			if(version < 1){
				DynData dynData = new DynData();
				version = dynData.getVersion(principal, conn, docCode, module);
				if(version < 1) version = 1;
			}
			
			DynUI ui = dao.getDynUI(module, conn);
			
			AppBar appBar = getAppBar(conn, ui, module, "detail", docCode);
			JsonObject barValues = new JsonObject();
			barValues.addProperty("version", version+"");
			appBar.setValues(barValues);
			//TODO 1. edit lock 
			//TODO 2. check update right
			
			// Action
			String action = ActionTypes.CHANGE_PAGE;
			
			// Page
			JsonObject pageValue = new JsonObject();
			pageValue.addProperty("id", docCode);
			pageValue.addProperty("version", version);
			pageValue.addProperty("selectPageId", selectPageId);
			String pageTitle = module.toUpperCase() + ".DETAIL.TITLE";
			
			Page page = new Page("/DynTemplate/Detail", langObj.getNameDesc(pageTitle), module, pageValue);
			
			TaskInfoMgr taskMgr = new TaskInfoMgr(request, module);
			Tasks tasksDao = new Tasks();
			Map<String, Object> task = tasksDao.getTaskById(-1, docCode, version, module, null, principal);
			
			JsonObject values = new JsonObject();
			taskMgr.addValues(task, values);
			
			Map<String, JSONObject> sectionMap = dataDao.getSections(conn, principal, module, docCode, version, null);
			if(sectionMap != null){
				for (Map.Entry<String, JSONObject> entry : sectionMap.entrySet())
				{
				    String sectionKey = entry.getKey();
				    if(!"TaskInfo".equalsIgnoreCase(sectionKey)){
				    	JSONObject section = entry.getValue();
				    	values.add(sectionKey, DynamicFunction.convertJsonToGson(section));
				    }
				}
			}
			
			values.add("id", gson.toJsonTree(docCode));
			values.add("version", gson.toJsonTree(version));

			values.addProperty("isNew", false);
			
			JsonObject selectPage = new JsonObject();
			selectPage.addProperty("id", selectPageId);
			values.add("selectPage", gson.toJsonTree(selectPage));
			
			//get template
			Template template = parseTemplate(getTemplateJson(conn, ui, "NEW", docCode, version));
			//content
			Content content = new Content(template, values, values);
			// Token
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());
			resp =  new Response(action, null, page, tokenID, tokenKey, appBar, content);
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return resp;
	}
	
	//translate template
	public JSONObject translateTemplate(JSONObject template){	
		try{
			//first, translate title
			List<String> nameList = new ArrayList<String>();
			
			getTemplateNameList(template, nameList);
			Log.error("nameList: " + nameList.toString());
			Map<String, String> nameMap = Function2.getTranslateMap(request, nameList);
			
			Log.error("nameMap: " + nameMap.toString());
			setTemplateName(template, nameMap);
			
			//second, get options
		}catch(Exception e){
			Log.error("translateProdTemplate error : " + e.getMessage());
		}
		
		return template;
	}
		
	public void getTemplateNameList(JSONObject template, List<String> list){
		if(template.has("title") && template.get("title").getClass() == String.class){
			list.add(template.getString("title"));
		}
		if(template.has("items")){
			JSONArray items = template.getJSONArray("items");
			int itemSize = items.length();
			for(int itemIndex = 0; itemIndex < itemSize; itemIndex++){
				getTemplateNameList(items.getJSONObject(itemIndex), list);
			}
		}
	}
		
	public void setTemplateName(JSONObject template, Map<String, String> nameMap){
		if(template.has("title")){
			String title = template.getString("title");
			if(nameMap.containsKey(title))
				template.put("title", nameMap.get(title));
		}
		if(template.has("items")){
			JSONArray items = template.getJSONArray("items");
			int itemSize = items.length();
			for(int itemIndex = 0; itemIndex < itemSize; itemIndex++){
				setTemplateName(items.getJSONObject(itemIndex), nameMap);
			}
		}
	}
		
	private JSONObject getPlanDetails(Connection conn, String module, String code, int version) {
		JSONObject planDetails = new JSONObject();
		String json = dataDao.getProductJson(conn, "product", code, version);
		
		if (json != null && !json.isEmpty()) {
			JSONObject plan = new JSONObject(json);
			// erase thumbnail for perf.
			plan.put("thumbnail3", "");
			plan.put("thumbnail4", "");
			planDetails.put(code, plan);

			if (plan.has("riderList")) {
				JSONArray riderList = plan.getJSONArray("riderList");
				
				for (Object item : riderList) {
					JSONObject rider = (JSONObject) item;
					String riderCode = rider.getString("covCode");
					json = dataDao.getProductJson(conn, module, riderCode, 1);//TODO(version number should not be hardcoded)
					if (json != null && !json.isEmpty()) {
						JSONObject riderDetails = new JSONObject(json);
						// erase thumbnail for perf.
						riderDetails.put("thumbnail3", "");
						riderDetails.put("thumbnail4", "");
						planDetails.put(riderCode, riderDetails);
					}
				}
			}
		}
		return planDetails;
	
	}
	
	public JSONObject getFuncParas() {
		//TODO : set these 3 params
		String param = request.getParameter("p0");
		String module = request.getParameter("p1");
		String docCode = "";

		int version = 1;
		JSONObject paraJSON = null;

		int age = 25;
		String gender = "M";
		String smoker = "N";
		
		Connection conn = null;
		JSONObject resp = new JSONObject();
		try {
			conn = DBAccess.getConnection();
			
			if (param != null && !param.isEmpty()) {
				String paraStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
				paraJSON = new JSONObject(paraStr);
				docCode = paraJSON.getString("docCode");
				version = paraJSON.getInt("version");
				
			}
			
			JSONObject data = layoutDao.getFunctionData();
			
			JSONObject defaultParas = new JSONObject();
			
			String json = null;
			if (paraJSON.has("params")  && !paraJSON.isNull("params")) {
				if (data.has("defParams")) {
					JSONObject defParams = data.getJSONObject("defParams");
					JSONObject planDetails = null;
					JSONObject planIllustProps = null;
					JSONObject extraPara = null;
					JSONObject quotation = null;
				
					JSONArray paramArr = paraJSON.getJSONArray("params");
						
					for (Object paraObj : paramArr) {
						JSONObject para = (JSONObject) paraObj;
						String type = para.has("type")?para.getString("type"):"";
						String planCode = para.has("code")?para.getString("code"): null;
						int planVer = para.has("version")?para.getInt("version"): 0;
						if (type.equals("quotation")) {
							if (defParams.has(type)) {
								quotation = defParams.getJSONObject(type);								
								if (quotation != null) {
									if (planDetails == null) {
										if (planCode != null && !planCode.isEmpty()) {
											planDetails = getPlanDetails(conn, module, planCode, version);
										} else if ("product".equals(module)) {
											planDetails = getPlanDetails(conn, module, docCode, version);									
										}
									}								
									age = quotation.getInt("iAge");
									gender = quotation.getString("iGender");
									smoker = quotation.getString("iSmoke");
									
									// set base product code
									if (!quotation.has("baseProductCode") || quotation.isNull("baseProductCode") || quotation.getString("baseProductCode").isEmpty()) {
										if (planCode != null && !planCode.isEmpty()) {
											quotation.put("baseProductCode", planCode);
										} else if ("product".equals(module)) {
											quotation.put("baseProductCode", docCode);
										}
									}
									
									// set basic plan to plans
									if (quotation.has("plans") && !quotation.isNull("plans")) {
										try {
											JSONArray plans = (JSONArray) quotation.get("plans");
											if (plans != null) {
												JSONObject planInfo = plans.getJSONObject(0);
												if (!planInfo.has("covCode") || planInfo.isNull("covCode") || planInfo.getString("covCode").isEmpty()) {
													if (planCode != null && !planCode.isEmpty()) {
														quotation.getJSONArray("plans").getJSONObject(0).put("covCode", planCode);
														quotation.getJSONArray("plans").getJSONObject(0).put("version", planVer);
													} else if ("product".equals(module)) {
														quotation.getJSONArray("plans").getJSONObject(0).put("covCode", docCode);
														quotation.getJSONArray("plans").getJSONObject(0).put("version", version);
													}
												} else {
													planCode = planInfo.getString("covCode");
													version = planInfo.getInt("version");
												}
											}
										} catch (Exception e) {
											quotation.remove("plans");
										}
									}
									
									if (planDetails == null) {
										planDetails = getPlanDetails(conn, module, planCode, version);
									}

									if (planDetails != null) {
										JSONObject plan = planDetails.has(planCode)?planDetails.getJSONObject(planCode):null;
										if (plan != null) {
											if (plan.has("policyOptions")) {
										
												JSONArray pOptions = plan.getJSONArray("policyOptions");
												JSONObject policyOptions = new JSONObject();
												try {
												for(Object option : pOptions) {
													JSONObject opt = (JSONObject) option;
													if (opt.has("id")) {
														policyOptions.put(opt.getString("id"), opt.has("value")?opt.getString("value"):"");
													}
												}
												quotation.put("policyOptions", policyOptions);
												} catch (Exception ex) {
													Log.debug("prase policyOption error:"+ pOptions.toString());
												}
											}
											if (plan.has("currencies")) {
												JSONArray currencies = plan.getJSONArray("currencies");
												if (currencies.length() > 0) {
													JSONObject ccy =  currencies.getJSONObject(0);
													quotation.put("residence", ccy.getString("country"));
													if (ccy.has("ccy")) {
														JSONArray ccys = null;
														try {
															ccys = ccy.getJSONArray("ccy");
														} catch (Exception e) {
															String ccyStr = ccy.getString("ccy");
															ccys = new JSONArray();
															String [] ccyss = ccyStr.split(",");
															for (String cc: ccyss) {
																ccys.put(cc);
															}														
														}
														quotation.put("ccy", ccys.getString(0));
													}
												}
											}
											if (plan.has("payModes")) {
												JSONArray payModes = plan.getJSONArray("payModes");
												if (payModes.length() > 0) {
													JSONObject paymode =  payModes.getJSONObject(0);
													quotation.put("paymode", paymode.getString("mode"));
												}											
											}
										}
									}
									defaultParas.put(type, quotation);
								}
							}
						} else if (type.equals("planInfo")) {							
							try {
								JSONObject planInfo = defParams.getJSONObject("quotation").getJSONArray("plans").getJSONObject(0);							
								if (planInfo != null) {
									if (!planInfo.has("covCode") || planInfo.isNull("covCode") || planInfo.getString("covCode").isEmpty()) {
										if (planCode != null && !planCode.isEmpty()) {
											planInfo.put("covCode", planCode);
										} else if ("product".equals(module)) {
											planInfo.put("covCode", docCode);
										}
									}
									defaultParas.put(type, planInfo); 
								}
							} catch (Exception e) {
								defaultParas.put(type, new JSONObject());
							}
						} else if (type.equals("planDetails")) {
							if (planDetails == null) {
								if (planCode != null && !planCode.isEmpty()) {
									planDetails = getPlanDetails(conn, module, planCode, version);
								} else if ("product".equals(module)) {
									planDetails = getPlanDetails(conn, module, docCode, version);									
								}
							}
							if (planDetails != null) {
								defaultParas.put(type, planDetails); 
							} else {
								defaultParas.put(type, new JSONObject());
							}
						} else if (type.equals("planDetail")) {
							if (planCode != null && !planCode.isEmpty() && planDetails != null && planDetails.has(planCode)) {
								defaultParas.put(type, planDetails.getJSONObject(planCode));
							} else {
								if (planCode != null && !planCode.isEmpty()) {									
									json = dataDao.getProductJson(conn, "product", planCode, planVer);
								} else if ("product".equals(module)) {
									json = dataDao.getProductJson(conn, "product", docCode, version);
								}
								
								if (json != null && !json.isEmpty()) {
									try {
										JSONObject plan = new JSONObject(json);
										plan.put("thumbnail3", "");
										plan.put("thumbnail4", "");

										defaultParas.put(type, plan);
									} catch (Exception e) {
										defaultParas.put(type, new JSONObject());
									}
								} else {
									defaultParas.put(type, new JSONObject());
								}
							} 
						} else if (type.equals("extraPara")) {
							if (defParams.has(type)) {
								extraPara = defParams.getJSONObject(type);
								
								if (quotation != null && quotation.has("plans")) {
									// get illustrate props
									planIllustProps = new JSONObject();
									String rateKey = gender + smoker + age;
									JSONArray plans = quotation.getJSONArray("plans");
									
									for (Object item : plans) {
										JSONObject plan = (JSONObject) item;
										String code = plan.getString("covCode");		
										int ver = plan.has("version")?plan.getInt("version"):0;
										json = dataDao.getProductJson(conn, module, code + "_" + rateKey, ver);
										if (json == null || json.isEmpty()) {
											json = dataDao.getProductJson(conn, module, code + "_A" + smoker + age, ver);
										}
										if (json == null || json.isEmpty()) {
											json = dataDao.getProductJson(conn, module, code + "_" + gender + "A" + age, ver);
										}
										if (json == null || json.isEmpty()) {
											json = dataDao.getProductJson(conn, module, code + "_AA" + age, ver);
										}
										if (json != null && !json.isEmpty()) {
											planIllustProps.put(code, new JSONObject(json));
										}
									}
									extraPara.put("planIllustProps", planIllustProps);
									
									JSONObject plan = null;
									if (planCode != null && !planCode.isEmpty() && planDetails.has(planCode)) {
										plan = planDetails.getJSONObject(planCode);
									} else if ("product".equals(module)) {
										plan = planDetails.getJSONObject(docCode);
									}
									 
									// get PDF template func
									if (plan != null) {
										JSONObject tempFuncs = new JSONObject();																			
										if (plan.has("reportTemplate")) {
											JSONArray reportTemplate = plan.getJSONArray("reportTemplate");
											for (Object tempObj : reportTemplate) {
												JSONObject temp = (JSONObject) tempObj;
												//String covCode = temp.has("covCode")?temp.getString("covCode"):null;
												try {
													String tempCode = temp.getString("code");
													json = dataDao.getProductJson(conn, "pdf", tempCode, 0);
													JSONObject tempJSON = new JSONObject(json);
													
													String func = tempJSON.getJSONObject("formulas").getString("prepareReportData");
													tempFuncs.put(tempCode, func);												
												} catch (Exception e) {
													
												}											
											}
										}
										extraPara.put("templateFuncs", tempFuncs);
									}
								}

								defaultParas.put(type, extraPara);
							} else {
								defaultParas.put(type, new JSONObject());
							}
						} else if (defParams.has(type)){
							defaultParas.put(type, defParams.getJSONObject(type));
						} else {
							Log.error("invalid type:"+ type);
						}
					}
				}
			}
			data.put("defParams", defaultParas);
			resp.put("data", data);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			resp = new JSONObject();
		} catch (JSONException e) {
			e.printStackTrace();
			resp = new JSONObject();
		} catch (SQLException e ){
			resp = new JSONObject();
		}finally{
			try {
				if(conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		resp.put("tokenID", tokenID);
		resp.put("tokenKey", tokenKey);
		
		return resp;
	}
	
//	public Dialog getLockDialog(){
//		
//	}

}
