package com.eab.biz.dynTemplate;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;
import com.eab.common.Constants;
import com.eab.common.Function2;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Constants.ActionTypes;
import com.eab.dao.common.Functions;
import com.eab.dao.dynTemplate.DynData;
import com.eab.dao.dynTemplate.DynLayout;
import com.eab.dao.dynTemplate.DynTemplate;
import com.eab.dao.dynamic.Companies;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.DialogItem;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.model.dynTemplate.DynField;
import com.eab.model.dynTemplate.DynUI;
import com.eab.model.dynamic.CompanyBean;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class DynMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	DynLayout layoutDao;
	DynData dataDao;
	HttpSession session;
	Gson gson;

	public DynMgr(HttpServletRequest request) {
		super();
		this.request = request;
		session = request.getSession();
		layoutDao = new DynLayout();
		dataDao = new DynData();
		principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public JSONObject getTemplateJson(Connection conn, String module, String docCode, int version, UserPrincipalBean principal) throws SQLException{
		DynData dataDao = new DynData();
		String templateCode = null;
		//add new 
		if(docCode == null){
			templateCode = "ADD";
		}else{
			try{
				JSONObject trxData = dataDao.getDynTrxData(principal, conn, docCode, version, module);
				if(trxData != null){
					Log.error("trxData: " + trxData.toString());
				}else{
					Log.error("trxData: nunll");
				}
				templateCode = trxData.getString("templateCode");
			}catch(Exception e){
				//it has no template code be4 a record stored in trx table
				templateCode = "ADD";
			}
		}
		if(templateCode == null){
			templateCode = "ADD";
		}
		JSONObject temp = (JSONObject)layoutDao.getTemplate(module, principal, templateCode, layoutDao.RETURN_JSON);
		return temp;
	}
	
	public JSONObject getTemplateJson(Connection conn, DynUI ui, String templateType, String docCode, int version){
		JSONObject template = new JSONObject();
		try {
			JSONObject jsonTemplate = getTemplateJson(conn, ui.getModule(), docCode, version, principal);
			String task = layoutDao.getTaskInfoSection(conn, templateType);
			
			template = new JSONObject(jsonTemplate.toString());
			JSONObject taskjson = new JSONObject(task);
			if(jsonTemplate.has("items")){
				JSONArray items = jsonTemplate.getJSONArray("items");
				JSONArray newItems = new JSONArray();
				newItems.put(taskjson);
				for(Object item: items){
					newItems.put(item);
				}
				template.put("items", newItems);
			}
			if(template.has("type")){
				Log.error("type: " + template.getString("type"));
			}else{
				Log.error("type: no type");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return template;
		
	}
	public Template parseTemplate(JSONObject template){
		fillTemplateOptions(template);
		Gson gson = new Gson();
		Template template_ = gson.fromJson(template.toString(), Template.class);;
		translateTemplate_(template_);
		return template_;
	}
	
	public void translateTemplate_(Template template){
		List<String> names = new ArrayList<String>();
		template.getNameCodes(names);
		Map<String, String> lmap;
		try {
			lmap = Function2.getTranslateMap(request, names);
			template.translate(lmap);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/*
	 * put options into picker.
	 * type 1: look up table
	 * type 2: mst table
	 */
	public void fillTemplateOptions(JSONObject template){
		if(template.has("optionTable") && template.get("optionTable") instanceof String){
			String table = template.getString("optionTable");
			if (table.isEmpty()) {
			} else {
				JSONArray options = new JSONArray();
				if("SYS_LOOKUP_MST".equalsIgnoreCase(table)){
					//template sample: {options: "SYS_LOOKUP_MST", subtype: "[lookup_key]"}
					options =  layoutDao.getLookUpOptions(template, principal);
				}
				else{
					options =  layoutDao.getMstOptions(template, principal);
				}
				template.put("options", options);
			}
		}
		if(template.has("items")){
			JSONArray items = template.getJSONArray("items");
			int itemSize = items.length();
			for(int itemIndex = 0; itemIndex < itemSize; itemIndex++){
				fillTemplateOptions(items.getJSONObject(itemIndex));
			}
		}
	}
	
	
	/*
	 1. get title
	 2. get options 
	 */
	public AppBar getAppBar(Connection conn, DynUI ui, String module, String viewType, String docCode) {
		AppBar appBar = null;
		DBManager dbm = new DBManager();
		ResultSet rs = null, rs1 = null;
		DynTemplate dao = new DynTemplate();
		try {
			if(ui == null)
				ui = dao.getDynUI(module, conn); 
			// get title
			String sqlTitle = " select * from dyn_template_action_mst"
							+ " where"
							+ "	ui_type = "
							+ dbm.param(viewType, DataType.TEXT) 
							+ " and module = " 
							+ dbm.param(module, DataType.TEXT) 
							+ " and is_title = " + dbm.param("Y", DataType.TEXT)
							+ " order by seq";
			rs = dbm.select(sqlTitle, conn);

			List<Field> tts = new ArrayList<Field>();
			if (rs != null) {
				while (rs.next()) {
					String id = rs.getString("id");
					String title = rs.getString("title");
					String optionsLookUp = rs.getString("options_lookup");
					String lookupField = rs.getString("lookup_field");
					String defaultValue = rs.getString("default_value");
					String langFilter = rs.getString("lang_filter");
					String type = rs.getString("type");
					String compFilter = rs.getString("comp_filter");
					String present = rs.getString("present");
					String setAll = rs.getString("set_all");
					
					Field tField = new Field();
					tField.setId(id);
					tField.setType(type);
					tField.setTitle(title);
					tField.setValue(defaultValue);
					tField.setSetAll("Y".equals(setAll));
					
					DynField field = new DynField();
					field.setOptionsLookUp(optionsLookUp);
					field.setLookUpField(lookupField);
					field.setLangFilter(langFilter);
					field.setCompFilter(compFilter);
					field.setPresent(present); 
					
					if (optionsLookUp != null && !"".equals(optionsLookUp)) {
						List<Option> options = new ArrayList<Option>();
						Map<String, String> mapOptions = dao.getLookUpList(ui, field, conn, principal, docCode);
						
						for (Map.Entry<String, String> thisOption : mapOptions.entrySet())
						{
						    options.add(new Option(thisOption.getValue(), thisOption.getKey()));
						}
						// set on UI
//						if(tField.getSetAll())
//							options.add(new Option("ALL", langObj.getNameDesc("ALL")));
						
						tField.setMandatory(true);
						tField.setOptions(options);
					}

					tts.add(tField);
				}
			}

			// hard code, supposed it has at most 2 titles
			AppBarTitle appBarTitle = null;
			if (tts.size() > 0) {
				Field title2 = (tts.size() > 1) ? tts.get(1) : null;
				Field title1 = tts.get(0);
				appBarTitle = new AppBarTitle(title1.getId(), title1.getType(), (String) title1.getValue(), null,
						title1.getOptions(), title2);
				appBarTitle.setSetAll(title1.getSetAll());
				appBarTitle.setPrimary(langObj.getNameDesc(title1.getTitle()));
			}

			// get actions by level
			// it may has 4 levels of action in module such as product
			
			dbm.clear();
			String sqlAction = "select * from dyn_template_action_mst "
								+ " where ui_type = "
								+ dbm.param(viewType, DataType.TEXT)
								+ " and module = " 
								+ dbm.param(module, DataType.TEXT) 
								+ " and is_title = "
								+ dbm.param("N", DataType.TEXT)
								+ " order by lvl ASC, seq";

			rs1 = dbm.select(sqlAction, conn);
			List<List<Field>> appBarActions = new ArrayList<>();
			if (rs1 != null) {
				int lvlIndex = 1;
				int rowcount = 0;
				if (rs1.last()) {
				  rowcount = rs1.getRow();
				  rs1.beforeFirst(); // not rs.first() because the rs.next() below will move on, missing the first element
				}
				List<Field> actions = new ArrayList<Field>();
				
				int countIndex = 0;
				while (rs1.next()) {
					countIndex += 1;
					int lvl = rs1.getInt("lvl");
					if(lvl > lvlIndex){
						lvlIndex += 1;
						addActions(actions, appBarActions);
						actions.clear();
					}
					Field action = new Field();
					action.setId(rs1.getString("id"));
					
					action.setValue(rs1.getString("default_value"));
					action.setTitle(rs1.getString("title"));
					
					String optionsLookUp = rs1.getString("options_lookup");
					String lookupField = rs1.getString("lookup_field");
					String langFilter = rs1.getString("lang_filter");
					String compFilter = rs1.getString("comp_filter");
					String present = rs1.getString("present");
					
					//get options from lookup or mst
					if (optionsLookUp != null && !"".equals(optionsLookUp)) {
						List<Option> options = new ArrayList<Option>();
						DynField field = new DynField();
						field.setOptionsLookUp(optionsLookUp);
						field.setLookUpField(lookupField);
						field.setLangFilter(langFilter);
						field.setCompFilter(compFilter);
						field.setPresent(present); 
						Map<String, String> mapOptions = dao.getLookUpList(ui, field, conn, principal, docCode);
																		
						for (Map.Entry<String, String> thisOption : mapOptions.entrySet())
						{
						    options.add(new Option(thisOption.getValue(),thisOption.getKey()));
						}
						//add a more option "ALL"
//						if (setAll != null && setAll.equals("Y")) {
//							options.add(new Option("ALL", langObj.getNameDesc("ALL")));
//						}
						action.setMandatory(true);
						action.setOptions(options);
					}
					String type = rs1.getString("type");
					action.setType(type);
					String subType = rs1.getString("sub_type");
					if("dialogButton".equalsIgnoreCase(type)){
						if("new".equalsIgnoreCase(subType)){
							action.setDialog(newVersionDialog(module));
						}else if("clone".equalsIgnoreCase(subType)){
							action.setDialog(cloneDialog(module));
						}else if("suspend".equalsIgnoreCase(subType)){
							action.setDialog(suspendDialog(module));
						}
					}
					
					actions.add(action);
					
					//check if its last row, handle if yes
					if(countIndex == rowcount ){
						addActions(actions, appBarActions);
					}
					
				}
				 appBar = new AppBar(appBarTitle, appBarActions, null);
			}

			// get other action
		} catch (Exception e) {
			Log.error("getAppBar error : " + e.getMessage() );
			e.printStackTrace();
		}finally{
			try {
				if(rs != null && !rs.isClosed())
					rs.close();
				if(rs1 != null && !rs1.isClosed())
					rs1.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			rs = null;
			rs1 = null;
			dbm.clear();
			dbm = null;
		}

		return appBar;
	}
	
	private List<List<Field>> addActions(List<Field> actions, List<List<Field>> appBarActions){
		List<Field> _actions = new ArrayList<Field>();
		int size = actions.size();
		
		for(int i=0; i < size; i++){
			Field action = actions.get(i);
			String title = action.getTitle();
			action.setTitle(langObj.getNameDesc(title));
			_actions.add(action);
		}
		appBarActions.add(_actions);
		return appBarActions;
	}
	
	public Dialog cloneDialog(String module) {
		Companies companiesDAO = new Companies();

		// Clone - Company Dialog Items
		List<Option> companyOptions = new ArrayList<>();
		try {
			List<CompanyBean> companyList = companiesDAO.getCompanyListByUser(request);

			for (int i = 0; i < companyList.size(); i++) {
				CompanyBean company = companyList.get(i);
				companyOptions.add(new Option(company.getCompName(), company.getCompCode()));
			}
		} catch (SQLException e1) {
			Log.error(e1);
		}

		List<Field> companyDialogItems = new ArrayList<>();
		companyDialogItems.add(new DialogItem("compCode", DialogItem.Types.RADIOBUTTON, null, true, companyOptions));

		// Clone - Company Positive
		Field companyPositive = new Field("/DynTemplate/CheckExist", "submitButton", langObj.getNameDesc("BUTTON.OK"));
		// Clone - Company Negative
		Field companyNegative = new Field("company_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL"));
		// Clone - Company Dialog
		Dialog companyDialog = new Dialog("company",
				langObj.getNameDesc(module.toUpperCase() + ".CLONE_DIALOG.SELECT_COMP"),
				langObj.getNameDesc(module.toUpperCase() + ".CLONE_DIALOG.SELECT_COMP_MSG"), companyPositive,
				companyNegative, companyDialogItems, 40);

		// Clone - Confirm Positive
		Field cloneConfirmPositive = new Field("confirm_ok", "dialogButton", langObj.getNameDesc("BUTTON.OK"),
				companyDialog);
		// Clone - Confirm Negative
		Field cloneConfirmNegative = new Field("confirm_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL"));
		// Clone - Confirm Dialog
		Dialog cloneConfirmDialog = new Dialog("confirm",
				langObj.getNameDesc(module.toUpperCase() + ".CLONE_DIALOG.CLONE"),
				langObj.getNameDesc(module.toUpperCase() + ".CLONE_DIALOG.CLONE_MSG"), cloneConfirmPositive,
				cloneConfirmNegative, 30);

		// return cloneConfirmDialog;
		return companyDialog;
	}
	
	public Dialog suspendDialog(String module) {

		// Suspend - Suspend Dialog Items

		List<Field> suspendDialogItems = new ArrayList<>();
		suspendDialogItems.add(new DialogItem("effDate", DialogItem.Types.DATEPICKER, null, true, null));

		// Suspend - Suspend Positive
		Field suspendPositive = new Field("/DynTemplate/Suspend", "submitButton", langObj.getNameDesc("BUTTON.OK"));
		// Suspend - Suspend Negative
		Field suspendNegative = new Field("company_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL"));
		// Clone - Company Dialog
		Dialog suspendDialog = new Dialog("suspend",
				langObj.getNameDesc(module.toUpperCase() + ".SUSPEND_DIALOG.SELECT_SUSPEND_DATE"),
				langObj.getNameDesc(module.toUpperCase() + ".SUSPEND_DIALOG.SELECT_SUSPEND_DATE_MSG"), suspendPositive,
				suspendNegative, suspendDialogItems, 40);


		// return cloneConfirmDialog;
		return suspendDialog;
	}

	
	public Dialog newVersionDialog(String module) {
		// New Version - Confirm Positive
		Field newVersionConfirmPositive = new Field("/DynTemplate/NewVersion", "submitButton",
				langObj.getNameDesc("BUTTON.OK"));
		// New Version - Confirm Negative
		Field newVersionCconfirmNegative = new Field("confirm_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL"));
		// New Version - Confirm Dialog
		Dialog newVersionConfirmDialog = new Dialog("confirm",
				langObj.getNameDesc(module.toUpperCase() + ".CLONE_DIALOG.NEW_VERSION"),
				langObj.getNameDesc(module.toUpperCase() + ".CLONE_DIALOG.NEW_VERSION_CONFIRM"),
				newVersionConfirmPositive, newVersionCconfirmNegative, 30);

		return newVersionConfirmDialog;
	}
	
	public Response changePage(String selectPageId, String targetPageId, String docCode, int version) {
		// Action
		String action = ActionTypes.RESET_VALUES;

		JsonObject values = new JsonObject();
		JsonObject selectPage = new JsonObject();
		selectPage.addProperty("id", targetPageId.equals("")?selectPageId:targetPageId);
		
//		if (selectPageId.equals(Constants.ProductPage.TASK)) {
//			TaskInfoMgr taskMgr = new TaskInfoMgr(request, TaskInfoMgr.module.PRODUCT);
//		
//			if (prodCode != null && !"".equals(prodCode))
//				taskMgr.addValues(values, -1, prodCode, version, null);
//		}
		
		values.add("selectPage", gson.toJsonTree(selectPage));
		values.addProperty("actionType", "merge");
		values.addProperty("id", docCode);
		Content content = new Content(null, values);
		
		return new Response(action, null, null, null, null, null, content);
	}
	
	public String getDocCode(JSONObject values, String primaryID){
		Iterator<?> keys = values.keys();
		String code = null;
		while( keys.hasNext()) {
			   String key = (String)keys.next();
			   Object obj = values.get(key);
			   if( obj instanceof JSONObject && code == null){
				   String code_ = getDocCode((JSONObject)obj, primaryID);
				   code = "".equals(code_) ? null : code_;
			   }else if( obj instanceof String){
				   if(primaryID.equals(key)){
					   String code_ = (String)obj;
					   code = "".equals(code_) ? null : code_;
				   }
			   }
			}
		return code;
	}
	
	public Object getKey(JSONObject values, String primaryID){
		Iterator<?> keys = values.keys();
		Object value = null;
		while( keys.hasNext()) {
			   String key = (String)keys.next();
			   Object obj = values.get(key);
			   if(key.equalsIgnoreCase(primaryID)){
				   value = obj;
			   }else{
				   if( obj instanceof JSONObject && value == null){
					   value = getKey((JSONObject)obj, primaryID);
				   }
			   }
			}
		return value;
	}
	
	
	
	/*
	pust result from front-end, filter useless info.
	itemTemplate - JSONObject, product template
	value - input result
	*/
	public Object getValue(JSONObject itemTemplate, Object value){
		JSONObject valueObj = (JSONObject)value;
		Iterator<?> keys = valueObj.keys();
		while( keys.hasNext() ) {
			   String key = (String)keys.next();
			   JSONObject template = getTemplate(itemTemplate, key);
			   String ss = template != null ? template.toString() : "nth";
			   Log.error("template: " + ss);
			   if(template != null)
				   valueObj.put(key, getItemValue(template, valueObj.get(key)));
		}
		return valueObj;
	}
	
	public JSONObject getTemplate(JSONObject template, String keyId){
		JSONObject item = null;
		if(template.has("id") && keyId.equals(template.getString("id"))){
			item = template;
		}else{
			   if(template.has("items")){
				   JSONArray items = template.getJSONArray("items");
				   int size = items.length();
				   for(int i = 0; i < size; i++){
					   JSONObject item_ = getTemplate(items.getJSONObject(i), keyId);
					   if(item_ != null)
						   item = item_;
				   }
			   }
		   }
		return item;
	}
	public Object getItemValue(JSONObject itemTemplate, Object value){
		Log.error("getItemValue_: " + itemTemplate.toString());
		String type = itemTemplate.has("type") ? itemTemplate.getString("type") : "";
		
		switch(type){
			case "productConfig":		
				JSONObject result = new JSONObject();
				JSONArray items = itemTemplate.getJSONArray("items");
				JSONObject valueObj = (JSONObject)value;
				for(int i = 0; i < items.length(); i++){
					JSONObject item = items.getJSONObject(i);
					String sectionID = item.getString("id");
					if(valueObj.has(sectionID))
						result.put(sectionID, getItemValue(item, valueObj.getJSONObject(sectionID)));
				}
				return result;
				
			case "sectionGroup":
				JSONObject sgResult = new JSONObject();
				JSONArray sgItems = itemTemplate.getJSONArray("items");
				JSONObject srValueObj = (JSONObject)value;
				for(int i = 0; i < sgItems.length(); i++){
					JSONObject srItem = sgItems.getJSONObject(i);
					JSONObject srItemValue = srValueObj.getJSONObject(srItem.getString("id")); 
					sgResult.put(srItem.getString("id"), getItemValue(srItem, srItemValue));
				}
				return sgResult;
				
			case "section":
			case "tabs":
				JSONObject sectionResult = new JSONObject();
				JSONArray sectionItems = itemTemplate.getJSONArray("items");
				for(int i = 0; i < sectionItems.length(); i++){
					JSONObject sectionItem = sectionItems.getJSONObject(i);
					if(sectionItem.has("id")){
						String sectionItemID = sectionItem.getString("id");
						JSONObject sectionValue = (JSONObject)value;
						if(sectionValue.has(sectionItemID)){
							Object sectionItemValue = sectionValue.get(sectionItemID);
							sectionResult.put(sectionItemID, getItemValue(sectionItem, sectionItemValue));
						}
					}else{
						if(allowNoID(sectionItem.getString("type"))){
							JSONObject mergeJSON = (JSONObject)getItemValue(sectionItem, value);
							Iterator  mergeJSONKeys = mergeJSON.keys();
							while (mergeJSONKeys.hasNext()) {
						        String key = (String)mergeJSONKeys.next();
						        sectionResult.put(key, mergeJSON.get(key));
						    }
							
						}
					}
				}
				return sectionResult;
				
			case "complexList":
				JSONArray complexResult = new JSONArray();
				//JSONArray complexItems = itemTemplate.getJSONArray("items");
				JSONArray complexValue = (JSONArray)value;
				int complexValueSize = complexValue.length();
				JSONObject complexTemplate = new JSONObject(itemTemplate.toString());
				complexTemplate.put("type", "section");
				for(int j = 0; j < complexValueSize; j++){
					JSONObject complexItemValue = complexValue.getJSONObject(j);
					complexResult.put(getItemValue(complexTemplate, complexItemValue));
				}
				return complexResult;
				
			case "hbox":
				JSONObject hbResult = new JSONObject();
				JSONArray hbItems = itemTemplate.getJSONArray("items");
				for(int i = 0; i < hbItems.length(); i++){
					JSONObject hbItem = hbItems.getJSONObject(i);
					if(hbItem.has("id")){
						String sectionItemID = hbItem.getString("id");
						JSONObject sectionValue = (JSONObject)value;
						if(sectionValue.has(sectionItemID)){
							Object hbItemValue = sectionValue.get(sectionItemID);
							hbResult.put(sectionItemID, getItemValue(hbItem, hbItemValue));
						}
					}
				}
				return hbResult;
				
			case "detailsList":
				JSONObject detailsResult = new JSONObject();
				//JSONArray complexItems = itemTemplate.getJSONArray("items");
				JSONObject detailsValue = (JSONObject)value;
				JSONObject detailsTemplate = new JSONObject(itemTemplate.toString());
				detailsTemplate.put("type", "section");
				
				Iterator<?> keys = detailsValue.keys();

				while( keys.hasNext() ) {
				    String key = (String)keys.next();
				    if ( detailsValue.get(key) instanceof JSONObject ) {
				    	detailsResult.put(key, getItemValue(detailsTemplate, detailsValue.getJSONObject(key)));
				    }
				}			
				return detailsResult;
			
			default:
				if(itemTemplate.has("id") && itemTemplate.has("type") && !itemTemplate.getString("type").equals("readOnly")){
					return value;
				}
		}
		
		
		return null;
	}
	
	private boolean allowNoID(String type){
		if(type.equals("hbox"))
			return true;
		return false;
	}
	
}
