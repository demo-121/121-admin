package com.eab.biz.dynTemplate;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.Function2;
import com.eab.common.Token;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Response;
import com.eab.webservice.dynamic.DynamicFunction;

public class AttachmentMgr extends SaveMgr{
	private String PacketKey = "DTSaveAttachmentTemp";
	public AttachmentMgr(HttpServletRequest request) {
		super(request);
	}
	
	public Response remove(){
		Response resp = null;
		DBManager dbm = null;
		Connection conn = null;
		boolean success = false;
		try{
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			JSONObject paraJson = DynamicFunction.getParap0(request);
			String module = request.getParameter("p1");
			String docCode = paraJson.getString("docCode");
			String attId = paraJson.getString("attId");
			int version = paraJson.getInt("version");
			
			success = dataDao.removeAttachments(dbm, conn, module, docCode, version, attId);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(dbm != null){
				dbm.clear();
				dbm = null;
			}
			try {
				if(conn != null && !conn.isClosed()){
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		Dialog dialog = null;
		if(!success){
			Field negative = new Field("ok", "button", "BUTTON.OK");
			dialog = new Dialog("saveAttDialog", "ATTACHMENT.SAVE.FAIL", "RECORD.SAVE.FAIL.MSG", null, negative);	
			dialog.translate(request);
		}

		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		resp = new Response(Constants.ActionTypes.SHOW_DIALOG, tokenID, tokenKey, dialog);
		return resp;
	}
	public String saveAttchment(Connection conn, DBManager dbm, String file, String module, String docCode, String attId, int version, int taskId){
		boolean success = false;
		try{
			HashMap<String,Object> condMap = new HashMap<String,Object>();
			
			condMap.put("comp_code", principal.getCompCode());
			condMap.put("version", version);
			condMap.put("module", module);
			condMap.put("doc_code", docCode);
			condMap.put("att_id", attId);
	 
			boolean isExist = Function2.hasRecordByType("DYN_ATTACHMENT_MST", condMap, null, null);
			if(isExist){
				//update
				success = dataDao.updateAttachment(principal.getCompCode(), module, docCode, version, attId, file, dbm, conn);
			}else{
				//insert 
				success = dataDao.insertAttachment(taskId, principal.getCompCode(), module, docCode, version, attId, file, dbm, conn);
			}
			
		}catch(Exception e){
			success = false;
		}
		return success ? null : "ERROR";
	}
	public Response getAttachment() {
		
		Response resp = null;
		DBManager dbm = null;
		Connection conn = null;
		
		try{
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String para = request.getParameter("p0");		
			byte[] decoded = Base64.decodeBase64(para.replace(" ", "+"));
			String paraStr = new String(decoded, StandardCharsets.UTF_8);	
			JSONObject paraJson = new JSONObject(paraStr);
			String module = paraJson.getString("module");
			String docCode = paraJson.getString("docCode");
			int version = paraJson.getInt("version");
			String attId = paraJson.getString("attId");
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("comp_code", principal.getCompCode());
			map.put("module", module);
			map.put("doc_code", docCode);
			map.put("version", version);
			map.put("att_id", attId);
			String file = dataDao.getAttachment(map, dbm, conn);
			resp = new Response("FILE_RETRIEVED", null, null);
			resp.setAttachment(file);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				conn.close();
				dbm.clear();
				dbm = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resp;
	}
}
