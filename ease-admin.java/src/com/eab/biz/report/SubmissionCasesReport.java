package com.eab.biz.report;

import com.eab.common.EnvVariable; 
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.eab.dao.report.Report;
import com.eab.dao.report.SubmissCasesReport;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.couchbase.CBServer;

import static com.eab.common.Function.jsonObject2Object;
import static com.eab.common.Function.jsonObject2String;

public class SubmissionCasesReport { 
	
	public String genSubmissionCaseReport(String generatedBy, String startDate, String endDate, String polNo, String fcCode, String piName, String encryptPassword ) throws ParseException{

		String gatewayURL = EnvVariable.get("GATEWAY_URL");
        String gatewayPort = EnvVariable.get("GATEWAY_PORT");
        String 	gatewayDBName = EnvVariable.get("GATEWAY_DBNAME");
        String gatewayUser = EnvVariable.get("GATEWAY_USER");
        String gatewayPW = EnvVariable.get("GATEWAY_PW");
		CBServer cb = new CBServer(gatewayURL,gatewayPort,gatewayDBName,gatewayUser,gatewayPW);

		ArrayList<String> rlsPolNum = SubmissCasesReport.getRlsPolNum();
		ReportUtil reportUtil = new ReportUtil();

		String startTime  = Long.toString(ReportUtil.dateStr2Long(startDate, 0,0,0,0));
		String endTime  = Long.toString(ReportUtil.dateStr2Long(endDate, 23,59,59, 999000000));

		Log.debug("submiss searchStartTime:"+startTime + " searchEndTime:"+endTime);

	    ArrayList<String> appIds = new ArrayList<String>();
	    ArrayList<String> polIds = new ArrayList<String>();


			JSONObject subDateList = cb.getDoc("_design/main/_view/appWithSubmitDate?startkey=[\"01\","+startTime+"]&endkey=[\"01\","+endTime+"]&stale=false");
		JSONObject noSubDateList = cb.getDoc("_design/main/_view/appWithoutSubmitDate?key=[\"01\"]&stale=false");

		JSONObject appList = new JSONObject();

		JSONArray subDateRows = subDateList.getJSONArray("rows");
		JSONArray noSubDateRows = noSubDateList.getJSONArray("rows");

		JSONArray combineList = new JSONArray();
		if(subDateRows != null) {
			for (int i = 0; i < subDateRows.length(); i++) {
				combineList.put(subDateRows.get(i));
			}
		}
		if(noSubDateRows != null) {
			for (int i = 0; i < noSubDateRows.length(); i++) {
				combineList.put(noSubDateRows.get(i));
			}
		}
		appList.put("rows" ,combineList);
        JSONArray result = appList!=null?appList.getJSONArray("rows"):new JSONArray();
		ArrayList<String> appAgentCodes = new ArrayList<String>();
        for (int j = 0; j < result.length(); j++) {
	          JSONObject row = result.getJSONObject(j).getJSONObject("value");
	          if(row.has("id"))
	          	appIds.add(row.getString("id"));
			  if(row.has("parentId"))
				appIds.add(row.getString("parentId"));
	          if(row.has("policyNumber") && row.get("policyNumber") instanceof String)
	          	polIds.add(row.getString("policyNumber"));
			  Object agentCode = jsonObject2Object(row, "agentCode");
			  if(agentCode!=null){
			  	appAgentCodes.add((String)agentCode);
			  }
		}


	    JSONObject channels = cb.getDoc("channels");
	    JSONObject paymentTmpl = cb.getDoc("paymentTmpl");
		JSONArray appAgents = cb.getDocsByBatch(cb, "agentDetails","[\"01\",\"agentCode\",\"", appAgentCodes, "\"]", "false");


		JSONArray bundleAppStatus =  cb.getDocsByBatch(cb, "bundleApp","[\"01\",\"application\",\"", appIds, "\"]", "false");
	    JSONArray policys = cb.getDocsByBatch(cb, "approvalApp","[\"01\",\"", polIds, "\"]", "false");
	    List<Object[]> resultList = new ArrayList<Object[]>();
	    if(null != appList){
        	JSONArray appListRows = (JSONArray)appList.get("rows");
            JSONObject agentHierarchys  = Report.getAgentHierarchys();
	        
            for(int i = 0 ; i < appListRows.length(); i++){
            	JSONObject appListRow = (JSONObject) appListRows.get(i);

            	String policyNumber = "";
            	String covNameEn = "";
            	String dealerGroup = "";
            	String agentCode = "";
            	String agentName = "";
            	String fsm = "";
            	String fsd = "";
            	String policyCcy = "";
            	String paymentMode = "";
            	String proposerFullName = "";
            	String proposerLastName = "";
            	String proposerFirstName = "";
            	String proposerOthName = "";
            	String proposerHanyuPinyinName = "";
            	String insuredFullName = "";
            	String insuredLastName = "";
            	String insuredFirstName = "";
            	String insuredOthName = "";
            	String insuredHanyuPinyinName = "";
            	Boolean isCrossAge = null;
            	String strIsCrossAge = "";
            	String pUndischargedBankrupt = "";
            	String iUndischargedBankrupt = "";
            	String trustedIndividualsFlag = "";
            	String comment = "";
            	String trxStatus = "-";
            	String payorFullName = "";
            	String payorSurname = "";
            	String payorGivenName = "";
            	String payorOtherName = "";
            	String payorPinYinName = "";

            	Object sumInsuredObj = null;
            	Object premiumObj = null;

            	String appId = (String) appListRow.get("id");
            	JSONObject application = appListRow.getJSONObject("value");

    			if(null != application){

    				policyNumber =  application.has("policyNumber") && application.get("policyNumber") instanceof String ?(String) application.get("policyNumber") : "";
					String parentId=  application.has("parentId") && application.get("parentId") instanceof String ?(String) application.get("parentId") : null;
    				String paymentMethod = null;
    				JSONObject basicPlan = null;
    				String sameAs  = "N";

					premiumObj = jsonObject2Object(application ,"premium") ;
					JSONArray plans =(JSONArray) jsonObject2Object(application ,"plans");
					basicPlan = (null != plans) ? (JSONObject) plans.get(0):null;

					policyCcy = jsonObject2String(application ,"ccy") ;
					sameAs = jsonObject2String(application ,"sameAs") ;

					paymentMode = jsonObject2String(application ,"paymentMode");
					paymentMode = reportUtil.mapPayMode(paymentMode);

    				//SEARCH-agent info
					dealerGroup = jsonObject2String(application ,"channel") ;
					agentCode = jsonObject2String(application ,"agentCode") ;
					agentName = jsonObject2String(application ,"agentName") ;

    				//SEARCH-proposer info
					trustedIndividualsFlag = application.has("pTrustedIndividuals")  && jsonObject2String(application, "pTrustedIndividuals.fullName") !="" ?"Y":"N";
					proposerFullName = jsonObject2String(application ,"pFullName") ;
					proposerLastName =  jsonObject2String(application ,"pLastName") ;
					proposerFirstName = jsonObject2String(application ,"pFirstName") ;
					proposerOthName =  jsonObject2String(application ,"pOthName") ;
					proposerHanyuPinyinName = jsonObject2String(application ,"pHanyuPinyinName") ;

    				//SEARCH-LA info
    				if(jsonObject2String(application ,"iFullName") == ""){
						insuredFullName = proposerFullName;
						insuredLastName = proposerLastName;
						insuredFirstName = proposerFirstName;
						insuredOthName = proposerOthName;
						insuredHanyuPinyinName = proposerHanyuPinyinName;
    				}else{
						insuredFullName = jsonObject2String(application ,"iFullName") ;
						insuredLastName = jsonObject2String(application ,"iLastName") ;
						insuredFirstName = jsonObject2String(application ,"iFirstName") ;
						insuredOthName = jsonObject2String(application ,"iOthName") ;
						insuredHanyuPinyinName = jsonObject2String(application ,"iHanyuPinyinName") ;
    				}

    				//search filtering
    				boolean matchPolNo =  polNo.isEmpty() || (!polNo.isEmpty() && polNo.equals(policyNumber));
    				boolean matchFcCode =  fcCode.isEmpty() || (!fcCode.isEmpty() && fcCode.equals(agentCode));
    				boolean matchPiName = false;
    				if(piName ==null || piName.isEmpty())
    					matchPiName = true;
    				else{
    					if(piName !=null && !piName.isEmpty()){
    						if(proposerFullName !=null && !proposerFullName.isEmpty() && proposerFullName.toLowerCase().contains(piName.toLowerCase())||
    								insuredFullName !=null && !insuredFullName.isEmpty() && insuredFullName.toLowerCase().contains(piName.toLowerCase()))
    							matchPiName=true;

        				}
    				}
    				if(!(matchPolNo && matchFcCode && matchPiName))
	    				 continue;

					paymentMethod =  jsonObject2String(application ,"initPayMethod") ;


    				if(paymentMethod !=null && ! paymentMethod.isEmpty() && ! paymentMethod.equals("")){
    					if(paymentMethod.equals(Constants.PaymentMethod.CREDIT_CARD) || paymentMethod.equals(Constants.PaymentMethod.ENETS) || paymentMethod.equals(Constants.PaymentMethod.IPP)){
							trxStatus = reportUtil.mapTrxStatus(jsonObject2String(application ,"trxStatus"));
    					}
    				}

    				//dealerGroup
    				if(null != channels && !"".equals(dealerGroup)){
    					JSONObject channelsObj = channels.has("channels") && channels.get("channels") instanceof JSONObject? (JSONObject)channels.get("channels"):null;
    					JSONObject channel = (null != channelsObj) && channelsObj.has(dealerGroup) && channelsObj.get(dealerGroup) instanceof JSONObject? (JSONObject)channelsObj.get(dealerGroup) : null;
    					dealerGroup = (null != channel) && channel.has("title") && channel.get("title") instanceof String? (String)channel.get("title") : "";
    				}
    				if(null != agentCode && !"".equals(agentCode)){
						JSONObject agentProfile = reportUtil.getValue(appAgents, "agentCode", agentCode );
						if(agentProfile != null && agentProfile.has("profileId")) {
							String profileId = jsonObject2String(agentProfile, 	"profileId");
							if (agentHierarchys != null && agentHierarchys.has(profileId)) {
								JSONObject agentUpline = ReportUtil.getAgentUpline(profileId, agentCode, agentHierarchys);
								if(agentUpline != null ) {
									if(agentUpline.has("manager"))
										fsm = agentUpline.getString("manager");
									if(agentUpline.has("director"))
										fsd = agentUpline.getString("director");
								}
							}
						}
    				}

					pUndischargedBankrupt = jsonObject2String(application ,"pUndischargedBankrupt") ;
					payorSurname = jsonObject2String(application ,"payorSurname") ;
					payorGivenName = jsonObject2String(application ,"payorGivenName") ;
					payorOtherName = jsonObject2String(application ,"payorOtherName") ;
					payorPinYinName = jsonObject2String(application ,"payorPinYinName") ;
					payorFullName = payorSurname+" "+payorGivenName;

    				if(null != basicPlan){
    					JSONObject covName = basicPlan.has("covName") && basicPlan.get("covName") instanceof JSONObject? (JSONObject)basicPlan.get("covName") : null;
    					covNameEn = (null != covName) && covName.has("en") && covName.get("en") instanceof String? (String)covName.get("en") : "";
    					sumInsuredObj = basicPlan.has("sumInsured") ? basicPlan.get("sumInsured") : null;

    				}

					iUndischargedBankrupt =  jsonObject2String(application ,"iUndischargedBankrupt") ;

    				String strAppSubDate = "";
    				String applicationSubmittedDateObj = application.has("applicationSubmittedDate") && application.get("applicationSubmittedDate") instanceof String ?application.getString("applicationSubmittedDate"): null;
    				if(null != applicationSubmittedDateObj && !applicationSubmittedDateObj.isEmpty()){
    		              Calendar cal = Function.convertISO8601(applicationSubmittedDateObj); 
    		              Log.debug("submiss app Sub 1:"+ Function.cal2StrFull(cal));
     					  Function.changeTimezone(cal, Constants.TIMEZONE_ID); 
    		              strAppSubDate = Function.cal2Str(cal);
    		              Log.debug("submiss app Sub 2:"+ Function.cal2StrFull(cal));
    				}
    				
    				
    				String appStatus= reportUtil.getValueString(bundleAppStatus, "applicationDocId", "appStatus", parentId==null?appId:parentId);

					comment = reportUtil.getStatusOfCase(appStatus, policys, policyNumber);
    				 
    				Object[] record = new Object[37];
    				int num = 0;
    				isCrossAge = application.has("isCrossAge") && application.get("isCrossAge") instanceof Boolean ?(Boolean) application.get("isCrossAge") : null;
    				if(isCrossAge != null)
    					strIsCrossAge = isCrossAge? "Y" :"N";
    				String strIsBackDate = application.has("isBackDate") && application.get("isBackDate") instanceof String ? (String) application.get("isBackDate") : null;
    				 
    				
    				record[num++] = policyNumber;
    				record[num++] = covNameEn;
    				record[num++] = dealerGroup;
    				record[num++] = agentCode;
    				record[num++] = agentName;
    				record[num++] = fsm;
    				record[num++] = fsd;
    				record[num++] = policyCcy;
    				record[num++] = sumInsuredObj;
    				record[num++] = premiumObj;
    				record[num++] = paymentMode;
    				record[num++] = paymentMethod.equals("-")?paymentMethod:ReportUtil.getPaymentMethodName(paymentTmpl,paymentMethod);
    				record[num++] = strAppSubDate == ""? "":strAppSubDate.substring(0, strAppSubDate.indexOf(' '));
    				record[num++] = strAppSubDate == ""? "":strAppSubDate.split(" ")[1];
    				record[num++] = proposerFullName;
    				record[num++] = proposerLastName;
    				record[num++] = proposerFirstName;
    				record[num++] = proposerOthName;
    				record[num++] = proposerHanyuPinyinName;
    				record[num++] = insuredFullName;
    				record[num++] = insuredLastName;
    				record[num++] = insuredFirstName;
    				record[num++] = insuredOthName;
    				record[num++] = insuredHanyuPinyinName;
    				record[num++] = strIsCrossAge; 
    				record[num++] = strIsBackDate;
    				record[num++] = pUndischargedBankrupt;
    				record[num++] = iUndischargedBankrupt;
    				record[num++] = trustedIndividualsFlag;
    				record[num++] = comment;
    				record[num++] = trxStatus;
    				record[num++] = payorFullName;
    				record[num++] = payorSurname;
    				record[num++] = payorGivenName;
    				record[num++] = payorOtherName; 
    				record[num++] = payorPinYinName;
    				record[num++] = (rlsPolNum.indexOf(policyNumber) > -1)? "Yes" : "No";

    				resultList.add(record);
    			}
            }
            
            Collections.sort(resultList, new Comparator<Object[]>(){
                @Override
                public int compare(Object[] objArray1, Object[] objArray2){
                	String lastString = "\uffff";//last char
                	String fsd1 = objArray1[6]!=null ?objArray1[6].toString():lastString;
                	String fsm1 = objArray1[5]!=null? objArray1[5].toString():lastString;
                	String agent1 = objArray1[4]!=null?objArray1[4].toString():lastString;
                	String fsd2 = objArray2[6]!=null ?objArray2[6].toString():lastString;
                	String fsm2 = objArray2[5]!=null? objArray2[5].toString():lastString;
                	String agent2 = objArray2[4]!=null ?objArray2[4].toString():lastString;
                	
                	int ret = 0;
                	int a = fsd1.toLowerCase().compareTo(fsd2.toLowerCase());
                	
                	if(a != 0){
                		ret = a>0?3:-1;
                	}else{
                		a = fsm1.toLowerCase().compareTo(fsm2.toLowerCase());
                		if(a != 0){
                			ret = a>0?2:-2;
                		}else{
                			a = agent1.toLowerCase().compareTo(agent2.toLowerCase());
                			if(a != 0){
                				ret = a>0?1:-3; 
                			}
                		}
                	}
                	return ret;
                }
            });
            
           
        }
	    Object[][] records = new Object[resultList.size()][];
        for(int i=0;i<resultList.size();i++){
        	records[i] = resultList.get(i);
        }
        
        Calendar cal = Calendar.getInstance(); 
        Function.changeTimezone(cal, Constants.TIMEZONE_ID);             
        String genDate = Function.cal2Str(cal);
			return ExcelUtil.createSubmissionCaseReport(records,generatedBy, genDate, encryptPassword);
	}
	
		
	}
