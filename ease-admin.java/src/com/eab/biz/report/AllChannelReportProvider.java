package com.eab.biz.report;

import com.eab.common.Log;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static com.eab.biz.report.ExcelUtil.*;
import static org.apache.poi.hssf.util.HSSFColor.WHITE.index;
import static org.apache.poi.ss.usermodel.FillPatternType.SOLID_FOREGROUND;
import static org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER;
import static org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT;
import static org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT;

public class AllChannelReportProvider {

    public static String createAllChannelReport(Object[][] dataSet, String generatedBy, String genDate, String password){

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("All Channel Report");

        int rowNum = 0;
        Object[][] subHeader = {{
                "", "", "","", "", "", "", "","", "", "","", "", "","", "", "","", "", ""
                ,"Proxy Period", "", "","", "", "","","", "","","", ""
                ,"CKA/Risk Profile", ""
                ,"Roadshow details", ""
                ,"Joint field work", ""
                ,"Selected client", "", "","","", "","","", ""
                ,"CDA cases", "", ""
                ,"ROP"

        }};

        Object[][] tableHeader = {{
                "Case Status",
                "Case No./Policy No.",
                "Case creation date",
                "Agent Case Submission date",
                "Supervisor approval date",
                "Firm approval date",
                "Proposer Document Type",
                "Proposer IC",
                "Proposer Name",
                "Life Assured Document Type",
                "Life Assured IC",
                "Life Assured Name",
                "Proposer Mobile No.",
                "Proposer Email address",
                "Agent Name",
                "Manager Name",
                "Director Name",
                "Case Approved by ",
                "Firm Name",
                "Channel",
                "Proxy Start Date",
                "Proxy End Date",
                "Product Name",
                "Premium amount (Basic plan)",
                "Sum assured (Basic Plan)",
                "Premium Type(RP/SP)",
                "Premium Frequency",
                "Top-up",
                "RSP",
                "RSP frequency",
                "Payment Method",
                "Currency",
                "CKA Pass/Fail",
                "Risk Profile",
                "Date of roadshow",
                "Venue",
                "Joint field work case? (Yes/No)",
                "Purpose of Joint Field work",
                "DOB",
                "Education level",
                "Language",
                "Trusted Individual name",
                "Trusted Individual Mobile No.",
                "Date of call",
                "Person contacted ",
                "Mobile No.",
                "Comments enetred by supervsior",
                "Approved/Reject status by CDA",
                "Comments entered by CDA",
                "Case approval date by CDA",
                "ROP (Yes/No)",
                "Rider Name 1",
                "Premium amount (Rider 1)",
                "Sum assured (Rider 1)",
                "Rider Name 2",
                "Premium amount (Rider 2)",
                "Sum assured (Rider 2)",
                "Rider Name 3",
                "Premium amount (Rider 3)",
                "Sum assured (Rider 3)",
                "Rider Name 4",
                "Premium amount (Rider 4)",
                "Sum assured (Rider 4)",
                "Rider Name 5",
                "Premium amount (Rider 5)",
                "Sum assured (Rider 5)",
                "Rider Name 6",
                "Premium amount (Rider 6)",
                "Sum assured (Rider 6)",
                "Rider Name 7",
                "Premium amount (Rider 7)",
                "Sum assured (Rider 7)",
                "Rider Name 8",
                "Premium amount (Rider 8)",
                "Sum assured (Rider 8)",
                "Rider Name 9",
                "Premium amount (Rider 9)",
                "Sum assured (Rider 9)",
                "Rider Name 10",
                "Premium amount (Rider 10)",
                "Sum assured (Rider 10)",
                "Rider Name 11",
                "Premium amount (Rider 11)",
                "Sum assured (Rider 11)",
                "Rider Name 12",
                "Premium amount (Rider 12)",
                "Sum assured (Rider 12)",
        }};

        HorizontalAlignment hAlignCenter = CENTER;
        HorizontalAlignment hAlignLeft = LEFT;
        VerticalAlignment vAlignBottom = VerticalAlignment.BOTTOM;
        FillPatternType fpPatternSolid  = SOLID_FOREGROUND;

        XSSFFont boldFont = workbook.createFont();
        boldFont.setFontHeightInPoints((short) 10);
        boldFont.setBold(true);

        XSSFFont normalFont = workbook.createFont();
        normalFont.setFontHeightInPoints((short) 10);
        normalFont.setBold(false);

        CellStyle whiteStyle = workbook.createCellStyle();
        whiteStyle.setFillForegroundColor(index);
        whiteStyle.setFillPattern(SOLID_FOREGROUND);
        whiteStyle.setAlignment(LEFT);
        whiteStyle.setVerticalAlignment(VerticalAlignment.BOTTOM);
        whiteStyle.setFont(boldFont);
        whiteStyle.setWrapText(true);
        setBlackBorder(whiteStyle);


        CellStyle dataStyle = workbook.createCellStyle();
        dataStyle.setAlignment(LEFT);
        dataStyle.setVerticalAlignment(VerticalAlignment.BOTTOM);
        dataStyle.setFont(normalFont);

        CellStyle tableHeaderCener = workbook.createCellStyle();
        tableHeaderCener.setFillForegroundColor(index);
        tableHeaderCener.setFillPattern(SOLID_FOREGROUND);
        tableHeaderCener.setAlignment(CENTER);
        tableHeaderCener.setWrapText(true);
        tableHeaderCener.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
        tableHeaderCener.setFont(boldFont);
        setDefaultBorder(tableHeaderCener);

        //Dollar format
        CellStyle moneyStyle = workbook.createCellStyle();
        moneyStyle.setDataFormat((short)4);
        moneyStyle.setFont(normalFont);
        setHCenter(moneyStyle);
        moneyStyle.setAlignment(RIGHT);
        //Datetime format
        CellStyle dateTimeStyle = workbook.createCellStyle();
        short dtf = workbook.createDataFormat().getFormat("YYYY-MM-DD HH:MM");
        dateTimeStyle.setDataFormat(dtf);
        dateTimeStyle.setFont(normalFont);
        setHCenter(dateTimeStyle);
        //number Style
        CellStyle numStyle = workbook.createCellStyle();
        numStyle.setFont(normalFont);
        setHCenter(numStyle);

        setColWidth( sheet ,tableHeader[0].length, null, 7000);

        //subHeader
        setText( sheet , subHeader, null,rowNum, 0) ;
        //merge cells
        sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum,20,21));
        sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum,32,33));
        sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum,34,35));
        sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum,36,37));
        sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum,38,46));
        sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum,47,49));
        rowNum += subHeader.length;

        //header
        setText( sheet , tableHeader, whiteStyle,rowNum, 0) ;
        //color cells
        for(int i = 0; i< 7 ; i++){
            java.awt.Color color = new java.awt.Color(255,255,255);
            int [] cols = new int[]{};
            switch(i){
                case 0:
                    cols = new int[]{20,21};
                    color = new java.awt.Color(244,176,132);
                    break;
                case 1:
                    cols = new int[]{32,33};
                    color = new java.awt.Color(146,208,80);
                    break;
                case 2:
                    cols = new int[]{34,35};
                    color = new java.awt.Color(226,239,218);
                    break;
                case 3:
                    cols = new int[]{36,37};
                    color = new java.awt.Color(172,185,202);
                    break;
                case 4:
                    cols = new int[]{38,39, 40, 41, 42, 43, 44, 45, 46};
                    color = new java.awt.Color(255,230,153);
                    break;
                case 5:
                    cols = new int[]{47,48,49};
                    color = new java.awt.Color(201,201,201);
                    break;
                case 6:
                    cols = new int[]{50};
                    color = new java.awt.Color(244,176,132);
                    break;
            }
            for(int j =0; j<2;j++){
                CellStyle subHeaderStyle = workbook.createCellStyle();
                ((XSSFCellStyle) subHeaderStyle).setFillForegroundColor(new XSSFColor(color));
                subHeaderStyle.setFillPattern(fpPatternSolid);
                subHeaderStyle.setVerticalAlignment(vAlignBottom);
                if(j==0){
                    subHeaderStyle.setAlignment(hAlignCenter);
                    subHeaderStyle.setFont(normalFont);
                }else{
                    subHeaderStyle.setAlignment(hAlignLeft);
                    subHeaderStyle.setFont(boldFont);
                    setBlackBorder(subHeaderStyle);
                }

                setStyle(sheet,rowNum,0,tableHeader.length,tableHeader[0].length,new int[]{j},cols ,subHeaderStyle);
            }

        }
        rowNum += tableHeader.length;

        //records
        setText( sheet , dataSet, dataStyle,rowNum, 0);
        int[] moneyArray = new int[24];
        int riderAmtCol = 52;
        int cnt = 0;
        for(int i = 0;i<12;i++){
            for(int j=0;j<2;j++){
                moneyArray[cnt] = riderAmtCol;
                cnt++;
                riderAmtCol++;
            }
            riderAmtCol++;
        }
        setStyle(sheet,rowNum,0,dataSet.length,tableHeader[0].length,null,moneyArray ,moneyStyle);
        setStyle(sheet,rowNum,0,dataSet.length,tableHeader[0].length,null,new int[]{23,24,28} ,moneyStyle);
        setStyle(sheet,rowNum,0,dataSet.length,tableHeader[0].length,null,new int[]{2,3,4,5, 20,21,34,38,43,48} ,dateTimeStyle );



        //return as base64 string
        return setExcelPassword(workbook, password);
    }
}
