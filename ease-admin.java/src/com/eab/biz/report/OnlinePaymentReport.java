package com.eab.biz.report;

 
 
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.couchbase.CBServer;
import com.eab.common.EnvVariable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.aop.interceptor.DebugInterceptor;

public class OnlinePaymentReport { 
	
	public String genOnlinePaymentExcel(String reportName,String generatedBy, String payStatus, String payMethod, String startDate, String endDate) throws ParseException{
		 String gatewayURL = EnvVariable.get("GATEWAY_URL");
         String gatewayPort = EnvVariable.get("GATEWAY_PORT");
         String gatewayDBName = EnvVariable.get("GATEWAY_DBNAME");
         String gatewayUser = EnvVariable.get("GATEWAY_USER");
         String gatewayPW = EnvVariable.get("GATEWAY_PW");
          CBServer cb = new CBServer(gatewayURL,gatewayPort,gatewayDBName,gatewayUser,gatewayPW);
          ReportUtil reportUtil = new ReportUtil();
          JSONArray result = new JSONArray(); 

			  String startTime  = Long.toString(ReportUtil.dateStr2Long(startDate, 0,0,0,0));
			  String endTime  = Long.toString(ReportUtil.dateStr2Long(endDate, 23,59,59, 999000000));

			  Log.debug("startTime:"+startTime + " endTime:"+endTime);
	      
	      
	      ArrayList<String> appIds = new ArrayList<String>();
	      ArrayList<String> polIds = new ArrayList<String>();
	      if(payMethod.equals("all")){
	    	  String[] ePays = new String[]{Constants.PaymentMethod.CREDIT_CARD, Constants.PaymentMethod.ENETS, Constants.PaymentMethod.IPP};
		      for(int i = 0; i< ePays.length;i++){
		    	  String ePay = ePays[i]; 
		    	  JSONObject onlinePayment = cb.getDoc("_design/main/_view/onlinePayment?startkey=[\"01\",\""+ePay+"\","+startTime+"]&endkey=[\"01\",\""+ePay+"\","+endTime+"]&stale=false");
		          if(onlinePayment!=null){
		        	  JSONArray rows = onlinePayment.getJSONArray("rows");
		        	  for (int j = 0; j < rows.length(); j++) {
			  	          result.put(rows.get(j));			  	          
			  	      }
		          }
		          
		      } 
	      }else{
	    	  JSONObject op = cb.getDoc("_design/main/_view/onlinePayment?startkey=[\"01\",\""+payMethod+"\","+startTime+"]&endkey=[\"01\",\""+payMethod+"\","+endTime+"]&stale=false");
	    	  result = op.getJSONArray("rows");
	      }	      
	      for (int j = 0; j < result.length(); j++) { 
  	          JSONObject row = result.getJSONObject(j).getJSONObject("value");
  	          if(row.has("id"))
  	          	appIds.add(row.getString("id"));
			  if(row.has("parentId"))
				  appIds.add(row.getString("parentId"));
  	          if(row.has("policyNumber"))
  	          	polIds.add(row.getString("policyNumber"));	  	          
  	      }
	      
	      JSONArray bundleAppStatus = cb.getDocsByBatch(cb, "bundleApp","[\"01\",\"application\",\"", appIds, "\"]", "false");
	      JSONArray approvalApp = cb.getDocsByBatch(cb, "approvalApp", "[\"01\",\"" , polIds, "\"]",  "false");


        	JSONArray appListRows = result;

        	List<Object[]> resultList = new ArrayList<Object[]>();
            int recCnt = 0;
            boolean isShield = false;
	        JSONObject paymentTmpl = cb.getDoc("paymentTmpl");
            for(int i = 0 ; i < appListRows.length(); i++){
            		JSONObject appListRow =   appListRows.getJSONObject(i).getJSONObject("value");
            		//Search filtering
            		String trxNo =  appListRow.has("trxNo") && appListRow.get("trxNo") instanceof String ?(String) appListRow.get("trxNo") : null;
    				double initTotalPrem =  appListRow.has("initTotalPrem") ? appListRow.getDouble("initTotalPrem") : -1;
    				double trxAmount =  appListRow.has("trxAmount") ? appListRow.getDouble("trxAmount") : -1;
    				String trxStatus =  appListRow.has("trxStatus") && appListRow.get("trxStatus") instanceof String ?(String) appListRow.get("trxStatus") : "";
//    				if(!isShield)
//    					trxAmount = initTotalPrem;
    				boolean recordStatus =false;
    				if(!trxNo.equals("") && trxStatus.equals("Y"))
    					recordStatus=true;    		 
    				if(recordStatus != payStatus.equals("success"))
    					continue;

    				
    				String appNo=  appListRow.has("id") && appListRow.get("id") instanceof String ?(String) appListRow.get("id") : "";

					String parentId=  appListRow.has("parentId") && appListRow.get("parentId") instanceof String ?(String) appListRow.get("parentId") : null;
    				Long trxTime =  appListRow.has("trxTime") ?(Long) appListRow.getLong("trxTime") : -1;
    				String policyNumber =  appListRow.has("policyNumber") && appListRow.get("policyNumber") instanceof String ?(String) appListRow.get("policyNumber") : "";
    				String trxCcy =   appListRow.has("ccy") && appListRow.get("ccy") instanceof String ?(String) appListRow.get("ccy") : "";    				
    				String paymentMethod =  appListRow.has("paymentMethod") && appListRow.get("paymentMethod") instanceof String ?(String) appListRow.get("paymentMethod") : "";    				
    				String fullName =  appListRow.has("proposerName") && appListRow.get("proposerName") instanceof String ?(String) appListRow.get("proposerName") : "";    				
    				String agentName =  appListRow.has("agentName") && appListRow.get("agentName") instanceof String ?(String) appListRow.get("agentName") : "";
    				String agentCompany =  appListRow.has("agentCompany") && appListRow.get("agentCompany") instanceof String ?(String) appListRow.get("agentCompany") : "";
    				String appStatus = reportUtil.getValueString(bundleAppStatus, "applicationDocId", "appStatus", parentId==null?appNo:parentId);
					 
    				String appSubDate = appListRow.has("applicationSubmittedDate") && appListRow.get("applicationSubmittedDate") instanceof String ?(String) appListRow.get("applicationSubmittedDate") : "";
    				String status = reportUtil.getStatusOfCase(appStatus, approvalApp, policyNumber);

            		if(null != paymentMethod && !"".equals(paymentMethod) && null != paymentTmpl){
            			paymentMethod = ReportUtil.getPaymentMethodName(paymentTmpl,paymentMethod);
            		}
            		
    				Object[] record = new Object[14];
    				int col = 0;    			    
    			    
    				record[col++] =  trxTime;
    				record[col++] = policyNumber;
    				record[col++] = trxCcy;
    				record[col++] = initTotalPrem<0?null:initTotalPrem;
    				record[col++] = trxAmount<0?null:trxAmount;
    				record[col++] = paymentMethod;
    				record[col++] = trxNo;
    				record[col++] = fullName;
    				record[col++] = status;
    				
    				//Date dtApproveRejectDate = null;
    				String submissionDate = "";
    				if(appSubDate!=null && !appSubDate.equals("")){    						
    						Calendar cal = Function.convertISO8601(appSubDate);
    						Log.debug("online appSubDate 1:"+ Function.cal2StrFull(cal)); 
    						Function.changeTimezone(cal, Constants.TIMEZONE_ID);
    						submissionDate= Function.cal2Str(cal);
    						Log.debug("online appSubDate 2:"+ Function.cal2StrFull(cal)); 

    				}   				
    				
    				record[col++] = submissionDate;
    				record[col++] = agentName;
    				record[col++] = agentCompany;

    				String	apiQueried =  appListRow.has("trxEnquiryStatus") && appListRow.get("trxEnquiryStatus") instanceof String ?(String) appListRow.get("trxEnquiryStatus") : null;
    				
    				record[col++] = payStatus.equals("success")?"":reportUtil.mapTrxStatus(trxStatus);
    				record[col++] = payStatus.equals("success")?"":apiQueried;
    				resultList.add(recCnt++, record);
    			
            }
            
            Collections.sort(resultList, new Comparator<Object[]>(){
                @Override
                public int compare(Object[] objArray1, Object[] objArray2){
                	boolean is1Empty = (null == objArray1[0]);
                	boolean is2Empty = (null == objArray2[0]);
                	
                	if(is1Empty && !is2Empty){
                		return 1;
                	}else if(!is1Empty && is2Empty){
                		return -1;
                	}else if(is1Empty && is2Empty){
                		return 0;
                	}
                	return ((Long)objArray2[0]).compareTo(((Long)objArray1[0]));
                }
            });
            
            Object[][] records = new Object[resultList.size()][];
            for(int i=0;i<resultList.size();i++){
            	Object[] obj = resultList.get(i);
            	if(null != obj[0]){
            		  Calendar cal = Function.convertISO8601((long)obj[0]);
		              Log.debug("online trxTime 1:"+ Function.cal2StrFull(cal)); 
                      Function.changeTimezone(cal, Constants.TIMEZONE_ID);  
                      obj[0] = Function.cal2Str(cal);
		              Log.debug("online trxTime 2:"+ Function.cal2StrFull(cal)); 
		            
            	}
            	
            	records[i] = resultList.get(i);
            }
            String headerStatus = " ("+(payStatus.equals("success")?"Successful":"Exception")+" Transaction)";
            
            Calendar cal = Calendar.getInstance(); 
            Function.changeTimezone(cal, Constants.TIMEZONE_ID);             
            String genDate = Function.cal2Str(cal);	
            
            return ExcelUtil.createOnlinePaymentReport(records,reportName+headerStatus,generatedBy, genDate);	
        
        
	}	 
	 
  
}

