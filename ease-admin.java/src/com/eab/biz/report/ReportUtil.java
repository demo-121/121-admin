package com.eab.biz.report;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

import com.eab.common.Constants;
import com.eab.common.Log;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONObject;

import static com.eab.common.Function.jsonObject2Object;

public class ReportUtil {
	
	public static String getPaymentMethodName(JSONObject paymentTmpl,String paymentMethod){	 
		JSONArray items = (JSONArray) paymentTmpl.get("items");
	    for(int j = 0 ; j < items.length(); j++ ){
	    	JSONObject item = (JSONObject)items.get(j);
	    	if( item.has("title") && item.get("title") instanceof String && item.getString("title").equals("Payment Methods")){
	    		JSONArray payMethodItems = (JSONArray)item.get("items");
	    		for(int k = 0 ; k < payMethodItems.length(); k++ ){
		    		JSONObject payMethodItem = (JSONObject) payMethodItems.get(k);
		    		if(payMethodItem.has("type") && payMethodItem.getString("type").equals("12Box")){
		    			JSONArray _12BoxItems = (JSONArray)payMethodItem.get("items");
		    			for(int l =0 ; l<_12BoxItems.length();l++){
		    				JSONObject _12BoxItem = _12BoxItems.getJSONObject(l) ;
		    				if(_12BoxItem.has("id") && _12BoxItem.get("id") instanceof String && _12BoxItem.getString("id").equals("initPayMethod")){
		    					JSONArray options  =  _12BoxItem.getJSONArray("options");
				    			for(int m = 0;m < options.length(); m++){
				    				JSONObject option = options.getJSONObject(m);
				    				if(option.has("value") && option.getString("value") instanceof String && option.getString("value").equals(paymentMethod)){
				    					return option.getString("title");
				    				}
				    			}
		    				}
		    				
		    			}
		    			
	    			}
	    		}
	    	} 
	    }
	    return "";
	}
	
	public String mapApprovalStatus(String approvealStatus){ 
		if(approvealStatus != null && !approvealStatus.isEmpty()){
			switch(approvealStatus){
				case "A":
					return "Supervisor Approved"; 
				case "R":
					return "Supervisor Rejected"; 
				case "E":
					return "Supervisor Expired"; 
				case "SUBMITTED":
				case "PDoc":
				case "PDis":
				case "PFAFA":
				case "PDocFAF":
				case "PDisFAF":
				case "PCdaA":
				case "PDocCda":					
				case "PDisCda":					
					return "Pending Supervisor Approval"; 
			}
		}
		return null;
	}
	
	public String mapAppStatus(String appStatus){ 
		if(appStatus != null && !appStatus.isEmpty()){
			switch(appStatus){
				case "APPLYING":
					return "Applying"; 
				case "INVALIDATED":
					return "Invalidated";
				case "INVALIDATED_SIGNED":
					return "Invalidated (Signed)";  
			}
		}
		return null;
	}

	public static String getValueString(JSONArray rows, String idKey, String returnKey, String id){
		for(int i = 0; i<rows.length();i++){
			JSONObject row = rows.getJSONObject(i);
			JSONObject value = row.getJSONObject("value");
			if(value.has(idKey)&&value.get(idKey)instanceof String && value.getString(idKey).equals(id)){
				if(value.has(returnKey)&&value.get(returnKey)instanceof String)
					return value.getString(returnKey);
			}			
		}		 
		return null;
	}

	public Object getValueItem(JSONArray rows, String idKey, String returnKey, String id ){
		for(int i = 0; i<rows.length();i++){
			JSONObject row = rows.getJSONObject(i);
			JSONObject value = row.getJSONObject("value");
			if(value.has(idKey)&&value.get(idKey)instanceof String && value.getString(idKey).equals(id)){
				if(value.has(returnKey))
					return value.get(returnKey);
			}
		}
		return null;
	}


	public static JSONObject getValue(JSONArray rows, String idKey, String id ){
		for(int i = 0; i<rows.length();i++){
			JSONObject row = rows.getJSONObject(i);
			JSONObject value = row.getJSONObject("value");
			if(value.has(idKey)&&value.get(idKey)instanceof String && value.getString(idKey).equals(id)){
				return value;
			}
		}
		return null;
	}


	public static String mapPayMode(String paymentMode){
		if(paymentMode != null && !paymentMode.isEmpty()){
			switch(paymentMode){
				case "A":
					return "Annual"; 
				case "S":
					return "Semi-Annual"; 
				case "Q":
					return "Quarterly"; 
				case "M":
					return "Monthly"; 
				case "L":
					return "Single Premium (SP)"; 
			}
		}
		return null;
	}
	
	
	public String mapTrxStatus(String trxStatus){
		if(trxStatus != null && !trxStatus.isEmpty()){
			switch(trxStatus){
				case "Y":
					return "Success"; 
				case "N":
					return "Failed"; 
				case "C":
					return "Cancelled"; 
				case "I":
					return "No Response"; 
				case "O":
					return "Status Not found"; 
			}
		}
		return null;
	}

	
	public static long dateStr2Long(String startDate, int hour, int min, int sec, int nanoSec){
       
		try {
			String format = startDate.indexOf('/')>-1 ? Constants.DATETIME_PATTERN_AS_ADMIN_DATE: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
			SimpleDateFormat sdfStart = new SimpleDateFormat(format);  
			Date dtStartDate = sdfStart.parse(startDate);
	       
	        Calendar cal = Calendar.getInstance();
	        cal.setTimeZone(TimeZone.getTimeZone(Constants.TIMEZONE_ID));
	        cal.setTime(dtStartDate);
	        int year = cal.get(Calendar.YEAR);
	        int month = cal.get(Calendar.MONTH)+1;
	        int day = cal.get(Calendar.DAY_OF_MONTH);
			 
			ZonedDateTime zdt1 = LocalDateTime.of(year, month,  day, hour, min, sec,nanoSec).atZone( ZoneId.of(Constants.TIMEZONE_ID));			
			return zdt1.toInstant().toEpochMilli(); 
			
		} catch (ParseException e) {
			Log.debug("Exception in dateStr2Long:" + e );
		}
		return -1;
 		  
	}

	public String getStatusOfCase(String appStatus, JSONArray policys, String policyNumber){
		String statusOfCase = "";
		if(appStatus != null ){
			if(appStatus.equals("SUBMITTED")) {
				String approvalStatus = getValueString(policys, "approvalCaseId", "approvalStatus", policyNumber);
				if(approvalStatus != null && !approvalStatus.equals("")){
					statusOfCase = mapApprovalStatus(approvalStatus);
				}
			}else{
				statusOfCase = mapAppStatus(appStatus);
			}
		}
		return statusOfCase;
	}

	public static JSONObject getAgentUpline(String profileId, String agentCode, JSONObject agentHierarchys) {
		JSONObject upline = new JSONObject();
		if (agentHierarchys != null && agentHierarchys.has(profileId)) {
			JSONObject agentHierarchy = agentHierarchys.getJSONObject(profileId);
			String _managerName = "";
			String _directorName = "";
			String upline_1_no = "";
			if (agentHierarchy != null) {
				if (agentHierarchy.has("upline_1_name"))
					_managerName = agentHierarchy.getString("upline_1_name");
				if (agentHierarchy.has("upline_2_name"))
					_directorName = agentHierarchy.getString("upline_2_name");

				upline_1_no = (agentHierarchy.has("upline_1_no")) ? upline_1_no = agentHierarchy.getString("upline_1_no") : null;
				String upline_2_no = (agentHierarchy.has("upline_2_no")) ? upline_2_no = agentHierarchy.getString("upline_2_no") : null;
				if (upline_2_no != null && upline_2_no.equals(upline_1_no))
					_directorName = "";
				if (upline_1_no != null && upline_1_no.equals(agentCode)) {
					_managerName = "";
					upline_1_no = "";
				}
			}
			upline.put("manager", _managerName);
			upline.put("director", _directorName);
			upline.put("managerCode", upline_1_no);
		}
		return upline;
	}

	public static JsonObject dateRangeValidation(Date startDate, Date endDate, Date maxDate){
		JsonObject error = new JsonObject();
		if(startDate.after(endDate)){
			error.addProperty("title", "Invalid Date");
			error.addProperty("message", "From Date cannot be later than To date");
		}else if(maxDate != null && endDate.after(maxDate)){
			error.addProperty("title", "Invalid Date");
			error.addProperty("message", "To Date cannot be later than today");
		}

		return error;
	}
}
