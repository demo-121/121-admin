package com.eab.biz.report;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.dao.report.AllChannelReportDao;
import com.eab.dao.report.Report;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static com.eab.common.Function.jsonObject2Object;
import static com.eab.common.Function.jsonObject2String;

public class AllChannelReportService {

    public static String handleMobileNumber(String mobileCountryCode, Object mobileNo){
        if(mobileNo != null && mobileCountryCode != null) {
            String concatMobileNo = "";
            if (mobileNo != null){
                if(mobileNo instanceof String)
                    concatMobileNo = (String)mobileNo;
                else if(mobileNo instanceof Double)
                    concatMobileNo = String.format ("%.0f", mobileNo);
                else if(mobileNo instanceof Integer)
                    concatMobileNo = Integer.toString ( (Integer)mobileNo);
            }
            if(!concatMobileNo.equals(""))
                concatMobileNo = mobileCountryCode + " " + concatMobileNo;
            return concatMobileNo;
        }
        return "";
    }


    public static Object[][] handleReportTimeFormat(Object[][] records){
        for(int i =0 ;i<records.length;i++){
            Object[] row = records[i];
            for(int j =0 ;j<row.length;j++){
                switch(j) {
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 34:
                    case 43:
                        if(records[i][j] instanceof  Double)
                            records[i][j] = utcTime2Local((long) (double)records[i][j]);
                        break;
                    default:
                        break;
                }

            }
        }
        return records;
    }

    public static String utcTime2Local(long datetime){
        Calendar cal = Function.convertISO8601(datetime);
        Function.changeTimezone(cal, Constants.TIMEZONE_ID);
        String result = Function.cal2StrDate(cal);
        return result;
    }


    public String mapApprovalStatusAllChannel(String approvealStatus){
        if(approvealStatus != null && !approvealStatus.isEmpty()){
            switch(approvealStatus){
                case "A":
                    return "Case approved";
                case "R":
                    return "Case rejected";
                case "E":
                    return "Case expired";
                case "SUBMITTED":
                    return "Pending Supervisor Approval";
                case "PDoc":
                    return "Pending Document";
                case "PDis":
                    return "Pending Discussion";
                case "PFAFA":
                    return "Pending FA firm approval";
                case "PDocFAF":
                    return "Pending Document (FA Firm)";
                case "PDisFAF":
                    return "Pending Discussion (FA Firm)";
                case "PCdaA":
                    return "Pending CDA approval";
                case "PDocCda":
                    return "Pending Document (CDA)";
                case "PDisCda":
                    return "Pending Discussion (CDA)";
                case "inProgressBI":
                    return "In Progress BI";
                case "inProgressApp":
                    return "In Progress Application";

            }
        }
        return null;
    }

    public String mapAppStatusAllChannel(String appStatus){
        if(appStatus != null && !appStatus.isEmpty()){
            switch(appStatus){
                case "APPLYING":
                    return "In progress e-application";
            }
        }
        return appStatus;
    }


    public String getStatusOfCaseAllChannel(String appStatus, JSONArray policys, String policyNumber){
        String statusOfCase = "";
        if(appStatus != null ){
            if(appStatus.equals("SUBMITTED")) {
                String approvalStatus = ReportUtil.getValueString(policys, "approvalCaseId", "approvalStatus", policyNumber);
                if(approvalStatus != null && !approvalStatus.equals("")){
                    statusOfCase = mapApprovalStatusAllChannel(approvalStatus);
                }
            }else{
                statusOfCase = mapAppStatusAllChannel(appStatus);
            }
        }
        return statusOfCase;
    }

    public static String getLanguage(String language){
        String returnLang = "";
        if(language != null) {
            String[] langs = language.split(",");
            for (int i = 0; i < langs.length; i++) {
                String lang = langs[i];
                switch (lang) {
                    case "en":
                        lang = "English";
                        break;
                    case "zh":
                        lang = "Mandarin";
                        break;
                    case "ms":
                        lang = "Malay";
                        break;
                    case "ta":
                        lang = "Tamil";
                        break;

                }
                returnLang = returnLang + lang + " ";
            }
            if (returnLang.length() > 1)
                returnLang = returnLang.substring(0, returnLang.length() - 1);
        }

        return returnLang;

    }


    public static String getEducationLevel(String educaton){
        if(educaton != null){
            switch(educaton){
                case "below":
                    return "Below GCE 'N' or 'O' Levels certificate";
                case "above":
                    return "GCE 'N’ or 'O' Levels and above certificate";
            }
        }
        return null;
    }



    public static String mapDocType(String docType){
        if(docType != null && !docType.isEmpty()){
            switch(docType){
                case "passport":
                    return "passport";
                case "nric":
                    return "NRIC";
                case "fin":
                    return "FIN No";

            }
        }
        return "";
    }

    public static String getChannelValue(JSONObject channels, String dealerGroup, String key){
        if(null != channels && !"".equals(dealerGroup)){
            JSONObject channelsObj = channels.has("channels") && channels.get("channels") instanceof JSONObject? (JSONObject)channels.get("channels"):null;
            JSONObject channel = (null != channelsObj) && channelsObj.has(dealerGroup) && channelsObj.get(dealerGroup) instanceof JSONObject? (JSONObject)channelsObj.get(dealerGroup) : null;
            return (null != channel) && channel.has(key) && channel.get(key) instanceof String? (String)channel.get(key) : "";
        }
        return null;
    }



    public static String getRiskProfile(JSONObject riskRating, int riskProfile){
        if(null != riskRating && !"".equals(riskProfile)){
            JSONArray options = riskRating.has("options") && riskRating.get("options") instanceof JSONArray? (JSONArray)riskRating.get("options"):null;
            for(int i=0; i<options.length();i++){
                if(options.get(i) != null && options.get(i) instanceof JSONObject){
                    JSONObject option = options.getJSONObject(i);
                    if(option.has("value") && option.get("value") instanceof Integer){
                        int value = option.getInt("value");
                        if(value == riskProfile){
                            Object title = jsonObject2Object(option, "title.en");
                            if(title != null && title instanceof String)
                                return (String) title;
                        }
                    }
                }
            }
        }
        return null;
    }

    public static JSONObject getProxyDateRange(String profileId , long compareDate) throws ParseException {
        JSONArray ranges = AllChannelReportDao.getProxyDateRanges(profileId);
        JSONObject targetRange = new JSONObject();
        for (int j = 0; j < ranges.length(); j++) {
            JSONObject range = ranges.getJSONObject(j);

            String strStartDt = "";
            if(range.has("START_DT")){
                strStartDt= range.getString("START_DT");
            }
            String strEndDt = "";
            if(range.has("END_DT")){
                strEndDt= range.getString("END_DT");
            }

            if (compareDate > -1 && strStartDt != "" && strEndDt != "") {
                long startTime  = ReportUtil.dateStr2Long(strStartDt, 0,0,0,0);
                long endTime  = ReportUtil.dateStr2Long(strEndDt, 23,59,59, 999000000);
                if(startTime <=compareDate && compareDate <= endTime)
                    targetRange = range;
            }
        }
        return targetRange;
    }
}
