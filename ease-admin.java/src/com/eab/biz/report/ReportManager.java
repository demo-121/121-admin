package com.eab.biz.report;

import com.eab.common.*;
import com.eab.json.model.*;
import com.eab.model.bcp.AppIdMap;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.JsonObject;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.eab.dao.report.Report.getChannels;
import static com.eab.dao.report.Report.getPickerOptions;


public class ReportManager {
	HttpServletRequest request;

	public ReportManager(HttpServletRequest request) {
		this.request = request;
	}
  
	public Response getSubmissionCasesReport(JSONObject paramJSON, boolean isInit) {
		UserPrincipalBean principal = Function.getPrincipal(request);
		JsonObject values = new JsonObject();
		Response resp = null;
		

		SimpleDateFormat sdf = new SimpleDateFormat(  Constants.DATETIME_PATTERN_AS_ADMIN_DATE);
		Calendar cal = Calendar.getInstance(); 
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		String endDate  = sdf.format(cal.getTime()) ;	
		cal.add(Calendar.DATE, -30);  
		String startDate=  sdf.format(cal.getTime());
		String polNo = "";
		String fcCode = "";
		String piName = "";
		boolean isGenExcel = false;

		// values
		try { 
				
			if (paramJSON != null  && !isInit){
				if(paramJSON.has("polNo") && paramJSON.get("polNo") instanceof String)
					polNo = paramJSON.getString("polNo");
				if(paramJSON.has("fcCode") && paramJSON.get("fcCode") instanceof String)
					fcCode = paramJSON.getString("fcCode");
				if(paramJSON.has("piName") && paramJSON.get("piName") instanceof String)
					piName = paramJSON.getString("piName");
				if(paramJSON.has("startDate") && paramJSON.get("startDate") instanceof String)
					startDate = paramJSON.getString("startDate");
				if(paramJSON.has("endDate") && paramJSON.get("endDate") instanceof String)
					endDate = paramJSON.getString("endDate");

				if(paramJSON.has("polNo") && paramJSON.has("fcCode") && paramJSON.has("piName") && paramJSON.has("startDate")&& paramJSON.has("endDate"))
					isGenExcel = true;
			}
			
			values.addProperty("polNo", polNo);
			values.addProperty("fcCode", fcCode);
			values.addProperty("piName",  piName);
			values.addProperty("startDate", startDate);
			values.addProperty("endDate", endDate);

			// AppBar - Title
			AppBarTitle appBarTitle = new AppBarTitle(null, "label", "", "REPORT.SUBMISSCASES", null, null);
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, null);
			Page pageObject = new Page("/Report", "");
			

			// create template
			List<Field> fields = new ArrayList<>();

			// Search fields	
			Field startDateField = new Field("startDate", "datePicker", startDate, null, "REPORT.STARTDATE"); 
			Field endDateField = new Field("endDate", "datePicker", endDate, null, "REPORT.ENDDATE");
			
			Field polNoField = new Field("polNo", "textField", polNo, null, "REPORT.POLNO");
			Field fcCodeField = new Field("fcCode", "textField", fcCode, null, "REPORT.FCCODE");
			Field piNameField = new Field("piName", "textField", piName, null, "REPORT.PINAME");

			Field generateBtn = new Field("gen", "button", "BUTTON.GENERATE"); 
			List<Option> reportOptons = getPickerOptions("REPORT.REPORT");

			fields.add(startDateField);
			fields.add(endDateField); 			
			fields.add(fcCodeField);
			fields.add(polNoField); 
			fields.add(piNameField);
			fields.add(generateBtn);
			// translation
			Template template = new Template("report", fields, "", "Reports");
			List<String> nameList = new ArrayList<String>();
			template.getNameCodes(nameList);
			appBar.getNameCodes(nameList);
			Map<String, String> nameMap = new HashMap<String, String>();
			nameMap = Function2.getTranslateMap(request, nameList);
			template.translate(nameMap);
			appBar.translate(nameMap);

			Content content = new Content(template, values, values);
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);
			
			
			if(isGenExcel){	 
				SubmissionCasesReport submissionCases = new SubmissionCasesReport();
				String b64 = submissionCases.genSubmissionCaseReport(principal.getUserName(), startDate, endDate, polNo, fcCode, piName, principal.getUserCode());
				if(b64!= null && !b64.isEmpty()){
					JsonObject report = new JsonObject();
					report.addProperty("file", "data:application/vnd.ms-excel;base64,"+b64);
					report.addProperty("fileName", "Submission Cases.xlsx");
					((JsonObject)resp.getContent().getValues()).add("report", report);
				}				
			}
			// get template
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;
	}
		
	public Response getOnlinePaymentReport(JSONObject paramJSON, boolean isInit) {
		UserPrincipalBean principal = Function.getPrincipal(request);
		JsonObject values = new JsonObject();
		Response resp = null;
		SimpleDateFormat sdf = new SimpleDateFormat(  Constants.DATETIME_PATTERN_AS_ADMIN_DATE);
		Calendar cal = Calendar.getInstance(); 
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);	
		
		String endDate = sdf.format(cal.getTime()) ;
		cal.add(Calendar.DATE, -30); 
		String startDate =  sdf.format(cal.getTime());
		String payMethod = "all";
		String payStatus = "success";
		boolean isGenExcel = false;

		// values
		try { 			
			if (paramJSON != null && !isInit){
				if(paramJSON.has("payStatus") && paramJSON.get("payStatus") instanceof String)
					payStatus = paramJSON.getString("payStatus");
				if(paramJSON.has("payMethod") && paramJSON.get("payMethod") instanceof String)
					payMethod = paramJSON.getString("payMethod");
				if(paramJSON.has("startDate") && paramJSON.get("startDate") instanceof String)
					startDate = paramJSON.getString("startDate");
				if(paramJSON.has("endDate") && paramJSON.get("endDate") instanceof String)
					endDate = paramJSON.getString("endDate");

				if(paramJSON.has("payStatus") && paramJSON.has("payMethod") && paramJSON.has("startDate") && paramJSON.has("endDate"))
					isGenExcel = true;
			}
			values.addProperty("payStatus", payStatus);
			values.addProperty("payMethod",  payMethod);
			values.addProperty("startDate", startDate);
			values.addProperty("endDate", endDate);


			// AppBar - Title
			AppBarTitle appBarTitle = new AppBarTitle(null, "label", "", "REPORT.ONLINEPAY", null, null);;
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, null);
			Page pageObject = new Page("/Report", "");
			

			// create template
			List<Field> fields = new ArrayList<>();

			// Search fields			
			List<Option> payStatusOptons = getPickerOptions("REPORT.PAYSTATUS");
			List<Option> payMethodOptons = getPickerOptions("REPORT.PAYMETHOD");
			
			Field payStatusField = new Field("payStatus", "radioGroup", payStatus, payStatusOptons, "REPORT.PAYSTATUS");
			Field payMethodField = new Field("payMethod", "selectField", payMethod, payMethodOptons, "REPORT.PAYMETHOD");
			Field startDateField = new Field("startDate", "datePicker", startDate, null, "REPORT.STARTDATE"); 
			Field endDateField = new Field("endDate", "datePicker", endDate, null, "REPORT.ENDDATE"); 

			Field generateBtn = new Field("gen", "button", "BUTTON.GENERATE"); 

			fields.add(startDateField);
			fields.add(endDateField);
			fields.add(payStatusField);
			fields.add(payMethodField);			
			fields.add(generateBtn);
			// translation
			Template template = new Template("report", fields, "", "Reports");
			List<String> nameList = new ArrayList<String>();
			template.getNameCodes(nameList);
			appBar.getNameCodes(nameList);
			Map<String, String> nameMap = new HashMap<String, String>();
			nameMap = Function2.getTranslateMap(request, nameList);
			template.translate(nameMap);
			appBar.translate(nameMap);

			Content content = new Content(template, values, values);
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);
			// get template
			
			
			if(isGenExcel){	  					 					
				OnlinePaymentReport onlinePayReport = new OnlinePaymentReport();
				String b64 = onlinePayReport.genOnlinePaymentExcel("Online Payment Report",principal.getUserName(), payStatus,   payMethod,   startDate,   endDate);
				if(b64!= null && !b64.isEmpty()){
					JsonObject report = new JsonObject();
					report.addProperty("file", "data:application/vnd.ms-excel;base64,"+b64);
					report.addProperty("fileName", "Online Payment Report.xlsx");
					((JsonObject)resp.getContent().getValues()).add("report", report);
				}
				
			}
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;
	}

	public Response getAllChannelReport(JSONObject paramJSON, boolean isInit) {
		UserPrincipalBean principal = Function.getPrincipal(request);
		JsonObject values = new JsonObject();
		Response resp = null;

		SimpleDateFormat sdf = new SimpleDateFormat(  Constants.DATETIME_PATTERN_AS_ADMIN_DATE);

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		String endDate  = sdf.format(cal.getTime());
		String startDate=  sdf.format(cal.getTime());
		String channel = "all";
		String dateType = "appStart";
		boolean isGenExcel = false;

		// values
		try {

			if (paramJSON != null  && !isInit){
				if(paramJSON.has("channel") && paramJSON.get("channel") instanceof String)
					channel = paramJSON.getString("channel");
				if(paramJSON.has("dateType") && paramJSON.get("dateType") instanceof String)
					dateType = paramJSON.getString("dateType");
				if(paramJSON.has("startDate") && paramJSON.get("startDate") instanceof String)
					startDate = paramJSON.getString("startDate");
				if(paramJSON.has("endDate") && paramJSON.get("endDate") instanceof String)
					endDate = paramJSON.getString("endDate");
				if(paramJSON.has("channel")  && paramJSON.has("startDate")&& paramJSON.has("endDate")&& paramJSON.has("dateType"))
					isGenExcel = true;
			}

			values.addProperty("channel", channel);
			values.addProperty("dateType", dateType);
			values.addProperty("startDate", startDate);
			values.addProperty("endDate", endDate);

			// AppBar - Title
			AppBarTitle appBarTitle = new AppBarTitle(null, "label", "", "REPORT.ALLCHANNEL", null, null);
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, null);
			Page pageObject = new Page("/Report", "");


			// create template
			List<Field> fields = new ArrayList<>();
			List<Option> dateTypeOptons = getPickerOptions("REPORT.DATETYPE");

			// Search fields
			Field startDateField = new Field("startDate", "datePicker", startDate, null, "REPORT.STARTDATE");
			Field endDateField = new Field("endDate", "datePicker", endDate, null, "REPORT.ENDDATE");

			List<Option> channelOptions = getChannels();
			for(int i = channelOptions.size()-1; i >= 0 ;i--){
				Option channelOption = channelOptions.get(i);
				if(channelOption.getValue().equals("GIAGENCY") || channelOption.getValue().equals("BANCA"))
					channelOptions.remove(i);
			}

			Field channelField = new Field("channel", "selectField", channel, channelOptions, "REPORT.CHANNEL");
			Field dateTypeField = new Field("dateType", "selectField", dateType, dateTypeOptons, "REPORT.DATETYPE");
			Field generateBtn = new Field("gen", "button", "BUTTON.GENERATE");

			fields.add(dateTypeField);
			fields.add(startDateField);
			fields.add(endDateField);
			fields.add(channelField);
			fields.add(generateBtn);
			// translation
			Template template = new Template("report", fields, "", "Reports");
			List<String> nameList = new ArrayList<String>();
			template.getNameCodes(nameList);
			appBar.getNameCodes(nameList);
			Map<String, String> nameMap = new HashMap<String, String>();
			nameMap = Function2.getTranslateMap(request, nameList);
			template.translate(nameMap);
			appBar.translate(nameMap);

			Content content = new Content(template, values, values);
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);

			JsonObject error = ReportUtil.dateRangeValidation(sdf.parse(startDate),sdf.parse(endDate), sdf.parse(sdf.format(new Date())));
			if(error != null && error.has("title"))
				((JsonObject)resp.getContent().getValues()).add("error", error);
			else if(isGenExcel){
				AllChannelReport allChannelReport = new AllChannelReport();
				String b64 = allChannelReport.genAllChannelReport(principal.getUserName(), startDate, endDate,dateType, channel, principal.getUserCode());
				if(b64!= null && !b64.isEmpty()){
					JsonObject report = new JsonObject();
					report.addProperty("file", "data:application/vnd.ms-excel;base64,"+b64);
					report.addProperty("fileName", "All_Channel_Report.xlsx");
					((JsonObject)resp.getContent().getValues()).add("report", report);
				}
			}
			// get template
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;
	}

	public Response getProxyHistoryReport(JSONObject paramJSON, boolean isInit) {
		UserPrincipalBean principal = Function.getPrincipal(request);
		JsonObject values = new JsonObject();
		Response resp = null;

		SimpleDateFormat sdf = new SimpleDateFormat(  Constants.DATETIME_PATTERN_AS_ADMIN_DATE);

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		String endDate  = sdf.format(cal.getTime());
		String startDate=  sdf.format(cal.getTime());
		boolean isGenExcel = false;

		// values
		try {

			if (paramJSON != null  && !isInit){
				if(paramJSON.has("startDate") && paramJSON.get("startDate") instanceof String)
					startDate = paramJSON.getString("startDate");
				if(paramJSON.has("endDate") && paramJSON.get("endDate") instanceof String)
					endDate = paramJSON.getString("endDate");
				if(paramJSON.has("startDate")&& paramJSON.has("endDate"))
					isGenExcel = true;
			}
			 
			values.addProperty("startDate", startDate);
			values.addProperty("endDate", endDate);

			// AppBar - Title
			AppBarTitle appBarTitle = new AppBarTitle(null, "label", "", "REPORT.PROXY", null, null);
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, null);
			Page pageObject = new Page("/Report", "");


			// create template
			List<Field> fields = new ArrayList<>();

			// Search fields
			Field startDateField = new Field("startDate", "datePicker", startDate, null, "REPORT.STARTDATE");
			Field endDateField = new Field("endDate", "datePicker", endDate, null, "REPORT.ENDDATE");

			Field generateBtn = new Field("gen", "button", "BUTTON.GENERATE");

			fields.add(startDateField);
			fields.add(endDateField);
			 
			fields.add(generateBtn);
			// translation
			Template template = new Template("report", fields, "", "Reports");
			List<String> nameList = new ArrayList<String>();
			template.getNameCodes(nameList);
			appBar.getNameCodes(nameList);
			Map<String, String> nameMap = new HashMap<String, String>();
			nameMap = Function2.getTranslateMap(request, nameList);
			template.translate(nameMap);
			appBar.translate(nameMap);

			Content content = new Content(template, values, values);
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);

			JsonObject error = ReportUtil.dateRangeValidation(sdf.parse(startDate),sdf.parse(endDate), sdf.parse(sdf.format(new Date())));

			if(error != null && error.has("title"))
				((JsonObject)resp.getContent().getValues()).add("error", error);
			else if(isGenExcel){
				ProxyHistoryReportController proxyHistoryReport = new ProxyHistoryReportController();
				String b64 = proxyHistoryReport.generateReport(startDate, endDate, principal.getUserCode());
				if(b64!= null && !b64.isEmpty()){
					JsonObject report = new JsonObject();
					report.addProperty("file", "data:application/vnd.ms-excel;base64,"+b64);
					report.addProperty("fileName", "Proxy_History_Report.xls");
					((JsonObject)resp.getContent().getValues()).add("report", report);
				}
			}
			// get template
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;
	}

	private void insertAppIdMapToList(ArrayList<AppIdMap> appIdMapList, 
			String app_id, String policy_number){
		int index;
		int max_size = appIdMapList.size();
		AppIdMap app_id_map;
		
		if (max_size == 0){
			app_id_map 					= new AppIdMap();
			app_id_map.appId			= app_id;
			app_id_map.policyNumbers 	= new ArrayList<String>();
			app_id_map.policyNumbers.add(policy_number);
			appIdMapList.add(app_id_map);
			return;
		}
		
		AppIdMap cur_app_id_map;
		String cur_app_id;
		for(index = 0; index < max_size; index++){
			cur_app_id_map 	= appIdMapList.get(index);
			cur_app_id 		= cur_app_id_map.appId; 
			
			if (cur_app_id.equalsIgnoreCase(app_id)){
				cur_app_id_map.policyNumbers.add(policy_number);
				return;
			}
		}
		
		app_id_map 					= new AppIdMap();
		app_id_map.appId			= app_id;
		app_id_map.policyNumbers 	= new ArrayList<String>();
		app_id_map.policyNumbers.add(policy_number);
		appIdMapList.add(app_id_map);

	}
	
	/** Should be no duplicated policy numbers*/
	private void insertPolicyNumberToList(ArrayList<String> policy_numbers, String policy_number){
		int index;
		int max_size = policy_numbers.size();
		if (max_size == 0){policy_numbers.add(policy_number); return;}
		
		String cur_policy_number;
		for(index = 0; index < max_size; index++){
			cur_policy_number = policy_numbers.get(index);
			
			if (cur_policy_number.equalsIgnoreCase(policy_number)){return;}
		}
		
		policy_numbers.add(policy_number);
	}
}
