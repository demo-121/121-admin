package com.eab.biz.report;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.biz.bcp.BcpRequest;
import com.eab.common.*;
import com.eab.couchbase.CBServer;
import com.eab.dao.profile.User;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.model.MenuList;
import com.eab.model.profile.UserBean;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.JsonObject;

import static com.eab.dao.report.Report.getPickerOptions;
import static com.eab.dao.report.Report.getChannels;

public class ReportManagerNew {
	HttpServletRequest request;
	UserPrincipalBean principal;
	String base_url;
	User currentUser = new User();
	JSONObject paramsToApi = new JSONObject();
    
	
	private final String USER_AGENT = "Mozilla/5.0";
	public ReportManagerNew(HttpServletRequest request) {
		this.request = request;
		this.principal = Function.getPrincipal(request);
		this.base_url = EnvVariable.get("INTERNAL_API_DOMAIN");
	}
	
	public Response getAllChannelReport(JSONObject paramJSON, boolean isInit) {
		JsonObject values = new JsonObject();
		Response resp = null;

		SimpleDateFormat sdf = new SimpleDateFormat(  Constants.DATETIME_PATTERN_AS_ADMIN_DATE);

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		String endDate  = sdf.format(cal.getTime());
		String startDate=  sdf.format(cal.getTime());
		String channel = "all";
		String dateType = "appStart";
		boolean isGenExcel = false;

		// values
		try {

			if (paramJSON != null  && !isInit){
				if(paramJSON.has("channel") && paramJSON.get("channel") instanceof String) {
					paramsToApi.put("channel", paramJSON.getString("channel")) ;
				}
				if(paramJSON.has("dateType") && paramJSON.get("dateType") instanceof String) {
					paramsToApi.put("dateType", paramJSON.getString("dateType")) ;
				}
				if(paramJSON.has("startDate") && paramJSON.get("startDate") instanceof String) {
					paramsToApi.put("startDate", paramJSON.getString("startDate")) ;
				}
				if(paramJSON.has("endDate") && paramJSON.get("endDate") instanceof String) {
					paramsToApi.put("endDate", paramJSON.getString("endDate")) ;
				}
				if(paramJSON.has("channel")  && paramJSON.has("startDate")&& paramJSON.has("endDate")&& paramJSON.has("dateType")) {
					isGenExcel = true;
				}
			}

			values.addProperty("channel", channel);
			values.addProperty("dateType", dateType);
			values.addProperty("startDate", startDate);
			values.addProperty("endDate", endDate);

			// AppBar - Title
			AppBarTitle appBarTitle = new AppBarTitle(null, "label", "", "REPORT.ALLCHANNEL", null, null);
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, null);
			Page pageObject = new Page("/Report", "");


			// create template
			List<Field> fields = new ArrayList<>();
			List<Option> dateTypeOptons = getPickerOptions("REPORT.DATETYPE");

			// Search fields
			Field startDateField = new Field("startDate", "datePicker", startDate, null, "REPORT.STARTDATE");
			Field endDateField = new Field("endDate", "datePicker", endDate, null, "REPORT.ENDDATE");

			List<Option> channelOptions = getChannels();
			for(int i = channelOptions.size()-1; i >= 0 ;i--){
				Option channelOption = channelOptions.get(i);
				if(channelOption.getValue().equals("GIAGENCY") || channelOption.getValue().equals("BANCA"))
					channelOptions.remove(i);
			}

			Field channelField = new Field("channel", "selectField", channel, channelOptions, "REPORT.CHANNEL");
			Field dateTypeField = new Field("dateType", "selectField", dateType, dateTypeOptons, "REPORT.DATETYPE");
			Field generateBtn = new Field("gen", "button", "BUTTON.GENERATE");

			fields.add(dateTypeField);
			fields.add(startDateField);
			fields.add(endDateField);
			fields.add(channelField);
			fields.add(generateBtn);
			// translation
			Template template = new Template("report", fields, "", "Reports");
			List<String> nameList = new ArrayList<String>();
			template.getNameCodes(nameList);
			appBar.getNameCodes(nameList);
			Map<String, String> nameMap = new HashMap<String, String>();
			nameMap = Function2.getTranslateMap(request, nameList);
			template.translate(nameMap);
			appBar.translate(nameMap);

			Content content = new Content(template, values, values);
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);

			JsonObject error = ReportUtil.dateRangeValidation(sdf.parse(startDate),sdf.parse(endDate), sdf.parse(sdf.format(new Date())));
			if(error != null && error.has("title"))
				((JsonObject)resp.getContent().getValues()).add("error", error);
			else if(isGenExcel){
				JsonObject apiResult = callApi("All Channel Report");
				((JsonObject)resp.getContent().getValues()).add("error", apiResult);
			}
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;
	}
	
	public Response getSubmissionCasesReport(JSONObject paramJSON, boolean isInit) {
		JsonObject values = new JsonObject();
		Response resp = null;
		

		SimpleDateFormat sdf = new SimpleDateFormat(  Constants.DATETIME_PATTERN_AS_ADMIN_DATE);
		Calendar cal = Calendar.getInstance(); 
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		String endDate  = sdf.format(cal.getTime()) ;	
		cal.add(Calendar.DATE, -30);  
		String startDate=  sdf.format(cal.getTime());
		String polNo = "";
		String fcCode = "";
		String piName = "";
		boolean isGenExcel = false;

		// values
		try { 
				
			if (paramJSON != null  && !isInit){
				if(paramJSON.has("polNo") && paramJSON.get("polNo") instanceof String) {
					paramsToApi.put("polNo", paramJSON.getString("polNo")) ;
				}
				if(paramJSON.has("fcCode") && paramJSON.get("fcCode") instanceof String) {
					paramsToApi.put("fcCode", paramJSON.getString("fcCode")) ;
				}
				if(paramJSON.has("piName") && paramJSON.get("piName") instanceof String) {
					paramsToApi.put("piName", paramJSON.getString("piName")) ;
					
				}
				if(paramJSON.has("startDate") && paramJSON.get("startDate") instanceof String) {
					paramsToApi.put("startDate", paramJSON.getString("startDate")) ;
					
				}
				if(paramJSON.has("endDate") && paramJSON.get("endDate") instanceof String) {
					paramsToApi.put("endDate", paramJSON.getString("endDate")) ;
				}

				if(paramJSON.has("polNo") && paramJSON.has("fcCode") && paramJSON.has("piName") && paramJSON.has("startDate")&& paramJSON.has("endDate")) {
					isGenExcel = true;
				}
			}
			
			values.addProperty("polNo", polNo);
			values.addProperty("fcCode", fcCode);
			values.addProperty("piName",  piName);
			values.addProperty("startDate", startDate);
			values.addProperty("endDate", endDate);

			// AppBar - Title
			AppBarTitle appBarTitle = new AppBarTitle(null, "label", "", "REPORT.SUBMISSCASES", null, null);
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, null);
			Page pageObject = new Page("/Report", "");
			

			// create template
			List<Field> fields = new ArrayList<>();

			// Search fields	
			Field startDateField = new Field("startDate", "datePicker", startDate, null, "REPORT.STARTDATE"); 
			Field endDateField = new Field("endDate", "datePicker", endDate, null, "REPORT.ENDDATE");
			
			Field polNoField = new Field("polNo", "textField", polNo, null, "REPORT.POLNO");
			Field fcCodeField = new Field("fcCode", "textField", fcCode, null, "REPORT.FCCODE");
			Field piNameField = new Field("piName", "textField", piName, null, "REPORT.PINAME");

			Field generateBtn = new Field("gen", "button", "BUTTON.GENERATE"); 
			List<Option> reportOptons = getPickerOptions("REPORT.REPORT");

			fields.add(startDateField);
			fields.add(endDateField); 			
			fields.add(fcCodeField);
			fields.add(polNoField); 
			fields.add(piNameField);
			fields.add(generateBtn);
			// translation
			Template template = new Template("report", fields, "", "Reports");
			List<String> nameList = new ArrayList<String>();
			template.getNameCodes(nameList);
			appBar.getNameCodes(nameList);
			Map<String, String> nameMap = new HashMap<String, String>();
			nameMap = Function2.getTranslateMap(request, nameList);
			template.translate(nameMap);
			appBar.translate(nameMap);

			Content content = new Content(template, values, values);
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);
			if(isGenExcel){	 
				JsonObject apiResult = callApi("Submission Cases");
				((JsonObject)resp.getContent().getValues()).add("error", apiResult);
			}
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;
	}
	
	public Response getOnlinePaymentReport(JSONObject paramJSON, boolean isInit) {
		JsonObject values = new JsonObject();
		Response resp = null;
		SimpleDateFormat sdf = new SimpleDateFormat(  Constants.DATETIME_PATTERN_AS_ADMIN_DATE);
		Calendar cal = Calendar.getInstance(); 
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);	
		
		String endDate = sdf.format(cal.getTime()) ;
		cal.add(Calendar.DATE, -30); 
		String startDate =  sdf.format(cal.getTime());
		String payMethod = "all";
		String payStatus = "success";
		boolean isGenExcel = false;

		// values
		try { 			
			if (paramJSON != null && !isInit){
				if(paramJSON.has("payStatus") && paramJSON.get("payStatus") instanceof String) {
					paramsToApi.put("payStatus", paramJSON.getString("payStatus")) ;
				}
				if(paramJSON.has("payMethod") && paramJSON.get("payMethod") instanceof String) {
					paramsToApi.put("payMethod", paramJSON.getString("payMethod")) ;
				}
				if(paramJSON.has("startDate") && paramJSON.get("startDate") instanceof String) {
					paramsToApi.put("startDate", paramJSON.getString("startDate")) ;
				}
				if(paramJSON.has("endDate") && paramJSON.get("endDate") instanceof String) {
					paramsToApi.put("endDate", paramJSON.getString("endDate")) ;
				}

				if(paramJSON.has("payStatus") && paramJSON.has("payMethod") && paramJSON.has("startDate") && paramJSON.has("endDate"))
					isGenExcel = true;
			}
			values.addProperty("payStatus", payStatus);
			values.addProperty("payMethod",  payMethod);
			values.addProperty("startDate", startDate);
			values.addProperty("endDate", endDate);


			// AppBar - Title
			AppBarTitle appBarTitle = new AppBarTitle(null, "label", "", "REPORT.ONLINEPAY", null, null);;
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, null);
			Page pageObject = new Page("/Report", "");
			

			// create template
			List<Field> fields = new ArrayList<>();

			// Search fields			
			List<Option> payStatusOptons = getPickerOptions("REPORT.PAYSTATUS");
			List<Option> payMethodOptons = getPickerOptions("REPORT.PAYMETHOD");
			
			Field payStatusField = new Field("payStatus", "radioGroup", payStatus, payStatusOptons, "REPORT.PAYSTATUS");
			Field payMethodField = new Field("payMethod", "selectField", payMethod, payMethodOptons, "REPORT.PAYMETHOD");
			Field startDateField = new Field("startDate", "datePicker", startDate, null, "REPORT.STARTDATE"); 
			Field endDateField = new Field("endDate", "datePicker", endDate, null, "REPORT.ENDDATE"); 

			Field generateBtn = new Field("gen", "button", "BUTTON.GENERATE"); 

			fields.add(startDateField);
			fields.add(endDateField);
			fields.add(payStatusField);
			fields.add(payMethodField);			
			fields.add(generateBtn);
			// translation
			Template template = new Template("report", fields, "", "Reports");
			List<String> nameList = new ArrayList<String>();
			template.getNameCodes(nameList);
			appBar.getNameCodes(nameList);
			Map<String, String> nameMap = new HashMap<String, String>();
			nameMap = Function2.getTranslateMap(request, nameList);
			template.translate(nameMap);
			appBar.translate(nameMap);

			Content content = new Content(template, values, values);
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);
			// get template
			
			
			if(isGenExcel){	 
				JsonObject apiResult = callApi("Online Payment Report");
				((JsonObject)resp.getContent().getValues()).add("error", apiResult);
				
			}
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;
	}

	public Response getProxyHistoryReport(JSONObject paramJSON, boolean isInit) {
		JsonObject values = new JsonObject();
		Response resp = null;

		SimpleDateFormat sdf = new SimpleDateFormat(  Constants.DATETIME_PATTERN_AS_ADMIN_DATE);

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		String endDate  = sdf.format(cal.getTime());
		String startDate=  sdf.format(cal.getTime());
		boolean isGenExcel = false;

		// values
		try {

			if (paramJSON != null  && !isInit){
				if(paramJSON.has("startDate") && paramJSON.get("startDate") instanceof String) {
					paramsToApi.put("startDate", paramJSON.getString("startDate")) ;
				}
				if(paramJSON.has("endDate") && paramJSON.get("endDate") instanceof String) {
					paramsToApi.put("endDate", paramJSON.getString("endDate")) ;
				}
				if(paramJSON.has("startDate")&& paramJSON.has("endDate")) {
					isGenExcel = true;
				}
			}
			 
			values.addProperty("startDate", startDate);
			values.addProperty("endDate", endDate);

			// AppBar - Title
			AppBarTitle appBarTitle = new AppBarTitle(null, "label", "", "REPORT.PROXY", null, null);
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, null);
			Page pageObject = new Page("/Report", "");


			// create template
			List<Field> fields = new ArrayList<>();

			// Search fields
			Field startDateField = new Field("startDate", "datePicker", startDate, null, "REPORT.STARTDATE");
			Field endDateField = new Field("endDate", "datePicker", endDate, null, "REPORT.ENDDATE");

			Field generateBtn = new Field("gen", "button", "BUTTON.GENERATE");

			fields.add(startDateField);
			fields.add(endDateField);
			 
			fields.add(generateBtn);
			// translation
			Template template = new Template("report", fields, "", "Reports");
			List<String> nameList = new ArrayList<String>();
			template.getNameCodes(nameList);
			appBar.getNameCodes(nameList);
			Map<String, String> nameMap = new HashMap<String, String>();
			nameMap = Function2.getTranslateMap(request, nameList);
			template.translate(nameMap);
			appBar.translate(nameMap);

			Content content = new Content(template, values, values);
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);

			JsonObject error = ReportUtil.dateRangeValidation(sdf.parse(startDate),sdf.parse(endDate), sdf.parse(sdf.format(new Date())));

			if(error != null && error.has("title"))
				((JsonObject)resp.getContent().getValues()).add("error", error);
			else if(isGenExcel){
				JsonObject apiResult = callApi("Proxy History Report");
				((JsonObject)resp.getContent().getValues()).add("error", apiResult);
			}
			// get template
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;
	}

	
	
	private JsonObject callApi (String targetReport) {
		JsonObject result = new JsonObject();
		StringBuilder sb = new StringBuilder();
		String url 		= base_url + "/report/generator";
		HttpURLConnection con;
		try{
			UserBean ub = currentUser.get(principal.getUserCode(), request);
			sb.append("{\"targetReport\":\"" + targetReport + "\",");
			Iterator<?> keys = paramsToApi.keys();
			
			while( keys.hasNext() ) {
				String key = (String)keys.next();
                sb.append("\"" + key + "\"" + ":\"" + paramsToApi.get(key) + "\",");
			}
			sb.append("\"UserName\" : \"" + principal.getUserName() + "\", \"UserCode\" : \"" +principal.getUserCode() + "\", \"toEmail\" : \"" + ub.getEmail() + "\"");
			
			sb.append("}");
//			sb.append("{\"UserName\":\"" + principal.getUserName() + "\", \"startDate\" : \"" + params.getString("startDate") + "\", \"endDate\" : \"" + params.getString("endDate") + "\", \"dateType\" : \"" + params.getString("dateType") + "\", \"channel\" : \"" + params.getString("channel") + "\", \"UserCode\" : \"" +principal.getUserCode() + "\", \"toEmail\" : \"" + ub.getEmail() + "\"} ");
			URL obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
	 
		    // Setting basic post request
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			con.setRequestProperty("Content-Type","application/json");
		 
		  
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(sb.toString());
			wr.flush();
			wr.close();
 
			int responseCode = con.getResponseCode();
			 
			BufferedReader in = null;
			if ((responseCode >= 200) && (responseCode < 400)){
				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			}
			else{
				in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
			}
			
			String output;
			
			
			sb.delete(0,  sb.length());
			while ((output = in.readLine()) != null) {
				sb.append(output);
			}
			in.close();
  
			String 		response_str = sb.toString();
//			Log.info("response from api:: " + response_str);
			
			JsonObject status = new JsonObject();
			JSONObject 	response_json 	= new JSONObject(response_str);
			if(response_json.has("success") && response_json.get("success") instanceof Boolean) {
				if(response_json.getBoolean("success")) {
					status.addProperty("status", true);
//					status.addProperty("title", "All Channel Report");
					status.addProperty("title", targetReport);
					status.addProperty("message", "System is generating your report. You will receive an email shortly.");
				} else {
					status.addProperty("status", false);
					status.addProperty("title", "Generate Failed");
					status.addProperty("message", response_json.getString("message"));
				}
			} else {
				status.addProperty("status", false);
				status.addProperty("title", "Generate Failed");
				status.addProperty("message", "Something went wrong");
			}
			result = status;
//			result.put("success", status);
//			return result;
			
//			((JsonObject)resp.getContent().getValues()).add("error", status);
			
			
		}
		catch (Exception e){
			
			sb.delete(0, sb.length());
			sb.append("ReportManagerNew.callApi( " + targetReport + " ); error - ").append(e);
			
			Log.info(sb.toString());
			JsonObject error1 = new JsonObject();
			error1.addProperty("status", false);
			error1.addProperty("msg", e.toString());
			result = error1;
//			result.put("error", error1);
//			return result;
//			((JsonObject)resp.getContent().getValues()).add("error", error1);
		}
		
		return result;
	}



}
