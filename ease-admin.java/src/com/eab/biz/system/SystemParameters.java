package com.eab.biz.system;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;

public class SystemParameters {

	public static HashMap<String, String> parameters = null;
	
	/***
	 * Load System Parameters from Database.
	 * @return Loaded parameter set from database? True or False.
	 * @throws SQLException 
	 */
	public static boolean load() throws SQLException {
		HashMap<String, String> output = null;
		boolean status = false;
		com.eab.dao.system.SystemParametersTrx dao = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = DBAccess.getConnection();
			dao = new com.eab.dao.system.SystemParametersTrx();
			
			rs = dao.get(conn);
			if (rs != null) {
				output = new HashMap<String, String>();
				while (rs.next()) {
					output.put(rs.getString("param_code"), rs.getString("param_value"));
				}
				
				parameters = output;
				status = true;
			}
		} catch(Exception e) {
			Log.error(e);
		} finally {
			rs = null;
			if (conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
		return status;
	}
	
	/***
	 * Get System Parameter value
	 * @param paramName
	 * @return
	 */
	public static String get(String paramName) throws SQLException {
		if (parameters == null)
			load();
		
		return parameters.get(paramName);
	}
}
