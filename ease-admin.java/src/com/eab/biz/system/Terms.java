package com.eab.biz.system;

import com.eab.common.Log;
import com.eab.dao.system.TermsTrx;

public class Terms {
	
	public boolean getTermsAccept(String userCode) {
		boolean output = false;
		TermsTrx dbo = null;
		
		try {
			dbo = new TermsTrx();
			String result = dbo.getTermsAccept(userCode);
			Log.debug("{Terms getTermsAccept} userCode=" + userCode + "; result=" + result);
			if ("1".equals(result)) {
				output = true;
			}
		} catch(Exception e) {
			Log.error(e);
		} finally {
			dbo = null;
		}
		
		return output;
	}
	
	public boolean setTermsAccept(String userCode, String ipAddr) {
		boolean output = false;
		TermsTrx dbo = null;
		
		try {
			dbo = new TermsTrx();
			output = dbo.setTermsAccept(userCode, ipAddr);
			Log.debug("{Terms setTermsAccept} userCode=" + userCode + "; ipaddr=" + ipAddr + "; result=" + output);
		} catch(Exception e) {
			Log.error(e);
		} finally {
			dbo = null;
		}
		
		return output;
	}
}
