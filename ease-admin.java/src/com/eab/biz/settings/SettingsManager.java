package com.eab.biz.settings;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.eab.common.Constants;
import com.eab.common.CryptoUtil;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.MainMenu;
import com.eab.common.Token;
import com.eab.dao.dynamic.Companies;
import com.eab.dao.needs.Needs;
import com.eab.dao.profile.User;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Result;
import com.eab.json.model.Template;
import com.eab.model.MenuList;
import com.eab.model.RSAKeyPair;
import com.eab.model.dynamic.CompanyBean;
import com.eab.model.profile.UserBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.system.SysParamBean;
import com.eab.webservice.dynamic.DynamicFunction;
import com.google.gson.JsonObject;

public class SettingsManager {
	HttpServletRequest request;

	public SettingsManager(HttpServletRequest request) {
		this.request = request;
	}

	public Response changeCompany(String compCode) {
		UserPrincipalBean principal = Function.getPrincipal(request);
		String userCode = principal.getUserCode();
		Response resp = null;
		String userRole = null;
		Result state = new Result();
		User user = new User();
		
		try {
			
			if (principal.isRegional()) {
				// keep the user role
				userRole = principal.getRoleCode();
			} else {
				userRole = user.getRoleIdByUser(userCode, compCode);
			}
			
			if (userRole != null && !userRole.isEmpty()) {
				Log.debug("has user role");
				// update compCode, userRole in principal	
				principal.setCompCode(compCode);
				principal.setRoleCode(userRole);
				User userObj = new User();
				principal.setChannelList(userObj.getChannelCodeByUser(principal));
		
				HttpSession session = request.getSession();
				session.setAttribute(Constants.USER_PRINCIPAL, principal);
				// return result
				state.setSuccess(true);
				state.setMsg("COMPANY.CHANGE.SUCCESS");
	
				// get company name
				Companies comp = new Companies();
				CompanyBean cb = comp.getCompany(compCode);
				JsonObject userOb = new JsonObject();
				userOb.addProperty("compName", cb.getCompName());
				userOb.addProperty("dateFormat", comp.getCompanyDateFormat(compCode));
				userOb.addProperty("timeFormat", comp.getCompanyTimeFormat(compCode));
				//get user name
				UserBean ub = user.get(userCode, request);
				userOb.addProperty("username", ub.getUserName());
	
				Function2 func2 = new Function2();
				String sysName = null;
				Map<String, SysParamBean> sysParamList = null;
				sysParamList = func2.getSysParam();
				sysName = sysParamList.get("SYS_NAME").getParamValue();
				userOb.addProperty("sysName", sysName);
				
				resp = getMySettings();
				resp.setUser(userOb);

				MenuList menuList = getMenu(request.getSession(), userRole);
				resp.setMenuList(menuList.getList());

			} else {
				state.setSuccess(false);
				state.setMsg("COMPANY.CHANGE.FAIL");
			}
		} catch (Exception e) {
			Log.error(e);
			state.setSuccess(false);
			state.setMsg("COMPANY.CHANGE.FAIL");
		}

		// create dialog
		String dialogId = "comDialog";
		String dialogTitle = "COMPANY.CHANGE";
		Field negative = new Field("ok", "button", "BUTTON.OK");
		Dialog dialog = new Dialog(dialogId, dialogTitle, state.getMsg(), null, negative, 30);
		dialog.translate(request);
		
		if(resp == null)
			resp = getMySettings();
		
		resp.setDialog(dialog);
		resp.setPath("/Home");
		resp.setAction(Constants.ActionTypes.PAGE_REDIRECT);
		
		return resp;
	}

	public Response changeLanguage(String langCode) {
		UserPrincipalBean principal = Function.getPrincipal(request);
		String userCode = principal.getUserCode();
		Response resp = null;
		DBManager dbm = null;
		Result state = new Result();
		int result = -1;
		try {
			state.setSuccess(true);
			state.setMsg("LANGUAGE.CHANGE.SUCCESS");	
			
			//set user lang into session
			principal.setLangCode(langCode);
			
			Language lang = new Language();
			lang.setLang(request, langCode);
			Map<String, Object> nameMap = lang.getUITranslation(request) ;
			principal.setUITranslation(nameMap);
			resp = getMySettings();
			resp.setLangMap(lang.getRegularLangList(nameMap));
			
			MenuList menuList = getMenu(request.getSession(), principal.getRoleCode());
			resp.setMenuList(menuList.getList());
		} catch (Exception e) {
			state.setSuccess(false);
			state.setMsg("LANGUAGE.CHANGE.FAIL");
		}

		// create dialog
		String dialogId = "langDialog";
		String dialogTitle = "LANGUAGE.CHANGE";
		Field negative = new Field("ok", "button", "BUTTON.OK");
		Dialog dialog = new Dialog(dialogId, dialogTitle, state.getMsg(), null, negative, 30);
		dialog.translate(request);
		resp.setDialog(dialog);		
		
		resp.setPath("/Home");
		resp.setAction(Constants.ActionTypes.PAGE_REDIRECT);

		return resp;
	}

	public Response changePassword(String curPass, String newPass, String newPass1) {
		RSAKeyPair keypair = null;
		Response resp = null;
		UserBean userInfo = null;
		CryptoUtil crypto = null;
		Result state = new Result();

		// get session for decrypt
		HttpSession session = request.getSession();
		UserPrincipalBean principal = Function.getPrincipal(request);
		String userCode = principal.getUserCode();
		if (session.getAttribute(Constants.CRYPTO_KEYPAIR_NAME) != null) {
			keypair = (RSAKeyPair) session.getAttribute(Constants.CRYPTO_KEYPAIR_NAME);
		}

		if (DynamicFunction.isNotEmptyOrNull(curPass) && DynamicFunction.isNotEmptyOrNull(newPass) && DynamicFunction.isNotEmptyOrNull(newPass1)) {
			String curPasswordInput = null;
			String newPasswordInput = null;
			String newPassword1Input = null;
			User userObj = new User();

			try {
				curPasswordInput = Function.decryptJsText(keypair, curPass);
				newPasswordInput = Function.decryptJsText(keypair, newPass);
				newPassword1Input = Function.decryptJsText(keypair, newPass1);

				userInfo = userObj.get(userCode, request);
				crypto = new CryptoUtil();
				// encrypt password(from browser)
				String encryptedCurPasswordInput = crypto.encryptUserData(userInfo.getUserCode(), curPasswordInput);
				// Validate Password
				// Log.info("changePassword encryptedCurPasswordInput: " + encryptedCurPasswordInput);
				// Log.info("changePassword getUserPassword: " + userInfo.getUserPassword());
				// Ignore encryption for Testing
				// if (encryptedCurPasswordInput != null && encryptedCurPasswordInput.equals(userInfo.getUserPassword())) {
				if (curPasswordInput != null && curPasswordInput.equals(userInfo.getUserPassword())) {
					// update password
					if (validateNewPassword(newPasswordInput, newPassword1Input)) {
						// pass the password without encryption
						state = changePassword(userInfo.getUserCode(), newPasswordInput);
					} else {
						state.setSuccess(false);
						state.setErrType(Constants.LoginMsg.WRONG_PASSWORD);
						state.setMsg("PASSWORD.NEW.WRONG");
					}
				} else {
					state.setSuccess(false);
					state.setErrType(Constants.LoginMsg.WRONG_PASSWORD);
					state.setMsg("PASSWORD.WRONG");
				}
			} catch (SQLException e) {
				state.setSuccess(false);
				state.setErrType(Constants.LoginMsg.UPDATE_FAILED);
				state.setMsg("PASSWORD.UPDATE.FAIL");
			}
		} else {
			state.setSuccess(false);
			state.setErrType(Constants.LoginMsg.INVALID_INPUT);
			state.setMsg("INPUT.EMPTY");
		}
		// create dialog
		String dialogId = "loginDialog";
		String diaMsg = state.getSuccess() ? "PASSWORD.CHANGE.SUCCESS" : state.getMsg();
		String dialogTitle = state.getSuccess() ? "PASSWORD.CHANGE" : "PASSWORD.CHANGE.FAIL";
		Field positive = null;
		Field negative = new Field("ok", "button", "BUTTON.OK");
		Dialog dialog = new Dialog(dialogId, dialogTitle, diaMsg, positive, negative, 30);

		resp = getMySettings();
		dialog.translate(request);
		resp.setDialog(dialog);
		return resp;
	}

	public boolean validateNewPassword(String newPass1, String newPass2) {
		if (newPass1.equals(newPass2))
			return true;
		return false;
	}

	public Result changePassword(String userCode, String newPassword) {
		boolean changePassword = false;
		Result state = new Result();
		CryptoUtil crypto = new CryptoUtil();

		DBManager dbm = null;
		Connection conn = null;
		try {
			// encrypt password(from browser)
			String encryptedNewPassword = crypto.encryptUserData(userCode, newPassword);
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			// check previous password
			HashMap<String, String> condMap = new HashMap<String, String>();
			condMap.put("user_code", userCode);
			condMap.put("password", encryptedNewPassword);
			if (!hasPasswordRecord("aud_user_password", condMap, dbm, conn, 2)) {
				// update password
				changePassword = updatePassword(dbm, userCode, encryptedNewPassword, userCode);
				if (changePassword) {
					// update aud_user_password
					state.setSuccess(true);
					updateAudPassword(dbm, encryptedNewPassword, userCode);
				} else {
					state.setSuccess(false);
					state.setErrType(Constants.LoginMsg.UPDATE_FAILED);
					state.setMsg("PASSWORD.UPDATE.FAIL");
				}
			} else {
				state.setSuccess(false);
				state.setErrType(Constants.LoginMsg.REPEATED_PASSWORD);
				state.setMsg("PASSWORD.REPEATED");
			}
		} catch (Exception e) {
			Log.error(e);
			state.setSuccess(false);
			state.setErrType(Constants.LoginMsg.WRONG_PARAMETER);
			state.setMsg("INPUT.EMPTY");
		} finally {
			try {if (conn != null && !conn.isClosed()) conn.close();} catch(Exception e) {}
			conn = null;
			dbm = null;
		}
		return state;
	}

	public boolean checkPasswordHistory(String userCode, String newPassword) throws SQLException {
		//for user input an empty password
		if(newPassword == null || "".equals(newPassword))
			return false;
		
		CryptoUtil crypto = new CryptoUtil();

		DBManager dbm = null;
		Connection conn = null;

		try {
			// encrypt password(from browser)
			String encryptedNewPassword = crypto.encryptUserData(userCode, newPassword);
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			// check previous password
			HashMap<String, String> condMap = new HashMap<String, String>();
			condMap.put("user_code", userCode);
			if (encryptedNewPassword == null) {
				condMap.put("password", "");
			} else {
				condMap.put("password", encryptedNewPassword);
			}

			if(dbm == null || conn == null){
				return true;
			}
			
			if (hasPasswordRecord("aud_user_password", condMap, dbm, conn, 2)) {
				return false;
			} else {
				return true;
			}

		} catch (Exception e) {
			return false;
		} finally {
			dbm = null;

			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}

	}
	//check if the new password is found in as last x old password
	public static boolean hasPasswordRecord(String table, HashMap<String,String> condMap, DBManager dbm, Connection conn, int checkNumber) {			
			boolean hasRecord = false;
			ResultSet rs = null;
			String newPswd=null;			
			try {
				String sql = null;
				
				try {
					for (Map.Entry<String, String> entry : condMap.entrySet())
					{ 
						if(entry.getKey()=="password")
							newPswd= entry.getValue();
						if(entry.getKey()=="user_code")
							dbm.param(entry.getValue(), DataType.TEXT);
					    System.out.println(entry.getKey() + "/" + entry.getValue());
					}					
					sql = "select password AS password from "+table+" where user_code= ? order by create_date desc ";
				} catch (Exception e) {
					Log.error(e);
				}			
				rs = dbm.select(sql, conn);				
				int loopCnt=1;
				while (rs.next() && (checkNumber==0 || loopCnt<=checkNumber) ) {
					if(rs.getString("password")!=null && rs.getString("password").length() > 0 && rs.getString("password").equals(newPswd))
						hasRecord=true;	 
					loopCnt++;
				} 
				rs.close();
			} catch (SQLException e) {
				Log.error(e);
			} finally {
				dbm.clear();
			}
			return hasRecord;	
	}

	public boolean updateAudPassword(DBManager dbm, String newPassword) throws Exception {
		return updateAudPassword(dbm, newPassword, null);
	}

	public boolean updateAudPassword(DBManager dbm, String newPassword, String inUserCode) throws Exception {
		int passwordCountLimit = 20;
		boolean updateAud = false;
		String sql = "insert into aud_user_password values(?,?,?,?)";
		String userCode;
		String compCode;
		try {
			if (inUserCode == null) {
				UserPrincipalBean principal = Function.getPrincipal(request);
				compCode = principal.getCompCode();
				userCode = principal.getUserCode();
			} else {
				String selectSql = " select comp_code "
						   + " from sys_user_mst "
						   + " where user_code = " + dbm.param(inUserCode, DataType.TEXT);
				compCode = dbm.selectSingle(selectSql, "comp_code");
				userCode = inUserCode;
			}

		
			dbm.clear();
			dbm.param(compCode, DataType.TEXT);
			dbm.param(userCode, DataType.TEXT);
			dbm.param(newPassword, DataType.TEXT);
			dbm.param(new Date(Calendar.getInstance().getTimeInMillis()), DataType.DATE);
			dbm.insert(sql);
			dbm.clear();
			String sqlCount = "select COUNT(*) AS rowcount from aud_user_password where user_code = ?";
			dbm.param(compCode, DataType.TEXT);
			if (CountPasswordRecord(sqlCount, dbm) > passwordCountLimit) {
				dbm.clear();
				String sqlDelete = "DELETE FROM aud_user_password WHERE comp_code = ? and user_code = ? and password IN (SELECT password FROM aud_user_password where comp_code = ? and user_code = ? ORDER BY create_date ASC LIMIT 1)";
				dbm.param(compCode, DataType.TEXT);
				dbm.param(userCode, DataType.TEXT);
				dbm.param(compCode, DataType.TEXT);
				dbm.param(userCode, DataType.TEXT);
				dbm.param(passwordCountLimit, DataType.INT);
				dbm.delete(sqlDelete);
			}
		} catch (Exception e) {
			updateAud = false;
		} finally {
			// plz dun set dbm null
			dbm.clear();
		}
		return updateAud;
	}

	public static int CountPasswordRecord(String sql, DBManager dbm) {
		int recordCount = 0;
		ResultSet rs = null;
		Connection conn;
		try {
			conn = DBAccess.getConnection();
			rs = dbm.select(sql, conn);
			rs.next();
			int count = rs.getInt("rowcount");
			if (count > 0)
				recordCount = count;
			conn.close();
		} catch (SQLException e) {
			Log.error(e);
		} finally {
			dbm.clear();
			conn = null;
		}
		return recordCount;
	}

	public boolean updatePassword(DBManager dbm, String userCode, String newPassword, String updatedBy) {
		boolean insert = false;
		try {
			if (dbm == null)
				dbm = new DBManager();
			dbm.clear();
			String sql = "update sys_user_mst set password = ?, modify_date = sysdate, modify_by = '" + updatedBy + "' where user_code = ?";
			dbm.param(newPassword, DataType.TEXT);
			dbm.param(userCode, DataType.TEXT);
			return (dbm.update(sql) > 0);
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm.clear();
		}
		return insert;
	}

	public Response getMySettings() {
		UserPrincipalBean principal = Function.getPrincipal(request);
		JsonObject values = new JsonObject();
		Response resp = null;

		// values
		try {
			
			values.addProperty("company", principal.getCompCode());
			values.addProperty("language", principal.getLangCode());
			Log.debug("language:--->"+ principal.getLangCode());
			values.addProperty("data_001", "");
			values.addProperty("data_002", "");
			values.addProperty("data_003", "");

			// AppBar - Title
			String userName = principal.getUserName();
			Log.debug("seetings:user:--->"+userName);
			AppBarTitle appBarTitle = new AppBarTitle("/MySettings", "levelTwo", "", "MY_SETTINGS", null, userName);
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, null);
			Page pageObject = new Page("/MySettings", "");

			// create template
			List<Field> fields = new ArrayList<>();

			// company
			List<Option> options = new ArrayList<Option>();
			HashMap<String, String> map = new HashMap<String, String>();
			Companies comps = new Companies();

			List<CompanyBean> compList = comps.getCompanyListByUser(request);

			for (int j = 0; j < compList.size(); j++) {
				CompanyBean cb = compList.get(j);
				options.add(new Option(cb.getCompName(), cb.getCompCode()));
				map.put(cb.getCompCode(), cb.getCompName());
			}
			Field fieldComp = new Field("company", "radioGroup", "", options, "COMPANY.CHANGE");

			// language
			Field fieldLang = new Field("language", "radioGroup", "", getLangOptions(), "LANGUAGE.CHANGE");

			List<Field> passwordList = new ArrayList<>();

			passwordList.add(new Field("data_001", "TextField", null, null, "PASSWORD.CURRENT"));
			passwordList.add(new Field("data_002", "TextField", null, null, "PASSWORD.NEW"));
			passwordList.add(new Field("data_003", "TextField", null, null, "PASSWORD.NEW"));

			Field fieldPassword = new Field("changePassword", "txtFieldGroup", "PASSWORD.CHANGE");
			fieldPassword.setItems(passwordList);

			Field compSave = new Field("/Settings/ChangeCompany", "button", "BUTTON.SAVE");
			Field compLang = new Field("/Settings/ChangeLang", "button", "BUTTON.SAVE");
			Field compPass = new Field("/Settings/ChangePassword", "button", "BUTTON.SAVE");

			fields.add(fieldComp);
			fields.add(compSave);
			fields.add(fieldLang);
			fields.add(compLang);
			fields.add(fieldPassword);
			fields.add(compPass);

			// translation
			Template template = new Template("settings", fields, "", "My Settings");
			List<String> nameList = new ArrayList<String>();
			template.getNameCodes(nameList);
			appBar.getNameCodes(nameList);
			Map<String, String> nameMap = new HashMap<String, String>();
			nameMap = Function2.getTranslateMap(request, nameList);
			template.translate(nameMap);
			appBar.translate(nameMap);

			Content content = new Content(template, values, values);
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);
			// get template
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;
	}

	public List<Option> getLangOptions() {
		DBManager dbm = null;
		List<Option> options = new ArrayList<Option>();
		String sqlLangs = "select name_code, lookup_field from sys_lookup_mst where lookup_key = ? order by seq";
		ResultSet rs = null;
		Connection conn = null;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			dbm.param("LANGUAGE", DataType.TEXT);
			rs = dbm.select(sqlLangs, conn);
			if (rs != null) {
				while (rs.next()) {
					String langNameCode = rs.getString("name_code");
					String langCode = rs.getString("lookup_field");
					options.add(new Option(langNameCode, langCode));
				}
			}
			conn.close();
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm.clear();
			dbm = null;
			conn = null;
		}

		return options;
	}

	public MenuList getMenu(HttpSession session, String roleCode) {
		MainMenu menu = new MainMenu();
		MenuList menuList = null;
		try {
			menuList = menu.loadFromDB(request, roleCode);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		menu.putToSession(session, menuList);
		return menuList;
	}

}
