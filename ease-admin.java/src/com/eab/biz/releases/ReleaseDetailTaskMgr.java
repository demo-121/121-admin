package com.eab.biz.releases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.biz.common.CommonManager;
import com.eab.biz.tasks.TaskReleaseMgr;
import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Token;
import com.eab.dao.releases.ReleasesDetailTask;
import com.eab.dao.tasks.Tasks;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.AssignContent;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.ListContent;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.SearchCondition;
import com.eab.json.model.Template;
import com.eab.json.model.Field.Types;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class ReleaseDetailTaskMgr {
	
	HttpServletRequest request;
	HttpSession session;
	UserPrincipalBean principal;
	Language langObj;
	
	public ReleaseDetailTaskMgr(HttpServletRequest request) {
		super();
		this.request = request;
		this.session = request.getSession();
		this.principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		this.langObj = new Language(principal.getUITranslation());
	}
	
	public String getJson(String criteria, List<Object> rows, String sortBy, String sortDir, int recordStart, int pageSize) throws Exception {		
		ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
        if (recordStart != 0) 
            list = (ArrayList<Map<String, Object>>) session.getAttribute(Constants.SessionKeys.SEARCH_RESULT);
          
        //Get list
        if (list.size() == 0) {			
        	list = this.getList(criteria, sortBy, sortDir);

        	//Add result to session for "More" button use
        	session.setAttribute(Constants.SessionKeys.SEARCH_RESULT, list);
        }
		        
		//Get template
        List<Field> item = this.getTemplate();
		
        String action = "";
        
		//Action
		if (sortDir != "") {
			//Page content change
			action =  Constants.ActionTypes.ASSIGN_PAGE;
		} else {
			//First time load
			action =  Constants.ActionTypes.ASSIGN_PAGE;
		}


	    //Token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());

	    //Template
		Template template = new Template();
		template.setItems(item);
		
		//More
	    int offset = 0;
	    int size = 10;	  
	    
	    List<Object> shortedList = new ArrayList<>();
		offset = recordStart;
		 
		size = pageSize != 0 ? pageSize : Constants.SEARCH_RESULT_DEFAULT_PAGE_SIZE;
	    		
		if (list != null) {
			for (int j = offset; j < offset + size && j < list.size(); j++) {
				shortedList.add(list.get(j));
			}
		}
		
	    //Content - values
	    ListContent assignContentValues = new ListContent(shortedList, list != null ? list.size(): 0, offset > 0, rows);
	    	    
	    //Content
	    AssignContent assignContent = new AssignContent(template, assignContentValues, AssignContent.SelectTypes.MULTI_SELECTABLE);
        	
	    //AppBar - Title
	    //First Level
	    AppBarTitle assignAppBarTitle = new AppBarTitle(null, langObj.getNameDesc("TASK_APPBAR_SEARCH").toString(), null, null);
			    	
	    //Action	    
		Field btnAssignOk = new Field("/Releases/Detail/Task/Assign", "submitButton", langObj.getNameDesc("BUTTON.OK").toString());
		Field btnAssignCancel = new Field("confirm_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL").toString());
		Dialog confirmSuspendDialog = new Dialog("confirmSuspendDialog", langObj.getNameDesc("TASK_ASSIGN").toString(), langObj.getNameDesc("RELEASE_CONFIRM_ASSIGN").toString(), btnAssignOk, btnAssignCancel, 30);  
	    Field appAssign = new Field("A", "dialogButton", langObj.getNameDesc("BUTTON.ASSIGN").toString(), confirmSuspendDialog);
	    	   
		List<Field> appAction1 = new ArrayList<>();
		appAction1.add(appAssign);
		
	    //AppBar - Actions	    
	    List<List<Field>> AssignAppBarActions = new ArrayList<>();
	    AssignAppBarActions.add(null);//No default action item
	    AssignAppBarActions.add(appAction1);
	    
	    //AppBar
		AppBar assignAppBar = new AppBar(assignAppBarTitle, AssignAppBarActions);
		SearchCondition cond = SearchCondition.RetrieveSearchCondition(request);
		assignAppBar.setValues(cond);
			    
		//Page
		JsonObject pageValue = new JsonObject();
		pageValue.addProperty("criteria", criteria);
		//pageValue.add("taskList", JsonConverter.convertObj(taskList));
		//pageValue.addProperty("assignLink", assignLink);
		pageValue.addProperty("sortBy", sortBy);
		pageValue.addProperty("sortDir", sortDir);
		pageValue.addProperty("recordStart", recordStart);
		pageValue.addProperty("pageSize", pageSize);
	    Page page = new Page("/Releases/Detail/Task", "", pageValue);
		
		//Convert to Json  
		Gson gson = new GsonBuilder().create();
		String outputJSON = gson.toJson(new Response(action, page, tokenID, tokenKey, null, null, assignAppBar, assignContent));

		return outputJSON;
	}
	
	
	public ArrayList<Map<String, Object>> getList(String criteria, String sortBy, String sortDir) throws Exception {		
		ReleasesDetailTask ReleasesDetailTaskDAO = new ReleasesDetailTask();
		return ReleasesDetailTaskDAO.getUnassignedTaskList(request, principal, criteria, sortBy, sortDir, langObj);
	}
	
	public List<Field> getTemplate() throws Exception { 
		List<String> List = Arrays.asList("TYPE", "TASK_CODE_NAME_VER", "DESCRIPTION", "REF", "EFF_DATE");
		List<String> ListType = Arrays.asList("text", "text", "text", "text", "text");
		List<String> ListId = Arrays.asList("type", "item_code", "task_desc", "ref_no", "effDate");
		List<String> ListWidth = Arrays.asList("100px", "auto", "auto", "150px", "200px");

		int seq = 0;
		List<Field> template = new ArrayList<>();

		for(String label: List) {
			Field field1 = new Field(ListId.get(seq),langObj.getNameDesc(label),ListType.get(seq), seq+1, seq+1,ListWidth.get(seq));
			template.add(field1);

			seq ++;
		}

		return template;
	}
	
	public Response AddTask(int releaseID, List<Object> keyList) throws Exception {
		Tasks taskDAO = new Tasks();		
		taskDAO.assignTasks(releaseID, keyList, principal);
		
		ReleaseDetailMgr releaseDetail = new ReleaseDetailMgr(request);
		return releaseDetail.getJson(Integer.toString(releaseID), true, "", "", 0, 0, false);
	}
	
	public Response UnassignTask(int releaseID, List<Object> taskList) throws Exception {
		Tasks taskDAO = new Tasks();		
		taskDAO.unassignTasks(taskList, principal);
				
		ReleaseDetailMgr releaseDetail = new ReleaseDetailMgr(request);
		return releaseDetail.getJson(Integer.toString(releaseID), true, "", "", 0, 0, false);
	}	
	
	public Response SuspendTask(int releaseID, List<Object> taskList) throws Exception {
		Tasks taskDAO = new Tasks();		
		taskDAO.suspendTasks(taskList, principal);
		
		ReleaseDetailMgr releaseDetail = new ReleaseDetailMgr(request);
		return releaseDetail.getJson(Integer.toString(releaseID), true, "", "", 0, 0, false);
	}		
	
	public String ReassignTask(int releaseID, List<Object> taskList) throws Exception {
		Tasks taskDAO = new Tasks();				
		taskDAO.reassignTasks(taskList, principal);

		TaskReleaseMgr taskRelease = new TaskReleaseMgr(request);
		return taskRelease.getJson("", taskList, "", "", 0 , 10, "/Releases/Detail/Release/Assign");
	}		
	
	public List<Object> getTaskList(int releaseID) throws Exception {	
		Tasks taskDAO = new Tasks();		
		return taskDAO.getTaskList(principal, releaseID);
	}	
	
}
