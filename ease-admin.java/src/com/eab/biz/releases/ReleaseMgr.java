package com.eab.biz.releases;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;













import com.eab.biz.common.CommonManager;
import com.eab.biz.dynamic.LockManager;
import com.eab.common.Constants;
import com.eab.common.Function2;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.common.Functions;
import com.eab.dao.dynamic.RecordLock;
import com.eab.dao.environment.Environment;
import com.eab.dao.releases.ReleaseDetail;
import com.eab.dao.releases.Releases;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.DialogItem;
import com.eab.json.model.Field;
import com.eab.json.model.ListContent;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.SearchCondition;
import com.eab.json.model.Template;
import com.eab.model.dynamic.RecordLockBean;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ReleaseMgr {

	HttpServletRequest request;
	HttpSession session;
	UserPrincipalBean principal;
	Language langObj;

	public ReleaseMgr(HttpServletRequest request) {
		super();
		this.request = request;
		this.session = request.getSession();
		this.principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		this.langObj = new Language(principal.getUITranslation());
	}
	
	public Response getJson() throws Exception{
		return getJson("", "", 0, Constants.SEARCH_RESULT_DEFAULT_PAGE_SIZE ,"" , "", "", "", "", null);
	}
	public Response getJson(Dialog inDialog) throws Exception{
		return getJson("", "", 0, Constants.SEARCH_RESULT_DEFAULT_PAGE_SIZE ,"" , "", "", "", "", inDialog);
	}

	public Response getJson(String criteria, String statusFilter,int recordStart, int pageSize, String sortBy, String sortDir, String releaseName, String targetDate, String remarks, Dialog inDialog) throws Exception {
		ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		if (recordStart != 0)
			list = (ArrayList<Map<String, Object>>) session.getAttribute(Constants.SessionKeys.SEARCH_RESULT);

		// Get list
		if (list.size() == 0 ) {
			list = this.getList(criteria, statusFilter, sortBy, sortDir);

			// Add result to session for "More" button use
			session.setAttribute(Constants.SessionKeys.SEARCH_RESULT, list);
		}

		// Get template
		List<Field> item = this.getTemplate();

		String action = "";

		// Action
		CommonManager cMgr = new CommonManager(request);
		boolean isChangePage = cMgr.isChangePage();
		if ((criteria != "" || statusFilter != "" || sortBy != "") && !isChangePage) {
			// Page content
			action = Constants.ActionTypes.CHANGE_CONTENT;
		} else {
			// First time load
			action = Constants.ActionTypes.CHANGE_PAGE;
		}

		// Page
		JsonObject pageValue = new JsonObject();
		pageValue.addProperty("criteria", criteria);
		//pageValue.addProperty("recordStart", recordStart);
		pageValue.addProperty("pageSize", pageSize);
		pageValue.addProperty("sortBy", sortBy);
		pageValue.addProperty("sortDir", sortDir);
		pageValue.addProperty("releaseName", releaseName);
		pageValue.addProperty("targetDate", targetDate);
		pageValue.addProperty("remarks", remarks);
		Page page = new Page("/Releases", langObj.getNameDesc("MENU.RELEASES").toString(), pageValue);

		// Token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());

		// Template
		Template template = new Template();
		template.setItems(item);

		// More
		int offset = 0;
		int size = 10;

		List<Object> shortedList = new ArrayList<>();
		 
		offset = recordStart;
		 
		size = pageSize != 0 ? pageSize : Constants.SEARCH_RESULT_DEFAULT_PAGE_SIZE;
						
		if (list != null) {
			for (int j = offset; j < offset + size && j < list.size(); j++) {
				shortedList.add(list.get(j));
			}
		}
		  

		//Content - values
		ListContent contentValues = new ListContent(shortedList, list != null ? list.size(): 0, offset > 0);

		//Content
		Content content = new Content(template, contentValues);

		//AppBar
		AppBar appBar = getAppBar();

		//Convert to Json  
		Gson gson = new GsonBuilder().create();
		
		Response response = new Response(action, null, page, tokenID, tokenKey, appBar, content);

		if(inDialog != null)
			response.setDialog(inDialog);
		
	    return response;
	}

	public AppBar getAppBar() throws Exception {
		// AppBar - Title
		Functions funcDao = new Functions();
		AppBarTitle appBarTitle = new AppBarTitle("/Releases", AppBarTitle.Types.LABEL, null, funcDao.getFunction("/Releases", request), null, null);
		
		// AppBar - Release Status Filter
		List<Option> appBarAction1_Option = new ArrayList<>();
		appBarAction1_Option = Function2.getOptionsByLookupKey(request, "REL_STATUS_FILTER", langObj);

		Field appBarAction1_1 = new Field("statusFilter", "picker", "E", null,appBarAction1_Option);

		// AppBar - Search
		Field appBarAction1_2 = new Field("searchString", "searchButton",langObj.getNameDesc("TASK_RELEASE_APPBAR_SEARCH").toString(), langObj.getNameDesc("SEARCH").toString(), null);

		String strOK = langObj.getNameDesc("BUTTON.OK").toString();
		String strCancel = langObj.getNameDesc("BUTTON.CANCEL").toString();

		// AppBar - Add
		Field btnOk = new Field("/Releases/Add", "submitButton", langObj.getNameDesc("BUTTON.ADD").toString());
		Field btnCancel = new Field("confirm_cancel", "button", strCancel);

		DialogItem txtReleaseName = new DialogItem("releaseName",DialogItem.Types.TEXT, "", true, "", langObj.getNameDesc("RELEASE_NAME").toString(), 50);
		DialogItem txtReleaseDate = new DialogItem("targetDate",DialogItem.Types.DATEPICKER, 0, true, "", langObj.getNameDesc("TARGET_REL_DATE").toString(), this.principal.getDateFormat());
		DialogItem txtRemarks = new DialogItem("remark", DialogItem.Types.TEXT,"", false, "" , langObj.getNameDesc("REMARK").toString(), 100);
		List<Field> dialogItems = new ArrayList<>();
		dialogItems.add(txtReleaseName);
		dialogItems.add(txtReleaseDate);
		dialogItems.add(txtRemarks);

		Dialog newReleaseDialog = new Dialog("confirm", langObj.getNameDesc("NEW_RELEASE").toString(), "", btnOk, btnCancel, dialogItems, 30);
		Field appBarAction1_3 = new Field("btnAdd", "iconDialogButton", langObj.getNameDesc("BUTTON.ADD").toString(), newReleaseDialog);
		appBarAction1_3.setValue("add");

		// AppBar - Add actions
		List<Field> appBarAction1 = new ArrayList<>();

	    appBarAction1.add(appBarAction1_1);
	    appBarAction1.add(appBarAction1_2);
	    appBarAction1.add(appBarAction1_3);
	    	    
	    //Define actions - S
		List<Option> testENVOptions = new ArrayList<>();
		ReleaseDetail releaseDAO = new ReleaseDetail();
	    
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Test Button
		Field testENVPositive = new Field("/Releases/Test", "submitButton", strOK);
		Field testENVNegative = new Field("test_cancel", "button", strCancel);
		
		ArrayList<Map<String , Object>> testENVList = releaseDAO.getTestENVList(request, principal);
		
		if(testENVList != null){
			for(Map<String, Object>testENV : testENVList){
				testENVOptions.add(new Option(testENV.get("envName").toString(), testENV.get("envID").toString()));
			}		
		}
		
		List<Field> testENVDialogItems = new ArrayList<>();
		DialogItem cdi_radioButton = new DialogItem("radio_ENV_Main", DialogItem.Types.RADIOBUTTON, null, true, testENVOptions);
		testENVDialogItems.add(cdi_radioButton);
		
		Dialog testENVDialog = new Dialog("testENVDialog", langObj.getNameDesc("SELECT_TEST_ENV").toString(), "", testENVPositive, testENVNegative, testENVDialogItems, 30);
//TODO: john: alert user when base version diff found		        
//		Field baseDiffNegative = new Field("", "button", strOK);
//		List<Field> baseDiffDialogItems = new ArrayList<>();        
//		Dialog baseDiffDialog = new Dialog("baseDiffDialog", langObj.getNameDesc("DIFFERENT_VERSION"), "", null, baseDiffNegative, baseDiffDialogItems, 30);		 
//		Field appTestBaseDiff = new Field("baseDiff", "dialogButton", langObj.getNameDesc("TEST").toString(), baseDiffDialog);
//		Field appReleaseBaseDiff = new Field("baseDiff", "dialogButton", langObj.getNameDesc("RELEASE").toString(), baseDiffDialog);

		Field appTest = new Field("/Releases/Detail/Test", "dialogButton", langObj.getNameDesc("TEST").toString(), testENVDialog);
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Release Button
		Field releasePositive = new Field("/Releases/Schedule", "submitButton", strOK);
		Field releaseNegative = new Field("release_cancel", "button", strCancel);
		
		List<Field> releaseDialogItems = new ArrayList<>();
		
		DialogItem di_ReleaseDate = new DialogItem("scheduleDate_Main", DialogItem.Types.DATEPICKER, null, true, langObj.getNameDesc("DATE").toString(), langObj.getNameDesc("DATE").toString(), 100);
		DialogItem di_ReleaseTime = new DialogItem("scheduleTime_Main", DialogItem.Types.TIMEPICKER, null, true, langObj.getNameDesc("TIME").toString(), langObj.getNameDesc("TIME").toString(), 100);
		releaseDialogItems.add(di_ReleaseDate);
		releaseDialogItems.add(di_ReleaseTime);
	

		Dialog releaseDialog = new Dialog("releaseDialog", langObj.getNameDesc("SCHEDULE_RELEASE").toString(), "", releasePositive, releaseNegative, releaseDialogItems, 30);
		Field appRelease = new Field("/Releases/Schedule", "dialogButton", langObj.getNameDesc("RELEASE").toString(), releaseDialog);
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	    //Reschedule
		Field reschedulePositive = new Field("/Releases/Schedule", "submitButton", strOK);
		Field rescheduleNegative = new Field("release_cancel", "button", strCancel);
		
		List<Field> rescheduleDialogItems = new ArrayList<>();
						
		DialogItem di_OriginReleaseLabel = new DialogItem("rescheduleLabel_Org", DialogItem.Types.LABEL, langObj.getNameDesc("RELEASE.RESCHEDULE.LABEL.ORIGIN").toString(), 1, 0);
		DialogItem di_OriginReleaseDate = new DialogItem("rescheduleDate_Org", DialogItem.Types.TEXT, "Date", false, "", langObj.getNameDesc("DATE").toString(), 100);
		DialogItem di_OriginReleaseTime = new DialogItem("rescheduleTime_Org", DialogItem.Types.TEXT, "Time", false, "", langObj.getNameDesc("TIME").toString(), 100);
		DialogItem di_ReleaseLabel = new DialogItem("rescheduleLabel", DialogItem.Types.LABEL, langObj.getNameDesc("RELEASE.RESCHEDULE.LABEL.NEW").toString(), 1, 0);
		DialogItem di_RescheduleDate = new DialogItem("scheduleDate_Main", DialogItem.Types.DATEPICKER, null, true, langObj.getNameDesc("DATE").toString(), langObj.getNameDesc("DATE").toString(), 100);
		DialogItem di_RescheduleTime = new DialogItem("scheduleTime_Main", DialogItem.Types.TIMEPICKER, null, true, langObj.getNameDesc("TIME").toString(), langObj.getNameDesc("TIME").toString(), 100);
		di_OriginReleaseDate.setDisabled(true);
		di_OriginReleaseTime.setDisabled(true);
		rescheduleDialogItems.add(di_OriginReleaseLabel);
		rescheduleDialogItems.add(di_OriginReleaseDate);
		rescheduleDialogItems.add(di_OriginReleaseTime);	
		rescheduleDialogItems.add(di_ReleaseLabel);
		rescheduleDialogItems.add(di_RescheduleDate);
		rescheduleDialogItems.add(di_RescheduleTime);

		Dialog rescheduleDialog = new Dialog("releaseDialog", langObj.getNameDesc("SCHEDULE_RELEASE").toString(), "", reschedulePositive, rescheduleNegative, rescheduleDialogItems, 30);
		Field appReschedule = new Field("/Releases/Schedule", "dialogButton", langObj.getNameDesc("RESCHEDULE").toString(), rescheduleDialog);
	    
	    //Export Note
	    Field appExportNote = new Field("/Releases", "submitSelectedButton", langObj.getNameDesc("EXPORT_NOTE").toString());
	    	    
	    //Abort
		Field btnAbortOk = new Field("/Releases/Abort", "submitButton", strOK);
		Field btnAbortCancel = new Field("confirm_cancel", "button", strCancel);
		com.eab.json.model.Dialog confirmAbortDialog = new com.eab.json.model.Dialog("confirmAbortDialog", langObj.getNameDesc("RELEASE_ABORT").toString(),langObj.getNameDesc("RELEASE_CONFIRM_ABORT").toString(), btnAbortOk,btnAbortCancel, 30);
		Field appAbort = new Field("A", "dialogButton", langObj.getNameDesc("ABORT").toString(), confirmAbortDialog);

		// Delete
		Field btnDeleteOk = new Field("/Releases/Delete", "submitButton", strOK);
		Field btnDeleteCancel = new Field("confirm_cancel", "button", strCancel);
		com.eab.json.model.Dialog confirmDeleteDialog = new com.eab.json.model.Dialog("confirmDeleteDialog",langObj.getNameDesc("RELEASE_DELETE").toString(), langObj.getNameDesc("RELEASE_CONFIRM_DELETE").toString(), btnDeleteOk,btnDeleteCancel, 30);
		Field appDelete = new Field("D", "dialogButton", langObj.getNameDesc("DELETE").toString(), confirmDeleteDialog);
		// Define actions - E

		// Release status = Pending
		List<Field> appBarAction2 = new ArrayList<>();
		appBarAction2.add(appTest);
		appBarAction2.add(appRelease);
		appBarAction2.add(appDelete);
		//appBarAction2.add(appExportNote);//TODO next phase
	    
	    //Release status = Testing
		List<Field> appBarAction3 = new ArrayList<>();
		appBarAction3.add(appTest);
		appBarAction3.add(appRelease);
		appBarAction3.add(appDelete);
		//appBarAction3.add(appExportNote);//TODO next phase
	    
	    //Release status = Scheduled
		List<Field> appBarAction4 = new ArrayList<>();
		appBarAction4.add(appReschedule);
		appBarAction4.add(appAbort);
		//appBarAction4.add(appExportNote);//TODO next phase
	    
	    //Release status = Released
		List<Field> appBarAction5 = new ArrayList<>();
		//appBarAction5.add(appExportNote);//TODO next phase
		
	    //Release status = Pending + Testing
		List<Field> appBarAction6 = new ArrayList<>();
		appBarAction6.add(appDelete);
		//appBarAction6.add(appExportNote);
		
		List<Field> appBarAction7 = new ArrayList<>();
		appBarAction7.add(appDelete);
	    
		//Base Version is Different
//		List<Field> appBarAction8 = new ArrayList<>();
//		appBarAction8.add(appTestBaseDiff);
//		appBarAction8.add(appReleaseBaseDiff);
//		appBarAction8.add(appDelete);

	    //AppBar - Actions	    
	    List<List<Field>> appBarActions = new ArrayList<>();
	    appBarActions.add(appBarAction1);
	    appBarActions.add(appBarAction2);
	    appBarActions.add(appBarAction3);
	    appBarActions.add(appBarAction4);
	    appBarActions.add(appBarAction5);
	    appBarActions.add(appBarAction6);
	    appBarActions.add(appBarAction7);
	    //appBarActions.add(appBarAction8);//TODO:alert user if base version diff is found

	    AppBar appBar = new AppBar(appBarTitle, appBarActions);

		SearchCondition cond = SearchCondition.RetrieveSearchCondition(request);
		appBar.setValues(cond);

		return appBar;
	}

	public ArrayList<Map<String, Object>> getList(String searchString, String statusFilter, String sortBy, String sortDir)throws Exception {
		Releases releaseDAO = new Releases();
		return releaseDAO.getReleaseList(request, principal, searchString, statusFilter, sortBy, sortDir);
	}

	public List<Field> getTemplate() throws Exception {
		List<String> List = Arrays.asList("RELEASE_ID", "RELEASE_NAME", "STATUS");
		List<String> ListType = Arrays.asList("text", "text", "text");
		List<String> ListId = Arrays.asList("release_ID", "release_Name", "status");
		List<String> ListWidth = Arrays.asList("150px", "auto", "450px");

		int seq = 0;
		List<Field> template = new ArrayList<>();

		for (String label : List) {			
			if(ListId.get(seq).equals("status")){
				Field field1 = new Field(ListId.get(seq), langObj.getNameDesc(label),ListType.get(seq), seq + 1, seq + 1, ListWidth.get(seq), "tooltipId");
				field1.setIcon("font_icon");
				template.add(field1);				
			}else{
				Field field1 = new Field(ListId.get(seq), langObj.getNameDesc(label),ListType.get(seq), seq + 1, seq + 1, ListWidth.get(seq));
				template.add(field1);				
			}
			
			seq++;
		}

		return template;
	}

	public Response addRelease(String releaseName, Date targetDate, String remarks)throws Exception {
		Releases releaseDAO = new Releases();
		String releaseID = releaseDAO.addRelease(principal, releaseName, targetDate, remarks);

		ReleaseDetailMgr release = new ReleaseDetailMgr(request);
		return release.getJson(releaseID);
	}

	public Response abortRelease(List<Object> releaseIDList) throws Exception {		
		//Handle record lock
		RecordLock rlD = new RecordLock();
		RecordLockBean rlbD = null;
		Dialog dialog = null;
		boolean isRecordLocked = false;
		
		for(int i = 0 ; i < releaseIDList.size(); i++){			
			rlbD = rlD.newRecordLockBean("/Releases/Detail", (String) releaseIDList.get(i));
			rlD.getRecordLock(request, rlbD);	
			
			if(!rlbD.canAccess()){
				LockManager lm = new LockManager(request);
				dialog = lm.lockDialog(rlbD);
				isRecordLocked = true;
				break;
			}
		}
		
		if (!isRecordLocked){			
			Releases releaseDAO = new Releases();
			releaseDAO.abortRelease(releaseIDList, principal);
		}
		
		return getJson("", "", 0, 0, "", "", "", "", "", dialog);
	}

	public Response deleteRelease(List<Object> releaseIDList) throws Exception {		
		//Handle record lock
		RecordLock rlD = new RecordLock();
		RecordLockBean rlbD = null;
		Dialog dialog = null;
		boolean isRecordLocked = false;
		
		for(int i = 0 ; i < releaseIDList.size(); i++){
			rlbD = rlD.newRecordLockBean("/Releases/Detail", (String) releaseIDList.get(i));
			rlD.getRecordLock(request, rlbD);	
			
			if(!rlbD.canAccess()){
				LockManager lm = new LockManager(request);
				dialog = lm.lockDialog(rlbD);
				isRecordLocked = true;
				break;
			}
		}
		
		if (!isRecordLocked){			
			Releases releaseDAO = new Releases();
			releaseDAO.deleteRelease(releaseIDList, principal);
		}
		
		return getJson("", "", 0, 0, "", "", "", "", "", dialog);
	}
}
	



        

