package com.eab.biz.releases;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.Blob;
import java.sql.Clob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.JSONArray;
import org.json.JSONObject;

import sun.misc.BASE64Decoder;

import com.eab.biz.dynamic.CompanyManager;
import com.eab.common.Constants;
import com.eab.common.CryptoUtil;
import com.eab.common.Function2;
import com.eab.common.ImageHelper;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.couchbase.CBServer;
import com.eab.dao.dynTemplate.DynData;
import com.eab.dao.environment.Environment;
import com.eab.dao.products.Products;
import com.eab.dao.releases.ReleasePublish;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Response;
import com.eab.model.dynTemplate.DynUI;
import com.eab.model.products.CashAndBonusInfoObj;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.system.SysParamBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class ReleasePublishMgr {

	HttpServletRequest request;
	HttpSession session;
	UserPrincipalBean principal;
	Language langObj;

	public ReleasePublishMgr(HttpServletRequest request) {
		this.request = request;
		this.session = request.getSession();
		this.principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		this.langObj = new Language(principal.getUITranslation());
	}

	public ReleasePublishMgr() {
		super();
	}

	public Response ScheduleRelease(String releaseID, String scheduleDate, boolean isMainPage) throws Exception {
		ReleasePublish publishdao = new ReleasePublish();

		HttpSession session = request.getSession();
		UserPrincipalBean principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		Language langObj = new Language(principal.getUITranslation());

		Calendar c = Calendar.getInstance();
		c.setTime(new Date());

		Function2 func2 = new Function2();
		Map<String, SysParamBean> sysParamList = func2.getSysParam();

		int RELEASE_SCHEDULE_TIME = 2;// By default 2 hours
		String RELEASE_SCHEDULE_UNIT = "H";// By default hour

		if (sysParamList.containsKey("RELEASE_SCHEDULE_HOURS")) {
			RELEASE_SCHEDULE_TIME = Integer.parseInt(sysParamList.get("RELEASE_SCHEDULE_HOURS").getParamValue());
			RELEASE_SCHEDULE_UNIT = "H";// Hour

			c.add(Calendar.HOUR, RELEASE_SCHEDULE_TIME);
		} else if (sysParamList.containsKey("RELEASE_SCHEDULE_MINUTES")) {
			RELEASE_SCHEDULE_TIME = Integer.parseInt(sysParamList.get("RELEASE_SCHEDULE_MINUTES").getParamValue());
			RELEASE_SCHEDULE_UNIT = "M";// Minute

			c.add(Calendar.MINUTE, RELEASE_SCHEDULE_TIME);
		} else {
			c.add(Calendar.HOUR, RELEASE_SCHEDULE_TIME);// Default
		}

		Date nowDate = c.getTime();

		String tempScheduleDate = scheduleDate.replace("T", " ");
		tempScheduleDate = tempScheduleDate.substring(0, 19);

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d = format.parse(tempScheduleDate);

		if (d.compareTo(nowDate) > 0) {
			publishdao.UpdateScheduleRelease(principal, releaseID, scheduleDate);

			if (isMainPage) {
				ReleaseMgr release = new ReleaseMgr(request);
				return release.getJson();
			} else {
				// Get Json from releaseDetail manager
				ReleaseDetailMgr releaseDetail = new ReleaseDetailMgr(request);
				return releaseDetail.getJson(releaseID);
			}
		} else {
			// Create Error Dialog
			Field negative = new Field("ok", "button", langObj.getNameDesc("BUTTON.OK"));
			Dialog dialog;

			if (RELEASE_SCHEDULE_UNIT.equals("H"))// Hours
				dialog = new Dialog("clone_completed", langObj.getNameDesc("ERROR"), langObj.getNameDesc("RELEASE.SCHEDULE.DATE.INVALID_A") + " " + String.valueOf(RELEASE_SCHEDULE_TIME) + langObj.getNameDesc("RELEASE.SCHEDULE.DATE.INVALID_B"), null, negative, 30);
			else // Minutes
				dialog = new Dialog("clone_completed", langObj.getNameDesc("ERROR"), langObj.getNameDesc("RELEASE.SCHEDULE.DATE.INVALID_A") + " " + String.valueOf(RELEASE_SCHEDULE_TIME) + langObj.getNameDesc("RELEASE.SCHEDULE.DATE.INVALID_C"), null, negative, 30);

			if (isMainPage) {
				// Get Json from release manager
				ReleaseMgr release = new ReleaseMgr(request);
				return release.getJson(dialog);
			} else {
				// Get Json from releaseDetail manager
				ReleaseDetailMgr releaseDetail = new ReleaseDetailMgr(request);
				return releaseDetail.getJson(releaseID, dialog);
			}
		}
	}

	// For upload to production environment
	public void productionRelease() throws Exception {
		ReleasePublish publishdao = new ReleasePublish();
		ReleaseDetailMgr release = new ReleaseDetailMgr();

		String _RELEASE_ID = "";
		String _COMP_CODE = "";

		// Get Scheduled Release List
		List<Map<String, Object>> scheduledLists = publishdao.getScheduledList();

		try {
			int releaseProcessStatus = -1;

			// Get Production Config
			List<Map<String, Object>> prodENVData = publishdao.getProductionENVData();

			String envID = "";
			Products prodDAO = new Products();

			for (Map<String, Object> scheduledList : scheduledLists) {
				_RELEASE_ID = scheduledList.get("RELEASE_ID").toString();
				_COMP_CODE = scheduledList.get("COMP_CODE").toString();

				/////////////////////////////////////////////////////////////////////////////////////////////
				// Step 1: Get sync gateway detail - S
				/////////////////////////////////////////////////////////////////////////////////////////////
				CBServer cbServer = null;

				for (Map<String, Object> prodENV : prodENVData) {
					if (prodENV.get("COMP_CODE").toString().equals(_COMP_CODE)) {
						CryptoUtil crypto = new CryptoUtil();
						String password = crypto.decryptUserData(prodENV.get("COMP_CODE").toString(), prodENV.get("DB_PASSWORD").toString());

						cbServer = new CBServer(prodENV.get("HOST").toString(), prodENV.get("PORT").toString(), prodENV.get("DB_NAME").toString(), prodENV.get("DB_LOGIN").toString(), password);
						envID = prodENV.get("ENV_ID").toString();
						break;
					}
				}
				/////////////////////////////////////////////////////////////////////////////////////////////
				// Step 1: Get sync gateway detail - E
				/////////////////////////////////////////////////////////////////////////////////////////////

				if (cbServer == null) {
					throw new Exception("Server Connection Fail");
				} else {
					/////////////////////////////////////////////////////////////////////////////////////////////
					// Step 2: Check sync gateway - S
					/////////////////////////////////////////////////////////////////////////////////////////////

					if (cbServer.checkDBConnection() == false)
						throw new Exception("Server Connection Fail");
					/////////////////////////////////////////////////////////////////////////////////////////////
					// Step 2: Check sync gateway - E
					/////////////////////////////////////////////////////////////////////////////////////////////

					List<Map<String, Object>> taskLists = publishdao.getTaskList(_COMP_CODE, _RELEASE_ID);

					// Mark Release to Publishing
					release.UpdateStatus(_RELEASE_ID, "L", _COMP_CODE);

					// Update Task list
					for (Map<String, Object> list : taskLists) {
						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
						long mills = (dateFormat.parse(list.get("CREATE_DATE").toString())).getTime();
						String funcCode = list.get("FUNC_CODE").toString();

						switch (funcCode) {
//							case "FN.PRO.PRODS":// Products
//								JSONObject prodJson = prepareProductJson(list, _COMP_CODE, cbServer, publishdao, mills);
//	
//								prodDAO.updateProdLaunchStatus(list);
//	
//								if (!publishdao.UpdateProductPublish(_COMP_CODE, list.get("ITEM_CODE").toString(), list.get("VER_NO").toString(), prodJson.toString(), prodJson.get("covCode").toString()))
//									throw new Exception("Update Publish Record Failed");
//	
//								break;
							case "FN.NEEDS":// Needs
								JSONObject needJson = prepareNeedJson(list, publishdao, _COMP_CODE, cbServer);
	
								if (!publishdao.UpdateNeedsPublish(_COMP_CODE, list.get("ITEM_CODE").toString(), list.get("VER_NO").toString(), needJson.toString(), list.get("ITEM_CODE").toString(), list.get("CHANNEL_CODE").toString()))
									throw new Exception("Update Publish Record Failed");
								break;
							default:
								JSONObject prodJson = prepareDynJson(list, _COMP_CODE, cbServer, publishdao, mills);
								DynData dynData = new DynData();
								dynData.updateLaunchStatus(list);
								//if (!publishdao.UpdateDynPublish(_COMP_CODE, list.get("ITEM_CODE").toString(), list.get("VER_NO").toString(), prodJson.toString(), prodJson.get("covCode").toString()))
//									throw new Exception("Update Publish Record Failed");
						}
						
						//Publish product line json
						prepareProductLineJson(_COMP_CODE, cbServer);
					}
					
					// Update release and task status to "Released"
					release.UpdateStatus(_RELEASE_ID, "R", _COMP_CODE);

					if (!publishdao.AddHistory(_RELEASE_ID, "PROD", "Y", "", "SYSTEM"))
						throw new Exception("Update History Fail");

					pushNotifyMessage(envID, _COMP_CODE, "System");
					envID = "";
				}
			}
		} catch (Exception e) {
			Log.error(e);

			release.UpdateStatus(_RELEASE_ID, "F", _COMP_CODE);
			publishdao.AddHistory(_RELEASE_ID, "PROD", "N", e.toString(), "SYSTEM");
		}
	}

	// For upload to testing environment
	public boolean testRelease(String releaseID, String evnID, String compCode, String userCode) throws Exception {
		UserPrincipalBean principal = new UserPrincipalBean();

		if (request != null) {
			HttpSession session = request.getSession();
			
			if (session != null)
				principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		}

		if (principal == null)
			principal = new UserPrincipalBean();

		if (compCode != null && !compCode.equals(""))
			principal.setCompCode(compCode);

		if (userCode != null && !userCode.equals(""))
			principal.setUserCode(userCode);

		ReleasePublish publishdao = new ReleasePublish();
		CBServer cbServer = null;
		ReleaseDetailMgr release = new ReleaseDetailMgr();

		try {
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 1: Get sync gateway detail then connect to Sync gateway - S
			/////////////////////////////////////////////////////////////////////////////////////////////
			Log.info("----------------------Step 2.0 - Connect to Sync Gateway " + evnID + " - Start");
			
			List<Map<String, Object>> envData = publishdao.getENVData(principal, evnID);

			// Create cbServer config
			CryptoUtil crypto = new CryptoUtil();
			String dbPassword = crypto.decryptUserData(compCode, envData.get(0).get("dbPassword").toString());
			cbServer = new CBServer(envData.get(0).get("host").toString(), envData.get(0).get("port").toString(), envData.get(0).get("dbName").toString(), envData.get(0).get("dbLogin").toString(), dbPassword);

			// Check DB connection
			if (cbServer.checkDBConnection() == false) {
				Log.info("----------------------Step 2.1(Error) - Failed to connect Sync Gateway");
				throw new Exception("Please check couchbase sync gateway connection.");
			}
				
			Log.info("----------------------Step 2.2 - Connect to Sync Gateway " + evnID + " - End");
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 1: Get sync gateway detail then connect to Sync gateway - E
			/////////////////////////////////////////////////////////////////////////////////////////////

			
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 2: Get all product json files from sync gateway - S
			/////////////////////////////////////////////////////////////////////////////////////////////
			Log.info("----------------------Step 3.1 - Get all product json files from Sync gateway - Start");
			
			String startKey = "EAB_PRODUCT_0000000_0000000000000";
			String endKey = "EAB_PRODUCT_ZZZZZZZZZ_ZZZZZZZZZZZ";

			// Get current product List
			String temp = cbServer.getListByKey(startKey, endKey);
			JSONObject serverDocListsObject = new JSONObject();
			ArrayList<String> serverDocLists = new ArrayList<String>();

			if (temp != null) {
				serverDocListsObject = new JSONObject(temp);
				JSONArray jsonArray = (JSONArray) serverDocListsObject.get("rows");

				if (jsonArray != null) {
					int len = jsonArray.length();

					for (int i = 0; i < len; i++) {
						serverDocLists.add(((JSONObject) jsonArray.get(i)).get("id").toString());
					}
				}
			}
			
			Log.info("----------------------Step 3.2 - Get all product json files from Sync gateway - End");
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 2: Get all product json files from sync gateway - E
			/////////////////////////////////////////////////////////////////////////////////////////////

			
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 3: Get base line of json files from oracle database then upload to couchbase
			/////////////////////////////////////////////////////////////////////////////////////////////
			Log.info("----------------------Step 4.1 - Get last published json files from oracle - Start");
			
//			List<Map<String, Object>> masterList = publishdao.getMasterData(principal);
//
//			for (Map<String, Object> list : masterList) {
//				if (list.containsKey("docid") & list.get("docid") != null) {
//					Clob clob = (Clob) list.get("json");
//					String json = clob.getSubString(1, (int) clob.length());
//
//					if (!UploadJSON(cbServer, json, list.get("docid").toString())) {
//						Log.info("----------------------Step 4.2(Error) - Failed to upload last published json files to Sync Gateway");
//						throw new Exception("Upload Base Master Fail");
//					}
//				}
//			}
			
			Log.info("----------------------Step 4.3 - Get last published json files from oracle - End");
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 3: Get base line of json files from oracle database then upload to couchbase
			/////////////////////////////////////////////////////////////////////////////////////////////

			
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 4: Get Task List base on the release ID - S
			/////////////////////////////////////////////////////////////////////////////////////////////
			Log.info("----------------------Step 5.1 - Get task list based on the release ID " + releaseID + "- Start");
			
			List<Map<String, Object>> taskLists = publishdao.getTaskList(compCode, releaseID);
			
			Log.info("----------------------Step 5.2 - Get task list based on the release ID " + releaseID + "- End");
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 4: Get Task List base on the release ID - E
			/////////////////////////////////////////////////////////////////////////////////////////////

			
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 5: Prepare json and upload to Sync Gateway - S
			/////////////////////////////////////////////////////////////////////////////////////////////
			Log.info("----------------------Step 6.1 - Prepare json - Start");
			
			for (Map<String, Object> list : taskLists) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				long mills = (dateFormat.parse(list.get("CREATE_DATE").toString())).getTime();

				switch (list.get("FUNC_CODE").toString()) {
					case "FN.NEEDS":// Needs
						prepareNeedJson(list, publishdao, compCode, cbServer);
						break;
					default:
						prepareDynJson(list, compCode, cbServer, publishdao, mills);
				}
			}
			
			//Publish product line json
			prepareProductLineJson(compCode, cbServer);
			
			Log.info("----------------------Step 6.2 - Prepare json - End");
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 5: Prepare json and upload to Sync Gateway - E
			/////////////////////////////////////////////////////////////////////////////////////////////

			
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 6: Data roll back to product version - S
			/////////////////////////////////////////////////////////////////////////////////////////////
			Log.info("----------------------Step 7.1 - Roll back to production data - Start");
			
//			if (serverDocLists != null) {
//				for (String serverDocList : serverDocLists) {
//					boolean needDel = true;
//					
//					// check it is base or not
//					for (Map<String, Object> list : masterList) {
//						if (list.containsKey("docid") & list.get("docid") != null) {
//							if (serverDocList.equals(list.get("docid").toString()))
//								needDel = false;
//						}
//					}
//
//					// //check it is release or not
//					// for(String listID : taskIDLists){
//					// if(serverDocList.equals(listID))
//					// needDel = false;
//					// }
//
//					// delete if need
//					if (needDel) {
//						if (cbServer.RemoveDoc(serverDocList) == null)
//							throw new Exception("Remove Document Fail");
//					}
//				}
//			}
			
			Log.info("----------------------Step 7.2 - Roll back to production data - End");
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 6: Data roll back to product version - E
			/////////////////////////////////////////////////////////////////////////////////////////////

			
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 7: Add release history log - S
			/////////////////////////////////////////////////////////////////////////////////////////////
			Log.info("----------------------Step 8.1 - Add release history log - Start");
			
			if (!publishdao.AddHistory(releaseID, evnID, "Y", "", userCode))
				throw new Exception("Update History Fail");
			
			Log.info("----------------------Step 8.2 - Add release history log - End");
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 7: Add release history log - E
			/////////////////////////////////////////////////////////////////////////////////////////////
			

			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 8: Push notification - S
			/////////////////////////////////////////////////////////////////////////////////////////////
			Log.info("----------------------Step 9.1 - Push notification - Start");
			
			pushNotifyMessage(evnID, compCode, userCode);
			
			Log.info("----------------------Step 9.2 - Push notification - End");
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Step 8: Push notification - E
			/////////////////////////////////////////////////////////////////////////////////////////////
		} catch (Exception e) {
			Log.error("{ReleasePublishMgr testRelease} --------> Exception");
			Log.error(e);

			release.UpdateStatus(releaseID, "F");// Failed
			publishdao.AddHistory(releaseID, "1", "N", e.toString().replace("java.lang.Exception:", ""), userCode);

			return false;
		} finally {

		}

		return true;
	}

	public JSONObject prepareDynJson(Map<String, Object> list, String compCode, CBServer cbServer, ReleasePublish publishdao, long mills) throws Exception {
		
		DocMgr docMgr = new DocMgr(principal);
		DynData dynData = new DynData();
		Integer taskCode = ((BigDecimal) list.get("TASK_CODE")).intValue();
		JSONObject json = docMgr.getReleaseDocs(taskCode, list.get("ITEM_CODE").toString());
		//Log.error("prepareDynJson: " + json.toString());
		
		if (json == null) {
			throw new Exception("Failed to generate product json file, please check server log for detail.");
		} else {
			JSONObject doc = json.getJSONObject("main");
			String docCode = doc.getString("id");
			
			if (!UploadJSON(cbServer, doc.getJSONObject("json").toString(), docCode))
				throw new Exception("Failed to upload product json file to couchbase server.");
			
			// Upload Rate Document
			Log.info("{ReleasePublish publishRelease prepareProductJson} --------Task Updated : " + docCode + "-------");
			JSONArray rateList = json.getJSONArray("rates");
			try{
				for (int i=0; i<rateList.length(); i++) {
					JSONObject rate = rateList.getJSONObject(i);
					String rateDocCode = rate.getString("id");
					String rateJson = rate.getJSONObject("json").toString();
					if (!UploadJSON(cbServer, rateJson, rateDocCode))
						throw new Exception("Failed to upload rate file to couchbase server.");
					
				}
			} catch (Exception e) {
				Log.error("{ReleasePublish publishRelease} --------Error to upload rate ----------- ");
				Log.error(e);
			}
		

			// Upload Attached
			JSONArray attchList = json.getJSONArray("attachments");
			try {
				for (int i=0; i<attchList.length(); i++) {
					JSONObject attch = attchList.getJSONObject(i);
					BASE64Decoder decoder = new BASE64Decoder();
					String attchId = attch.getString("attId");
					String b64String = attch.getString("attFile");
					byte[] decodedBytes = decoder.decodeBuffer(b64String.split(";base64,")[1]);
					InputStream fileIn = new ByteArrayInputStream(decodedBytes);
					InputStream fileInPDF = new ByteArrayInputStream(decodedBytes);
					String contentType = b64String.split(";base64,")[0].replace("data:", "");

					/*
					ImageHelper ih = new ImageHelper();
					BufferedImage bim = ih.convertPDFToJPG(fileIn, 0);

					for (int j=0; j<3; j++) {
						ByteArrayOutputStream os = new ByteArrayOutputStream();
						int width = 340;
						int height = 480;

						if (j == 0) {
							width = 56;
							height = 80;
						} else if (j == 1) {
							width = 152;
							height = 216;
						} else if (j == 2) {
							width = 340;
							height = 480;
						}

						ImageIO.write(ih.resizeImage(bim, width, height), "png", os);
						cbServer.CreateAttach(docCode, attchId + "_thumbnail" + (j+1), "image/png", new ByteArrayInputStream(os.toByteArray()));
					}
					*/

					cbServer.CreateAttach(docCode, attchId, contentType, fileInPDF);
					fileIn.close();
					fileInPDF.close();

				}
			} catch (Exception e) {
				Log.error("{ReleasePublish publishRelease} --------Error to upload Attached ----------- ");
				Log.error(e);
			}
		
			return json;
		}
	}

	public JSONObject prepareProductLineJson(String compCode, CBServer cbServer) throws Exception {
		JSONObject json = createProductLineJson(request, compCode);

		if (json == null) {
			throw new Exception("Failed to generate product line json file, please check server log for detail.");
		} else {
			Log.info(json.toString());
			
			if (!UploadJSON(cbServer, json.toString(), "productLine_template"))
				throw new Exception("Failed to upload product json file to couchbase server.");
			
			return json;
		}
	}
	
	public JSONObject prepareNeedJson(Map<String, Object> taskList, ReleasePublish publishdao, String compCode, CBServer cbServer) throws Exception {
		Map<String, Object> CNA_JSON = publishdao.getDummyJson("CNAForm");

		Clob clob = (Clob) CNA_JSON.get("json");
		String jsonString = clob.getSubString(1, (int) clob.length());

		Map<String, JSONObject> needData = publishdao.getNeedData(Integer.parseInt(taskList.get("TASK_CODE").toString()));

		// Get base CNAForm Json file from couchbase
		JSONObject CNAForm = new JSONObject();
		CNAForm = cbServer.getDoc("CNAForm");
		
		this.langObj = new Language();
		
		if (CNAForm == null)
			CNAForm = new JSONObject();
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// N0001 == Concept Selling
		if (taskList.get("ITEM_CODE").equals("N0001")) {
			JSONObject NeedsConceptSellingJson = new JSONObject();

			if (needData.containsKey(Constants.NeedPage.NEEDS_CONCEPT_SELLING))
				NeedsConceptSellingJson = (JSONObject) needData.get(Constants.NeedPage.NEEDS_CONCEPT_SELLING);

			JSONObject conceptSelling = new JSONObject();
			conceptSelling.put("id", "NeedsConceptSelling");
			JSONArray categories = new JSONArray();

			if (NeedsConceptSellingJson != null && NeedsConceptSellingJson.has("pages") && NeedsConceptSellingJson.get("pages") instanceof JSONArray) {
				JSONArray pages = (JSONArray) NeedsConceptSellingJson.get("pages");
					
				for (int i = 0; i < pages.length(); i++) { 
					JSONObject page = (JSONObject)pages.get(i);
						
					if (page != null && page instanceof JSONObject) {
						CompanyManager cm = new CompanyManager(request);
						List<String> langList = cm.getCompanyLangList(compCode); 
						String[] lang = new String[langList.size()];
						langList.toArray(lang);
						JSONObject items = new JSONObject();		
						JSONObject targetJSO = (JSONObject) page;
						
						// Get id
						if (targetJSO != null && targetJSO.has("id") && targetJSO.get("id") instanceof String)
							items.put("id", targetJSO.getString("id"));  	
							
						// Get title							 
						JSONObject title = new JSONObject();
						JSONObject headerObj = (JSONObject) page.get("title");
						
						if (headerObj != null) {
							for (int j = 0; j < lang.length; j++) {								 
								String targetLang = lang[j];	
								
								if (headerObj.has(targetLang) && headerObj.get(targetLang) instanceof String)
									title.put(targetLang, headerObj.get(targetLang));									 
							}
							items.put("title", title);
						}
						// Get items
						JSONObject selections = (JSONObject) page.get("selections");
						
						if (selections != null) {
							JSONObject langItems = new JSONObject();

							for (int j = 0; j < lang.length; j++) {
								JSONArray files = new JSONArray();
								String langName = lang[j];
								JSONObject langSet = (JSONObject) selections.get(langName); 
								JSONArray otpions = (JSONArray) langSet.get("options");
								
								for (int l = 0; l < otpions.length(); l++) {
									JSONObject option = (JSONObject) otpions.get(l);
									JSONObject item = new JSONObject();

									item.put("id", option.get("id"));
									item.put("boxRegularWidth", option.get("boxRegularWidth"));
									String fileName = (String) option.get("image");
									String fileType = (String) option.get("fileType");
									item.put("file", fileName);
									item.put("fileType", fileType);
									item.put("thumbnail", "s_" + fileName);
									files.put(item);
								} 
								langItems.put(langName, files);									
							}
							items.put("items", langItems);
						}
						categories.put(items);
					}
				}
			}

			conceptSelling.put("categories", categories);
			CNAForm.put("conceptSelling", conceptSelling);
			
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// N0002 = Needs Questionnaire
		} else if (taskList.get("ITEM_CODE").equals("N0002")) {
			/////////////////////////////////////////////////////////////////////////////
			// TODO - S
			// To be removed after the setup up function is ready
			JSONObject baseJson = new JSONObject(jsonString);
			JSONArray baseJsonArray = new JSONArray();
			baseJsonArray = (JSONArray) baseJson.get("needsLandingContent");
			JSONObject _riskData = null;
			JSONArray _finaData = null;
			JSONObject _needsAnal = null;
			String _mandatoryFunc = null;

			for (int i = 0; i < baseJsonArray.length(); i++) {
				JSONObject obj = baseJsonArray.getJSONObject(i);

				if (obj.get("id").equals("finanEval")) {
					_mandatoryFunc = (String) obj.get("mandatoryFunc");
					_finaData = (JSONArray) obj.get("finaData");
				} else if (obj.get("id").equals("riskAssess")) {
					_riskData = (JSONObject) obj.get("riskData");
				} else if (obj.get("id").equals("needsAnal")) {
					_needsAnal = (JSONObject) obj.get("analysisData");
				}
			}

			JSONObject summaryReport = (JSONObject) baseJson.get("summaryReport");
			JSONArray summaryTemplate = (JSONArray) baseJson.get("summaryTemplate");
			// TODO - E
			/////////////////////////////////////////////////////////////////////////////

			/////////////////////////////////////////////////////////////////////////////
			// Get data from need_section table - S
			// Needs Tab
			JSONObject needsTabJson = needData.get("NeedsTab");
			JSONObject learnMoreJson = new JSONObject();
			JSONObject recommendProductsJson = new JSONObject();
			JSONObject finanEvalJson = new JSONObject();
			JSONObject needsAnalJson = new JSONObject();
			JSONObject riskAssessJson = new JSONObject();
			JSONObject NeedsFinEvalJson = new JSONObject();
			JSONObject NeedsSelectionJson = new JSONObject();
			JSONObject NeedPrioritizationJson = new JSONObject();
			JSONObject NeedsRAQuestJson = new JSONObject();
			JSONObject NeedsRAToleranceJson = new JSONObject();

			if (needsTabJson != null) {
				//////////////////////////////
				// For Need Tab - S
				if (needsTabJson.has("learnMore"))
					learnMoreJson = (JSONObject) needsTabJson.get("learnMore");

				if (needsTabJson.has("recommendProducts"))
					recommendProductsJson = (JSONObject) needsTabJson.get("recommendProducts");

				if (needsTabJson.has("finanEval"))
					finanEvalJson = (JSONObject) needsTabJson.get("finanEval");

				if (needsTabJson.has("needsAnal"))
					needsAnalJson = (JSONObject) needsTabJson.get("needsAnal");

				if (needsTabJson.has("riskAssess"))
					riskAssessJson = (JSONObject) needsTabJson.get("riskAssess");
				// For Need Tab - S
				//////////////////////////////

				// Financial Evaluation
				if (needData.containsKey("NeedsFinEval"))
					NeedsFinEvalJson = (JSONObject) needData.get("NeedsFinEval");

				// Need Selection
				if (needData.containsKey("NeedsSelection"))
					NeedsSelectionJson = (JSONObject) needData.get("NeedsSelection");

				// Needs Prioritization
				if (needData.containsKey("NeedsPrior"))
					NeedPrioritizationJson = (JSONObject) needData.get("NeedsPrior");

				// Risk Assessment > Questionnaire
				if (needData.containsKey("NeedsRAQuest"))
					NeedsRAQuestJson = (JSONObject) needData.get("NeedsRAQuest");

				// Risk Assessment > Risk Tolerance
				if (needData.containsKey("NeedsRATolerance"))
					NeedsRAToleranceJson = (JSONObject) needData.get("NeedsRATolerance");
			}
			// Get data from need_section table - E
			/////////////////////////////////////////////////////////////////////////////

			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			// needsLandingTitle
			// needsLandingTitle > Learn More
			JSONObject learnMoreButton = new JSONObject();
			JSONObject learnMore = getJsonValue(learnMoreJson);
			learnMoreButton = (JSONObject)learnMore.get("button");
			
			Iterator i1 = learnMoreButton.keys();
			String tmp_key;

			while (i1.hasNext()) {
				tmp_key = (String) i1.next();
				learnMoreButton.put(tmp_key, langObj.getNameDescWithLang("LEARN_MORE", tmp_key));
			}

			learnMore.put("id", "learnMore");
			
			i1 = null;
			tmp_key = null;

			// needsLandingTitle > Recommend Products
			JSONObject recommendButton = new JSONObject();
			JSONObject recommendProducts = getJsonValue(recommendProductsJson);
			recommendProducts.put("id", "recommendProducts");
			recommendButton = (JSONObject)recommendProducts.get("button");
			
			i1 = recommendButton.keys();

			while (i1.hasNext()) {
				tmp_key = (String) i1.next();
				recommendButton.put(tmp_key, langObj.getNameDescWithLang("RECOMMEND_PRODUCTS", tmp_key));
			}

			JSONArray needsLandingTitle = new JSONArray();
			needsLandingTitle.put(learnMore);
			needsLandingTitle.put(recommendProducts);
			/////////////////////////////////////////////////////////////

			/////////////////////////////////////////////////////////////
			// needsLandingContent
			// needsLandingContent > Financial Evaluation
			JSONObject finanEval = getJsonValue(finanEvalJson);
			finanEval.put("id", "finanEval");

			/////////////////////////////////////////////////////////////
			// Financial Evaluation > finaData
			JSONArray finaDataArray = new JSONArray();
			JSONObject finaDataHeaderTitle = new JSONObject();
			JSONObject finaDataFooterTitle = new JSONObject();

			if (NeedsFinEvalJson != null) {
				if (NeedsFinEvalJson.has("headerTitle"))
					finaDataHeaderTitle = (JSONObject) NeedsFinEvalJson.get("headerTitle");

				if (NeedsFinEvalJson.has("footerTitle"))
					finaDataFooterTitle = (JSONObject) NeedsFinEvalJson.get("footerTitle");

				JSONArray questions = new JSONArray();

				if (NeedsFinEvalJson.has("questions"))
					questions = (JSONArray) NeedsFinEvalJson.get("questions");

				// TODO - To be removed - S
				// for (int i = 0; i < questions.length(); i++) {
				// finaDataArray.put(questions.getJSONObject(i));
				// }

				String _questionId1 = "";
				String _questionId2 = "";
				Boolean hasRecord = false;
				JSONArray options = new JSONArray();
				Boolean isRelationNeedsAly = false;;
				
				for (int i = 0; i < questions.length(); i++) {
					hasRecord = false;
					_questionId1 = questions.getJSONObject(i).get("id").toString();
					
					for (int j = 0; j < _finaData.length(); j++) {
						_questionId2 = _finaData.getJSONObject(j).get("id").toString();

						if (_questionId1.equals(_questionId2)) {
							if (_finaData.getJSONObject(j).has("errorMessages"))
								questions.getJSONObject(i).put("errorMessages", _finaData.getJSONObject(j).get("errorMessages"));

							finaDataArray.put(questions.getJSONObject(i));
							hasRecord = true;
							break;
						}
					}

					if (!hasRecord)
						finaDataArray.put(questions.getJSONObject(i));
					
					if (questions.getJSONObject(i).has("isRelationNeedsAly")) {
						isRelationNeedsAly = (Boolean)questions.getJSONObject(i).get("isRelationNeedsAly");
						
						if (isRelationNeedsAly) {
							if (questions.getJSONObject(i).has("options")){
								options = (JSONArray) questions.getJSONObject(i).get("options");
								
								for (int k = 0; k < options.length(); k++) {
									if (options.getJSONObject(k).has("needAlyRelationIds")) 
										options.getJSONObject(k).put("needAlyRelationIds", Arrays.asList(options.getJSONObject(k).get("needAlyRelationIds")));							
								}
								
								questions.getJSONObject(i).put("options", options);
								//finaDataArray.put(questions.getJSONObject(i));
							}
						}
					}
					
				}
				// TODO - To be removed - E
			}

			finanEval.put("finaData", finaDataArray);
			finanEval.put("headerTitle", finaDataHeaderTitle);
			finanEval.put("footerTitle", finaDataFooterTitle);
			finanEval.put("mandatoryFunc", _mandatoryFunc);// TODO
			finanEval.put("template", "manyTypesQuestionTmpl");// TODO
			finanEval.put("buttonContinue", publishdao.getTranlationValue("buttonContinue", compCode));
			finanEval.put("buttonStart", publishdao.getTranlationValue("buttonStart", compCode));
			finanEval.put("buttonView", publishdao.getTranlationValue("buttonView", compCode));
			finanEval.put("contentLastCompleted", publishdao.getTranlationValue("contentLastCompleted", compCode));
			//////////////////////////////////////////////////////////////////////////////////////////////////////////

			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Needs Analysis
			JSONObject needsAnal = getJsonValue(needsAnalJson);
			needsAnal.put("id", "needsAnal");

			JSONObject analysisData = new JSONObject();

			// Needs Analysis > aspectSelectionHeader
			if (NeedsSelectionJson != null) {
				JSONArray selectionsArray = new JSONArray();
				JSONArray options = new JSONArray();
				JSONArray aspects = new JSONArray();

				if (NeedsSelectionJson.has("selections"))
					selectionsArray = (JSONArray) NeedsSelectionJson.get("selections");

				if (selectionsArray.length() > 0 && selectionsArray.getJSONObject(0).has("options"))
					options = (JSONArray) selectionsArray.getJSONObject(0).get("options");

				for (int i = 0; i < options.length(); i++) {
					JSONObject option = options.getJSONObject(i);

					String id = option.getString("id");

					// Guide
					JSONObject guide = (JSONObject) needData.get("NAG" + id);
					
					if (guide != null) {
						JSONArray guideArray = guide.getJSONArray("guide");
						option.put("guide", guideArray);

						// footerTitle
						if (guide.has("footerTitle")) {
							option.put("footerMsg", guide.get("footerTitle"));
						}
					}

					// Question
					JSONObject questions = (JSONObject) needData.get("NAQ" + id);
					
					if (questions != null) {
						JSONArray questionsArray = questions.getJSONArray("questions");
						option.put("questions", questionsArray);
					}


					// shortfallFunc
					// TODO					
					if (id.equals("r1") || id.equals("protect"))
						option.put("shortfallFunc", "function(needStr){eval('var need=' + needStr);try{ var values=need.values,incomePeriod=values.incomePeriod?values.incomePeriod:0,extraMoney=values.extraMoney?values.extraMoney:0,incomeReplacement=values.incomeReplacement?values.incomeReplacement:0,preparedInsurance=values.preparedInsurance?values.preparedInsurance:0,preparedCapital=values.preparedCapital?values.preparedCapital:0,inflat=need.inflation?need.inflation:0,i=inflat / 100, total=(inflat==0)?incomeReplacement*incomePeriod+ extraMoney:(1-Math.pow(1+i,incomePeriod))/-i*incomeReplacement+extraMoney;var hasAmount=preparedInsurance+preparedCapital,shortfall=total-hasAmount;need.hasAmount=hasAmount;need.needAmount=total;if(0!=total){if(shortfall>0){need.status='Shortfall';need.shortfallAmount=shortfall;}else{need.status='Fulfilled';need.shortfallAmount=0;}}else{need.status='None';need.shortfallAmount=0;}} catch(ex){debug.ex = ex;} return returnResult(need); };");	
					else if (id.equals("r2") || id.equals("critIllness"))
						option.put("shortfallFunc", "function(needStr){eval('var need='+needStr);var values=need.values,incomePeriod=values.incomePeriod?values.incomePeriod:0,treatment=values.treatment?values.treatment:0,incomeReplacement=values.incomeReplacement?values.incomeReplacement:0,preparedInsurance=values.preparedInsurance?values.preparedInsurance:0,preparedCapital=values.preparedCapital?values.preparedCapital:0,inflat=need.inflation?need.inflation:0,total=0,i=inflat/100;total=(inflat==0)?incomeReplacement*incomePeriod+ treatment:(1-Math.pow(1+i,incomePeriod))/-i * incomeReplacement + treatment;var hasAmount=preparedInsurance+preparedCapital,shortfall=total-hasAmount;need.hasAmount=hasAmount,need.needAmount=total,0!=total?shortfall>0?(need.status='Shortfall',need.shortfallAmount=shortfall):(need.status='Fulfilled',need.shortfallAmount=0):(need.status='None',need.shortfallAmount=0);return returnResult(need);};");
					else if (id.equals("r3") || id.equals("childEducate"))
						option.put("shortfallFunc", "function(needStr){eval('var need='+needStr);var values=need.values,yearsUntilStudy=values.yearsUntilStudy?values.yearsUntilStudy:0,preSchoolValue=values.preSchoolValue?values.preSchoolValue:0,preparedCapital=values.preparedCapital?values.preparedCapital:0,inflat=need.inflation?need.inflation:0,total=preSchoolValue * Math.pow(1+inflat/100,yearsUntilStudy);var hasAmount=preparedCapital,shortfall=total-hasAmount;need.hasAmount=hasAmount,need.needAmount=total,0!=total?shortfall>0?(need.status='Shortfall',need.shortfallAmount=shortfall):(need.status='Fulfilled',need.shortfallAmount=0):(need.status='None',need.shortfallAmount=0);return returnResult(need);};");
					else if (id.equals("r4") || id.equals("retirement"))
						option.put("shortfallFunc", "function(needStr){ try{ eval('var need='+needStr);var values=need.values,moneySpending=values.moneySpending?values.moneySpending:0,retirementAge=values.retirementAge?values.retirementAge:0,currentAge=values.currentAge?values.currentAge:0,yearSpending=values.numberOfYearsSpending?values.numberOfYearsSpending:0,lumpSum=values.lumpSum?values.lumpSum:0,retirementCapital=values.preparedCapital?values.preparedCapital:0,retirementInsurance=values.preparedInsurance?values.preparedInsurance:0,inflat=need.inflation?need.inflation:0,afterYear=retirementAge-currentAge,i=inflat/100,total=(inflat==0)?moneySpending*12*yearSpending+lumpSum:(1-Math.pow(1+i,yearSpending))/-i * 12 * moneySpending*Math.pow(1+i,afterYear) + lumpSum*Math.pow(1+inflat/100,afterYear);var hasAmount=retirementInsurance+retirementCapital,shortfall=total-hasAmount;need.hasAmount=hasAmount,need.needAmount=total,0!=total?shortfall>0?(need.status='Shortfall',need.shortfallAmount=shortfall):(need.status='Fulfilled',need.shortfallAmount=0):(need.status='None',need.shortfallAmount=0);} catch(ex) {debug.ex = ex} return returnResult(need);};");
					else if (id.equals("r5") || id.equals("saving")) {
						JSONObject footerMsg = new JSONObject();
						footerMsg.put("en", "You need to save %@ a month in order to achieve your target in %@ year(s).");
						footerMsg.put("zh-Hant", "你需要一個月儲蓄%@，從而在%@年內達到您的目標。");
						footerMsg.put("zh-Hans", "你需要一个月储蓄%@，从而在%@年内达到您的目标。");
						
						option.put("footerMsg", footerMsg);
						option.put("shortfallFunc", "function(needStr){try{eval('var need='+needStr);var values=need.values,savingMoney=values.saveMoney?values.saveMoney:0,years=values.savePeriod?values.savePeriod:0,inflat=need.inflation?need.inflation:0,returnRate=values.returnRate?values.returnRate:0,preparedCapital=values.preparedCapital?values.preparedCapital:0;need.years=years,need.needAmount=savingMoney,need.hasAmount=preparedCapital;var inc=(returnRate-inflat)/100.0/12.0,total=(returnRate==inflat)?years*12:(Math.pow(1+inc, years*12)-1)/inc, shortfall=savingMoney-preparedCapital,monthly=shortfall/ total;need.monthlyPremium=monthly;if(0!=shortfall){if(shortfall>0){(need.status='Shortfall',need.shortfallAmount=shortfall)}else{(need.status='Fulfilled',need.shortfallAmount=0)}}else{(need.status='None',need.shortfallAmount=0);}}catch(ex){debug.ex=ex;}                return returnResult(need);}");
					}
					
					aspects.put(option);
				}

				analysisData.put("aspects", aspects);

				if (NeedsSelectionJson.has("header"))
					analysisData.put("aspectSelectionHeader", (JSONObject) NeedsSelectionJson.get("header"));
			}

			// Needs Analysis > aspectPrioritiseHeader
			if (NeedPrioritizationJson != null && NeedPrioritizationJson.has("header") &&  NeedPrioritizationJson.get("header") instanceof JSONObject)
				analysisData.put("aspectPrioritiseHeader", (JSONObject) NeedPrioritizationJson.get("header"));

			needsAnal.put("analysisData", analysisData);// Still missing need
			// question
			needsAnal.put("template", "needAnalysisTmpl");// TODO
			needsAnal.put("buttonContinue", publishdao.getTranlationValue("buttonContinue", compCode));
			needsAnal.put("buttonStart", publishdao.getTranlationValue("buttonStart", compCode));
			needsAnal.put("buttonView", publishdao.getTranlationValue("buttonView", compCode));
			needsAnal.put("contentLastCompleted", publishdao.getTranlationValue("contentLastCompleted", compCode));
			//////////////////////////////////////////////////////////////////////////////////////////////////////////

			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Risk Assessment - S
			JSONObject riskAssess = getJsonValue(riskAssessJson);
			riskAssess.put("id", "riskAssess");

			if (NeedsRAQuestJson != null && NeedsRAToleranceJson != null) {
				if (NeedsRAToleranceJson.has("titleText")) {
					JSONArray titleText = NeedsRAToleranceJson.getJSONArray("titleText");
					NeedsRAQuestJson.put("titleText", titleText);
				}

				// TODO
				if (_riskData.has("calRiskFunc"))
					NeedsRAQuestJson.put("calRiskFunc", _riskData.get("calRiskFunc"));

				riskAssess.put("riskData", NeedsRAQuestJson);
			}

			riskAssess.put("template", "rowSelectionQuestionTmpl");// TODO
			riskAssess.put("buttonContinue", publishdao.getTranlationValue("buttonContinue", compCode));
			riskAssess.put("buttonStart", publishdao.getTranlationValue("buttonStart", compCode));
			riskAssess.put("buttonView", publishdao.getTranlationValue("buttonView", compCode));
			riskAssess.put("contentLastCompleted", publishdao.getTranlationValue("contentLastCompleted", compCode));

			JSONArray needsLandingContent = new JSONArray();
			needsLandingContent.put(finanEval);
			needsLandingContent.put(needsAnal);
			needsLandingContent.put(riskAssess);
			// Risk Assessment - E
			///////////////////////////////////////////////////////////////////////////////////////////////////////////

			CNAForm.put("needsLandingTitle", needsLandingTitle);
			CNAForm.put("needsLandingContent", needsLandingContent);
			CNAForm.put("summaryReport", summaryReport);// TODO
			CNAForm.put("summaryTemplate", summaryTemplate);// TODO
		}

		CNAForm.put("dealerGroup", "");//TODO
		CNAForm.put("compCode", compCode);
		CNAForm.put("type", "layout");

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Upload Json-S
		if (CNAForm == null)
			throw new Exception("Create FNA Json Fail");

		String strCNAForm = CNAForm.toString();

		strCNAForm = strCNAForm.replace("\\\\n", "\\n");

		if (!UploadJSON(cbServer, strCNAForm, "CNAForm"))
			throw new Exception("Upload FNA Json file failed!");
		// Upload Json-E
		//////////////////////////////////////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Upload Attached-S
		List<Map<String, Object>> attachLists = publishdao.getNeedAttachLists(taskList.get("COMP_CODE").toString(), taskList.get("ITEM_CODE").toString(), taskList.get("VER_NO").toString(), taskList.get("CHANNEL_CODE").toString());

		if (attachLists != null && attachLists.size() > 0) {
			for (Map<String, Object> attachList : attachLists) {
				BASE64Decoder decoder = new BASE64Decoder();
				Blob blob = (Blob) attachList.get("ATTACH_FILE");
				byte[] bdata = blob.getBytes(1, (int) blob.length());
				String b64String = new String(bdata);
				byte[] decodedBytes = decoder.decodeBuffer(b64String.split(";base64,")[1]);
				InputStream fileIn = new ByteArrayInputStream(decodedBytes);
				String contentType = b64String.split(";base64,")[0].replace("data:", "");

				String imageName = attachList.get("ATTACH_CODE").toString();
				cbServer.CreateAttach("CNAForm", imageName, contentType, fileIn);

				//// gen thumbnail for concept selling
				if (attachList.get("SECTION_CODE").toString().equals(Constants.NeedPage.NEEDS_CONCEPT_SELLING)) {
					InputStream tmpFileIn = new ByteArrayInputStream(decodedBytes);
					ImageHelper ih = new ImageHelper();
					BufferedImage bim = null;
					
					if (contentType.startsWith("image")) 
						bim = ImageIO.read(tmpFileIn);
					else if (contentType.equals("application/pdf"))
						bim = ih.convertPDFToJPG(tmpFileIn, 0);
				
					if (bim != null) {
						BufferedImage rzBim = ih.resizeImage(bim, 600, 450);// ImageIO.read(fileIn);
						ByteArrayOutputStream os = new ByteArrayOutputStream();
						ImageIO.write(rzBim, "png", os);
						cbServer.CreateAttach("CNAForm", "s_" + imageName, "image/png", new ByteArrayInputStream(os.toByteArray()));
					}
					
					tmpFileIn.close();
				}

				fileIn.close();
			}
		}
		// Upload Attached-E
		//////////////////////////////////////////////////////////////////////////////////////////////////////////

		Log.debug("CNAForm = " + CNAForm.toString());
		return CNAForm;
	}

	public JSONObject getJsonValue(JSONObject jsonObject) {
		String key = "";
		JSONObject jsonResult = new JSONObject();
		Iterator ir = jsonObject.keys();

		while (ir.hasNext()) {
			key = (String) ir.next();
			jsonResult.put(key, jsonObject.get(key));
		}

		return jsonResult;
	}

	public boolean stopUserRole(CBServer cbServer) {
		try {
			ArrayList<String> channels = new ArrayList<String>();
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("name", "user");
			jsonObj.put("admin_channels", channels);
			cbServer.updateRole("user", jsonObj.toString());
			Log.info("{ReleasePublish publishRelease} --------User Role Stoped-------");
			return true;
		} catch (Exception e) {
			Log.error("{ReleasePublish publishRelease} --------Error When Stoping User Role --------- ");
			Log.error(e);
			return false;
		}
	}

	public boolean startUserRole(CBServer cbServer) {
		try {
			ArrayList<String> channels = new ArrayList<String>();
			channels.add("product");

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("name", "user");
			jsonObj.put("admin_channels", channels);
			cbServer.updateRole("user", jsonObj.toString());
			Log.info("{ReleasePublish publishRelease} --------User Role Started-------");
			return true;
		} catch (Exception e) {
			Log.error("{ReleasePublish publishRelease} --------Error When Starting User Role ----------- ");
			Log.error(e);
			return false;
		}
	}

	public JSONObject createProductLineJson(HttpServletRequest request, String comp_code) throws Exception {
		ReleasePublish publishdao = new ReleasePublish();
		UserPrincipalBean principal;

		if (request == null) {
			principal = new UserPrincipalBean();
			principal.setCompCode(comp_code);
		} else {
			if (comp_code != null && !comp_code.equals("")) {
				principal = new UserPrincipalBean();
				principal.setCompCode(comp_code);
			} else {
				HttpSession session = request.getSession();
				principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
			}
		}

		return publishdao.getProductLineData(request, principal, comp_code);
	}

	public boolean UploadJSON(CBServer cbServer, String json, String docID) {
		int file = cbServer.CreateDoc(docID, json);

		if (file == 400)
			return false;
		else
			return true;
	}

	public void pushNotifyMessage(String envID, String compCode, String userCode) {
		String is_push_notify = "N";
		String notify_ws_url = "";
		String notify_msg = "";

		try {
			Environment env = new Environment();

			if (principal == null)
				principal = new UserPrincipalBean();

			if (userCode == null || userCode.equals(""))
				principal.setUserCode(userCode);

			if (compCode == null || compCode.equals(""))
				principal.setCompCode(compCode);

			Map<String, Object> envMap = env.getPushNotificationWS(request, compCode, envID);

			if (envMap.get("is_push_notify") != null && !envMap.get("is_push_notify").equals(""))
				is_push_notify = envMap.get("is_push_notify").toString();

			if (envMap.get("notify_ws_url") != null && !envMap.get("notify_ws_url").equals(""))
				notify_ws_url = envMap.get("notify_ws_url").toString();

			if (envMap.get("notify_msg") != null && !envMap.get("notify_msg").equals(""))
				notify_msg = envMap.get("notify_msg").toString();

			if (is_push_notify.equals("Y")) {// Is push notification flag
				// enabled for current
				// publishing environment?
				if (!notify_ws_url.equals("")) {
					HttpClient httpClient = new HttpClient();
					PostMethod method = new PostMethod(notify_ws_url);
					method.setParameter("pushMsg", notify_msg);
					method.setParameter("userCode", userCode);

					// connect to web service
					int statusCode = httpClient.executeMethod(method);

					if (statusCode != 200) {
						Log.error("{ReleaseDetail pushMessage 1} -----------> Cannot found the web service. " + envID);
					} else {
						Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
						String responseBody = method.getResponseBodyAsString();
						JsonObject jsonObj = gson.fromJson(responseBody, JsonObject.class);

						if (jsonObj != null) {
							boolean isSuccess = jsonObj.get("isSuccess").getAsBoolean();

							if (!isSuccess)
								Log.error("{ReleaseDetail pushMessage 2} -----------> "
										+ jsonObj.get("msg").getAsString() + "-" + envID);
						}
					}
				} else {
					Log.error(
							"{ReleaseDetail pushMessage 3} -----------> Missing to setup webservice link for publishing environment. "
									+ envID);
				}
			}
		} catch (Exception e) {
			Log.error("{ReleaseDetail pushMessage 4} -----------");
			Log.error(e);
		}
	}

	public JSONObject removeTitleElement(JSONObject inObject) {
		JSONObject objects = null;
		Iterator<?> keys = inObject.keys();

		while (keys.hasNext()) {
			String key = (String) keys.next();

			try {
				objects = (JSONObject) inObject.get(key);

				JSONObject titles;
				if (objects.has("title")) {
					titles = (JSONObject) objects.get("title");
					objects.remove("title");

					Iterator<?> ir_title = titles.keys();

					while (ir_title.hasNext()) {
						String title = (String) ir_title.next();
						objects.put(title, (String) titles.get(title));
					}

					inObject.put(key, objects);
				} else {
					// inObject.remove("title");

					// inObject = objects;

				}
			} catch (Exception e) {
			}
		}

		return inObject;
	}

}
