package com.eab.biz.upload;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.common.Log;

public class UploadManager {
	HttpServletRequest request;
	HttpSession session;
	public UploadManager(HttpServletRequest request){
		this.request = request;
		session = request.getSession();
	}
	
	public boolean isOffsetValid(String module, String id, int version, int offset){
		int lastOffset;
		boolean valid;
		String dataKey = genDataKey(module, id, version);
		String key = dataKey + "-offset";
		if(offset == 0){
			saveAttachmentOffsetToSession(module, id, version, 0);
			return true;
		}
		
		if(session.getAttribute(key) != null){
			Log.debug("has key in session");
			lastOffset = (int)session.getAttribute(key);	
			Log.debug("has offset in session:"+lastOffset);
			if(offset == lastOffset + 1)
				valid = true;
			else
				valid = false;
		}else{
			//no data in session yet
			if(offset == 0){
				valid = true;
			}
			else
				valid = false;
		}
		
		return valid;
	}
	public String genDataKey(String module, String id, int version){
		String versionStr = "";
		String key = null;
		try{
			if(version == -1){
				versionStr = "";
			}else{
				versionStr = version + "";
			}
			key = module + id + versionStr;
		}catch(Exception e){
			Log.error(e);
		}
			
		return key;
	}
	
	
	public void  saveAttachmentOffsetToSession(String module, String id, int version, int offset){
		String dataKey = genDataKey(module, id, version);
		String key = dataKey + "-offset";
		session.setAttribute(key, offset);
	}
		
	public void  saveAttachmentToSession(String module, String id, int version, int offset, String data){
		String dataKey = genDataKey(module, id, version);
		session.setAttribute(dataKey, data);
	}
	public String getAttachmentFromSession(String module, String id, int version, int offset){
		String file = null;
		String dataKey = genDataKey(module, id, version);
		file = (String)session.getAttribute(dataKey); 
		return file;
	}
	public void  clearAttachmentFromSession(String module, String id, int version, int offset){
		String dataKey = genDataKey(module, id, version);
		String dataOffsetKey = dataKey + "-offset";
		session.removeAttribute(dataKey);
		session.removeAttribute(dataOffsetKey);		
	}
}
