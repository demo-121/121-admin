package com.eab.biz.products;

import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.util.ArrayList;

import org.apache.commons.codec.binary.Base64;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.biz.dynamic.CompanyManager;
import com.eab.biz.upload.UploadManager;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.products.Products;
import com.eab.json.model.Field;
import com.eab.json.model.Field.Types;
import com.eab.json.model.UploadResponse;
import com.eab.model.products.ProductAttachBean;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class ProductCoverMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	
	public ProductCoverMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public void addSection(List<Field> sections, JsonObject values, String prodCode, int version, boolean isReadOnly){
		try{
			Log.debug("---------------------[ProductCoverMgr - start adding section]--------------------------");
			List<Field> bncItems = new ArrayList<Field>();
	
			CompanyManager cm = new CompanyManager(request);
			List<String> langList = cm.getCompanyLangList(principal.getCompCode());
			HashMap<String, String> langMap = cm.getCompanyLangMap(principal.getCompCode());
	
			Products prodDAO = new Products();
			
			for (int langIndex = 0; langIndex < langList.size(); langIndex++) {
				List<Field> bncLangItems = new ArrayList<Field>();
				Field bncLangItem = new Field();
//				bncLangItems.add(new Field("brochureTitle", Types.TEXT, langObj.getNameDesc("BROCHURE.TITLE")));
//				bncLangItems.add(new Field("brochureDesc", Types.TEXT, langObj.getNameDesc("BROCHURE.DESCRIPTION")));
				Field brochure = new Field("brochure", Types.PDF, langObj.getNameDesc("PROD.BROCHURE.TITLE"));
				brochure.setPlaceholder(langObj.getNameDesc("PROD.BROCHURE.DESCRIPTION"));
				brochure.setMandatory(false);
				bncLangItems.add(brochure);
				
//				bncLangItems.add(new Field("removeBrochure", Types.TEXT, langObj.getNameDesc("REMOVE_FILE")));
//				bncLangItems.add(new Field("addBrochure", Types.TEXT, langObj.getNameDesc("ADD_FILE")));
//				bncLangItems.add(new Field("coverTitle", Types.TEXT, langObj.getNameDesc("COVER.TITLE")));
//				bncLangItems.add(new Field("coverDesc", Types.TEXT, langObj.getNameDesc("COVER.DESCRIPTION")));
				Field cover = new Field("cover", Types.IMAGE, langObj.getNameDesc("PROD.COVER.TITLE"));
				cover.setPlaceholder(langObj.getNameDesc("PROD.COVER.DESCRIPTION"));
				cover.setMandatory(false);
				bncLangItems.add(cover);
				
//				bncLangItems.add(new Field("removeCover", Types.TEXT, langObj.getNameDesc("REMOVE_FILE")));
//				bncLangItems.add(new Field("addCover", Types.TEXT, langObj.getNameDesc("ADD_FILE")));
				bncLangItem = new Field(langList.get(langIndex), null, langMap.get(langList.get(langIndex)), true, bncLangItems);
				bncLangItem.setMandatory(false);
				if(isReadOnly)
					bncLangItem.setDisabled(true);
				else
					bncLangItem.setDisabled(false);
				bncItems.add(bncLangItem);
			}
	
			Field sectionBNC = new Field(Constants.ProductPage.COVER, Types.SECTION, langObj.getNameDesc("MENU.PRODUCTS.DETAIL.BNC"), true, bncItems);
			sections.add(sectionBNC);
			
			Log.debug("---------------------[ProductCoverMgr - end adding section]--------------------------");
			Log.debug("---------------------[ProductCoverMgr - start adding values]--------------------------");
			// add brochure and cover into values
			JsonObject langObj = new JsonObject();
			JsonObject bnc = new JsonObject();
			String compCode = principal.getCompCode();
			for (int langIndex = 0; langIndex < langList.size(); langIndex++) {
				String langCode = langList.get(langIndex);
				JsonObject valueBNC = new JsonObject();
					// to do task, get langCode
					ProductAttachBean attachBrochure = prodDAO.getProductAttach(compCode, prodCode, version, langCode, "brochure");
					Blob file = attachBrochure.getAttachFile();
					if (file != null) {
						byte[] fielData = null;
						fielData = file.getBytes(1, (int) file.length());
						String fileData = new String(fielData);
						valueBNC.addProperty("brochure", fileData);
					} else {
						valueBNC.addProperty("brochure", "");
					}
	
					ProductAttachBean attachCover = prodDAO.getProductAttach(compCode, prodCode, version, langCode, "cover");
					Blob fileCover = attachCover.getAttachFile();
					if (fileCover != null) {
						byte[] coverData = null;
						coverData = fileCover.getBytes(1, (int) fileCover.length());
						String fileDataStr = new String(coverData);
						valueBNC.addProperty("cover", fileDataStr);
					} else {
						valueBNC.addProperty("cover", "");
					}
				langObj.add(langCode, valueBNC);
			}
			values.add(Constants.ProductPage.COVER, langObj);
			Log.debug("---------------------[ProductCoverMgr - end adding values]--------------------------");
	
		}catch(Exception e){
			Log.error(e);
		}
	}

	public String getparam(String key){
		String value = request.getParameter(key);
		if(value == null && request.getParameterValues(key) != null){
			String[] values = request.getParameterValues(key);
			value = values[0];
		}
		return value;
	}
	public UploadResponse saveBNC(){
		UploadResponse response = new UploadResponse();

		String param = request.getParameter("p0");		
		String id = request.getParameter("id");
		
		if(id != null){
			byte[] decoded = Base64.decodeBase64(id.replace(" ", "+"));
			id = new String(decoded, StandardCharsets.UTF_8);
			id = id.replace("\"", "");
		}
		
		
		String module = getparam("module");
		String versionStr = getparam("version");
		String packetNumStr = getparam("packetNum");
		String offsetStr = getparam("offset");
		
		String selectPageId = "";
		String targetPageId = "";
		boolean isSkip = false;

		int version = Function.stringToInt(versionStr);
		if(version < 1)
			version = 1;
		
		int packetNum = Function.stringToInt(packetNumStr);
		int offset = Function.stringToInt(offsetStr);
		
		UploadManager um = new UploadManager(request);
		try{		
			if(offset != -1 && packetNum != -1 && um.isOffsetValid(module, id, version, offset)){
					if(offset == packetNum - 1){
						//insert into db
						Log.debug("saveBNC: last packet");
						
						String fullData = null;
						if(packetNum == 1)
							fullData = param;
						else
							fullData = um.getAttachmentFromSession(module, id, version, offset) + param;
						
						Log.debug("version:"+version);			
						UserPrincipalBean principal = Function.getPrincipal(request);
						CompanyManager cm = new CompanyManager(request);
						List<String> langList = cm.getCompanyLangList(principal.getCompCode());
						byte[] decoded = Base64.decodeBase64(fullData.replace(" ", "+"));
						String paraStr = new String(decoded, StandardCharsets.UTF_8);	
						JSONObject resultJSON = new JSONObject(paraStr);
						
						if(resultJSON.has("skip")){
							isSkip = resultJSON.getBoolean("skip");
						}
						
						if (resultJSON.has("version"))
							version = resultJSON.getInt("version");
						if (resultJSON.has("selectPageId"))
							selectPageId = resultJSON.getString("selectPageId");
						
						if(!isSkip){
							JSONObject bnc = resultJSON.getJSONObject(Constants.ProductPage.COVER);
							
							
							
							for (int i = 0; i < langList.size(); i++) {
								String lang = langList.get(i);
								if (bnc.has(lang)) {
									JSONObject bncLangItem = bnc.getJSONObject(lang);
									ProductMgr pm = new ProductMgr(request);
									if (bncLangItem.has("cover")) {
										String coverData = bncLangItem.getString("cover");
										pm.updateProductAttachFile(id, version, "cover", coverData, lang);
									}
									if (bncLangItem.has("brochure")) {
										String brochureeData = bncLangItem.getString("brochure");
										pm.updateProductAttachFile(id, version, "brochure", brochureeData, lang);
									}
								}
							}
						}
						
						
						
						
						
						String param2 = request.getParameter("p1");
						if (param2 != null && !param2.isEmpty()) {
							targetPageId = param2;
						}else{
							targetPageId = selectPageId;
						}
						
						
						//clear session
						um.clearAttachmentFromSession(module, id, version, offset);
						
						//return done state
						response.setSelectPageId(targetPageId);
						response.setItemCode(id);
						response.setVersion(version);						
						response.setCurPageId(selectPageId);
						response.setComplete(true);
						response.setTeminate(true);
					}else{
						Log.debug("saveBNC:next packet");
					
						String currentData = um.getAttachmentFromSession(module, id, version, offset);
						String newData = null;
						if(currentData != null)
							newData = um.getAttachmentFromSession(module, id, version, offset) + param;
						else
							newData = param;
						
						um.saveAttachmentOffsetToSession(module, id, version, offset);
						um.saveAttachmentToSession(module, id, version, offset, newData);
						
						//return the next offset
						int newOffset = offset + 1;
						response.setOffSet(newOffset);
						response.setComplete(false);
						response.setTeminate(false);
					}
				
				
			}else{
				response.setComplete(false);
				response.setTeminate(true);
			}
		}catch(Exception e){
			Log.debug("saveBNC: Exception");
			Log.error(e);
			//clear session
			um.clearAttachmentFromSession(module, id, version, offset);
			response.setComplete(false);
			response.setTeminate(true);
		}
		
		Log.debug("saveBNC: done");
		
		//set token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		response.setTokenID(tokenID);
		response.setTokenKey(tokenKey);
		return response;
	}
}
