package com.eab.biz.products;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.config.Config;
import com.eab.dao.funds.Funds;
import com.eab.dao.products.Products;
import com.eab.json.model.Field;
import com.eab.json.model.Field.GridColumnTypes;
import com.eab.json.model.Field.PresentationTypes;
import com.eab.json.model.Field.Types;
import com.eab.json.model.Option;
import com.eab.json.model.grid.Column;
import com.eab.json.model.grid.Grid;
import com.eab.json.model.grid.Header;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class ProductGridMgr {

	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;

	public static List<String> limList = Arrays.asList(Constants.ProductPage.Limitation.BEN_LIM, Constants.ProductPage.Limitation.PREM_LIM);
	public static List<String> rateList = Arrays.asList(Constants.ProductPage.Rate.PREM_RATE, Constants.ProductPage.Rate.TDC_RATE, Constants.ProductPage.Rate.CV_RATE, Constants.ProductPage.Rate.BON_RATE);
	public static List<String> riderList = Arrays.asList(Constants.ProductPage.Rider.RIDER_LIST, Constants.ProductPage.Rider.AUTO_ATTACH, Constants.ProductPage.Rider.COEXIST);
	public static List<String> fundParamList = Arrays.asList(
			Constants.ProductPage.FundParam.FUND_PARAMTER,
			Constants.ProductPage.FundParam.NET_INVEST_RETURN,
			Constants.ProductPage.FundParam.STARTUP_BONUS,
			Constants.ProductPage.FundParam.LOYALTY_BONUS,
			Constants.ProductPage.FundParam.COI_RATE,
			Constants.ProductPage.FundParam.SURRENDER_CHARGE
			);
 
	public ProductGridMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}

	public void addSection(List<Field> sections, JsonObject values, String prodCode, int version, String listId, boolean isReadOnly) {
		try {
			/* Generate Grid by config JSON */
			// Get JSON from database and convert to JsonArray
			JsonArray grids = ((JsonObject) JsonConverter.convertObj(new Config().getConfigJSON(listId))).getAsJsonArray(listId);

			for (int i = 0; i < grids.size(); i++) {
				Grid grid = gson.fromJson(grids.get(i), Grid.class);

				// Grid Columns
				List<Field> gridColumns = new ArrayList<Field>();

				// Name Code
				String name_code = langObj.getNameDesc(grid.getName_code()) == null ? "" : langObj.getNameDesc(grid.getName_code());

				List<Column> columns = grid.getColumns();
				Boolean insertable = grid.getInsertable();

				// Columns
				for (int j = 0; j < columns.size(); j++) {
					Column column = columns.get(j);

					String data = column.getData();
					String type = column.getType();
					String header = column.getHeader() != null ? langObj.getNameDesc(column.getHeader().getId()) : null;
					Boolean readOnly = column.getReadOnly() != null && column.getReadOnly();
					Boolean mandatory = column.getMandatory() != null && column.getMandatory();
					Boolean auto = column.getAuto() != null && column.getAuto();
					Float min = column.getMinValue();
					Float max = column.getMaxValue();
					String format = column.getFormat();

					// Generate Grid Columns
					Field gridColumn = new Field(data, type, header);
					gridColumn.setDisabled(readOnly);
					gridColumn.setMandatory(mandatory);
					gridColumn.setAuto(auto);
					gridColumn.setMin(min);
					gridColumn.setMax(max);
					gridColumn.setPresentation(format);

					// Special Handle
					switch (column.getType()) {
					case "dropdown":
						List<Option> options = new ArrayList<>();
						List<Option> source = column.getSource();
						for (int k = 0; k < source.size(); k++) {
							options.add(new Option(source.get(k).getTitle(), source.get(k).getValue()));
						}
						gridColumn.setOptions(options);
					case "text":
					case "numeric":
						gridColumns.add(gridColumn);
						break;
					case "numbers":
						for (int k = column.getStartNumber(); k <= column.getEndNumber(); k += column.getInterval()) {
							Field yearColumn = new Field(k + "", GridColumnTypes.NUMERIC, k + "");
							yearColumn.setDisabled(readOnly);
							yearColumn.setMandatory(mandatory);
							yearColumn.setAuto(auto);
							yearColumn.setMin(min);
							yearColumn.setMax(max);
							yearColumn.setPresentation(format);
							gridColumns.add(yearColumn);
						}
						break;
					}
				}

				// Section Items
				List<Field> sectionItems = new ArrayList<Field>();
				Funds fundDAO = new Funds();				
				String sectionId = grid.getRate_code();
				if( sectionId.equals(Constants.ProductPage.FUND_LIST)){
					List<Option> fundListOpts = fundDAO.getFundList(principal);					
					sectionItems.add(new Field("selectedFund", Types.PICKER, null,langObj.getNameDesc("FUND"), 100, "20%", null	, false, false, fundListOpts, null, null, null));
				} 

				// Tab
				List<Header> tabs = grid.getTabs();
				if (tabs != null && !tabs.isEmpty()) {
					List<String> resultNames = grid.getResultName();
					List<Field> tabsItems = new ArrayList<>();

					for (int j = 0; j < tabs.size(); j++) {
						Header tab = tabs.get(j);
						String resultName = resultNames.get(j);

						// Add Grid to Tab items
						List<Field> tabItems = new ArrayList<>();
						Field gridItem = new com.eab.json.model.Grid(resultName, name_code, gridColumns, insertable);
						if(isReadOnly)
							gridItem.setDisabled(true);
						tabItems.add(gridItem);

						// Add Tab to Tabs items
						tabsItems.add(new Field(null, Types.TAB, Types.NUMBER, langObj.getNameDesc(tab.getId()), (j + 1) * 100, null, null, false, false, null, tabItems, null));
					}
					// Add Tabs to Section
					sectionItems.add(new Field(null, Types.TABS, Types.NUMBER, null, 100, null, null, false, false, null, tabsItems, null));

				} else {
					// Without tabs
					Field gridItem = new com.eab.json.model.Grid(grid.getResultName().get(0), name_code, gridColumns, insertable);
					if(isReadOnly)
						gridItem.setDisabled(true);
					sectionItems.add(gridItem);
				}

				// Add Section items to Section
				sections.add(new Field(grid.getRate_code(), Types.SECTION, null, false, sectionItems));

				Products prodDAO = new Products();
				JSONObject prodSection = prodDAO.getProdSections(prodCode, principal, version, sectionId).get(sectionId);
				if (prodSection != null) {
					values.add(sectionId, JsonConverter.convertObj(prodSection));
				}
				if(sectionId.equals(Constants.ProductPage.FUND_LIST)){ 
					List<Option> fundListOpts = fundDAO.getFundList(principal);	
					for(Option opt : fundListOpts){	
						String key = opt.getValue();
						JSONObject fundSection = fundDAO.getFundListJsonFile(key, principal).get(key);
						if (fundSection != null) {
							values.add("fundList_"+key, JsonConverter.convertObj(fundSection));
						}
					}					
				}
				
			}
		} catch (Exception e) {
			Log.error(e);
		}
	}
}
