package com.eab.biz.products;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.products.Products;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Field.PresentationTypes;
import com.eab.json.model.Field.Types;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class ProductDetailMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	
	public ProductDetailMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public void addSection(List<Field> sections, JsonObject values, String prodCode, int version, boolean isNewProduct, boolean canEditProdCode, boolean isReadOnly){
		try{
			Log.debug("---------------------[ProductDetailMgr - start adding section]--------------------------");
			List<Field> prodDetailItems = new ArrayList<Field>();
	
			Products prodDAO = new Products();
			List<Option> prodLineOpts = prodDAO.getProdLines(principal);
			prodDetailItems.add(new Field("productLine", Types.PICKER, null,langObj.getNameDesc("PROD_LINE"), 100, "100%", null, true, isReadOnly, prodLineOpts, null, null, null));
			prodDetailItems.add(new Field("covCode", Types.TEXT, null, langObj.getNameDesc("PROD_CODE"), 200, "100%", null, true, (version<2 && (!canEditProdCode || isReadOnly)), null, null, null, 20));
			List<Field> covNameLangs = new ArrayList<Field>();
			covNameLangs.add(new Field("en", langObj.getNameDesc("LANG.EN"), true));
			covNameLangs.add(new Field("zh-tw", langObj.getNameDesc("LANG.ZH-TW"), false));
			prodDetailItems.add(new Field("covName", Types.MULTTEXT, null, langObj.getNameDesc("PROD_NAME"), 300, "100%", null, true, isReadOnly, null, covNameLangs, null, 40));
			prodDetailItems.add(new Field("expDate", Types.DATEPICKER, null, langObj.getNameDesc("EXP_DATE"), 400, "100%", null, false, isReadOnly, null, null, null, null));
			prodDetailItems.add(new Field("launchDate", Types.READONLY, null, langObj.getNameDesc("INIT_LAUNCH_DATE"), 500, "100%", null, false, isReadOnly, null, null, null, null));
			List<Option> channelOpts = prodDAO.getChannels(principal);
			prodDetailItems.add(new Field("channels", Types.CHECKBOXGROUP, null, langObj.getNameDesc("CHANNEL"), 600, "100%", PresentationTypes.COLUMN4, false, isReadOnly, channelOpts, null, null, null));
			Field sectionProdDetail = new Field(Constants.ProductPage.DETAIL, Types.SECTION, langObj.getNameDesc("MENU.PRODUCTS.DETAIL.DETAIL"), true, prodDetailItems);
	
			sections.add(sectionProdDetail);
			
			Log.debug("---------------------[ProductDetailMgr - end adding section]--------------------------");
			Log.debug("---------------------[ProductDetailMgr - start adding values]--------------------------");
			if(!isNewProduct){
				JsonObject productDetail = prodDAO.getProduct(prodCode, principal, version, langObj);
				if (productDetail != null) {
					values.add(Constants.ProductPage.DETAIL, productDetail);
				}
			}
			Log.debug("---------------------[ProductDetailMgr - end adding values]--------------------------");
		}catch(Exception e){
			Log.error(e);
		}
	}

}
