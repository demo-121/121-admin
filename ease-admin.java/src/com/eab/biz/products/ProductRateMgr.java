package com.eab.biz.products;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Function;
import com.eab.common.JsonConverter;
import com.eab.database.jdbc.DBManager;
import com.eab.model.products.CashAndBonusInfoObj;
import com.eab.model.products.CashAndBonusRateValueObj;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ProductRateMgr {

	DBManager dbm;
	Connection conn;
	private String JSON_ROOT_FOLDER = "/productJsonTemplate";

	public void hardCodeValue(JsonObject mobileJson, String productLine) throws IOException {
		if (!"ILP".equals(productLine)) {
			productLine = "WL";
		}

		if ("WL".equalsIgnoreCase(productLine)) {
			mobileJson.add("premAdj", JsonConverter.readJsonFromFile(JSON_ROOT_FOLDER + "/" + productLine + "_PREMADJ.json").get("premAdj"));
			mobileJson.add("zfacRate", JsonConverter.readJsonFromFile(JSON_ROOT_FOLDER + "/" + productLine + "_ZFACRATE.json").get("zfacRate"));
			mobileJson.add("pbRate", JsonConverter.readJsonFromFile(JSON_ROOT_FOLDER + "/" + productLine + "_PBRATE.json").get("pbRate"));
		}

	}
	public JsonElement convertRate(JsonObject _rateJson, String rateCode) {
		try {
			Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
			JSONObject rateJson = new JSONObject(JsonConverter.convertObj(_rateJson));
			if (rateJson.getBoolean("dynamicHeader")) {
				JSONArray rates = rateJson.getJSONArray(rateCode);
				int min = -1;
				int max = -1;
				for (int i = 0; i < rateJson.length(); i++) {
					JSONObject rootNode = rates.getJSONObject(i);
					Iterator values = rootNode.keys();
					while (values.hasNext()) {
						String value = (String) values.next();
						if (!value.equals("return")) {
							int _value = Integer.parseInt(value);
							if (min == -1) {
								min = _value;
								max = _value;
							}
							if (min > _value)
								min = _value;
							if (max < _value)
								max = _value;
						}
					}
					JsonArray _rates = new JsonArray();
					for (int j = 0; j < rates.length(); j++) {
						JsonObject nodeObj = new JsonObject();
						if ("pbRate".equals(rateCode))
							nodeObj.add("return", JsonConverter.convertObj(rates.getJSONObject(j).getString("return")));
						nodeObj.add("polYrFr", JsonConverter.convertObj(rates.getJSONObject(j).getString("min")));
						nodeObj.add("polYrTo", JsonConverter.convertObj(rates.getJSONObject(j).getString("max")));
						JsonArray rateObj = new JsonArray();
						for (int k = min; k <= max; k++) {
							rateObj.add(rates.getJSONObject(j).getString(String.valueOf(k)));
							;
						}
						nodeObj.add(("pbRate".equals(rateCode)) ? "pbRateList" : "rateList", JsonConverter.convertObj(rateObj));
						_rates.add(JsonConverter.convertObj(nodeObj));
					}
					return JsonConverter.convertObj(_rates);
					// File tempFile = File.createTempFile("appFromField", ".txt");
					// PrintWriter writer = new PrintWriter(tempFile, "UTF-8");
					// writer.println(mapper.writeValueAsString(rootObj));
					// writer.close();
					// return tempFile.getCanonicalPath().replace("\\\\", "/");

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public JsonArray convertPremRate(JsonArray premRate) {

		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();

		int fromAge = 1;
		int toAge = 1;
		int age;
		String rate;
		String rateMN;
		String rateMS;
		String rateFN;
		String rateFS;

		JsonObject nodeMN = new JsonObject();
		JsonObject nodeMS = new JsonObject();
		JsonObject nodeFN = new JsonObject();
		JsonObject nodeFS = new JsonObject();

		JsonArray rateListMN = new JsonArray();
		JsonArray rateListMS = new JsonArray();
		JsonArray rateListFN = new JsonArray();
		JsonArray rateListFS = new JsonArray();

		// if (premRate.isArray()) {
		for (int i = 0; i < premRate.size(); i++) {

			JsonObject node = premRate.get(i).getAsJsonObject();
			age = node.get("age").getAsInt();

			if (age < fromAge)
				fromAge = age; // set min
			if (age > toAge)
				toAge = age; // set max

			rateMN = node.get("MN").getAsString();
			rateMS = node.get("MS").getAsString();
			rateFN = node.get("FN").getAsString();
			rateFS = node.get("FS").getAsString();

			rateListMN.add(rateMN);
			rateListMS.add(rateMS);
			rateListFN.add(rateFN);
			rateListFS.add(rateFS);

		}
		// }

		nodeMN.add("planInd", JsonConverter.convertObj("B"));
		nodeMN.add("covClass", JsonConverter.convertObj("1"));
		nodeMN.add("sex", JsonConverter.convertObj("M"));
		nodeMN.add("ccy", JsonConverter.convertObj("HKD"));
		nodeMN.add("smoke", JsonConverter.convertObj("N"));
		nodeMN.add("issAgeMin", JsonConverter.convertObj(fromAge));
		nodeMN.add("issAgeMax", JsonConverter.convertObj(toAge));
		nodeMN.add("premRateList", JsonConverter.convertObj(rateListMN));

		nodeMS.add("planInd", JsonConverter.convertObj("B"));
		nodeMS.add("covClass", JsonConverter.convertObj("1"));
		nodeMS.add("sex", JsonConverter.convertObj("M"));
		nodeMS.add("ccy", JsonConverter.convertObj("HKD"));
		nodeMS.add("smoke", JsonConverter.convertObj("Y"));
		nodeMS.add("issAgeMin", JsonConverter.convertObj(fromAge));
		nodeMS.add("issAgeMax", JsonConverter.convertObj(toAge));
		nodeMS.add("premRateList", JsonConverter.convertObj(rateListMS));

		nodeFN.add("planInd", JsonConverter.convertObj("B"));
		nodeFN.add("covClass", JsonConverter.convertObj("1"));
		nodeFN.add("sex", JsonConverter.convertObj("F"));
		nodeFN.add("ccy", JsonConverter.convertObj("HKD"));
		nodeFN.add("smoke", JsonConverter.convertObj("N"));
		nodeFN.add("issAgeMin", JsonConverter.convertObj(fromAge));
		nodeFN.add("issAgeMax", JsonConverter.convertObj(toAge));
		nodeFN.add("premRateList", JsonConverter.convertObj(rateListFN));

		nodeFS.add("planInd", JsonConverter.convertObj("B"));
		nodeFS.add("covClass", JsonConverter.convertObj("1"));
		nodeFS.add("sex", JsonConverter.convertObj("F"));
		nodeFS.add("ccy", JsonConverter.convertObj("HKD"));
		nodeFS.add("smoke", JsonConverter.convertObj("Y"));
		nodeFS.add("issAgeMin", JsonConverter.convertObj(fromAge));
		nodeFS.add("issAgeMax", JsonConverter.convertObj(toAge));
		nodeFS.add("premRateList", JsonConverter.convertObj(rateListFS));

		JsonArray result = new JsonArray();
		result.add(nodeMN);
		result.add(nodeMS);
		result.add(nodeFN);
		result.add(nodeFS);

		return result;
	}

	public JsonArray convertTdcRate(JsonArray tdcRate) {

		// To be implemented
		return tdcRate;

	}

	public JsonArray convertPremAdj(JsonArray premAdj) {

		// To be implemented
		return premAdj;
	}

	public void convertRate(String rateFlag, JsonArray nodes, Map<String, CashAndBonusInfoObj> nfortabs, String prodCode, String genderSmokeingFlag, List<String> dealerGroup, String compCode, String planInd, List<String> ccy, int polYrMin, int polYrMax) {
		String mapKey = prodCode + "_";
		String type = "nfor_tab";
		String covCode = prodCode;
		String gender = "";
		String smoke = "";
		
		if ("1".equals(genderSmokeingFlag)) {
			gender = "M";
			smoke = "N";
		} else if ("2".equals(genderSmokeingFlag)) {
			gender = "M";
			smoke = "Y";
		} else if ("3".equals(genderSmokeingFlag)) {
			gender = "F";
			smoke = "N";
		} else if ("4".equals(genderSmokeingFlag)) {
			gender = "F";
			smoke = "Y";
		}
		
		mapKey += gender;
		mapKey += smoke;

		CashAndBonusInfoObj cashAndBonusInfoObj;
		CashAndBonusRateValueObj cashAndBonusRateValueObj;
		JsonObject node;
		Iterator<String> fieldNames;

		// hardcode payMode, premTerm, benefitTerm, refundAge and scale first.
		String issAge = "";
		String payMode = "*";
		int premTerm = 999;
		int benefitTerm = 999;
		int refundAge = 999;
		Double scale = 1000.00;

		for (int i = 0; i < nodes.size(); i++) {
			node = nodes.get(i).getAsJsonObject();
			
			// set age
			issAge = node.get("age").getAsString();
			
			// create rate target
			if (nfortabs.get(mapKey + issAge) != null)
				cashAndBonusInfoObj = nfortabs.get(mapKey + issAge);
			else
				cashAndBonusInfoObj = new CashAndBonusInfoObj(type, dealerGroup, compCode, covCode, planInd, Integer.parseInt(issAge), gender, smoke, ccy, payMode, premTerm, benefitTerm, refundAge);
			
			if ("cvRate".equals(rateFlag)) {
				if (cashAndBonusInfoObj.getCvRate() == null)
					cashAndBonusInfoObj.setCvRate(new ArrayList<CashAndBonusRateValueObj>());
			} else if ("bonRate".equals(rateFlag)) {
				if (cashAndBonusInfoObj.getBonusRate() == null)
					cashAndBonusInfoObj.setBonusRate(new ArrayList<CashAndBonusRateValueObj>());
			}

			// start to set-up cvRate / bonusRate [s]
			cashAndBonusRateValueObj = new CashAndBonusRateValueObj();
			cashAndBonusRateValueObj.setPolYrMin(polYrMin);
			cashAndBonusRateValueObj.setPolYrMax(polYrMax);
			cashAndBonusRateValueObj.setScale(scale);
			cashAndBonusRateValueObj.setRateList(new ArrayList<Integer>());

			fieldNames = (new JSONObject(node.toString())).keys();
			String fieldName = "";
			int calpolYrMax = 0;
			String rate = ""; 
			
			while (fieldNames.hasNext()) {
				fieldName = fieldNames.next();

				if (!"age".equals(fieldName)) {
					rate = node.get(fieldName).getAsString();
					
					if (Function.isInteger(rate))
						cashAndBonusRateValueObj.getRateList().add(Integer.valueOf(rate));	
				
					calpolYrMax++;
				}
			}

			// reset PolYrMax , value is calculated base on json
			cashAndBonusRateValueObj.setPolYrMax(calpolYrMax);
			// start to set-up cvRate / bonusRate [e]

			if ("cvRate".equals(rateFlag))
				cashAndBonusInfoObj.getCvRate().add(cashAndBonusRateValueObj);
			else if ("bonRate".equals(rateFlag))
				cashAndBonusInfoObj.getBonusRate().add(cashAndBonusRateValueObj);
			
			nfortabs.put(mapKey + issAge, cashAndBonusInfoObj);// 这里是会直接覆盖
		}
	}

}
