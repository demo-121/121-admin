package com.eab.biz.products;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.products.Products;
import com.eab.json.model.Field;
import com.eab.json.model.Field.Types;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class ProductFormulaMgr {
	
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	
	public static List<String> funcList = Arrays.asList(
		Constants.ProductPage.Formula.PREM_FUNC,
		Constants.ProductPage.Formula.SUM_INS_FUNC,
		Constants.ProductPage.Formula.COMM_FUNC,
		Constants.ProductPage.Formula.DA_FUNC,
		Constants.ProductPage.Formula.TSV_FUNC,
		Constants.ProductPage.Formula.GDB_FUNC,
		Constants.ProductPage.Formula.NGDB_FUNC,
		Constants.ProductPage.Formula.PB_FUNC,
		Constants.ProductPage.Formula.RB_FUNC,
		Constants.ProductPage.Formula.GSV_FUNC,
		Constants.ProductPage.Formula.NGSV_FUNC,
		Constants.ProductPage.Formula.TDB_FUNC,
		Constants.ProductPage.Formula.SUB_FUNC,
		Constants.ProductPage.Formula.LB_FUNC,
		Constants.ProductPage.Formula.MATY_FUNC
		
	);

	public ProductFormulaMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public String getNameDescCode(String funcId){
		switch(funcId){
		case Constants.ProductPage.Formula.PREM_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.PREM");
		case Constants.ProductPage.Formula.SUM_INS_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.SI");
		case Constants.ProductPage.Formula.COMM_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.CM");
		case Constants.ProductPage.Formula.DA_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.DA");
		case Constants.ProductPage.Formula.TSV_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.TSV");
		case Constants.ProductPage.Formula.GDB_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.GDB");
		case Constants.ProductPage.Formula.NGDB_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.NGDB");
		case Constants.ProductPage.Formula.PB_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.PB");
		case Constants.ProductPage.Formula.RB_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.RB");
		case Constants.ProductPage.Formula.GSV_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.GSV");
		case Constants.ProductPage.Formula.NGSV_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.NGSV");
		case Constants.ProductPage.Formula.TDB_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.TDB");
		case Constants.ProductPage.Formula.SUB_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.SUB");
		case Constants.ProductPage.Formula.LB_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.LB");
		case Constants.ProductPage.Formula.MATY_FUNC:
			return langObj.getNameDesc("MENU.PRODUCTS.DETAIL.FORMULAS.MATY");
		default:
			return "";
		}
	}
	
	public void addSection(List<Field> sections, JsonObject values, String prodCode, int version, String funcId, boolean isReadOnly){
		try{
			Log.debug("---------------------[ProductFormulaMgr - start adding sections]--------------------------");
			List<Field> funcItem = new ArrayList<Field>();
	
			funcItem.add(new Field(funcId, Types.JSBOX, Types.NUMBER, null, 100, "100%", null, false, isReadOnly, null, null, null));
			sections.add(new Field(funcId, Types.SECTION, getNameDescCode(funcId), true, funcItem));
		
			Log.debug("---------------------[ProductFormulaMgr - end adding sections]--------------------------");
			Log.debug("---------------------[ProductFormulaMgr - start adding values]--------------------------");
			Products prodDAO = new Products();
			JSONObject prodSection = prodDAO.getProdSections(prodCode, principal, version, funcId).get(funcId);
			if(prodSection != null){
				values.add(funcId, JsonConverter.convertObj(prodSection));
			}
			Log.debug("---------------------[ProductFormulaMgr - end adding values]--------------------------");
		}catch(Exception e){
			Log.error(e);
		}
			
	}

}
