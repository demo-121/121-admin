package com.eab.biz.products;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.products.Products;
import com.eab.dao.tasks.Tasks;
import com.eab.database.jdbc.DBAccess;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.UploadResponse;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.tasks.TaskBean;

public class ProductBuilderMgr {
	
	HttpServletRequest request;
	HttpSession session;
	UserPrincipalBean principal;
	Language langObj;
	Products prodDao;

	public ProductBuilderMgr(HttpServletRequest request) {
		super();
		this.request = request;
		session = request.getSession();
		principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		prodDao = new Products();
	}
	
	
	private boolean allowNoID(String type){
		if(type.equals("hbox"))
			return true;
		return false;
	}
	
	/*
	pust result from front-end, filter useless info.
	itemTemplate - JSONObject, product template
	value - input result
	*/
	private Object getItemValue(JSONObject itemTemplate, Object value){
		
		
		String type = itemTemplate.has("type") ? itemTemplate.getString("type") : "";
		
		switch(type){
			case "productConfig":		
				JSONObject result = new JSONObject();
				JSONArray items = itemTemplate.getJSONArray("items");
				JSONObject valueObj = (JSONObject)value;
				for(int i = 0; i < items.length(); i++){
					JSONObject item = items.getJSONObject(i);
					String sectionID = item.getString("id");
					if(valueObj.has(sectionID))
						result.put(sectionID, getItemValue(item, valueObj.getJSONObject(sectionID)));
				}
				return result;
				
			case "sectionGroup":
				JSONObject sgResult = new JSONObject();
				JSONArray sgItems = itemTemplate.getJSONArray("items");
				JSONObject srValueObj = (JSONObject)value;
				for(int i = 0; i < sgItems.length(); i++){
					JSONObject srItem = sgItems.getJSONObject(i);
					JSONObject srItemValue = srValueObj.getJSONObject(srItem.getString("id")); 
					sgResult.put(srItem.getString("id"), getItemValue(srItem, srItemValue));
				}
				return sgResult;
				
			case "section":
			case "tabs":
				JSONObject sectionResult = new JSONObject();
				JSONArray sectionItems = itemTemplate.getJSONArray("items");
				for(int i = 0; i < sectionItems.length(); i++){
					JSONObject sectionItem = sectionItems.getJSONObject(i);
					if(sectionItem.has("id")){
						String sectionItemID = sectionItem.getString("id");
						JSONObject sectionValue = (JSONObject)value;
						if(sectionValue.has(sectionItemID)){
							Object sectionItemValue = sectionValue.get(sectionItemID);
							sectionResult.put(sectionItemID, getItemValue(sectionItem, sectionItemValue));
						}
					}else{
						if(allowNoID(sectionItem.getString("type"))){
							JSONObject mergeJSON = (JSONObject)getItemValue(sectionItem, value);
							Iterator  mergeJSONKeys = mergeJSON.keys();
							while (mergeJSONKeys.hasNext()) {
						        String key = (String)mergeJSONKeys.next();
						        sectionResult.put(key, mergeJSON.get(key));
						    }
							
						}
					}
				}
				return sectionResult;
				
			case "complexList":
				JSONArray complexResult = new JSONArray();
				//JSONArray complexItems = itemTemplate.getJSONArray("items");
				JSONArray complexValue = (JSONArray)value;
				int complexValueSize = complexValue.length();
				JSONObject complexTemplate = new JSONObject(itemTemplate.toString());
				complexTemplate.put("type", "section");
				for(int j = 0; j < complexValueSize; j++){
					JSONObject complexItemValue = complexValue.getJSONObject(j);
					complexResult.put(getItemValue(complexTemplate, complexItemValue));
				}
				return complexResult;
				
			case "hbox":
				JSONObject hbResult = new JSONObject();
				JSONArray hbItems = itemTemplate.getJSONArray("items");
				for(int i = 0; i < hbItems.length(); i++){
					JSONObject hbItem = hbItems.getJSONObject(i);
					if(hbItem.has("id")){
						String sectionItemID = hbItem.getString("id");
						JSONObject sectionValue = (JSONObject)value;
						if(sectionValue.has(sectionItemID)){
							Object hbItemValue = sectionValue.get(sectionItemID);
							hbResult.put(sectionItemID, getItemValue(hbItem, hbItemValue));
						}
					}
				}
				return hbResult;
				
			case "detailsList":
				JSONObject detailsResult = new JSONObject();
				//JSONArray complexItems = itemTemplate.getJSONArray("items");
				JSONObject detailsValue = (JSONObject)value;
				JSONObject detailsTemplate = new JSONObject(itemTemplate.toString());
				detailsTemplate.put("type", "section");
				
				Iterator<?> keys = detailsValue.keys();

				while( keys.hasNext() ) {
				    String key = (String)keys.next();
				    if ( detailsValue.get(key) instanceof JSONObject ) {
				    	detailsResult.put(key, getItemValue(detailsTemplate, detailsValue.getJSONObject(key)));
				    }
				}			
				return detailsResult;
			
			default:
				if(itemTemplate.has("id") && itemTemplate.has("type") && !itemTemplate.getString("type").equals("readOnly")){
					return value;
				}
		}
		
		
		return null;
	}
	
	public UploadResponse saveProduct(){
		UploadResponse response = new UploadResponse();
		
		String prodCode = "";
		String selectPageId = "";
		String targetPageId = "";
		int version = 1;
		PrintWriter out = null;
		TaskBean taskInfo = null;
		JSONObject mProdDetail = null;
		int taskId = -1;
		Map<String, JSONObject> arrMSections = new HashMap<String, JSONObject>();;
		boolean isNewProduct = false;
		boolean isSkip = false;
		String errMsg = null;
		
		try {
			String param = request.getParameter("p0");
			if (param != null && !param.isEmpty()){
				String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
				
				JSONObject inputJson = new JSONObject(resultStr);
				JSONObject resultJSON = inputJson.getJSONObject("values");
				
				if(inputJson.has("taskId")){
					Object taskIdObj = inputJson.get("taskId");
					Log.error("taskId:" + taskIdObj.toString());
					if(taskIdObj instanceof JSONArray){
						JSONArray taskIdArr = (JSONArray)taskIdObj;	
						if(taskIdArr.length() > 0 )
							taskId = taskIdArr.getInt(0);
					}else{
						taskId = resultJSON.getInt("taskId");
					}	
				}else{
					isNewProduct = true;
				}
				
				if (inputJson.has("id")) {
					prodCode = inputJson.getString("id");
					if (prodCode == null || prodCode.equals(""))
						isNewProduct = true;
				}else{
					if(resultJSON.has(Constants.ProductPage.DETAIL)){
						JSONObject datail = resultJSON.getJSONObject(Constants.ProductPage.DETAIL);
						if(datail.has("covCode"))
							prodCode = datail.getString("covCode");
						else{
							isSkip = true;
						}
							
					}
				}
				
				// check id, do nth without id
				if(prodCode.equals("")){
					isSkip = true;
				}
				Log.error("covCode: " + prodCode);
				
				if (inputJson.has("version"))
					version = inputJson.getInt("version");
				else{
					if(resultJSON.has("version"))
						version = resultJSON.getInt("version");
					else
						version = 1;
				}
				
				JSONObject template = prodDao.getProductTemplateJson("DEFAULT", principal, false);
				
				//save sections
				JSONObject jSection = (JSONObject)getItemValue(template, resultJSON);
				Log.error("jSection: "+ jSection.toString());
				Iterator<?> keys = jSection.keys();
				while( keys.hasNext() ) {
				   String key = (String)keys.next();
				   if(jSection.get(key) instanceof JSONObject){
					   prodDao.saveProdSection(principal, null, jSection.getJSONObject(key), prodCode, key, version);
				   }
				}
				
				//make and save product json 
				JSONObject prodJson = parseSectionsToProduct(template, prodCode, version);
				prodDao.saveProdJson(principal, null, prodJson, prodCode, version);

				if (inputJson.has("selectPage")){
					selectPageId = inputJson.getJSONObject("selectPage").getString("id");
				}
				if(inputJson.has("selectPageId")){
					Object targetPageIdObj = inputJson.get("selectPageId");
					if((targetPageIdObj instanceof JSONArray)){
						JSONArray targetPageIdObjArr = (JSONArray)targetPageIdObj;
						targetPageId = targetPageIdObjArr.getString(0);
					}else{
						targetPageId = inputJson.getString("selectPageId");
					}
				}else{
					targetPageId = selectPageId;
				}			

				if(inputJson.has("skip")){
					isSkip = inputJson.getBoolean("skip");
				}
				
				
				
				if (resultJSON.has(Constants.ProductPage.TASK)) {
					taskInfo = new TaskBean();
					JSONObject _mTaskInfo = resultJSON.getJSONObject(Constants.ProductPage.TASK);
					if(!_mTaskInfo.has("task_desc") || (_mTaskInfo.has("task_desc") && "".equals(_mTaskInfo.get("task_desc")) 
							|| !_mTaskInfo.has("effDate") || (_mTaskInfo.has("effDate") && _mTaskInfo.get("effDate").getClass() != Long.class)))
						isSkip = false;
					else{
						taskInfo.setTaskDesc(_mTaskInfo.getString("task_desc"));
						if(_mTaskInfo.has("ref_no"))taskInfo.setRefNo(_mTaskInfo.getString("ref_no"));
						Date effDate = new Date(_mTaskInfo.getLong("effDate"));
						taskInfo.setEffDate(effDate);
					}
					
					if(taskId>0){
						taskInfo.setTaskCode(String.valueOf(taskId));
					}
				}
				
				if (resultJSON.has(Constants.ProductPage.DETAIL)) {
					JSONObject _mProdDetail = resultJSON.getJSONObject(Constants.ProductPage.DETAIL);
					if(!(!_mProdDetail.has("covCode") || (_mProdDetail.has("covCode") && _mProdDetail.get("covCode").equals("")) 
							|| !_mProdDetail.has("covName")
							|| !_mProdDetail.has("productLine") || (_mProdDetail.has("productLine") && "".equals(_mProdDetail.has("productLine"))))){
						mProdDetail = new JSONObject();
						for (String key : _mProdDetail.keySet()) {
							mProdDetail.put(key, _mProdDetail.get(key));
							if (key.equals("covCode"))
								prodCode = _mProdDetail.get(key).toString();
						}
						mProdDetail.put("version", version);
						if(isNewProduct){
							mProdDetail.put("launch_date", taskInfo.getEffDate());
							if(taskInfo != null){
								taskInfo.setItemCode(prodCode);
								taskInfo.setFuncCode(Tasks.FuncCode.PROD);
								taskInfo.setVerNo("1");
							}
						}
					}
				}

				if(!isSkip){
					Log.error("save product");
					errMsg = saveProduct(prodCode, version, taskInfo, mProdDetail, taskId, principal);
				}	
			}			
			
			if(errMsg != null){
				Field negativeBtn = new Field("cancel", "button", langObj.getNameDesc("BUTTON.OK"));
				Dialog errDialog = new Dialog("Error", langObj.getNameDesc("ERROR"), errMsg, null, negativeBtn, 30);
				response.setDialog(errDialog);
			}
			response.setSelectPageId(targetPageId);
			response.setItemCode(prodCode);
			response.setVersion(version);						
			response.setCurPageId(selectPageId);
			response.setChange((isNewProduct)?true:false);
		} catch (Exception e) {
			Log.debug("[save product] throw error: cxc");
			Log.error(e);
		}
		
		//set token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		response.setTokenID(tokenID);
		response.setTokenKey(tokenKey);
		return response;
	}
	
	
	public JSONObject parseSectionsToProduct(JSONObject template, String prodCode, int version) throws SQLException{
		JSONObject prodJson = new JSONObject();
		Map<String, JSONObject> sectionMap = prodDao.getProdSections(prodCode, principal, version, null);
		
		Log.error("parseSectionsToProduct : " + sectionMap.toString());
		for( String key : sectionMap.keySet() ) {
		    JSONObject section = sectionMap.get(key);
		    JSONObject templateSection = getSectionTemplate(template, key);
		    Iterator<?> keys = section.keys();
		    while( keys.hasNext() ) {
		        String sKey = (String)keys.next();
		        if(templateSection != null){
		        	Object sectionObj = section.get(sKey);
		        	//sectionMassage(template.getJSONObject(sKey), sectionObj);
		        	prodJson.put(sKey, sectionObj);
		        }
		    }
		}
		return prodJson;
	}
	
	public JSONObject getSectionTemplate(JSONObject template, String key){
		JSONArray items = template.getJSONArray("items");
		int itemCount = items.length();
		for(int i = 0; i < itemCount; i++){
			JSONObject section = items.getJSONObject(i);
			if(key.equalsIgnoreCase(section.getString("id"))){
				return section;
			}
		}
		return null;
	}
	
	//to do task
	public void sectionMassage(JSONObject template, Object section){
		String tempType = template.getString("type");
		if(section.getClass() == JSONObject.class){
			//
			JSONObject sJson = (JSONObject)section;
			if("formulas".equalsIgnoreCase(sJson.getString("id")) && "detailsList".equalsIgnoreCase(tempType)){
				Iterator<?> keys = sJson.keys();
				while( keys.hasNext() ) {
			        String fKey = (String)keys.next();
			        JSONObject func = sJson.getJSONObject(fKey);
			        if(func.has("func")){
			        	sJson.put(fKey, func.get("func"));
			        }
			    }
			}
			
		}
		// single field in complex list
		if("complexList".equalsIgnoreCase(tempType)){
			JSONArray sAry = (JSONArray)section;
			int listCount = sAry.length();
			String onlyKey = null;
			Object onlyValues = null;
			for(int i = 0; i < listCount; i++){
				JSONObject clJson = sAry.getJSONObject(i);
				Iterator<?> keys = clJson.keys();
				int keyCount = 0;
				while( keys.hasNext() ) {
			        keyCount += 1;
			    }
				if(keyCount == 1){
					keys = clJson.keys();
					onlyKey = (String)keys.next();
					if(clJson.get(onlyKey).getClass() == String.class){
						if(onlyValues == null){
							onlyValues = new String[listCount];
						}else{
							String[]  _onlyValues = (String[])onlyValues;
							_onlyValues[i] = clJson.getString(onlyKey);
						}
					}else if(clJson.get(onlyKey).getClass() == int.class){
						if(onlyValues == null){
							onlyValues = new int[listCount];
						}else{
							int[]  _onlyValues = (int[])onlyValues;
							_onlyValues[i] = clJson.getInt(onlyKey);
						}
					}
					
				}
			}
			
			if(onlyValues != null){
				section = onlyValues;
			}
		}
		
		//single field in details list 
		//multiple fields in details list: remove key "id"
		if("detailsList".equalsIgnoreCase(tempType)){
			JSONObject dlSection = (JSONObject)section;
			Iterator<?> keys = dlSection.keys();
			while( keys.hasNext() ) {
				String dlKey = (String)keys.next();
				JSONObject dlItemSection = dlSection.getJSONObject(dlKey);
				dlItemSection.remove("id");
				String onlyKey = null;
				Iterator<?> dltKeys = dlItemSection.keys();
				while( dltKeys.hasNext() ) {
					String curKey = (String)keys.next();
					if(!dltKeys.hasNext())
						onlyKey = curKey;
				}
				if(onlyKey != null){
					dlSection.put(dlKey, dlItemSection.get(onlyKey));
				}
		    }
			
		}
		
		//
	}
	
	/*
	added by nathan.chan@27/02/2017
	to do all bz logic in mgr instead
	task: 
		1.save task info
		2.save product details
	*/
	public String saveProduct(String prodCode, int version, TaskBean taskInfo, JSONObject prodDetail, int taskId, UserPrincipalBean principal) throws SQLException{
		String errMsg = null;
		Connection conn = null;
		Language langObj = new Language(principal.getUITranslation());

		try {
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			Tasks taskMgr = new Tasks();
			Products products = new Products();
			
			if (taskInfo != null) {
				if (taskInfo.getTaskCode() == null || taskInfo.getTaskCode().equals("")) {
					if (!taskMgr.addTask(principal, taskInfo, conn)) {
						conn.rollback();
						conn.commit();
						
						return langObj.getNameDesc(products.NO_INSERT);
					}
				} else {
					if (!taskMgr.updateTask(principal, taskInfo, conn)) {
						conn.rollback();
						conn.commit();
						
						return langObj.getNameDesc(products.NO_UPDATE);
					}
				}
			}
			if (prodDetail != null) {
				errMsg = products.saveProdDetail(taskId, prodDetail, principal, conn);
				if(errMsg != null){
					conn.rollback();
					conn.commit();
					return errMsg;
				}
			}
			conn.commit();
			return null;
		}catch (Exception e) {
			Log.error(e);
			if (conn != null && !conn.isClosed())
				conn.rollback();
			
			return e.getMessage();
		} finally {
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
	}
	
	public UploadResponse saveProduct_(){
		UploadResponse response = new UploadResponse();
		
		String prodCode = "";
		String selectPageId = "";
		String targetPageId = "";
		int version = 1;
		PrintWriter out = null;
		TaskBean taskInfo = null;
		JSONObject mProdDetail = null;
		int taskId = -1;
		Map<String, JSONObject> arrMSections = new HashMap<String, JSONObject>();;
		boolean isNewProduct = false;
		boolean isNewProductComplete = true;
		boolean isSkip = false;
		
		String[] sIds = {
				Constants.ProductPage.FEATURES,
				Constants.ProductPage.PROPERTIES,
				Constants.ProductPage.ALT_OPTION,
				Constants.ProductPage.FUND_LIST,
				Constants.ProductPage.Formula.COMM_FUNC,
				Constants.ProductPage.Formula.DA_FUNC,
				Constants.ProductPage.Formula.GDB_FUNC,
				Constants.ProductPage.Formula.GSV_FUNC,
				Constants.ProductPage.Formula.LB_FUNC,
				Constants.ProductPage.Formula.MATY_FUNC,
				Constants.ProductPage.Formula.NGDB_FUNC,
				Constants.ProductPage.Formula.NGSV_FUNC,
				Constants.ProductPage.Formula.PB_FUNC,
				Constants.ProductPage.Formula.PREM_FUNC,
				Constants.ProductPage.Formula.RB_FUNC,
				Constants.ProductPage.Formula.SUB_FUNC,
				Constants.ProductPage.Formula.SUM_INS_FUNC,
				Constants.ProductPage.Formula.TDB_FUNC,
				Constants.ProductPage.Formula.TSV_FUNC
		};
		List<List<String>> sArrs = new ArrayList<List<String>>();
		sArrs.add(ProductGridMgr.limList);
		sArrs.add(ProductGridMgr.rateList);
		sArrs.add(ProductGridMgr.fundParamList);
		sArrs.add(ProductGridMgr.riderList);
		
		try {

			String param = request.getParameter("p0");
			if (param != null && !param.isEmpty()){
				String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
				JSONObject resultJSON = new JSONObject(resultStr);
				
				if(resultJSON.has("taskId")){
					Object taskIdObj = resultJSON.get("taskId");
					Log.error("taskId:" + taskIdObj.toString());
					if(taskIdObj instanceof JSONArray){
						JSONArray taskIdArr = (JSONArray)taskIdObj;	
						if(taskIdArr.length() > 0 )
							taskId = taskIdArr.getInt(0);
					}else{
						taskId = resultJSON.getInt("taskId");
					}
				}
				if (resultJSON.has("id")) {
					prodCode = resultJSON.getString("id");
					if (prodCode == null || prodCode.equals(""))
						isNewProduct = true;
				}
				
				JSONObject template = prodDao.getProductTemplateJson("DEFAULT", principal, false);
				
				JSONObject prodJson = new JSONObject();
				try{
					JSONArray templateItems = template.getJSONArray("items");
					int itemCount = templateItems.length();
					
					for(int itemIndex = 0; itemIndex < itemCount; itemIndex++){
						JSONObject templateItem = templateItems.getJSONObject(itemIndex);
						if(templateItem.has("id")){
							String itemID = templateItem.getString("id");
							
							if(resultJSON.has(itemID)){
								
								JSONObject itemJSON = resultJSON.getJSONObject(itemID);
								Iterator<?> keys = itemJSON.keys();
								
								//section children
								if(templateItem.has("items")){
									JSONArray items = templateItem.getJSONArray("items");
									int ct = items.length();
									for(int ctIndex = 0; ctIndex < ct; ctIndex++){
										JSONObject item = items.getJSONObject(ctIndex);
										//check if this filed shud be put in prod json 
										if(Function.isDataValid(item)){
											
										}
									}
								}
								
								
								while( keys.hasNext() ) {
								    String key = (String)keys.next();	    
								    prodJson.put(key, itemJSON.get(key));
								}
							}
						}
						
					}
				}catch(Exception e){
					Log.error("product template parse err: "+e.getMessage());
				}
				Log.error("prodJson: " + prodJson.toString());
				
				if (resultJSON.has("version"))
					version = resultJSON.getInt("version");

				if (resultJSON.has("selectPage")){
					selectPageId = resultJSON.getJSONObject("selectPage").getString("id");
				}
				targetPageId = resultJSON.has("selectPageId")?  resultJSON.getString("selectPageId") : selectPageId;
				
				if (resultJSON.has(Constants.ProductPage.TASK)) {
					taskInfo = new TaskBean();
					JSONObject _mTaskInfo = resultJSON.getJSONObject(Constants.ProductPage.TASK);
					if(!_mTaskInfo.has("task_desc") || (_mTaskInfo.has("task_desc") && "".equals(_mTaskInfo.get("task_desc")) 
							|| !_mTaskInfo.has("effDate") || (_mTaskInfo.has("effDate") && _mTaskInfo.get("effDate").getClass() != Long.class)))
						isNewProductComplete = false;
					else{
						taskInfo.setTaskDesc(_mTaskInfo.getString("task_desc"));
						if(_mTaskInfo.has("ref_no"))taskInfo.setRefNo(_mTaskInfo.getString("ref_no"));
						Date effDate = new Date(_mTaskInfo.getLong("effDate"));
						taskInfo.setEffDate(effDate);
					}
					
					if(taskId>0){
						taskInfo.setTaskCode(String.valueOf(taskId));
					}
				}else if(isNewProduct){
					isNewProductComplete = false;
				}
				
				if (resultJSON.has(Constants.ProductPage.DETAIL)) {
					JSONObject _mProdDetail = resultJSON.getJSONObject(Constants.ProductPage.DETAIL);
					if(!_mProdDetail.has("covCode") || (_mProdDetail.has("covCode") && _mProdDetail.get("covCode").equals("")) 
							|| !_mProdDetail.has("covName")
							|| !_mProdDetail.has("productLine") || (_mProdDetail.has("productLine") && "".equals(_mProdDetail.has("productLine"))))
						isNewProductComplete = false;
					else{
						mProdDetail = new JSONObject();
						for (String key : _mProdDetail.keySet()) {
							mProdDetail.put(key, _mProdDetail.get(key));
							if (key.equals("covCode"))
								prodCode = _mProdDetail.get(key).toString();
						}
						mProdDetail.put("version", version);
						if(isNewProduct){
							mProdDetail.put("launch_date", taskInfo.getEffDate());
							if(taskInfo != null){
								taskInfo.setItemCode(prodCode);
								taskInfo.setFuncCode(Tasks.FuncCode.PROD);
								taskInfo.setVerNo("1");
							}
						}
					}
				}else if(isNewProduct){
					isNewProductComplete = false;
				}
				
				for(String sid: sIds){
					if (resultJSON.has(sid)) {
						arrMSections.put(sid,resultJSON.getJSONObject(sid));
					}
				}
				
				for(List<String> sArr: sArrs){
					for(String key: sArr){
						if(resultJSON.has(key)){
							arrMSections.put(key, resultJSON.getJSONObject(key));
						}
					}
				}
				
				if(resultJSON.has("skip")){
					isSkip = resultJSON.getBoolean("skip");
				}
			
				
			}
			Products products = new Products();
			String errMsg = (!isSkip && (!isNewProduct || isNewProductComplete))?products.saveProduct(prodCode, version, taskInfo, mProdDetail, arrMSections, taskId, principal):null;
			if(errMsg != null){
				Field negativeBtn = new Field("cancel", "button", langObj.getNameDesc("BUTTON.OK"));
				Dialog errDialog = new Dialog("Error", langObj.getNameDesc("ERROR"), errMsg, null, negativeBtn, 30);
				response.setDialog(errDialog);
			}
			response.setSelectPageId(targetPageId);
			response.setItemCode(prodCode);
			response.setVersion(version);						
			response.setCurPageId(selectPageId);
			response.setChange((isNewProduct && isNewProductComplete)?true:false);
		} catch (Exception e) {
			Log.debug("[save product] throw error");
			Log.error(e);
		}
		
		//set token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		response.setTokenID(tokenID);
		response.setTokenKey(tokenKey);
		return response;
	}
	
	
}
