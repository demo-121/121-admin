package com.eab.biz.products;
 
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.products.Products;
import com.eab.json.model.Condition;
import com.eab.json.model.Dialog;
import com.eab.json.model.DialogItem;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Trigger;
import com.eab.json.model.Field.PresentationTypes;
import com.eab.json.model.Field.Types;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class ProductChargesMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;

	public ProductChargesMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public void addSection(List<Field> sections, JsonObject values, String prodCode, int version, boolean isReadOnly){
		try{
			Log.debug("---------------------[ProductChargesMgr - start adding section]--------------------------");
			List<Field> prodChargesItems = new ArrayList<Field>();
	 
			int ageMax = 999;
			int ageMin = 0;
	
			// Charges item - entry age
			List<Field> chargesItems = new ArrayList<Field>();
			chargesItems.add(new Field("fmc", Types.TEXT, Types.NUMBER, langObj.getNameDesc("FUND_MANAGEMENT_CHARGE") , 301, null, null, false, isReadOnly, null, null, null, ageMin, ageMax));
			chargesItems.add(new Field("admChrg", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("ADMINISTRATION_CHARGE") , 302, null, null, false, isReadOnly, null, null, null, ageMin, ageMax));
			chargesItems.add(new Field("polFee", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("POLICY_FEE") , 303, null, null, false, isReadOnly, null, null, null, ageMin, ageMax));
			
			prodChargesItems.add(new Field("charges", Types.HBOX, null, langObj.getNameDesc("CHARGES"), 300, "100%", PresentationTypes.COLUMN4, false, false, null, chargesItems, null));
	 	
			Field sectionProdCharges = new Field(Constants.ProductPage.CHARGES, Types.SECTION, null, true, prodChargesItems);
	
			sections.add(sectionProdCharges);
			
			Log.debug("---------------------[ProductChargesMgr - end adding section]--------------------------");
			Log.debug("---------------------[ProductChargesMgr - start adding values]--------------------------");
			Products prodDAO = new Products();
			String sectionId = Constants.ProductPage.CHARGES;
			JSONObject prodSection = prodDAO.getProdSections(prodCode, principal, version, sectionId).get(sectionId);
			if(prodSection != null){
				values.add(sectionId, JsonConverter.convertObj(prodSection));
			}
			Log.debug("---------------------[ProductChargesMgr - end adding values]--------------------------");
		}catch(Exception e){
			Log.error(e);
		}
	}
	
	

}
