package com.eab.biz.products;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import com.eab.biz.dynamic.LockManager;
import com.eab.biz.tasks.TaskInfoMgr;
import com.eab.common.Constants;
import com.eab.common.Constants.ActionTypes;
import com.eab.common.Constants.PageIDs;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.common.Functions;
import com.eab.dao.dynamic.Companies;
import com.eab.dao.dynamic.RecordLock;
import com.eab.dao.products.Products;
import com.eab.dao.tasks.Tasks;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.DialogItem;
import com.eab.json.model.Field;
import com.eab.json.model.Field.Types;
import com.eab.json.model.ListContent;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.ProductListCondition;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.model.dynamic.CompanyBean;
import com.eab.model.dynamic.RecordLockBean;
import com.eab.model.products.ProductAttachBean;
import com.eab.model.products.ProductBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.system.FuncBean;
import com.eab.security.SysPrivilegeMgr;
import com.eab.webservice.dynamic.DynamicFunction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class ProductMgr {

	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	HttpSession session;
	Gson gson;

	public ProductMgr(HttpServletRequest request) {
		super();
		this.request = request;
		session = request.getSession();
		principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}

	public Dialog cloneDialog() {
		Companies companiesDAO = new Companies();

		// Clone - Company Dialog Items
		List<Option> companyOptions = new ArrayList<>();
		try {
			List<CompanyBean> companyList = companiesDAO.getCompanyListByUser(request);
			
			for (int i = 0; i < companyList.size(); i++) {
				CompanyBean company = companyList.get(i);
				companyOptions.add(new Option(company.getCompName(), company.getCompCode()));
			}
		} catch (SQLException e1) {
			Log.error(e1);
		}
		
		List<Field> companyDialogItems = new ArrayList<>();
		// companyDialogItems.add(new DialogItem("text", DialogItem.Types.TEXT, "my text", true, "Enter your words", "TextField"));
		// companyDialogItems.add(new DialogItem("date", DialogItem.Types.DATEPICKER, "Mon Feb 15 2016 12:10:36 GMT+0800 (HKT)", true, "Pick your date", "DatePicker"));
		// companyDialogItems.add(new DialogItem("check", DialogItem.Types.CHECKBOX, "EAB", true, companyOptions));
		// companyDialogItems.add(new DialogItem("label", DialogItem.Types.LABEL, "Testing1", 1, 1));
		// companyDialogItems.add(new DialogItem("label", DialogItem.Types.LABEL, "Testing2", 1, 2));
		// companyDialogItems.add(new DialogItem("label", DialogItem.Types.LABEL, "Testing3", 1, 3));
		companyDialogItems.add(new DialogItem("compCode", DialogItem.Types.RADIOBUTTON, null, true, companyOptions));

		// Clone - Company Positive
		Field companyPositive = new Field("/Products/CheckExist", "submitButton", langObj.getNameDesc("BUTTON.OK"));
		// Clone - Company Negative
		Field companyNegative = new Field("company_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL"));
		// Clone - Company Dialog
		Dialog companyDialog = new Dialog("company", langObj.getNameDesc("PROD.CLONE_DIALOG.SELECT_COMP"), langObj.getNameDesc("PROD.CLONE_DIALOG.SELECT_COMP_MSG"), companyPositive, companyNegative, companyDialogItems, 40);

		// Clone - Confirm Positive
		Field cloneConfirmPositive = new Field("confirm_ok", "dialogButton", langObj.getNameDesc("BUTTON.OK"), companyDialog);
		// Clone - Confirm Negative
		Field cloneConfirmNegative = new Field("confirm_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL"));
		// Clone - Confirm Dialog
		Dialog cloneConfirmDialog = new Dialog("confirm", langObj.getNameDesc("PROD.CLONE_DIALOG.CLONE"), langObj.getNameDesc("PROD.CLONE_DIALOG.CLONE_MSG"), cloneConfirmPositive, cloneConfirmNegative, 30);

		//return cloneConfirmDialog;
		return companyDialog;
	}

	public Dialog newVersionDialog() {
		// New Version - Confirm Positive
		Field newVersionConfirmPositive = new Field("/Products/NewVersion", "submitButton", langObj.getNameDesc("BUTTON.OK"));
		// New Version - Confirm Negative
		Field newVersionCconfirmNegative = new Field("confirm_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL"));
		// New Version - Confirm Dialog
		Dialog newVersionConfirmDialog = new Dialog("confirm", langObj.getNameDesc("PROD.CLONE_DIALOG.NEW_VERSION"), langObj.getNameDesc("PROD.CLONE_DIALOG.NEW_VERSION_CONFIRM"), newVersionConfirmPositive, newVersionCconfirmNegative, 30);

		return newVersionConfirmDialog;
	}

	public Response productList(ProductListCondition productListCondition) {
		try{
			// productListCondition
			String criteria = productListCondition != null ? productListCondition.getCriteria() : null;
			String productLine = productListCondition != null ? productListCondition.getProductLine() : null;
			String status = productListCondition != null ? productListCondition.getStatus() : null;
			String sortBy = productListCondition != null ? productListCondition.getSortBy() : null;
			String sortDir = productListCondition != null ? productListCondition.getSortDir() : null;
			Integer recordStart = productListCondition != null ? productListCondition.getRecordStart() : null;
			Integer pageSize = productListCondition != null ? productListCondition.getPageSize() : null;
	
			// DAO
			Products productsDAO = new Products();
	
			Boolean updateContentOnly = criteria != null || productLine != null || status != null || (sortBy != null && sortDir != null);
	
			// Action
			String action = updateContentOnly ? ActionTypes.CHANGE_CONTENT : ActionTypes.CHANGE_PAGE;
	
			// Page
			Page page = new Page(PageIDs.PRODUCT_LIST, "Product List");
	
			// Token
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());
	
			// AppBar - Title
			AppBarTitle appBarTitle = new AppBarTitle("productLine", "picker", "ALL", null, productsDAO.getProdLineFilter(principal, langObj), null);
	
			// Action1 - 1
			Field appBarAction1_1 = new Field("status", "picker", "-R", null, productsDAO.getProductStatus(langObj));
			// Action1 - 2
			Field appBarAction1_2 = new Field("/Products/Search", "searchButton", null, "Search", null);
			// Action1 - 3
			Field appBarAction1_3 = new Field("/Products/Detail", "iconButton", "add", "Add", null);
			// Action1
			List<Field> appBarAction1 = new ArrayList<>();
			appBarAction1.add(appBarAction1_1);
			appBarAction1.add(appBarAction1_2);
			appBarAction1.add(appBarAction1_3);
	
			Dialog cloneDialog = cloneDialog();
			Dialog newVersionDialog = newVersionDialog();
	
			// Action2 - 1
			Field appBarAction2_1 = new Field("/Products/NewVersion", "dialogButton", langObj.getNameDesc("BUTTON.CREATE_NEW_VERSION"), newVersionDialog);
			// Action2 - 2
			Field appBarAction2_2 = new Field("/Products/Clone", "dialogButton", langObj.getNameDesc("BUTTON.CLONE"), cloneDialog);
			// Action2
			List<Field> appBarAction2 = new ArrayList<>();
			appBarAction2.add(appBarAction2_1);
			appBarAction2.add(appBarAction2_2);
	
			// Action3 - 1
			Field appBarAction3_1 = new Field("/Products/Clone", "dialogButton", langObj.getNameDesc("BUTTON.CLONE"), cloneDialog);
			// Action3
			List<Field> appBarAction3 = new ArrayList<>();
			appBarAction3.add(appBarAction3_1);
	
			// Action4 - 1
			Field appBarAction4_1 = new Field("/Products/NewVersion", "dialogButton", langObj.getNameDesc("BUTTON.CREATE_NEW_VERSION"), newVersionDialog);
			// Action4
			List<Field> appBarAction4 = new ArrayList<>();
			appBarAction4.add(appBarAction4_1);
	
			// Actions
			List<List<Field>> appBarActions = new ArrayList<>();
			appBarActions.add(appBarAction1);
			appBarActions.add(appBarAction2);
			appBarActions.add(appBarAction3);
			appBarActions.add(appBarAction4);
	
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, appBarActions, productListCondition);
	
			// Product List Data
			List<ProductBean> productList = new ArrayList<>();
	
			if (recordStart != null && recordStart != 0)
				productList = (List<ProductBean>) session.getAttribute(Constants.SessionKeys.SEARCH_RESULT);
	
			// Get list
			if (productList.size() == 0) {
				try {
					productList = productsDAO.searchProductList(productListCondition, principal, langObj);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					Log.error(e);
				}
	
				// Add result to session for "More" button use
				session.setAttribute(Constants.SessionKeys.SEARCH_RESULT, productList);
			}
	
			// Template
			Template template = new Template();
			List<Field> items = new ArrayList<>();
			items.add(new Field("prodLine", langObj.getNameDesc("PRODUCT_LIST_COLUMN_PRODUCT_LINE"), Types.TEXT, 1, 1, "200px"));
			items.add(new Field("prodCode", langObj.getNameDesc("PRODUCT_LIST_COLUMN_PRODUCT_CODE"), Types.TEXT, 2, 2, "200px"));
			items.add(new Field("prodName", langObj.getNameDesc("PRODUCT_LIST_COLUMN_PRODUCT_NAME"), Types.TEXT, 3, 3, "auto"));
			items.add(new Field("status", langObj.getNameDesc("PRODUCT_LIST_COLUMN_STATUS"), Types.TEXT, 4, 4, "150px"));
			template.setItems(items);
	
			// More
			int offset = recordStart != null ? recordStart : 0;
			int size = pageSize != null ? pageSize : Constants.SEARCH_RESULT_DEFAULT_PAGE_SIZE;
	
			List<Object> shortedList = new ArrayList<>();
	
			for (int j = offset; j < offset + size && j < productList.size(); j++) {
				shortedList.add(productList.get(j));
			}
	
			// Content - values
			ListContent contentValues = new ListContent(shortedList, productList.size(), offset > 0);
	
			// Content
			Content content = new Content(template, contentValues);
	
			// Build
			return new Response(action, null, page, tokenID, tokenKey, appBar, content);
		}catch(Exception e){
			Log.error(e);
		}
		return null;
	}

	public Response changePage(String selectPageId, String targetPageId, String prodCode, int version) {
		// Action
		String action = ActionTypes.RESET_VALUES;

		JsonObject values = new JsonObject();
		JsonObject selectPage = new JsonObject();
		selectPage.addProperty("id", targetPageId.equals("")?selectPageId:targetPageId);
		
		if (selectPageId.equals(Constants.ProductPage.TASK)) {
			TaskInfoMgr taskMgr = new TaskInfoMgr(request, "pro");
		
			if (prodCode != null && !"".equals(prodCode))
				taskMgr.addValues(values, -1, prodCode, version, null);
		}
		
		values.add("selectPage", gson.toJsonTree(selectPage));
		values.addProperty("actionType", "merge");
		values.addProperty("id", prodCode);
		Content content = new Content(null, values);
		
		return new Response(action, null, null, null, null, null, content);
	}

	public Response getProdDetail(String prodCode, int version, String selectPageId) {
		try {
			Log.error("getProdDetail: " + prodCode + "/" + version + "/" + selectPageId);
			if (prodCode == null)
				prodCode = "";
			
			if (selectPageId == null || selectPageId.equals(""))
				selectPageId = Constants.ProductPage.TASK;
			
			boolean isNewProduct = (prodCode == null || prodCode.equals("")) ? true : false;
			
//			JsonObject values = new JsonObject();
			Products prod = new Products();
			// Map<String, Object> nameMap = getUITranslation();
			
			Log.debug("---------------------[ProductMgr - start check is record readOnly]--------------------------");
			boolean canAccess = false;
			// Product status
			Map<String, Object> prodStatus = prod.getProdStatusById(prodCode, version, principal);
			Log.error("prodStatus"+ prodStatus.toString());
			// recordLock
			Dialog lockDialog = null;
			if (!isNewProduct) {
				RecordLock recordLock = new RecordLock();
				RecordLockBean recordLockBean = recordLock.newRecordLockBean("prodDetails", prodCode, version);
				recordLock.getRecordLock(request, recordLockBean);
				
				if (recordLockBean.canAccess()) {
					if(!recordLockBean.isCurrentUserAccess())
						if (recordLock.addRecordLock(request, recordLockBean))
							canAccess = true;
						else {
							LockManager lm = new LockManager(request);
							lockDialog = lm.lockDialog(recordLockBean);
						}
				} else {
					LockManager lm = new LockManager(request);
					lockDialog = lm.lockDialog(recordLockBean);
				}
			}
			
			boolean isReadOnly = false;
			boolean isSuspend = false;
			boolean updateRight = false;
			
			if (!isNewProduct) {
				if (!canAccess) {
					isReadOnly = true;
				} else if (prodStatus!=null) {
					if (prodStatus.containsKey("launch_status") && prodStatus.get("launch_status").toString().equals("L")) {
						isReadOnly = true;
					} else if (prodStatus.containsKey("status") && prodStatus.get("status").toString().equals("E")) {
						if (prodStatus.containsKey("prod_ver") && ((int)prodStatus.get("prod_ver")) >= version) {
							isReadOnly = true;
						}
					} else if (prodStatus.containsKey("task_status")) {
						if (prodStatus.get("task_status").equals("S"))
							isReadOnly = true;
						
						if (prodStatus.get("task_status").equals("D"))
							isSuspend = true;
					}
				}
			}

			//check access right
			SysPrivilegeMgr privilegeMgr = new SysPrivilegeMgr();
			ArrayList<FuncBean> funcList = privilegeMgr.getFunc(principal.getRoleCode(), "en");
			
			for (FuncBean funcBean: funcList) {
				if (funcBean.getCode().equals("FN.PRO.PRODS.UPD"))
					updateRight = true;
			}
			
			boolean canEdit = !isSuspend && !isReadOnly && updateRight;
			Log.debug("---------------------[ProductMgr - end check is record readOnly]--------------------------");
			Log.debug("---------------------[ProductMgr - start get language]--------------------------");

			// Action
			String action = ActionTypes.CHANGE_PAGE;

			// Token
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());
			
			// AppBar - Title			
			String pageTitle = "";
			Log.debug("---------------------[ProductMgr - start building app bar]--------------------------");
			List<ProductBean> products = prod.getProducts(prodCode, principal.getCompCode(), principal);
			List<Option> secondaryOpts = new ArrayList<Option>();
			
			for (ProductBean product : products) {
				String _title = product.getProdName() + " v.";
				_title += String.valueOf(product.getVersion());
				String taskState = product.getTaskStatus();
				if(taskState == null )
					taskState = "";
				_title += " (" + taskState.toLowerCase() + ")";
				
				Option opt = new Option(_title, String.valueOf(product.getVersion()));
				if (prodStatus.containsKey("prod_ver") && ((int)prodStatus.get("prod_ver")) == product.getVersion())
					opt.setHighLight(true);
					
				secondaryOpts.add(opt);
				
				if (version == product.getVersion())
					pageTitle = _title;
			}

			Object secondary = (isNewProduct) ? langObj.getNameDesc("PROD.NEW_PRODUCT") : new Field("/Products/Detail", Types.VERSIONPICKER, String.valueOf(version), secondaryOpts, null);
			AppBarTitle appBarTitle = new AppBarTitle(AppBarTitle.Types.LEVEL_TWO, null, null, secondary);
			//CommonManager cMgr = new CommonManager(request);
			//cMgr.setAppbarPrimary(appBarTitle, "/Products");

			// Action1
			List<Field> appBarAction1 = new ArrayList<>();
			
			// Action1 - 1
			if (!isReadOnly && updateRight) {
				Field appBarAction1_1 = new Field("/Products/Detail/Save", "submitChangedButton", "save", langObj.getNameDesc("BUTTON.SAVE"), null);
				appBarAction1.add(appBarAction1_1);
			}
			
			// Action1 - 2
			if (!isNewProduct && updateRight) {
				//Field appBarAction1_2 = new Field(null, "iconButton", "restore", "RESTORE", null);
				//appBarAction1_2.setDisabled(true);
				// Action1 - 3
				List<Field> appBarAction1_3Items = new ArrayList<>();
				Dialog cloneDialog = cloneDialog();
				Dialog newVersionDialog = newVersionDialog();
				
				if(prodStatus.containsKey("status") && !prodStatus.get("status").toString().equals("T"))
					appBarAction1_3Items.add(new Field("/Products/NewVersion", "dialogButton", langObj.getNameDesc("ACTION_NEW_VERSION"), newVersionDialog));
				appBarAction1_3Items.add(new Field("/Products/Clone", "dialogButton", langObj.getNameDesc("ACTION_CLONE_PRODUCT"), cloneDialog));
				Field appBarAction1_3 = new Field("more", "iconMenu", appBarAction1_3Items);
				
				//appBarAction1.add(appBarAction1_2);
				appBarAction1.add(appBarAction1_3);
			}
			

			// Actions
			List<List<Field>> appBarActions = new ArrayList<>();
			appBarActions.add(appBarAction1);

			// AppBar
			AppBar appBar = new AppBar(appBarTitle, appBarActions);

			// Page
			JsonObject pageValue = new JsonObject();
			pageValue.addProperty("id", prodCode);
			pageValue.addProperty("version", version);
			pageValue.addProperty("selectPageId", selectPageId);
			Page page = new Page("/Products/Detail", pageTitle, pageValue);

			Log.debug("---------------------[ProductMgr - start building template and values]--------------------------");
			//menu
			JsonArray menuLists = prod.getMenu(prodCode, principal, version, isNewProduct);
			
			// Template - Section
			List<Field> sections = new ArrayList<Field>();

			Log.debug("---------------------[ProductMgr - start building template - " + selectPageId + "]--------------------------");

			TaskInfoMgr taskMgr = new TaskInfoMgr(request, "pro");
			ProductDetailMgr detailMgr = new ProductDetailMgr(request);
			ProductFeatureMgr featureMgr = new ProductFeatureMgr(request);
			//ProductChargesMgr chargesMgr = new ProductChargesMgr(request);
//			ProductPropertyMgr propertyMgr = new ProductPropertyMgr(request);
//			ProductGridMgr gridMgr = new ProductGridMgr(request);
//			ProductFormulaMgr funcMgr = new ProductFormulaMgr(request);
//			ProductAlternationOptionMgr altOptionMgr = new ProductAlternationOptionMgr(request);
			
			ProductCoverMgr coverMgr = new ProductCoverMgr(request);
			Tasks tasksDao = new Tasks();
			Map<String, Object> task = tasksDao.getTaskById(-1, prodCode, version, Tasks.FuncCode.PROD, null, principal);
			
			
			taskMgr.addSection(task, sections, isNewProduct, isReadOnly || !updateRight);
			
			JsonObject values = prod.getProductTemplateValue("Data", principal, isNewProduct);
			
			if(!isNewProduct)
				taskMgr.addValues(task, values);
			
			Map<String, JSONObject> sectionMap = prod.getProdSections(prodCode, principal, version, null);
			if(sectionMap != null){
				for (Map.Entry<String, JSONObject> entry : sectionMap.entrySet())
				{
				    String sectionKey = entry.getKey();
				    if(!"TaskInfo".equalsIgnoreCase(sectionKey)){
				    	JSONObject section = entry.getValue();
				    	values.add(sectionKey, DynamicFunction.convertJsonToGson(section));
				    }
				}
			}
			
			/*
			detailMgr.addSection(sections, values, prodCode, version, isNewProduct,(isNewProduct || (!isNewProduct && ((int)prodStatus.get("prod_ver"))==1 )),canEdit);
			 
			if (!isNewProduct) {
				featureMgr.addSection(sections, values, prodCode, version, canEdit);
				//chargesMgr.addSection(sections, values, prodCode, version, isReadOnly);
//				propertyMgr.addSection(sections, values, prodCode, version, canEdit);
//				gridMgr.addSection(sections, values, prodCode, version, "limitGridList", canEdit);
//				gridMgr.addSection(sections, values, prodCode, version, "rateGridList", canEdit);
//			
//				for (String funcId : ProductFormulaMgr.funcList) {
//					funcMgr.addSection(sections, values, prodCode, version, funcId, canEdit);
//				}
//				
//				gridMgr.addSection(sections, values, prodCode, version, "riderGridList", canEdit);
//				gridMgr.addSection(sections, values, prodCode, version, "fundParamGridList", canEdit);
//				gridMgr.addSection(sections, values, prodCode, version, "fundGridList", canEdit);
//				altOptionMgr.addSection(sections, values, prodCode, version, canEdit);
				coverMgr.addSection(sections, values, prodCode, version, canEdit);
			}
			Log.debug("---------------------[ProductMgr - end building template - " + selectPageId + "]--------------------------");

//			Template template = new Template("template", sections, null, null);
//			template.setMenu(menuLists);
//
//			Log.debug("---------------------[ProductMgr - start building template]--------------------------");
//			Log.debug("---------------------[ProductMgr - start adding values]--------------------------");
*/
			values.add("id", gson.toJsonTree(prodCode));
			values.add("version", gson.toJsonTree(version));

			values.addProperty("isNew", isNewProduct);

			JsonObject selectPage = new JsonObject();
			selectPage.addProperty("id", selectPageId);
			values.add("selectPage", gson.toJsonTree(selectPage));
			
			JSONObject tempJson = translateProdTemplate(prod.getProductTemplateJson("DEFAULT", principal, true));
			Gson gson = new Gson();
			Template template = gson.fromJson(tempJson.toString(), Template.class);
			
			Content content = new Content(template, values);
			Log.debug("---------------------[ProductMgr - end adding values]--------------------------");
			Response resp =  new Response(action, null, page, tokenID, tokenKey, appBar, content);
			resp.setDialog(lockDialog);
			return resp;
		} catch (Exception e) {
			Log.error(e);
		}

		return null;
	}
	
	//tr
	public JSONObject translateProdTemplate(JSONObject template){	
		try{
			//first, translate title
			List<String> nameList = new ArrayList<String>();
			
			getTemplateNameList(template, nameList);
			Log.error("nameList: " + nameList.toString());
			Map<String, String> nameMap = Function2.getTranslateMap(request, nameList);
			
			Log.error("nameMap: " + nameMap.toString());
			setTemplateName(template, nameMap);
			
			//second, get options
		}catch(Exception e){
			Log.error("translateProdTemplate error : " + e.getMessage());
		}
		
		return template;
	}
	
	/*
	 * put options into picker.
	 * type 1: look up table
	 * type 2: mst table
	 */
	public void getTemplateOptions(JSONObject template, Map<String, Object> optionMap){
		if(template.has("options") && template.get("options").getClass() == String.class){
			//optionMap.put(template.getString("options"), null);
			String opStr = template.getString("options");
			Functions functions = new Functions();
			JSONArray options = new JSONArray();
			if("SYS_LOOKUP_MST".equalsIgnoreCase(opStr)){
				options =  functions.getLookUpOptions(request, template.getString("subtype"));
				template.put("options", options);
			}
			/*
			else{
				options = functions.getMstOptions(request, template.getString("presentation"), opStr, principal.getCompCode());
				template.put("options", options);
			}
			*/
		}
		if(template.has("items")){
			JSONArray items = template.getJSONArray("items");
			int itemSize = items.length();
			for(int itemIndex = 0; itemIndex < itemSize; itemSize++){
				getTemplateOptions(items.getJSONObject(itemIndex), optionMap);
			}
		}
	}
	
	
	public void getTemplateNameList(JSONObject template, List<String> list){
		if(template.has("title") && template.get("title").getClass() == String.class){
			list.add(template.getString("title"));
		}
		if(template.has("items")){
			JSONArray items = template.getJSONArray("items");
			int itemSize = items.length();
			for(int itemIndex = 0; itemIndex < itemSize; itemIndex++){
				getTemplateNameList(items.getJSONObject(itemIndex), list);
			}
		}
	}
	
	public void setTemplateName(JSONObject template, Map<String, String> nameMap){
		if(template.has("title")){
			String title = template.getString("title");
			if(nameMap.containsKey(title))
				template.put("title", nameMap.get(title));
		}
		if(template.has("items")){
			JSONArray items = template.getJSONArray("items");
			int itemSize = items.length();
			for(int itemIndex = 0; itemIndex < itemSize; itemIndex++){
				setTemplateName(items.getJSONObject(itemIndex), nameMap);
			}
		}
	}

	public Response cloneProduct(List<String> prodCodes, String destCompCode) {
		Products productDAO = new Products();
		String newProdCode = null;
		
		for (int i = 0; i < prodCodes.size(); i++) {
			String prodCode = prodCodes.get(i);

			try {
				newProdCode = productDAO.clone(principal, prodCode, destCompCode);
			} catch (SQLException e) {
				Log.error(e);
				return null;
			}
		}

		// OK Dialog

		// Action
		if (prodCodes.size()==1 && destCompCode.equals(principal.getCompCode())) {
			return getProdDetail(newProdCode, 1, null);
		} else {
			String action = ActionTypes.CHANGE_PAGE;
	
			// Token
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());
	
			// Page
			Page page = new Page(PageIDs.PRODUCT_LIST, "Product List");
	
			// Dialog
			Field negative = new Field("ok", "button", langObj.getNameDesc("BUTTON.OK"));
			Dialog dialog = new Dialog("clone_completed", langObj.getNameDesc("PROD.CLONE_DIALOG.CLONE"), langObj.getNameDesc("COMPLETE"), null, negative, 30);
	
			// Build
			Response rs = new Response(action, tokenID, tokenKey, page);
			rs.setDialog(dialog);
	
			return rs;
		}
	}

    public int newVersion(String prodCode) {
        Products productDAO = new Products();

        if (prodCode != null && !prodCode.isEmpty()) {
            try {
                return productDAO.newVersion(principal, prodCode);
            } catch (SQLException e) {
                Log.error(e);
            }
        }
        
        /*// OK Dialog

        // Action
        String action = ActionTypes.SHOW_DIALOG;

        // Token
        String tokenID = Token.CsrfID(request.getSession());
        String tokenKey = Token.CsrfToken(request.getSession());

        // Dialog
        Field negative = new Field("ok", "button", langObj.getNameDesc("BUTTON.OK"));
        Dialog dialog = new Dialog("new_completed", langObj.getNameDesc("PROD.CLONE_DIALOG.NEW_VERSION"), langObj.getNameDesc("COMPLETE"), null, negative, 30);

        // Build
        return new Response(action, tokenID, tokenKey, dialog);*/
        return -1;
    }

	public Response checkExisting(List<String> prodCodes, String compCode) {
		List<String> existedProdCode = new ArrayList<>();

		Products productDAO = new Products();

		for (int i = 0; i < prodCodes.size(); i++) {
			String prodCode = prodCodes.get(i);

			try {
				if (productDAO.checkExist(prodCode, compCode, -1)) {
					existedProdCode.add(prodCode);
				}
			} catch (SQLException e) {
				Log.error(e);
			}
		}

		if (existedProdCode.size() > 0) {
			// Confirmation Dialog

			// Action
			String action = ActionTypes.SHOW_DIALOG;

			// Token
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			// Dialog
			Field positive = new Field("/Products/Clone", "submitButton", langObj.getNameDesc("BUTTON.OK"));
			Field negative = new Field("cancel", "button", langObj.getNameDesc("BUTTON.CANCEL"));
			Dialog dialog = new Dialog("clone_confirm", langObj.getNameDesc("PROD.CLONE_DIALOG.CLONE"), langObj.getNameDesc("PROD.CLONE_DIALOG.CLONE_CONFIRM_MSG"), positive, negative, true, 30);

			return new Response(action, tokenID, tokenKey, dialog);
		} else {
			// Clone
			return cloneProduct(prodCodes, compCode);
		}
	}

	public void updateProductAttachFile(String prodCode, int version, String attachCode, String attachFile, String lang) {
		try {
			Products pd = new Products();
			UserPrincipalBean principal = Function.getPrincipal(request);
			ProductAttachBean attach = pd.getProductAttach(principal.getCompCode(), prodCode, version, lang, attachCode);

			if (attach == null || attach.getAttachCode() == null) {
				if (!attachFile.equals("")) {
					// Create attachment
					attach.setCompCode(principal.getCompCode());
					attach.setProdCode(prodCode);
					attach.setVersion(version);
					attach.setLangCode(lang);
					attach.setAttachCode(attachCode);
					attach.setCreateBy(principal.getUserCode());
					attach.setCreateDate(new Date(Calendar.getInstance().getTimeInMillis()));
					pd.addProductAttach(attach, attachFile);
				}
			} else {
				if (attachFile == null || attachFile.equals("")) {
					// Delete attachment
					pd.deleteProductAttach(attach);
				} else {
					// Update attachment
					attach.setAttachCode(attachCode);
					attach.setUpdateBy(principal.getUserCode());
					attach.setUpdateDate(new Date(Calendar.getInstance().getTimeInMillis()));
					pd.updateProductAttach(attach, attachFile);
				}
			}
		} catch (Exception e) {
			Log.error(e);
		}
	}

}
