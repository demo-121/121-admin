package com.eab.biz.products;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.products.Products;
import com.eab.json.model.Dialog;
import com.eab.json.model.DialogItem;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Trigger;
import com.eab.json.model.Field.PresentationTypes;
import com.eab.json.model.Field.Types;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class ProductPropertyMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;

	public ProductPropertyMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public void addSection(List<Field> sections, JsonObject values, String prodCode, int version, boolean isReadOnly){
		try{
			Log.debug("---------------------[ProductPropertyMgr - start adding section]--------------------------");
			List<Field> prodPropertiesItems = new ArrayList<Field>();
	
			
			// feature item - property
			List<Option> propertyOpts = new ArrayList<Option>();
			propertyOpts.add(new Option(langObj.getNameDesc("SUM_IND_CHANGE"), "sumInsChangeable"));
			propertyOpts.add(new Option(langObj.getNameDesc("WHOLE_LIFE_IND"), "wholeLifeInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("MED_IND"), "medInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("UTLK_IND"), "utlkInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("UVLF_IND"), "uvlfInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("OCCUP_CLASS_IND"), "occupClassInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("BAND_IND"), "bandInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("DIV_IND"), "divInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("PUA_IND"), "puaInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("RPU_IND"), "rpuInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("ANU_IND"), "anuInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("CSV_IND"), "csvInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("COU_IND"), "couInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("DB_IND"), "dbInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("SB_IND"), "sbInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("RB_IND"), "rbInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("PB_IND"), "pbInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("MB_IND"), "mbInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("NF_IND"), "nfInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("SUB_IND"), "subInd"));
			propertyOpts.add(new Option(langObj.getNameDesc("LB_IND"), "lbInd"));
			prodPropertiesItems.add(new Field("prodProperties", Types.CHECKBOXGROUP, Types.NUMBER, langObj.getNameDesc("PROD_PROPERTIES"), 1100, "100%", PresentationTypes.COLUMN2, false, isReadOnly, propertyOpts, null, null));
			Field sectionProdProperties = new Field(Constants.ProductPage.PROPERTIES, Types.SECTION, null, true, prodPropertiesItems);
	
			sections.add(sectionProdProperties);
			
			Log.debug("---------------------[ProductPropertyMgr - end adding section]--------------------------");
			Log.debug("---------------------[ProductPropertyMgr - start adding values]--------------------------");
			Products prodDAO = new Products();
			String sectionId = Constants.ProductPage.PROPERTIES;
			JSONObject prodSection = prodDAO.getProdSections(prodCode, principal, version, sectionId).get(sectionId);
			if(prodSection != null){
				values.add(sectionId, JsonConverter.convertObj(prodSection));
			}
			Log.debug("---------------------[ProductPropertyMgr - end adding values]--------------------------");
		}catch(Exception e){
			Log.error(e);
		}
	}
	
	

}
