package com.eab.biz.products;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.benefitIllustraions.BenefitIllustrations;
import com.eab.dao.products.Products;
import com.eab.json.model.Condition;
import com.eab.json.model.Dialog;
import com.eab.json.model.DialogItem;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Trigger;
import com.eab.json.model.Field.PresentationTypes;
import com.eab.json.model.Field.Types;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class ProductFeatureMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;

	public ProductFeatureMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public void addSection(List<Field> sections, JsonObject values, String prodCode, int version, boolean isReadOnly){
		try{
			Log.debug("---------------------[ProductFeatureMgr - start adding section]--------------------------");
			List<Field> prodFeatureItems = new ArrayList<Field>();
	
			// feature item - currency
			List<Option> currencyOpts = new ArrayList<Option>();
			currencyOpts.add(new Option("EUR", "EUR"));
			currencyOpts.add(new Option("GBP", "GBP"));
			currencyOpts.add(new Option("HKD", "HKD"));
			currencyOpts.add(new Option("MOP", "MOP"));
			currencyOpts.add(new Option("RMB", "RMB"));
			currencyOpts.add(new Option("USD", "USD"));
			List<Field> currencyItems = new ArrayList<Field>();
	
			List<Field> currencyDialogItems = new ArrayList<>();
			currencyDialogItems.add(new DialogItem("ccy", Types.CHECKBOXGROUP, null, true, currencyOpts));
			Dialog currencyDialog = new Dialog("currencyDialog", langObj.getNameDesc("PRODUCT_CURRENCY"), langObj.getNameDesc("CCY_SELECTION"), new Field("currency_save", "saveValueButton", "OK"), new Field("currency_cancel", "button", "cancel"), currencyDialogItems, 40);
			currencyItems.add(new Field(null, "button", langObj.getNameDesc("ADD"), currencyDialog));
			prodFeatureItems.add(new Field("ccy", Types.SELECTLIST, null, langObj.getNameDesc("PRODUCT_CURRENCY"), 100, "100%", null, true, isReadOnly, currencyOpts, currencyItems, new Trigger("removeIfContains", "saMultiple", "Multiplier"), null, null));
	
			// feature item - smoking
			List<Option> smokingOpts = new ArrayList<Option>();
			smokingOpts.add(new Option(langObj.getNameDesc("NON_SMOKER"), "nonSmokeInd"));
			smokingOpts.add(new Option(langObj.getNameDesc("SMOKER"), "smokeInd"));
			prodFeatureItems.add(new Field("smoking_habit", Types.CHECKBOXGROUP, null, langObj.getNameDesc("SMOKING_HABIT"), 200, "100%", PresentationTypes.COLUMN4, true, isReadOnly, smokingOpts, null, null));
	
//			List<Option> ageOpts = new ArrayList<Option>();
//			for (int i = 0; i < 100; i++) {
//				ageOpts.add(new Option(String.valueOf(i), String.valueOf(i)));
//			}
			int ageMax = 999;
			int ageMin = 0;
	
			// feature item - entry age
			List<Field> entryAgeItems = new ArrayList<Field>();
			entryAgeItems.add(new Field("staAgeMale", Types.TEXT, Types.NUMBER, langObj.getNameDesc("MALE_LIFE_INSURED") + " (" + langObj.getNameDesc("START") + ") ", 301, null, null, false, isReadOnly, null, null, new Trigger("updDfValue", "endAgeMale"), ageMin, ageMax));
			entryAgeItems.add(new Field("endAgeMale", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("MALE_LIFE_INSURED") + " (" + langObj.getNameDesc("END") + ") ", 302, null, null, false, isReadOnly, null, null, new Trigger("updDfOption", "staAgeMale"), ageMin, ageMax));
			entryAgeItems.add(new Field("staAgeFemale", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("FEMALE_LIFE_INSURED") + " (" + langObj.getNameDesc("START") + ") ", 303, null, null, false, isReadOnly, null, null, new Trigger("updDfValue", "endAgeFemale"), ageMin, ageMax));
			entryAgeItems.add(new Field("endAgeFemale", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("FEMALE_LIFE_INSURED") + " (" + langObj.getNameDesc("END") + ") ", 304, null, null, false, isReadOnly, null, null, new Trigger("updDfOption", "staAgeFemale"), ageMin, ageMax));
			entryAgeItems.add(new Field("ownerStaAge", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("MALE_POLICYHOLDER") + " (" + langObj.getNameDesc("START") + ") ", 305, null, null, false, isReadOnly, null, null, new Trigger("updDfValue", "ownerStaAge"), ageMin, ageMax));
			entryAgeItems.add(new Field("ownerEndAge", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("MALE_POLICYHOLDER") + " (" + langObj.getNameDesc("END") + ") ", 306, null, null, false, isReadOnly, null, null, new Trigger("updDfOption", "ownerEndAge"), ageMin, ageMax));
			entryAgeItems.add(new Field("ownerFemaleStaAge", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("FEMALE_POLICYHOLDER") + " (" + langObj.getNameDesc("START") + ") ", 307, null, null, false, isReadOnly, null, null, new Trigger("updDfValue", "ownerFemaleEndAge"), ageMin, ageMax));
			entryAgeItems.add(new Field("ownerFemaleEndAge", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("FEMALE_POLICYHOLDER") + " (" + langObj.getNameDesc("END") + ") ", 308, null, null, false, isReadOnly, null, null, new Trigger("updDfOption", "ownerFemaleStaAge"), ageMin, ageMax));
	
			prodFeatureItems.add(new Field("entryAge", Types.HBOX, null, langObj.getNameDesc("ENTRY_AGE"), 300, "100%", PresentationTypes.COLUMN4, false, false, null, entryAgeItems, null));
	
			// feature item - premium maturity
			List<Field> premiumMaturityItems = new ArrayList<Field>();
			premiumMaturityItems.add(new Field("premMataMale", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("MALE_AGE"), 401, null, null, false, isReadOnly, null, null, null, ageMin, ageMax));
			premiumMaturityItems.add(new Field("premMataFemale", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("FEMALE_AGE"), 402, null, null, false, isReadOnly, null, null, null, ageMin, ageMax));
			premiumMaturityItems.add(new Field("maleTerm", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("MALE_TERM"), 403, null, null, false, isReadOnly, null, null, null, ageMin, ageMax));
			premiumMaturityItems.add(new Field("femaleTerm", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("FEMALE_TERM"), 404, null, null, false, isReadOnly, null, null, null, ageMin, ageMax));
			prodFeatureItems.add(new Field("premiumMaturity", Types.HBOX, null,  langObj.getNameDesc("PREMIUM_MATURITY"), 400, "100%", PresentationTypes.COLUMN4, false, false, null, premiumMaturityItems, null));
	
			// feature item - policy maturity
			List<Field> policyMaturityItems = new ArrayList<Field>();
			policyMaturityItems.add(new Field("polMataMale", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("MALE_AGE"), 501, null, null, false, isReadOnly, null, null, null, ageMin, ageMax));
			policyMaturityItems.add(new Field("polMataFemale", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("FEMALE_AGE"), 502, null, null, false, isReadOnly, null, null, null, ageMin, ageMax));
			policyMaturityItems.add(new Field("maleTerm", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("MALE_TERM"), 503, null, null, false, isReadOnly, null, null, null, ageMin, ageMax));
			policyMaturityItems.add(new Field("femaleTerm", Types.TEXT, Types.NUMBER,  langObj.getNameDesc("FEMALE_TERM"), 504, null, null, false, isReadOnly, null, null, null, ageMin, ageMax));
			prodFeatureItems.add(new Field("policyMaturity", Types.HBOX, null, langObj.getNameDesc("POLICY_MATURITY"), 500, "100%", PresentationTypes.COLUMN4, false, false, null, premiumMaturityItems, null));
	
			// feature item - input
			List<Option> inputOpts = new ArrayList<Option>();
			inputOpts.add(new Option(langObj.getNameDesc("SUM_ASSURED"), "sumInd"));
			inputOpts.add(new Option(langObj.getNameDesc("PREMIUM"), "premInputInd"));
			inputOpts.add(new Option(langObj.getNameDesc("CLASS"), "classInd"));
			inputOpts.add(new Option(langObj.getNameDesc("LOADING"), "loadInd"));
			prodFeatureItems.add(new Field("input", Types.CHECKBOXGROUP, null, langObj.getNameDesc("INPUT"), 600, "100%", PresentationTypes.COLUMN4, false, isReadOnly, inputOpts, null, null));
	
			// feature item - sum assured multiplier
			List<Field> sumAssuredMultiplierItems = new ArrayList<Field>();
			int ccyCnt=0;
			for(Option ccyOpt : currencyOpts){
				String ccyVal=ccyOpt.getValue();
				sumAssuredMultiplierItems.add(new Field(ccyVal.toLowerCase()+"Multiplier", Types.TEXT, Types.NUMBER, langObj.getNameDesc("MULT_"+ccyVal.toUpperCase()), 701+ccyCnt, null, null, false, isReadOnly, null, null, new Trigger("showIfContains", "ccy", ccyVal.toUpperCase())));
				ccyCnt++;	
			}
			
			prodFeatureItems.add(new Field("saMultiple", Types.HBOX, null, langObj.getNameDesc("SUM_ASSURED_MULTIPLIER"), 700, "100%", PresentationTypes.COLUMN4, false, false, null, sumAssuredMultiplierItems, null));
	
			// feature item - payment mode
			List<Option> paymentModeOpts = new ArrayList<Option>();
			paymentModeOpts.add(new Option(langObj.getNameDesc("SINGLE"), "singlePremInd"));
			paymentModeOpts.add(new Option(langObj.getNameDesc("MONTHLY"), "monthlyPremInd"));
			paymentModeOpts.add(new Option(langObj.getNameDesc("QUARTERLY"), "quarterlyPremInd"));
			paymentModeOpts.add(new Option(langObj.getNameDesc("SEMI_ANNUALLY"), "semiAnnuallyPremInd"));
			paymentModeOpts.add(new Option(langObj.getNameDesc("ANNUALLY"), "annually"));
			prodFeatureItems.add(new Field("paymentMode", Types.CHECKBOXGROUP, Types.NUMBER, langObj.getNameDesc("PAYMENT_MODE"), 800, "100%", PresentationTypes.COLUMN4, true, isReadOnly, paymentModeOpts, null, null));
	
			// feature item - premium type
			List<Option> premiumTypeOpts = new ArrayList<Option>();
			premiumTypeOpts.add(new Option(langObj.getNameDesc("LEVEL"), "L"));
			premiumTypeOpts.add(new Option(langObj.getNameDesc("RENEWAL"), "R"));
			prodFeatureItems.add(new Field("levelPrem", Types.RADIOGROUP, Types.NUMBER, langObj.getNameDesc("PREMIUM_TYPE"), 900, "100%", PresentationTypes.COLUMN4, true, isReadOnly, premiumTypeOpts, null, null));
			
			// feature item - Projected Investment Return Rate
			//List<Field> returnRateItems = new ArrayList<Field>();
			//returnRateItems.add(new Field("pirrHigh", Types.TEXT, Types.NUMBER, langObj.getNameDesc("HIGH") + " (%)", 1001, null, "0.00", false, isReadOnly, null, null, null));
			//returnRateItems.add(new Field("pirrLow", Types.TEXT, Types.NUMBER, langObj.getNameDesc("LOW") + " (%)", 1002, null, "0.00", false, isReadOnly, null, null, null));
			//prodFeatureItems.add(new Field("returnRates", Types.HBOX, Types.NUMBER, langObj.getNameDesc("PROJECTED_INVESTMENT_RETURN_RATE"), 1000, "100%", PresentationTypes.COLUMN4, false, false, null, returnRateItems, null));
			
			//BI Template	
			List<Field> biList = new ArrayList<Field>();
			BenefitIllustrations bi = new BenefitIllustrations ();
			List<Option> biTemplateOpts = bi.getBITemplate(principal,  version); 	
			biList.add(new Field("biTemplate", Types.PICKER, null,langObj.getNameDesc("BI_TEMPLATE"), 1100, "100%", null , false, false, biTemplateOpts, null, null, null));		
			prodFeatureItems.add(new Field("biTemplateTitle", Types.HBOX, null, langObj.getNameDesc("BI_TEMPLATE_TITLE"), 1050, "100%", null, false, false, null, biList, null));
	
			Field sectionProdFeature = new Field(Constants.ProductPage.FEATURES, Types.SECTION, null, true, prodFeatureItems);
	
			sections.add(sectionProdFeature);
			
			Log.debug("---------------------[ProductFeatureMgr - end adding section]--------------------------");
			Log.debug("---------------------[ProductFeatureMgr - start adding values]--------------------------");
			String sectionId = Constants.ProductPage.FEATURES;
			Products prodDAO = new Products(); 
			JSONObject prodSection = prodDAO.getProdSections(prodCode, principal, version, sectionId).get(sectionId);
			if(prodSection != null){
				values.add(sectionId, JsonConverter.convertObj(prodSection));
			}
			Log.debug("---------------------[ProductFeatureMgr - end adding values]--------------------------");
		}catch(Exception e){
			Log.error(e);
		}
	}
	
	

}
