package com.eab.biz.webSettings;

import com.eab.common.*;
import com.eab.couchbase.CBServer;
import com.eab.dao.dynamic.DynamicDAO;
import com.eab.dao.webSettings.WebSettingsDao;
import com.eab.database.jdbc.DBAccess;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Response;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public class WebSettingsManager {

	Connection conn = null;
	DynamicDAO dynamic = new DynamicDAO();
	HttpServletRequest request = null;
	HttpSession session = null;
	UserPrincipalBean principal = null;
	
	String module = "websettings";

	public WebSettingsManager(HttpServletRequest request) {
		this.request = request;
		this.session = request.getSession();
		
		String _module = request.getParameter("p1");
		if (_module != null) {
			module = _module;
		}
		
		try {
			conn = DBAccess.getConnection();
		} catch (SQLException e) {
		}

		principal = Function.getPrincipal(request);
	}
	
	public void close() {
		try {
			if (conn != null && !conn.isClosed())
				conn.close();
		} catch (Exception e) {
		}
	}

	public Response getTableView() {
		
		DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
		if (ui == null || !module.equals(ui.getModule())) {
			ui = dynamic.getDynamicUI(module, conn);
			session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
		}
		
		ui.getMasterView();

		return null;
	}

	private CBServer getCBServer() throws Exception {
		String gatewayURL = EnvVariable.get("GATEWAY_URL");
		String gatewayPort = EnvVariable.get("GATEWAY_PORT");
		String gatewayDBName = EnvVariable.get("GATEWAY_DBNAME");
		String gatewayUser = EnvVariable.get("GATEWAY_USER");
		String gatewayPW = EnvVariable.get("GATEWAY_PW");
		CBServer cbServer = new CBServer(gatewayURL,gatewayPort,gatewayDBName,gatewayUser,gatewayPW);
		if (!cbServer.checkDBConnection()) {
			throw new Exception("Server Connection Fail");
		}
		return cbServer;
	}

	public Response publish(JSONObject data) {
		Response resp = null;
		Dialog dialog = null;

		if (data.has("rows")) {
			JSONArray names = data.getJSONArray("rows");
			JSONObject settings = null;

			try {
				WebSettingsDao wsDao = new WebSettingsDao();
				CBServer cbServer = getCBServer();

				for (int r = 0; r < names.length(); r++) {
					String settingName = names.getString(r);
					
					settings = wsDao.getSettings(principal, settingName);
					
					try {
						List<Field> outputs = null;
						if ("productelg".equals(module)) {
							outputs = publishEligProducts(cbServer, settings);
						} else if ("agentcert".equals(module)) {
							outputs = publishAchievements(cbServer, settings);
						}

						dialog = new Dialog("result", "WEBSETGS.PUBLISH.TITLE", "WEBSETGS.PUBLISH.SUCCESS.MSG", null, new Field("ok", "button", "OK"), outputs, 40);
					} catch (Exception ex) {
						Log.error(ex);
						dialog = new Dialog("error", "WEBSETGS.PUBLISH.TITLE", "WEBSETGS.PUBLISH.FAILURE.MSG", null, new Field("ok", "button", "OK"), null, 30);
					}
				}


			} catch(Exception ex) {
				Log.error(ex);
				dialog = new Dialog("error", "WEBSETGS.PUBLISH.TITLE", "WEBSETGS.PUBLISH.FAILURE.MSG", null, new Field("ok", "button", "OK"), null, 30);
			}
		} else {
		}

		
		try {
			Function2.translateField(request, dialog);
		} catch (Exception ex) {
			Log.error(ex);
		}
		
		// Token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		resp = new Response(Constants.ActionTypes.SHOW_DIALOG, tokenID, tokenKey, dialog);
			
		return resp;
	}

	private List<Field> publishEligProducts(CBServer cbServer, JSONObject settings) throws Exception {
		JSONObject raw = settings.getJSONObject("raw");
		Log.debug("validate obj:" + raw.toString(2));

		JSONObject setObj = new JSONObject();
		setObj.put("type", settings.getString("type"));

		List<Field> outputs = new ArrayList<>();
		Set<String> mainSet = new HashSet<>();
		JSONArray validList = doValidate(cbServer, module, raw, mainSet, outputs);

		JSONObject mappingObj = new JSONObject();
		Map<String, Set<String>> covCodeByNRIC1 = new HashMap<>();
		Map<String, Set<String>> covCodeByNRIC2 = new HashMap<>();

		if (validList != null) {
			for (int i = 0; i < validList.length(); i++) {
				JSONObject record = validList.getJSONObject(i);
				String chanId = record.getString("2");

				if ("AG".equals(chanId)) {
					String nric = record.getString("0");
					String covCode = record.getString("1");

					if (covCodeByNRIC1.containsKey(nric)) {
						covCodeByNRIC1.get(nric).add(covCode);
					} else {
						Set<String> strSet = new HashSet<>(Arrays.asList(covCode));
						covCodeByNRIC1.put(nric, strSet);
					}
				} else if ("FA".equals(chanId)) {
					String nric = record.getString("0");
					String covCode = record.getString("1");

					if (covCodeByNRIC2.containsKey(nric)) {
						covCodeByNRIC2.get(nric).add(covCode);
					} else {
						Set<String> strSet = new HashSet<>(Arrays.asList(covCode));
						covCodeByNRIC2.put(nric, strSet);
					}
				}
			}

			mappingObj.put("AG", covCodeByNRIC1);
			mappingObj.put("FA", covCodeByNRIC2);
			setObj.put("mapping", mappingObj);
		}
		String docId = "eligProducts";
		int updatedCount = 0, failuredCount = 0;
		StringBuilder sb = new StringBuilder();
		try {
			String updResult = cbServer.UpdateDoc(docId, setObj.toString());
			if (updResult == cbServer.UPDATE_SUCCESS) {
				updatedCount++;
			} else {
				failuredCount++;
				sb.append("Failure to publish for " + docId+ "</br>");
			}
		} catch (Exception ex) {
			failuredCount++;
			Log.error(ex);
			sb.append("Failure to publish for " + docId+ "</br>");
		}

		outputs = new ArrayList<>();
		outputs.add(new Field("msg", "PARA", updatedCount + " record published, "+ failuredCount+ " records failure"));
		outputs.add(new Field("errors", "PARA", sb.toString()));
		return outputs;
	}

	private List<Field> publishAchievements(CBServer cbServer, JSONObject settings) throws Exception {
		JSONObject raw = settings.getJSONObject("raw");
		Log.debug("validate obj:" + raw.toString(2));

		List<Field> outputs = new ArrayList<>();
		Set<String> mainSet = new HashSet<>();
		JSONArray validList = doValidate(cbServer, module, raw, mainSet, outputs);

		Map<String, JSONArray> shortList = new HashMap<>();
		if (validList != null) {
			for (int i = 0; i < validList.length(); i++) {
				JSONObject record = validList.getJSONObject(i);
				String recId = record.getString("0");
				String value = record.getString("1");

				if ("999999999".equals(recId)) {
					for (String key : mainSet) {
						if (shortList.containsKey(key)) {
							shortList.get(key).put(value);
						} else {
							JSONArray arr = new JSONArray();
							arr.put(value);
							shortList.put(key, arr);
						}
					}
				} else {
					if (shortList.containsKey(recId)) {
						shortList.get(recId).put(value);
					} else {
						JSONArray arr = new JSONArray();
						arr.put(value);
						shortList.put(recId, arr);
					}
				}
			}
		}

		int updatedCount = 0, failuredCount = 0;
		StringBuilder sb = new StringBuilder();
		Log.info("Total User in DB:"+ mainSet.size());
		Log.info("Total Valid record:"+ shortList.size());
		for (String key : mainSet) {
			if ("agentcert".equals(module)) {
				String docId = "UX_"+key;
				JSONArray value = shortList.get(key);
				if (value == null) {
					value = new JSONArray();
				}

				JSONObject agentUX = cbServer.getDoc(docId);

				if (agentUX == null) {
					agentUX = new JSONObject();
					agentUX.put("type", "userExt");
				}

				if ("agentcert".equals(module)) {
					agentUX.put("achievements", value);
				}

				try {
					String updResult = cbServer.UpdateDoc(docId, agentUX.toString());
					if (updResult == cbServer.UPDATE_SUCCESS) {
						updatedCount++;
					} else {
						failuredCount++;
						sb.append("Failure to publish for " + key+ "</br>");
					}
				} catch (Exception ex) {
					failuredCount++;
					Log.error(ex);
					sb.append("Failure to publish for " + key+ "</br>");
				}
			}
		}

		outputs = new ArrayList<>();
		outputs.add(new Field("msg", "PARA", updatedCount + " record published, "+ failuredCount+ " records failure"));
		outputs.add(new Field("errors", "PARA", sb.toString()));
		return outputs;
	}

	public void addExtraTableProps(Response resp) {
		resp.getContent().getTemplate().setSubType("selectable");
		resp.getPage().setId("/WebSettings");

		try {
			// add pub button on 2nd actions list
			Field pubButton = new Field("/WebSettings/Publish", "submitSelectedButton", "Publish");

			List<Field> actions2 = new ArrayList<Field>();
			actions2.add(pubButton);
			resp.getAppBar().getActions().add(actions2);			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public Response validate(JSONObject data) {
		
		Response resp = null;
		Dialog dialog = null;
		Content content = null;
		
		try {
			String gatewayURL = EnvVariable.get("GATEWAY_URL");
	        String gatewayPort = EnvVariable.get("GATEWAY_PORT");
	        String gatewayDBName = EnvVariable.get("GATEWAY_DBNAME");
	        String gatewayUser = EnvVariable.get("GATEWAY_USER");
	        String gatewayPW = EnvVariable.get("GATEWAY_PW");
			CBServer cbServer = new CBServer(gatewayURL,gatewayPort,gatewayDBName,gatewayUser,gatewayPW);
			if (cbServer.checkDBConnection() == false)
				throw new Exception("Server Connection Fail");
		
			if (data.has("values")) {
				Log.debug("validate obj:" + data.toString(2));
				
					JSONObject raw = data.getJSONObject("values");
					try {
						List<Field>  items = new ArrayList<Field> ();
						Set<String> mainSet = new HashSet<String> ();
						JSONArray validList = doValidate(cbServer, module, raw, mainSet, items);
						
						JSONArray newValues = new JSONArray();
						
						newValues.put(raw.getJSONArray("values").getJSONObject(0));
						
						for (int v = 0; v < validList.length(); v++) {
							newValues.put(validList.getJSONObject(v));
						}
						
						raw.put("values", newValues);
						Gson gson = new Gson();
						content = new Content(gson.fromJson(raw.toString(), JsonObject.class));
						
						dialog = new Dialog("result", "WEBSETGS.VALIDATE.TITLE", "", null, new Field("ok", "button", "OK"), items, 40);
						
					} catch (Exception ex) {
						Log.error(ex);
						dialog = new Dialog("error", "WEBSETGS.VALIDATE.TITLE", ex.getMessage(), null, new Field("ok", "button", "OK"), null, 30);
					}
//				}
			} else {
				Log.debug("validate obj: 2" + data.toString(2));
				dialog = new Dialog("error","WEBSETGS.VALIDATE.TITLE",  "Invaid format.", null, new Field("ok", "button", "OK"), null, 30);
			}
			

		} catch (Exception ex) {
//			ex.printStackTrace();
			Log.error(ex);
			dialog = new Dialog("error","WEBSETGS.VALIDATE.TITLE",  "Invaid environment.", null, new Field("ok", "button", "OK"), null, 30);			
		}
		
		try {
			Function2.translateField(request, dialog);
		} catch (Exception ex) {
			Log.error(ex);
		}
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		
		dialog.translate(request);
		
		resp = new Response(Constants.ActionTypes.SHOW_DIALOG, tokenID, tokenKey, dialog);
		if (content != null) {
			resp.setContent(content);
		}		
		return resp;
	}

	private JSONArray doValidate(CBServer cbServer, String module, JSONObject raw, Set<String> set1, List<Field> output) throws Exception {
		
		int totalCount = 0;
		int invalidCount = 0;
		StringBuilder sb = new StringBuilder();
		JSONArray values = raw.getJSONArray("values");
//		JSONArray titles = raw.getJSONArray("titles");
		
		JSONObject view1 = null;
		JSONArray list1 = null;
		
		Set<String> set2 = null;
		JSONArray validList = new JSONArray();

		try {
//		Dialog dialog;
			
			if ("productelg".equals(module) || "agentcert".equals(module)) {
				view1 = cbServer.queryView("main", "agents", null, null);
				if (view1.has("rows")) {
					Log.info("query result:"+ view1.getInt("total_rows"));
					list1 = view1.getJSONArray("rows");
					for (int i = 0; i < list1.length(); i++) {
						String code = null;
						try {
							code = list1.getJSONObject(i).getJSONObject("value").getString("profileId");
							set1.add(code);
						} catch(Exception ex) {
							// ignored
						}
					}
				}
			}
			
			if ("productelg".equals(module)) {
				view1 = cbServer.queryView("main", "products", "[\""+principal.getCompCode()+"\",\"B\",\"0\"]", "[\""+principal.getCompCode()+"\",\"B\",\"ZZZZZ\"]");
				if (view1.has("rows")) {
					Log.info("query result:"+ view1.getInt("total_rows"));
					list1 = view1.getJSONArray("rows");
					set2 =  new HashSet<String>();
					for (int i = 0; i < list1.length(); i++) {
						String code = null;
						try {
							code = list1.getJSONObject(i).getJSONObject("value").getString("planCode");
							if (StringUtils.isEmpty(code)) {
								code = list1.getJSONObject(i).getJSONObject("value").getString("covCode");
							}
						} catch (Exception ex) {
							code = list1.getJSONObject(i).getJSONObject("value").getString("covCode");
						}
						if (code != null) {
							set2.add(code);
						}
					}
				}
			}
			
		} catch (Exception ex) {
			throw new Exception("Invalid formal");
		}
		
		if (set1 != null && set1.size()>0) {
			totalCount = values.length();
			for (int i = 1; i < totalCount; i++) {
				JSONObject record = values.getJSONObject(i);
				if ("productelg".equals(module)) {
					if (!record.has("0") || !record.has("1")) {
						invalidCount ++;
						sb.append("Invalid NRIC or Plan code at row "+(i+1)+"<br/>");
					} else {
						String docId = record.getString("0");
						String prodId = record.getString("1");
						if (docId.equals("") || prodId.equals("")) {
							invalidCount ++;
							sb.append("Invalid NRIC or Plan code at row "+(i+1)+"<br/>");
						} else {
							
							if (record.has("2")) {
								String channelId = record.getString("2");
								String[] validChannelList = {"AG","FA",""}; 
								if (Arrays.asList(validChannelList).contains(channelId.trim())) {
									if ("".equals(channelId.trim())) {
										record.put("2", "AG");
									} else {
										record.put("2", channelId.trim());
									}
									record.put("0", docId.trim());
									record.put("1", prodId.trim());
									validList.put(record);
								} else {
									invalidCount ++;
									sb.append("Channel "+channelId+" is invalid at row "+(i+1)+"<br/>");
								}
							} else {
								record.put("2", "AG");
								record.put("0", docId.trim());
								record.put("1", prodId.trim());
								validList.put(record);
							}
						}
					}
				} else {
					validList.put(record);
				}	
			}
			
			output.add(new Field("msg", "PARA", validList.length() + " record uploaded, "+ invalidCount+ " records skipped"));
			
			output.add(new Field("errors", "PARA", sb.toString()));
		} else {
			throw new Exception("Failure to retrieve vaildation map." + module);
		}
		
		return validList;
	}
	
}
