package com.eab.biz.needs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.dao.needs.Needs;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class NeedTabMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	
	public NeedTabMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public void addValues(JsonObject contentValues, String needCode, int version, String channelCode) throws Exception {		
		 
		////testing-needstab
		 
		////needsLandingTitle
		String[] needsLandingTitle= {
				"learnMore","recommendProducts"				
		};
		String[] needsLandingTitleItem= {
				"title", "content", "button"
		};
		
		 
		String[][] needsLandingTitleValue = new String[][]{
				  {"HEADLINE", "HEADLINE"  },
				  { "DESCRIPTION", "DESCRIPTION"},
				  {"LEARN_MORE", "RECOMMEND_PRODUCTS"  },
		};
		
		//needsLandingContent
		String[] needsLandingContent= {
				"finanEval","needsAnal", "riskAssess"		
		};
		String[] needsLandingContentItem= {				
				"title", 
				"content" , 
				"button", 
				"bgImage", 
				"bgColor"
		};
		
		String[][] needsLandingContentValue = new String[][]{
			  {"FINANCIAL_EVALUATION", "NEEDS_ANALYSIS", "RISK_ASSESSMENT" },
			  { "DESCRIPTION", "DESCRIPTION", "DESCRIPTION" },
			  { "START", "START", "START" },
			  { "needsFinancialEvaluation", "needsAnalysis", "needsRiskAssessment" },
			  { "#E6E2DF", "#D1DCE6", "#BFCFC8" },
		};
		String[] attachList= {
				"needsFinancialEvaluation", "needsAnalysis", "needsRiskAssessment"		
		};
				 
		//JsonObject valuesSet= new JsonObject();
		Needs needsDAO = new Needs();
		Map<String, JSONObject> _sections = needsDAO.getNeedsSections(needCode, principal, version, Constants.NeedPage.NEEDS_TAB, channelCode);
		Map<String, Object> _attachs = needsDAO.getNeedsAttachContent(needCode, principal, version, channelCode, Constants.NeedPage.NEEDS_TAB);
		JsonObject valuesSet = null;
		JsonObject attachsSet= new JsonObject();
		//load image from db
		if(_attachs !=null  ){
			for (Entry<String, Object> entry : _attachs.entrySet()) {
			    String key = entry.getKey();			
			    
			    Blob blob = (Blob)_attachs.get(key);
				byte[] bdata = blob.getBytes(1, (int) blob.length());
				String b64String = new String(bdata);
				attachsSet.remove(key);
			    attachsSet.add(key, gson.toJsonTree(b64String));
			}	
		}
		
	     for(int i=0; i<attachList.length;i++){
	          if(!attachsSet.has(attachList[i])){
	              attachsSet.add(attachList[i], gson.toJsonTree("null"));
	          }
	     }

		if( _sections !=null && _sections.containsKey(Constants.NeedPage.NEEDS_TAB)){
			valuesSet = JsonConverter.convertObj(_sections.get(Constants.NeedPage.NEEDS_TAB)).getAsJsonObject(); 
		}else{						
			valuesSet = new JsonObject(); 
			initContentValues(valuesSet,needsLandingTitle, needsLandingTitleItem,needsLandingTitleValue, "needsLandingTitle");
			initContentValues(valuesSet,needsLandingContent, needsLandingContentItem,needsLandingContentValue, "needsLandingContent");
			
			needsDAO.saveNeedSection(needCode, version, channelCode, Constants.NeedPage.NEEDS_TAB, new  JSONObject (valuesSet.toString()), principal);
			
		}		
		
		contentValues.add(Constants.NeedPage.NEEDS_TAB, valuesSet); 
		contentValues.add(Constants.NeedPage.ATTACHMENTS, attachsSet);
	}
	 
	private JsonObject initContentValues(JsonObject valuesSet, String[] sections, String[] sectionsItem, String[][] sectionsValue, String needsLandingKey){
		JsonArray needsLandingJSA =new JsonArray(  );
		for(int i=0; i<sections.length; i++){
			JsonObject values = new JsonObject();
			for(int j=0; j<sectionsItem.length; j++){	
				String item=sectionsItem[j];
				 if(item.equals("bgImage")||item.equals("bgColor")){
					 values.add(item, gson.toJsonTree(sectionsValue[j][i]));					  
				 }else
					values.add(item, createConfigVal(langObj.getNameDesc(sectionsValue[j][i]) ));
			}			
			valuesSet.add(sections[i], values);
			//Mapping for needLandingTitle/needLandingContent section
			values.add("id",  gson.toJsonTree(sections[i]));
			JsonObject tmpJSO = new JsonObject();
			tmpJSO.add( sections[i], gson.toJsonTree(""));
			needsLandingJSA.add(tmpJSO);
		}
		valuesSet.add(needsLandingKey, needsLandingJSA);
		return valuesSet;
		
	}
	
	private JsonObject createConfigVal(String value){		
		 
		JsonObject titleValue = new JsonObject();
		titleValue.add("en", gson.toJsonTree(""));
		titleValue.add("zh-Hans", gson.toJsonTree(""));
		titleValue.add("zh-Hant", gson.toJsonTree(""));		 
		return titleValue;
	}
	
}
