package com.eab.biz.needs;

import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.codec.binary.Base64;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.biz.dynamic.CompanyManager;
import com.eab.biz.upload.UploadManager;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.needs.Needs;
import com.eab.dao.tasks.Tasks;
import com.eab.json.model.UploadResponse;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.tasks.TaskBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class NeedAttachmentMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;

	public NeedAttachmentMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}

	public String getparam(String key){
		String value = request.getParameter(key);
		
		if (value == null && request.getParameterValues(key) != null) {
			String[] values = request.getParameterValues(key);
			value = values[0];
		}
		
		return value;
	}
	
	//find the attachment file key by walkthrough all items in json
	private void saveAttachmentsByKey(Object sectionJson, JSONObject attachJson, String[] attachKeys, NeedMgr nm, String needCode, int version, String langCode, String channelCode, String sectionCode, List<String> codeArray) throws SQLException{
		if (sectionJson instanceof JSONArray) {
			JSONArray jsa=(JSONArray)sectionJson;
			
			for (int i = 0; i < jsa.length(); i++) {
				Object jso = jsa.get(i);
				saveAttachmentsByKey(jso, attachJson, attachKeys, nm ,needCode, version, langCode, channelCode, sectionCode, codeArray);
			}			  
		} else if (sectionJson instanceof JSONObject) {
			JSONObject jso = (JSONObject)sectionJson;
			Iterator it= jso.keys();
		
			while (it.hasNext()) {	
				String key = (String)it.next();
				Object obj = jso.get(key);
				
				if (obj instanceof JSONObject || obj instanceof JSONArray)
					saveAttachmentsByKey(obj, attachJson, attachKeys, nm, needCode, version, langCode, channelCode, sectionCode, codeArray);
				else if (obj instanceof String){ 						 
					if (Arrays.asList(attachKeys).contains(key)) {
						String attachCode = (String)obj;
						codeArray.add(attachCode);
						
						if (attachJson != null && attachJson.length() > 0 && attachJson.has(attachCode)) 							 
							nm.updateNeedAttachFile(needCode, version, attachCode, attachJson.getString(attachCode), langCode, channelCode, sectionCode); 						
					}	
				}  				  
			} 
		} 
	}

	public UploadResponse saveAttach(){
		UploadResponse response = new UploadResponse();

		String param = request.getParameter("p0");		
		String id = request.getParameter("id");

		if (id != null) {
			byte[] decoded = Base64.decodeBase64(id);
			id = new String(decoded, StandardCharsets.UTF_8);
			id = id.replace("\"", "");
		}

		String module = getparam("module");
		String versionStr = getparam("version");
		String packetNumStr = getparam("packetNum");
		String offsetStr = getparam("offset");

		String needCode=null;
		String channelCode=null;
		String langCode=null; 
		String selectPageId = "";
		String curPageId = ""; 		
		JSONObject mNeedDetail= null; 
		JSONObject attachItem = null;
		TaskBean taskInfo = null;		
		int taskId = -1;
		Map<String, JSONObject> arrMSections = new HashMap<String, JSONObject>();;
		int version = Function.stringToInt(versionStr);
		
		if(version < 1)
			version = 1;
		
		boolean isSkip = false;

		int packetNum = Function.stringToInt(packetNumStr);
		int offset = Function.stringToInt(offsetStr);

		UploadManager um = new UploadManager(request);  
		
		try{		
			if (offset != -1 && packetNum != -1 && um.isOffsetValid(module, id, version, offset)) {
				if (offset == packetNum - 1) {
					//insert into db
					Log.debug("needAttchment: last packet");

					String fullData = null;
			
					if(packetNum == 1)
						fullData = param;
					else
						fullData = um.getAttachmentFromSession(module, id, version, offset) + param;

					Log.debug("version:"+version);			
					
					UserPrincipalBean principal = Function.getPrincipal(request);
					CompanyManager cm = new CompanyManager(request);
					//List<String> langList = cm.getCompanyLangList(principal.getCompCode());
					byte[] decoded = Base64.decodeBase64(fullData.replace(" ", "+"));
					String paraStr = new String(decoded, StandardCharsets.UTF_8);						
					JSONObject resultStr = new JSONObject(paraStr);
					NeedMgr nm = new NeedMgr(request);

					if (resultStr.has("skip") && resultStr.get("skip") instanceof Boolean)
						isSkip = resultStr.getBoolean("skip");

					if (resultStr.has("taskId"))
						taskId = resultStr.getJSONArray("taskId").getInt(0);

					if (resultStr.has("needCode") && resultStr.get("needCode") instanceof String) 
						needCode = resultStr.getString("needCode");					

					if (resultStr.has("version") && resultStr.get("version") instanceof Integer)
						version = resultStr.getInt("version");

					if (resultStr.has("channelCode") && resultStr.get("channelCode") instanceof String)
						channelCode = resultStr.getString("channelCode");

					if (resultStr.has("selectPageId") && resultStr.get("selectPageId") instanceof String)
						selectPageId = resultStr.getString("selectPageId");

					if (resultStr.has("selectPage") && resultStr.get("selectPage") instanceof JSONObject )
						curPageId = resultStr.getJSONObject("selectPage").getString("id");

					needCode = resultStr.getString("needCode");
					channelCode = resultStr.getString("channelCode");

					if (resultStr.has(Constants.NeedPage.TASK)) {
						taskInfo = new TaskBean();
						JSONObject _mTaskInfo = resultStr.getJSONObject(Constants.ProductPage.TASK);

						if (_mTaskInfo.has("task_desc"))
							taskInfo.setTaskDesc(_mTaskInfo.getString("task_desc"));
						
						if (_mTaskInfo.has("ref_no"))
							taskInfo.setRefNo(_mTaskInfo.getString("ref_no"));
						
						if (_mTaskInfo.has("effDate") && _mTaskInfo.get("effDate").getClass() == java.lang.Long.class) {
							Date effDate = new Date(_mTaskInfo.getLong("effDate"));
							taskInfo.setEffDate(effDate);
						}

						if (taskId > 0) {
							taskInfo.setTaskCode(String.valueOf(taskId));
						} else {
							taskInfo.setChannelCode(channelCode);
							taskInfo.setFuncCode(Tasks.FuncCode.NEED);
							taskInfo.setItemCode(needCode);
							taskInfo.setVerNo("1");
						}
					}

					langCode="en";//TODO:NeedsConceptSelling:extract from DB
					NeedMgr needManager = new NeedMgr(request);
					NeedBuilderMgr needBuilderMgr = new NeedBuilderMgr(request);

					if (!isSkip) {
						if (resultStr.has(Constants.NeedPage.ATTACHMENTS))
							attachItem = resultStr.getJSONObject(Constants.NeedPage.ATTACHMENTS);

						if (curPageId.equals(Constants.NeedPage.NEEDS_TAB) && resultStr.has(Constants.NeedPage.NEEDS_TAB)) {	
							//if(attachItem != null){
							JSONObject attachmentLangItem = resultStr.getJSONObject(Constants.NeedPage.NEEDS_TAB); 							
							String[] attachKeys = new String[]{"bgImage"};			
							List<String> codeArray = new ArrayList<String>();
							saveAttachmentsByKey(attachmentLangItem, attachItem,attachKeys, nm, needCode, version, langCode, channelCode, curPageId,  codeArray);
							needBuilderMgr.removeUnusedAttachment(needCode, channelCode, version, langCode, Constants.NeedPage.NEEDS_TAB, codeArray);
							//}

							arrMSections.put(curPageId,resultStr.getJSONObject(Constants.NeedPage.NEEDS_TAB));							

						} else if((curPageId.equals(Constants.NeedPage.NEEDS_FIN_EVAL) || curPageId.startsWith("NAQ"))&&resultStr.has(curPageId)) {
							//if(attachItem != null){
							JSONObject needsFinEvalItems = resultStr.getJSONObject(curPageId); 

							String[] attachKeys = new String[]{"image","attachmentSetting", "leftImage", "rightImage" };			
							List<String> codeArray = new ArrayList<String>();
							saveAttachmentsByKey(needsFinEvalItems, attachItem,attachKeys, nm, needCode, version, langCode, channelCode, curPageId,  codeArray);
							needBuilderMgr.removeUnusedAttachment(needCode, channelCode, version, langCode, curPageId, codeArray);

							//}
							arrMSections.put(curPageId,resultStr.getJSONObject(curPageId));

						} else if (curPageId.equals(Constants.NeedPage.NEEDS_SELECTION) && resultStr.has(Constants.NeedPage.NEEDS_SELECTION)) {

							//if(attachItem != null){
							JSONObject needsSelectionItems = resultStr.getJSONObject(Constants.NeedPage.NEEDS_SELECTION); 							
							String[] attachKeys = new String[]{"image"};
							List<String> codeArray = new ArrayList<String>();
							saveAttachmentsByKey(needsSelectionItems, attachItem,attachKeys, nm, needCode, version, langCode, channelCode, curPageId, codeArray);
							needBuilderMgr.removeUnusedAttachment(needCode, channelCode, version, langCode, curPageId, codeArray);
							//}

							arrMSections.put(curPageId,resultStr.getJSONObject(Constants.NeedPage.NEEDS_SELECTION));	
							JSONObject needSelection= resultStr.getJSONObject(Constants.NeedPage.NEEDS_SELECTION);

							//Handling for Needs Analysis sub menu
							if (needSelection.has("selections") && needSelection.getJSONArray("selections") instanceof JSONArray) {
								JSONArray selections = needSelection.getJSONArray("selections");
								List<String> idList = new ArrayList<String>();

								for(int i = 0; i < selections.length(); i++){
									JSONObject item = selections.getJSONObject(i);                                        

									if (item.has("options") && item.get("options") instanceof JSONArray) {
										JSONArray options = item.getJSONArray("options");  				                                      

										for (int j = 0; j < options.length(); j++){
											JSONObject opt = options.getJSONObject(j);      		                                               

											if (opt.has("id") && opt.get("id")instanceof String && opt.has("title") && opt.get("title") instanceof JSONObject) {		                                            	 
												String menuName = "";
												String status = "A";
												String qId = opt.getString("id");
												JSONObject title = opt.getJSONObject("title");		                                            	  	

												//NeedsAnalysis
												String sectionId = "01"; 	
												String sectionType = "N";
												String menuId = "NA" + qId;
												idList.add(menuId);
												String uplineMenuId = "NeedsAnalysis";
												int disSeq = (j + 1) * 100; 
												needBuilderMgr.saveMenuList(needCode, menuId, sectionId, uplineMenuId, menuId, sectionType, disSeq, status, channelCode, version);		

												if(title.has(langCode)&&title.get(langCode) instanceof String)
													menuName= title.getString(langCode);
												if(menuName.length()==0) 
													menuName=menuId;
												needBuilderMgr.saveMenuName( needCode,    menuId,  langCode, menuName,  status , channelCode, version );
												
												//Questionnaire
												menuId = "NAQ" + qId;
												idList.add(menuId);
												uplineMenuId = "NA" + qId;
												String nameCode = "Questionnaire";
												sectionType = ""; 
												disSeq = 100;
												needBuilderMgr.saveMenuList(needCode, menuId, sectionId, uplineMenuId, nameCode, sectionType, disSeq, status, channelCode, version);

												//Guide
												menuId = "NAG" + qId;
												idList.add(menuId);
												uplineMenuId = "NA" + qId;
												nameCode = "Guide";
												sectionType = ""; 
												disSeq = 200;
												needBuilderMgr.saveMenuList(needCode, menuId, sectionId, uplineMenuId, nameCode, sectionType, disSeq, status, channelCode, version);		    
											}
										}
									}
								}
								//delete  removed questions'attachment, menuId and menuName 
								needBuilderMgr.deleteNeedsAnaylsis(  needCode  ,  channelCode,   version,   langCode,   curPageId,   idList ,  "NA%");
							} 
						} else if (curPageId.equals(Constants.NeedPage.NEEDS_PRIORITIZATION) && resultStr.has(Constants.NeedPage.NEEDS_PRIORITIZATION)){
							arrMSections.put(curPageId,resultStr.getJSONObject(Constants.NeedPage.NEEDS_PRIORITIZATION));							
						} else if (resultStr.has(Constants.NeedPage.NEEDS_CONCEPT_SELLING)){
							//save section
							arrMSections.put(Constants.NeedPage.NEEDS_CONCEPT_SELLING, resultStr.getJSONObject(Constants.NeedPage.NEEDS_CONCEPT_SELLING));

							//save attachments
							String[] attachKeys = new String[]{"image"};			
							List<String> codeArray = new ArrayList<String>();
							JSONObject attachmentLangItem = resultStr.getJSONObject(Constants.NeedPage.NEEDS_CONCEPT_SELLING);	
							saveAttachmentsByKey(attachmentLangItem, attachItem,attachKeys, nm, needCode, version, langCode, channelCode, Constants.NeedPage.NEEDS_CONCEPT_SELLING,  codeArray);

							//remove unused attachment
 							needBuilderMgr.removeUnusedAttachment(needCode, channelCode, version, langCode, curPageId, codeArray);

						} else if(resultStr.has(curPageId)) {								
							JSONObject needsSelectionItems = resultStr.getJSONObject(curPageId);
							
							if (curPageId.startsWith("NAG")) {
								String[] attachKeys=new String[]{"image"};
								List<String> codeArray=new ArrayList<String>();
								saveAttachmentsByKey(needsSelectionItems, attachItem,attachKeys, nm, needCode, version,  langCode, channelCode, curPageId, codeArray);
								needBuilderMgr.removeUnusedAttachment(needCode, channelCode, version, langCode, curPageId, codeArray);
							}

							arrMSections.put(curPageId,resultStr.getJSONObject(curPageId));
						} 
					} 

					String errMsg = (!isSkip)?needBuilderMgr.saveNeed(needCode, version, channelCode, curPageId, taskInfo, mNeedDetail, arrMSections, taskId):"";			
					response.setSelectPageId("".equals(selectPageId)?curPageId:selectPageId);
					response.setItemCode(needCode);
					response.setVersion(version);
					response.setChannelCode(channelCode); 			
					response.setCurPageId(curPageId); 
					////////////////////////////////

					//clear session
					um.clearAttachmentFromSession(module, id, version, offset);

					//return done state
					response.setComplete(true);
					response.setTeminate(true);
				} else {
					Log.debug("saveAttachment:next:next packet");

					String currentData = um.getAttachmentFromSession(module, id, version, offset);
					String newData = null;

					if (currentData != null)
						newData = um.getAttachmentFromSession(module, id, version, offset) + param;
					else
						newData = param;

					um.saveAttachmentOffsetToSession(module, id, version, offset);
					um.saveAttachmentToSession(module, id, version, offset, newData);

					//return the next offset
					int newOffset = offset + 1;
					response.setOffSet(newOffset);
					response.setComplete(false);
					response.setTeminate(false);
				}
			} else {
				response.setComplete(false);
				response.setTeminate(true);
			}
		}catch(Exception e){
			Log.debug("saveAttachment: Exception");
			Log.error(e);
			//clear session
			um.clearAttachmentFromSession(module, id, version, offset);
			response.setComplete(false);
			response.setTeminate(true);
		}

		Log.debug("saveAttachment: done");

		//set token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		response.setTokenID(tokenID);
		response.setTokenKey(tokenKey);
		return response;
	}

}
