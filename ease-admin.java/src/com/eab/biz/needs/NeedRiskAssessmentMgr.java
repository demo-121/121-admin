package com.eab.biz.needs;

import javax.servlet.http.HttpServletRequest;

import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class NeedRiskAssessmentMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	
	public NeedRiskAssessmentMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
}
