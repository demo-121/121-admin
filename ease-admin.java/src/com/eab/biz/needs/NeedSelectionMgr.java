package com.eab.biz.needs;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.needs.Needs;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class NeedSelectionMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	
	 
	
	public NeedSelectionMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public void addValues(JsonObject contentValues, String needCode, int version, String channelCode) throws Exception {		
		  
		Needs needsDAO = new Needs();  
		
		String sectionId=Constants.NeedPage.NEEDS_SELECTION;
		
		//get selections 
			 
		JsonObject valuesSet = null;	
		JsonArray selections = null;
		JsonObject header = null;
		String selsKey="selections";
		Map<String, JSONObject> _sections = needsDAO.getNeedsSections(needCode, principal, version, Constants.NeedPage.NEEDS_SELECTION, channelCode);		
		if(_sections !=null && _sections.containsKey(sectionId)){ 	
			valuesSet=JsonConverter.convertObj(_sections.get(sectionId)).getAsJsonObject();		
			selections=valuesSet.getAsJsonArray(selsKey);
			header=valuesSet.getAsJsonObject("header");
			JsonArray pickerArray= addRightPanelPickerArray( needCode,  principal,   version,  channelCode , "en");
			if(selections!=null && selections.size()>0 && selections.get(0)instanceof JsonObject){ 
				JsonObject selection=selections.get(0).getAsJsonObject();				
				if(selection.has("pickerArray") &&selection.get("pickerArray") instanceof JsonArray){
					selection.remove("pickerArray");
					selection.add("pickerArray", pickerArray);
				}
			}
		} 
		if((selections==null||(selections!=null&&selections.size()<1))||(header==null))
			valuesSet=new JsonObject();
		//init selections if no existing record		
		boolean isInit=false;
		if(selections==null||(selections!=null&&selections.size()<1)){ 
			JsonArray JSA=new JsonArray();
			int i=0;			
			JsonArray pickerArray=addRightPanelPickerArray( needCode,  principal,   version,  channelCode , "en");
			JSA.add(addSelection(Integer.toString(i),"NeedsSelection", Integer.toString(i) , i, pickerArray));
			if(selections!=null)
				valuesSet.remove(selsKey);
			valuesSet.add(selsKey, JSA); 
			isInit=true;
		} 
		//init header if no existing record				
		if(header==null){ 
			header=new JsonObject();
			header.add("id",gson.toJsonTree(""));
			JsonObject title=new JsonObject();
			title.add("en", gson.toJsonTree("Headline"));
			title.add("zh-Hant", gson.toJsonTree(""));
			title.add("zh-Hans", gson.toJsonTree(""));
			header.add("title",gson.toJsonTree(title));
			valuesSet.add("header", header);
			isInit=true;
		} 
		if(isInit==true)
			needsDAO.saveNeedSection(needCode, version, channelCode, Constants.NeedPage.NEEDS_SELECTION, new  JSONObject (valuesSet.toString()), principal);
		contentValues.add(sectionId, valuesSet); 
		
		//get attach
		Map<String, Object> _attachs = needsDAO.getNeedsAttachContent(needCode, principal, version, channelCode, sectionId);		 
		JsonObject attachsSet = new JsonObject();
		if(_attachs !=null  ){
			for (Entry<String, Object> entry : _attachs.entrySet()) {
			    String key = entry.getKey();			
			    
			    Blob blob = (Blob)_attachs.get(key);
				byte[] bdata = blob.getBytes(1, (int) blob.length());
				String b64String = new String(bdata);
				attachsSet.remove(key);
			    attachsSet.add(key, gson.toJsonTree(b64String));
			}	
		}
		contentValues.add(Constants.NeedPage.ATTACHMENTS, attachsSet);

		 
	} 
	
	private JsonObject addSelection(String id, String inputType, String title , int seq, JsonArray pickerArray){
		JsonObject values=new JsonObject();  
		values.add("options",new JsonArray());
		values.add("pickerArray",pickerArray);
		values.add("type",gson.toJsonTree("iconSelection")); 
		values.add("id", gson.toJsonTree(id)); 
		values.add("title", addLeftPanelTextField(title)); 
		values.add("seq", gson.toJsonTree((seq+1)*100)); 
		
		return  values;
	}
	
	private JsonObject addLeftPanelTextField(String title){		 
		JsonObject values=new JsonObject();
		JsonObject langJSO=new JsonObject();
		langJSO.add("en", gson.toJsonTree(title));
		langJSO.add("zh-Hans", gson.toJsonTree(title));
		langJSO.add("zh-Hant", gson.toJsonTree(title));
		values.add("title", langJSO);
		 
		
		return  values;
	}
	           

	private JsonArray addRightPanelPickerArray(String needCode, UserPrincipalBean principal,  int version, String channelCode, String langCode ) throws SQLException{	
		
		JsonArray jsa=new JsonArray();		 
		Needs needsDAO = new Needs();  
		
		String sectionId=Constants.NeedPage.NEEDS_CONCEPT_SELLING;
		Map<String, JSONObject> _sections = needsDAO.getNeedsSections("N0001", principal, version,Constants.NeedPage.NEEDS_CONCEPT_SELLING, channelCode);		
		if(_sections !=null && _sections.containsKey(sectionId)){ 	   
			JSONObject ncs=_sections.get(sectionId);
			JSONArray items = ncs.getJSONArray("pages");;
			for(int i=0; i<items.length();i++){		
				JSONObject item = items.getJSONObject(i); 
				String id=item.getString("id");
				String title=item.getJSONObject("title").getString(langCode);
				JsonObject values=new JsonObject();
				values.add("title",  gson.toJsonTree(title));
				values.add("value",  gson.toJsonTree(id)); 
				jsa.add(values);
			}
		}
		return  jsa;              
	}
	 

}
