 
package com.eab.biz.needs;

import java.sql.Blob;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.dao.needs.Needs;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class NeedsConceptSellingMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	
	 
	
	public NeedsConceptSellingMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public void addValues(JsonObject contentValues, String needCode, int version, String channelCode) throws Exception {		
		  
		Needs needsDAO = new Needs();   
		
		//get selections 
			 
		JsonObject valuesSet = null;			 
		Map<String, JSONObject> _sections = needsDAO.getNeedsSections(needCode, principal, version, Constants.NeedPage.NEEDS_CONCEPT_SELLING, channelCode);		
		if(_sections !=null && _sections.containsKey(Constants.NeedPage.NEEDS_CONCEPT_SELLING) ){ 				
			valuesSet=new JsonObject();
			valuesSet=JsonConverter.convertObj(_sections.get(Constants.NeedPage.NEEDS_CONCEPT_SELLING)).getAsJsonObject();
		} 
				 
		contentValues.add( Constants.NeedPage.NEEDS_CONCEPT_SELLING, valuesSet); 
	
		//get attach
		Map<String, Object> _attachs = needsDAO.getNeedsAttachContent(needCode, principal, version, channelCode, Constants.NeedPage.NEEDS_CONCEPT_SELLING);		 
		JsonObject attachsSet = new JsonObject();
		if(_attachs !=null  ){
			for (Entry<String, Object> entry : _attachs.entrySet()) {
			    String key = entry.getKey();			
			    
			    Blob blob = (Blob)_attachs.get(key);
				byte[] bdata = blob.getBytes(1, (int) blob.length());
				String b64String = new String(bdata);
				attachsSet.remove(key);
			    attachsSet.add(key, gson.toJsonTree(b64String));
			}	
		}
		contentValues.add(Constants.NeedPage.ATTACHMENTS, attachsSet);
		
	} 
	
 
	           

 
}
	 