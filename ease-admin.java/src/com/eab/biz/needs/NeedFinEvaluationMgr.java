package com.eab.biz.needs;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.dao.needs.Needs;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class NeedFinEvaluationMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	
	public NeedFinEvaluationMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public void addValues(JsonObject contentValues, String needCode, int version, String channelCode) throws Exception {		
		 
		Needs needsDAO = new Needs();
		String sectionCode=Constants.NeedPage.NEEDS_FIN_EVAL;
		String dataSectionId= Constants.NeedPage.NEEDS_SELECTION;
		
		Map<String, JSONObject> _sections = needsDAO.getNeedsSections(needCode, principal, version,sectionCode, channelCode);
		Map<String, JSONObject> _dataSections = needsDAO.getNeedsSections(needCode, principal, version, dataSectionId, channelCode);
		
		//get image id from section
		List<String> imgIds = new ArrayList<String>();
		
		Map<String, Object> _attachs = needsDAO.getNeedsAttachContent(needCode, principal, version, channelCode, sectionCode);
		JsonObject valuesSet = null; 
		JsonArray dataSet = null;
		JsonObject attachsSet= new JsonObject();
		//load image from db
		if(_attachs !=null  ){
			for (Entry<String, Object> entry : _attachs.entrySet()) {
			    String key = entry.getKey();			
			    
			    Blob blob = (Blob)_attachs.get(key);
				byte[] bdata = blob.getBytes(1, (int) blob.length());
				String b64String = new String(bdata);
				attachsSet.remove(key);
			    attachsSet.add(key, gson.toJsonTree(b64String));
			}	
		}
		
	     
		
		if(_sections !=null && _sections.containsKey(sectionCode)){
			valuesSet = JsonConverter.convertObj(_sections.get(sectionCode)).getAsJsonObject(); 		
		} 
		
		if(_dataSections !=null && _dataSections.containsKey(dataSectionId)){
			JsonObject _data = JsonConverter.convertObj(_dataSections.get(dataSectionId)).getAsJsonObject();
			JsonArray _selection = _data.getAsJsonArray("selections");
			if(_selection.size()>0){
				JsonArray _selectionOpts = _selection.get(0).getAsJsonObject().getAsJsonArray("options");
				dataSet = new JsonArray();
				for(int j=0; j<_selectionOpts.size(); j++){
					JsonObject _opt = _selectionOpts.get(j).getAsJsonObject();
					JsonObject option = new JsonObject();
					option.add("value", _opt.get("id"));
					option.add("title", _opt.getAsJsonObject("title").get(principal.getLangCode()));
					dataSet.add(option);
				}
				
			}
		} 
		
		contentValues.add(sectionCode, valuesSet); 
		contentValues.add("needsAlyOpts", dataSet);
		contentValues.add(Constants.NeedPage.ATTACHMENTS, attachsSet);
	} 
	private JsonObject genItem(String id, String inputType, String title, String label, boolean isMadatory, int seq){
		JsonObject values=new JsonObject(); 
		values.add("id", gson.toJsonTree(id));
		values.add("inputType", gson.toJsonTree(inputType));
		values.add("title", genTitle(title));
		values.add("label", gson.toJsonTree(label));//"Question"			
		values.add("isMadatory", gson.toJsonTree(isMadatory));
		values.add("seq", gson.toJsonTree(seq));
		
		return  values;
	}
	
	private JsonObject genTitle(String title){		 
		JsonObject values=new JsonObject();
		JsonObject langJSO=new JsonObject();
		langJSO.add("en", gson.toJsonTree(title));
		langJSO.add("zh-Hans", gson.toJsonTree(title));
		langJSO.add("zh-Hant", gson.toJsonTree(title));
		values.add("title", langJSO);
		 
		
		return  values;
	}
	
	


}

