package com.eab.biz.needs;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.StopWatch.TaskInfo;

import com.eab.common.Constants;
import com.eab.dao.needs.Needs;
import com.eab.dao.products.Products;
import com.eab.dao.tasks.Tasks;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.tasks.TaskBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class NeedBuilderMgr {
	
	HttpServletRequest request;
	Gson gson;

	public NeedBuilderMgr(HttpServletRequest request) {
		super();
		this.request = request;
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public String saveNeed(String needCode, int version, String channelCode, String sectionCode, TaskBean taskInfo, JSONObject mNeedDetail, Map<String, JSONObject> sections, int taskId) {

		HttpSession session = request.getSession();
		UserPrincipalBean principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		Needs needsDAO = new Needs();
		String errMsg = null;
		
		 
		try {
			
			 //TODO:save taskinfo
			Tasks taskMgr = new Tasks();
			if (taskInfo != null) {
				if(taskInfo.getTaskCode() == null || taskInfo.getTaskCode().equals("")){
					taskMgr.addTask(principal, taskInfo);
				}else{
					Tasks taskDAO = new Tasks();
					taskDAO.updateTask(principal, taskInfo);
				}
			}
			
			 //TODO:save needDetail
			if (mNeedDetail != null) {
				needsDAO.saveNeedDetail(needCode, version, channelCode,  mNeedDetail, principal);
			}
			
			
			if (sections != null) {
				for (String key : sections.keySet()) {
					JSONObject section = sections.get(key);
					needsDAO.saveNeedSection(needCode, version, channelCode, sectionCode, section, principal);
					
				}
			}
			
			return errMsg;
		} catch (Exception e) {
			e.printStackTrace();
			
			return "ERROR IN SAVING NEEDS";
		}

	}
	public String saveMenuName( String needCode,String  nameCode, String langCode, String menuName, String status,String channelCode,int version){
		HttpSession session = request.getSession();
		UserPrincipalBean principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		Needs needsDAO = new Needs();
		String errMsg = null;		
		 
		try {	
		    needsDAO.saveDynMenuName( needCode,  nameCode, langCode,  menuName, status,  principal, channelCode, version);				
			return errMsg;
		} catch (Exception e) {
			e.printStackTrace();
			
			return "ERROR IN saveMenuName";
		}
 
	}
	public String saveMenuList(String needCode, String menuId,   String sectionId, String uplineMenuId, String nameCode, String sectionType, int disSeq, String status ,String channelCode,int version) {

		HttpSession session = request.getSession();
		UserPrincipalBean principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		Needs needsDAO = new Needs();
		String errMsg = null;
		
		 
		try {  
		    needsDAO.saveDynMenuList( needCode,  menuId, sectionId,  uplineMenuId,  nameCode,  sectionType,  disSeq,  status,  principal, channelCode, version ); 
			return errMsg;
		} catch (Exception e) {
			e.printStackTrace();
			
			return "ERROR IN saveMenuList";
		}

	}
	 
      
      public String deleteNeedsAnaylsis(String needCode  ,String channelCode, int version, String langCode, String sectionCode, List<String> idSet, String restrictPrefix )  {
          HttpSession session = request.getSession();
          UserPrincipalBean principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
          Needs needsDAO = new Needs();
          String errMsg = null;
          try {             
              needsDAO.deleteNeedsAnaylsis( principal.getCompCode(), needCode,  channelCode, version, langCode,    sectionCode,  idSet, restrictPrefix ) ;         
              return errMsg;
          } catch (Exception e) {
              e.printStackTrace();            
              return "ERROR IN deleteNeedsAnaylsis";
          }
  
      } 
	
	public String deleteAttachment(String needCode,String  channelCode, String   sectionCode,int  version, List<String> idSet ) {

		HttpSession session = request.getSession();
		UserPrincipalBean principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		Needs needsDAO = new Needs();
		String errMsg = null;		
		 
		try {  
		    needsDAO.deleteAttachment( principal.getCompCode(), needCode,  channelCode,   sectionCode,  version, idSet ) ; 
			return errMsg;
		} catch (Exception e) {
			e.printStackTrace();
			
			return "ERROR IN deleteMenuName";
		}

	}
	
	
	public String removeUnusedAttachment(String needCode  ,String channelCode, int version, String langCode, String sectionCode, List<String> codeArray )  {

		HttpSession session = request.getSession();
		UserPrincipalBean principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		Needs needsDAO = new Needs();
		String errMsg = null;		
		 
		try {  
		    needsDAO.removeUnusedAttachment( principal.getCompCode(), needCode,  channelCode, version, langCode,    sectionCode,  codeArray ) ; 
			return errMsg;
		} catch (Exception e) {
			e.printStackTrace();
			
			return "ERROR IN removeUnusedAttachment";
		}

	}
	
	
}
