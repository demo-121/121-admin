package com.eab.biz.needs;

import java.sql.Blob;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.dao.needs.Needs;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class NeedPrioritizationMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	
	public NeedPrioritizationMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public void addValues(JsonObject contentValues, String needCode, int version, String channelCode) throws Exception {		
		  
		Needs needsDAO = new Needs();  
		
		
		String selectionId= Constants.NeedPage.NEEDS_SELECTION;			
		JsonObject valuesSet = null;
		Map<String, JSONObject> _dataSections = needsDAO.getNeedsSections(needCode, principal, version, selectionId, channelCode);
		if(_dataSections !=null && _dataSections.containsKey(selectionId))
			valuesSet = JsonConverter.convertObj(_dataSections.get(selectionId)).getAsJsonObject();
		contentValues.add(selectionId, valuesSet);  
		
		String priorId=Constants.NeedPage.NEEDS_PRIORITIZATION;
		JsonObject priorVal = null;
		Map<String, JSONObject> _sections = needsDAO.getNeedsSections(needCode, principal, version, priorId, channelCode);
		if(_sections !=null  && _sections.containsKey(priorId)){ 
			priorVal = JsonConverter.convertObj(_sections.get(priorId)).getAsJsonObject(); 
			
		}else{
			priorVal =new JsonObject(); 		 
		}		
		contentValues.add(priorId, priorVal); 
		 
		
		JsonObject attachsSet = new JsonObject();
		Map<String, Object> _attachs = needsDAO.getNeedsAttachContent(needCode, principal, version, channelCode, selectionId);
		if(_attachs !=null  ){
			for (Entry<String, Object> entry : _attachs.entrySet()) {
			    String key = entry.getKey();			
			    
			    Blob blob = (Blob)_attachs.get(key);
				byte[] bdata = blob.getBytes(1, (int) blob.length());
				String b64String = new String(bdata);
				attachsSet.remove(key);
			    attachsSet.add(key, gson.toJsonTree(b64String));
			}	
		}
		contentValues.add(Constants.NeedPage.ATTACHMENTS, attachsSet);

		 
	} 
	
	 
	
	private JsonObject genTitle(String title){		 
		JsonObject values=new JsonObject();
		JsonObject langJSO=new JsonObject();
		langJSO.add("en", gson.toJsonTree(title));
		langJSO.add("zh-Hans", gson.toJsonTree(title));
		langJSO.add("zh-Hant", gson.toJsonTree(title));
		values.add("title", langJSO);
		 
		
		return  values;
	}
	String[] pickerTitle=new String[] {"Please select",
	                    "Education",
	                    "Retirement",
	                    "Critical Illness",
	                    "Saving",
	                    "Protection",
	};
	String[] pickerValue=new String[] {"pleaseSelect",
            "childEducate",
            "retirement",
            "critIllness",
            "saving",
            "protect",
	};
                    
 

}
