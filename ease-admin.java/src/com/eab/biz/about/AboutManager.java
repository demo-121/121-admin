package com.eab.biz.about;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.common.Constants;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.about.About;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Field;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.model.about.AboutBean;
import com.google.gson.JsonObject;

public class AboutManager {
	HttpServletRequest request = null;

	public AboutManager(HttpServletRequest request) {
		super();
		this.request = request;
	}

	public Response genAbout() {
		HttpSession session = request.getSession();
		Response resp = null;
		String error = null;

		try {
			// bar
			AppBarTitle appBarTitle = new AppBarTitle(null, "label", "", "About", null, null);
			AppBar appBar = new AppBar(appBarTitle, null);

			// create template
			List<Field> fields = new ArrayList<Field>();
			About about = new About();
			AboutBean ab = about.getAbout(request);

			fields.add(new Field("logo", "img", ab.getLogo()));
			fields.add(new Field("title", "p", ab.getTitle()));
			fields.add(new Field("desc", "p", ab.getDesc()));

			fields.add(new Field("address", "viewOnly", "Address"));
			fields.add(new Field("tel", "viewOnly", "Telephone"));
			fields.add(new Field("fax", "viewOnly", "Fax"));
			fields.add(new Field("website", "viewOnly", "Website"));
			fields.add(new Field("mail", "viewOnly", "Email"));

			fields.add(new Field("rightDesc", "text", ab.getRightDesc()));
			fields.add(new Field("sysVer", "text", ab.getSysVer()));
			fields.add(new Field("copyright", "text", ab.getCopyRight()));

			Template template = new Template("about", fields, "", "");
			Function2.translateField(request, template);

			JsonObject values = new JsonObject();

			values.addProperty("address", ab.getAddress());
			values.addProperty("tel", ab.getTel());
			values.addProperty("fax", ab.getFax());
			values.addProperty("website", ab.getWebsite());
			values.addProperty("mail", ab.getMail());

			Page page = new Page("/About", "About");
			// Token
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());
			Content content = new Content(template, null);
			content.setValues(values);
			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, appBar, content);

		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}
		return resp;
	}

}
