package com.eab.biz.environment;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.common.Constants;
import com.eab.common.CryptoUtil;
import com.eab.common.Function;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.couchbase.CBServer;
import com.eab.dao.environment.Environment;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Response;
import com.eab.model.environment.EnvBean;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class EnvironmentMgr {

	HttpServletRequest request;
	HttpSession session;
	UserPrincipalBean principal;
	Language langObj;

	public EnvironmentMgr(HttpServletRequest request) {
		super();
		this.request = request;
		this.session = request.getSession();
		this.principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		this.langObj = new Language(principal.getUITranslation());
	}
	
	public String getJson(EnvBean envBean) throws Exception {
		//Action 
		String action = Constants.ActionTypes.SHOW_DIALOG;
		
		//Token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
				
		String message = "";
		
		try {
			String compCode = principal.getCompCode();
			
			Environment env = new Environment();
			String password = env.getDBPassword(compCode, envBean.getEnvId());
			
			CryptoUtil crypto = new CryptoUtil();
			envBean.setDBPassword(crypto.decryptUserData(compCode, password));
					
			CBServer cbServer = new CBServer(envBean.getHost(), envBean.getPort(), envBean.getDBName(), envBean.getDBLogin(), envBean.getDBPassword());
			
			if (cbServer.checkDBConnection())
				message = langObj.getNameDesc("CONN_SUCCESS");
			else
				message = langObj.getNameDesc("CONN_FAIL");
		} catch (Exception e) {
			Log.error("{EnvironmentMgr getJson} ----------->" + e);
		}
		
		//Dialog
		Field btnOk = new Field("ok", "button", langObj.getNameDesc("BUTTON.OK"));	
		Dialog dialog = new Dialog("new_completed", langObj.getNameDesc("TEST_RESULT"), message, null, btnOk, 30);
		
		Gson gson = new GsonBuilder().create();
		return gson.toJson(new Response(action, tokenID, tokenKey, dialog));
	}
	
	public void envDataInitial(String compCode) throws Exception {
		Environment env = new Environment();
		env.envDataInitial(compCode);
	}

}
