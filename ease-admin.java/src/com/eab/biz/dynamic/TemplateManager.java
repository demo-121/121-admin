package com.eab.biz.dynamic;

import java.util.List;

import com.eab.json.model.Field;
import com.eab.json.model.Template;

public class TemplateManager {
	public void disableFieldBySeq(Template template, String seq){
		List<Field> fields = template.getItems();		
		for(Field field : fields){
			if(seq.equals("detailSeq")){
				if(field.getDetailSeq() == null || field.getDetailSeq() == 0){
					fields.remove(field);
				}
			}else{
				if(field.getListSeq() == null || field.getListSeq() == 0){
					fields.remove(field);
				}
			}
		}
		template.setItems(fields);
	}
}
