package com.eab.biz.dynamic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.eab.dao.dynamic.Companies;
import com.eab.dao.system.Name;
import com.eab.model.dynamic.CompanyBean;

public class CompanyManager {

	HttpServletRequest request;

	public CompanyManager(HttpServletRequest request) {
		super();
		this.request = request;
	}
	public CompanyBean getCompany(String compCode){
		Companies cm = new Companies(); 
		return cm.getCompany(compCode);
	}
	
	public List<String> getCompanyLangList(String compCode){
		Companies cm = new Companies(); 
		CompanyBean cmb=  cm.getCompany(compCode);
		return getCompany(compCode).getLangList();
	}
	
	public HashMap<String, String> getCompanyLangMap(String compCode){
		Companies cm = new Companies(); 
		return cm.getCompanyLangNameMap(getCompany(compCode),  request);
	}
	
}
