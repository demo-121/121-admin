package com.eab.biz.dynamic;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.StringUtils;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.dynamic.DynamicDAO;
import com.eab.dao.dynamic.TemplateDAO;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Response;
import com.eab.json.model.Field.Types;
import com.eab.model.dynamic.DynMultiLangField;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.webservice.dynamic.DynamicFunction;

public class SaveAddManager {
	
	HttpServletRequest request;
	public SaveAddManager(HttpServletRequest request){
		this.request = request;
	}
	
	public Response saveAdd(){
		String module = request.getParameter("p1");
		String cols = null;
		String colValues = null;
		JSONObject paraJson = DynamicFunction.getParap0(request);
		JSONObject template = null;
		DBManager dbm = null;
		Connection conn = null;
		HttpSession session = request.getSession();
		Response resp = null;
		DynamicManager dm = new DynamicManager(request);
		DynamicDAO dynamic = new DynamicDAO();
		boolean hasError = false;
		
		//create dialog	
		String dialogId = "saveAddDialog";
		String dialogTitle = null;
		Field negative = null;
		String dialogMsg = null;
		UserPrincipalBean pricipal = Function.getPrincipal(request);
			try {
				dialogTitle = (module + ".NEW.TITLE").toUpperCase();
				negative = new Field("ok", "button", "BUTTON.OK");
				
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				DynamicUI ui  = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
				if (ui == null || !ui.getModule().equals(module)) {
					
					ui = dynamic.getDynamicUI(module, conn);
					session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
				}
						
				// get dynamic template
				template = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) != null ? (JSONObject) session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) : null;
				if (template == null || !module.equals(template.getString("id"))) {
					TemplateDAO tempalteDao =  new TemplateDAO();
					template = tempalteDao.getTemplate(module, ui, conn);
					session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, template);
				}
				
				//get principal
				UserPrincipalBean principal = Function.getPrincipal(request); 
				
				//get current date
				//same approve date
				Calendar calendar = Calendar.getInstance();
				java.util.Date currentDate = calendar.getTime();
				Date curDate = new java.sql.Date(currentDate.getTime());
				
				//check if the record exist
				HashMap<String, String> mapFilter = getFilterKeys(pricipal.getCompCode(), paraJson, DynamicFunction.getTemplateReverseMap(template), ui);
				boolean hasRecordInMst = Function2.hasRecord(ui.getDataTable(), mapFilter, dbm, conn);
				boolean hasRecordInTmp = false;
				if (!StringUtils.isEmpty(ui.getTempTable())) {
					hasRecordInTmp = Function2.hasRecord(ui.getTempTable(), mapFilter, dbm, conn);
				}
				if(hasRecordInMst || hasRecordInTmp){
					//deny insert
					dialogMsg = ("NEW.FAIL.KEY").toUpperCase();
					return dm.returnDialog(dialogMsg, dialogId, dialogTitle);
				}								
							
				// gen table cols and values
				JSONArray items = template.getJSONArray("items");
				int length = items.length();
				for (int i = 0; i < length; i++) {
					JSONObject item = items.getJSONObject(i);
					
					if(item.get("key") != null && item.get("id") != null){
						String fieldKey = item.getString("key");
						String fieldValue = null;
						
						//insert multi-lang text field
						if(item.has("type") && item.getString("type").equals(Types.MULTTEXT)){
							DBManager dbm3 = null;
							try{
								addMultText(conn, module, ui, principal, template, item, paraJson, curDate);
								
							}catch(Exception e){
								Log.error(e);
							}
							
						}else{
							//get primary name
							//check duplicated name
							Log.debug("non mulTEXXX");
							if(ui.getPrimaryName() != null && fieldKey.equals(ui.getPrimaryName())){
								
								String nameValue = paraJson.getString(item.getString("id"));
								HashMap<String, String> nameFilter = new HashMap<String, String>();
								nameFilter.put(fieldKey, nameValue);
								if(ui.getCompanyFilter()){
									nameFilter.put(Constants.COL_COMP_CODE, principal.getCompCode());
								}
								
								boolean hasDuplicatedName = Function2.hasRecord(ui.getDataTable(), nameFilter, null, null);
								boolean hasDuplicatedNameImTmp = false;
								if (!StringUtils.isEmpty(ui.getTempTable())) {
									hasDuplicatedNameImTmp = Function2.hasRecord(ui.getTempTable(), nameFilter, null, null);
								}
								if(hasDuplicatedName || hasDuplicatedNameImTmp){
									dialogMsg = ("NEW.FAIL.NAME").toUpperCase();
									return dm.returnDialog(dialogMsg, dialogId, dialogTitle);
								}
							}
							
							//get password field
							if(item.has("subType") && item.get("subType") != null && "PASSWORD".equals(item.getString("subType").toUpperCase())){
								//ency password
								fieldValue = dm.encryptPassword(paraJson.getString(item.getString("id")));
							}else{								
								fieldValue = paraJson.get(item.getString("id")).toString();
							}
							
							try {
								Log.debug("fieldValue: " + fieldValue);
								cols = buildColStr(cols, fieldKey);
								colValues = buildColStr(colValues, "?");
								dbm.param(fieldValue, DataType.TEXT);
								
							} catch (Exception e) {
								Log.error(e);
							}
						}
					}
				}
			 	try {
					 	
					dbm.param(curDate, DataType.DATE);
					dbm.param(pricipal.getUserCode(), DataType.TEXT);
					dbm.param(curDate, DataType.DATE);
					dbm.param(pricipal.getUserCode(), DataType.TEXT);
					
					if (!StringUtils.isEmpty(ui.getTempTable())) {
						cols = cols + ", CREATE_DATE, CREATE_BY, MODIFY_DATE, MODIFY_BY)";
						
					 	colValues = colValues + ", ?, ?, ?, ?)";				 	
					 	
						String sql = "insert into " + ui.getTempTable() + " " + cols + " values " + colValues;					
						boolean insert = dbm.insert(sql, conn);												
						if(insert){
							//inserted
							String sqlAud = "insert into " + ui.getAudTable() + " " + cols + " values " + colValues;
							boolean insertAud = dbm.insert(sqlAud);
							//return to Dynamic 
							resp = dm.getDynTable();
							//add success dialog
								dialogMsg = (module+".NEW.SUCCESS").toUpperCase();
						}else{
							//not able to insert
							//add fail dialog
							hasError = true;
							dialogMsg = (module+".NEW.FAIL.INPUT").toUpperCase();														
						}	
						

						//copy mul fields to aud
						DynMultiLangField dynMultiLangField = dynamic.getDynMultiLangField(ui.getModule());
						if(dynMultiLangField != null && dynMultiLangField.getTmpTable() != null){
							//get recordId from template
							HashMap<String, String> templateMap = DynamicFunction.getTemplateReverseMap(template);
							String recordId = paraJson.getString(templateMap.get(ui.getPrimaryKey()));
							dynamic.copyMULfromTmpToAud(dbm, dynMultiLangField,  ui, recordId, principal.getCompCode());
						}
						
					} else {
						cols = cols + ", STATUS, CREATE_DATE, CREATE_BY, MODIFY_DATE, MODIFY_BY)";
						
					 	colValues = colValues + ", 'A', ?, ?, ?, ?)";				 	

					 	String sql = "insert into " + ui.getDataTable() + " " + cols + " values " + colValues;					
						boolean insertSuccess = dbm.insert(sql, conn);
						if (insertSuccess) {
							resp = dm.getDynTable();
							dialogMsg = (module+".NEW.SUCCESS").toUpperCase();
						} else {
							hasError = true;
							dialogMsg = (module+".NEW.FAIL.INPUT").toUpperCase();								
						}
					}
				} catch (Exception e) {
					hasError = true;
					dialogMsg = (module+".NEW.FAIL.ERROR").toUpperCase();
				}		 	
			} catch (SQLException e) {
				hasError = true;
				dialogMsg = (module+".NEW.FAIL.ERROR").toUpperCase();
			}finally{
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					Log.error(e);
				}
				dbm = null;
				conn = null;
			}
			//add dialog
			Dialog dialog = new Dialog(dialogId, dialogTitle, dialogMsg, null, negative);	
			dialog.translate(request);
			if(!hasError){				
				resp.setDialog(dialog);	
			}else{
				String tokenID = Token.CsrfID(request.getSession());
				String tokenKey = Token.CsrfToken(request.getSession());
				resp = new Response(Constants.ActionTypes.SHOW_DIALOG, tokenID, tokenKey, dialog);	
			}
			return resp;
	}


	public void addMultText(Connection conn, String module, DynamicUI ui, UserPrincipalBean principal, JSONObject template, JSONObject item, JSONObject paraJson, Date curDate){
		addMultText(conn, module, ui, principal, template, item, paraJson, curDate, null);
	}
	public void addMultText(Connection conn, String module, DynamicUI ui, UserPrincipalBean principal, JSONObject template, JSONObject item, JSONObject paraJson, Date curDate, String action){
		//get values json
		
		JSONObject multiValue = paraJson.getJSONObject(item.getString("id"));
		
		//get recordId from template
		HashMap<String, String> templateMap = DynamicFunction.getTemplateReverseMap(template);
		String recordId = paraJson.getString(templateMap.get(ui.getPrimaryKey()));
		
		//insert every lang text
		Iterator<?> keys = multiValue.keys();
		while( keys.hasNext() ) {
		    String key = (String)keys.next();
		    String columnValues = null;
		    String columns = null;
		    DBManager dbm3 = new DBManager();
		    try{
		    	DynamicDAO dao = new DynamicDAO();
				DynMultiLangField dynMultiLangField = dao.getDynMultiLangField(ui.getModule());
				String table = dynMultiLangField.getTmpTable();
			    if(ui.getCompanyFilter() && !"company".equals(module)){
			    	columns = (columns == null)? "comp_code" : columns + ", comp_code";
			    	columnValues = (columnValues == null) ? "?" : columnValues + ", ?";
			    	dbm3.param(principal.getCompCode(), DataType.TEXT);
			    }
			    
			    //check name type
			    columns = (columns == null) ? "name_type" : columns + ", " + "name_type";
			    columnValues = (columnValues == null) ? "?" : columnValues + ", ?";
			    dbm3.param(item.getString("id"), DataType.TEXT);
			    	    
			    columns = (columns == null)? ui.getPrimaryKey() : columns + ", " + ui.getPrimaryKey();
			    columnValues = (columnValues == null) ? "?" : columnValues + ", ?";
			    dbm3.param(recordId, DataType.TEXT);
			    
			    columns = (columns == null)? "lang_code" : columns + ", lang_code";
		    	columnValues = (columnValues == null) ? "?" : columnValues + ", ?";
		    	dbm3.param(key, DataType.TEXT);
		    	
		    	columns = (columns == null)? "name_desc" : columns + ", name_desc";
		    	columnValues = (columnValues == null) ? "?" : columnValues + ", ?";
		    	dbm3.param(multiValue.get(key), DataType.TEXT);
		    	
		    	columns = (columns == null)? "action" : columns + ", action";
		    	columnValues = (columnValues == null) ? "?" : columnValues + ", ?";
		    	action = (action == null) ? "N" : action;
		    	dbm3.param(action, DataType.TEXT);
		    	
		    	columns = (columns == null)? "create_by" : columns + ", create_by";
		    	columnValues = (columnValues == null) ? "?" : columnValues + ", ?";
		    	dbm3.param(principal.getUserCode(), DataType.TEXT);
		    	
		    	columns = (columns == null)? "modify_by" : columns + ", modify_by";
		    	columnValues = (columnValues == null) ? "?" : columnValues + ", ?";
		    	dbm3.param(principal.getUserCode(), DataType.TEXT);
		    	
		    	columns += ", create_date";
		    	columnValues += ", ?";
		    	dbm3.param(curDate, DataType.DATE);
		    	
		    	columns += ", modify_date";
		    	columnValues += ", ?";
		    	dbm3.param(curDate, DataType.DATE);
		    		    								    							    							    		
		    String sql = "insert into " + table + "(" + columns + ") values (" + columnValues + ")";
		    
		    dbm3.insert(sql, conn);
		    
		    }catch(Exception e){
		    	Log.error(e);
		    }finally{
		    	dbm3.clear();
		    }
		}
	}

	public HashMap<String, String> getFilterKeys(String compCode, JSONObject paraJson, HashMap<String, String> templateMap, DynamicUI ui){
		HashMap<String, String> map = new HashMap<String, String>();
		if(ui.getCompanyFilter()){
			map.put("comp_code", compCode);
		}
		map.put(ui.getPrimaryKey(), paraJson.getString(templateMap.get(ui.getPrimaryKey())));
		return map;
	}
	public String buildColStr(String col, String newCol){
		if(col == null){
			col = "(" + newCol;
		}else{
			col += ", " + newCol;
		}
		return col;
	}
}
