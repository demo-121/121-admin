package com.eab.biz.dynamic;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.StringUtils;

import com.couchbase.client.deps.com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.dao.dynamic.DynamicDAO;
import com.eab.dao.dynamic.TemplateDAO;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Response;
import com.eab.json.model.Field.Types;
import com.eab.model.dynamic.DynMultiLangField;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.webservice.dynamic.DynamicFunction;

public class SaveEditManager {
	HttpServletRequest request;
	public SaveEditManager(HttpServletRequest request){
		this.request = request;
	}
	
	public Response saveEdit() {
		Response resp = null;
		JSONObject paraJson = DynamicFunction.getParap0(request);
		String module = request.getParameter("p1");
		String id = paraJson.getString("id");

		DBManager dbm = null;
		Connection conn = null;
		HttpSession session = request.getSession();
		DynamicUI ui = null;
		DynamicDAO dynamic = new DynamicDAO();
		Dialog dialog = null;
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			// get dynamic ui from session
			ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !ui.getModule().equals(module)) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}

			// get dynamic template
			JSONObject template = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) != null ? (JSONObject) session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) : null;
			if (template == null || !module.equals(template.getString("id"))) {
				TemplateDAO tempalteDao = new TemplateDAO();
				template = tempalteDao.getTemplate(module, ui, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, template);
			}
			JSONObject detailTemplate = DynamicFunction.getDetailTemplate(template);
			// get dynamic template id and table column name map

			UserPrincipalBean principal = Function.getPrincipal(request); 
			
			//get current date
			//same approve date
			Calendar calendar = Calendar.getInstance();
			java.util.Date currentDate = calendar.getTime();
			Date curDate = new java.sql.Date(currentDate.getTime());
			
			//check duplicated name
			dbm.clear();
			String nameKey = null;
			String nameValue = null;
			JSONArray items = template.getJSONArray("items");
			int length = items.length();
			for (int i = 0; i < length; i++) {
				JSONObject item = items.getJSONObject(i);
				if(item.get("key") != null && item.get("id") != null){
					nameKey = item.getString("key");				
					if(ui.getPrimaryName() != null && nameKey.equals(ui.getPrimaryName())){						
						nameValue = paraJson.getString(item.getString("id"));
						HashMap<String, String> equalFilter = new HashMap<String, String>();
						equalFilter.put(nameKey, nameValue);
						if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
							equalFilter.put(Constants.COL_COMP_CODE, principal.getCompCode());
						}
						
						HashMap<String, String> notEqualFilter = new HashMap<String, String>();
						notEqualFilter.put(ui.getPrimaryKey(), id);					
						
						boolean hasDuplicatedName = Function2.hasRecord(ui.getDataTable(), equalFilter, notEqualFilter, null, null);
						boolean hasDuplicatedNameInTmp = Function2.hasRecord(ui.getTempTable(), equalFilter,notEqualFilter, null, null);
						if(hasDuplicatedName || hasDuplicatedNameInTmp){
							String dialogId = "saveEditDialog";
							String dialogTitle = (module + ".NEW.TITLE").toUpperCase();
							String dialogMsg = ("EDIT.FAIL.NAME").toUpperCase();
							DynamicManager dm = new DynamicManager(request);
							return dm.returnDialog(dialogMsg, dialogId, dialogTitle);
						}
							
					}
					
				}
			}
			
			
			//handle multi lang text field
			//1)delete all multi lang of this record
			try{
				DynamicDAO dao = new DynamicDAO();
				DynMultiLangField dynMultiLangField = dao.getDynMultiLangField(ui.getModule());
				if (dynMultiLangField.getTmpTable() != null) {
					String sql = "delete from " + dynMultiLangField.getTmpTable() + " where ";
					String conds = null;
					if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
						conds = "comp_code = ?";
						dbm.param(principal.getCompCode(), DataType.TEXT);
					}
					
					conds = (conds == null) ? ui.getPrimaryKey() + " = ?" : conds + " and " + ui.getPrimaryKey() + " = ?";
					dbm.param(id, DataType.TEXT);
					
					sql += conds;
					
					dbm.delete(sql);		

					//reload the json, prevent delete query
					for(int j = 0; j<length; j++){
						JSONObject item = items.getJSONObject(j);
						if(item.has("type") && item.getString("type").equals(Types.MULTTEXT)){
							//1. delete from tmp
							//2. insert into tmp
							//3. insert to aud
							//2)
							SaveAddManager sam = new SaveAddManager(request);	
							
							sam.addMultText(conn, module, ui, principal, template, item, paraJson, curDate, "E");
							
							//3)
							dynamic.copyMULfromTmpToAud(dbm, dynMultiLangField,  ui, id, principal.getCompCode());
						}
					}
				}
				
			}catch(Exception e){
				Log.error(e);
			}
			
			
			// save record to tmp table
			if (!StringUtils.isEmpty(ui.getTempTable())) {
			
				String sql = null;
	
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(ui.getPrimaryKey(), id);
				if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
					map.put(Constants.COL_COMP_CODE, principal.getCompCode());
				}
				if (Function2.hasRecord(ui.getTempTable(), map, dbm, conn)) {
					// update record in tmp
					sql = getUpdateSqL(request, dbm, ui, id, paraJson, detailTemplate, "update", principal.getCompCode());
					dbm.update(sql);
				} else {
					//insert record into tmp
					sql = getSqlStrbyType(request, dbm, ui, id, paraJson, detailTemplate, "insert");
					boolean insert = dbm.insert(sql);
					
					
					//update approve and create
					try{
					dbm.clear();
					boolean comFil = ui.getCompanyFilter() && !"company".equals(ui.getModule());
					String wheres = comFil ? "comp_code = ?" : null;
					wheres = (wheres == null) ? ui.getPrimaryKey() + "=?" : wheres + " and " + ui.getPrimaryKey() + "=?";
					String sqlApprDate = "select approv_date from " + ui.getDataTable() + " where " +  wheres;
					if(comFil)
						dbm.param(principal.getCompCode(), DataType.TEXT);
					
					dbm.param(id, DataType.TEXT);
					
					String sqlApprBy = "select approv_by from " + ui.getDataTable() + " where " +  wheres;
					if(comFil)
						dbm.param(principal.getCompCode(), DataType.TEXT);
					
					dbm.param(id, DataType.TEXT);
					
					String sqlCrtDate = "select create_date from " + ui.getDataTable() + " where " +  wheres;
					if(comFil)
						dbm.param(principal.getCompCode(), DataType.TEXT);
					
					dbm.param(id, DataType.TEXT);
					String sqlCrtBy = "select create_by from " + ui.getDataTable() + " where " +  wheres;
					if(comFil)
						dbm.param(principal.getCompCode(), DataType.TEXT);
					
					dbm.param(id, DataType.TEXT);
					
					sqlApprDate  = "(" + sqlApprDate + ")";
					sqlApprBy  = "(" + sqlApprBy + ")";
					sqlCrtDate  = "(" + sqlCrtDate + ")";
					sqlCrtBy  = "(" + sqlCrtBy + ")";				
					
					String updateAppNC = "update " + ui.getTempTable() + " set approv_date = " + sqlApprDate + "," + " approv_by = " + sqlApprBy +
							", " + "create_date = " + sqlCrtDate + ", " + "create_by = " + sqlCrtBy + " where " + wheres;
					if(comFil)
						dbm.param(principal.getCompCode(), DataType.TEXT);
					
					dbm.param(id, DataType.TEXT);
					
					dbm.update(updateAppNC);
					}catch(Exception e){
						Log.error(e);
					}
				}
				
				// make a copy to aud table
				dbm.clear();
				dynamic.copyFromTmpToAud(dbm, ui, id, principal);
				
			} else {
				dbm.clear();
				String sql = updateToMst(principal, dbm, ui, id, paraJson, detailTemplate);
				dbm.update(sql);

			}

		} catch (SQLException e) {
			Field negative = new Field("ok", "button", "BUTTON.OK");
			dialog = new Dialog("saveEditDialog", "RECORD.SAVE.FAIL", "RECORD.SAVE.FAIL.MSG.UNKNOWN", null, negative);			
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				Log.error(e);
			}
			dbm = null;
			conn = null;
			
		}
		// return to table page
		DynamicManager dm = new DynamicManager(request);
		resp = dm.getDynTable();
		if(dialog != null){
			dialog.translate(request);
			resp.setDialog(dialog);
		}
		return resp;
	}

	public String getSqlStrbyType(HttpServletRequest request, DBManager dbm, DynamicUI ui, String id, JSONObject paraJson, JSONObject template, String type) {
		dbm.clear();
		String cols = null;
		String colValues = null;

		JSONArray items = template.getJSONArray("items");
		int length = items.length();
		for (int i = 0; i < length; i++) {
			JSONObject item = items.getJSONObject(i);
			if(item.get("key") != null && item.get("id") != null && !item.getString("type").equals(Types.MULTTEXT)){
				String fieldKey = item.getString("key");
				String fieldValue = null;
				if(paraJson.has(item.getString("id"))){
					if(item.has("subType") && item.get("subType") != null && "PASSWORD".equals(item.getString("subType").toUpperCase()) && !paraJson.getString(item.getString("id")).equals(Constants.DUMMY_PASSWORD)){
						//ency password
						DynamicManager dm = new DynamicManager(request);
						fieldValue = dm.encryptPassword(paraJson.getString(item.getString("id")));
					}else{
						fieldValue = paraJson.get(item.getString("id")).toString();
					}
				}
				
				try {
					if (fieldKey != null && fieldValue != null) {
						cols = DynamicFunction.buildColStr(cols, fieldKey);
						if (fieldKey.equals("action")) {
							if (DynamicFunction.isNotEmptyOrNull(fieldValue))
								fieldValue = "E";
						}
						colValues = DynamicFunction.buildColStr(colValues, dbm.param(fieldValue, DataType.TEXT));
					}
					
				} catch (Exception e) {
					Log.error(e);
				}
			}
		}
		
		// make sure the col "action" shud be "E" - > "Edit"
		try {
			cols += ", modify_date, modify_by)";
			colValues += ", ?, ?)";
			UserPrincipalBean pricipal = Function.getPrincipal(request);
			dbm.param(new Date(Calendar.getInstance().getTimeInMillis()), DataType.DATE);
			dbm.param(pricipal.getUserCode(), DataType.TEXT);
		} catch (Exception e) {
			Log.error(e);
		}

		String sql = null;
		if (type.equals("update")) {
			try {
				sql = "update " + ui.getTempTable() + " " + cols + " values " + colValues + " where " + ui.getPrimaryKey() + " = " + dbm.param(id, DataType.TEXT);
			} catch (Exception e) {
				Log.error(e);
			}
		} else {
			sql = "insert into " + ui.getTempTable() + " " + cols + " values " + colValues;
		}
		return sql;

	}

	public String getUpdateSqL(HttpServletRequest request, DBManager dbm, DynamicUI ui, String id, JSONObject paraJson, JSONObject template, String type, String compCode) {
		dbm.clear();
		String setChangeCols = null;

		JSONArray items = template.getJSONArray("items");
		int length = items.length();
		for (int i = 0; i < length; i++) {
			JSONObject item = items.getJSONObject(i);
			if(item.get("key") != null && item.get("id") != null && !Types.MULTTEXT.equals(item.getString("type"))){
				String fieldID = item.getString("id");
				String fieldKey = item.getString("key");
				String fieldValue = null;
				if(paraJson.has(item.getString("id"))){
					if(item.has("subType") && item.get("subType") != null && "PASSWORD".equals(item.getString("subType").toUpperCase())&& !paraJson.getString(fieldID).equals(Constants.DUMMY_PASSWORD)){
						//ency password
						DynamicManager dm = new DynamicManager(request);
						fieldValue = dm.encryptPassword(paraJson.getString(fieldID));
					}else{
						fieldValue = paraJson.get(item.getString("id")).toString();
					}
				}
				
				try {
					if (fieldKey != null && fieldValue != null) {
						setChangeCols = (setChangeCols == null) ? fieldKey + "=?" : setChangeCols + "," + fieldKey + "=?";
						dbm.param(fieldValue, DataType.TEXT);
					}
					
				} catch (Exception e) {
					Log.error(e);
				}
			}
		}
		try {
			setChangeCols += ", modify_date=?, modify_by=?";
			UserPrincipalBean pricipal = Function.getPrincipal(request);
			dbm.param(new Date(Calendar.getInstance().getTimeInMillis()), DataType.DATE);
			dbm.param(pricipal.getUserCode(), DataType.TEXT);
			
			setChangeCols = "update " + ui.getTempTable() + " set " + setChangeCols + " where " + ui.getPrimaryKey() + "=?";
			dbm.param(id, DataType.TEXT);
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				setChangeCols += " and " + Constants.COL_COMP_CODE + "=?";
				dbm.param(compCode, DataType.TEXT);
			}
			
		} catch (Exception e) {
			Log.error(e);
		}

		return setChangeCols;

	}
	
	private String updateToMst(UserPrincipalBean pricipal, DBManager dbm, DynamicUI ui, String id, JSONObject paraJson, JSONObject template) {
		dbm.clear();
		String setChangeCols = null;

		JSONArray items = template.getJSONArray("items");
		int length = items.length();
		for (int i = 0; i < length; i++) {
			JSONObject item = items.getJSONObject(i);
			String fieldID = item.getString("id");
			String fieldKey = item.getString("key");
			String type = item.getString("type");
			if(fieldKey != null && fieldID != null){
				String fieldValue = paraJson.get(item.getString("id")).toString();
				try {
					setChangeCols = (setChangeCols == null) ? fieldKey + "=?" : setChangeCols + "," + fieldKey + "=?";
					if ("genericGrid".equals(type)) {
						dbm.param(fieldValue, DataType.CLOB);
					} else if ("datePicker".equals(type)) {
						dbm.param(fieldValue, DataType.INT);
					} else {
						dbm.param(fieldValue, DataType.TEXT);
					}
				} catch (Exception e) {
					Log.error(e);
				}
			}
		}
		try {
			String compCode = pricipal.getCompCode();
			setChangeCols += ", modify_date=?, modify_by=?";
			dbm.param(new Date(Calendar.getInstance().getTimeInMillis()), DataType.DATE);
			dbm.param(pricipal.getUserCode(), DataType.TEXT);
			
			setChangeCols = "update " + ui.getDataTable() + " set " + setChangeCols + " where " + ui.getPrimaryKey() + "=?";
			dbm.param(id, DataType.TEXT);
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				setChangeCols += " and " + Constants.COL_COMP_CODE + "=?";
				dbm.param(compCode, DataType.TEXT);
			}
			
		} catch (Exception e) {
			Log.error(e);
		}

		return setChangeCols;
		
	}

	public boolean checkRecordExist(String sql, DBManager dbm, Connection conn) {

		ResultSet rs = null;
		int rowCount = 0;
		try {
			rs = dbm.select(sql, conn);
			if (rs != null) {
				rowCount = rs.getRow();
			}
		} catch (SQLException e) {
			return false;
		} finally {
			dbm.clear();
		}
		return (rowCount > 0);
	}
}
