package com.eab.biz.dynamic;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.StringUtils;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.dao.dynamic.DynamicDAO;
import com.eab.dao.dynamic.RecordLock;
import com.eab.dao.dynamic.TemplateDAO;
import com.eab.dao.profile.User;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Response;
import com.eab.json.model.Field.Types;
import com.eab.model.dynamic.DynMultiLangField;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.dynamic.RecordLockBean;
import com.eab.model.profile.UserBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.webservice.dynamic.DynamicFunction;

public class DeleteManager {
	HttpServletRequest request;
	public DeleteManager(HttpServletRequest request){
		this.request = request;
	}
	public Response deleteRecord(){
		String module = request.getParameter("p1") ;
		String id = request.getParameter("p0") ;	
		Response resp = null;
		
		DynamicManager dm = new DynamicManager(request);
		
		RecordLock rlD = new RecordLock();
		RecordLockBean rlbD = rlD.newRecordLockBean(module, id);
		rlD.getRecordLock(request, rlbD);
		if(!rlbD.canAccess()){
			
			resp = dm.getDetails(request, "details");
			LockManager lm = new LockManager(request);
			resp.setDialog(lm.lockDialog(rlbD));
			return resp;
		}else{
			if(!rlbD.isCurrentUserAccess())
				rlD.addRecordLock(request, rlbD);
			
		}
		
		DynamicUI ui = null;
		HttpSession session = request.getSession();
		DBManager dbm = null;
		Connection conn = null;	
		DynamicDAO dynamic = new DynamicDAO();
		Dialog dialog = null;
		if (DynamicFunction.isNotEmptyOrNull(module) && DynamicFunction.isNotEmptyOrNull(id)) {
			try {
				conn = DBAccess.getConnection();
				dbm = new DBManager();
				ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
				if (ui == null || !ui.getModule().equals(module)) {
					ui = dynamic.getDynamicUI(module, conn);
					session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
				}
				// get dynamic template
				JSONObject template = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) != null ? (JSONObject) session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) : null;
				if (template == null || !module.equals(template.getString("id"))) {
					TemplateDAO tempalteDao =  new TemplateDAO();
					template = tempalteDao.getTemplate(module,ui,conn);
					session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, template);
				}
				
				UserPrincipalBean principal = Function.getPrincipal(request); 
				
				//check if record deleteAble
				DynamicDAO dd = new DynamicDAO();
				if(!dd.isDeleteAble(module, id, ui.getPrimaryKey(), ui.getCompanyFilter(), principal.getCompCode())){
					resp = dm.getDetails(request, "details");
					// set Dialog
					dialog = dependenceDenyDialog();
					dialog.translate(request);
					resp.setDialog(dialog);
					return resp;
				}
				
				User user = new User();
				UserBean ub = user.get(principal.getUserCode(), request);
				String compCode = ub.getCompCode();

				//check if the record deleteAble
				if("company".equals(module)){
					//get user default comp_code
					
					//compare user comp and this comp
					if(compCode != null && compCode.equals(id)){
						Field positive = new Field("ok", "button", "BUTTON.OK");
						dialog = new Dialog("deleteDialog", "Notice", "You cannot delete this company", null, positive);
						resp = dm.getDynTable();
						if(dialog != null){
							dialog.translate(request);
							resp.setDialog(dialog);
						}

						return resp;
					}
				}
						
				//same approve date
				Calendar calendar = Calendar.getInstance();
				java.util.Date currentDate = calendar.getTime();
				Date curDate = new java.sql.Date(currentDate.getTime());
				boolean comFil = ui.getCompanyFilter() && !ui.getModule().equals("company");

				if (!StringUtils.isEmpty(ui.getTempTable())) {				
					//1
					//insert a row from mst to tmp
					String selectColsOringal = DynamicFunction.getTableColByTemplate(template, null);
					selectColsOringal = DynamicFunction.dateNUserCol(selectColsOringal, null);
					String selectCols = "(" + selectColsOringal + ", action)";
					
					String insertValues = "select " + selectColsOringal + ", 'D' action from "+ui.getDataTable() + " where " + ui.getPrimaryKey() + "= ? ";	
					dbm.param(id, DataType.TEXT);
					if(comFil){
						insertValues += " and " + Constants.COL_COMP_CODE + "=?";
						dbm.param(principal.getCompCode(), DataType.TEXT);
					}
					
						
					String sqlTmp = "insert into " + ui.getTempTable()+ " " + selectCols + " "+ insertValues;			
					
					boolean insertToMst = dbm.insert(sqlTmp);
				
					//
					dbm.clear();
					dynamic.updateTmpTableModify(request, dbm, ui, "D", id);
					
					//2
					dbm.clear();
					dynamic.copyFromTmpToAud(dbm, ui, id, principal);				
				} else {
					
					// delete directly if there are not temp table
					String deleteMstSql = "delete from " + ui.getDataTable() + " where " + ui.getPrimaryKey() +"=?";
					dbm.param(id, DataType.TEXT);
					if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
						deleteMstSql += " and comp_code=?";
						dbm.param(compCode, DataType.TEXT);
					}
					int deleteFromMst = dbm.delete(deleteMstSql);
					if(deleteFromMst == -1){
						//String id, String title, String message, Field positive, Field negative
						Field negative = new Field("ok", "button", "BUTTON.OK");
						dialog = new Dialog("approveDialog", "BUTTON.APPROVE", "RECORD.DELETE.FAIL", null, negative, 30);
						dialog.translate(request);
						//resp.setDialog(dialog);
						
					}
					
				}
				
				//if the dyn item has multi lang fields				
				boolean hasMulField = false;
				JSONArray fields = template.getJSONArray("items");
				for(int i=0; i < fields.length(); i++){
					JSONObject field = fields.getJSONObject(i);
					if(field.has("type") && Types.MULTTEXT.equals(field.getString("type"))){
						hasMulField = true;
					}
				}
				
				if(hasMulField){
					//handle mult-lang fields
					DynMultiLangField dynMultiLangField = dynamic.getDynMultiLangField(ui.getModule());
						//1. update modify time
					
					HashMap<String, Object> condMap = new HashMap<String, Object>();
					condMap.put(ui.getPrimaryKey(), id);
					
					if(comFil)
						condMap.put("comp_code", principal.getCompCode());
					
					dynamic.updateTableModify(dbm, dynMultiLangField.getMstTable(), condMap, principal.getUserCode(), curDate);
						//2.insert into tmp table
					String columns = "comp_code" + ", " + ui.getPrimaryKey() + ", " + "name_type, name_desc, lang_code, status, create_date, create_by, modify_date, modify_by, approv_date, approv_by";
					String tmpCols = "(" + columns + ", action" + ")";
					String mstCols = columns + ", 'D' as ACTION";
							
					String sqlInsert = "insert into " + dynMultiLangField.getTmpTable() + " " + tmpCols + " select " + mstCols + " from "+ dynMultiLangField.getMstTable() + " where ";
					String wheres = null;
					if(comFil){
						wheres = "comp_code = ?";
						dbm.param(principal.getCompCode(), DataType.TEXT);
					}
					wheres = comFil ? wheres + " and " + ui.getPrimaryKey() + "=?" : " and " + ui.getPrimaryKey() + "=?";
					dbm.param(id, DataType.TEXT);
					sqlInsert += wheres;
					dbm.insert(sqlInsert);
					
					//4.copy to aud table
					dynamic.copyMULfromTmpToAud(dbm, dynMultiLangField,  ui, id, principal.getCompCode());
				}
							
				
			}catch(Exception e){
				
			}finally{
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dbm = null;
				conn = null;
			}
			
		}else{
			Field positive = new Field("ok", "button", "BUTTON.OK");
			dialog = new Dialog("deleteDialog", "Notice", "Unknow Record", null, positive);
			
		}
		//redirect to table 
		resp = dm.getDynTable();
		if(dialog != null){
			dialog.translate(request);
			resp.setDialog(dialog);
		}

		return resp;
	}
	
	public Dialog dependenceDenyDialog(){
		String id = "dialogDeny";
		String title = "DELETE.DENY";
		//public Dialog(String id, String title, String message, Field positive, Field negative) {
		String msg = "DELETE.DENY.DEPENDENCE";
		Field positive = new Field("ok", "button", "BUTTON.OK");
		
		Dialog dialog = new Dialog(id, title, msg, null, positive);		
		return dialog;
	}
	
}
