package com.eab.biz.dynamic;

import java.sql.Connection;
import java.sql.Date;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.biz.environment.EnvironmentMgr;
import com.eab.biz.needs.NeedMgr;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.dynamic.DynamicDAO;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Response;
import com.eab.json.model.Field.Types;
import com.eab.model.dynamic.DynMultiLangField;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.webservice.dynamic.DynamicFunction;

public class ApproveManager {
	
	HttpServletRequest request;
	public ApproveManager(HttpServletRequest request){
		this.request = request;
	}
	
	public Response approve(){
		
		boolean success = true;
		Response resp = null;
		String module = request.getParameter("p1") ;
		String id = request.getParameter("p0") ;	
		DynamicUI ui = null;
		HttpSession session = request.getSession();
		DBManager dbm = null;
		Connection conn = null;	
		JSONObject template = null;	
		DynamicDAO dadynamic = new DynamicDAO();
		DynamicManager dm = new DynamicManager(request);
		Dialog dialog = null;
		boolean hasMulField = false;
			
		if (DynamicFunction.isNotEmptyOrNull(module) && DynamicFunction.isNotEmptyOrNull(id)) {
			try {
				conn = DBAccess.getConnection();
				dbm = new DBManager();
				ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
				if (ui == null || !ui.getModule().equals(module)) {
					DynamicDAO dynamic = new DynamicDAO();
					ui = dynamic.getDynamicUI(module, conn);
					session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
				}
				
				// get dynamic template
				template = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) != null ? (JSONObject) session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) : null;
				if (template == null || !module.equals(template.getString("id"))) {
					template = DynamicFunction.getTempalteWithDetailSEQ(module, ui, conn);
					session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, template);
				}	
				
				UserPrincipalBean principal = Function.getPrincipal(request); 
				//get action type
				String compCode = null;
				if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
					compCode = principal.getCompCode();
				}
							
				String action = dadynamic.checkRecordStatus(dbm, conn, id, ui, compCode);
				
				
				DynMultiLangField dynMultiLangField = dadynamic.getDynMultiLangField(ui.getModule());
				if(dynMultiLangField != null && dynMultiLangField.getTmpTable() != null){
					hasMulField = true;
				}
				
				//same approve date
				Calendar calendar = Calendar.getInstance();
				java.util.Date currentDate = calendar.getTime();
				Date approvDate = new java.sql.Date(currentDate.getTime());
				
				//update approve data and user in tmp
				dbm.clear();		
				dadynamic.updateTmpTableForCopy(request, dbm, ui, "A", id, approvDate);
				
				//insert or update copy from tmp to mst				
				if(action.equals("N")){
					//insert
					boolean insertToMst = false;
					//sql format : insert into sys_company_mst  select sys_company_mst_tmp.comp_code from sys_company_mst_tmp where comp_code = "EAB"
					String selectColsOringal = DynamicFunction.getTableColByTemplate(template, null);
					selectColsOringal = dateNUserCol(selectColsOringal, null);
					String selectCols = "(" + selectColsOringal + ")";
					
					String selectColsWithTable = DynamicFunction.getTableColByTemplate(template, ui.getTempTable());
					selectColsWithTable = dateNUserCol(selectColsWithTable, ui.getTempTable());
									
					String sqlMst = "insert into " + ui.getDataTable() + " " + selectCols + " select "+ selectColsWithTable +" from " + ui.getTempTable() + " where " + ui.getPrimaryKey()  + "=?";	
					dbm.param(id, DataType.TEXT);
					if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
						sqlMst += " and comp_code=?";
						dbm.param(compCode, DataType.TEXT);
					}
					
					insertToMst = dbm.insert(sqlMst);
					
					if(insertToMst){	
						switch(module){
						case "company":
							Log.debug("add a new env");
							//add env
							EnvironmentMgr env = new EnvironmentMgr(request);
							env.envDataInitial(id);
							break;
						case "channel":
							NeedMgr need = new NeedMgr(request);
							need.needDataInitial(compCode, id);
							break;
						default:
							break;
						}
					}
					
					//insert all multi lang text  from tmp to mst
					JSONArray items = template.getJSONArray("items");
					for(int j=0; j<items.length(); j++){
						JSONObject item = items.getJSONObject(j);
						if(Types.MULTTEXT.equals(item.getString("type"))){
							dbm.clear();
							
							//update the action to approve "A", 
							dadynamic.updateMULTmpTableForCopy(dbm, dynMultiLangField, ui, id, principal, approvDate, "A");
													
							//insert into mst 
							insertIntoMst(dbm, conn, ui, id, principal);
														
							//then move to aud
							dadynamic.copyMULfromTmpToAud(dbm, dynMultiLangField,  ui, id, compCode);
							
							//delete from tmp table
							dadynamic.deleteMULFromTmp(dbm, dynMultiLangField, ui, id, principal);													
							
						}
					}
					
				}else{
						
					if(!action.equals("D")){
						Log.debug("approv : update");
						//update
						String setCols = null;
						String sqlUpdateMst = null;
						setCols = getUpdateTableColByTemplate(template, ui, dbm, id, principal);	
						setCols = getUpdateDateNUserCols(setCols, ui, dbm, id, principal);
						sqlUpdateMst = "update " + ui.getDataTable() + " set " +  setCols + " where " + ui.getPrimaryKey() + "=?";
						dbm.param(id, DataType.TEXT);
						if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
							sqlUpdateMst += " and comp_code=?";
							dbm.param(compCode, DataType.TEXT);
						}
						dbm.update(sqlUpdateMst, conn);
						
						//multi text fields 
						//insert all multi lang text  from tmp to mst
						dbm.clear();
						if(hasMulField){
							dadynamic.deleteMULFromMst(dbm, dynMultiLangField, ui, id, principal);
							
							JSONArray items = template.getJSONArray("items");
							for(int j=0; j<items.length(); j++){
								JSONObject item = items.getJSONObject(j);
								if(Types.MULTTEXT.equals(item.getString("type"))){
									dbm.clear();
									/*1.update tmp table for copy								
										2. update mst table
										3. copy tmp to aud
									*/								
									//update the action to approve "A", 
									dadynamic.updateMULTmpTableForCopy(dbm, dynMultiLangField, ui, id, principal, approvDate, "A");
									
									//insert into mst 
									insertIntoMst(dbm, conn, ui, id, principal);
																
									//then move to aud
									dadynamic.copyMULfromTmpToAud(dbm, dynMultiLangField,  ui, id, compCode);
									
									//delete from tmp table
									dadynamic.deleteMULFromTmp(dbm, dynMultiLangField, ui, id, principal);													
									
								}
							}
						}
						
					}else{
						//delete
						//delete the record in mst		
						dbm.clear();
						String deleteMstSql = "delete from " + ui.getDataTable() + " where " + ui.getPrimaryKey() +"=?";
						dbm.param(id, DataType.TEXT);
						if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
							deleteMstSql += " and comp_code=?";
							dbm.param(compCode, DataType.TEXT);
						}
						int deleteFromMst = dbm.delete(deleteMstSql);
						if(deleteFromMst == -1){
							success = false;
							//String id, String title, String message, Field positive, Field negative
							Field negative = new Field("ok", "button", "BUTTON.OK");
							dialog = new Dialog("approveDialog", "BUTTON.APPROVE", "RECORD.DELETE.FAIL", null, negative, 30);
							dialog.translate(request);
							//resp.setDialog(dialog);
							
						}
						
						//delete multi lang
						
						if(hasMulField){
							//1. update tmp modify and approve date
							//2. copy from tmp to aud
							//3. delete from mst and tmp
							dadynamic.updateMULTmpTableForCopy(dbm, dynMultiLangField, ui, id, principal, approvDate, "A");
							//then move to aud
							dadynamic.copyMULfromTmpToAud(dbm, dynMultiLangField,  ui, id, compCode);
							
							dadynamic.deleteMULFromTmp(dbm, dynMultiLangField, ui, id, principal);	
							
							dadynamic.deleteMULFromMst(dbm, dynMultiLangField, ui, id, principal);	
							
						}
						
					}
				}					
				
				//update tmp action->"approve", data and user
				if(success){
					dadynamic.updateTmpTableForCopy(request, dbm, ui,"A", id);	
				
					//insert copy from tmp to aud
					
					dadynamic.copyFromTmpToAud(dbm, ui, id, principal);
				}
				
				//delete record in tmp
				dbm.clear();
				dadynamic.deleteFromTmp(dbm, ui, id, principal);								
				
			}catch(Exception e){
				Log.error(e);
				Field negative = new Field("ok", "button", "BUTTON.OK");
				dialog = new Dialog("approveDialog", "BUTTON.APPROVE", "RECORD.APPROVE.FAIL", null, negative, 30);
				dialog.translate(request);
				
			}finally{
				resp = dm.getDynTable();
				resp.setDialog(dialog);
				
				try{
					if(!conn.isClosed()) conn.close();
					conn = null;
				}catch(Exception e){
					Log.error(e);
				}
				dbm = null;
			}
								
		}
		
		return resp;
	}
	
	public static String getUpdateTableColByTemplate(JSONObject template, DynamicUI ui, DBManager dbm, String id, UserPrincipalBean principal) {
		String tableName = ui.getTempTable();
		String primaryKey = ui.getPrimaryKey();
		String cols = null;
		JSONArray items = template.getJSONArray("items");
		int length = items.length();
		boolean compFil = ui.getCompanyFilter() && !"company".equals(ui.getModule());
		for (int i = 0; i < length; i++) {
			JSONObject item = items.getJSONObject(i);
			String col = item.getString("key");
			boolean seqTrue = false;
			if(item.has("detailSeq")){
				if(item.get("detailSeq") != null && item.getInt("detailSeq") != 0 && !Types.MULTTEXT.equals(item.getString("type"))){
					 seqTrue = true;
				}
			}
			if(!col.equals(primaryKey) &&  seqTrue) {
				if(DynamicFunction.isNotEmptyOrNull(col)){
					try {
						String fieldWithTable = ui.getDataTable()+"."+col + "=(select "+tableName + "." +col+" from " + tableName + " where "+tableName +"."+ ui.getPrimaryKey() +"=?";
						dbm.param(id, DataType.TEXT);
						if(compFil){
							fieldWithTable += " and comp_code=?";
							dbm.param(principal.getCompCode(), DataType.TEXT);
						}
						fieldWithTable += ")";
						
						String newCol = DynamicFunction.isNotEmptyOrNull(tableName) ? fieldWithTable : col;
						if(cols == null){					
							cols = newCol;
						}else{
							cols += ", " + newCol;
						}
					} catch (Exception e) {
						// 
					}									
				}
			}
			
		}
		return cols;
		
	}
	
	private String getUpdateDateNUserCols(String col, DynamicUI ui, DBManager dbm, String id, UserPrincipalBean principal){
		String[] cols = new String[]{"MODIFY_DATE", "MODIFY_BY", "APPROV_DATE", "APPROV_BY"};
		
		for(int i = 0; i < cols.length; i++){
			String newCol = getUpdateDateNUserCol(ui, dbm, id, cols[i], principal);
			if(col == null)
				col += newCol;
			else{
				col += ","+ newCol;
			}
		}
		return col;
	}
	
	private String getUpdateDateNUserCol(DynamicUI ui, DBManager dbm, String id, String col, UserPrincipalBean principal){
		String colStr = "";
		
		try {
			colStr = ui.getDataTable()+"."+col + "=(select "+col+" from " + ui.getTempTable() + " where "+ui.getTempTable()+"."+ui.getPrimaryKey() +"=?";
			dbm.param(id, DataType.TEXT);
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				colStr += " and comp_code=?";
				dbm.param(principal.getCompCode(), DataType.TEXT);
			}
			colStr += ")";
			
		} catch (Exception e) {
			// 
		}
		
		return colStr;
			
	}
	
	private String dateNUserCol(String col, String table){
		if(table == null)
			col += ",CREATE_DATE, CREATE_BY, MODIFY_DATE, MODIFY_BY, APPROV_DATE, APPROV_BY";
		else{
			col += ","+table+".CREATE_DATE, "+table+".CREATE_BY ,"+table+".MODIFY_DATE,"+table+".MODIFY_BY,"+table+".APPROV_DATE,"+table+".APPROV_BY";
		}
		return col;
	}
	
	private void insertIntoMst(DBManager dbm, Connection conn, DynamicUI ui, String id, UserPrincipalBean principal){
		DynamicDAO dao = new DynamicDAO();
		DynMultiLangField dynMultiLangField = dao.getDynMultiLangField(ui.getModule());
		String conds = null;
		try{
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				conds = "comp_code = ?";
				dbm.param(principal.getCompCode(), DataType.TEXT);
			}
			conds = (conds == null) ? ui.getPrimaryKey() + " = ?" : conds + " and " + ui.getPrimaryKey() + " = ?";
			dbm.param(id, DataType.TEXT);
			
			String sqlMstInsert = "insert into "+ dynMultiLangField.getMstTable() +" ";
			String cols = null;
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				cols = "COMP_CODE";
			}
			cols = (cols == null) ?  ui.getPrimaryKey() : cols + ", " + ui.getPrimaryKey();						
			cols += ", LANG_CODE, NAME_DESC, STATUS, CREATE_BY, CREATE_DATE, MODIFY_BY, MODIFY_DATE, NAME_TYPE";
			
			sqlMstInsert +="(" + cols + ")"+ " select " +  cols + " from " + dynMultiLangField.getTmpTable() + " where " + conds;
	
			dbm.insert(sqlMstInsert);
			dbm.clear();
		}catch(Exception e){
			dbm.clear();
		}
	}
	
	
}
