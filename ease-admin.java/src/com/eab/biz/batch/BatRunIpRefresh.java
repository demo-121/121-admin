package com.eab.biz.batch;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.eab.common.Constants;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DataType;
import com.eab.database.jdbc.DBManager;


public class BatRunIpRefresh {
	
	public void refresh() throws SQLException {
		Connection conn = null;
		StringBuffer sb = new StringBuffer();
		
		DBManager dbm = new DBManager();
		String localHost = null;
		int count = 0;
		ResultSet rs = null;

		Log.info("{BatRunIpRefresh refresh} ---------START---------");
		try {
			localHost = Constants.SERVER_IP_ADDR;
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			
			sb.delete(0, sb.length());
			sb.append("select count(1) as lcount from sys_batch_running_ip");
			
			rs = dbm.select(sb.toString(), conn);
			count = rs.getInt("lcount");
			Log.info("{BatRunIpRefresh refresh} count="+count);
			
			if (count>0) {
				sb.delete(0, sb.length());
				sb.append("update sys_batch_running_ip set last_upd_date=SYSDATE where app_server_ip =" + dbm.param(localHost, DataType.TEXT));
				int dummy = dbm.update(sb.toString(), conn);
				Log.info("{BatRunIpRefresh refresh} update="+dummy);
			} else {
				sb.delete(0, sb.length());
				sb.append("INSERT INTO sys_batch_running_ip (app_server_ip,last_upd_date) ");
				sb.append("VALUES("+ dbm.param(localHost, DataType.TEXT)+",SYSDATE)");
				boolean dummy = dbm.insert(sb.toString(), conn);
				Log.info("{BatRunIpRefresh refresh} insert="+dummy);
			}
			conn.commit();
			Log.info("{BatRunIpRefresh refresh} -----------------> Committed");
		} catch (Exception e) {
			Log.info("{BatRunIpRefresh refresh} -----------------> Exception & Rollback");
			conn.rollback();
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
			
			Log.info("{BatRunIpRefresh refresh} -----------------> Finally is completed");
		}
		
		Log.info("{BatRunIpRefresh refresh} ---------END---------");
	}
	
	public boolean updateServerIp() throws SQLException {
		Connection conn = null;
		StringBuffer sb = new StringBuffer();
		StringBuffer sql = new StringBuffer();
		DBManager dbm = new DBManager();
		String localHost = null;
		String serverIp = null;
		String upd_flag =null;
		ResultSet result = null;
		boolean isProssBatch = false;

		Log.info("{BatRunIpRefresh updateServerIp} ---------START---------");
		try {
			localHost = Constants.SERVER_IP_ADDR;
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
						
			sb.delete(0, sb.length());
			sb.append("select app_server_ip,last_upd_date,(sysdate-last_upd_date)*24*60 as upd_flag from sys_batch_running_ip");
			result = dbm.select(sb.toString(), conn);
			while (result.next()) {
				serverIp = result.getString("app_server_ip");
				upd_flag = result.getString("upd_flag");
			}
			Log.info("{BatRunIpRefresh updateServerIp} serverIp="+serverIp);
			Log.info("{BatRunIpRefresh updateServerIp} upd_flag="+upd_flag);
			
			if (upd_flag==null||"".equals(upd_flag)){
				upd_flag = "0";
			}
			
			
			if(serverIp!=null&&!"".equals(serverIp)&&localHost.equals(serverIp)){
				sql.delete(0, sql.length());
				sql.append("update sys_batch_running_ip set last_upd_date=SYSDATE where app_server_ip ="+dbm.param(localHost, DataType.TEXT));
				int dummy = dbm.update(sql.toString(), conn);
				Log.info("{BatRunIpRefresh updateServerIp} update="+dummy);
				isProssBatch = true;
				Log.info("{BatRunIpRefresh updateServerIp} ip equal---isProssBatch----"+isProssBatch);
			}else if(serverIp!=null&&!"".equals(serverIp)&&(Double.parseDouble(upd_flag)>1)){
				sql.delete(0, sql.length());
				sql.append("update sys_batch_running_ip set app_server_ip="+dbm.param(localHost, DataType.TEXT)+", last_upd_date=SYSDATE ");
				int dummy = dbm.update(sql.toString(), conn);
				Log.info("{BatRunIpRefresh updateServerIp} update="+dummy);
				isProssBatch = true;
				Log.info("{BatRunIpRefresh updateServerIp} ip not equal---isProssBatch----"+isProssBatch);
			}
		
			conn.commit();
		} catch (Exception e) {
			Log.info("{BatRunIpRefresh updateServerIp} -----------------> Exception & Rollback");
			conn.rollback();
			Log.error(e);
		} finally {
			dbm = null;
			result = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
			
			Log.info("{BatRunIpRefresh updateServerIp} -----------------> Finally is completed");
		}
		Log.info("{BatRunIpRefresh updateServerIp} ---------END---------");
		
		return isProssBatch;
	}
}
