package com.eab.biz.tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.eab.biz.beneIllus.BeneIllusMgr;
import com.eab.biz.dynTemplate.DetailMgr;
import com.eab.biz.needs.NeedDetailMgr;
import com.eab.biz.needs.NeedMgr;
import com.eab.biz.products.ProductMgr;
import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.dynTemplate.DynTemplate;
import com.eab.dao.products.ProductsDetailTask;
import com.eab.dao.tasks.Tasks;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.json.model.Trigger;
import com.eab.json.model.Field.Types;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.tasks.TaskBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class TaskInfoMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	static String FIELD_ID = "TaskInfo";
	String _module;
	
	public TaskInfoMgr(HttpServletRequest request, String _module) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		this._module = _module;
	}

	public Response goToReleaseSelectionPage(List<Object> taskList) throws Exception {
		//Get Json
		TaskReleaseMgr taskRelease = new TaskReleaseMgr(request);
		Response response = gson.fromJson(taskRelease.getJson("", taskList, "", "", 0 , 10, "/Task/Assign"), Response.class);
		JsonObject values = new JsonObject();
		values.addProperty("actionType", "merge");
		addValues(values,(int)taskList.get(0), null, -1, null);
		Content content = new Content(values);
		response.setContent(content);
		
		return response;
	}
	
	public Response taskDetailUpdate(List<Object> taskList, int newReleaseID, String action) throws Exception {
		Gson gson = new GsonBuilder().create();
		Response response = taskUpdate(taskList, newReleaseID, action);
		if(action.equals("D") || action.equals("R")){
			Tasks taskDao = new Tasks();
			Map<String, Object> task = taskDao.getTaskById((int) taskList.get(0), null, -1, null, null, principal);
			String itemCode = task.get("item_code").toString(); 
			String _funcCode = task.get("func_code").toString();
			int version = (int) task.get("ver_no");
			Response _resp = null;
			DynTemplate dynTemplate = new DynTemplate();
			String module = dynTemplate.getModuleByFuncCode(null, _funcCode);
			switch(_funcCode){
				case "needs":
					NeedDetailMgr needMgr = new NeedDetailMgr(request);
					String channel = task.get("channel_code").toString();
					_resp = needMgr.getNeedDetail(itemCode, channel, version, Constants.NeedPage.TASK, false);
				default:
					DetailMgr dynDetailMgr = new DetailMgr(request);
					dynDetailMgr.getDetails(module, itemCode, version, null);
			}
			if(_resp != null){
				_resp.setAction(Constants.ActionTypes.CHANGE_CONTENT);
				return _resp;
			}
		}
		return response;
	}
		
	public Response taskUpdate(List<Object> taskList, int newReleaseID, String action) throws Exception {
		HttpSession session = request.getSession();
		UserPrincipalBean principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
				
		Tasks taskDao = new Tasks();
		
		switch(action){
		case "A"://Assign
			taskDao.assignTasks(newReleaseID, taskList, principal);
			break;
		case "U"://Unassign
			taskDao.unassignTasks(taskList, principal);	
			break;
		case "D"://Suspend
			taskDao.suspendTasks(taskList, principal);	
			break;
		case "R"://Resume
			taskDao.resumeTasks(taskList, principal);
			break;
		}
		
		// Token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		
		// content
		JsonObject values = new JsonObject();
		values.addProperty("actionType", "merge");
		addValues(values, (int)taskList.get(0), null, -1,null);
		Content content = new Content(null, values);
		
		return new Response(Constants.ActionTypes.RESET_VALUES, null, null, tokenID, tokenKey, null, content);
	}
	
	public Response reassignTask(List<Object> taskList) throws Exception {
		HttpSession session = request.getSession();
		UserPrincipalBean principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
				
		Tasks taskDAO = new Tasks();
		taskDAO.reassignTasks(taskList, principal);
						
		//Get Json
		return goToReleaseSelectionPage(taskList);
	}		
	
//	
//	
	public void addSection(Map<String, Object> taskInfo, List<Field> sections, boolean isNewItem, boolean isReadOnly){
		try{
			Log.debug("---------------------[TaskMgr - start adding section]--------------------------");
			List<Field> taskInfoActions = new ArrayList<Field>();
			boolean isSuspend = taskInfo!=null && !isNewItem && taskInfo.containsKey("statusCode")?taskInfo.get("statusCode").equals("D"):false;
			if (!isReadOnly) {
				// Action button
				Field btnCancel = new Field("confirm_cancel", "button", "cancel");
				
				// action assign
				Field assignField = new Field("/Tasks/Detail/Assign", Types.ACTION_ASSIGN, langObj.getNameDesc("BUTTON.ASSIGN"));
				assignField.setTrigger(new ArrayList<Trigger>());
				assignField.getTrigger().add(new Trigger("showIfContains", "statusCode","U"));
				taskInfoActions.add(assignField);
				
				// action - unassign
				Field btnUnassignOk = new Field("/Releases/Detail/Unassign", "customButton", "ok");
				Dialog confirmUnassignDialog = new Dialog("confirm", "Task(s) Unassign", "Are you confirm to unassign current selected task(s)?", btnUnassignOk, btnCancel, 30);
				Field unAssignField = new Field("/Tasks/Detail/Unassign", Types.ACTION_ASSIGN, langObj.getNameDesc("BUTTON.UNASSIGN"), confirmUnassignDialog);
				unAssignField.setTrigger(new ArrayList<Trigger>());
				unAssignField.getTrigger().add(new Trigger("showIfContains", "statusCode","A"));
				taskInfoActions.add(unAssignField);
				
				// action - reassign
				Field btnReassignOk = new Field("/Releases/Detail/Reassign", "customButton", "ok");
				Dialog confirmReassignDialog = new Dialog("confirm", "Task(s) Reassign", "Are you confirm to reassign current selected task(s)?", btnReassignOk, btnCancel, 30);
				Field reAssignField = new Field("/Tasks/Detail/Reassign", Types.ACTION_ASSIGN, langObj.getNameDesc("BUTTON.REASSIGN"), confirmReassignDialog);
				reAssignField.setTrigger(new ArrayList<Trigger>());
				reAssignField.getTrigger().add(new Trigger("showIfContains", "statusCode","A"));
				taskInfoActions.add(reAssignField);
				
				// action - suspend
				Field btnSuspendOk = new Field("/Releases/Detail/Suspend", "customButton", "ok");
				Dialog confirmSuspendDialog = new Dialog("confirm", "Task(s) Suspend", "Are you confirm to suspend current selected task(s)?", btnSuspendOk, btnCancel, 30);
				Field suspendField = new Field("/Tasks/Detail/Suspend", Types.ACTION_ASSIGN, langObj.getNameDesc("BUTTON.SUSPEND"), confirmSuspendDialog);
				suspendField.setTrigger(new ArrayList<Trigger>());
				suspendField.getTrigger().add(new Trigger("showIfContains", "statusCode","A"));
				taskInfoActions.add(suspendField);
				
				// action - resume
				Field resumeField = new Field("/Tasks/Detail/Resume", Types.ACTION_ASSIGN, langObj.getNameDesc("BUTTON.RESUME"));
				resumeField.setTrigger(new ArrayList<Trigger>());
				resumeField.getTrigger().add(new Trigger("showIfContains", "statusCode","D"));
				taskInfoActions.add(resumeField);
	
			}
			List<Field> taskInfoItems = new ArrayList<Field>();
			boolean isEdit = !(isReadOnly || (taskInfo!=null?taskInfo.get("statusCode").equals("D"):false));
			taskInfoItems.add(new Field("actions", Types.ACTION, null, null, 100, null, null, false, !isEdit, null, taskInfoActions, null, null));
			taskInfoItems.add(new Field("task_desc", Types.TEXT, null, langObj.getNameDesc("DESCRIPTION"), 200, "100%", null, true, isSuspend || !isEdit, null, null, null, 500));
			taskInfoItems.add(new Field("ref_no", Types.TEXT, null, langObj.getNameDesc("REF"), 300, "100%", null, false, isSuspend || !isEdit, null, null, null, 100));
			taskInfoItems.add(new Field("effDate", Types.DATEPICKER, null, langObj.getNameDesc("EFF_DATE"), 400, "100%", null, true, isSuspend || !isEdit, null, null, null, null));
			taskInfoItems.add(new Field("status", Types.READONLY, null, langObj.getNameDesc("STATUS"), 500, "100%", null, false, isSuspend || !isEdit, null, null, null, null));
			if (!isNewItem) {
				taskInfoItems.add(new Field("base_version", Types.READONLY, null, langObj.getNameDesc("BASE_VERSION"), 600, "100%", null, false, isSuspend || !isEdit, null, null, null, null));
				taskInfoItems.add(new Field("upd", Types.READONLY, null, langObj.getNameDesc("LAST_UPD"), 700, "100%", null, false, isSuspend || !isEdit, null, null, null, null));
				taskInfoItems.add(new Field("create", Types.READONLY, null, langObj.getNameDesc("CREATE_BY"), 800, "100%", null, false, isSuspend || !isEdit, null, null, null, null));
			}
			Field sectionTaskInfo = new Field(FIELD_ID, Types.SECTION, langObj.getNameDesc("TASK_INFO"), true, taskInfoItems);
			sections.add(sectionTaskInfo);
			Log.debug("---------------------[TaskMgr - end adding section]--------------------------");
		}catch(Exception e){
			Log.error(e);
		}
	}
	
	
	public Map<String, Object> addValues(JsonObject values, int taskId, String itemCode, int version, String channelCode){
		try{
			Tasks taskDAO = new Tasks();
			Map<String, Object> task = taskDAO.getTaskById(taskId, itemCode, version, _module, channelCode, principal);
			return addValues(task, values);
		}catch(Exception e){
			Log.error(e);
		}
		return null;
	}
	
	public Map<String, Object> addValues(Map<String, Object> task, JsonObject values){
		try{
			JsonObject taskObj = null;
			if(task!=null){
				taskObj = new JsonObject();
				for (String key : task.keySet()) {
					if(key.equals("taskId") && task.get(key) != null){
						values.add(key, JsonConverter.convertObj(task.get(key)));
					}else if(task.get(key) != null)
						taskObj.add(key, JsonConverter.convertObj(task.get(key)));
				}
				values.add(FIELD_ID, taskObj);
			}
			return task;
		}catch(Exception e){
			Log.error(e);
		}
		return null;
	}

}
