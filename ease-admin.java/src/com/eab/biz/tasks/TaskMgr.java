package com.eab.biz.tasks;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.eab.biz.beneIllus.BeneIllusMgr;
import com.eab.biz.common.CommonManager;
import com.eab.biz.dynTemplate.DetailMgr;
import com.eab.biz.dynamic.LockManager;
import com.eab.biz.needs.NeedDetailMgr;
import com.eab.biz.needs.NeedMgr;
import com.eab.biz.products.ProductMgr;
import com.eab.common.Constants;
import com.eab.common.Function2;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.dynTemplate.DynData;
import com.eab.dao.dynamic.RecordLock;
import com.eab.dao.tasks.Tasks;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.ListContent;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.SearchCondition;
import com.eab.json.model.Template;
import com.eab.model.dynamic.RecordLockBean;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class TaskMgr {
	
	HttpServletRequest request;
	HttpSession session;
	UserPrincipalBean principal;
	Language langObj;

	public TaskMgr(HttpServletRequest request) {
		super();
		this.request = request;
		this.session = request.getSession();
		this.principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		this.langObj = new Language(principal.getUITranslation());
	}
	
	public String getJson(String criteria, String typeFilter, String statusFilter, String sortBy, String sortDir, int recordStart, int pageSize) throws Exception {
		ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
								
		if (recordStart != 0) 
			list = (ArrayList<Map<String, Object>>) session.getAttribute(Constants.SessionKeys.SEARCH_RESULT);
						
		//Get list
		if (list.size() == 0) {			
			list = this.getList(criteria, typeFilter, statusFilter, sortBy, sortDir);
			
			//Add result to session for "More" button use
			session.setAttribute(Constants.SessionKeys.SEARCH_RESULT, list);
		}
			
		//Get template
		List<Field> item = this.getTemplate();
		
		String action = "";
		
		//Action	
		CommonManager cMgr = new CommonManager(request);
		boolean isChangePage = cMgr.isChangePage();
		if ((criteria != "" || typeFilter != "" || statusFilter != "") && !isChangePage) {
			//Page content change
			action =  Constants.ActionTypes.CHANGE_CONTENT;
		} else {
			//First time load
			action =  Constants.ActionTypes.CHANGE_PAGE;
		}
		
	    //Page
		JsonObject pageValue = new JsonObject();
		pageValue.addProperty("criteria", criteria);
		pageValue.addProperty("typeFilter", typeFilter);
		pageValue.addProperty("statusFilter", statusFilter);
		pageValue.addProperty("sortBy", sortBy);
		pageValue.addProperty("sortDir", sortDir);
		pageValue.addProperty("recordStart", recordStart);
		pageValue.addProperty("pageSize", pageSize);
		Page page = new Page("/Tasks", langObj.getNameDesc("MENU.TASKS").toString(), pageValue);
		
	    //Token	    
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());

	    //Template		
		Template template = new Template();
		template.setItems(item);
	    
		//More
	    int offset = 0;
	    int size = 10;	  
	    
	    List<Object> shortedList = new ArrayList<>();
		offset = recordStart;
		 
		size = pageSize != 0 ? pageSize : Constants.SEARCH_RESULT_DEFAULT_PAGE_SIZE;
	    		
		if (list != null) {
			for (int j = offset; j < offset + size && j < list.size(); j++) {
				shortedList.add(list.get(j));
			}
		}

		//Content - values
		ListContent contentValues = new ListContent(shortedList, list != null ? list.size(): 0, offset > 0);
	    	    
	    //Content
	    Content content = new Content(template, contentValues);
	    	    
	    //AppBar - Title & Task Type Filter
	    Tasks taskDao = new Tasks();
	    List<Option> appBarTitleOption = taskDao.getTaskTypes(null);
		AppBarTitle appBarTitle = new AppBarTitle("typeFilter", "picker", "*", null, appBarTitleOption, null);
		appBarTitle.setSetAll(true);
		appBarTitle.setMandatory(true);
		
	    //AppBar - Task Status Filter
	    List<Option> appBarAction1_Option = new ArrayList<>();
	    appBarAction1_Option = Function2.getOptionsByLookupKey(request, "TASK_STATUS_FILTER", langObj);
		Field appBarAction1_1 = new Field("statusFilter", "picker", "*", null, appBarAction1_Option);
		
	    
	    //AppBar - Search
		Field appBarAction1_2 = new Field("searchString", "searchButton", langObj.getNameDesc("TASK_APPBAR_SEARCH").toString(), langObj.getNameDesc("SEARCH").toString(), null);
	
	    //AppBar - Add actions
		List<Field> appBarAction1 = new ArrayList<>();
	    appBarAction1.add(appBarAction1_1);
	    appBarAction1.add(appBarAction1_2);
	    
	    //Define actions - S
	    //Assign  
	    Field appAssign = new Field("/Task/Release", "submitSelectedButton", langObj.getNameDesc("BUTTON.ASSIGN").toString());
	    	    			   	    
	    String strOK = langObj.getNameDesc("BUTTON.OK").toString();
	    String strCancel = langObj.getNameDesc("BUTTON.CANCEL").toString();
	    
	    //Suspend
		Field btnSuspendOk = new Field("/Tasks/Suspend", "submitButton", strOK);
		Field btnSuspendCancel = new Field("confirm_cancel", "button", strCancel);
		Dialog confirmSuspendDialog = new Dialog("confirmSuspendDialog", langObj.getNameDesc("TASK_SUSPEND").toString(), langObj.getNameDesc("TASK_CONFIRM_SUSPEND").toString(), btnSuspendOk, btnSuspendCancel, 30);  
	    Field appSuspend = new Field("S", "dialogButton", langObj.getNameDesc("BUTTON.SUSPEND").toString(), confirmSuspendDialog);
	    	    
	    //Un-assign
		Field btnUnassignOk = new Field("/Tasks/Unassign", "submitButton", strOK);
		Field btnUnassignCancel = new Field("confirm_cancel", "button", strCancel);
		Dialog confirmUnassignDialog = new Dialog("confirmUnassignDialog", langObj.getNameDesc("TASK_UNASSIGN").toString(), langObj.getNameDesc("TASK_CONFIRM_UNASSIGN").toString(), btnUnassignOk, btnUnassignCancel, 30);  
	    Field appUnassign = new Field("confirmUnassignDialog", "dialogButton", langObj.getNameDesc("BUTTON.UNASSIGN").toString(), confirmUnassignDialog);
	    
	    //Reassign
		Field btnReassignOk = new Field("/Tasks/Reassign", "submitButton", strOK);
		Field btnReassignCancel = new Field("confirm_cancel", "button", strCancel);
		Dialog confirmReassignDialog = new Dialog("confirmReassignDialog", langObj.getNameDesc("TASK_REASSIGN").toString(), langObj.getNameDesc("TASK_CONFIRM_REASSIGN").toString(), btnReassignOk, btnReassignCancel, 30);  
	    Field appReassign = new Field("confirmReassignDialog", "dialogButton", langObj.getNameDesc("BUTTON.REASSIGN").toString(), confirmReassignDialog);
	    
	    //Resume
		Field btnResumeOk = new Field("/Tasks/Resume", "submitButton", strOK);
		Field btnResumeCancel = new Field("confirm_cancel", "button", strCancel);
		Dialog confirmResumeDialog = new Dialog("confirmResumeDialog", langObj.getNameDesc("TASK_RESUME").toString(), langObj.getNameDesc("TASK_CONFIRM_RESUME").toString(), btnResumeOk, btnResumeCancel, 30);  
	    Field appResume = new Field("confirmResumeDialog", "dialogButton", langObj.getNameDesc("BUTTON.RESUME").toString(), confirmResumeDialog);
	    //Define actions - E

	    //Task status = Unassigned
		List<Field> appBarAction2 = new ArrayList<>();
		appBarAction2.add(appAssign);
		//appBarAction2.add(appSuspend);
	    
	    //Task status = Assigned
		List<Field> appBarAction3 = new ArrayList<>();
		appBarAction3.add(appUnassign);
		appBarAction3.add(appReassign);
		//appBarAction3.add(appSuspend);
			    
	    //Task status = Suspended
		List<Field> appBarAction4 = new ArrayList<>();
		//appBarAction4.add(appResume);

	    //Task status = Unassigned + Assigned
		List<Field> appBarAction5 = new ArrayList<>();
		//appBarAction5.add(appSuspend);
	    
	    //AppBar - Actions	    
	    List<List<Field>> appBarActions = new ArrayList<>();
	    appBarActions.add(appBarAction1);
	    appBarActions.add(appBarAction2);
		appBarActions.add(appBarAction3);
		appBarActions.add(appBarAction4);
		appBarActions.add(appBarAction5);

	    //AppBar	    
		AppBar appBar = new AppBar(appBarTitle, appBarActions);
		SearchCondition cond = SearchCondition.RetrieveSearchCondition(request);
		appBar.setValues(cond);

	    //Convert to Json  
		Gson gson = new GsonBuilder().create();
	    String outputJSON = gson.toJson(new Response(action, null, page, tokenID, tokenKey, appBar, content));
	    
	    return outputJSON;
	}
		
	public ArrayList<Map<String, Object>> getList(String criteria, String typeFilter, String statusFilter, String sortBy, String sortDir) throws Exception {		
		Tasks tasksDAO = new Tasks();
		return tasksDAO.getTaskList(request, principal, criteria, typeFilter, statusFilter, sortBy, sortDir, langObj);
	}
	
	public List<Field> getTemplate() throws Exception {	   
	    List<String> List = Arrays.asList("TYPE", "TASK_CODE_NAME_VER", "DESCRIPTION", "REF", "STATUS", "LAST_UPDATED");
	    List<String> ListType = Arrays.asList("text", "text", "text", "text", "text", "text");
	    List<String> ListId = Arrays.asList("type", "item_code", "task_desc", "ref_no", "status", "upd_date");
	    List<String> ListWidth = Arrays.asList("150px", "auto", "auto", "200px", "150px", "150px");
	    
	    int seq = 0;
		List<Field> template = new ArrayList<>();
	    
		for(String label: List) {
			if (label.equals("STATUS")) {
				Field field1 = new Field(ListId.get(seq),langObj.getNameDesc(label),ListType.get(seq), seq+1, seq+1,ListWidth.get(seq), "tooltipId");
		    	template.add(field1);
			} else {
		    	Field field1 = new Field(ListId.get(seq),langObj.getNameDesc(label),ListType.get(seq), seq+1, seq+1,ListWidth.get(seq));
		    	template.add(field1);
			}
	    	
		    seq ++;
	    }

	    return template;
	}

	public String updateTask(List<Object> taskList, String action, int releaseID) throws Exception {
		Tasks taskDAO = new Tasks();
		String outputJSON;
				
		switch (action) {
		case "A"://Assign  
			taskDAO.assignTasks(releaseID, taskList, principal);
			break;
		case "U"://Unassign
			taskDAO.unassignTasks(taskList, principal);			
			break;
		case "S"://Suspend  
			taskDAO.suspendTasks(taskList, principal);
			break;
		case "R"://Reassign
			taskDAO.unassignTasks(taskList, principal);	
			
			TaskReleaseMgr taskRelease = new TaskReleaseMgr(request);
			return outputJSON = taskRelease.getJson("", taskList, "", "", 0, 0, "/Task/Release/Assign");					
		case "M"://Resume
			taskDAO.resumeTasks(taskList, principal);
			break;					
		default: 
			
			break;
		}
				
		if (action.equals("A"))
			outputJSON = getJson("", "", "", "", "", 0, 0);//Set criteria to empty will case the action type return "CHANGE_PAGE"
		else 
			outputJSON = getJson("%", "", "", "", "", 0, 0);//Set criteria to % will case the action type return "CHANGE_CONTENT"
		
	    return outputJSON;
	}
	
	public String pageRedirect(String taskID, String backFuncCode, String releaseID) throws Exception {		
		Tasks tasksDAO = new Tasks();
		Map<String, Object> keys = tasksDAO.getRedirectKeys(principal, taskID);
		
		String func_code = keys.get("func_code").toString();
		String item_code = keys.get("item_code").toString();
		String version = keys.get("version").toString();
		
		String channel_code = "";
		if (keys.get("channel_code") != null && !keys.get("channel_code").equals(""))
			channel_code = keys.get("channel_code").toString();
		
		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		
		DynData dynData = new DynData();
		String module = dynData.getModuleByFuncCode(null, func_code);
		switch(module){
		case "needs"://Needs
			NeedDetailMgr need = new NeedDetailMgr(request);
			return gson.toJson(need.getNeedDetail(item_code, channel_code, Integer.parseInt(version), Constants.NeedPage.TASK, false));
		default:
			DetailMgr dynDetailMgr = new DetailMgr(request);
			return gson.toJson(dynDetailMgr.getDetails(module, item_code, Integer.parseInt(version), null));
		}
	}
	
	public String addContentToAssignPage(String ouptString){
		try{
			String statusFilter = "";
			String typeFilter = "";
			String criteria = "";
			String sortBy = "";		
			String sortDir = "";	
			int recordStart = 0;
			int pageSize = 0;
			// Get Parameter
			try {
				String param = request.getParameter("p10");
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);
					if(resultJSON.has("value")){
						JSONObject value = resultJSON.getJSONObject("value");
						if (value.has("statusFilter"))
							statusFilter = (String) value.get("statusFilter");
						
						if (value.has("typeFilter"))
							typeFilter = (String) value.get("typeFilter");
						
						if (value.has("criteria"))
							criteria = (String) value.get("criteria");
						
						if (value.has("sortBy"))
							sortBy = (String) value.get("sortBy");
						
						if (value.has("sortDir"))
							sortDir = (String) value.get("sortDir");
						
						if (value.has("recordStart"))
							recordStart = (int) value.get("recordStart");
						
						if (value.has("pageSize"))
							pageSize = (int) value.get("pageSize");
					}
					
				}
			} catch (Exception e) {
				 Log.error(e);
			}
			
			Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
			Response contentResponse = gson.fromJson(getJson(criteria, typeFilter, statusFilter, sortBy, sortDir, recordStart, pageSize), Response.class);
			
			//add action to values
			Content content = contentResponse.getContent();
			JsonObject values = JsonConverter.convertObj(content.getValues()).getAsJsonObject();
			values.addProperty("actionType", "change");
			content.setValues(values);
			
			// add content and appbar to response
			Response result = gson.fromJson(ouptString, Response.class);
			result.setContent(content);
			result.setAppBar(contentResponse.getAppBar());
			result.setPage(null);
			return gson.toJson(result);
		}catch(Exception e){
			Log.error(e);
		}
		return ouptString;
	}
	
	
}
