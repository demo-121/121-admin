package com.eab.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.WriteListener;
//import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.audit.AudAccessLog;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.security.CsrfExclusion;
import com.eab.security.LoginExclusion;

public class RequestScan implements Filter {
	
//	CharResponseWrapper wrappedResponse = null;
	HttpServletResponse wrappedResponse = null;
	private FilterConfig filterConfig = null;

    private static class ByteArrayServletStream extends ServletOutputStream
    {
        ByteArrayOutputStream baos;

        ByteArrayServletStream(ByteArrayOutputStream baos) {
            this.baos = baos;
        }

        public void write(int param) throws IOException {
            baos.write(param);
        }

		@Override
		public boolean isReady() {
			return true;
		}

		@Override
		public void setWriteListener(WriteListener arg0) {
			throw new RuntimeException("Not implemented");
			
		}
    }
    
    private static class ByteArrayPrintWriter
    {

        private ByteArrayOutputStream baos = new ByteArrayOutputStream();

        private PrintWriter pw = new PrintWriter(baos);

        private ServletOutputStream sos = new ByteArrayServletStream(baos);

        public PrintWriter getWriter() {
            return pw;
        }

        public ServletOutputStream getStream() {
            return sos;
        }

        byte[] toByteArray() {
            return baos.toByteArray();
        }
    }
    
    public class CharResponseWrapper extends HttpServletResponseWrapper
    {
        private ByteArrayPrintWriter output;
//        private boolean usingWriter;

        public CharResponseWrapper(HttpServletResponse response)
        {
            super(response);
//            usingWriter = false;
            output = new ByteArrayPrintWriter();
        }

        public byte[] getByteArray()
        {
            return output.toByteArray();
        }

        @Override
        public ServletOutputStream getOutputStream() throws IOException
        {
//            // will error out, if in use
//            if (usingWriter) {
//                super.getOutputStream();
//            }
//            //usingWriter = true;
            return output.getStream();
        }

        @Override
        public PrintWriter getWriter() throws IOException
        {
//            // will error out, if in use
//            if (usingWriter) {
//                super.getWriter();
//            }
//            usingWriter = true;
//            return output.getWriter();
        	return output.getWriter();
        	
        }

        public String toString()
        {
            return output.toString();
        }
    }
	
	public void init(FilterConfig config) throws ServletException {	
		// Do Nothing
	}
	
	public void destroy() {
		// Do Nothing
	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
				throws IOException, ServletException {
		String sessionID = null;
		String jsessionID = null;
		String uri = null;
		HttpServletRequest httpRequest = null;
		HttpServletResponse httpResponse = null;
		HttpSession session = null;	
		String section = "";
		boolean scanFlag = false;
		String contextPath = null;
		
		boolean hasError = false;
		boolean flagCSRF = false;
		boolean flagSpecialChar = false;
		boolean flagCRLF = false;
		boolean flagServlet = false;
		
		boolean loginSession = false;
		
		httpRequest = (HttpServletRequest)request;
		httpResponse = (HttpServletResponse)response;
		session = httpRequest.getSession(false);
		
		section = "Request Initial";
		
		uri = httpRequest.getScheme() + "://" +
						httpRequest.getServerName() + 
						("http".equals(httpRequest.getScheme()) && httpRequest.getServerPort() == 80 || "https".equals(httpRequest.getScheme()) && httpRequest.getServerPort() == 443 ? "" : ":" + httpRequest.getServerPort() ) +
						httpRequest.getRequestURI() +
						(httpRequest.getQueryString() != null ? "?" + httpRequest.getQueryString() : "");

		jsessionID = getJSessionID(httpRequest);
		
		loginSession = hasLoginSession(session, section);
		
		sessionID = (session!=null)? session.getId() : jsessionID;
		
		contextPath = httpRequest.getContextPath();
		
		Log.info("{" + section +"} ...................Session ID=" + sessionID + "; URI=" + uri);
		
//		wrappedResponse = new CharResponseWrapper((HttpServletResponse)response);
		wrappedResponse = (HttpServletResponse) response;
		
		scanFlag = needToScan(httpRequest, section);
		Log.info("{" + section +"} scanFlag is " + scanFlag);
		
		// Reject all request if system does not startup correctly.
		if (!Constants.APP_STARTUP_COMPLETE) {
			scanFlag = false;
			hasError = true;
			session.setAttribute("errorMessage", "SYS_ERROR_STARTUP");
			session.setAttribute("errorStatusCode", HttpStatus.SERVICE_UNAVAILABLE);	//503
			httpResponse.sendRedirect(contextPath + "/Error");
			Log.info("{" + section +"} Security Issue (Startup incomplete) ....... Please check which App Startup process is failure.");
		}
		
		if (scanFlag) {
			/*=================== Incoming Request (START) ===================*/
			section = "Request Filter";
			
			auditRecord(httpRequest, session, section, jsessionID);
			
//			listHttpHeader(httpRequest, section);
//			listQueryString(httpRequest, section);
//			listParameter(httpRequest, section);
			
			UserPrincipalBean p = Function.getPrincipal(httpRequest);
			LoginExclusion exclude = new LoginExclusion(p);
			boolean canAccess = exclude.canAccess(httpRequest, Function.getPathUrl(httpRequest));
			
			//Validate Login Session
			if (!hasError && !loginSession) {
				if (!canAccess) {
					hasError = true;
					httpResponse.sendRedirect(contextPath + "/Logout?err=session_expired");
					Log.info("{" + section +"} Session Expiry or Missing ....... User will be redirected to logout.");
				}
			}
			
			//Validate Servlet Path
			if (!hasError) {
//				UserPrincipalBean p = Function.getPrincipal(httpRequest);
//				LoginExclusion exclude = new LoginExclusion(p);
//				flagServlet = (exclude.canAccess(httpRequest, Function.getPathUrl(httpRequest)))?false:true;
				flagServlet = (canAccess)?false:true;
				if (flagServlet) {
					hasError = true;
					session.setAttribute("errorMessage", "SYS_INVALID_ACCESS");
					session.setAttribute("errorStatusCode", 10403);	//403
					httpResponse.sendRedirect(contextPath + "/Error");
					Log.info("{" + section +"} Security Issue (Not Allow Path Access) ....... User will be redirected to error page.");
				}
			}
			
			//Validate HTTP Header
			if (!hasError) {
				flagCRLF = scanHttpHeaderInjection(httpRequest, httpResponse, section);
				if (flagCRLF) {
					hasError = true;
					session.setAttribute("errorMessage", "VULNERABILITY_HTTP_HEADER");
					session.setAttribute("errorStatusCode", 10403);	//403
					httpResponse.sendRedirect(contextPath + "/Error");
					Log.info("{" + section +"} Security Issue (Header Injection) ....... User will be redirected to error page.");
				}
			}
			
			//Validate CSRF Token from Request
			if (!hasError) {
				if (CsrfExclusion.needTokenCheck(Function.getPathUrl(httpRequest))) {
					flagCSRF = (validCsrfToken(httpRequest, session, section))?false:true;
					if (flagCSRF) {
						hasError = true;
						session.setAttribute("errorMessage", "VULNERABILITY_CSRF_TOKEN");
						session.setAttribute("errorStatusCode", 10403);	//403
						httpResponse.sendRedirect(contextPath + "/Error");
						Log.info("{" + section +"} Security Issue (CSRF Token) ....... User will be redirected to error page.");
					}
				}
			}
			
			//Validate Special Character
			if (!hasError) {
				flagSpecialChar = scanSpecialChar(httpRequest, section);
				if (flagSpecialChar) {
					hasError = true;
					session.setAttribute("errorMessage", "VULNERABILITY_SPECIAL_CHARACTER");
					session.setAttribute("errorStatusCode", 10403);	//403
					httpResponse.sendRedirect(contextPath + "/Error");
					Log.info("{" + section +"} Security Issue (Special Char) ....... User will be redirected to error page.");
				}
			}
			
			//Attribute
			if (!hasError) {
				scanAttribute(httpRequest, section);
			}
			
			/*=================== Incoming Request (END) ===================*/
		}
		
		if (hasError) {
			return;		// Exit	
		} else {
			if (scanFlag) {
				chain.doFilter(request, wrappedResponse);
			} else {
				chain.doFilter(request, response);
			}
		}
		
		if (scanFlag) {
			/*=================== Outgoing Response (START) ===================*/
			section = "Response Filter";
			
//			byte[] bytes = wrappedResponse.getByteArray();
	
			//Set No Cache to all HTML output
			setNoCache(section);
			
			//Disallow Frame
			setXFrameOption(section);

//			response.getOutputStream().write(bytes);
//			Log.info("{" + section +"} '" + Function.getPathUrl(httpRequest) + "' size is " + bytes.length + " bytes.");
			/*=================== Outgoing Response (END) ===================*/
		}
		
		Log.info("{" + section +"} ...................END");
	}
	
	private boolean needToScan(HttpServletRequest httpRequest, String section) {
		boolean result = true;
		String extension = null;
		String path = null;
		
		//Check Extension
		try {
			path = Function.getPathUrl(httpRequest);
			
			int posExt = httpRequest.getRequestURI().lastIndexOf(".");
			Log.debug("{" + section +"} URI=" + httpRequest.getRequestURI());
			Log.debug("{" + section +"} posExt=" + posExt);
			
			if (posExt >= 0)
				extension = httpRequest.getRequestURI().substring(posExt + 1);
			
			// URL Path
			Log.debug("{" + section +"} URL Path: '" + path + "' under " + Constants.SECURITY_SCAN_EXT_PATH_WEB);
			if (result && path.indexOf(Constants.SECURITY_SCAN_EXT_PATH_WEB) >= 0) {
				result = false;
				Log.debug("{" + section +"} No Need Scan (Path).");
			}
			Log.debug("{" + section +"} URL Path: '" + path + "' under " + Constants.SECURITY_SCAN_EXT_PATH_EASE);
			if (result && path.indexOf(Constants.SECURITY_SCAN_EXT_PATH_EASE) >= 0) {
				result = false;
				Log.debug("{" + section +"} No Need Scan (Path).");
			}
			
			// Extension
			Log.debug("{" + section +"} URI Extension: " + extension);
			if (result && Constants.SECURITY_SCAN_EXT_EXCLUSION.contains(extension)) {
				result = false;
				Log.debug("{" + section +"} No Need Scan (Extension).");
			}
		} catch(NullPointerException e) {
			Log.debug(e.getMessage());
		} catch(Exception e) {
			Log.error(e);
		}
		
		return result;
	}
	
	/***
	 * Prevent Webpage is cached by browser.
	 */
	private void setNoCache(String section) {
		try {
			wrappedResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");		// HTTP 1.1
			wrappedResponse.setHeader("Pragma", "no-cache");		// HTTP 1.0
			wrappedResponse.setDateHeader("Expires", 0);			// Proxies
			Log.debug("{" + section +"} Set No Cache.");
		} catch(Exception e) {
			Log.error(e);
		}
	}
	
	/***
	 * Prevent "Clickjacking" attack. IE 8 or above.
	 * Options:
	 * DENY				The page cannot be displayed in a frame, regardless of the site attempting to do so.
	 * SAMEORIGIN		The page can only be displayed in a frame on the same origin as the page itself.
	 * ALLOW-FROM uri	The page can only be displayed in a frame on the specified origin.
	 */
	private void setXFrameOption(String section) {
		String option = Constants.XFRAME_OPTION_DEFAULT;
		try {
			wrappedResponse.setHeader("X-Frame-Options", option);
			Log.debug("{" + section +"} Set X-Frame-Options is " + option + ".");
		} catch(Exception e) {
			Log.error(e);
		}
	}
	
	private String[] getCsrfTokenFromParam(HttpServletRequest httpRequest, String section) {
		String[] output = new String[2];
		String key = null;
		String keypart = null;
    	
		Enumeration<String> params = null;
		
		try {
			params = httpRequest.getParameterNames();
			while (params.hasMoreElements()) {
				key = params.nextElement();
				String[] values = httpRequest.getParameterValues(key);
				keypart = (key != null && key.length() > Constants.TOKEN_KEY_PREFIX.length())? (key.substring(0, Constants.TOKEN_KEY_PREFIX.length())):null;
				
				Log.debug("{" + section +"} HTTP Param: " + key + " ");
				
				if (Constants.TOKEN_KEY_PREFIX.equals( keypart )) {
					output[0] = key;
					output[1] = values[0];
    				Log.debug("{" + section +"} output[0]: " + output[0] + ", output[1]: " + output[1]);
					break;
    			}
    		}
			
			if (output[0] == null)
				output = null;
    	} catch(Exception e) {
    		Log.error(e);
    	}
		
		return output;
	}
	
	private boolean validCsrfToken(HttpServletRequest httpRequest, HttpSession session, String section) {
		boolean result = false;
		String[] token = getCsrfTokenFromParam(httpRequest, section);
		String key = null;
		
		if (token != null) {
			String tokenID = token[0];
			String tokenValue = token[1];
			
			Log.info("{" + section +"} Validate CSRF Token ....... START");
			Log.debug("{" + section +"} Token ID from HTTP Parameter is " + tokenID);
			
			try {
				if (tokenID != null) {
					Log.info("{" + section +"} Input Token: [" + tokenID + "] --> " + tokenValue);
					
					key = Token.withdrawToken(session, tokenID);	// Withdraw Token Key from Session.
					
					Log.info("{" + section +"} Input Token: [" + tokenValue + "] <---> Session Token: [" + key + "]");
					
					if (tokenValue != null && key != null) {		// Compare Token Key between Session and HTTP Parameter.
						if (tokenValue.equals(key)) {
							result = true;
						}
					}
				}
			} catch(IllegalStateException e) {
				Log.error(e.getMessage());
			} catch(Exception e) {
				Log.error(e);
			}
			Log.info("{" + section +"} CSRF Token is " + (result?"valid":"invalid") + ".");
			Log.info("{" + section +"} Validate CSRF Token ....... END");
		} else {
			Log.info("{" + section +"} Validate CSRF Token ....... Session is null.");
		}
		
		return result;
	}
	
	private void listHttpHeader(HttpServletRequest httpRequest, String section) {
		Enumeration names = null;
    	
    	try {
    		names = httpRequest.getHeaderNames();
    		Log.info("{" + section +"} [!] HTTP Header" + Constants.MESSAGE_DONT_SHOW_DEBUG);
			while (names.hasMoreElements()) {
				String name = (String) names.nextElement();
				Log.info("{" + section +"} HTTP Header: " + name + " = " + httpRequest.getHeader(name));
			}
    	} catch(Exception e) {
    		Log.error(e);
    	}
		
	}
	
	private void listParameter(HttpServletRequest httpRequest, String section) {
		Enumeration<String> params = null;
		
		try {
			params = httpRequest.getParameterNames();
			Log.info("{" + section +"} [!] HTTP Parameter" + Constants.MESSAGE_DONT_SHOW_DEBUG);
			while (params.hasMoreElements()) {
				String name = params.nextElement();
				String[] values = httpRequest.getParameterValues(name);
				for (int i=0; i<values.length; i++) {
					Log.info("{" + section +"} HTTP Parameter: " + name + "[" + i + "] = " + values[i]);
				}
				values = null;
			}
		} catch(Exception e) {
    		Log.error(e);
    	}
	}
	
	private void listQueryString(HttpServletRequest httpRequest, String section) {
		String params = httpRequest.getQueryString();
		
		try {
			Log.info("{" + section +"} [!] HTTP Query String" + Constants.MESSAGE_DONT_SHOW_DEBUG);
			if (params != null && params.length() > 0) {
				String[] param = params.split("&");
				for (String pair : param) {
					int idx = pair.indexOf("=");
					String name = (idx > 0)? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
					String value = (idx > 0 && pair.length() > idx + 1)? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
					Log.info("{" + section +"} HTTP Query String: " + name + " = " + value);
				}
				param = null;
			}
		} catch(Exception e) {
    		Log.error(e);
    	}
	}
	
	private String getJSessionID(HttpServletRequest httpRequest) {
		String id = httpRequest.getHeader("Cookie");
		String output = null;
		String target = "JSESSIONID=";
		
		try {
			if (id != null) {
				int idx1 = id.indexOf(target);
				int idx2 = id.indexOf(";", idx1);
				if (idx2 > 0) {
					output = id.substring(idx1 + target.length(), idx2);
				} else {
					output = id.substring(idx1 + target.length());
				}
			}
		} catch(Exception e) {
    		Log.error(e);
    	}
		
		return output;
	}
	
	private void scanAttribute(HttpServletRequest httpRequest, String section) {
		Enumeration attributes = null;
		
		try {
			attributes = httpRequest.getAttributeNames();
			Log.info("{" + section +"} [!] Attributes" + Constants.MESSAGE_DONT_SHOW_DEBUG);
			while (attributes.hasMoreElements()) {
				String name = (String) attributes.nextElement();
				Object obj = httpRequest.getAttribute(name);
				Log.info("{" + section +"} Attribute: " + name + " = " + obj);
			}
		} catch(Exception e) {
    		Log.error(e);
    	}
	}
	
	private boolean scanSpecialChar(HttpServletRequest httpRequest, String section) {
		boolean result = false;
		
		///TODO: Exclusion List
		
		if (Constants.SECURITY_SCAN_SPECIAL_CHARS != null && Constants.SECURITY_SCAN_SPECIAL_CHARS.size() > 0) {
		
			// Query String scan
			if (!result) {
				String params = httpRequest.getQueryString();
				try {
					if (params != null && params.length() > 0) {
						String[] param = params.split("&");
						for (String pair : param) {
							int idx = pair.indexOf("=");
							String name = (idx > 0)? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
							String value = (idx > 0 && pair.length() > idx + 1)? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
							// Check any special character(s) inside param value.  param name does not check.
							if (value != null) {
								for (String spchar : Constants.SECURITY_SCAN_SPECIAL_CHARS) {
									if (value.indexOf(spchar) >= 0) {
										// Found
										result = true;
										Log.info("{" + section +"} Special Character is found in Query String. \n" + name + " = " + value + "");
										break;
									}
								}
								
								if (result) {
									break;
								}
							}
						}
						param = null;
					}
				} catch(Exception e) {
		    		Log.error(e);
		    	}
			}
			
			// POST Parameter scan
			if (!result) {
				Enumeration<String> params = null;
				
				try {
					params = httpRequest.getParameterNames();
					Log.info("{" + section +"} [!] HTTP Parameter" + Constants.MESSAGE_DONT_SHOW_DEBUG);
					while (params.hasMoreElements()) {
						String name = params.nextElement();
						String[] values = httpRequest.getParameterValues(name);
						for (int i=0; i<values.length; i++) {
							// Check any special character(s) inside param value.  param name does not check.
							if (values[i] != null) {
								for (String spchar : Constants.SECURITY_SCAN_SPECIAL_CHARS) {
									if (values[i].indexOf(spchar) >= 0) {
										// Found
										result = true;
										Log.info("{" + section +"} Special Character is found in POST Parameter. \n" + name + " = " + values[i] + "");
										break;
									}
								}
								
								if (result) {
									break;
								}
							}
						}
						values = null;
					}
				} catch(Exception e) {
		    		Log.error(e);
		    	}
				
				params = null;
			}
		
		}
		return result;
	}
	
	private boolean scanHttpHeaderInjection(HttpServletRequest httpRequest, HttpServletResponse httpResponse, String section) {
		boolean result = false;
		Enumeration names = null;
    	
    	try {
    		names = httpRequest.getHeaderNames();
			while (names.hasMoreElements()) {
				String name = (String) names.nextElement();
				String value = httpRequest.getHeader(name);
				
				// Detect CR and LF
				if (value.indexOf("\r") >= 0 || value.indexOf("\n") >= 0) {
					Log.info("{" + section +"} HTTP Header Injection: CRLF is detected. " + name + " = " + value);
					httpResponse.setHeader(name, value.replace("\r", ""));
					httpResponse.setHeader(name, value.replace("\n", ""));
				}
			}
    	} catch(Exception e) {
    		Log.error(e);
    	}
		
		return result;
	}
	
	private void auditRecord(HttpServletRequest httpRequest, HttpSession session, String section, String jsessionID) {
		AudAccessLog audlog = new AudAccessLog();
		Function2 func2 = new Function2();
		
		String userCode = null;
		String sessionID = null;
		String uriPath = null;
		String funcCode = null;
		String keyValue = null;
		boolean validAccess = false;
		String ipServAddr = null;
		String ipUserAddr = null;
		Object principal = null;

		try {
			principal = session.getAttribute(Constants.USER_PRINCIPAL);
			
			if (principal != null) {
				userCode = ((UserPrincipalBean) principal).getUserCode();
			}
			
			if (userCode != null) {
				sessionID = (session!=null)? session.getId() : jsessionID;
				uriPath = Function.getPathUrl(httpRequest);
				funcCode = func2.getFuncCode(uriPath);
				ipServAddr = Function.getServerIp(httpRequest);
				keyValue = null;			///TODO: get specify http parameter
				validAccess = true;			///TODO: get access of Servlet Path requiring Login Session
				ipServAddr = Function.getServerIp(httpRequest);
				ipUserAddr = Function.getClientIp(httpRequest);
				
				audlog.add(userCode, sessionID, uriPath, funcCode, keyValue, validAccess, ipServAddr, ipUserAddr);
			}
		} catch(NullPointerException e) {
			Log.debug("{" + section +"} No user principal");
		} catch(Exception e) {
			Log.error(e);
		} finally {
			audlog = null;
			func2 = null;
		}
	}
	
	private boolean hasLoginSession(HttpSession session, String section) {
		boolean output = false;
		Object principal = null;
		Function2 func2 = null;
		
		try {
			if (session != null) {
				principal = session.getAttribute(Constants.USER_PRINCIPAL);
				if (principal != null) {
					String userCode = ((UserPrincipalBean) principal).getUserCode();
					boolean isComplete = ((UserPrincipalBean) principal).getWorkflow().isLoginReady();
					if (userCode != null && userCode.length() > 0) {	// Has User Code in Session.
						func2 = new Function2();
						if (func2.isSessionLogin(session.getId())) {		// Session ID is not yet logout.
							output = true;
						} else {	// remove User Principal because Logout time is filled.
							session.removeAttribute(Constants.USER_PRINCIPAL);
						}
					}
				}
			}
		} catch(Exception e) {
			Log.error(e);
		} finally {
			principal = null;
			func2 = null;
		}
		if (output)
			Log.debug("{" + section +"} hasLoginSession: Has Login Session");
		else
			Log.debug("{" + section +"} hasLoginSession: Without Login Session");
		
		return output;
	}
}
