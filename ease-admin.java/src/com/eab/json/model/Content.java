package com.eab.json.model;

import java.util.Map;

import com.google.gson.JsonObject;

public class Content {
	private Template template;
	private JsonObject langMap; 
	private Object values;
	private Object changedValues;
	private Map<String, Object> dialog;
	
	public Content(Object values) {
		super();
		this.values = values;
	}

	public Content(Template template, Object values) {
		super();
		this.template = template;
		this.values = values;
	}

	public Content(Template template, Object values, Object changedValues) {
		super();
		this.template = template;
		this.values = values;
		this.changedValues = changedValues;
	}

	public Template getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}

	public Object getValues() {
		return values;
	}

	public void setValues(Object values) {
		this.values = values;
	}

	public Object getChangedValues() {
		return changedValues;
	}

	public void setChangedValues(Object changedValues) {
		this.changedValues = changedValues;
	}

	public JsonObject getLangMap() {
		return langMap;
	}

	public void setLangMap(JsonObject langMap) {
		this.langMap = langMap;
	}
	
	public Map<String, Object> getDialog() {
		return dialog;
	}

	public void setDialog(Map<String, Object> dialog) {
		this.dialog = dialog;
	}

}
