package com.eab.json.model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.eab.common.Function2;

public class Dialog extends Field {
	private String message;
	private Field positive;
	private Field negative;
	private boolean inhertValue;
	private int width;
	private int defaultWidth = 30;

	public Dialog(String id, String title, String message, Field positive, Field negative) {
		this.type = "dialog";
		this.id = id;
		this.title = title;
		this.message = message;
		this.positive = positive;
		this.negative = negative;
		this.width = defaultWidth;
	}
	
	public Dialog(String id, String title, String message, Field positive, Field negative, int width) {
		this.type = "dialog";
		this.id = id;
		this.title = title;
		this.message = message;
		this.positive = positive;
		this.negative = negative;
		this.width = width;
	}

	public Dialog(String id, String title, String message, Field positive, Field negative, boolean inhertValue, int width) {
		this(id, title, message, positive, negative, width);
		this.inhertValue = inhertValue;
	}
	
	public Dialog(String id, String title, String message, Field positive, Field negative, List<Field> items, int width) {
		this(id, title, message, positive, negative, width);
		this.items = items;
	}
	
	public Dialog(String id, String title, String message, Field positive, Field negative, boolean inhertValue, List<Field> items, int width) {
		this(id, title, message, positive, negative, inhertValue, width);
		this.items = items;
	}
	
	public void translate(Map<String, String> map) {
		super.translate(map);
		
		if (this.message != null && !this.message.isEmpty()) {
			String message = map.get(this.message);
			if (message != null && !message.isEmpty()) {
				this.message = message;
			}
		}
		
		if (this.positive != null ) {
			this.positive.translate(map);
		}

		if (this.negative != null ) {
			this.negative.translate(map);
		}
	}
	
	@Override
	public void getNameCodes(List<String> list) {
		super.getNameCodes(list);
		if (this.message != null && !this.message.isEmpty()) {
			list.add(this.message);
		}
		if (this.positive != null ) {
			this.positive.getNameCodes(list);
		}

		if (this.negative != null ) {
			this.negative.getNameCodes(list);
		}
	}
	
	public void translate(HttpServletRequest request){
		List<String> names = new ArrayList<String>();
		getNameCodes(names);
		Map<String, String> lmap;
		try {
			lmap = Function2.getTranslateMap(request, names);
			translate(lmap);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
