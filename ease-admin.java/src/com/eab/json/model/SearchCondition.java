package com.eab.json.model;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.common.Constants;
import com.google.gson.Gson;

public class SearchCondition {

	private String criteria;
	private String typeFilter;
	private String statusFilter;
	private String sortBy;
	private String sortDir;
	private boolean sorting;
	private int recordStart;
	private int pageSize;
	private boolean useDefault; // indicated using default condition
	
	public SearchCondition() {
		useDefault = true;
		criteria = null;
		typeFilter = null;
		statusFilter = null;
		sortBy = null;
		sortDir = "A";
		recordStart = 0;
		pageSize = Constants.SEARCH_RESULT_DEFAULT_PAGE_SIZE;
	}

	public static SearchCondition RetrieveSearchCondition(HttpServletRequest request) {
		String p0 = request.getParameter("p0");
		if (p0 != null && !p0.isEmpty()) {
			try {
				p0 = new String(Base64.getDecoder().decode(p0.replace(" ", "+")), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Gson gson = new Gson();
			SearchCondition cond = gson.fromJson(p0, SearchCondition.class);
			cond.useDefault = false;
			return cond;
		} else {
			return new SearchCondition();
		}
	}

	public String getCriteria() {
		return criteria;
	}

	public String getTypeFilter() {
		return typeFilter;
	}

	public String getStatusFilter() {
		return statusFilter == null || statusFilter.isEmpty()?"E":statusFilter;
	}

	public String getSortBy() {
		return sortBy;
	}

	public String getSortDir() {
		return sortDir == null || sortDir.isEmpty()? "A": sortDir;
	}

	public int getRecordStart() {
		return recordStart;
	}

	public int getPageSize() {
		return pageSize == 0?Constants.SEARCH_RESULT_DEFAULT_PAGE_SIZE:pageSize;
	}

	public String getSearchKey() {
		return this.criteria + "_" + statusFilter + "_" + typeFilter + "_" + sortBy + "_" + sortDir;
	}

	public boolean isSorting() {
		return sorting;
	}

	public void setSorting(boolean sorting) {
		this.sorting = sorting;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public void setSortDir(String sortDir) {
		this.sortDir = sortDir;
	}

	public boolean isUseDefault() {
		return useDefault;
	}

	public JSONObject toJSON() {
		JSONObject json = new JSONObject();

		json.put("criteria", this.criteria);
		json.put("typeFilter", this.typeFilter);
		json.put("statusFilter", this.statusFilter);
		json.put("sortBy", this.sortBy);
		json.put("sortDir", this.sortDir);

		return json;

	}

}
