package com.eab.json.model;

public class BiListCondition extends SearchCondition {
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
