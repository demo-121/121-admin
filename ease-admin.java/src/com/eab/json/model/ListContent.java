package com.eab.json.model;
import java.util.List;

public class ListContent {
	private Object list;
	private int total;
	private boolean isMore;
	private List<Object> id;
	private String backPageKey;
	
	public ListContent(Object list, int total) {
		super();
		this.list = list;
		this.total = total;
	}
	
	public ListContent(Object list, int total, boolean isMore) {
		super();
		this.list = list;
		this.total = total;
		this.isMore = isMore;
	}
	
	public ListContent(Object list, int total, boolean isMore, List<Object> rows) {
		super();
		this.list = list;
		this.total = total;
		this.isMore = isMore;
		this.id = rows;
	}
	
	public ListContent(Object list, int total, boolean isMore, List<Object> rows, String backPageKey) {
		super();
		this.list = list;
		this.total = total;
		this.isMore = isMore;
		this.id = rows;
		this.backPageKey = backPageKey;
	}

	public Object getList() {
		return list;
	}

	public void setList(Object list) {
		this.list = list;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}	
		
	public boolean getIsMore() {
		return isMore;
	}

	public void setIsMore(boolean isMore) {
		this.isMore = isMore;
	}
	
	public List<Object> getId() {
		return id;
	}

	public void setId(List<Object> id) {
		this.id = id;
	}
	
	public String getBackPageKey() {
		return backPageKey;
	}

	public void setBackPageKey(String backPageKey) {
		this.backPageKey = backPageKey;
	}
}
