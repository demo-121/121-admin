package com.eab.json.model;

import java.util.List;

public class DialogItem extends Field {
	public class Types {
		public final static String LABEL = "label";
		public final static String TEXT = "text";
		public final static String DATEPICKER = "datePicker";
		public final static String CHECKBOX = "checkbox";
		public final static String RADIOBUTTON = "radioButton";
		public final static String TIMEPICKER = "timePicker";
	}

	private boolean multiLine;
	private int h;
	private int indent;
	
	// Label
	public DialogItem(String id, String type, String title, int h, int indent) {
		this.id = id;
		this.type = type;
		this.title = title;
		this.h = h;
		this.indent = indent;
	}

	// Checkbox and RadioButton group 
	public DialogItem(String id, String type, String value, Boolean mandatory, List<Option> options) {
		this.id = id;
		this.type = type;
		this.value = value;
		this.mandatory = mandatory;
		this.options = options;
	}

	// TextField
	public DialogItem(String id, String type, String value, Boolean mandatory, String hint, String floatText, int maxLength) {
		this.id = id;
		this.type = type;
		this.value = value;
		this.mandatory = mandatory;
		this.placeholder = hint;
		this.title = floatText;
		this.max = maxLength;
	}

	// DatePicker
	public DialogItem(String id, String type, long value, Boolean mandatory, String hint, String floatText, String presentation) {
		this.id = id;
		this.type = type;
		this.value = value;
		this.mandatory = mandatory;
		this.placeholder = hint;
		this.title = floatText;
		this.presentation = presentation;
	}

	public DialogItem(String id, String type, String value, Boolean mandatory, String hint, String floatText, boolean multiLine, int maxLength) {
		this(id, type, value, mandatory, hint, floatText, maxLength);
		this.multiLine = multiLine;
	}

	public boolean isMultiLine() {
		return multiLine;
	}

	public void setMultiLine(boolean multiLine) {
		this.multiLine = multiLine;
	}

}
