package com.eab.json.model;

import java.util.Map;

import com.google.gson.JsonObject;

public class AssignContent extends Content{
	public class SelectTypes {
		public static final String SELECTABLE = "SELECTABLE";
		public static final String MULTI_SELECTABLE = "MULTI_SELECTABLE";
		public static final String DISABLED = "DISABLED";
	}
	private String selectType;

	public AssignContent(Template template, Object values, String selectType){
		super(template, values);
		this.selectType = selectType;
	}

}
