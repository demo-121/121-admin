package com.eab.json.model;

import java.io.Serializable;

public class ConfigPanel implements Serializable {
	
	Field topBar;
	Field leftPanel;
	Field rightPanel;
	
	public ConfigPanel(Field topBar, Field leftPanel, Field rightPanel){
		this.topBar = topBar;
		this.leftPanel = leftPanel;
		this.rightPanel = rightPanel;
	}

}
