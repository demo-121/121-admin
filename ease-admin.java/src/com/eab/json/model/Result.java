package com.eab.json.model;

public class Result {
	boolean success;
	String errType;
	String msg;
	
	public void setSuccess(boolean success){
		this.success = success;
	}
	public boolean getSuccess(){
		return success;
	}
	
	public void setErrType(String errType){
		this.errType = errType;
	}
	public String getErrType(){
		return errType;
	}
	
	public void setMsg(String msg){
		this.msg = msg;
	}
	public String getMsg(){
		return msg;
	}
	
}
