package com.eab.json.model;

import java.util.List;
import java.util.Map;

public class AppBarTitle extends Field {
	String primary;
	Object secondary;
	Object tertiary;
	
	public class Types{
		public static final String LABEL = "label";
		public static final String PICKER = "picker";
		public static final String LEVEL_TWO = "levelTwo";
		public static final String LEVEL_THREE = "levelThree";
	}

	public AppBarTitle(String id, String type, String value, String primary, List<Option> options, Object secondary) {
		super();
		this.id = id;
		this.type = type;
		this.value = value;
		this.primary = primary;
		this.options = options;
		this.secondary = secondary;
	}
	public AppBarTitle(String type, String value, List<Option> options, Object secondary) {
		super();
		this.type = type;
		this.value = value;
		this.options = options;
		this.secondary = secondary;
	}
	
	public AppBarTitle(String type, String value, List<Option> options, Object secondary, Object tertiary) {
		super();
		this.type = type;
		this.value = value;
		this.options = options;
		this.secondary = secondary;
		this.tertiary = tertiary;
	}
	
	public void setBackPage(String id, String primary){
		this.id = id;
		this.primary = primary;
	}
	
	
//	public AppBarTitle(String id, String type, String value, String primary, Object secondary, List<Option> options) { 
//		this(id, type, value, primary, options, null);
//		this.secondary = secondary;
//	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPrimary() {
		return primary;
	}

	public void setPrimary(String primary) {
		this.primary = primary;
	}

	public void setOptions(List<Option> options) {
		this.options = options;
	}

	public Object getSecondary() {
		return secondary;
	}

	public void setSecondary(Object secondary) {
		this.secondary = secondary;
	}

	public void translate(Map<String, String> map) {
		
		if (this.primary != null && !this.primary.isEmpty()) {
			String tit = map.get(this.primary);
			if (tit != null && !tit.isEmpty()) {
				this.primary = tit;
			}
		}

		if (this.secondary != null){
			if (secondary instanceof Field) {
				((Field)secondary).translate(map);
			} else {
				String tit = map.get(this.secondary);
				if (tit != null && !tit.isEmpty()) {
					this.secondary = tit;
				}
			}
		}

		if (this.options != null) {
			for (Option o : options) {
				String ot = map.get(o.getTitle());
				if (ot != null && !ot.isEmpty()) {
					o.setTitle(ot);
				}
			}
		}
	}

	public void getNameCodes(List<String> list) {
		if (this.primary != null && !this.primary.isEmpty()) {
			list.add(primary);
		}

		if (this.secondary != null){
			if (secondary instanceof Field) {
				((Field)secondary).getNameCodes(list);
			} else {
				list.add((String)secondary);
			}
		}

		if (this.options != null) {
			for (Option o : options) {
				list.add(o.getTitle());
			}
		}
	}
	
}
