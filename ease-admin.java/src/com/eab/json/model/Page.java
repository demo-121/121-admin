package com.eab.json.model;

import com.google.gson.JsonObject;

public class Page {
	private String id;
	private String title;
	private String module;
	private JsonObject value;

	/**
	 * @param id
	 * @param title
	 */
	public Page(String id, String title) {
		super();
		this.id = id;
		this.title = title;
	}

	/**
	 * @param id
	 * @param title
	 * @param value
	 */
	public Page(String id, String title, JsonObject value) {
		super();
		this.id = id;
		this.title = title;
		this.value = value;
	}

	/**
	 * @param id
	 * @param title
	 * @param module
	 */
	public Page(String id, String title, String module) {
		super();
		this.id = id;
		this.title = title;
		this.module = module;
	}

	/**
	 * @param id
	 * @param title
	 * @param module
	 * @param value
	 */
	public Page(String id, String title, String module, JsonObject value) {
		super();
		this.id = id;
		this.title = title;
		this.module = module;
		this.value = value;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public JsonObject getValue() {
		return value;
	}

	public void setValue(JsonObject value) {
		this.value = value;
	}

}
