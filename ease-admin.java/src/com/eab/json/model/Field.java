package com.eab.json.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.eab.common.Log;
import com.eab.json.model.ConfigPanel;

public class Field {
	public class Types {
		public final static String ACTION 			= "action";
		public final static String ACTION_ASSIGN 	= "assign";
		public final static String SECTION 			= "section";
		public final static String TEXT 			= "text";
		public final static String NUMBER 			= "number";
		public final static String PASSWORD 		= "password";
		public final static String EMAIL 			= "email";
		public final static String PHONENUMBER 		= "phoneNumber";
		public final static String PICKER 			= "picker";
		public final static String DATEPICKER 		= "datePicker";
		public final static String CHECKBOX 		= "checkbox";
		public final static String CHECKBOXGROUP 	= "Checkboxgroup";
		public final static String RADIOGROUP 		= "radioGroup";
		public final static String COMPLEX 			= "complex";
		public final static String COMPLEXLIST 		= "complexList";
		public final static String VERSIONPICKER 	= "versionPicker";
		public final static String READONLY 		= "readOnly";
		public final static String MULTTEXT 		= "multText";
		public final static String HBOX 			= "hBox";
		public final static String SEARCHFIELD 		= "searchField";
		public final static String LIST 			= "list";
		public final static String SELECTLIST 		= "selectList";
		public final static String JSBOX 			= "jsBox";
		public final static String IMAGE 			= "image";
		public final static String GRID 			= "grid";
		public final static String TAB 				= "tab";
		public final static String TABS 			= "tabs";
		public final static String PDF 				= "pdf";
		public final static String ROWKEY 			= "rowKey";
	}

	public class PresentationTypes {
		public final static String COLUMN2 = "column2";
		public final static String COLUMN3 = "column3";
		public final static String COLUMN4 = "column4";
	}

	// HandsonTable Type
	public class GridColumnTypes {
		public final static String TEXT = "text";
		public final static String NUMERIC = "numeric";
		public final static String CHECKBOX = "checkbox";
		public final static String DROPDOWN = "dropdown";
		public final static String IMAGE = "image";
	}

	// for field id, map to value, action path
	protected String 	id;
	// for floating label, page title, section title, group title
	protected String 	title;
	protected String 	tooltipId;
	// for complex list/details list dialog header or table default row name 
	protected String 	hints;
	// table no record label
	protected String 	placeholder;
	protected String 	icon;

	protected String 	type;
	protected boolean 	isKey;

	// for layout
	protected Integer 	listSeq;
	protected Integer 	detailSeq;
	protected String 	colWidth;
	// for value formating
	protected Object 	presentation;

	protected String 	status;
	
	protected String	errorStr;

	protected Object 	value;
	// for validation, for number picker, date picker
	protected Object 	max, min, interval;

	// options for picker
	protected List<Option> options;
	// "ALL" option for picker
	protected boolean 	setAll;
	
	protected String 	validateFunc;
	// create dialog
	protected Dialog 	dialog;

	// sub items
	protected List<Field> items;

	protected boolean 	mandatory;

	protected String 	subType;

	protected String 	reference;

	// for readonly and disabled field
	protected boolean 	disabled;
	protected boolean 	allowUpdate = true;
	protected boolean 	allowAdd = true;
	
	protected boolean 	forConfigOnly;

	// visible condition
	// value trigger
	protected List<Trigger> trigger;

	// Grid Column
	protected boolean 	auto;
	
	// For Config Panel
	protected ConfigPanel configPanel;

	public Field() {
	}

	public Field(String id) {
		this.id = id;
	}

	// for app bar action
	public Field(String id, String type, String title) {
		this.id 		= id;
		this.type 		= type;
		this.title 		= title;
	}

	// Constructor for all
	public Field(String id, String title, String type, Integer listSeq, 
			Integer detailSeq, String colWidth) {
		this.id 		= id;
		this.title 		= title;
		this.type 		= type;
		this.listSeq 	= listSeq;
		this.detailSeq 	= detailSeq;
		this.colWidth 	= colWidth;
	}

	public Field(String id, String title, String type, Integer listSeq, 
			Integer detailSeq, String colWidth, String tooltipId) {
		super();
		this.id 		= id;
		this.title 		= title;
		this.type 		= type;
		this.listSeq 	= listSeq;
		this.detailSeq 	= detailSeq;
		this.colWidth 	= colWidth;
		this.tooltipId 	= tooltipId;
	}

	// Constructor for appbar - secondary
	public Field(String id, String type, String value, 
			List<Option> options, String title) {
		super();
		this.id 		= id;
		this.type 		= type;
		this.value 		= value;
		this.options 	= options;
		this.title 		= title;
		this.mandatory 	= true;
	}

	// Constructor for section
	public Field(String id, String type, String title, 
			boolean disabled, List<Field> items) {
		this.id 		= id;
		this.type 		= type;
		this.title 		= title;
		this.disabled 	= disabled;
		this.items 		= items;
	}

	// Constructor for item
	public Field(String id, String type, String subType, String title, 
			int detailSeq, String colWidth, String presentation, 
			boolean mandatory, boolean disabled, List<Option> options, 
			List<Field> items, Trigger trigger) {
		this.id 		= id;
		this.type 		= type;
		this.subType 	= subType;
		this.title 		= title;
		this.detailSeq 	= detailSeq;
		this.colWidth 	= colWidth;
		this.presentation = presentation;
		this.mandatory 	= mandatory;
		this.disabled 	= disabled;
		this.options 	= options;
		this.items 		= items;
		this.trigger 	= new ArrayList<Trigger>();
		this.trigger.add(trigger);
	}

	// Constructor for item
	public Field(String id, String type, String subType, 
			String title, int detailSeq, String colWidth, 
			String presentation, boolean mandatory, 
			boolean disabled, List<Option> options, 
			List<Field> items, Trigger trigger, Object max) {
		
		this(id, type, subType, title, detailSeq, colWidth, 
				presentation, mandatory, disabled, options, 
				items, trigger);
		this.max 		= max;
	}

	// Constructor for item
	public Field(String id, String type, String subType, 
			String title, int detailSeq, String colWidth, 
			String presentation, boolean mandatory, 
			boolean disabled, List<Option> options, 
			List<Field> items, Trigger trigger, 
			Object min, Object max) {
		
		this(id, type, subType, title, detailSeq, colWidth, 
				presentation, mandatory, disabled, options, 
				items, trigger);
		this.min 		= min;
		this.max 		= max;
	}

	// Constructor for multText
	public Field(String id, String title, boolean mandatory) {
		this.id 		= id;
		this.title 		= title;
		this.mandatory 	= mandatory;
	}

	// Constructor for appBarAction
	public Field(String id, String type, String value, String title, List<Option> options) {
		this.id 		= id;
		this.type 		= type;
		this.value 		= value;
		this.title 		= title;
		this.options 	= options;
	}

	// Constructor for appBar IconMenu
	public Field(String id, String type, List<Field> items) {
		this.id 		= id;
		this.type 		= type;
		this.items 		= items;
	}

	// Constructor for dialogButton
	public Field(String id, String type, String title, Dialog dialog) {
		this.id 		= id;
		this.type 		= type;
		this.title 		= title;
		this.dialog 	= dialog;
	}
	
	// Constructor for config panel
	public Field(String id, String type, String title, ConfigPanel configPanel) {
		this.id 		= id;
		this.type 		= type;
		this.title 		= title;
		this.configPanel = configPanel;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}
	
	public Integer getListSeq() {
		return listSeq;
	}

	public void setListSeq(Integer listSeq) {
		this.listSeq = listSeq;
	}

	public Integer getDetailSeq() {
		return detailSeq;
	}

	public void setDetailSeq(Integer detailSeq) {
		this.detailSeq = detailSeq;
	}

	public String getColWidth() {
		return colWidth;
	}

	public void setColWidth(String colWidth) {
		this.colWidth = colWidth;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
	public String getErrorStr() {
		return errorStr;
	}

	public void setErrorStr(String error_str) {
		this.errorStr = error_str;
	}


	public Object getMax() {
		return max;
	}

	public void setMax(Object max) {
		this.max = max;
	}

	public Object getMin() {
		return min;
	}

	public void setMin(Object min) {
		this.min = min;
	}

	public Object getInterval() {
		return interval;
	}

	public void setInterval(Object interval) {
		this.interval = interval;
	}

	public Object getPresentation() {
		return presentation;
	}

	public void setPresentation(Object presentation) {
		this.presentation = presentation;
	}

	public String getValidateFunc() {
		return validateFunc;
	}

	public void setValidateFunc(String validateFunc) {
		this.validateFunc = validateFunc;
	}

	public List<Field> getItems() {
		return items;
	}

	public void setItems(List<Field> items) {
		this.items = items;
	}

	public void addItem(Field field) {
		if (this.items == null) {
			this.items = new ArrayList<>();
		}
		this.items.add(field);
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isAllowUpdate() {
		return allowUpdate;
	}

	public void setAllowUpdate(boolean allowUpdate) {
		this.allowUpdate = allowUpdate;
	}

	public boolean isAllowAdd() {
		return allowAdd;
	}

	public void setAllowAdd(boolean allowAdd) {
		this.allowAdd = allowAdd;
	}

	public List<Option> getOptions() {
		return options;
	}

	public void setOptions(List<Option> options) {
		this.options = options;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Dialog getDialog() {
		return dialog;
	}

	public void setDialog(Dialog dialog) {
		this.dialog = dialog;
	}

	public String getTooltipId() {
		return tooltipId;
	}

	public void setTooltipId(String tooltipId) {
		this.tooltipId = tooltipId;
	}

	public boolean isAuto() {
		return auto;
	}

	public void setAuto(boolean auto) {
		this.auto = auto;
	}

	public List<Trigger> getTrigger() {
		return trigger;
	}

	public void setTrigger(List<Trigger> trigger) {
		this.trigger = trigger;
	}

	public void translate(Map<String, String> map) {
		if (this.title != null && !this.title.isEmpty()) {
			String tit = map.get(this.title);
			if (tit != null && !tit.isEmpty()) {
				this.title = tit;
			}
		}

		if (this.items != null) {
			for (Field f : items) {
				f.translate(map);
			}
		}

		if (this.options != null) {
			for (Option o : options) {
				String ot = map.get(o.getTitle());
				if (ot != null && !ot.isEmpty()) {
					o.setTitle(ot);
				}
			}
		}
	}

	public void getNameCodes(List<String> list) {
		if (this.title != null && !this.title.isEmpty()) {
			list.add(this.title);
		}
		if (this.items != null) {
			for (Field f : items) {
				f.getNameCodes(list);
			}
		}

		if (this.options != null) {
			for (Option o : options) {
				list.add(o.getTitle());
			}
		}

		if (this.dialog != null) {
			this.dialog.getNameCodes(list);
		}
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getPlaceholder() {
		return placeholder;
	}

	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}

	public boolean isKey() {
		return isKey;
	}

	public void setKey(boolean isKey) {
		this.isKey = isKey;
	}
	
	public boolean getSetAll(){
		return setAll;
	}
	
	public void setSetAll(boolean setAll){
		this.setAll = setAll;
	}

	public String getHints() {
		return hints;
	}

	public void setHints(String hints) {
		this.hints = hints;
	}

	public boolean isForConfigOnly() {
		return forConfigOnly;
	}

	public void setForConfigOnly(boolean forConfigOnly) {
		this.forConfigOnly = forConfigOnly;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}
}
