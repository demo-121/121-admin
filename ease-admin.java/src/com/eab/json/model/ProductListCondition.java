package com.eab.json.model;

public class ProductListCondition extends SearchCondition {
	private String productLine;
	private String status;

	public String getProductLine() {
		return productLine;
	}

	public String getStatus() {
		return status;
	}

	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
