package com.eab.json.model;

import java.util.List;

public class Grid extends Field {
	Boolean insertable;

	public Grid(String id, String title, List<Field> columns, Boolean insertable) {
		super(id, Types.GRID, null, title, 100, "100%", null, false, false, null, columns, null, null);
		this.insertable = insertable;
	}
}
