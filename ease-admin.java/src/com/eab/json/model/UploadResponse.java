package com.eab.json.model;

public class UploadResponse {
	boolean isComplete, isTeminate, isChange;
	String tokenID, tokenKey;
	int offSet;
	String selectPageId, itemCode, channelCode,  curPageId;
	int version;
	Dialog dialog;
	
	public boolean isChange() {
		return isChange;
	}

	public void setChange(boolean isChange) {
		this.isChange = isChange;
	}

	public boolean isTeminate() {
		return isTeminate;
	}

	public void setTeminate(boolean isTeminate) {
		this.isTeminate = isTeminate;
	}

	public boolean isComplete() {
		return isComplete;
	}

	public void setComplete(boolean isComplete) {
		this.isComplete = isComplete;
	}

	public String getTokenID() {
		return tokenID;
	}

	public void setTokenID(String tokenID) {
		this.tokenID = tokenID;
	}

	public String getTokenKey() {
		return tokenKey;
	}

	public void setTokenKey(String tokenKey) {
		this.tokenKey = tokenKey;
	}

	public int getOffSet() {
		return offSet;
	}

	public void setOffSet(int offSet) {
		this.offSet = offSet;
	}

	public String getSelectPageId() {
		return selectPageId;
	}

	public void setSelectPageId(String selectPageId) {
		this.selectPageId = selectPageId;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	 

	public String getCurPageId() {
		return curPageId;
	}

	public void setCurPageId(String curPageId) {
		this.curPageId = curPageId;
	}

	public Dialog getDialog() {
		return dialog;
	}

	public void setDialog(Dialog dialog) {
		this.dialog = dialog;
	}
	
	

}
