package com.eab.json.model;

import java.io.Serializable;
/***
 * @author sam.lam
 * Defining trigger on A, which depends B 
 */
public class Trigger implements Serializable{
	
	public class TriggerType {
		/**
		 * id : condition of overriding - [operand key][operator][operand value] 
		 * 		- operand key is used to mapping another value from the current values pool
		 * 		- operator can be '=', '<' or '>'
		 * 		- operand value is a constant, can be optional.
		 * value : default value of A to be set if condition valid, it is optional. if it is blank, value of operand key will be set.
		 * e.g.
		 * 		{ id: 'action=N', value: 'P' } 
		 * - if action = 'N', set the value to 'P'
		 * 
		 * 		{ id: 'strManAge<' }
		 * - if start man age < source value, set the value to start man age. 
		 */
		public final static String valueOverride = "valueOverride";
		
		/**
		 * similar to value override, but only apply if the source value is blank
		 */
		public final static String defaultValueOverride = "defaultValueOverride";
		
		public final static String showIfContains = "showIfContains";
		public final static String showIfEqual = "showIfEqual";
		public final static String optionCondition = "optionCondition";

	}
	
	private String type;	// type of the trigger
	private String id;		// look up id on values, or condition against
	private String value;	// expected value if any
	private String reference;	// ID of expected value if any
	
	public Trigger(String type, String value) {
		super();
		this.type = type;
		this.value = value;
	}
	public Trigger(String type, String id, String value) {
		this(type, value);
		this.id = id;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
