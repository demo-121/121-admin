package com.eab.json.model;

public class Option {
	private String value;
	private String title;
	private String condition;
	private String label;
	private boolean highLight;
	
	public Option(String title, String value) {
		this.title = title;
		this.value = value;
	}
	
	public Option(String title, String value, String label) {
		this.title = title;
		this.value = value;
		this.label = label;
	}
	
	public String getTitle() {
		return title;
	}

	public String getValue() {
		return value;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isHighLight() {
		return highLight;
	}

	public void setHighLight(boolean highLight) {
		this.highLight = highLight;
	}
	
	
}
