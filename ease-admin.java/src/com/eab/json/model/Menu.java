package com.eab.json.model;

import java.util.List;

import com.google.gson.JsonObject;

public class Menu {
	private List<JsonObject> menuItem;

	public Menu(List<JsonObject> menuItem) {
		super();
		this.menuItem = menuItem;
	}

	public List<JsonObject> getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(List<JsonObject> menuItem) {
		this.menuItem = menuItem;
	} 
	
}
