package com.eab.json.model;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.model.MenuItem;
import com.google.gson.JsonObject;

public class Response {

	private transient HttpServletRequest request;

	private String action;
	private String path;
	private String saml;
	private String loggedOn;
	private String errorMsg;
	private Page page;
	private String tokenID;
	private String tokenKey;
	private AppBar appBar;
	private Content content;
	private AppBar assignAppBar;
	private AssignContent assignContent;
	private Menu menu;
	// private MainMenu mainMenu;
	private Dialog dialog;
	private ArrayList<MenuItem> mainMenu;
	private JsonObject user;
	private Map<String, Object> langMap;
	//file or slice upload
	private JsonObject uploadStatus;
	private String attachment;
	private Map<String, Object> actionChain;
	
	
	public Response(String action, String tokenID, String tokenKey, Dialog dialog) {
		super();
		this.action = action;
		this.tokenID = tokenID;
		this.tokenKey = tokenKey;
		this.dialog = dialog;
	}

	public Response(String action, String tokenID, String tokenKey, Page page) {
		super();
		this.action = action;
		this.tokenID = tokenID;
		this.tokenKey = tokenKey;
		this.page = page;
	}

	/**
	 * @param action
	 * @param path
	 * @param content
	 */
	public Response(String action, String path, Content content) {
		super();
		this.action = action;
		this.path = path;
		this.content = content;
	}
	
	/**
	 * @param action
	 * @param actionChain
	 * @param path
	 * @param page
	 * @param tokenID
	 * @param tokenKey
	 * @param appBar
	 * @param content
	 */
	
	public Response(String action, Map<String, Object> actionChain, String path, Page page, String tokenID, String tokenKey, AppBar appBar, Content content) {
		super();
		this.action = action;
		this.actionChain = actionChain;
		this.path = path;
		this.page = page;
		this.tokenID = tokenID;
		this.tokenKey = tokenKey;
		this.appBar = appBar;
		this.content = content;
	}

	/**
	 * @param action
	 * @param path
	 * @param page
	 * @param tokenID
	 * @param tokenKey
	 * @param appBar
	 * @param content
	 */
	public Response(String action, String path, Page page, String tokenID, String tokenKey, AppBar appBar, Content content) {
		super();
		this.action = action;
		this.path = path;
		this.page = page;
		this.tokenID = tokenID;
		this.tokenKey = tokenKey;
		this.appBar = appBar;
		this.content = content;
	}

	/**
	 * @param action
	 * @param tokenID
	 * @param tokenKey
	 * @param appBar
	 * @param content
	 * @param assignAppBar
	 * @param assignContent
	 */
	public Response(String action, Page page, String tokenID, String tokenKey, AppBar appBar, Content content, AppBar assignAppBar, AssignContent assignContent) {
		super();
		this.action = action;
		this.page = page;
		this.tokenID = tokenID;
		this.tokenKey = tokenKey;
		this.appBar = appBar;
		this.content = content;
		this.assignAppBar = assignAppBar;
		this.assignContent = assignContent;
	}

	/**
	 * @param action
	 * @param path
	 * @param page
	 * @param tokenID
	 * @param tokenKey
	 * @param appBar
	 * @param content
	 * @param mainMenu
	 */
	public Response(String action, String path, Page page, String tokenID, String tokenKey, AppBar appBar, Content content, Menu menu) {
		super();
		this.action = action;
		this.path = path;
		this.page = page;
		this.tokenID = tokenID;
		this.tokenKey = tokenKey;
		this.appBar = appBar;
		this.content = content;
		this.menu = menu;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public String getTokenID() {
		return tokenID;
	}

	public void setTokenID(String tokenID) {
		this.tokenID = tokenID;
	}

	public String getTokenKey() {
		return tokenKey;
	}

	public void setTokenKey(String tokenKey) {
		this.tokenKey = tokenKey;
	}

	public AppBar getAppBar() {
		return appBar;
	}

	public void setAppBar(AppBar appBar) {
		this.appBar = appBar;
	}

	public Content getContent() {
		return content;
	}

	public void setContent(Content content) {
		this.content = content;
	}

	public Menu getMainMenu() {
		return menu;
	}

	public void setMainMenu(Menu menu) {
		this.menu = menu;
	}

	public Dialog getDialog() {
		return dialog;
	}

	public void setDialog(Dialog dialog) {
		this.dialog = dialog;
	}

	public void setMenuList(ArrayList<MenuItem> mainMenu) {
		this.mainMenu = mainMenu;
	}

	public ArrayList<MenuItem> setMenuList() {
		return mainMenu;
	}

	public void setUser(JsonObject user) {
		this.user = user;
	}

	public JsonObject getUser() {
		return user;
	}
	
	public void setLangMap(Map<String, Object> langMap) {
		this.langMap = langMap;
	}

	public Map<String, Object> getLangMap() {
		return langMap;
	}
	
	public JsonObject getUploadStatus(){
		return uploadStatus;
	}
	
	public void setUploadStatus(JsonObject uploadStatus){
		this.uploadStatus = uploadStatus;
	}
	
	public void setAttachment(String attachment){
		this.attachment = attachment;
	}
	
	public String getAttachment(){
		return attachment;
	}

	public void setSaml(String _saml) {
		this.saml = _saml;
	}
	
	public void setLoggedOn(String _flag) {
		this.loggedOn = _flag;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getSaml() {
		return saml;
	}

	public String getLoggedOn() {
		return loggedOn;
	}

	public String getErrorMsg() {
		return errorMsg;
	}
	
	
}
