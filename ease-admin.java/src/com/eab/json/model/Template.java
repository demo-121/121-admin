package com.eab.json.model;

import java.util.List;

public class Template extends Field {
	String action;
	protected List<Field> detailItems;
	protected Object menu;
	String emptyMessage;
	private boolean isDirectEdit = false;
	
	public Template() {
		super();
	}

	public Template(String id, List<Field> items, String type, String title) {
		super(id, type, title);
		setItems(items);
	}
	
	public Template(String id, List<Field> items, String type, String title, List<Field> menu ) {
		this(id, items, type, title);
		this.menu = menu;
	}
	
	public String getAction(){
		return action;
	}
	public void setAction(String action){
		this.action = action;
	}
	

	public List<Field> getDetailItems() {
		return detailItems;
	}

	public void setDetailItems(List<Field> detailItems) {
		this.detailItems = detailItems;
	}
	
	public String getEmptyMessage(){
		return emptyMessage;
	}
	public void setEmptyMessage(String emptyMessage){
		this.emptyMessage = emptyMessage;
	}

	public Object getMenu() {
		return menu;
	}

	public void setMenu(Object menu) {
		this.menu = menu;
	}

	public boolean isDirectEdit() {
		return isDirectEdit;
	}

	public void setDirectEdit(boolean isDirectEdit) {
		this.isDirectEdit = isDirectEdit;
	}
	
	
}
