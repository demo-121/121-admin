package com.eab.json.model;

import java.util.List;
import java.util.Map;

public class AppBar {
	private AppBarTitle title;
	private List<List<Field>> actions;
	private Object values;
	
	public AppBar(AppBarTitle title, List<List<Field>> actions) {
		super();
		this.title = title;
		this.actions = actions;
	}
	
	public AppBar(AppBarTitle title, List<List<Field>> actions, Object values) {
		super();
		this.title = title;
		this.actions = actions;
		this.values = values;
	}

	public AppBarTitle getTitle() {
		return title;
	}

	public void setTitle(AppBarTitle title) {
		this.title = title;
	}

	public List<List<Field>> getActions() {
		return actions;
	}

	public void setActions(List<List<Field>> actions) {
		this.actions = actions;
	}

	public Object getValues() {
		return values;
	}

	public void setValues(Object values) {
		this.values = values;
	}

	public void translate(Map<String, String> map){
		
		if (this.title != null) {
			title.translate(map);
		}

		if (this.actions != null) {
			for (List<Field> l : this.actions) {
				for (Field f : l) {
					f.translate(map);
				}
			}
		}
	}


	public void getNameCodes(List<String> list) {
		if (this.title != null) {
			title.getNameCodes(list);
		}
		if (this.actions != null) {
			for (List<Field> l : this.actions) {
				if (l != null) {
					for (Field f : l) {
						f.getNameCodes(list);
					}
				}
			}
		}
	}
	
}
