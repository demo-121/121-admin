package com.eab.json.model.grid;

import java.util.List;

public class Grid {
	private String rate_code;
	private String name_code;

	private List<Header> tabs;
	private List<String> resultName;

	private List<Column> columns;

	private Boolean insertable;

	private int height;
	private int width;

	public String getRate_code() {
		return rate_code;
	}

	public void setRate_code(String rate_code) {
		this.rate_code = rate_code;
	}

	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	public List<String> getResultName() {
		return resultName;
	}

	public void setResultName(List<String> resultName) {
		this.resultName = resultName;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public List<Header> getTabs() {
		return tabs;
	}

	public void setTabs(List<Header> tabs) {
		this.tabs = tabs;
	}

	public Boolean getInsertable() {
		return insertable;
	}

	public void setInsertable(Boolean insertable) {
		this.insertable = insertable;
	}

	public String getName_code() {
		return name_code;
	}

	public void setName_code(String name_code) {
		this.name_code = name_code;
	}

}
