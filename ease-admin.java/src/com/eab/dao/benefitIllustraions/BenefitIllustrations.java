package com.eab.dao.benefitIllustraions;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Option;
import com.eab.model.profile.UserPrincipalBean;

public class BenefitIllustrations {
	//TODO:test for BI template		
		public List<Option> getBITemplate(UserPrincipalBean principal, int version) throws SQLException {
			DBManager dbm;
			Connection conn = null;
			ResultSet rs = null; 
			List<Option> biOptions = null;
			try {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				biOptions = new ArrayList<Option>();

				String sql = " select n.name_desc as name_desc, n.bi_code as bi_code from bi_name n inner join bi_trx t on ";
				sql = sql + " n.comp_code=t.comp_code ";
				sql = sql + " and n.bi_code=t.bi_code ";
				sql = sql + " and n.VERSION=t.VERSION ";
				sql = sql + " and n.comp_code =  " + dbm.param(principal.getCompCode(), DataType.TEXT) ;
				sql = sql + " and n.lang_code =  " + dbm.param(principal.getLangCode(), DataType.TEXT) ;				
				sql = sql + " and n.status = 'A' ";
				sql = sql + " and n.NAME_TYPE = 'BINAME' ";
				sql = sql + " and n.VERSION= " + dbm.param(version, DataType.INT) ;;

				rs = dbm.select(sql, conn);
				if (rs != null) {
					while (rs.next()) {
						biOptions.add(new Option(rs.getString("name_desc"), rs.getString("bi_code")));
					}
				}
			} catch (Exception e) {
				Log.error(e);
			} finally {
				dbm = null;
				rs = null;
				if (conn != null && !conn.isClosed())
					conn.close();
				conn = null;
			}
			return biOptions;
		}
}
