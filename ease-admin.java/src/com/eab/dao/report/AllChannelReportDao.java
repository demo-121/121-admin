package com.eab.dao.report;

import com.eab.common.Constants;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

public class AllChannelReportDao {

    public static JSONArray getProxyDateRanges(String userId){
        Connection conn = null;
        DBManager dbm = null;
        ResultSet rs = null;

        JSONArray ranges = new JSONArray();
        try{
            conn = DBAccess.getConnectionApi();
            dbm = 	new DBManager();
            String sql = " SELECT USER_ID" +
                    ",to_char( cast(START_DT as timestamp) at time zone '"+Constants.TIMEZONE_ID+"', '"+Constants.DATE_PATTERN_AS_REPORT_DATE+"') as DISPLAY_START " +
                    ",to_char( cast(END_DT as timestamp) at time zone '"+Constants.TIMEZONE_ID+"', '"+Constants.DATE_PATTERN_AS_REPORT_DATE+"') as DISPLAY_END " +
                    ",to_char( cast(START_DT as timestamp) at time zone '"+Constants.TIMEZONE_ID+"', '"+Constants.DATETIME_PATTERN_AS_ADMIN_DATE+"') as START_DT " +
                    ",to_char( cast(END_DT as timestamp) at time zone '"+Constants.TIMEZONE_ID+"', '"+Constants.DATETIME_PATTERN_AS_ADMIN_DATE+"') as END_DT " +
                    "FROM API_AGT_PROXY_HIST WHERE USER_ID = " + dbm.param(userId, DataType.TEXT)+" ORDER BY START_DT";
            rs = dbm.select(sql, conn);
            if(rs != null){

                while(rs.next()){
                    JSONObject range = new JSONObject();
                    range.put("DISPLAY_START",  rs.getString("DISPLAY_START"));
                    range.put("DISPLAY_END",  rs.getString("DISPLAY_END"));
                    range.put("START_DT",  rs.getString("START_DT"));
                    range.put("END_DT",  rs.getString("END_DT"));
                    ranges.put(range);
                }
            }
        }catch (Exception e){
            Log.error(e);
        }finally {
            dbm.clear();
            dbm = null;
            try {
                if (conn != null && !conn.isClosed())
                    conn.close();
            }catch(Exception e){
                Log.error(e);
            }
            conn = null;
        }
        return ranges;
    }
}
