package com.eab.dao.report;

import com.eab.common.EnvVariable;
import com.eab.common.Log;
import com.eab.couchbase.CBServer;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Option;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Report {

    public static List<Option> getPickerOptions(String lookupKey) {
        DBManager dbm = null;
        List<Option> options = new ArrayList<Option>();
        String sqlLangs = "select name_code, lookup_field from sys_lookup_mst where lookup_key = ? order by seq";
        ResultSet rs = null;
        Connection conn = null;
        try {
            dbm = new DBManager();
            conn = DBAccess.getConnection();
            dbm.param(lookupKey, DataType.TEXT);
            rs = dbm.select(sqlLangs, conn);
            if (rs != null) {
                while (rs.next()) {
                    String nameCode = rs.getString("name_code");
                    String lookupField = rs.getString("lookup_field");
                    options.add(new Option(nameCode, lookupField));
                }
            }
            conn.close();
        } catch (Exception e) {
            Log.error(e);
        } finally {
            dbm.clear();
            dbm = null;
            try {
                if (conn != null && !conn.isClosed())
                    conn.close();
            }catch(Exception e){
                Log.error(e);
            }
            conn = null;
        }

        return options;
    }


    public static List<Option> getChannels(){

        String gatewayURL = EnvVariable.get("GATEWAY_URL");
        String gatewayPort = EnvVariable.get("GATEWAY_PORT");
        String gatewayDBName = EnvVariable.get("GATEWAY_DBNAME");
        String gatewayUser = EnvVariable.get("GATEWAY_USER");
        String gatewayPW = EnvVariable.get("GATEWAY_PW");
        CBServer cb = new CBServer(gatewayURL,gatewayPort,gatewayDBName,gatewayUser,gatewayPW);
        List<Option> channelOptons = new ArrayList<Option>();
        channelOptons.add(new Option("All", "all"));
        JSONObject channelsDoc = cb.getDoc("channels");
            if(channelsDoc != null && channelsDoc.has("channels") && channelsDoc.get("channels") instanceof JSONObject){
            JSONObject channels = channelsDoc.getJSONObject("channels");
            Iterator<?> keys = channels.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                if (channels.get(key) instanceof JSONObject) {
                    JSONObject channelItem = channels.getJSONObject(key);
                    if (channelItem.has("title") && channelItem.get("title") instanceof String){
                            channelOptons.add(new Option(channelItem.getString("title"), key));
                    }


                }
            }
        }
        return channelOptons;
    }

    public static JSONObject getAgentHierarchys(){
        Connection conn = null;
        DBManager dbm = null;
        ResultSet rs = null;
        JSONObject records  = new JSONObject();
        try{
            conn = DBAccess.getConnectionApi();
            dbm = 	new DBManager();
            String sql = "SELECT   t2.user_id, t2.agent_no, t2.name, t2.upline_1_no, t2.upline_1_name, t2.upline_2_no,  (SELECT name FROM user_profile WHERE agent_no = t2.upline_2_no  ) upline_2_name FROM  (SELECT t0.user_id, t0.agent_no,    t0.name,    t1.agent_no upline_1_no,    t1.name upline_1_name,    t1.upline_no_1 as upline_2_no  FROM user_profile t0,    user_profile t1  WHERE t0.upline_no_1 = t1.agent_no ) t2";
            rs = dbm.select(sql, conn);

            if(rs != null){
                while(rs.next()){
                    JSONObject record = new JSONObject();
                    record.put("name", rs.getString("name"));
                    record.put("upline_1_name", rs.getString("upline_1_name"));
                    record.put("upline_2_name", rs.getString("upline_2_name"));
                    record.put("agent_no", rs.getString("agent_no"));
                    record.put("upline_1_no", rs.getString("upline_1_no"));
                    record.put("upline_2_no", rs.getString("upline_2_no"));
                    records.put(rs.getString("user_id"), record);
                }
            }
        }catch (Exception e){
            Log.error(e);
        }finally {
            dbm.clear();
            dbm = null;
            try {
                if (conn != null && !conn.isClosed())
                    conn.close();
            }catch(Exception e){
                Log.error(e);
            }
            conn = null;
        }
        return records;
    }
}
