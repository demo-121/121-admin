package com.eab.dao.report;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SubmissCasesReport {
    public static ArrayList<String> getRlsPolNum(){
        Connection conn = null;
        DBManager dbm = null;
        ResultSet rs = null;
        ArrayList<String> rlsPolNum = new ArrayList<String>();
        try{
            conn = DBAccess.getConnectionApi();
            dbm = 	new DBManager();
            String sql = "SELECT COUNT(POL_NUM) AS CNT, POL_NUM FROM API_RLS_APP_STATUS WHERE STATUS !='F' GROUP BY POL_NUM HAVING COUNT(POL_NUM) >0";
            rs = dbm.select(sql, conn);
            if(rs != null){
                while(rs.next()){
                    String polNum = rs.getString("POL_NUM");
                    if(polNum != null && !polNum.isEmpty())
                        rlsPolNum.add(polNum);
                }
            }
        }catch (Exception e){
            Log.error(e);
        }finally {
            dbm.clear();
            dbm = null;
            try {
                if (conn != null && !conn.isClosed())
                    conn.close();
            }catch(Exception e){
                Log.error(e);
            }
            conn = null;
        }
        return rlsPolNum;
    }


}
