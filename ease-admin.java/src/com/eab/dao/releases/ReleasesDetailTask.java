package com.eab.dao.releases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.system.Name;
import com.eab.dao.tasks.Tasks;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.profile.UserPrincipalBean;

public class ReleasesDetailTask {

	public ArrayList<Map<String, Object>> getUnassignedTaskList(HttpServletRequest request, UserPrincipalBean principal, String criteria, String sortBy, String sortDir, Language langObj) throws SQLException {		
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			rs = searchUnassigedTaskList(criteria, dbm, principal, sortBy, sortDir, conn);
			return getList(rs, request, principal, langObj);			
		} catch (Exception e) {
			Log.error("{ReleaseDetailTask getUnassignedTaskList} ----------->" + e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}

	public ResultSet searchUnassigedTaskList(String criteria, DBManager dbm, UserPrincipalBean principal, String sortBy, String sortDir, Connection conn)throws Exception {
		ResultSet rs = null;
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////			   										   
		String sql = " select *"
			   	   + " from v_task_list"
				   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
				   + " and status_code = 'U'";//Unassigned

		//Searching criteria by user input
		if(criteria != null && !criteria.equals("")){	
			criteria = '%' + criteria.toUpperCase() +'%';

			sql += " and (upper(item_code) like "+ dbm.param(criteria, DataType.TEXT) 
				 + " or upper(task_desc) like "+ dbm.param(criteria, DataType.TEXT)
				 + " or upper(ref_no) like " + dbm.param(criteria, DataType.TEXT) +")";
		}
			
		if (!sortBy.equals("")){
			if (sortDir.equals("A")) {
				sortDir = "asc";
			} else {
				sortDir = "desc";
			}
			
			if (sortBy.equals("effDate"))
				sortBy = "eff_datetime";
						
			sql += " order by " + sortBy + " " + sortDir;	
		} else {
			//Default sorting
			sql += " order by eff_datetime desc";	
		}		
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		rs = dbm.select(sql, conn);
						
		return rs;
	}

	private ArrayList<Map<String, Object>> getList(ResultSet rs, HttpServletRequest request, UserPrincipalBean principal, Language langObj) throws Exception {
	    ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
	    String status = "";
	    String release = "";
	    String type = "";
	    String name = "";
	    String ver_no = "";
	    String item_code = "";
	    String func_code = "";
	    String eff_date = "";
	    SimpleDateFormat dateformatter = Function.getDateFormat(principal);
	    
	    while (rs.next()) {
	    	Map<String, Object> row = new HashMap<String, Object>();
	    	
	    	if(rs.getObject("status") != null && !rs.getObject("status").equals("")){
	    		status = rs.getObject("status").toString();
	    	}
	    	
	    	if(rs.getObject("release") != null && !rs.getObject("release").equals("")){
	    		release = rs.getObject("release").toString();	
	    	}
	    	
	    	if(rs.getObject("type") != null && !rs.getObject("type").equals("")){
	    		type = rs.getObject("type").toString();	
	    	}
	    	
	    	if(rs.getObject("ver_no") != null && !rs.getObject("ver_no").equals("")){
	    		ver_no = rs.getObject("ver_no").toString();	
	    	}
	    	
	    	if(rs.getObject("item_code") != null && !rs.getObject("item_code").equals("")){
	    		item_code = rs.getObject("item_code").toString();	
	    	}
	    	
	       	if(rs.getObject("func_code") != null && !rs.getObject("func_code").equals("")){
	       		func_code = rs.getObject("func_code").toString();	
	    	}
	    	
			try {
				eff_date = dateformatter.format(rs.getDate("eff_date"));		
			} catch (Exception e) {
				
			}
			
			Tasks tasks = new Tasks();
			name = tasks.getItemName(func_code, ver_no, item_code, principal); 
			
			row.put("id", rs.getObject("task_code"));
			row.put("taskCode", rs.getObject("task_code"));		
			row.put("item_code", item_code + " " + name + " (v " + ver_no + ")");
			row.put("task_desc", rs.getObject("task_desc"));
			row.put("ref_no", rs.getObject("ref_no"));
			row.put("type", type.equals("") ? "" : langObj.getNameDesc(type));
			row.put("status", status.equals("") ? "" : langObj.getNameDesc(status));
			row.put("release", release.equals("") ? "" : langObj.getNameDesc("RELEASE_ASSIGNED_TO") + " " + rs.getObject("release_id") + " - " + rs.getObject("release_name") + " (" + langObj.getNameDesc(release) +")");
			row.put("upd_date", rs.getObject("upd_date"));
			row.put("statusCode", rs.getObject("status_code"));
			row.put("lastUpdBy", rs.getObject("upd_by"));
			row.put("effDate", eff_date);
			row.put("createDate", rs.getObject("create_date"));
			row.put("createBy", rs.getObject("create_by"));
	     
			list.add(row);
			
	        //Reset all values before going next record
		    status = "";
		    release = "";
		    type = "";
		    name = "";
		    ver_no = "";
		    item_code = "";
		    func_code = "";	    
		    eff_date = "";
	    }
	    
	    return list;
	}
	
}
