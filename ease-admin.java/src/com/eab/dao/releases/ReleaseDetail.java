package com.eab.dao.releases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.tasks.Tasks;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.profile.UserPrincipalBean;

public class ReleaseDetail {

	public ArrayList<Map<String, Object>> getReleaseDetail(HttpServletRequest request, UserPrincipalBean principal, String releaseID, Language langObj) throws SQLException {		
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select * "
					   + " from v_release "
					   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					   + " and release_id = " + dbm.param(releaseID, DataType.TEXT)
					   + " order by upd_date desc";
			
			rs = dbm.select(sql, conn);
			
			return getList(rs, request, principal, langObj);		
		} catch (SQLException e) {
			Log.error("{ReleaseDetail getReleaseDetail 1} ----------->" + e);
		} catch (Exception e) {
			Log.error("{ReleaseDetail getReleaseDetail 2} ----------->" + e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}
	
	////TODO : for check base version when release -s
	public Map<String, Integer> getMstVerMap() throws SQLException{
		HashMap<String, Integer> mstVerMap = new HashMap<String, Integer>();
		
		for(int i=0; i<3; i++){
			String prefix = ""; 
			 
			switch (i){
				case 0:
					prefix  = "prod";
					break;
				case 1:
					prefix  = "need";
					break;
				case 2:
					prefix  = "bi";
					break; 
					
			}
			Connection conn = null;
			DBManager dbm = null;
			ResultSet rs = null;
			 
					
			try {	
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
	 
				String sql = "select comp_code, "+prefix+"_code, version from "+ prefix +"_mst";
				 	rs = dbm.select(sql, conn); 
				
				while (rs.next()) {
					String compCode = rs.getString("comp_code");
					String prodCode = rs.getString(prefix+"_code"); 
					String key = prefix+"_"+compCode+"_"+prodCode;
					int mstVer = rs.getInt("version");
					
					mstVerMap.put(key, (mstVer));
				}
				
				rs = null;
				dbm.clear();
				
				 
			} catch (SQLException e) {
				Log.error("{ReleaseDetail checkBaseVersion 1} ----------->" + e);
			} catch (Exception e) {
				Log.error("{ReleaseDetail checkBaseVersion 2} ----------->" + e);
			} finally {
				dbm = null;
				rs = null;
				
				if (conn != null && !conn.isClosed())
					conn.close();
					conn = null;
			}
		}
		return mstVerMap;
		
	}
	
	
	public Map<String, Integer> getTrxVerMap() throws SQLException{
		HashMap<String, Integer> trxVerMap = new HashMap<String, Integer>();
		
		for(int i=0; i<3; i++){
			String prefix = ""; 
			 
			switch (i){
				case 0:
					prefix  = "prod";
					break;
				case 1:
					prefix  = "need";
					break;
				case 2:
					prefix  = "bi";
					break; 
					
			}
			Connection conn = null;
			DBManager dbm = null;
			ResultSet rs = null;
			 
					
			try {	
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
	 
				String sql = "select comp_code, "+prefix+"_code, version, base_version from "+ prefix +"_trx";
				 	rs = dbm.select(sql, conn); 
				
				while (rs.next()) {
					String compCode = rs.getString("comp_code");
					String prodCode = rs.getString(prefix+"_code"); 
					int version = rs.getInt("version");
					String key = prefix+"_"+compCode+"_"+prodCode+"_"+version;
					int trxVer = rs.getInt("base_version");
					
					trxVerMap.put(key, (trxVer));
				}
				
				rs = null;
				dbm.clear();
				
				 
			} catch (SQLException e) {
				Log.error("{ReleaseDetail checkBaseVersion 1} ----------->" + e);
			} catch (Exception e) {
				Log.error("{ReleaseDetail checkBaseVersion 2} ----------->" + e);
			} finally {
				dbm = null;
				rs = null;
				
				if (conn != null && !conn.isClosed())
					conn.close();
					conn = null;
			}
		}
		return trxVerMap;
		
	}
	
	////TODO : for check base version when release -e
	
	 

	private ArrayList<Map<String, Object>> getList(ResultSet rs, HttpServletRequest request, UserPrincipalBean principal, Language langObj) throws SQLException {
	    ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
	    String status = "";
	    String target_release_date = "";
	    String scheduled_release_date = "";
	    String released_date = "";
	    String create_date = "";
	    String update_by = "";
	    String create_by = "";
	    SimpleDateFormat dateformatter;
	    SimpleDateFormat dateTimeformatter;
	    SimpleDateFormat timeformatter;
	    
		dateformatter = Function.getDateFormat(principal);
		timeformatter = Function.getTimeFormat(principal);
		dateTimeformatter = Function.getDateTimeFormat(principal);
	    
	    while (rs.next()) {
	    	Map<String, Object> row = new HashMap<String, Object>();
	    	
	    	if(rs.getObject("status") != null && !rs.getObject("status").equals(""))
	    		status = rs.getObject("status").toString();
	    					    
			try {
				target_release_date = dateformatter.format(rs.getDate("target_release_date"));	
			} catch (Exception e) {}
			
			try {				
				scheduled_release_date = dateTimeformatter.format(rs.getObject("scheduled_release_date"));
			} catch (Exception e) {}
			
			try {
				released_date = dateTimeformatter.format(rs.getDate("upd_date"));	
			} catch (Exception e) {}	
			
			try {
				create_date = dateTimeformatter.format(rs.getDate("create_date"));	
			} catch (Exception e) {}
			
			row.put("releaseID", rs.getObject("RELEASE_ID"));
			row.put("releaseName", rs.getObject("RELEASE_NAME"));
			row.put("targetReleaseDate", rs.getDate("target_release_date").getTime());
			row.put("statusCode", rs.getObject("STATUS"));
			
			if(rs.getObject("scheduled_release_date") != null && !rs.getObject("scheduled_release_date").equals(""))
				row.put("scheduledDate",dateformatter.format(rs.getObject("scheduled_release_date")));
			
			if(rs.getObject("scheduled_release_date") != null && !rs.getObject("scheduled_release_date").equals(""))
				row.put("scheduledTime",timeformatter.format(rs.getObject("scheduled_release_date")));
									
			switch (status) {
				case "P":  
					row.put("status", langObj.getNameDesc("TARGET_RELEASE_FOR") + " " + target_release_date + " [" + langObj.getNameDesc("PENDING") +"]");
					break;
				case "T":  
					row.put("status", langObj.getNameDesc("TARGET_RELEASE_FOR") + " " + target_release_date + " [" + langObj.getNameDesc("TESTING") +"]");
					break;
				case "S":  
					row.put("status", langObj.getNameDesc("SCHEDULED_TO_BE") + " " + scheduled_release_date);
					break;
				case "R":  
					row.put("status", langObj.getNameDesc("RELEASED_ON") + " " + released_date);
					break;			
				default: 
					break;
			}
			
			if(rs.getObject("UPD_BY") != null && !rs.getObject("UPD_BY").equals(""))
				update_by = Function2.getUserName(rs.getObject("UPD_BY").toString());
			
			if(rs.getObject("CREATE_BY") != null && !rs.getObject("CREATE_BY").equals(""))
				create_by = Function2.getUserName(rs.getObject("CREATE_BY").toString());
			
			row.put("updateBy", update_by + " " + langObj.getNameDesc("ON") + " " + released_date);
			row.put("createBy", create_by + " " + langObj.getNameDesc("ON") + " " + create_date);
			
			row.put("remarks", rs.getObject("REMARKS"));
			row.put("isFailed", rs.getObject("IS_FAILED"));
			row.put("isPublishing", rs.getObject("IS_PUBLISHING"));
			row.put("tooltipId", rs.getObject("failed_reason"));
			list.add(row);
	    }
	    
	    return list;
	}
	
	public ArrayList<Map<String, Object>> getTaskList(HttpServletRequest request, UserPrincipalBean principal, String releaseID, String sortBy, String sortDir, Language langObj) throws SQLException {		
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		Tasks task = new Tasks();
		
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			rs = searchTaskList(releaseID , dbm, principal, sortBy, sortDir, conn);
			return task.getList(rs, request, principal, langObj);
			
		} catch (SQLException e) {
			Log.error("{ReleaseDetail getTaskList 1} ----------->" + e);
		} catch (Exception e) {
			Log.error("{ReleaseDetail getTaskList 2} ----------->" + e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}
	
	public ArrayList<Map<String, Object>> getMasterList(HttpServletRequest request, UserPrincipalBean principal, Language langObj) throws SQLException {		
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		Tasks task = new Tasks();
		
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			rs = masterList(dbm, principal, conn);
			return task.getList(rs, request, principal, langObj);			
		} catch (Exception e) {
			Log.error("{ReleaseDetail getMasterList} ----------->" + e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}
	
	public ResultSet masterList(DBManager dbm, UserPrincipalBean principal, Connection conn)throws Exception {
		ResultSet rs = null;
						   		
		String sql = " select task.task_code,"
	               + " task.item_code,"
	               + " task.ver_no,"
	               + " task.task_desc,"
	               + " task.ref_no,"	               
  				   + " task.upd_date,"
  				   + " task.create_date,"
  				   + " task.eff_date,"
	               + " task.upd_by,"
	               + " task.create_by,"                
	               + " lookup_a.name_code as status,"
	               + " func.name_code as type,"
	               + " release.release_name,"
	               + " release.release_id,"
	               + " release.status as status_code,"
	               + " lookup_b.name_code as release"
	
                   + " from task_mst task"

	               + " inner join sys_func_mst func"
	               + " on func.func_code = task.func_code"
	
				   + " left join sys_lookup_mst lookup_a"
				   + " on lookup_a.lookup_key = 'TASK_STATUS'"
				   + " and lookup_a.lookup_field = task.status"
				
				   + " left join release_task"
				   + " on release_task.task_code = task.task_code"
				
				   + " left join release_mst release"
				   + " on release.release_id = release_task.release_id"
				
				   + " left join sys_lookup_mst lookup_b"
				   + " on lookup_b.lookup_key = 'RELEASE_STATUS'"
				   + " and lookup_b.lookup_field = RELEASE.STATUS"
				   				
				   + " where task.comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT); 
			
		sql += " order by upd_date desc";
		
		rs = dbm.select(sql, conn);	
		
		return rs;
	}
	
	public ResultSet searchTaskList(String releaseID , DBManager dbm, UserPrincipalBean principal, String sortBy, String sortDir, Connection conn)throws Exception {
		ResultSet rs = null;
				
//        String sql = " select task.task_code,"
//	               + " task.item_code,"
//	               + " task.ver_no,"
//	               + " task.task_desc,"
//	               + " task.ref_no,"	               
//				   + " task.upd_date,"
//				   + " task.create_date,"
//				   + " task.eff_date,"
//	               + " task.upd_by,"
//	               + " task.create_by,"                
//	               + " lookup_a.name_code as status,"
//	               + " func.name_code as type,"
//	               + " release.release_name,"
//	               + " release.release_id,"
//	               + " release.status as status_code,"
//	               
//	 			   + " lookup_b.name_code as release,"
//	 			   + " task.upd_date as upd_datetime,"
//	 			   + " func.func_code as func_code"
//	 			  
//	               + " from task_mst task"
//	
//	               + " inner join sys_func_mst func"
//	               + " on func.func_code = task.func_code"
//	
//	               + " left join sys_lookup_mst lookup_a"
//	               + " on lookup_a.lookup_key = 'TASK_STATUS_FILTER'"
//	               + " and lookup_a.lookup_field = task.status"
//	
//	               + " left join release_task"
//	               + " on release_task.task_code = task.task_code"
//	
//	               + " left join release_mst release"
//	               + " on release.release_id = release_task.release_id"
//	
//	               + " left join sys_lookup_mst lookup_b"
//	               + " on lookup_b.lookup_key = 'RELEASE_STATUS_FILTER'"
//	               + " and lookup_b.lookup_field = RELEASE.STATUS"
		
		
		String sql = " select a.*, "
				   + " b.name_desc as prod_name,"
				   + " f.name_desc as need_name"
			   	   + " from v_task_list a"
				
			   	   //Get product name
		           + " left join prod_name b" 
		           + " on a.item_code = b.prod_code"
		           + " and b.lang_code = " + dbm.param(principal.getLangCode(), DataType.TEXT)
		           + " and a.comp_code = b.comp_code"
		           + " and a.ver_no = b.version"
		           + " and b.name_type = 'PRODNAME'"
		           
		           //Get needs name 
		           + " left join ("
		           + " select d.name_desc, c.lookup_field from sys_lookup_mst c"
		           + " inner join sys_name_mst d"
		           + " on c.name_code = d.name_code"
		           + " and d.lang_code = " + dbm.param(principal.getLangCode(), DataType.TEXT)
		           + " and c.lookup_key = 'NEED_NAME') f"
		           + " on a.item_code = f.lookup_field" 
		          
		           + " where a.comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT);     
				
		//Task ReleaseID
		if(releaseID != null && !releaseID.equals("")){
			if(!releaseID.equals("*"))
				sql += " and a.release_id = "+ dbm.param(releaseID, DataType.TEXT);
		}
		
		if (!sortBy.equals("")){
			if (sortDir.equals("A"))
				sortDir = "asc";
			else
				sortDir = "desc";
						
			if (sortBy.equals("upd_date"))
				sortBy = "upd_datetime";
						
			sql += " order by " + sortBy + " " + sortDir;	
		} else {
			//Default sorting
			sql += " order by a.upd_date desc";
		}			
				
		rs = dbm.select(sql, conn);	
		
		return rs;
	}
		
	public boolean updateRelease(String releaseID, String releaseName, Date targetDate , String remarks, UserPrincipalBean principal)throws Exception {
		boolean output = false;
		Connection conn = null;
		DBManager dbm = null;
		String sql = "";
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			java.sql.Timestamp _targetDate = new java.sql.Timestamp(targetDate.getTime());
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy 12:00:00");
			String target_release_date = format.format(_targetDate);
			
			sql = " update release_mst "
		    	+ " set upd_date = SYSDATE,"												//Update date
				+ " upd_by = " + dbm.param(principal.getUserCode(), DataType.TEXT) + ","	//Update by
				+ " release_name = " + dbm.param(releaseName, DataType.TEXT) + ","			//Release name
				+ " target_release_date = TO_DATE( " + dbm.param(target_release_date, DataType.TEXT) + ", 'MM/DD/YYYY HH24:MI:SS')"+","//Target release date
				+ " remarks = " +  dbm.param(remarks, DataType.TEXT)						//Remarks
				+ " where release_id = " + dbm.param(releaseID, DataType.TEXT);				
					  
			dbm.update(sql, conn);
			
			output = true;
		} catch (SQLException e) {
			Log.error("{ReleaseDetail updateRelease 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{ReleaseDetail updateRelease 2} ----------->" + e);
		} finally {
			dbm = null;
			
			if (conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
	
		return output;
	}

	public ArrayList<Map<String, Object>> abortRelease(HttpServletRequest request, UserPrincipalBean principal, String releaseID) throws SQLException {		
		Connection conn = null;
		DBManager dbm = null;
		ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		try {		
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			if(releaseID != null && !releaseID.equals("")){
				String sql = " update release_mst "
						   + " set upd_date = SYSDATE, "											//Update date
						   + " upd_by = " + dbm.param(principal.getUserCode(), DataType.TEXT) +","	//Update by
					       + " status = 'P'"
					       + " where release_id = " +  dbm.param(releaseID, DataType.TEXT)
					       + " AND status = 'S'";

				dbm.update(sql, conn);	
			}
			
			Map<String, Object> output = new HashMap<String, Object>();
			output.put("sucess", true);
			list.add(output);
			return list;			
		} catch (SQLException e) {
			Map<String, Object> output = new HashMap<String, Object>();
			Log.error("{ReleaseDetail abortRelease 1} ----------->" + e);
			list.add(output);
		} catch (Exception e) {
			Map<String, Object> output = new HashMap<String, Object>();
			Log.error("{ReleaseDetail abortRelease 2} ----------->" + e);
			list.add(output);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}
		
	public ArrayList<Map<String, Object>> deleteRelease(HttpServletRequest request, UserPrincipalBean principal, String releaseID) throws SQLException {		
		Connection conn = null;
		DBManager dbm = null;
		ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			if(releaseID != null && !releaseID.equals("")){
				String sql = " update release_mst "
						   + " set upd_date = SYSDATE,"												//Update date
						   + " upd_BY = " + dbm.param(principal.getUserCode(), DataType.TEXT) + ","	//Update by
						   + " status = 'D'"														
						   + " where release_id = " +  dbm.param(releaseID, DataType.TEXT)
						   + " and (status = 'P' or status = 'T')";
				
				dbm.update(sql, conn);	
			}		
			
			Map<String, Object> output = new HashMap<String, Object>();
			output.put("sucess", true);
			list.add(output);
			return list;	
		} catch (SQLException e) {
			Map<String, Object> output = new HashMap<String, Object>();
			Log.error("{ReleaseDetail deleteRelease 1} ----------->" + e);
			list.add(output);
		} catch (Exception e) {
			Map<String, Object> output = new HashMap<String, Object>();
			Log.error("{ReleaseDetail deleteRelease 2} ----------->" + e);
			list.add(output);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}
		
	public ArrayList<Map<String, Object>> rescheduleRelease(HttpServletRequest request, UserPrincipalBean principal, String releaseID, String targetDate) throws SQLException {		
		Connection conn = null;
		DBManager dbm = null;
		ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			if(releaseID != null && !releaseID.equals("")){
				String sql = " update release_mst "
						   + " set upd_Date = SYSDATE,"																		//Update date
						   + " upd_by = " + dbm.param(principal.getUserCode(), DataType.TEXT) + ","							//Update by	
						   + " scheduled_release_date = TO_DATE( " + dbm.param(targetDate, DataType.TEXT) + ", 'yyyy/mm/dd')"
						   + " where release_id = " +  dbm.param(releaseID, DataType.TEXT)
						   + " and status = 'S'";
				
				dbm.update(sql, conn);	
			}		
			
			Map<String, Object> output = new HashMap<String, Object>();
			output.put("sucess", true);
			list.add(output);
			return list;	
		} catch (SQLException e) {
			Map<String, Object> output = new HashMap<String, Object>();
			Log.error("{ReleaseDetail rescheduleRelease 1} ----------->" + e);
			list.add(output);
		} catch (Exception e) {
			Map<String, Object> output = new HashMap<String, Object>();
			Log.error("{ReleaseDetail rescheduleRelease 2} ----------->" + e);
			list.add(output);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}
	
	public ArrayList<Map<String, Object>> getTestENVList(HttpServletRequest request, UserPrincipalBean principal) throws SQLException {		
		return getENVList(request, principal, "Y");
	}
	
	public ArrayList<Map<String, Object>> getENVList(HttpServletRequest request, UserPrincipalBean principal, String allDelete) throws SQLException {		
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select env_id,"
					   + " env_name"
					   + " from sys_env_mst "				   
					   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					   + " and status = 'A'";
			if (allDelete != null) {
			   sql += " and allow_delete = '"+allDelete+"'";
			}
					   
		   sql += " order by modify_date desc";
						
			rs = dbm.select(sql, conn);
			
			ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			    
			while (rs.next()) {
				Map<String, Object> row = new HashMap<String, Object>();
			    	
				row.put("envID", rs.getObject("env_id"));
				row.put("envName", rs.getObject("env_name"));			
	
				list.add(row);
			}			
			
			return list;
		} catch (SQLException e) {
			Log.error("{ReleaseDetail getTestENVList 1} ----------->" + e);
		} catch (Exception e) {
			Log.error("{ReleaseDetail getTestENVList 2} ----------->" + e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}	
		
	public Boolean UpdateStatus(UserPrincipalBean principal, String releaseID, String status) throws SQLException {		
		Connection conn = null;
		DBManager dbm = null;
			
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			
			String sql = " update release_mst set"
					   + " upd_date = SYSDATE,"													//Update date
					   + " upd_by =  " + dbm.param(principal.getUserCode(), DataType.TEXT);		//Update by
			
			switch (status){
				case "F"://Failed
					sql += " , is_publishing = 'N'";
					sql += " , is_failed = 'Y'"; 													
					break;
				case "L"://Locked
					sql += " , is_publishing = 'Y'";												
					sql += " , is_failed = 'N'"; 
					break;
				case "T"://Testing																
				case "R"://Released															
				case "S"://Scheduled
					sql += " , is_publishing = 'N'";												
					sql += " , is_failed = 'N'"; 
					sql += " , status = " + dbm.param(status, DataType.TEXT);	
					break;
				default:
					sql += " , is_publishing = 'N'";												
					sql += " , is_failed = 'N'"; 
					sql += " , status = " + dbm.param(status, DataType.TEXT);						
					break;
			}
			
			sql += " where release_id = " + dbm.param(releaseID, DataType.TEXT);

			dbm.update(sql, conn);
			dbm.clear();
						
			/////////////////////////////////////////////////////////////////////////////////////////////
			//Update all task status under the current update release id
			sql = " update task_mst set" 
				+ " upd_date = SYSDATE, "
			    + " upd_by = " + dbm.param(principal.getUserCode(), DataType.TEXT) +",";
			    
			switch (status){
				case "F"://Failed	
				case "T"://Testing
					sql += " status = 'A' ";
					break;
				case "R"://Released
					sql += " status = 'R' ";
					break;
				case "S"://Scheduled
					sql += " status = 'S' ";
					break;
				case "L"://Locked
					sql += " status = 'L' ";
					break;
				default:	
					sql += " status = " +  dbm.param(status, DataType.TEXT);
					break;
			}
			
			sql += " where task_code in(select task_code from release_task where release_id = " + dbm.param(releaseID, DataType.TEXT)+") ";
			
			dbm.update(sql, conn);	
			conn.commit();
		} catch (SQLException e) {	
			Log.info("{ReleaseDetail UpdateStatus1} -----------> Exception & Rollback");
			Log.error(e);
			
			if(conn == null){
				Log.info("{ReleaseDetail UpdateStatus2} -----------------> Connection is null, can't Rollback");
			}else{
				conn.rollback();
			}
		} catch (Exception e) {		
			Log.info("{ReleaseDetail UpdateStatus3} -----------> Exception & Rollback");
			
			if(conn == null){
				Log.info("{ReleaseDetail UpdateStatus4} -----------------> Connection is null, can't Rollback");
			}else{
				conn.rollback();
			}
			
			Log.error(e);
			return false;
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return true;
	}

}
