package com.eab.dao.releases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.biz.beneIllus.BeneIllusTemplateMgr;
import com.eab.biz.products.ProductRateMgr;
import com.eab.common.Compressor;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.dynTemplate.DynData;
import com.eab.dao.needs.Needs;
import com.eab.dao.products.Products;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.products.CashAndBonusInfoObj;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ReleasePublish {
	

	public JSONObject getProductLineData(HttpServletRequest request, UserPrincipalBean principal, String comp_code) throws Exception {
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		JSONObject _result = new JSONObject();
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select" 
					   + " b.pl_code,"
					   + " b.name_desc,"
					   + " b.lang_code"
					   + " from sys_prod_line_mst a"
					   + " inner join"
					   + " sys_prod_line_name b"
					   + " on a.comp_code = b.comp_code"
					   + " and a.pl_code = b.pl_code"
					   + " where a.comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					   + " and a.status = 'A'"
					   + " and b.status = 'A'"
					   + " and b.lang_code = 'en'";//TODO Handle multiple language
			
		   rs = dbm.select(sql, conn);
			
		   int seq = 0;
		   JSONObject options = new JSONObject();
		   
		   if (rs != null) {
			   while (rs.next()) {
				   String plCode = rs.getString("pl_code");
				   String nameDesc = rs.getString("name_desc");
				   String langCode = rs.getString("lang_code");
				   
				   //Title
				   JSONObject title = new JSONObject();
				   title.put(langCode, nameDesc);
				   
				   //Option
				   JSONObject prodLine = new JSONObject();
				   prodLine.put("title", title);
				   prodLine.put("sequence", seq++);
				   		
				   options.put(plCode, prodLine);
				}
			}
		   
		   _result.put("options", options);
		   _result.put("type", "layout");
		   
		   return new JSONObject(_result.toString());
		} catch (Exception e) {
			Log.error("{ReleasePublish getProdctData} ----------->");
			Log.error(e);
			
			throw new Exception(e.toString());
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed()) {
				conn.close();
				conn = null;
			}
		}
	}	
	
	private void initJson(JsonObject result, String productLine){
		result.add("type", JsonConverter.convertObj("product"));
		result.add("status", JsonConverter.convertObj("A"));
		//result.add("compCode", JsonConverter.convertObj("HK"));
		result.add("planInd", JsonConverter.convertObj("B"));
				
		switch(productLine) {
			case "Whole Life" ://Whole Life
				result.add("allowClaims", JsonConverter.convertObj(true));   
				result.add("deathAggregateSeq", JsonConverter.convertObj(1));
				result.add("deathAggregateFunc", JsonConverter.convertObj("function(prod, para) { var sa = para.sumInsured; var riskSA = 0; riskSA = sa; return riskSA; }"));
				result.add("contractCode", JsonConverter.convertObj("PL7"));
				result.add("productLineSequence", JsonConverter.convertObj(1));
				break;
			case "Unit Link" ://Unit Link
			
				break;
			case "Saving" ://Saving
			
				break;
			case "Critical Illness" ://Critical Illness
			
				break;
			case "TERM" ://Term
				
				break;
			case "AD" ://Accidental & Disability
				
				break;
			case "Medical" ://Medical
				
				break;
			default:
		}
 
		//HardCode
		result.add("quotForm", JsonConverter.convertObj("SingleInsuredForm"));     	
		result.add("orderSeq", JsonConverter.convertObj(901));
		result.add("scrOrderSeq", JsonConverter.convertObj(904));
		result.add("showTotPrem", JsonConverter.convertObj("M"));
		result.add("tabletQuoteInd", JsonConverter.convertObj("Y"));
		result.add("tabletChartInd", JsonConverter.convertObj("N"));
		result.add("medInd", JsonConverter.convertObj("N"));

	}
	
	private List<Map<String, Object>> getList(ResultSet rs, HttpServletRequest request, Language langObj) throws Exception {
	    ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
	    String productLine = "";
	    		
	    while (rs.next()) {
	    	Map<String, Object> row = new HashMap<String, Object>();
	    	    	
	    	if(rs.getObject("PROD_LINE") != null && !rs.getObject("PROD_LINE").equals(""))
	    		productLine = rs.getObject("PROD_LINE").toString();
	    	
	    	if(request != null){				
				row.put("productLine",langObj.getNameDesc(productLine));
	    	}else{
	    		row.put("productLine",productLine);
	    	}
	    	
			row.put("covCode", rs.getObject("PROD_CODE"));			
			row.put("covName", rs.getObject("name_desc"));
			row.put("covNameOth",rs.getObject("name_desc"));
			row.put("effDate",rs.getObject("eff_date"));
 
			list.add(row);
	    }
	    
	    return list;
	}	

	public List<Map<String, Object>> getENVData(UserPrincipalBean principal, String radio) throws SQLException {
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select host,"//Host IP
					   + " port," 		//Port
					   + " db_name," 	//Database Name
					   + " db_login,"	//Database Login
					   + " db_password" //Database Password
					   + " from sys_env_mst"				   
					   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					   + " and env_id = " + dbm.param(radio, DataType.TEXT);
		
			rs = dbm.select(sql, conn);
					
			return getEnvList(rs);			
		} catch (Exception e) {
			Log.error("{ReleasePublish getENVData} ----------->");
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}
	
	private List<Map<String, Object>> getEnvList(ResultSet rs) throws SQLException {
	    ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
    
	    while (rs.next()) {
	    	Map<String, Object> row = new HashMap<String, Object>();
					
			row.put("host", rs.getObject("host"));			
			row.put("port", rs.getObject("port"));
			row.put("dbName",rs.getObject("db_name"));
			row.put("dbLogin",rs.getObject("db_login"));
			row.put("dbPassword",rs.getObject("db_password"));
 
			list.add(row);
	    }
	    
	    return list;
	}	
	
	public List<Map<String, Object>> getMasterData(UserPrincipalBean principal) throws SQLException {
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select a.* "
					   + " from prod_publish a"
				       + " inner join prod_mst b"
				       + " on b.comp_code = a.comp_code"
				       + " and b.prod_code = a.prod_code"
				       + " and b.version = a.version"				   
					   + " where a.comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT);
			
			rs = dbm.select(sql, conn);
			
			return getMasterList(rs);			
		} catch (Exception e) {
			Log.error("{ReleasePublish getMasterData} ----------->");
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}	
		
	private List<Map<String, Object>> getMasterList(ResultSet rs) throws SQLException {
	    ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

	    while (rs.next()) {
	    	Map<String, Object> row = new HashMap<String, Object>();
					
			row.put("docid", rs.getObject("DOC_ID"));			
			row.put("json", rs.getObject("JSON_FILE"));
 
			list.add(row);
	    }
	    
	    return list;
	}	
	
	public List<Map<String, Object>> getScheduledList() throws SQLException {
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
				
		try {		
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select * from release_mst "
					   + " where status = 'S'"//Scheduled
					   + " and scheduled_release_date < SYSDATE";
	
			rs = dbm.select(sql, conn);
			
			return getCommonList(rs);			
		} catch (Exception e) {
			Log.error("{ReleasePublish getScheduledList} ----------->" );
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}
		
	private List<Map<String, Object>> getCommonList(ResultSet rs) throws SQLException {
	    ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

	    while (rs.next()) {
	    	Map<String, Object> row = new HashMap<String, Object>();
	    	ResultSetMetaData rsmd = rs.getMetaData();
	    	int columnCount = rsmd.getColumnCount();

	    	for (int i = 1; i <= columnCount; i++ ) {	    		
	    	  String name = rsmd.getColumnName(i);
	    	  row.put(name, rs.getObject(name));		
	    	}					
 
			list.add(row);
	    }
	    
	    return list;
	}	
	
	public List<Map<String, Object>> getProductionENVData() throws SQLException {
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select ENV_ID, "
					   + " COMP_CODE, "
					   + " ENV_NAME, "
					   + " HOST, "
					   + " PORT, "
					   + " DB_NAME, "
					   + " DB_LOGIN, "
					   + " DB_PASSWORD, "
					   + " STATUS "
					   + " from sys_env_mst "
					   + " where status = 'A'"
					   + " and allow_delete = 'N'"; //Get production record only
	
			rs = dbm.select(sql, conn);
				
			return getCommonList(rs);			
		} catch (Exception e) {
			Log.error("{ReleasePublish getMasterENVData} ----------->");
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}
		
	public List<Map<String, Object>> getTaskList(String compCode, String releaseID) throws SQLException {
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
				
		try {			
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select res.release_id,"
					   + " task.task_code,"
					   + " task.comp_code,"
					   + " task.item_code,"
					   + " task.ver_no,"
					   + " task.create_date,"
					   + " task.func_code,"
					   + " task.channel_code"
					   + " from task_mst task"
					   + " inner join release_task res"
					   + " on res.task_code = task.task_code"
					   + " where res.release_id = " + dbm.param(releaseID, DataType.TEXT)
					   + " and task.comp_code = "  + dbm.param(compCode, DataType.TEXT);	
			
			rs = dbm.select(sql, conn);
			
			return getCommonList(rs);			
		} catch (Exception e) {
			Log.error("{ReleasePublish getTaskList} ----------->");
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}
	
	public List<Map<String, Object>> getAttachLists(String module, String compCode, String docCode, String version) throws SQLException {
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
				
		try {			
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select att.module, "
					   + " att.doc_code, " 
					   + " att.version, "
					   + " att.att_id, "
					   + " att.att_file, "
					   + " att.status, "
					   + " att.comp_code "
					   + " from dyn_attachment_mst att"
					   + " where att.status = 'A'"
					   + " and att.comp_code = " + dbm.param(compCode, DataType.TEXT)
					   + " and att.doc_code = " + dbm.param(docCode, DataType.TEXT)
					   + " and att.version = " + dbm.param(version, DataType.TEXT)
					   + " and att.module = " + dbm.param(module, DataType.TEXT);
					

			rs = dbm.select(sql, conn);
			
			return getCommonList(rs);			
		} catch (Exception e) {
			Log.error("{ReleasePublish getAttachLists} ----------->");
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}	
		
		return null;
	}
	
	public Boolean UpdateDynPublish(String compCode, String productCode, String version, String json, String docID)throws Exception {
		Connection conn = null;
		DBManager dbm = null;
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			
			String sql = " begin"
					   		+ " INSERT INTO prod_publish"
					   		+ " VALUES (" + dbm.param(compCode, DataType.TEXT) + ", " + dbm.param(productCode, DataType.TEXT) + ", " + dbm.param(version, DataType.TEXT) + ", " + dbm.param(json, DataType.TEXT) + ", 'A', SYSDATE, '' , SYSDATE, '', " + dbm.param(docID, DataType.TEXT) + ");"
					   + " exception"
					   		+ " when dup_val_on_index then"
					   			+ " UPDATE prod_publish"
					   			+ " SET COMP_CODE = " + dbm.param(compCode, DataType.TEXT) + ","
					   			+ " PROD_CODE = " + dbm.param(productCode, DataType.TEXT) + ","
					   			+ " VERSION = " + dbm.param(version, DataType.TEXT) + ","
					   			+ " JSON_FILE = " + dbm.param(json, DataType.TEXT) + ","
					   			+ " STATUS = 'A',"
					   			+ " UPD_DATE = SYSDATE,"
					   			+ " DOC_ID = " + dbm.param(docID, DataType.TEXT)
					   			+ " WHERE COMP_CODE = " + dbm.param(compCode, DataType.TEXT) + ""
					   			+ " AND PROD_CODE = " + dbm.param(productCode, DataType.TEXT) + ""
					   			+ " AND VERSION = " + dbm.param(version, DataType.TEXT) + ";"
					   + " end;";
			
			dbm.update(sql, conn);
			Products prodDAO = new Products();
			String errMsg = prodDAO.updateProdMst(productCode,Integer.parseInt(version), compCode, conn);
			
			if(errMsg != null)
				conn.rollback();
			
			conn.commit();
			return true;
		} catch (Exception e) {
			Log.error("{ReleasePublish UpdateProductPublish} ----------->");
			Log.error(e);
			conn.rollback();
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return false;		
	}	

	public Boolean UpdateNeedsPublish(String compCode, String needCode, String version, String json, String docID, String channelCode)throws Exception {
		Connection conn = null;
		DBManager dbm = null;
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " begin"
					   		+ " INSERT INTO NEED_PUBLISH"
					   		+ " VALUES (" + dbm.param(compCode, DataType.TEXT) + ", " + dbm.param(needCode, DataType.TEXT) + ", " + dbm.param(version, DataType.TEXT) + ", " + dbm.param(channelCode, DataType.TEXT) + ", " + dbm.param(json, DataType.TEXT) + ", 'A', SYSDATE, '' , SYSDATE, '', " + dbm.param(docID, DataType.TEXT) + ");"
					   + " exception"
					   		+ " when dup_val_on_index then"
					   			+ " UPDATE NEED_PUBLISH"
					   			+ " SET COMP_CODE = " + dbm.param(compCode, DataType.TEXT) + ","
					   			+ " NEED_CODE = " + dbm.param(needCode, DataType.TEXT) + ","
					   			+ " VERSION = " + dbm.param(version, DataType.TEXT) + ","
					   			+ " JSON_FILE = " + dbm.param(json, DataType.TEXT) + ","
					   			+ " STATUS = 'A',"
					   			+ " UPD_DATE = SYSDATE,"
					   			+ " DOC_ID = " + dbm.param(docID, DataType.TEXT)
					   			+ " WHERE COMP_CODE = " + dbm.param(compCode, DataType.TEXT) + ""
					   			+ " AND NEED_CODE = " + dbm.param(needCode, DataType.TEXT) + ""
					   			+ " AND CHANNEL_CODE = " + dbm.param(channelCode, DataType.TEXT) + ""
					   			+ " AND VERSION = " + dbm.param(version, DataType.TEXT) + ";"
					   + " end;";
			
			dbm.update(sql, conn);
			
			Needs needDAO = new Needs();
			needDAO.updateNeedMst(needCode, Integer.parseInt(version), compCode, channelCode);
			return true;			
		} catch (Exception e) {
			Log.error("{ReleasePublish UpdateNeedsPublish} ----------->");
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return false;		
	}	
	
	public Boolean UpdateScheduleRelease(UserPrincipalBean principal, String releaseID, String scheduleDate) throws SQLException {
		Connection conn = null;
		DBManager dbm = null;
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			scheduleDate = scheduleDate.replace("T", " ");
			scheduleDate = scheduleDate.substring(0,19);
			
			String sql = " update release_mst set "
					   + " is_failed = 'N', "
					   + " status = 'S',"
					   + " upd_date = SYSDATE, "
					   + " upd_by = " + dbm.param(principal.getUserCode(), DataType.TEXT);
				   
			if(scheduleDate != null && !scheduleDate.equals(""))
				sql += " , scheduled_release_date = to_date( " + dbm.param(scheduleDate, DataType.TEXT) + ", 'yyyy-MM-dd hh24:mi:ss')";

			sql += " where release_id = " +  dbm.param(releaseID, DataType.TEXT);
					   
			dbm.update(sql, conn);
			dbm.clear();		
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
			sql = " update task_mst set  "
				+ " status = 'S'," 	//Scheduled
				+ " upd_date = sysdate,"
				+ " upd_by = "+ dbm.param(principal.getUserCode(), DataType.TEXT)
				+ " where task_code in (select task_code from release_task where release_id = " +  dbm.param(releaseID, DataType.TEXT) + ")";

			dbm.update(sql, conn);
			dbm.clear();
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
		} catch (Exception e) {			
			Log.error("{ReleasePublish UpdateScheduleRelease} ----------->");
			Log.error(e);
			return false;
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return true;
	}

	public Boolean AddHistory(String releaseID, String envID, String isSuccess, String reason, String user)throws Exception {
		Connection conn = null;
		DBManager dbm = null;		
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			if(reason == null)
				reason = " ";
			
			String sql = " insert into release_history(RELEASE_ID,PUBLISH_DATE,ENV_ID,IS_SUCCESS, FAILED_REASON, CREATE_DATE, CREATE_BY) values"
					   + " ( " + dbm.param(releaseID, DataType.TEXT) + "," 		//Release ID
					   + " SYSDATE,"											//Publish Date
					   + " " + dbm.param(envID, DataType.TEXT) + ","			//ENV ID
					   + " " + dbm.param(isSuccess, DataType.TEXT) + ","		//Is Success	
					   + " " + dbm.param(reason, DataType.TEXT) + ","			//Failed Reason
					   + " SYSDATE,"											//Create Date
					   + " " + dbm.param(user, DataType.TEXT) + " )";			//Create By
			
			dbm.update(sql, conn);
			
			return true;			
		} catch (Exception e) {
			Log.error("{ReleasePublish AddHistory} -----------> Exception");
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return false;		
	}			

	public Map<String, Object> getDummyJson(String doc_id) throws SQLException {
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select json"
					   + " from dummy_json"			   
					   + " where doc_id = " + dbm.param(doc_id, DataType.TEXT);
		
			rs = dbm.select(sql, conn);
			
			Map<String, Object> row = new HashMap<String, Object>();
		    
			boolean hasRecord = false;
			
			while (rs.next()) {
		    	row.put("json", rs.getObject("json"));
		    	hasRecord = true;
		    }
					
			if (hasRecord)
				return row;
			else 
				return null;
		} catch (Exception e) {
			Log.error("{ReleasePublish getDummyJson} ----------->");
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}
	
	public Map<String, JSONObject> getNeedData(int task_code) throws SQLException{
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		Map<String, JSONObject> result = null;		
		
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select section_code, json_file"
					   + " from need_section a"		
					   + " inner join task_mst b"
					   + " on a.comp_code = b.comp_code"
					   + " and a.need_code = b.item_code"
					   + " and a.version = b.ver_no"
					   + " and a.channel_code = b.channel_code"
					   + " and b.task_code = " + dbm.param(task_code, DataType.INT);
					   
			rs = dbm.select(sql, conn);
			
			if (rs != null) {
				result = new HashMap<String, JSONObject>();
				
				while (rs.next()) {
					result.put(rs.getString("section_code"), new JSONObject(rs.getString("json_file")));
				}
			}
					
			return result;			
		} catch (Exception e) {
			Log.error("{ReleasePublish getNeedData} ----------->");
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}
		
	public Map<String, String> getTranlationValue(String name_code, String comp_code) throws SQLException{
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		Map<String, String> result = null;
		String sql = "";
		String[] lang = null;
		String lang_code = "";
		
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			sql = " select language "
				+ " from sys_company_mst "
				+ " where comp_code = " + dbm.param(comp_code, DataType.TEXT);
			
			rs = dbm.select(sql, conn);
			
			while (rs.next()) {
				lang = rs.getString("language").split(",");
			}
			
			for (String _lang: lang){
				lang_code += "'" + _lang + "',";
			}
				
			if (lang_code.endsWith(","))
				lang_code = lang_code.substring(0, lang_code.length() - 1) + "";
			
			dbm.clear();
			
			sql = " select lang_code, name_desc"
			    + " from sys_name_mst"		  
			    + " where name_code = " + dbm.param(name_code, DataType.TEXT)
			    + " and lang_code in ("+lang_code+")";
					   
			rs1 = dbm.select(sql, conn);
			
			if (rs1 != null) {
				result = new HashMap<String, String>();
				
				while (rs1.next()) {
					result.put(rs1.getString("lang_code"), rs1.getString("name_desc"));
				}
			}
					
			return result;			
		} catch (Exception e) {
			Log.error("{ReleasePublish getTranlationValue} ----------->");
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			rs1 = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}
	
	public List<Map<String, Object>> getNeedAttachLists(String compCode, String needCode, String verNo, String channelCode) throws SQLException {
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
				
		try {			
			dbm = new DBManager();
			conn = DBAccess.getConnection();
						
			String sql = " select lang_code,"
					   + " attach_file,"
					   + " attach_code,"
					   + " section_code"
					   + " from need_attach"
					   + " where status = 'A'"
					   + " and comp_code = " + dbm.param(compCode, DataType.TEXT)
					   + " and need_code = " + dbm.param(needCode, DataType.TEXT)
					   + " and version = " + dbm.param(verNo, DataType.TEXT)
					   + " and channel_code = " + dbm.param(channelCode, DataType.TEXT);
			
			rs = dbm.select(sql, conn);
			
			return getCommonList(rs);			
		} catch (Exception e) {
			Log.error("{ReleasePublish getNeedAttachLists} ----------->");
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}	
		
		return null;
	}
	
}
