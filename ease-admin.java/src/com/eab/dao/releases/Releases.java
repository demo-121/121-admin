package com.eab.dao.releases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import com.eab.common.Function;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.profile.UserPrincipalBean;

public class Releases {

	public ArrayList<Map<String, Object>> getReleaseList(HttpServletRequest request, UserPrincipalBean principal, String searchString, String statusFilter, String sortBy, String sortDir) throws SQLException {		
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
				
		try {				
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			rs = searchReleaseList(searchString, statusFilter, sortBy, sortDir, dbm, principal, conn);
			return getList(rs, request, principal);			
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}

	public ResultSet searchReleaseList(String searchString, String statusFilter, String sortBy, String sortDir, DBManager dbm, UserPrincipalBean principal, Connection conn)throws Exception {
		String sql = " select release_id,"
				   + " release_name,"
				   + " status,"
				   + " target_release_date,"
				   + " scheduled_release_date,"
				   + " upd_date,"
				   + " task_count,"
				   + " is_failed,"
				   + " is_publishing," 			       
			       + " failed_reason "
				   + " from v_release"
				   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT) 
				   + " and status != 'D'";//Deleted
		
		//Task status
		if(statusFilter != null && !statusFilter.equals("") && !statusFilter.equals("E") ){
			if(!statusFilter.equals("*"))
				sql += " and status = " + dbm.param(statusFilter, DataType.TEXT);
		}else
			sql += " and status != 'R' "; //Show all record except released

		//Searching criteria by user input
		if(searchString != null && !searchString.equals("")){
			searchString = '%' + searchString.toUpperCase() +'%';

			sql += " and (upper(release_id) like "+ dbm.param(searchString, DataType.TEXT)
				+ " or upper(release_name) like " + dbm.param(searchString, DataType.TEXT) +")";
		}
		
		if (!sortBy.equals("")){
			if (sortDir.equals("A")) 
				sortDir = "asc";
			else
				sortDir = "desc";
			
			if (sortBy.equals("upd_date"))
				sortBy = "upd_datetime";
						
			sql += " order by " + sortBy + " " + sortDir;	
		} else {
			//Default sorting
			sql += " order by upd_date desc";
		}			

		return dbm.select(sql, conn);
	}

	private ArrayList<Map<String, Object>> getList(ResultSet rs, HttpServletRequest request, UserPrincipalBean principal) throws SQLException {
	    ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
	    String status = "";
	    		    
	    Language langObj = new Language(principal.getUITranslation());
	    		
	    String target_release_date = "";
	    String scheduled_release_date = "";
	    String released_date = "";
	    SimpleDateFormat dateformatter;
	    SimpleDateFormat timeformatter;
	    SimpleDateFormat dateTimeformatter;
	    
		dateformatter = Function.getDateFormat(principal);
		timeformatter = Function.getTimeFormat(principal);
		dateTimeformatter = Function.getDateTimeFormat(principal);
		//TODO: alert user if base version diff is found  		 	    
		//ReleaseDetail releaseDAO = new ReleaseDetail (); 
		//Map<String, Integer> mstVerMap = releaseDAO.getMstVerMap();
		//Map<String, Integer> trxVerMap= releaseDAO.getTrxVerMap();
	    while (rs.next()) {
	    	Map<String, Object> row = new HashMap<String, Object>();
	    	
	    	if(rs.getObject("status") != null && !rs.getObject("status").equals(""))
	    		status = rs.getObject("status").toString();
	    		
			row.put("release_ID", rs.getObject("target_release_date"));
			row.put("release_Name",rs.getObject("scheduled_release_date"));
			row.put("statusCode",rs.getObject("upd_date"));
			row.put("isPublishing",rs.getObject("is_Publishing"));
	    				
			try {
				target_release_date = dateformatter.format(rs.getDate("target_release_date"));		
			} catch (Exception e) {}
			
			try {
				scheduled_release_date = dateTimeformatter.format(rs.getObject("scheduled_release_date"));	
			} catch (Exception e) {}
			
			try {
				released_date = dateTimeformatter.format(rs.getDate("upd_date"));	
			} catch (Exception e) {}
						
			row.put("release_ID", rs.getObject("RELEASE_ID"));
			row.put("release_Name",rs.getObject("RELEASE_NAME"));
			row.put("statusCode",rs.getObject("status"));
			
			if(rs.getObject("scheduled_release_date") != null && !rs.getObject("scheduled_release_date").equals(""))
				row.put("scheduledDate",dateformatter.format(rs.getObject("scheduled_release_date")));
			
			if(rs.getObject("scheduled_release_date") != null && !rs.getObject("scheduled_release_date").equals(""))
				row.put("scheduledTime",timeformatter.format(rs.getObject("scheduled_release_date")));
			
			row.put("id", rs.getObject("RELEASE_ID"));
			row.put("task_count", rs.getObject("task_count"));
		

//			Boolean isBaseDiff =false;
//			//TODO: need to handle multitime releaseDAO.getTaskList as it slow down performance 
//			ArrayList<Map<String, Object>> tasks = releaseDAO.getTaskList(request, principal,  rs.getString("RELEASE_ID"), "", "", langObj);
//			
//			for(int i=0;i<tasks.size();i++){
//				Map<String, Object> task = tasks.get(i);
//				String compCode = (String) task.get("comp_code");
//				String prodCode = (String) task.get("prod_code");
//				String version = (String) task.get("version");
//				String type = (String) task.get("type");  
//				int prodVer = -1; 
//				String prefix = "";
//				switch (type){
//					case "Products":
//						prefix  = "prod";
//						break;
//					case "Needs":
//						prefix  = "need";
//						break;
//					case "Benefit Illustrations":
//						prefix  = "bi";
//						break;
//					 	 
//				}
//				 
//				String prodVerKey = prefix+"_"+ compCode+"_"+prodCode;
//				if( mstVerMap.get(prodVerKey) != null){
//					prodVer = mstVerMap.get(prodVerKey);
//					 
//				}
//				 
//				int trxVer = -1;
//				String trxVerKey = prefix+"_"+ compCode+"_"+prodCode+"_"+version;
//				if(  trxVerMap.get(trxVerKey) != null){
//					trxVer = trxVerMap.get(trxVerKey);
//					 
//				}
//				 
//				if(trxVer>0 && trxVer != prodVer)
//					isBaseDiff=true; 
//			}
//			row.put("isBaseDiff", isBaseDiff);
					
			 
			
			Map<String, String> icon = new HashMap<String , String>();
			
			if(rs.getObject("is_Publishing").toString().equals("Y")){
				icon.put("icon", "cloud_upload");
				icon.put("color", "Colors.green500");
				row.put("font_icon", icon);
			}else if(rs.getObject("is_Failed").toString().equals("Y")){
				icon.put("icon", "error");
				icon.put("color", "Colors.red500");
				row.put("font_icon", icon);
				row.put("tooltipId", rs.getObject("failed_reason"));
			}
						
			switch (status) {
				case "P":  //Pending
					row.put("status", langObj.getNameDesc("TARGET_RELEASE_FOR").toString() + " " + target_release_date + " [" + langObj.getNameDesc("PENDING").toString() +"]");
					break;
				case "T":  //Testing
					row.put("status", langObj.getNameDesc("TARGET_RELEASE_FOR").toString() + " " + target_release_date + " [" + langObj.getNameDesc("TESTING").toString() +"]");
					break;
				case "S":  //Scheduled							
					row.put("status", langObj.getNameDesc("SCHEDULED_TO_BE").toString() + " " + scheduled_release_date);
					break;
				case "R":  //Released
					row.put("status",langObj.getNameDesc("RELEASED_ON").toString() + " " + released_date);
					break;
				default: 
					row.put("status", "");
					break;
			}
				     
			list.add(row);
	    }
	    
	    return list;
	}
	
	public String addRelease(UserPrincipalBean principal, String releaseName, Date targetDate , String remarks)throws Exception {
		String releaseID = "";
		Connection conn = null;
		DBManager dbm = null;
		String sql = "";
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			java.sql.Date _targetDate = new java.sql.Date(targetDate.getTime());
			
			sql = " insert into release_mst(COMP_CODE, RELEASE_NAME, TARGET_RELEASE_DATE, SCHEDULED_RELEASE_DATE ,REMARKS, STATUS, CREATE_DATE, CREATE_BY, UPD_DATE, UPD_BY) values ("
					+ dbm.param(principal.getCompCode(), DataType.TEXT) + "," 						//company code
					+ dbm.param(releaseName, DataType.TEXT) + ","									//release name
					+ dbm.param(_targetDate, DataType.DATE) + "," 									//task release date
					+ "NULL,"																		//scheduled release date
					+ dbm.param(remarks, DataType.TEXT) + ","										//remarks
					+ dbm.param("P", DataType.TEXT)		 											//status
					+ " , SYSDATE "																	//create date
					+ " , "  + dbm.param(principal.getUserCode(), DataType.TEXT)					//create by
					+ " , SYSDATE "																	//update date
					+ " , "  + dbm.param(principal.getUserCode(), DataType.TEXT)					//update by
					+ ") ";
			
			dbm.update(sql, conn);
			dbm.clear();
			
			//TODO
			sql = " select release_id"
				+ " from release_mst"
				+ " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT) 
				+ " and release_name = " + dbm.param(releaseName, DataType.TEXT)
				+ " and target_release_date = " + dbm.param(_targetDate, DataType.DATE)
				//+ " and remarks = " + dbm.param(remarks, DataType.TEXT)
				+ " and status = 'P'"
				+ " and create_by = " + dbm.param(principal.getUserCode(), DataType.TEXT);
			    
			releaseID = dbm.selectSingle(sql, "release_id", conn);
			dbm.clear();
		} catch(Exception e) {
			Log.error("{Release addRelease} ----------->" + e);
			conn.rollback();
		} finally {
			dbm = null;
			
			if (conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
		return releaseID;
	}
	
	public Boolean abortRelease(List<Object> releaseIDList, UserPrincipalBean principal) throws SQLException {
		boolean output = false;
		Connection conn = null;
		DBManager dbm = null;
		String sql = "";
		
		if(releaseIDList.size() < 1)
			return output;
		
		try {
			dbm = new DBManager();
			
			//Get DB connection for update with begin transaction
			conn = DBAccess.getConnection();
		
			String _releaseIDList = "";
						
			for(int i = 0 ; i < releaseIDList.size(); i++){
				_releaseIDList += (String) releaseIDList.get(i) + ",";
			}
			
			//Remove the last comma from a string
			_releaseIDList = _releaseIDList.substring(0, _releaseIDList.length() - 1);
					
			sql = " update release_mst set"
				+ " status = 'P'," 	//Pending
				+ " upd_date = sysdate,"
				+ " upd_by = "+ dbm.param(principal.getUserCode(), DataType.TEXT)
				+ " where release_id in (";
			for (int i =0;i<releaseIDList.size();i++ ) {  
		  		sql=sql+" ? ";
	        	if(i<releaseIDList.size()-1){
	        		sql=sql+" , ";
	        	} 
		    }		 
		  	sql=sql+" ) ";
		  	 
			for (int i =0;i<releaseIDList.size();i++ ) {  
				dbm.param(Integer.parseInt((String)releaseIDList.get(i)), DataType.INT);
		    }	
			 			 

			dbm.update(sql, conn);
			dbm.clear();
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
			sql = " update task_mst set  "
				+ " status = 'A'," 	//Assigned
				+ " upd_date = sysdate,"
				+ " upd_by = "+ dbm.param(principal.getUserCode(), DataType.TEXT)
				+ " where task_code in (select task_code from release_task where release_id in ( "; 			 
		  	for (int i =0;i<releaseIDList.size();i++ ) {  
		  		sql=sql+" ? ";
	        	if(i<releaseIDList.size()-1){
	        		sql=sql+" , ";
	        	} 
		    }		 
		  	sql=sql+" ) )"; 
		  	 
			for (int i =0;i<releaseIDList.size();i++ ) {  
				dbm.param(Integer.parseInt((String)releaseIDList.get(i)), DataType.INT);		
	        	 
		    }	
			 			
			
			
			dbm.update(sql, conn);
			dbm.clear();
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			output = true;
		} catch(Exception e) {
			Log.error("{Release abortRelease} ----------->" + e);
			conn.rollback();
		} finally {
			dbm = null;
			
			if (conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
		return output;
	}		
	
	public Boolean deleteRelease(List<Object> releaseIDList, UserPrincipalBean principal) throws SQLException {
		boolean output = false;
		Connection conn = null;
		DBManager dbm = null;
		String sql = "";
		
		try {
			dbm = new DBManager();
			
			//Get DB connection for update with begin transaction
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
						
			String _releaseIDList = "";
			
			for(int i = 0 ; i < releaseIDList.size(); i++){
				_releaseIDList += (String) releaseIDList.get(i) + ",";
			}
			
			//Remove the last comma from a string
			_releaseIDList = _releaseIDList.substring(0, _releaseIDList.length() - 1);
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
			sql = " update release_mst set"
				+ " status = 'D'," 	//Deleted
				+ " upd_date = sysdate,"
				+ " upd_by = "+ dbm.param(principal.getUserCode(), DataType.TEXT)
				+ " where release_id in ("+_releaseIDList+")";
			
			dbm.update(sql, conn);
			dbm.clear();
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
			sql = " update task_mst set  "
				+ " status = 'U'," 	//Unassigned
				+ " upd_date = sysdate,"
				+ " upd_by = "+ dbm.param(principal.getUserCode(), DataType.TEXT)
				+ " where task_code in (select task_code from release_task where release_id in ("+_releaseIDList+"))";

			dbm.update(sql, conn);
			dbm.clear();
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
			sql = " delete from release_task"
				+ " where release_id in ("+_releaseIDList+")";
			
			dbm.update(sql, conn);	
			dbm.clear();
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			conn.commit();
			output = true;
		} catch(Exception e) {
			Log.error("{Release deleteRelease} ----------->" + e);
			conn.rollback();
		} finally {
			dbm = null;
			
			if (conn != null) {
				
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
		return output;
	}			

}
