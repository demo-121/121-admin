package com.eab.dao.beneIllus;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import java.util.Map;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.common.DynMenu;
import com.eab.dao.tasks.Tasks;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.database.jdbc.IOType;
import com.eab.json.model.BiListCondition;
import com.eab.json.model.Option;
import com.eab.model.beneIllus.BiBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.tasks.TaskBean;
import com.eab.webservice.dynamic.DynamicFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class BeneIllus {

	public final String NO_INSERT = "ERR_MSG.NO_INSERT";
	public final String NO_UPDATE = "ERR_MSG.NO_UPDATE";
	public final String DUPLICATE_CODE = "ERR_MSG.DUPLICATE_CODE";
	public final String CODE_WITHOUT_SPACE = "ERR_MSG.CODE_WITHOUT_SPACE";

	public List<BiBean> getBis(String biCode, String compCode, UserPrincipalBean principal) throws SQLException {
		ResultSet rs = null;
		List<BiBean> biBeans = new ArrayList<BiBean>();
		DBManager dbm;
		Connection conn = null;

		try {
			if (biCode != null && !biCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();

				//get data
				String sql = " select comp_code,"
						   + " bi_code,"
						   + " version,"
						   + " name_desc,"
						   + " default_name_desc,"
						   + " task_status "
						   + " from v_bi_trx "
				     	   + " where bi_code = " + dbm.param(biCode, DataType.TEXT)
						   + " and lang_code = " + dbm.param(principal.getLangCode(), DataType.TEXT) 
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " order by version";
				
				rs = dbm.select(sql, conn);
				
				if (rs != null) {
					biBeans =  new ArrayList<BiBean>();
					
					while (rs.next()) {
						BiBean biBean = new BiBean();
						biBean.setBiCode(rs.getString("bi_code"));
						biBean.setBiName((rs.getString("name_desc")==null)?rs.getString("default_name_desc"):rs.getString("name_desc"));
						biBean.setVersion(rs.getInt("version"));
						biBean.setTaskStatus(rs.getString("task_status"));
						biBeans.add(biBean);
					}
				}
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		
		return biBeans;
	}

	public String getBiImage(String biCode, int version, JSONObject images, String imageSrc, UserPrincipalBean principal) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;

		String compCode = principal.getCompCode();
		JSONArray imageItems = images.getJSONArray("items");
		String image = null;
		
		for (int i = 0; i < imageItems.length(); i++) {
			JSONObject imageItem = imageItems.getJSONObject(i);
			
			if (imageItem.has("image") && imageItem.has("id")) {
				if (imageItem.getString("id").equals(imageSrc)) {
					image = imageItem.getString("image");
					break;
				}
			}
		}
		
		try {
			if (biCode != null && !biCode.isEmpty() && compCode != null && !compCode.isEmpty() && image != null && !image.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				String sql = " select attach_file "
						   + " from bi_attach" 
						   + " where bi_Code = " + dbm.param(biCode, DataType.TEXT) 
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " and attach_code = " + dbm.param(image, DataType.TEXT) 
						   + " and version = " + dbm.param(version, DataType.INT) 
						   + " and status = 'A'";

				rs = dbm.select(sql, conn);
				dbm.clear();
				
				if (rs != null) {
					while (rs.next()) {
						Blob blob = (Blob)rs.getBlob("ATTACH_FILE");
						byte[] bdata = blob.getBytes(1, (int) blob.length());
						return new String(bdata);
					}
				}

			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}

		return null;
	}

	public boolean checkExist(String biCode, String compCode, int version) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		
		try {
			if (biCode != null && !biCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
	
				String sql = " select count(1) as cnt"
						   + " from bi_trx"
						   + " where comp_code = " + dbm.param(compCode, DataType.TEXT) 
						   + " and bi_code = " + dbm.param(biCode, DataType.TEXT) + "";
				
				if(version > 0)
					sql += " and version = " + dbm.param(version, DataType.INT);
				
				String cnt = dbm.selectSingle(sql, "cnt", conn);
				
				if (Integer.parseInt(cnt) > 0)
					return true;
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();

			conn = null;
		}

		return false;
	}

	public String clone(UserPrincipalBean principal, String biCode, String destCompCode) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		CallableStatement callableStatement = null;
		
		try {
			if (biCode != null && !biCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
				String sql = "call p_bi_clone(" + dbm.param(principal.getCompCode(), DataType.TEXT, IOType.IN) + "," + 
						dbm.param(destCompCode, DataType.TEXT, IOType.IN) + "," + 
						dbm.param(biCode, DataType.TEXT, IOType.IN) + "," + 
						dbm.param(principal.getUserCode(), DataType.TEXT, IOType.IN) + "," + 
						dbm.param(null, DataType.TEXT, IOType.OUT) + ")";
				
				List<Object> outData = dbm.executeCall(sql, conn);
				
				return (String) outData.get(0);
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			
			if (callableStatement != null)
				callableStatement.close();
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		
		return null;		
	}

	public int newVersion(UserPrincipalBean principal, String biCode) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		
		try {
			if (biCode != null && !biCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
				String sql = "call p_bi_new_version(" + dbm.param(principal.getCompCode(), DataType.TEXT, IOType.IN) + "," + 
						dbm.param(biCode, DataType.TEXT, IOType.IN) + "," + 
						dbm.param(principal.getUserCode(), DataType.TEXT, IOType.IN) + "," +
						dbm.param(null, DataType.INT, IOType.OUT) + ")";
				
				List<Object> rs= dbm.executeCall(sql, conn);
				
				return (Integer)rs.get(0);
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		
		return -1;
	}

	public List<Option> getBiStatus(Language langObj) {
		DBManager dbm;
		Connection conn = null;

		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			List<Option> optionList = new ArrayList<>();

			String sql = " select "
					   + " lookup_field,"
					   + " name_code "
					   + " from sys_lookup_mst"
					   + " where upper(lookup_key) = 'PRODUCT_STATUS'"
					   + " order by seq";
			
			ResultSet rs = dbm.select(sql, conn);

			if (rs != null) {
				while (rs.next()) {
					optionList.add(new Option(langObj.getNameDesc(rs.getString("name_code")), rs.getString("lookup_field")));
				}

				return optionList;
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				Log.error(e);
			}
			conn = null;
		}

		return null;
	}

	public List<BiBean> searchBiList(BiListCondition biListCondition, UserPrincipalBean principal, Language langObj) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;

		List<BiBean> biList = new ArrayList<>();

		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			String criteria = biListCondition != null ? biListCondition.getCriteria() : null;
			String status = biListCondition != null ? biListCondition.getStatus() : null;
			String sortBy = biListCondition != null ? biListCondition.getSortBy() : null;
			String sortDir = biListCondition != null ? biListCondition.getSortDir() : null;
			String typeFilter = biListCondition != null ? biListCondition.getTypeFilter() : "";
			Integer pageSize = biListCondition != null ? biListCondition.getPageSize() : null;

			String compCode = principal.getCompCode();
			String langCode = principal.getLangCode();

			// Condition
			String condition = " ";
			condition = DynamicFunction.addCompanyFilter(dbm, condition, null, compCode, "list");

			if (criteria != null && !criteria.isEmpty())
				condition += " and (UPPER(list.bi_code) like UPPER('%" + criteria + "%') or UPPER(name.name_desc) like UPPER('%" + criteria + "%'))";
			
			if (typeFilter != null && !typeFilter.isEmpty() && !typeFilter.equalsIgnoreCase("ALL"))
				condition += " and UPPER(list.channel_code) = UPPER('" + typeFilter + "')";
			
			if (status != null && !status.isEmpty() && !status.equalsIgnoreCase("ALL")) {
				if (status.equalsIgnoreCase("-R"))
					condition += " and UPPER(list.status_code) <> UPPER('" + status + "')";
				else
					condition += " and UPPER(list.status_code) = UPPER('" + status + "')";
			}

			// Order
			String order = "";
			if (sortBy != null && !sortBy.isEmpty()) {
				// Direction
				String dir = "";
				
				if (sortDir != null && !sortDir.isEmpty()) {
					switch (sortDir.toLowerCase()) {
					case "d":
						dir = " desc";
						break;
					case "a":
						dir = " asc";
						break;
					}
				}

				switch (sortBy.toLowerCase()) {
				case "bicode":
					sortBy = "bi_code";
					break;
				case "biname":
					sortBy = "bi_name";
					break;
				case "status":
					sortBy = "status";
					break;
				}

				order = " order by " + sortBy + dir;
			}

			String sql = " select list.bi_code,"
					   + " name.name_desc bi_name,"
					   + " name.default_name_desc bi_name_default,"
					   + " list.status,"
					   + " list.status_code,"
					   + " list.version"
					   
					   + " from v_bi_list list"
					   
					   + " left join v_bi_trx name"
					   + " on list.bi_code = name.bi_code"
					   + " and list.comp_code = name.comp_code"
					   + " and list.version = name.version"
					   + " and name.lang_code = '" + langCode + "'"
					   
					   + " where 1=1 " + condition + order;
			
			rs = dbm.select(sql, conn);

			if (rs != null) {
				while (rs.next()) {
					BiBean bi = new BiBean();

					bi.setId(rs.getString("bi_code"));
					bi.setBiCode(rs.getString("bi_code"));
					bi.setBiName((rs.getString("bi_name")==null)?rs.getString("bi_name_default"):rs.getString("bi_name"));
					bi.setStatus(langObj.getNameDesc(rs.getString("status")));
					bi.setVersion(rs.getInt("version"));
					bi.setIsNewVersion(rs.getString("status_code").equalsIgnoreCase("e"));
					bi.setIsClone(true);

					biList.add(bi);
				}
			}

		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}

		return biList;
	}
	
	public JsonArray getMenu(String biCode, UserPrincipalBean principal, int version, boolean isNewBi) throws SQLException{
		JsonArray menuLists = DynMenu.getMenu(null, "BI", null, principal, null, version);
		//if new bi, disabled all menu item except task info and detail
		if(isNewBi){
			for(int i=0; i<menuLists.size(); i++){
				JsonArray menuList = menuLists.get(i).getAsJsonArray();
				for(int j=0; j<menuList.size(); j++){
					if(!(i==0 && j<=1)){
						JsonObject menuItem = menuList.get(j).getAsJsonObject();
						menuItem.addProperty("disabled", true);
					}					
				}
			}
		}

		return menuLists;
	}

	public Map<String, Object> getBiStatusById(String biCode, int version, UserPrincipalBean principal) throws SQLException{
		Map<String, Object> result = null;
		DBManager dbm;
		Connection conn = null;

		try {
			String compCode = principal.getCompCode();
			if (biCode != null && !biCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
				String sql = " select status_code,"
						   + " version"
						   + " from v_bi_list"
						   + " where bi_code = " + dbm.param(biCode, DataType.TEXT)
						   + " and comp_code=" + dbm.param(compCode, DataType.TEXT);
				
				ResultSet rs = dbm.select(sql, conn);
				dbm.clear();
				
				if(rs.next()){
					result = new HashMap<String, Object>();
					result.put("status",rs.getString("status_code"));
					result.put("bi_ver", rs.getInt("version"));
					
					sql = " select launch_status"
						+ " from bi_trx"
						+ " where bi_code = " + dbm.param(biCode, DataType.TEXT)
						+ " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						+ " and version =" + dbm.param(version, DataType.INT);
					
					String launchStatus = dbm.selectSingle(sql, "launch_status");
				
					if(launchStatus!=null && !launchStatus.equals(""))
						result.put("launch_status", launchStatus);
					
					Tasks taskDao = new Tasks();
					Map<String, Object> taskBean = taskDao.getTaskById(-1, biCode, version, Tasks.FuncCode.BI, null, principal);
					
					if (taskBean.containsKey("statusCode"))
						result.put("task_status", taskBean.get("statusCode"));
				}
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}

		return result;
	}
	
	public String saveBi(String biCode, int version, TaskBean taskInfo, JSONObject biDetail, Map<String, JSONObject> sections, JSONObject attachItems, int taskId, UserPrincipalBean principal)  throws SQLException{
		String errMsg = null;
		Connection conn = null;
		Language langObj = new Language(principal.getUITranslation());
		
		try {
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			Tasks taskMgr = new Tasks();
			if (taskInfo != null) {
				if (taskInfo.getTaskCode() == null || taskInfo.getTaskCode().equals("")) {
					if (!taskMgr.addTask(principal, taskInfo, conn)) {
						conn.rollback();
						conn.commit();
						return langObj.getNameDesc(NO_INSERT);
					}
				} else {
					if (!taskMgr.updateTask(principal, taskInfo, conn)) {
						conn.rollback();
						conn.commit();
						return langObj.getNameDesc(NO_UPDATE);
					}
				}
			}
			if (biDetail != null) {
				errMsg = saveBiDetail(taskId, biDetail, principal, conn);
				
				if(errMsg != null){
					conn.rollback();
					conn.commit();
					return errMsg;
				}
			}
			if (sections != null) {
				for (String key : sections.keySet()) {
					JSONObject section = sections.get(key);
					errMsg = saveBiSection(biCode, version, key, section, attachItems, principal, conn);

					if (errMsg != null) {
						conn.rollback();
						conn.commit();
						return errMsg;
					}
				}
			}
			conn.commit();
			return null;
		}catch (Exception e) {
			if (conn != null && !conn.isClosed())
				conn.rollback();
			
			return e.getMessage();
		} finally {
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
	}
	
	private String saveBiDetail(int taskId, JSONObject biDetail, UserPrincipalBean principal, Connection conn) {
		DBManager dbm;
		String biCode = biDetail.getString("biCode");
		String compCode = principal.getCompCode();
		int version = biDetail.getInt("version");
		Language langObj = new Language(principal.getUITranslation());

		try {
			boolean isWhiteSpace = Pattern.compile("\\s").matcher(biCode).find();
	
			if(isWhiteSpace)
				return langObj.getNameDesc(CODE_WITHOUT_SPACE);
	
			String orgBiCode = biCode;
			
			if (taskId > -1) {
				Tasks tasksDAO = new Tasks();
				Map<String, Object> task = tasksDAO.getTaskById(taskId, null, -1, null, null, principal);
				
				if (task == null) 
					return langObj.getNameDesc(NO_UPDATE);
		
				orgBiCode = (String) task.get("item_code");
			}


			if (biCode != null && !biCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				
				if (!orgBiCode.equals(biCode)) {
					//check biCode is unique
					if (checkExist(biCode, compCode, -1))
						return langObj.getNameDesc(DUPLICATE_CODE);
					
					String sql = "";
					
					sql = "update task_mst set item_code = " + dbm.param(biCode, DataType.TEXT) + " where item_code = " + dbm.param(orgBiCode, DataType.TEXT);
					dbm.update(sql);
					dbm.clear();
					
					sql = "update bi_trx set bi_code = " + dbm.param(biCode, DataType.TEXT) + " where bi_code = " + dbm.param(orgBiCode, DataType.TEXT);
					dbm.update(sql);
					dbm.clear();
					
					sql = "update bi_section set bi_code = " + dbm.param(biCode, DataType.TEXT) + " where bi_code = " + dbm.param(orgBiCode, DataType.TEXT);
					dbm.update(sql);
					dbm.clear();
					
					sql = "update bi_name set bi_code = " + dbm.param(biCode, DataType.TEXT) + " where bi_code = " + dbm.param(orgBiCode, DataType.TEXT);
					dbm.update(sql);
					dbm.clear();
					
					sql = "update bi_attach set bi_code = " + dbm.param(biCode, DataType.TEXT) + " where bi_code = " + dbm.param(orgBiCode, DataType.TEXT);
					dbm.update(sql);
					dbm.clear();
				}

				if (checkExist(biCode, principal.getCompCode(), version)) {
					// update bi_trx record
					String sql = " update bi_trx set"
							   + " bi_code = " + dbm.param(biCode, DataType.TEXT)+", "				//bi code
							   + " upd_date = CURRENT_TIMESTAMP,"									//update date
							   + " upd_by = " + dbm.param(principal.getUserCode(), DataType.TEXT) 	//update by
							   + " where comp_code = " + dbm.param(compCode, DataType.TEXT)		
							   + " and bi_code = " + dbm.param(biCode, DataType.TEXT)
							   + " and version = " + dbm.param((int) biDetail.getInt("version"), DataType.INT);
					
					if (dbm.update(sql, conn) < 1)
						return langObj.getNameDesc(NO_UPDATE);
				} else {
					// insert bi_trx record
					// check biCode is unique
					if (checkExist(biCode, compCode, -1))
						return langObj.getNameDesc(DUPLICATE_CODE);
					
					String sql = "insert into bi_trx values ("
							+ dbm.param(compCode, DataType.TEXT) + " , " 					//comCode
							+ dbm.param(biCode, DataType.TEXT) + " , "						//biCode
							+ dbm.param(version, DataType.INT) + " , "						//version
							+ "NULL, "														//base version
							+ "NULL, "														//launch_date
							+ dbm.param("P", DataType.TEXT) + " , "							//launch_status
							+ dbm.param("A", DataType.TEXT) + " , "							//status
							+ "CURRENT_TIMESTAMP, "											//create date
							+ dbm.param(principal.getUserCode(), DataType.TEXT)	+ " , "		//create by
							+ "CURRENT_TIMESTAMP, "											//update date
							+ dbm.param(principal.getUserCode(), DataType.TEXT)				//update by
							+ " ) "; 
					
					if (!dbm.insert(sql, conn))
						return langObj.getNameDesc(NO_INSERT);
				}

				// insert or update bi_name record
				dbm.clear();
				ResultSet rs = null;
				
				String sql = " select lang_code"
						   + " from bi_name"
						   + " where comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " and bi_code = " + dbm.param(biCode, DataType.TEXT)
						   + " and version = " + dbm.param(biDetail.getInt("version"), DataType.INT)
						   + " and name_type = 'BINAME'"
						   + " and status = 'A'";
				
				rs = dbm.select(sql, conn);

				List<String> mLangs = new ArrayList<String>();
				
				if (rs != null) {
					while (rs.next()) {
						mLangs.add(rs.getString("lang_code"));
					}
				}
				
				rs = null;
				JSONObject langs = biDetail.getJSONObject("biName");
				
				for (String key : langs.keySet()) {
					dbm.clear();
					
					if (mLangs.contains(key)) {
						sql = " update bi_name "
							+ " set name_desc = " + dbm.param(langs.getString(key), DataType.TEXT)	//langDesc
							+ " where comp_code = " + dbm.param(compCode, DataType.TEXT)				//comCode
							+ "	and bi_code = " + dbm.param(biCode, DataType.TEXT)					//biCode
							+ " and version = " + dbm.param(version, DataType.INT)					//version
							+ "	and name_type = " + dbm.param("BINAME", DataType.TEXT)				//name_type
							+ "	and lang_code = " + dbm.param(key, DataType.TEXT); 					//lang_code
					
						if (dbm.update(sql, conn) < 1)
							return langObj.getNameDesc(NO_UPDATE);
						
					} else if (!langs.getString(key).equals("")) {
						sql = "insert into bi_name values("
								+ dbm.param(compCode, DataType.TEXT) + " , "					//comCode
								+ dbm.param(biCode, DataType.TEXT) + " , "						//biCode
								+ dbm.param(version, DataType.INT) + " , "						//version
								+ dbm.param("BINAME", DataType.TEXT) + " , "					//name_type
								+ dbm.param(key, DataType.TEXT) + " , "							//lang_code
								+ dbm.param(langs.get(key).toString(), DataType.TEXT) + " , "	//langDesc
								+ dbm.param("A", DataType.TEXT) + " , "							//status
								+ "CURRENT_TIMESTAMP, "											//createDate
								+ dbm.param(principal.getUserCode(), DataType.TEXT) + " , "		//createBy
								+ "CURRENT_TIMESTAMP, "											//updDate
								+ dbm.param(principal.getUserCode(), DataType.TEXT)				//updBy
								+ " ) "; 
						
						if(!dbm.insert(sql, conn))
							return langObj.getNameDesc(NO_INSERT);
					}
				}
			}

		} catch (Exception e) {
			Log.error(e);
			return e.getMessage();
		} finally {
			dbm = null;
		}

		return null;
	}

	public JsonObject getBi(String biCode, UserPrincipalBean principal, int version, Language langObj) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;
		String compCode = principal.getCompCode();
		JsonObject biObj = new JsonObject();
		
		try {

			if (biCode != null && !biCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();

				String sql = " select bi_code,"
						   + " version"
						   + " from bi_trx "
						   + " where bi_code = " + dbm.param(biCode, DataType.TEXT)
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " and version = " + dbm.param(version, DataType.INT);
				rs = dbm.select(sql, conn);

				if (rs != null) {
					while (rs.next()) {

						biObj.add("biCode", JsonConverter.convertObj(rs.getString("bi_code")));
						dbm.clear();
						JsonObject names = JsonConverter.convertObj(getBiName(biCode, version, principal)).getAsJsonObject();
						biObj.add("biName", JsonConverter.convertObj(names));
						biObj.add("version", JsonConverter.convertObj(rs.getInt("version")));
						break;

					}
				}

			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}

		return biObj;
	}

	public JSONObject getBiName(String biCode, int version, UserPrincipalBean principal) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;
		JSONObject names = null;
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			names = new JSONObject();

			String sql = " select lang_code,"
					   + " name_desc"
					   + " from bi_name"
					   + " where bi_code = " + dbm.param(biCode, DataType.TEXT)
					   + " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					   + " and version = " + dbm.param(version, DataType.INT)
					   + " and name_type = " + dbm.param("BINAME", DataType.TEXT);

			rs = dbm.select(sql, conn);
		
			if (rs != null) {
				while (rs.next()) {
					names.put(rs.getString("lang_code"), rs.getString("name_desc"));
				}
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		
		return names;
	}

	private String saveBiSection(String biCode, int version, String sectionCode, JSONObject section, JSONObject attachItems, UserPrincipalBean principal, Connection conn) throws SQLException {
		DBManager dbm;
		Language langObj = new Language(principal.getUITranslation());
		try {
			String compCode = principal.getCompCode();

			if (biCode != null && !biCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				String jsonFile = section.toString();

				String sql = " select count(1) as cnt"
						   + " from bi_section"
						   + " where bi_code = " + dbm.param(biCode, DataType.TEXT) 
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT) 
						   + " and version = " + dbm.param(version, DataType.INT)
						   + " and section_code = " + dbm.param(sectionCode, DataType.TEXT)
						   + " and status = 'A'";

				int count = Integer.parseInt(dbm.selectSingle(sql, "cnt", conn));

				dbm.clear();

				if (count > 0) {
					sql = "update bi_section " + " set json_file=" + dbm.param(jsonFile, DataType.CLOB) + " , " + "   upd_date=CURRENT_TIMESTAMP, " + "   upd_by=" + dbm.param(principal.getUserCode(), DataType.TEXT) + " where bi_code=" + dbm.param(biCode, DataType.TEXT) + "   and comp_code=" + dbm.param(compCode, DataType.TEXT) + "   and version=" + dbm.param(version, DataType.INT) + "   and section_code=" + dbm.param(sectionCode, DataType.TEXT);

					count = dbm.update(sql, conn);

					if (count < 1)
						return langObj.getNameDesc(NO_UPDATE);

				} else {
					sql = "insert into bi_section values("
							+ dbm.param(compCode, DataType.TEXT) + " , " 						//comCode
							+ dbm.param(biCode, DataType.TEXT) + " , " 							//biCode
							+ dbm.param(version, DataType.INT) + " , " 							//version
							+ dbm.param(sectionCode, DataType.TEXT) + " , "						//name_type
							+ dbm.param(jsonFile, DataType.TEXT) + " , "						//json_file
							+ dbm.param("A", DataType.TEXT) + " , "								//status
							+ "CURRENT_TIMESTAMP, "  											//create_date
							+ dbm.param(principal.getUserCode(), DataType.TEXT) + " , "			//create_by
							+ "NULL, "															//upd_date
							+ "NULL)"; 															//upd_by

					if(!dbm.insert(sql, conn))
						return langObj.getNameDesc(NO_INSERT);
				}

				String errMsg = saveAttachmentsByKey(section, attachItems, biCode, version, sectionCode, principal, conn);

				if(errMsg != null)
					return errMsg;
			}
		} catch (Exception e) {
			return e.getMessage();
		} finally {
			dbm = null;
		}

		return null;
	}

	private String saveAttachmentsByKey(Object sectionJson, JSONObject attachJson, String biCode, int version, String sectionCode, UserPrincipalBean principal, Connection conn) throws SQLException{
		String[] attachKeys = null;
		String errMsg = null;
		
		switch (sectionCode) {
			case Constants.BiPage.BI_IMAGE:
				attachKeys = new String[]{"image"};
				break;
			default:
		}
		
		if(attachKeys != null){
			List<String> codeArray = new ArrayList<String>();
			errMsg = saveAttachmentsByKey(sectionJson, attachJson, attachKeys, biCode, version, sectionCode, codeArray, principal, conn);
		
			if (errMsg == null)
				errMsg = removeUselessAttachments(biCode, version, sectionCode, codeArray, principal, conn);
			
			return errMsg;
		}
		return null;

	}

	private String removeUselessAttachments(String biCode, int version, String sectionCode , List<String> codeArray, UserPrincipalBean principal, Connection conn) throws SQLException{
		DBManager dbm;

		try {
			String compCode = principal.getCompCode();
			
			if (biCode != null && !biCode.isEmpty() && compCode != null && !compCode.isEmpty() && sectionCode != null && !sectionCode.isEmpty() && codeArray.size() > 0) {
				dbm = new DBManager();
				String codes = "";
				
				for(int i=0; i<codeArray.size(); i++){
					if(i > 0)
						codes +=",";
					
					codes += "'"+codeArray.get(i)+"'";
				}
				
				String sql = " delete from bi_attach"
						   + " where bi_code = " + dbm.param(biCode, DataType.TEXT)
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " and version = " + dbm.param(version, DataType.INT)
						   + " and section_code = " + dbm.param(sectionCode, DataType.TEXT)
						   + " and attach_code not in (" + codes +")";
				
				dbm.delete(sql, conn);
			}
		} catch (Exception e) {
			return e.getMessage();
		} finally {
		}

		return null;
	}

	private String saveAttachmentsByKey(Object sectionJson, JSONObject attachJson, String[] attachKeys, String biCode, int version, String sectionCode , List<String> codeArray, UserPrincipalBean principal, Connection conn) throws SQLException{

		String errMsg = null;
		if (sectionJson instanceof JSONArray) {
			JSONArray jsa=(JSONArray)sectionJson;
			for(int i =0; i<jsa.length();i++){
				Object jso=jsa.get(i);
				errMsg = saveAttachmentsByKey(jso,  attachJson,  attachKeys, biCode, version,  sectionCode,  codeArray, principal, conn);
				
				if (errMsg != null)
					return errMsg;
			}			  
		} else if (sectionJson instanceof JSONObject) {
			JSONObject jso=(JSONObject)sectionJson;
			Iterator it= jso.keys();
			
			while (it.hasNext()){	
				String key=(String)it.next();
				Object obj=jso.get(key);
			
				if (obj instanceof JSONObject||obj instanceof JSONArray) {
					errMsg = saveAttachmentsByKey(obj,  attachJson,  attachKeys, biCode, version,  sectionCode,  codeArray, principal, conn);
					
					if (errMsg != null)
						return errMsg;
				} else if (obj instanceof String) { 						 
					if (Arrays.asList(attachKeys).contains(key)) {
						String attachCode=(String)obj;
						codeArray.add(attachCode);
						
						if (attachJson!=null&&attachJson.length()>0&&attachJson.has(attachCode)){ 							 
							errMsg = updateAttachFile(biCode, version, attachCode, attachJson.getString(attachCode), sectionCode, principal, conn);
						
							if (errMsg != null)
								return errMsg;
						}
					}	
				}  				  
			} 
		} 

		return null;
	}

	private String updateAttachFile(String biCode, int version, String attachCode, String attachFile, String sectionCode, UserPrincipalBean principal, Connection conn)  throws SQLException {
		DBManager dbm;
		Language langObj = new Language(principal.getUITranslation());
		
		try {
			String compCode = principal.getCompCode();
			if (biCode != null && !biCode.isEmpty() && compCode != null && !compCode.isEmpty() && attachCode != null && !attachCode.isEmpty() && sectionCode != null && !sectionCode.isEmpty()) {
				dbm = new DBManager();
				String jsonFile = attachFile.toString();
				String sql = "select count(1) as cnt from bi_attach " + "where bi_code=" + dbm.param(biCode, DataType.TEXT) + " and comp_code=" + dbm.param(compCode, DataType.TEXT) + " and version=" + dbm.param(version, DataType.INT) + " and section_code=" + dbm.param(sectionCode, DataType.TEXT) + " and attach_code=" + dbm.param(attachCode, DataType.TEXT) +" and status='A'";
				int count = Integer.parseInt(dbm.selectSingle(sql, "cnt", conn));

				dbm.clear();
				
				if (count > 0) {
					sql = "update bi_attach " + " set attach_file=" + dbm.param(jsonFile.getBytes(), DataType.BLOB) + " , " + "   upd_date=CURRENT_TIMESTAMP, " + "   upd_by=" + dbm.param(principal.getUserCode(), DataType.TEXT) + " where bi_code=" + dbm.param(biCode, DataType.TEXT) + "   and comp_code=" + dbm.param(compCode, DataType.TEXT) + "   and version=" + dbm.param(version, DataType.INT) + "   and section_code=" + dbm.param(sectionCode, DataType.TEXT)+ " and attach_code=" + dbm.param(attachCode, DataType.TEXT);

					count = dbm.update(sql, conn);
					
					if (count < 1)
						return langObj.getNameDesc(NO_UPDATE);
				} else {
					sql = "insert into bi_attach values("
							+ dbm.param(compCode, DataType.TEXT) + " , " 						//comCode
							+ dbm.param(biCode, DataType.TEXT) + " , " 							//biCode
							+ dbm.param(version, DataType.INT) + " , " 							//version
							+ dbm.param(sectionCode, DataType.TEXT) + " , "						//section_code
							+ dbm.param(attachCode, DataType.TEXT) + " , "						//attach_code
							+ dbm.param(jsonFile.getBytes(), DataType.BLOB) + " , "						//attach_file
							+ dbm.param("A", DataType.TEXT) + " , "								//status
							+ "CURRENT_TIMESTAMP, "  											//create_date
							+ dbm.param(principal.getUserCode(), DataType.TEXT) + " , "			//create_by
							+ "NULL, "															//upd_date
							+ "NULL)"; 															//upd_by
					
					if (!dbm.insert(sql, conn)) 
						return langObj.getNameDesc(NO_INSERT);
				}
			}
		} catch (Exception e) {
			return e.getMessage();
		} finally {
			dbm = null;
		}

		return null;
	}

	public Map<String, Object> getAttachments(String biCode, int version, UserPrincipalBean principal) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		Map<String, Object> attachItems = null;
		ResultSet rs = null;
		String compCode = principal.getCompCode();

		try {
			if (biCode != null && !biCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
				String sql = " select attach_code,"
						   + " attach_file"
						   + " from bi_attach"
						   + " where bi_Code = " + dbm.param(biCode, DataType.TEXT)
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " and version = " + dbm.param(version, DataType.INT)
						   + " and status='A' ";

				rs = dbm.select(sql, conn);
				dbm.clear();

				if (rs != null) {
					attachItems = new HashMap<String, Object>();
				
					while (rs.next()) {
						attachItems.put(rs.getString("attach_code"), (Blob)rs.getBlob("ATTACH_FILE"));
					}
				}

			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		return attachItems;
	}

	public Map<String, JSONObject> getBiSections(String biCode, UserPrincipalBean principal, int version, String sectionId) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		Map<String, JSONObject> sections = null;
		ResultSet rs = null;
		String compCode = principal.getCompCode();
		
		try {
			if (biCode != null && !biCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
				String sql = " select section_code,"
						   + " json_file "
						   + " from bi_section"
						   + " where bi_code = " + dbm.param(biCode, DataType.TEXT)
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " and version=" + dbm.param(version, DataType.INT)
						   + " and status='A'";
			
				if (sectionId != null && !sectionId.isEmpty())
					sql += " and section_code = " + dbm.param(sectionId, DataType.TEXT);
				
				rs = dbm.select(sql, conn);
				dbm.clear();
				
				if (rs != null) {
					sections = new HashMap<String, JSONObject>();
				
					while (rs.next()) {
						sections.put(rs.getString("section_code"), new JSONObject(rs.getString("json_file")));
					}
				}

			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		return sections;
	}

	public boolean updateBiLaunchStatus(Map<String,Object> release) throws SQLException{
		boolean success = false;
		DBManager dbm;
		Connection conn = null;

		try {
			String biCode = release.get("ITEM_CODE").toString();
			int version = Integer.parseInt(release.get("VER_NO").toString());
			String compCode = release.get("COMP_CODE").toString();

			dbm = new DBManager();
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);

			if (biCode != null && !biCode.isEmpty() && compCode != null && !compCode.isEmpty() && version >0) {
				String sql = "update bi_trx set launch_status = 'L' where bi_code= " + dbm.param(biCode, DataType.TEXT) + "and version = " + dbm.param(version, DataType.INT) + " and comp_code=" + dbm.param(compCode, DataType.TEXT);
				
				if(dbm.update(sql)>0){
					conn.commit();
					return true;
				}
				
				conn.rollback();
			}
		} catch (Exception e) {
			conn.rollback();
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}

		return success;
	}

}
