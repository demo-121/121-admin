package com.eab.dao.userroles;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.search.DateTerm;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Field;
import com.eab.json.model.SearchCondition;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.webservice.dynamic.DynamicFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class UserRolesDAO {
	private DBManager dbm = null;
	private Connection conn = null;
	private boolean newConn = false;
	
	String[] ids = null; 
	String[] keys = null;
	
	String[] rightIds = null;
	String[] rightKeys = null;

	public UserRolesDAO() {
		super();
		dbm = new DBManager();
		try {
			conn = DBAccess.getConnection();
			newConn = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public UserRolesDAO(Connection conn, String[] ids, String[] keys, String[] rids, String[] rkeys) {
		super();
		dbm = new DBManager();
		
		this.conn = conn;
		this.ids = ids;
		this.keys = keys;
		this.rightIds = rids;
		this.rightKeys = rkeys;
	}

	public DBManager getManager() {
		return dbm;
	}
	
	public JsonArray getRoleMaster(SearchCondition cond, String statusStr, String sortBy, String sortDir, JsonArray fields, DynamicUI ui, String compCode) throws Exception, SQLException {
		JsonArray result;
		ResultSet rs = null;
		String condition = "";

		if (cond.getCriteria() != null && !cond.getCriteria().isEmpty()) {
			condition = DynamicFunction.buildCondition(condition, "(role_code LIKE " + dbm.param("%"+cond.getCriteria()+"%", DataType.TEXT) + " OR role_name LIKE " + dbm.param("%"+cond.getCriteria()+"%", DataType.TEXT) + ")"); 
		}

		if (statusStr.equals("All")) {
		} else if (statusStr.equals("E")) {
			condition = DynamicFunction.buildCondition(condition, "(action = 'N' OR status <> 'D')");
		} else if (statusStr.equals("P")) {
			condition = DynamicFunction.buildCondition(condition, "action = 'N'");
		} else { // A and D
			condition = DynamicFunction.buildCondition(condition, "(action <> 'N' OR action is null) AND status = " + dbm.param(statusStr, DataType.TEXT));
		}
		
		condition = DynamicFunction.addCompanyFilter(dbm, condition, ui, compCode, null);

		String sqlData = "SELECT * FROM V_USER_ROLE_MST " + condition + " ORDER BY " + sortBy +  " "+ ("A".equals(sortDir)?"ASC":"DESC");
		result = new JsonArray();
		rs = dbm.select(sqlData, conn);
		dbm.clear();

		if (rs != null) {
			while (rs.next()) {

				JsonObject rec = new JsonObject();
				rec.addProperty("id", rs.getString("role_code"));
				
				for (int i = 0; i < fields.size(); i++) {
					JsonObject item = fields.get(i).getAsJsonObject();
					if (item.has("key") && !item.get("key").isJsonNull()) {
						String key = item.get("key").getAsString();
						if ( rs.getString(key) != null) {
							String id = item.get("id").getAsString();
							rec.addProperty(id, rs.getString(key));
						}
					}
				}
				
				result.add(rec);
			}
			rs.close();
		}

		if (newConn) {
			this.conn.close();
			this.conn = null;
		}
		return result;
	}

	public List<Field> genRightMap(List<String> nameList) throws SQLException {
		List<Field> fields = new ArrayList<Field>();
		List<Field> rightGroup = new ArrayList<Field>();
//		String sql = "select func_code, name_code, upline_func_code, access_ctrl from SYS_FUNC_MST where STATUS = 'A' and (ACCESS_CTRL = 4 OR (ACCESS_CTRL = 5 AND UPLINE_FUNC_CODE in (select FUNC_CODE as UPLINE_FUNC_CODE from SYS_FUNC_MST where ACCESS_CTRL = 4 and status = 'A'))) order by disp_seq";
//		String sql = "select func_code, name_code, upline_func_code, access_ctrl, setable from SYS_FUNC_MST where STATUS = 'A' and (ACCESS_CTRL = 4 or ACCESS_CTRL = 5) order by disp_seq";
		String sql = "select func_code, name_code, upline_func_code, access_ctrl, setable from SYS_FUNC_MST where STATUS = 'A' and ACCESS_CTRL > 3 order by disp_seq";
		ResultSet rs = null;

		rs = dbm.select(sql, conn);
		dbm.clear();

		if (rs != null) {
			Field field = null;

			Map<String, Field> fieldMap = new HashMap<String, Field>();
			
			while (rs.next()) {

				int level = rs.getInt("access_ctrl");
				String code = rs.getString("func_code");
				String name = rs.getString("name_code");
				String upline = rs.getString("upline_func_code");
				boolean setable = rs.getString("setable").equals("Y");

				if (nameList != null) {
					nameList.add(name);
				}
				
				if (level == 4) {
					if (fieldMap.containsKey(code)) {
						field = fieldMap.get(code);
						field.setTitle(name);
					} else {
						field = new Field(null, null, name);
						field.setItems(new ArrayList<Field>());
						fieldMap.put(code, field);
					}
					field.setAllowUpdate(setable);				
					fields.add(field);
				} else {
					if (fieldMap.containsKey(code)) {
						field = fieldMap.get(code);
						field.setTitle(name);
					} else {
						field = new Field(code, null, name);
						field.setItems(new ArrayList<Field>());
						fieldMap.put(code, field);
					}
					field.setAllowUpdate(setable);
					field.setDetailSeq(level);
					
					Field parent = null;
					if (upline != null && !upline.isEmpty()) {
						if (fieldMap.containsKey(upline)) {
							parent = fieldMap.get(upline);
						} else {
							parent = new Field(upline, null, "");
							parent.setItems(new ArrayList<Field>());						
							fieldMap.put(upline, parent);
						}
						parent.getItems().add(field);
//					} else {
//						fields.add(field);
					}
				};
			}
			rs.close();
			
			// loop thu fields to get the right
			for (int i = 0; i<fields.size(); i++) {
				field = fields.get(i);
				
				if (field.isAllowUpdate()) {
					Field rg = new Field(field.getId(), "hBox", field.getTitle());
	//				rg.setAllowUpdate(field.isAllowUpdate());
	
					// create a bag to collection the level 5
					rg.setItems(new ArrayList<Field>());
					checkLevel5(field, rg.getItems());	
					
					// add select all check box if rg.getItems().length >= 2
					if (rg.getItems().size() > 0) {
						Field all = new Field("sab"+field.getId(), "selectAllButton", "");
						String keys = "";
						for (Field f : rg.getItems()) {
							if (!f.isDisabled()) {
								keys += (keys.isEmpty()?"":",") + f.getId();
							}
						}
						all.setPresentation(keys);
						rg.getItems().add(all);
					}
					
					rightGroup.add(rg);
				}
			}
			
		}
		return rightGroup;
	}
	
	/*
	 * Only add level 5 to the bag
	 */
	private void checkLevel5(Field field, List<Field> bag) {
		for (int i = 0; i<field.getItems().size(); i++) {
			Field sf = field.getItems().get(i);
//			if (sf.isAllowUpdate()) {
			if (sf.getDetailSeq() == 5) {
				Field right = new Field(sf.getId(), "checkbox", sf.getTitle());
				right.setDisabled(!sf.isAllowUpdate());
				bag.add(right);
			}
			if (sf.getItems() != null) {
				checkLevel5(sf, bag);
			}
		}
			
	}
	

	public List<JsonObject> getRoleById(DynamicUI ui, JsonArray fields, String rid, String compCode) throws Exception {
		ArrayList<JsonObject> result = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		

		String acols = "";
		String cols = "";
		for (int i = 0; i<keys.length; i++) {
			cols += keys[i] + ", ";
			acols += "a."+ keys[i] + ", ";
		}
		
		String sqlData = "SELECT " +acols+ "'' AS action FROM "+ui.getDataTable()+" a"
				+ " WHERE role_code = "+ dbm.param(rid, DataType.TEXT)
				+ " and comp_code = "+ dbm.param(compCode, DataType.TEXT)
				+ " UNION SELECT "+cols+" action FROM "+ ui.getTempTable() 
				+ " WHERE role_code = "+dbm.param(rid, DataType.TEXT)
				+ " and comp_code = "+ dbm.param(compCode, DataType.TEXT);

		result = new ArrayList<JsonObject>();
		rs = dbm.select(sqlData, conn);
		dbm.clear();
		if (rs != null) {
			while (rs.next()) {
				JsonObject rec = new JsonObject();
				String action = rs.getString("action");
				String roleCode = rs.getString(ui.getPrimaryKey());
				rec.addProperty("id", roleCode);
				rec.addProperty("action", action);
				
				for (int i = 0; i < fields.size(); i++) {
					JsonObject item = fields.get(i).getAsJsonObject();
					
					if (item.has("key") && !item.get("key").isJsonNull()) {
						String key = item.get("key").getAsString();
						if ( rs.getString(key) != null) {
							String id = item.get("id").getAsString();
							rec.addProperty(id, rs.getString(key));
						}
					}
				}

				if (rs.getDate("create_date") != null) {
					rec.addProperty("createDate", rs.getDate("create_date").getTime());
				}
				if (rs.getDate("modify_date") != null) {
					rec.addProperty("modifyDate", rs.getDate("modify_date").getTime());
				}
				if (rs.getDate("approv_date") != null) {
					rec.addProperty("approvDate", rs.getDate("approv_date").getTime());
				}
				
				// get related user name
				Map<String, String> userMap = new HashMap<String, String>();
				Map<String, String> nameMap = new HashMap<String, String>();

				String createBy = rs.getString("create_by");
				if (createBy != null && !createBy.isEmpty()) {
					userMap.put("createBy", createBy);
					if (!nameMap.containsKey(createBy)) {
						nameMap.put(createBy, createBy);
					}
				}
				String modifyBy = rs.getString("modify_by");
				if (modifyBy != null && !modifyBy.isEmpty()) {
					userMap.put("modifyBy", modifyBy);
					rec.addProperty("updateBy", modifyBy); // for checking maker only
					if (!nameMap.containsKey(modifyBy)) {
						nameMap.put(modifyBy, modifyBy);
					}
				}
				String approveBy = rs.getString("approv_by");
				if (approveBy != null && !approveBy.isEmpty()) {
					userMap.put("approvBy", approveBy);
					if (!nameMap.containsKey(approveBy)) {
						nameMap.put(approveBy, approveBy);
					}
				}
				if (userMap.size() > 0) {
					String names = "";
					for (String name : nameMap.keySet()) {
						names += (names.isEmpty()?"":", ")+dbm.param(name, DataType.TEXT);
					}
					sqlData = "SELECT user_code, user_name FROM sys_user_mst WHERE user_code in ("+ names +")";
					ResultSet rs2 = dbm.select(sqlData, conn);
					dbm.clear();
					while (rs2.next()) {
						nameMap.put(rs2.getString(1), rs2.getString(2));
					}
					for (Map.Entry<String,String> item : userMap.entrySet()) {
						rec.addProperty(item.getKey(), nameMap.get(item.getValue()));
					}
					rs2.close();
				}
				
				JsonObject rights = new JsonObject();
				if (action == null || action.isEmpty()) {
					// mst
					sqlData = "SELECT func_code FROM sys_user_role_func WHERE role_code = " + dbm.param(roleCode, DataType.TEXT)
					+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
				} else {
					// tmp
					sqlData = "SELECT func_code FROM sys_user_role_func_tmp WHERE role_code = " + dbm.param(roleCode, DataType.TEXT)
					+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
				}
				rs1 = dbm.select(sqlData, conn);
				dbm.clear();
				while (rs1.next()) {
					String rfFuncCode = rs1.getString("func_code");
					if (!rfFuncCode.isEmpty()) {
						rights.addProperty(rfFuncCode, "Y");
					}
				}
				rec.add("rights", rights);
				result.add(rec);
			}
			rs.close();
		}

		if (newConn) {
			this.conn.close();
			this.conn = null;
		}
		return result;
	}
	
	public String getRoleAction(String rid, String compCode) throws Exception {
		String action = null;
		ResultSet rs = null;

		String sqlData = "SELECT action FROM sys_user_role_mst_tmp WHERE role_code = "+ dbm.param(rid, DataType.TEXT)
		+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);

		rs = dbm.select(sqlData, conn);
		dbm.clear();
		if (rs != null) {
			while (rs.next()) {
				action = rs.getString("action");
			}
			rs.close();
		}

		if (newConn) {
			this.conn.close();
			this.conn = null;
		}
		return action;
	}

	
	public boolean deleteRole(String rid, String compCode, String userCode) throws Exception {

		String cols = "";
		for (int i = 0; i<keys.length - 4; i++) {
			cols += keys[i] + ", ";
		}
		
		Date now = new Date(new java.util.Date().getTime());
		
		//insert a row from mst to tmp
		String insertCols = "(" + cols + "modify_date, modify_by, action)";
		String selectSql = "select " + cols + dbm.param(now, DataType.DATE)+" AS modify_date, "+dbm.param(userCode, DataType.TEXT)+" AS modify_by, "+dbm.param("D", DataType.TEXT)+" AS action "
				+ "from sys_user_role_mst"
				+ " where role_code = " + dbm.param(rid, DataType.TEXT)
				+ " AND comp_code = "+dbm.param(compCode, DataType.TEXT);						
		String tmpInsert = "insert into sys_user_role_mst_tmp " + insertCols + " "+ selectSql;
		boolean result = dbm.insert(tmpInsert);
		dbm.clear();
		
		if (result) {
			cols = "";
			for (int i = 0; i<rightKeys.length - 4; i++) {
				cols += rightKeys[i] + ", ";
			}
			insertCols = "("+cols+"modify_date, modify_by, action)";
			selectSql = "select " + cols + dbm.param(now, DataType.DATE)+" AS modify_date, "+dbm.param(userCode, DataType.TEXT)+" AS modify_by, "+dbm.param("D", DataType.TEXT)+" AS action "
					+ "from sys_user_role_func"
					+ " where role_code = " + dbm.param(rid, DataType.TEXT)
					+ " AND comp_code = "+dbm.param(compCode, DataType.TEXT);						
			tmpInsert = "insert into sys_user_role_func_tmp " + insertCols + " "+ selectSql;

			result = dbm.insert(tmpInsert);
			dbm.clear();
		}
		
		return result;
	}
	     

	public boolean saveInsert(String rid, String compCode, JsonObject roleJson, String updateBy, Date createDate, String createBy, String action) throws Exception {
		
		String cols = keys[0] + ", ";
		String values = dbm.param(compCode, DataType.TEXT) + ", ";

		cols += keys[1] + ", ";
		values += dbm.param(rid, DataType.TEXT) + ", ";

		Date now = new Date(new java.util.Date().getTime());
		
		for (int i = 2; i<keys.length - 2; i++) {
			String key = keys[i];
			String id = ids[i];
			if (i < keys.length - 6) {
				if (roleJson.has(id) && !roleJson.get(id).isJsonNull()) {
					cols += key + ", ";
					values += dbm.param(roleJson.get(id).getAsString(), DataType.TEXT) + ", ";
				}
			} else {
				cols += key + ", ";
			}
		}
		
		values += dbm.param(createDate != null? createDate: now, DataType.DATE) + ", "; // create date
		values += dbm.param(createBy != null? createBy: updateBy, DataType.TEXT) + ", ";	// create by
		values += dbm.param(now, DataType.DATE) + ", ";	// modify date
		values += dbm.param(updateBy, DataType.TEXT) + ", "; // modify be
		
		//insert a row from mst to tmp
		String insertCols = "(" + cols + "action)";
		String valuesField = "("+values + dbm.param(action, DataType.TEXT)+")";
		
		String roleFuncSql = "";
		if (roleJson.has("rights") && !roleJson.get("rights").isJsonNull()) {
			JsonObject rightMap = roleJson.get("rights").getAsJsonObject();
			
			String aCols = "";
			for (int i = 0; i<rightKeys.length - 2; i++) {
				aCols += rightKeys[i] + ", ";
			}

			String roleFuncInsert = " INTO sys_user_role_func_tmp ("+aCols+"action) VALUES ";
			
			for (Map.Entry<String,JsonElement> entry : rightMap.entrySet()) {
				String func = entry.getKey();
				if ("Y".equals(entry.getValue().getAsString())) {
					roleFuncSql += roleFuncInsert 
							+"("+dbm.param(rid, DataType.TEXT)+","+ dbm.param(compCode, DataType.TEXT)
							+","+ dbm.param(func, DataType.TEXT)+","+dbm.param("A", DataType.TEXT)
							+","+ dbm.param(now, DataType.DATE)+","+dbm.param(updateBy, DataType.TEXT) 
							+","+ dbm.param(now, DataType.DATE)+","+dbm.param(updateBy, DataType.TEXT) 
							+","+ dbm.param(action, DataType.TEXT)+ ")";
				}
			}
		}
		
		String tmpInsert = "INSERT ALL INTO sys_user_role_mst_tmp " + insertCols + " VALUES "+ valuesField
							+ roleFuncSql + " SELECT 1 FROM DUAL";
			
		boolean result = dbm.insert(tmpInsert);
		dbm.clear();
		return result;
	}

	public boolean saveUpdate(String rid, String compCode, JsonObject roleJson, String updateBy) throws Exception {

		String fields = "";
		Date now = new Date(new java.util.Date().getTime());

		for (int i = 2; i<keys.length - 6; i++) {
			String key = keys[i];
			String id = ids[i];
			
			if (roleJson.has(id) && !roleJson.get(id).isJsonNull()) {
				fields += (fields.isEmpty()?"":", ") + key +" = " + dbm.param(roleJson.get(id).getAsString(), DataType.TEXT);
			}
		}
		
		fields += ", modify_date = " + dbm.param(now, DataType.DATE);
		fields += ", modify_by = " + dbm.param(updateBy, DataType.TEXT);
		
		//insert a row from mst to tmp
		String tmpInsert = "UPDATE sys_user_role_mst_tmp SET " + fields 
				+ " WHERE role_code = " + dbm.param(rid, DataType.TEXT)
				+ " AND comp_code = "+dbm.param(compCode, DataType.TEXT);
			
		int result = dbm.update(tmpInsert);
		dbm.clear();
		if (result == 1) {
			String removeCor = "DELETE FROM sys_user_role_func_tmp WHERE role_code = " + dbm.param(rid, DataType.TEXT)
			+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
			dbm.delete(removeCor);
			dbm.clear();
			
			String roleFuncInsert = " INTO sys_user_role_func_tmp (role_code, comp_code, func_code, status, create_date, create_by, modify_date, modify_by, action) VALUES ";
			
			String roleFuncSql = "";
			JsonObject rightMap = null;
			if (roleJson.has("rights") && !roleJson.get("rights").isJsonNull()) {
				rightMap = roleJson.get("rights").getAsJsonObject();
				
				for (Map.Entry<String,JsonElement> entry : rightMap.entrySet()) {
					String func = entry.getKey();
					if ("Y".equals(entry.getValue().getAsString())) {
						roleFuncSql += roleFuncInsert 
						+"("+ dbm.param(rid, DataType.TEXT)	+","+ dbm.param(compCode, DataType.TEXT)
						+","+ dbm.param(func, DataType.TEXT)+","+ dbm.param("A", DataType.TEXT)
						+","+ dbm.param(now, DataType.DATE)+","+dbm.param(updateBy, DataType.TEXT) 
						+","+ dbm.param(now, DataType.DATE)+","+dbm.param(updateBy, DataType.TEXT) 
						+","+ dbm.param("E", DataType.TEXT)+")";
					}
				}
				tmpInsert = "INSERT ALL "+ roleFuncSql + " SELECT 1 FROM DUAL";
				
				result += dbm.insert(tmpInsert)?1:0;
				dbm.clear();
			}			
		}
		
		return result > 0;
	}
	
	public boolean approveAdd(String rid, String compCode, String userCode) throws Exception {

		String cols = "";
		for (int i = 0; i<keys.length - 2; i++) {
			cols += keys[i] + ", ";
		}
		String insertCols = "(" +cols+ "approv_date, approv_by)";
		Date now = new Date(new java.util.Date().getTime());
		// insert to sys_user_role
		String approveSQL = "INSERT INTO sys_user_role_mst "+insertCols
				+" SELECT "+cols + dbm.param(now, DataType.DATE) +" approv_date, "+dbm.param(userCode, DataType.TEXT)+" approv_by FROM sys_user_role_mst_tmp WHERE role_code = " +  dbm.param(rid, DataType.TEXT)
				+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
		
		boolean result = dbm.insert(approveSQL);
		dbm.clear();
		
		if (result) {
		// insert new func code for the role
			cols = "";
			for (int i = 0; i<rightKeys.length - 2; i++) {
				cols += rightKeys[i] + ", ";
			}
			
			approveSQL = "INSERT INTO sys_user_role_func ("+cols+"approv_date, approv_by) "
					+" SELECT "+cols+dbm.param(now, DataType.DATE)+" AS approv_date, "+dbm.param(userCode, DataType.TEXT)+" AS approv_by FROM sys_user_role_func_tmp WHERE role_code = " +  dbm.param(rid, DataType.TEXT)
					+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
			
			result &= dbm.insert(approveSQL);
			dbm.clear();

		}
		return result;
	}
	
	public boolean approveEdit(String rid, String compCode, String updateBy) throws Exception {

		String cols = "";
		for (int i = 2; i<keys.length - 2; i++) {
			cols += keys[i] + ", ";
		}
		String insertCols = "(" +cols+ "approv_date, approv_by)";
		Date now = new Date(new java.util.Date().getTime());
		String approveSQL = "UPDATE sys_user_role_mst SET "+insertCols
				+" = (SELECT "+cols + dbm.param(now, DataType.DATE) +" approv_date, "+dbm.param(updateBy, DataType.TEXT)+" approv_by FROM sys_user_role_mst_tmp WHERE role_code = " +  dbm.param(rid, DataType.TEXT) + " AND comp_code = " + dbm.param(compCode, DataType.TEXT) + " )"
				+" WHERE role_code = " +  dbm.param(rid, DataType.TEXT)
				+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
		
		int result = dbm.update(approveSQL);
		dbm.clear();
		
		if (result > 0) {
		
			// delete existing func code for the role
			String removeCor = "DELETE FROM sys_user_role_func WHERE role_code = " + dbm.param(rid, DataType.TEXT)+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
			dbm.delete(removeCor);
			dbm.clear();
			
			cols = "";
			for (int i = 0; i<rightKeys.length - 2; i++) {
				cols += rightKeys[i] + ", ";
			}
						// insert new func code for the role
			approveSQL = "INSERT INTO sys_user_role_func ("+cols+"approv_date, approv_by) "
					+" SELECT "+cols+dbm.param(now, DataType.DATE)+" AS approv_date, "+dbm.param(updateBy, DataType.TEXT)+" AS approv_by FROM sys_user_role_func_tmp "
							+ "WHERE role_code = " +  dbm.param(rid, DataType.TEXT)
							+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);

			dbm.insert(approveSQL);
			dbm.clear();
		}		
		return result > 0;
	}
	

	public boolean approveDelete(String rid, String compCode, String updateBy) throws Exception {

		//String deleteSQL = "DELETE FROM sys_user_role_mst WHERE role_code = " +  dbm.param(updateBy, DataType.TEXT)
		String deleteSQL = "DELETE FROM sys_user_role_mst WHERE role_code = " +  dbm.param(rid, DataType.TEXT)
		+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
		int result = dbm.delete(deleteSQL);
		dbm.clear();		
		// delete existing func code for the role
		//String removeCor = "DELETE FROM sys_user_role_func WHERE role_code = " + dbm.param(updateBy, DataType.TEXT)
		String removeCor = "DELETE FROM sys_user_role_func WHERE role_code = " + dbm.param(rid, DataType.TEXT)
		+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
		dbm.delete(removeCor);
		dbm.clear();
		return result > 0;
	}
	
	public boolean removeTmp(String rid, String compCode) throws Exception {

		String deleteSQL = "DELETE FROM sys_user_role_mst_tmp WHERE role_code = " +  dbm.param(rid, DataType.TEXT)
		+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
		int result = dbm.delete(deleteSQL);
		dbm.clear();
		
		// delete existing func code for the role
		String removeCor = "DELETE FROM sys_user_role_func_tmp WHERE role_code = " + dbm.param(rid, DataType.TEXT)
		+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
		result += dbm.delete(removeCor);
		dbm.clear();

		return result > 0;
	}
	

	public boolean insertAudit(String rid, String compCode) throws Exception {
		
		String cols = "";
		for (int i = 0; i<keys.length - 2; i++) {
			cols += keys[i] + ", ";
		}
		
		String audInsert = "INSERT INTO aud_sys_user_role_mst ("+cols+"action) SELECT "+cols+"action FROM sys_user_role_mst_tmp WHERE role_code = " +  dbm.param(rid, DataType.TEXT)
		+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
		boolean result = dbm.insert(audInsert);
		dbm.clear();
		
		cols = "";
		for (int i = 0; i<rightKeys.length - 2; i++) {
			cols += rightKeys[i] + ", ";
		}
		audInsert = "INSERT INTO aud_sys_user_role_func ("+cols+"action) SELECT "+cols+"action FROM sys_user_role_func_tmp WHERE role_code = " +  dbm.param(rid, DataType.TEXT)
		+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
		result &= dbm.insert(audInsert);

		dbm.clear();
		return result;
	}
	
	public boolean insertAudit(String rid, String compCode, String userCode, String action) throws Exception {
		Date now = new Date(new java.util.Date().getTime());
		
		String cols = "";
		for (int i = 0; i<keys.length - 2; i++) {
			cols += keys[i] + ", ";
		}
		
		String audInsert = "insert into aud_sys_user_role_mst (" +cols+"approv_date, approv_by, action)"
				+" select "+cols 
				+ dbm.param(now, DataType.DATE) +" AS approv_date,"
				+ dbm.param(userCode, DataType.TEXT) +" AS approv_by,"
				+ dbm.param(action, DataType.TEXT) +" AS action from sys_user_role_mst_tmp WHERE role_code = " +  dbm.param(rid, DataType.TEXT)
				+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);
		
		boolean result = dbm.insert(audInsert);
		dbm.clear();
		
		String roleFuncCols = "";
		for (int i = 0; i<rightKeys.length - 2; i++) {
			roleFuncCols += rightKeys[i] + ", ";
		}
		
		String roleFuncInsertStr = "INSERT INTO aud_sys_user_role_func ("+roleFuncCols+"approv_date, approv_by, action) "
				+ "SELECT " + roleFuncCols 
				+ dbm.param(now, DataType.DATE)+" AS approv_date, "
				+ dbm.param(userCode, DataType.TEXT)+" AS approv_by, "
				+ dbm.param(action, DataType.TEXT)+" AS action FROM sys_user_role_func_tmp WHERE role_code = " + dbm.param(rid, DataType.TEXT)
				+" AND comp_code = "+dbm.param(compCode, DataType.TEXT);

		result = dbm.insert(roleFuncInsertStr);
		dbm.clear();
		return result;
	}

}
