package com.eab.dao.dynamic;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.eab.common.Function;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.system.Name;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.dynamic.CompanyBean;
import com.eab.model.profile.UserPrincipalBean;

public class Companies {
	public List<CompanyBean> getAllCompanies() throws SQLException {
		String sqlSelectCom = "select comp_code, comp_alias from sys_company_mst where status = 'A' order by UPPER(comp_alias) ASC";
		List<CompanyBean> companyList = new ArrayList<CompanyBean>();
		ResultSet rs = null;
		DBManager dbm = null;
		Connection conn = null;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			rs = dbm.select(sqlSelectCom, conn);
			if (rs != null) {
				while (rs.next()) {
					CompanyBean comp = new CompanyBean();
					comp.setCompCode(rs.getString("comp_code"));
					comp.setCompName(rs.getString("comp_alias"));
					
					Log.debug("Companies:"+rs.getString("comp_alias"));
					companyList.add(comp);
				}
			}
		} catch (Exception e) {
			// no need to handle
		} finally {
			rs.close();
			dbm.clear();
			conn.close();
		}
		return companyList;

	}

	public List<CompanyBean> getCompanyListByUser(HttpServletRequest request) throws SQLException {
		List<CompanyBean> companyList = new ArrayList<CompanyBean>();
		UserPrincipalBean principal = Function.getPrincipal(request);
		String userCode = principal.getUserCode();
//		if (Function.canSkipMakerChecker(principal))
//			companyList = getAllCompanies();
//		else {
			ResultSet rs = null;
			DBManager dbm = null;
			Connection conn = null;
			try {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
				String sqlSelectCom = "select sys_company_mst.comp_code, sys_company_mst.comp_alias from sys_company_mst";
				// get all if compCode == 'SYS'
				if (principal.isRegional()) {					
					sqlSelectCom += " where sys_company_mst.status = 'A'";
				} else {
					// otherwise, join user_user_role to get the granded companies
					sqlSelectCom += " inner join sys_user_user_role on sys_company_mst.comp_code=sys_user_user_role.comp_code";
					sqlSelectCom += " where sys_user_user_role.user_code = ?"
							+ " and sys_company_mst.status = 'A'"
							+ " and sys_user_user_role.status = 'A'";
					dbm.param(userCode, DataType.TEXT);
				}
				
				sqlSelectCom += " order by UPPER(sys_company_mst.comp_alias)";
				
				rs = dbm.select(sqlSelectCom, conn);
				if (rs != null) {
					while (rs.next()) {
						CompanyBean comp = new CompanyBean();
						comp.setCompCode(rs.getString("comp_code"));
						comp.setCompName(rs.getString("comp_alias"));
						companyList.add(comp);
					}
				}
			} catch (Exception e) {
				// no need to handle
			} finally {
				if (rs != null) {
					rs.close();
				}
				if (dbm != null) {
					dbm.clear();
				}
				if (conn != null) {
					conn.close();
				}
			}
//		}
		return companyList;

	}

	public String getDateFormat(String compCode) {
		String df = null;
		String sql = "select sys_lookup_mst.name_code from sys_lookup_mst inner join sys_company_mst on sys_lookup_mst.lookup_field = sys_company_mst.date_format where sys_company_mst.comp_code=?";
		DBManager dbm = null;
		Connection conn = null;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			dbm.param(compCode, DataType.TEXT);
			df = dbm.selectSingle(sql, "name_code", conn);
		} catch (Exception e) {
			Log.error(e);
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				Log.error(e);
			}
			dbm.clear();
			dbm = null;
		}
		return df;
	}

	public CompanyBean getCompany(String compCode) {
		CompanyBean company = new CompanyBean();
		String compSql = "select * from sys_company_mst where comp_code = ?";
		DBManager dbm = null;
		Connection conn = null;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			dbm.param(compCode, DataType.TEXT);
			ResultSet rs = null;
			rs = dbm.select(compSql, conn);
			if (rs != null) {
				while (rs.next()) {
					company.setCompName(rs.getString("comp_alias"));
					company.setLang(rs.getString("language"));
					company.setIsActive((rs.getString("status").equals("A")) ? true : false);
					company.setDateFormat(rs.getString("date_format"));
				}
			}

			rs.close();
			dbm.clear();
		} catch (Exception e) {
			company = null;
			dbm = null;
			conn = null;
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				Log.error(e);
			}
			dbm.clear();
			dbm = null;
		}
		return company;
	}

	public HashMap<String, String> getCompanyLangNameMap(CompanyBean company, HttpServletRequest request) {
		List<Name> nameList = new ArrayList<Name>();
		List<String> langCodeList = company.getLangList();
		String langCols = null;
		DBManager dbm = null;
		Connection conn = null;
		HashMap<String, String> codeLangMap = new HashMap<String, String>();
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			for (int i = 0; i < langCodeList.size(); i++) {
				if (langCols == null) {
					langCols = "(?";
				} else {
					langCols += ", ?";
				}
				dbm.param(langCodeList.get(i), DataType.TEXT);
			}
			langCols += ")";
			String sqlLangName = "select * from sys_lang_mst where lang_code in " + langCols;
			ResultSet rs = null;
			rs = dbm.select(sqlLangName, conn);
			HashMap<String, String> codeNameMap = new HashMap<String, String>();

			if (rs != null) {
				while (rs.next()) {
					String langCode = rs.getString("lang_code");
					String langNameCode = rs.getString("name_code");
					codeNameMap.put(langNameCode, langCode);
					nameList.add(new Name(langNameCode, ""));
				}
			}
			Language lang = new Language();
			lang.getNameList(request, nameList);
			for (int j = 0; j < nameList.size(); j++) {
				Name newName = nameList.get(j);
				codeLangMap.put(codeNameMap.get(newName.getCode()), newName.getDesc());
			}
			rs.close();
			dbm.clear();
		} catch (Exception e) {

		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				Log.error(e);
			}
			dbm.clear();
			dbm = null;
		}
		return codeLangMap;
	}

	public String getCompanyDateFormat(String compCode) throws SQLException {
		String result = "";
		DBManager dbm = null;
		Connection conn = null;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			String sql = " select lookup.name_code as date_format " + " from sys_company_mst comp " + " inner join sys_lookup_mst lookup " + " on lookup.lookup_field = comp.date_format " + " and lookup.lookup_key = 'DATEFORMAT' " + " where comp.comp_code = " + dbm.param(compCode, DataType.TEXT);

			result = dbm.selectSingle(sql, "date_format");
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
			dbm = null;
		}

		return result;
	}

	public String getCompanyTimeFormat(String compCode) throws SQLException {
		String result = "";
		DBManager dbm = null;
		Connection conn = null;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			String sql = " select lookup.name_code as time_format " + " from sys_company_mst comp " + " inner join sys_lookup_mst lookup " + " on lookup.lookup_field = comp.time_format " + " and lookup.lookup_key = 'TIMEFORMAT' " + " where comp.comp_code = " + dbm.param(compCode, DataType.TEXT);

			result = dbm.selectSingle(sql, "time_format");
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
			dbm = null;
		}

		return result;
	}
}
