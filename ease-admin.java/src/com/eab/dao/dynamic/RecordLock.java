package com.eab.dao.dynamic;

import java.net.InetAddress;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.dynamic.RecordLockBean;
import com.eab.model.profile.UserPrincipalBean;

public class RecordLock {
	public RecordLockBean newRecordLockBean(String module, String recordId){
		RecordLockBean rlb = new RecordLockBean();
		rlb.setModule(module);
		rlb.setRecordId(recordId);
		return rlb;
	}
	public RecordLockBean newRecordLockBean(String module, String recordId, int version){
		RecordLockBean rlb = new RecordLockBean();
		rlb.setModule(module);
		rlb.setRecordId(recordId);
		rlb.setRecordVersion(version);
		return rlb;
	}
	
	public RecordLockBean getRecordLock(HttpServletRequest request, RecordLockBean rlb){
		UserPrincipalBean principal = Function.getPrincipal(request);
		return getRecordLock(request, rlb, principal.getUserCode(), principal.getCompCode());
	}
	public RecordLockBean getRecordLock(HttpServletRequest request, RecordLockBean rlb, String userCode_, String compCode){
		DBManager dbm = null;
		Connection conn = null;
		ResultSet rs = null;
					
		try{
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			String sql = "select user_code from aud_record_lock where module=? and record_id=? ";
			dbm.param(rlb.getModule(), DataType.TEXT);
			dbm.param(rlb.getRecordId(), DataType.TEXT);
			if(rlb.getRecordVersion() != 0){
				sql += "and record_version = ?";
				dbm.param(rlb.getRecordVersion(), DataType.INT);
			}
			if(!"company".equals(rlb.getModule())){
				sql += "and comp_code = ?";
				dbm.param(compCode, DataType.TEXT);
			}
			
			rs = dbm.select(sql, conn);
			if(rs != null){
				if(rs.next()){
					String userCode = rs.getString("user_code");
					if(userCode.equals(userCode_)){
						rlb.setIsCurrentUserAccess(true);
						rlb.setCanAccess(true);
					}
					else{
						rlb.setIsCurrentUserAccess(false);
						rlb.setCanAccess(false);
					}
					rlb.setUserCode(userCode);
				}else{
					rlb.setUserCode(userCode_);
					rlb.setCanAccess(true);
					rlb.setIsCurrentUserAccess(false);
				}
			}else{
				rlb.setUserCode(userCode_);
				rlb.setCanAccess(true);
				rlb.setIsCurrentUserAccess(false);
			}
		}catch(Exception e){
			Log.error(e);
		}finally{
			rs = null;
			dbm.clear();
			dbm = null;
			try {
				conn.close();
			} catch (SQLException e) {
				Log.error(e);
			}
			
		}
		return rlb;
	}
	

	public boolean addRecordLock(HttpServletRequest request, RecordLockBean rlb){
		RecordLockBean result = null;
		RecordLock rlD = new RecordLock();
		rlb = rlD.getRecordLock(request, rlb); 
		
		if (rlb.canAccess()) {
			DBManager dbm = null;
			Connection conn = null;
			InetAddress ip;
			HttpSession session = request.getSession();
			try {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				//
				//get ip address
				ip = InetAddress.getLocalHost();
				String ipAddress = ip.getHostAddress();
					
				String sql = "insert into aud_record_lock (session_id, module, record_id, user_code, record_version, server_ip, comp_code) values(?,?,?,?,?,?,?)";
				dbm.param(session.getId(), DataType.TEXT);
				dbm.param(rlb.getModule(), DataType.TEXT);
				dbm.param(rlb.getRecordId(), DataType.TEXT);
				dbm.param(rlb.getUserCode(), DataType.TEXT);
				dbm.param(rlb.getRecordVersion(), DataType.INT);
				dbm.param(ipAddress, DataType.TEXT);
				
				if (!rlb.getModule().equals("company")) {
					UserPrincipalBean principal = Function.getPrincipal(request);
					dbm.param(principal.getCompCode(), DataType.TEXT);
				} else {
					dbm.param("", DataType.TEXT);
				}
				
				if (dbm.insert(sql, conn))
					return true;
				else
					return false;
			} catch(Exception e) {
				Log.error(e);
				return false;
			} finally {
				dbm = null;
				
				try {
					conn.close();
				} catch (SQLException e) {
					Log.error(e);
				}
			}
		}
		return true;
		
	}
	
	public boolean removeRecordLock(String module, String id, String compCode){
		boolean remove = false;
		DBManager dbm = null;
		Connection conn = null;
		try{
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			String sql = "delete from aud_record_lock where module = ? and record_id = ?";
			dbm.param(module, DataType.TEXT);
			dbm.param(id, DataType.TEXT);
			if(!"company".equals(module)){
				sql += "and comp_code = ?";
				dbm.param(compCode, DataType.TEXT);
			}
			int reuslt = dbm.delete(sql, conn);	
			if(reuslt != -1)
				remove = true;
		}catch(Exception e){
			Log.error(e);
		}finally{
			dbm = null;
			try {
				conn.close();
			} catch (SQLException e) {
				Log.error(e);
			}
		}
		return remove;
	}
	
	public boolean removeRecordLockBySession(String sessionId){
		Log.debug("removeRecordLockBySession");
		boolean remove = false;
		DBManager dbm = null;
		Connection conn = null;
		try{
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			String sql = "delete from aud_record_lock where session_id = ?";
			dbm.param(sessionId, DataType.TEXT);
			int reuslt = dbm.delete(sql, conn);	
			if(reuslt != -1)
				remove = true;
		}catch(Exception e){
			Log.error(e);
		}finally{
			dbm = null;
			try {
				conn.close();
			} catch (SQLException e) {
				Log.error(e);
			}
		}
		return remove;
	}
	
	public boolean removeRecordLockByIp(String serverIp){
		Log.debug("removeRecordLockByIp");
		boolean remove = false;
		DBManager dbm = null;
		Connection conn = null;
		try{
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			String sql = "delete from aud_record_lock where server_ip = ?";
			dbm.param(serverIp, DataType.TEXT);
			int reuslt = dbm.delete(sql, conn);	
			if(reuslt != -1)
				remove = true;
		}catch(Exception e){
			Log.error(e);
		}finally{
			dbm = null;
			try {
				conn.close();
			} catch (SQLException e) {
				Log.error(e);
			}
		}
		return remove;
	}
}
