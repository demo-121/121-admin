package com.eab.dao.dynamic;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Trigger;
import com.eab.model.dynamic.DynMultiLangField;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.security.SysPrivilegeMgr;

public class DynamicDAO {
	public DynMultiLangField getDynMultiLangField(String module){
		DynMultiLangField dynMultiLangField = new DynMultiLangField();
		DBManager dbm = null;
		Connection conn = null;	
		ResultSet rs = null;
		String sql = "select * from dyn_field_mul_lang_mst where module = ?";
		try{
			conn = DBAccess.getConnection();
			dbm = new DBManager();
			dbm.param(module, DataType.TEXT);
			rs = dbm.select(sql, conn);
			if(rs != null){
				if( rs.next()){
					dynMultiLangField.setModule(module);
					dynMultiLangField.setAudTable(rs.getString("aud_table"));
					dynMultiLangField.setMstTable(rs.getString("mst_table"));
					dynMultiLangField.setTmpTable(rs.getString("tmp_table"));
				}
			}
		}catch(Exception e){
			Log.error(e);
		}finally{
			dbm.clear();
			dbm = null;
			try{
				conn.close();
			}catch(Exception e){
				Log.error(e);
			}
		}
		return dynMultiLangField;
	}
	
	public String getModifyUser(String table, String key, String value){
		String userCode = null;
		DBManager dbm = null;
		Connection conn = null;	
		String sql = "select MODIFY_BY from "+ table + " where "+ key + "=?";
		try{
			conn = DBAccess.getConnection();
			dbm = new DBManager();
			dbm.param(value, DataType.TEXT);		
			userCode = dbm.selectSingle(sql, "MODIFY_BY", conn);
		}catch(Exception e){
			Log.error(e);
		}finally{
			dbm.clear();
			dbm = null;
			try{
				conn.close();
			}catch(Exception e){
				Log.error(e);
			}
		}		
		return userCode;
	}
	public  boolean hasApproveRight(DBManager dbm, DynamicUI ui, String id, String userCode) throws SQLException {
		String sql = "select action, create_by, modify_by from "+ ui.getTempTable() + " where "+ ui.getPrimaryKey() +"=?";
		ResultSet rs = null;
		Connection conn = null;	
		boolean hasRight = true;
		try{
			conn = DBAccess.getConnection();
			dbm.param(id, DataType.TEXT);
			rs = dbm.select(sql, conn);
			if(rs != null){
				while( rs.next() ){
					String action = rs.getString("action");
					String createBy = rs.getString("create_by");
					String modifyBy = rs.getString("modify_by");
					if(action.equals("N")){
						if(userCode.equals(createBy))
							return false;
					}
					else{ 
						if(userCode.equals(modifyBy))
							return false;
					}
				}
			}
			rs.close();
			dbm.clear();
			
		}catch(Exception e){
			hasRight = false;
		}finally{
			conn.close();
			dbm = null;
			conn = null;
		}
		return hasRight;
	}
	
	public String getTempTableAction(DBManager dbm, String table, String key, String value){
		String action = null; 		
		try {
			String _sqlAction = "select action from "+ table + " where " + key + "= ? " ;
			dbm.param(value, DataType.TEXT);
			action = dbm.selectSingle(_sqlAction, "action");
		} catch (Exception e) {
			//nth
		}finally{
			dbm.clear();
		}
		return action;
	}
	
	public String getTempTableAction(DBManager dbm, String table, HashMap<String,String> condMap){
		String action = null; 		
		try {
			
			String conditionStr = null;
			for (Map.Entry<String, String> entry : condMap.entrySet())
			{
				if(conditionStr == null)
					conditionStr = entry.getKey() + "=? ";					
				else
					conditionStr = conditionStr + " and " + entry.getKey() + "=? "; 
				
				dbm.param(entry.getValue(), DataType.TEXT);
			}
			
			String _sqlAction = "select action from "+ table + " where " + conditionStr ;
			
			action = dbm.selectSingle(_sqlAction, "action");
		} catch (Exception e) {
			//nth
		}finally{
			dbm.clear();
		}
		return action;
	}

	
	public void deleteMULFromTmp(DBManager dbm, DynMultiLangField dynMultiLangField, DynamicUI ui, String id, UserPrincipalBean principal){
		//delete record in tmp		
		deleteMULRecords(dbm, dynMultiLangField.getTmpTable(), ui, id, principal);
	}
	
	public void deleteMULFromMst(DBManager dbm, DynMultiLangField dynMultiLangField, DynamicUI ui, String id, UserPrincipalBean principal){
		//delete record in tmp		
		deleteMULRecords(dbm, dynMultiLangField.getMstTable(), ui, id, principal);
	}
	
	public void deleteMULRecords(DBManager dbm, String table, DynamicUI ui, String id, UserPrincipalBean principal){
		//delete record in tmp		
		dbm.clear();
		try {
			String deleteSql = "delete from " + table + " where " + ui.getPrimaryKey() +"=?";
			dbm.param(id, DataType.TEXT);
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				deleteSql += " and " + Constants.COL_COMP_CODE + "=?";
				dbm.param(principal.getCompCode(), DataType.TEXT);
			}
			int delete = dbm.delete(deleteSql);
		} catch (Exception e) {
			Log.error(e);
		}finally{
			dbm.clear();
		}
		
	}
	
	public void deleteFromTmp(DBManager dbm, DynamicUI ui, String id, UserPrincipalBean principal){
		//delete record in tmp		
		dbm.clear();
		try {
			String deleteSql = "delete from " + ui.getTempTable() + " where " + ui.getPrimaryKey() +"=?";
			dbm.param(id, DataType.TEXT);
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				deleteSql += " and " + Constants.COL_COMP_CODE + "=?";
				dbm.param(principal.getCompCode(), DataType.TEXT);
			}
			int delete = dbm.delete(deleteSql);
		} catch (Exception e) {
			Log.error(e);
		}finally{
			dbm.clear();
		}
		
	}
	public void updateTmpTableForCopy(HttpServletRequest request, DBManager dbm, DynamicUI ui, String action, String id){
		updateTmpTableForCopy(request, dbm, ui, action, id, null);
	}
	public void updateTmpTableForCopy(HttpServletRequest request, DBManager dbm, DynamicUI ui, String action, String id, Date date){
		dbm.clear();
		String usqlUpdateTmp = "update " + ui.getTempTable() + " set APPROV_BY = ? , APPROV_DATE = ?, action = ? where "+ui.getPrimaryKey() + "=?";
		UserPrincipalBean principal = Function.getPrincipal(request);

		try{
			Calendar calendar = Calendar.getInstance();
			java.util.Date currentDate = calendar.getTime();
			dbm.param(principal.getUserCode(), DataType.TEXT);
			if(date == null)
				dbm.param( new java.sql.Date(currentDate.getTime()), DataType.DATE);	
			else
				dbm.param(date, DataType.DATE);
			
			dbm.param(action, DataType.TEXT);
			dbm.param(id, DataType.TEXT);
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				usqlUpdateTmp += " and " + Constants.COL_COMP_CODE + "=?";
				dbm.param(principal.getCompCode(), DataType.TEXT);
			}
			
			dbm.update(usqlUpdateTmp);					 	
		}catch(Exception e){
			Log.error(e);
		}finally{
			dbm.clear();
		}

	}
	public void updateTmpTableModify(HttpServletRequest request, DBManager dbm, DynamicUI ui, String action, String id){
		String usqlUpdateTmp = "update " + ui.getTempTable() + " set MODIFY_BY = ? , MODIFY_DATE = ?, action = ? where "+ui.getPrimaryKey() + "=?";
		UserPrincipalBean principal = Function.getPrincipal(request);
		try{
			Calendar calendar = Calendar.getInstance();
			java.util.Date currentDate = calendar.getTime();
			dbm.param(principal.getUserCode(), DataType.TEXT);
			dbm.param( new java.sql.Date(currentDate.getTime()), DataType.DATE);					
			dbm.param(action, DataType.TEXT);
			dbm.param(id, DataType.TEXT);
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				usqlUpdateTmp += " and " + Constants.COL_COMP_CODE + "=?";
				dbm.param(principal.getCompCode(), DataType.TEXT);
			}
			
			dbm.update(usqlUpdateTmp);					 	
		}catch(Exception e){
			Log.error(e);
		}finally{
			dbm.clear();
		}

	}
	
	public void updateTableModify(DBManager dbm, String table, HashMap<String, Object> condMap, String user, Date date){
		String usqlUpdate = "update " + table + " set MODIFY_BY = ? , MODIFY_DATE = ? where ";
		
		try{
			dbm.param(user, DataType.TEXT);
			dbm.param(date, DataType.DATE);
		String conds = null;
		for (Map.Entry<String, Object> entry : condMap.entrySet())
		{
			conds = (conds == null) ? entry.getKey() + "=? ": conds + " and " + entry.getKey() + "=? "; 
			if(entry.getValue() instanceof String)
				dbm.param(entry.getValue(), DataType.TEXT);
			else if(entry.getValue() instanceof Date)
				dbm.param(entry.getValue(), DataType.DATE);
		}
		usqlUpdate += conds;
		dbm.update(usqlUpdate);					 	
		}catch(Exception e){
			Log.error(e);
		}finally{
			dbm.clear();
		}

	}

	
	public void copyFromTmpToAud(DBManager dbm, DynamicUI ui, String id, UserPrincipalBean principal){
		//insert copy from tmp to aud
		dbm.clear();	
			try {
				String sqlAud = "insert into "+ ui.getAudTable() +" select * from " + ui.getTempTable() + " where " + ui.getPrimaryKey() + " = " +  dbm.param(id, DataType.TEXT);
				if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
					sqlAud += " and " + Constants.COL_COMP_CODE + "=?";
					dbm.param(principal.getCompCode(), DataType.TEXT);
				}
				dbm.insert(sqlAud);
			} catch (Exception e) {
				Log.error(e);
			}			
		dbm.clear();	
	}
	
	public void updateMULTmpTableForCopy(DBManager dbm, DynMultiLangField dynMultiLangField, DynamicUI ui, String id, UserPrincipalBean principal, Date approvDate, String action){
		dbm.clear();
		String conds = null;
		try{
			String actionSql = "update " + dynMultiLangField.getTmpTable() + " set MODIFY_BY = ?, MODIFY_DATE = ?, action = ? ";
			dbm.param( principal.getUserCode(), DataType.TEXT);									
			dbm.param( approvDate, DataType.DATE);
			dbm.param(action, DataType.TEXT);
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				conds = "comp_code = ?";
				dbm.param(principal.getCompCode(), DataType.TEXT);
			}
			conds = (conds == null) ? ui.getPrimaryKey() + " = ?" : conds + " and " + ui.getPrimaryKey() + " = ?";
			dbm.param(id, DataType.TEXT);
			
			actionSql += " where " + conds;
			dbm.update(actionSql);
			
			dbm.clear();
		}catch(Exception e){
			dbm.clear();
		}
	}
	
public void updateMULMstTable(DBManager dbm, DynMultiLangField dynMultiLangField, DynamicUI ui, String id, UserPrincipalBean principal, Date approvDate, String action){
		
		String conds = null;
		try{
			String wheres = null;
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				wheres = "comp_code = " + principal.getCompCode();
			}
			wheres = (wheres == null) ? ui.getPrimaryKey() + " = "+id : wheres + " and " + ui.getPrimaryKey() + " = " + id;
			wheres += " and ";
			
			String sqlBy = "select approv_by from " + dynMultiLangField.getTmpTable() + wheres;
			String sqlDate = "select approv_date from " + dynMultiLangField.getTmpTable() + wheres;
			String sqlName = "select name_desc from " + dynMultiLangField.getTmpTable() + wheres;
			
			sqlBy = "(" + sqlBy + ")";
			sqlDate = "(" + sqlDate + ")";
			sqlName = "(" + sqlName + ")";
			
			String actionSql = "update " + dynMultiLangField.getMstTable() + " set APPROV_BY = "+ sqlBy +" , APPROV_DATE = "+ sqlDate +", name_desc = " + sqlName;
			
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				conds = "comp_code = ?";
				dbm.param(principal.getCompCode(), DataType.TEXT);
			}
			conds = (conds == null) ? ui.getPrimaryKey() + " = ?" : conds + " and " + ui.getPrimaryKey() + " = ?";
			dbm.param(id, DataType.TEXT);
			
			actionSql += " where " + conds;
			dbm.update(actionSql);
			
			dbm.clear();
		}catch(Exception e){
			dbm.clear();
		}
	}
	
	
	public void copyMULfromTmpToAud(DBManager dbm, DynMultiLangField dynMultiLangField, DynamicUI ui, String id, String compCode){
		dbm.clear();	
		
		try {
			String sqlAud = "insert into "+ dynMultiLangField.getAudTable() +" select * from " + dynMultiLangField.getTmpTable() + " where " + ui.getPrimaryKey() + " = " +  dbm.param(id, DataType.TEXT);
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				sqlAud += " and " + Constants.COL_COMP_CODE + "=?";
				dbm.param(compCode, DataType.TEXT);
			}
			dbm.insert(sqlAud);
		} catch (Exception e) {
			Log.error(e);
		}			
		dbm.clear();		
	}
	
	 public DynamicUI getDynamicUI(String module, Connection conn) {
		DBManager dbm = new DBManager();
		DynamicUI ui = null;
		try {

			String sql = "select * from dyn_ui_mst where module = " + dbm.param(module, DataType.TEXT);
			ResultSet rs = dbm.select(sql, conn);
			ui = DynamicUI.getDynamicUIFromResultSet(rs);

			rs.close();
			dbm.clear();
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm.clear();
			dbm = null;
		}

		return ui;
	}
	
	public JSONObject genAppBarByViewType(Connection conn, DynamicUI module, UserPrincipalBean principal, String type) {
		return genAppBarByViewType(conn, module, principal, type, null, null);
	}

	public JSONObject genAppBarByViewType(Connection conn, DynamicUI module, UserPrincipalBean principal, String type, JSONObject values) {
		return genAppBarByViewType(conn, module, principal, type, values, null);
	}
	
	public JSONObject genAppBarByViewType(Connection conn, DynamicUI module, UserPrincipalBean principal, String type, JSONObject values,
			String recordName) {
		JSONObject appBar = new JSONObject();

		JSONObject title = new JSONObject();
		JSONArray actions = new JSONArray();
		
		// get granded uri list to filter the app bar
		SysPrivilegeMgr spMgr = new SysPrivilegeMgr();
		List<String> uriList = new ArrayList<String>();
		spMgr.getGrandedURIList(principal.getRoleCode(), uriList);
	
		// get all actions
		String sql = "select * from DYN_ACTION_MST where module = ? and UI_TYPE = ? order by SEQ";
		JSONArray actions1 = new JSONArray();

		DBManager dbm = null;
		DBManager dbm2 = null;
		DBManager dbm3 = null;
		ResultSet rs = null;
		boolean isNewCon = false;
		try {
			dbm = new DBManager();
			if (conn == null) {
				conn = DBAccess.getConnection();
				isNewCon = true;
			}
			dbm.param(module.getModule(), DataType.TEXT);
			dbm.param(type, DataType.TEXT);

			rs = dbm.select(sql, conn);

			while (rs.next()) {

				String id = rs.getString("ID");
				String actionTitle = rs.getString("TITLE");
				String actionType = rs.getString("TYPE");
				
				if (!actionType.toUpperCase().equals("PICKER") 
				&& !actionType.toUpperCase().equals("SEARCHBUTTON") 
				&& !uriList.contains(id+"/"+module.getModule())) {
					continue;			
				}
				
				String defaultValue = rs.getString("default_value");
				String optionsLookup = rs.getString("options_lookup");

				JSONObject action = new JSONObject();
				action.put("id", id);
				action.put("title", actionTitle);
				action.put("type", actionType);
				action.put("value", defaultValue);

				// get options
				if (optionsLookup != null && !optionsLookup.isEmpty()) {
					dbm2 = new DBManager();

					String lookUpStr = "select * from SYS_LOOKUP_MST where LOOKUP_KEY = ? order by SEQ";
					JSONArray options = new JSONArray();
					dbm2.param(optionsLookup, DataType.TEXT);
					ResultSet rs2 = dbm2.select(lookUpStr, conn);

					while (rs2.next()) {
						String lkupValue = rs2.getString("lookup_field");
						String lkupTitle = rs2.getString("name_code");

						JSONObject option = new JSONObject();
						option.put("value", lkupValue);
						option.put("title", lkupTitle);
						options.put(option);
					}
					rs2.close();
					dbm2.clear();
					action.put("options", options);
				}
				actions1.put(action);
			}
			rs.close();
			actions.put(actions1);

			// get title
			if ("table".equals(type)) {
				String filter = module.getFilterKey();
				String typeLookUp = module.getFilterLookup();
				// check if it is a filter
				if (filter != null && !filter.isEmpty() && typeLookUp != null && !typeLookUp.isEmpty()) {
					title.put("id", filter);
					title.put("typeFilter", "picker");

					String defaultValue = null;
					// get options
					dbm3 = new DBManager();
					String lookUpStr = "select * from SYS_LOOKUP_MST where LOOKUP_KEY = ? order by SEQ";
					JSONArray options = new JSONArray();
					dbm3.param(typeLookUp, DataType.TEXT);
					ResultSet rs3 = dbm3.select(lookUpStr, conn);

					while (rs3.next()) {
						String lkupValue = rs.getString("lookup_field");
						String lkupTitle = rs.getString("name_code");

						if (defaultValue == null) {
							defaultValue = lkupValue;
						}

						JSONObject option = new JSONObject();
						option.put("value", lkupValue);
						option.put("title", lkupTitle);
						options.put(option);
					}
					rs3.close();
					dbm3.clear();
					title.put("options", options);
					title.put("primary", defaultValue);
				} else {
					title.put("type", "label");
					title.put("primary", module.getUiName());
				}
			} else {
				title.put("type", "levelTwo");
				title.put("primary", module.getUiName());

				if ("add".equals(type) || recordName == null || recordName.isEmpty()) {
					title.put("secondary", module.getModule().toUpperCase()+".NEW");
				} else {
					title.put("secondary", recordName);
				}
			}

			dbm.clear();
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			dbm2 = null;
			dbm3 = null;
			if(isNewCon){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					Log.error(e);
				}
			}
		}
		appBar.put("title", title);
		appBar.put("actions", actions);
		if (values != null) {
			appBar.put("values", values);
		}

		return appBar;
	}
	
	public JSONArray getActionsByType(Connection conn, DynamicUI module, String type) {
		JSONArray actions = new JSONArray();
		String sql = "select * from DYN_ACTION_MST where module = ? and UI_TYPE = ? order by SEQ";
		JSONArray actions1 = new JSONArray();

		DBManager dbm = null;
		dbm = new DBManager();
		DBManager dbm2 = new DBManager();
		ResultSet rs = null;
		boolean isNewCon = false;
		try {
			if (conn == null) {
				conn = DBAccess.getConnection();
				isNewCon = true;
			}
			dbm.param(module.getModule(), DataType.TEXT);
			dbm.param(type, DataType.TEXT);

			rs = dbm.select(sql, conn);

			while (rs.next()) {
				String id = rs.getString("ID");
				String actionTitle = rs.getString("TITLE");
				String actionType = rs.getString("TYPE");
				String defaultValue = rs.getString("default_value");
				String optionsLookup = rs.getString("options_lookup");

				JSONObject action = new JSONObject();
				action.put("id", id);
				action.put("title", actionTitle);
				action.put("type", actionType);
				action.put("value", defaultValue);

				// get options
				if (optionsLookup != null && !optionsLookup.isEmpty()) {
					dbm2 = new DBManager();

					String lookUpStr = "select * from SYS_LOOKUP_MST where LOOKUP_KEY = ? order by SEQ";
					JSONArray options = new JSONArray();
					dbm2.param(optionsLookup, DataType.TEXT);
					ResultSet rs2 = dbm2.select(lookUpStr, conn);

					while (rs2.next()) {
						String lkupValue = rs2.getString("lookup_field");
						String lkupTitle = rs2.getString("name_code");

						JSONObject option = new JSONObject();
						option.put("value", lkupValue);
						option.put("title", lkupTitle);
						options.put(option);
					}
					rs2.close();
					dbm2.clear();
					action.put("options", options);
				}
				actions1.put(action);
			}
			rs.close();

		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			dbm2 = null;
			if(isNewCon){
				try {
					conn.close();
				} catch (SQLException e) {
					Log.error(e);
				}
			}
		}
		return actions1;
	}
	
	public AppBar genAppBar(Connection conn, DynamicUI module, String type, List<String> uriList) {
		AppBar appBar = null;
		DynamicDAO dynamic = new DynamicDAO();
		AppBarTitle title = null;
		List<List<Field>> actions = new ArrayList<List<Field>>();

		Log.debug("genAppbar module:" + module + " - type:"+type);
		DBManager dbm3 = null;
		try {
			// get all actions
			
			List<Field> acts = dynamic.getActions(conn, module, type, uriList);
			if (acts != null) {
				actions.add(acts);
			}

			// get title
			if ("table".equals(type)) {
				String filter = module.getFilterKey();
				String typeLookUp = module.getFilterLookup();
				// check if it is a filter
				if (filter != null && !filter.isEmpty() && typeLookUp != null && !typeLookUp.isEmpty()) {

					String defaultValue = null;
					// get options
					dbm3 = new DBManager();
					String lookUpStr = "select * from SYS_LOOKUP_MST where LOOKUP_KEY = ? order by SEQ";
					List<Option> options = new ArrayList<Option>();
					dbm3.param(typeLookUp, DataType.TEXT);
					ResultSet rs3 = dbm3.select(lookUpStr, conn);

					while (rs3.next()) {
						String lkupValue = rs3.getString("lookup_field");
						String lkupTitle = rs3.getString("name_code");

						if (defaultValue == null) {
							defaultValue = lkupValue;
						}

						options.add(new Option(lkupTitle, lkupValue));
					}
					rs3.close();
					dbm3.clear();
					
					title = new AppBarTitle("typeFilter", "picker", defaultValue, null, options, null);
				} else {
					title = new AppBarTitle(null, "label", null, module.getUiName(), null, null);
				}
			} else {
				title = new AppBarTitle("/Users", "levelTwo", null, module.getUiName(), null, null);
			}

		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm3 = null;
		}
		appBar = new AppBar(title, actions);
		return appBar;
	}
	
	
	public List<Field> getActions(Connection conn, DynamicUI module, String type, List<String> uriList) {
		List<Field> actions = null;
		String sql = "select * from DYN_ACTION_MST where module = ? and UI_TYPE = ? order by SEQ";

		DBManager dbm = null;
		dbm = new DBManager();
		DBManager dbm2 = new DBManager();
		ResultSet rs = null;
		boolean isNewConn = false;
		try {
			if (conn == null) {
				conn = DBAccess.getConnection();
				isNewConn = true;
			}
			dbm.param(module.getModule(), DataType.TEXT);
			dbm.param(type, DataType.TEXT);

			rs = dbm.select(sql, conn);
			
			while (rs.next()) {
				String id = rs.getString("id");
				String actionType = rs.getString("type");
				
				if (uriList != null && !actionType.toUpperCase().equals("PICKER") 
				&& !actionType.toUpperCase().equals("SEARCHBUTTON") 
				&& !uriList.contains(id+"/"+module.getModule())) {
					continue;			
				}
				
				String actionTitle = rs.getString("title");
				String defaultValue = rs.getString("default_value");
				String optionsLookup = rs.getString("options_lookup");
				
				String triggerType = rs.getString("trigger_type");
				
				Field action = new Field();
				action.setId(id);
				action.setTitle(actionTitle);
				action.setType(actionType);
				action.setValue(defaultValue);
				
				// set trigger
				if (triggerType != null && !triggerType.isEmpty()) {
					List<Trigger> trigger = new ArrayList<Trigger>();
					trigger.add(new Trigger(triggerType, null));
					action.setTrigger(trigger);
				}

				// get options
				if (optionsLookup != null && !optionsLookup.isEmpty()) {
					dbm2 = new DBManager();

					String lookUpStr = "select * from SYS_LOOKUP_MST where LOOKUP_KEY = ? order by SEQ";
					List<Option> options = new ArrayList<Option>();
					dbm2.param(optionsLookup, DataType.TEXT);
					ResultSet rs2 = dbm2.select(lookUpStr, conn);

					while (rs2.next()) {
						String lkupValue = rs2.getString("lookup_field");
						String lkupTitle = rs2.getString("name_code");

						Option option = new Option(lkupTitle, lkupValue);
						options.add(option);
					}
					rs2.close();
					dbm2.clear();
					action.setOptions(options);
				}
				
				if (actions == null) {
					actions = new ArrayList<Field>();
				}
				actions.add(action);
			}
			rs.close();

		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			dbm2 = null;
			if(isNewConn){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					Log.error(e);
				}
			}
		}
		return actions;
	}
	
	public JSONObject getDetailsData(DBManager dbm, Connection conn, String sql, JSONObject template){
		//to do
		JSONObject values = new JSONObject();
		JSONArray items = template.getJSONArray("items");
		ResultSet rs = null; 
		try{
			rs = dbm.select(sql, conn);	
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			ArrayList<String> columns = new ArrayList<String>();
		    for (int i = 1; i < columnCount + 1; i++) {
				String columnName = rsmd.getColumnName(i).toLowerCase();
				columns.add(columnName);
		    }
		    HashMap<String, String> map = new HashMap();
		    while (rs.next()) {
				for (String columnName : columns) {
				    String value = rs.getString(columnName);
				    map.put(columnName, value);
				}
		    }

				for(int i=0; i<items.length(); i++){				
					JSONObject item = items.getJSONObject(i);
					String  id = item.getString("id");
					String key = item.getString("key");				
					values.put(id, map.get(key));
														
				}
		    
			rs.close();
		}catch(Exception e){
		//				
		}finally{
			dbm.clear();
		}		
		return values;
	}
		
	public String checkRecordStatus(DBManager dbm, Connection conn, String id, DynamicUI ui, String compCode){
		
		String status = null;
		try{
			//get data from mst table		
			HashMap<String, String> condMap = new HashMap<String, String>();
			condMap.put(ui.getPrimaryKey(), id);
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule()))
				condMap.put("comp_code", compCode);
			
			boolean hasRecordInTmp =  Function2.hasRecord(ui.getTempTable(),condMap, dbm, conn);
			if(hasRecordInTmp){
				status = getTempTableAction(dbm, ui.getTempTable(), condMap);
			}else{
				status  = "details";
			}							
		}catch(Exception e){
			Log.error(e);
		}finally{
			dbm.clear();	
		}
		return status;
	}
	
	public boolean isEnvDeleteAble(String id){
		boolean result = true;
		
		DBManager dbm = null;
		Connection conn = null;	
		String sql = "select ALLOW_DELETE from sys_env_mst where env_id = ?";
		try{
			conn = DBAccess.getConnection();
			dbm = new DBManager();
			dbm.param(id, DataType.TEXT);		
			String allowDelete = dbm.selectSingle(sql, "ALLOW_DELETE", conn);
			if("N".equals(allowDelete)){
				return false;
			}
		}catch(Exception e){
			Log.error(e);
		}finally{
			dbm.clear();
			dbm = null;
			try{
				conn.close();
			}catch(Exception e){
				Log.error(e);
			}
		}		
		return result;
	}
	
	public boolean isDeleteAble(String module, String id, String primaryKey, boolean compFilter, String compCode){
		boolean allow = true;
		DBManager dbm = null;
		Connection conn = null;
		ResultSet rs = null;
		try{
			conn = DBAccess.getConnection();
			dbm = new DBManager();
			String sql = "select related_table, column_name from dyn_dependence_map where module=? and type = 'D'";
			dbm.param(module, DataType.TEXT);
			
			rs = dbm.select(sql, conn);
			dbm.clear();
			if(rs != null){
				while(rs.next()){
					HashMap<String, String> condMap = new HashMap<String, String>();
					String colName = rs.getString("column_name");
					if(colName == null || "".equals(colName)){
						condMap.put(primaryKey, id);
						Log.debug("dep: use primary key");
					}
					else{
						condMap.put(colName, id);
						Log.debug("dep: use talbe col");
					}
					if(compFilter && !"company".equals(module)){
						Log.debug("has comp filter");
						condMap.put("comp_code", compCode);
					}
					if(Function2.hasRecord(rs.getString("related_table"), condMap, dbm, conn)){
						allow = false;
						break;
					}
				}
			}
			
		}catch(Exception e){
			Log.error(e);
		}finally{
			dbm.clear();
			dbm = null;
			try{
				conn.close();
			}catch(Exception e){
				Log.error(e);
			}
		}
		
		
		return allow;
	}

	public String getProducLinetName(String pl_code, UserPrincipalBean principal, DBManager dbm, Connection conn) throws Exception{
		String sql = " select name_desc"
				   + " from "
				   + " ( select comp_code, pl_code, lang_code, name_desc from Sys_Prod_Line_name union select comp_code,pl_code, lang_code, name_desc from Sys_Prod_Line_name_Tmp ) "
				   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
				   + " and pl_code = " + dbm.param(pl_code, DataType.TEXT)
				   + " and lang_code = " + dbm.param(Constants.LANG_DEFAULT, DataType.TEXT);;
	
		return dbm.selectSingle(sql, "name_desc", conn);
	}
}
