package com.eab.dao.dynamic;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import com.eab.common.Log;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Template;
import com.eab.json.model.Field.Types;
import com.eab.model.dynamic.DynamicUI;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class TemplateDAO {
	public JSONObject getTemplate(String module, DynamicUI ui, Connection conn) {
		JSONArray items = new JSONArray();
		DBManager dbm = null;
		DBManager dbm2 = null;
		ResultSet rs = null;
		try {
			dbm = new DBManager();
			String sql = null;
			sql = "select * from dyn_field_mst where module = " + dbm.param(module, DataType.TEXT) +" and NOT (status = 'D')";
			rs = dbm.select(sql, conn);
			dbm.clear();
			if (rs != null) {

				Map<String, JSONObject> map = new HashMap<String, JSONObject>();
				while (rs.next()) {

					String id = rs.getString("field_id");
					JSONObject item = null;
					if (map.containsKey(id)) {
						item = map.get(id);
					} else {
						item = new JSONObject();
					}
					
					item.put("id", rs.getString("field_id"));
					String key = rs.getString("field_key");
					item.put("key", key);
					boolean isKey = ui.getPrimaryKey().equals(key);

					item.put("isKey", isKey);	
					item.put("type", rs.getString("field_type"));
					item.put("title", rs.getString("field_title"));
					item.put("colWidth", rs.getString("column_width"));
					item.put("listSeq", rs.getString("list_seq"));
					item.put("detailSeq", rs.getString("detail_seq"));
					item.put("mandatory", "Y".equals(rs.getString("mandatory").toUpperCase()));
					item.put("value", rs.getString("default_value"));
					item.put("allowUpdate", rs.getString("allow_update").equals("Y") ? true : false);
					item.put("max", rs.getInt("max_value"));
					item.put("min", rs.getInt("min_value"));				
					item.put("interval", rs.getInt("interval"));
					item.put("validateFunc", rs.getString("VALIDATE_FUNC"));
					item.put("status", rs.getString("status"));
					

					String subType = rs.getString("sub_type");
					String optionsLookup = rs.getString("options_lookup");
					String parent = rs.getString("parent");
					String present = rs.getString("present");
					
					if(rs.getString("field_type") != null && rs.getString("field_type").equals(Types.MULTTEXT)){
						JSONArray subItems = new JSONArray();
						//hard code, to do list
						JSONObject subItem = new JSONObject();
						subItem.put("id", "en");
						subItem.put("title", "LANG.EN");
						subItem.put("mandatory", true);
						
						subItems.put(subItem);
						
						subItem = new JSONObject();
						subItem.put("id", "zh-tw");
						subItem.put("title", "LANG.ZH-TW");
						subItem.put("mandatory", false);
						
						subItems.put(subItem);
						item.put("items", subItems);
					}

					String triggerType = rs.getString("trigger_type");
					Log.debug("field : " + key + " > trigger:" + triggerType);
					if (triggerType != null && !triggerType.isEmpty()) {
						String triggerId = rs.getString("trigger_id");
						String triggerValue = rs.getString("trigger_value");
						JSONObject trigger = new JSONObject();
						trigger.put("type", triggerType);
						trigger.put("id", triggerId);
						trigger.put("value", triggerValue);
						
						JSONArray triggers = new JSONArray();
						triggers.put(trigger);
						item.put("trigger", triggers);
					}
					
					// get option from lookup
					if (optionsLookup != null && present != null && !optionsLookup.isEmpty() && !present.isEmpty()) {
						String filter = "status = 'A'";
						String orderBy = present;
						if (subType != null && !subType.isEmpty() && optionsLookup.toUpperCase().equals("SYS_LOOKUP_MST")) { // it is a lookup table
							filter = "lookup_key = " + dbm.param(subType, DataType.TEXT);
							orderBy = "seq";
						}
						String lookUpStr = "select "+present+" from "+optionsLookup+" where "+filter+" order by "+orderBy;

						JSONArray options = new JSONArray();
						ResultSet rs2 = dbm.select(lookUpStr, conn);
						dbm.clear();
						
						String[] subkey = present.split(",");

						while (rs2.next()) {
							String lkupValue = rs2.getString(1);
							String lkupTitle = rs2.getString(2);

							JSONObject option = new JSONObject();
							option.put("value", lkupValue);
							option.put("title", lkupTitle);
//							options.put(option);
							
							if (subkey.length > 2) {
								option.put("condition", rs2.getString(3));
							}

							options.put(option);
							
						}
						rs2.close();

						item.put("options", options);
					} else {
						item.put("subType", subType);
						item.put("presentation", present);
					}

					// add to item map
					map.put(id, item);

					// add as sub item if it has parent, otherwise add to list
					if (parent != null && !parent.isEmpty()) {
						JSONObject par = null;
						if (map.containsKey(parent)) {
							par = map.get(parent);
						} else {
							par = new JSONObject();
						}

						JSONArray pItems = null;
						if (par.has("items")) {
							pItems = (JSONArray) par.get("items");
						} else {
							pItems = new JSONArray();
						}
						pItems.put(item);
						par.put("items", pItems);

						map.put(parent, par);
					} else {
						// add as item if it is root
						items.put(item);
					}
				}
			}

			rs.close();
			dbm.clear();
		} catch (Exception e) {
			//null
			Log.error(e);
		} finally {
			dbm = null;
			dbm2 = null;
			rs = null;
		}

		JSONObject template = new JSONObject();
		template.put("id", module);
		template.put("items", items);

		return template;
	}
	
	public Template getFields(DynamicUI ui, boolean isRegional, String userCode, String compCode, Connection conn, JsonObject tempJson) {
		if (tempJson == null) {
			tempJson = new JsonObject();
		}
		JsonArray items = new JsonArray();
		DBManager dbm = null;
		ResultSet rs = null;
		try {
			dbm = new DBManager();
			String sql = null;
			sql = "select * from dyn_field_mst where module = " + dbm.param(ui.getModule(), DataType.TEXT) + " and NOT (status = 'D')" ;
			rs = dbm.select(sql, conn);
			dbm.clear();
			if (rs != null) {

				Map<String, JsonObject> map = new HashMap<String, JsonObject>();
				while (rs.next()) {
					String id = rs.getString("field_id");

					JsonObject item = null;
					if (map.containsKey(id)) {
						item = map.get(id);
					} else {
						item = new JsonObject();
					}
					String key = rs.getString("field_key");

					item.addProperty("id", id);
					item.addProperty("key", key);
					item.addProperty("type", rs.getString("field_type"));
					item.addProperty("title", rs.getString("field_title"));
					item.addProperty("colWidth", rs.getString("column_width"));
					item.addProperty("listSeq", rs.getString("list_seq"));
					item.addProperty("detailSeq", rs.getString("detail_seq"));
					item.addProperty("mandatory", "Y".equals(rs.getString("mandatory").toUpperCase()));
					item.addProperty("value", rs.getString("default_value"));
					item.addProperty("allowUpdate", "Y".equals(rs.getString("allow_update").toUpperCase()));
					item.addProperty("max", rs.getInt("max_value"));
					item.addProperty("min", rs.getInt("min_value"));
					item.addProperty("interval", rs.getInt("interval"));
//					item.addProperty("presentation", rs.getString("present"));
					item.addProperty("validateFunc", rs.getString("VALIDATE_FUNC"));

					item.addProperty("status", rs.getString("status"));
					
					String triggerType = rs.getString("trigger_type");
					
					if (triggerType != null && !triggerType.isEmpty()) {
						String triggerId = rs.getString("trigger_id");
						String triggerValue = rs.getString("trigger_value");
						JsonObject trigger = new JsonObject();
						trigger.addProperty("type", triggerType);
						trigger.addProperty("id", triggerId);
						trigger.addProperty("value", triggerValue);
						
						JsonArray triggers = new JsonArray();
						triggers.add(trigger);
						
						item.add("trigger", triggers);
					}
					
					if (key != null && key.equals(ui.getPrimaryKey())) {
						item.addProperty("isKey", true);
					}
				
					String subType = rs.getString("sub_type") != null ? rs.getString("sub_type").toUpperCase() : "";
					String present = rs.getString("present") != null ? rs.getString("present").toUpperCase() : "";
					String optionsLookup = rs.getString("options_lookup");
					String parent = rs.getString("parent");
					
					// get options from option lookup
					if (optionsLookup != null && present != null && !optionsLookup.isEmpty() && !present.isEmpty()) {
						String filter = "status = 'A'";
						String orderBy = present;
						if (optionsLookup.toUpperCase().equals("SYS_LOOKUP_MST")) { // it is a lookup table
							filter = " lookup_key = " + dbm.param(subType, DataType.TEXT);
							orderBy = "seq";
						} else {
							if (optionsLookup.toUpperCase().equals("SYS_COMPANY_MST")
									|| optionsLookup.toUpperCase().equals("SYS_USER_ROLE_MST")
									|| optionsLookup.toUpperCase().equals("SYS_CHANNEL_MST")) {
								if (isRegional) {					
									// no condition for regional user getting company
								} else {
									String[] pres = present.split(",");
									present = "";
									for (int i = 0; i<pres.length; i++) {
										present += (present.isEmpty()?"":",") + "target."+ pres[i].trim();
									}
									
									// otherwise, join user_user_role to get the granded companies
									optionsLookup += " target inner join sys_user_user_role uur on target.comp_code=uur.comp_code"
											 + " and uur.user_code = " + dbm.param(userCode, DataType.TEXT)
											 + " and uur.status = 'A'";
									filter = "target.status = 'A'";
								}
							} else {
								filter += " and comp_code = " + dbm.param(compCode, DataType.TEXT);
							}
							
							item.addProperty("subType", subType);
						}
						String lookUpStr = "select "+present+" from "+optionsLookup+" where "+filter+" order by "+orderBy;

						JsonArray options = new JsonArray();
						ResultSet rs2 = dbm.select(lookUpStr, conn);
						dbm.clear();
						
						String[] subkey = present.split(",");

						while (rs2.next()) {
							String lkupValue = rs2.getString(1);
							String lkupTitle = rs2.getString(2);

							JsonObject option = new JsonObject();
							option.addProperty("value", lkupValue);
							option.addProperty("title", lkupTitle);
							
							if (subkey.length > 2) {
								option.addProperty("condition", rs2.getString(3));
							}

							options.add(option);
							
						}
						rs2.close();

						item.add("options", options);
					} else {
						item.addProperty("subType", subType);
						item.addProperty("presentation", present);
					}
					// add to item map
					map.put(id, item);

					// add as sub item if it has parent, otherwise add to list
					if (parent != null && !parent.isEmpty()) {
						JsonObject par = null;
						if (map.containsKey(parent)) {
							par = map.get(parent);
						} else {
							par = new JsonObject();
						}

						JsonArray pItems = null;
						if (par.has("items")) {
							pItems = (JsonArray) par.get("items");
							pItems.add(item);
						} else {
							pItems = new JsonArray();
							pItems.add(item);
							par.add("items", pItems);
						}
						map.put(parent, par);
					} else {
						// add as item if it is root
						items.add(item);
					}
				}
			}

			rs.close();
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
		}

		tempJson.addProperty("id", ui.getModule());
		tempJson.add("items", items);

		Gson gson = new Gson();
		return gson.fromJson(tempJson, Template.class);
	}
}
