package com.eab.dao.environment;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.profile.UserPrincipalBean;

public class Environment {

	public void envDataInitial(String compCode) throws SQLException  {
		Connection conn = null;
		DBManager dbm = null;
		String sql = "";
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			sql = " delete from sys_env_mst "
				+ " where comp_code = " + dbm.param(compCode, DataType.TEXT)
				+ " and allow_delete = " + dbm.param("N", DataType.TEXT);
			
			dbm.update(sql, conn);
			dbm.clear();
			
			sql = " insert into sys_env_mst(COMP_CODE, ENV_ID, ENV_NAME, STATUS, ALLOW_DELETE, CREATE_DATE, CREATE_BY) values ("
					+ dbm.param(compCode, DataType.TEXT) + "," 							//company code
					+ dbm.param("PROD", DataType.TEXT) + ","							//environment id
					+ dbm.param("Production", DataType.TEXT) + ","						//environment Name
					+ dbm.param("A", DataType.TEXT) + ","			 					//status
					+ dbm.param("N", DataType.TEXT)		 								//allow delete
					+ " , SYSDATE "														//create date
					+ " , " + dbm.param("SYSTEM", DataType.TEXT)						//create by
					+ ") ";
			
			dbm.update(sql, conn);
			dbm.clear();
		} catch(SQLException e) {
			Log.error("{envDataInitial 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{envDataInitial 2} ----------->" + e);
		} finally {
			dbm = null;
			
			if (conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
	}
	
	public Map<String, Object> getPushNotificationWS(HttpServletRequest request, String compCode, String envID)  throws SQLException{
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		Map<String, Object> row = new HashMap<String, Object>();
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select notify_ws_url, notify_msg, is_push_notify "
					   + " from sys_env_mst "
					   + " where comp_code = " + dbm.param(compCode, DataType.TEXT)
					   + " and env_id = " + dbm.param(envID, DataType.TEXT);

			rs = dbm.select(sql, conn);
			
			while (rs.next()) {
				if(rs.getObject("notify_ws_url") != null && !rs.getObject("notify_ws_url").equals(""))
					row.put("notify_ws_url", rs.getObject("notify_ws_url"));	
				
				if(rs.getObject("notify_msg") != null && !rs.getObject("notify_msg").equals(""))
					row.put("notify_msg", rs.getObject("notify_msg"));	
				
				if(rs.getObject("is_push_notify") != null && !rs.getObject("is_push_notify").equals(""))
					row.put("is_push_notify", rs.getObject("is_push_notify"));	
				
				break;
			}
		} catch (SQLException e) {
			Log.error("{Environment getPushNotificationWS 1} ----------->" + e);
		} catch (Exception e) {
			Log.error("{Environment getPushNotificationWS 2} ----------->" + e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return row;
	}
	
	public String getDBPassword(String compCode, String envID) throws SQLException{
		Connection conn = null;
		DBManager dbm = null;
		String result = "";
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select db_password "
					   + " from sys_env_mst "
					   + " where comp_code = " + dbm.param(compCode, DataType.TEXT)
					   + " and env_id = " + dbm.param(envID, DataType.TEXT);

			result = dbm.selectSingle(sql, "db_password", conn);
		} catch (SQLException e) {
			Log.error("{Environment getPushNotificationWS 1} ----------->" + e);
		} catch (Exception e) {
			Log.error("{Environment getPushNotificationWS 2} ----------->" + e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return result;	
	}
	
}
