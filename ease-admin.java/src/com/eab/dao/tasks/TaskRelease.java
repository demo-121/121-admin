package com.eab.dao.tasks;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.eab.common.Function;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.system.Name;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.profile.UserPrincipalBean;

public class TaskRelease {

	public ArrayList<Map<String, Object>> getUnassignedReleaseList(HttpServletRequest request, UserPrincipalBean principal, String criteria, String sortBy, String sortDir, Language langObj) throws SQLException {		
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			rs = searchUnassigedReleaseList(criteria, dbm, principal, sortBy, sortDir, conn);
			return getList(rs, request, principal, langObj);			
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}

	public ResultSet searchUnassigedReleaseList(String criteria, DBManager dbm, UserPrincipalBean principal, String sortBy, String sortDir, Connection conn)throws Exception {
		ResultSet rs = null;
			   										   
		String sql = " select * "
				   + " from v_release"
				   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
				   + " and status in ('P','T')"//Pending or Testing
				   + " and is_publishing = 'N'";//Not publishing
				   
		//Searching criteria by user input
		if(criteria != null && !criteria.equals("")){	
			criteria = '%' + criteria.toUpperCase() +'%';

			sql += " and (upper(release_id) like "+ dbm.param(criteria, DataType.TEXT) 
				 + " or upper(release_name) like " + dbm.param(criteria, DataType.TEXT) +")";
		}

		if (!sortBy.equals("")){
			if (sortDir.equals("A")) {
				sortDir = "asc";
			} else {
				sortDir = "desc";
			}
						
			if (sortBy.equals("status")){
				sql += " order by status " + sortDir +" ,target_release_date " + sortDir;		
			} else {
				sql += " order by " + sortBy + " " + sortDir;	
			}
		} else {
			//Default sorting
			sql += " order by upd_date desc";	
		}		

		rs = dbm.select(sql, conn);
		
		return rs;
	}

	private ArrayList<Map<String, Object>> getList(ResultSet rs, HttpServletRequest request, UserPrincipalBean principal, Language langObj) throws SQLException {
	    ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
	    String status = "";
	    String target_release_date = "";
	    SimpleDateFormat dateformatter;
		
	    while (rs.next()) {
	    	Map<String, Object> row = new HashMap<String, Object>();
	    
	    	if(rs.getObject("status") != null && !rs.getObject("status").equals("")){	    		
	    		switch (rs.getObject("status").toString()) {
				case "P":  //Pending
					status = langObj.getNameDesc("PENDING");
					break;
				case "T":  //Testing
					status = langObj.getNameDesc("TESTING");
					break;
	    		}
	    	}
	    		    	
			dateformatter = Function.getDateFormat(principal);
									
			try {
				target_release_date = dateformatter.format(rs.getDate("target_release_date"));		
			} catch (Exception e) {
				
			}
	    	
			row.put("id", rs.getObject("RELEASE_ID"));
			row.put("release_id", rs.getObject("RELEASE_ID"));
			row.put("release_name", rs.getObject("RELEASE_NAME"));
			row.put("status", status + " " + langObj.getNameDesc("FOR") + " " + target_release_date);
			
			Map<String, String> icon = new HashMap<String , String>();
			
			if(rs.getObject("is_Publishing").toString().equals("Y")){
				icon.put("icon", "cloud_upload");
				icon.put("color", "Colors.green500");
				row.put("font_icon", icon);
			}else if(rs.getObject("is_Failed").toString().equals("Y")){
				icon.put("icon", "error");
				icon.put("color", "Colors.red500");
				row.put("font_icon", icon);
				row.put("tooltipId", rs.getObject("failed_reason"));
			}			
				     
			list.add(row);
	    }
	    
	    return list;
	}
	
}
