package com.eab.dao.tasks;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.dynTemplate.DynData;
import com.eab.dao.dynTemplate.DynTemplate;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Option;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.tasks.TaskBean;
import com.eab.security.LoginExclusion;
import com.eab.security.SysPrivilegeMgr;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Tasks {
	
	public static class FuncCode {
		public final static String PROD = "FN.PRO.PRODS";
		public final static String BI = "FN.PRO.BENEF";
		public final static String NEED = "FN.NEEDS";
		public final static String FUND = "FN.PRO.FUNDS";
		public final static String FORM = "FN.PRO.FORMS";
		public final static String CAMP = "FN.PRO.CAMPS";
	}
	
	public List<Option> getTaskTypes(Connection conn){
		DBManager dbm = null;
		ResultSet rs = null;		
		List<Option> options = new ArrayList<Option>();
		boolean isNew = false;
		try{
			if(conn == null){
				isNew = true;
				conn =  DBAccess.getConnection();
			}
			dbm = new DBManager();
			String sql =  " SELECT "
					+ " MIN(task_mst.task_code) AS id,"
					+ " MIN(name_table.name_desc) AS name_desc,"
					+ " MIN(name_table.func_code) AS func_code"
					+ " FROM "
					+ " task_mst "
					+ " INNER JOIN "
					+     "("
					+     "SELECT "
					+     " sys_func_mst.func_code, sys_name_mst.name_desc as name_desc "
					+     " FROM " 
					+     " sys_func_mst "
					+     " INNER JOIN "
					+     " sys_name_mst "
					+     " ON "
					+     " sys_func_mst.name_code = sys_name_mst.name_code "
					+     ") name_table "
					+   " ON task_mst.func_code = name_table.func_code "
					+ " GROUP BY task_mst.func_code "
					+ " ORDER BY name_desc";
			
			rs = dbm.select(sql, conn);
			if(rs != null){
				while(rs.next()){
					String lkupValue = rs.getString("func_code");
					String lkupTitle = rs.getString("name_desc");
					Option option = new Option(lkupTitle, lkupValue);
					options.add(option);
				}
			}
		}catch(Exception e){
			
		}finally{
			if(isNew){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return options;
	}
	
	public int getTaskId( Connection conn, String compCode, String module, String docCode, int version){
		DBManager dbm = null;
		ResultSet rs = null;
		
		try {	
			dbm = new DBManager();
			DynData dDao = new DynData();
			String func = dDao.getFuncCodeByModule(conn, module);
			String sql = " select task_code "
						+" from task_mst where "
						+" comp_code = "+  dbm.param(compCode, DataType.TEXT)
						+" and func_code = " + dbm.param(func, DataType.TEXT)
						+" and item_code = " + dbm.param(docCode, DataType.TEXT)
						+" and ver_no = " + dbm.param(version, DataType.INT);
			rs = dbm.select(sql, conn);
			if(rs != null){
				if(rs.next()){
					return rs.getInt("task_code");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return -1;
	}

	public ArrayList<Map<String, Object>> getTaskList(HttpServletRequest request, UserPrincipalBean principal, String criteria, String typeFilter, String statusFilter, String sortBy, String sortDir, Language langObj) throws SQLException {				
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			rs = searchTaskList(criteria, typeFilter, statusFilter, dbm, principal, sortBy, sortDir, conn);
			return getList(rs, request, principal, langObj);
		} catch (SQLException e) {
			Log.error("{Tasks getTaskList 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{Tasks getTaskList 2} ----------->" + e);
			e.printStackTrace();
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return null;
	}

	public ResultSet searchTaskList(String criteria, String typeFilter, String statusFilter, DBManager dbm, UserPrincipalBean principal, String sortBy, String sortDir, Connection conn)throws Exception {
		ResultSet rs = null;

		/////////////////////////////////////////////////////////////////////////////////////////////////////			   										   
		String sql = " select a.*, "
				   + " b.name_desc as prod_name,"
				   + " f.name_desc as need_name"
			   	   + " from v_task_list a"
				
			   	   //Get product name
		           + " left join prod_name b" 
		           + " on a.item_code = b.prod_code"
		           + " and b.lang_code = " + dbm.param(principal.getLangCode(), DataType.TEXT)
		           + " and a.comp_code = b.comp_code"
		           + " and a.ver_no = b.version"
		           + " and b.name_type = 'PRODNAME'"
		           
		           //Get needs name 
		           + " left join ("
		           + " select d.name_desc, c.lookup_field from sys_lookup_mst c"
		           + " inner join sys_name_mst d"
		           + " on c.name_code = d.name_code"
		           + " and d.lang_code = " + dbm.param(principal.getLangCode(), DataType.TEXT)
		           + " and c.lookup_key = 'NEED_NAME') f"
		           + " on a.item_code = f.lookup_field" 
		          
		           + " where a.comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT);     
		 		   
		//Task status
		if(statusFilter != null && !statusFilter.equals("")){
			if(!statusFilter.equals("*"))
				sql += " and a.status_code = " + dbm.param(statusFilter, DataType.TEXT); 	
		}
		
		//Item type code
		if(typeFilter != null && !typeFilter.equals("") && !typeFilter.equals("A")){
			if(!typeFilter.equals("*"))
				sql += " and a.func_code = " + dbm.param(typeFilter, DataType.TEXT);
		}
		
		//Searching criteria by user input
		if(criteria != null && !criteria.equals("")){	
			criteria = '%' + criteria.toUpperCase() +'%';

			sql += " and (upper(a.item_code) like "+ dbm.param(criteria, DataType.TEXT) 
				 + " or upper(a.task_desc) like "+ dbm.param(criteria, DataType.TEXT)
				 + " or upper(b.name_desc) like "+ dbm.param(criteria, DataType.TEXT)//Product name
				 + " or upper(f.name_desc) like "+ dbm.param(criteria, DataType.TEXT)//Need name
				 + " or upper(a.ref_no) like " + dbm.param(criteria, DataType.TEXT) +")";
		}
			
		if (!sortBy.equals("")) {
			if (sortDir.equals("A"))
				sortDir = "asc";
			else
				sortDir = "desc";
			
			if (sortBy.equals("upd_date"))
				sortBy = "a.upd_datetime";
						
			sql += " order by a." + sortBy + " " + sortDir;	
		} else {
			//Default sorting
			sql += " order by a.upd_datetime desc";	
		}		
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		rs = dbm.select(sql, conn);
				
		return rs;
	}
	
	public Map<String, Object> getTaskById(int id, String item_code, int version, String module, String channel_code, UserPrincipalBean principal) throws SQLException{
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		Map<String, Object> task = null;
		Language langObj = new Language(principal.getUITranslation());
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
			
			DynData dynData = new DynData();
			String funcCode = "";
			
			String sql = "select task_code, func_code, item_code, ver_no, channel_code, task_desc, ref_no, eff_date, upd_date, create_date, upd_by, create_by, status, status_code from v_task_list where 1=1";
			
			if(id >= 0) {
				sql += " and task_code = " + dbm.param(id, DataType.INT);
			} else {
				funcCode = dynData.getFuncCodeByModule(conn, module);
				sql += " and item_code = " + dbm.param(item_code, DataType.TEXT);
				sql += " and ver_no = " + dbm.param(version, DataType.INT);
				sql += " and func_code = " + dbm.param(funcCode, DataType.TEXT);
			}
			
			if(channel_code != null && !channel_code.equals(""))
				sql += " and channel_code = " + dbm.param(channel_code, DataType.TEXT);

			sql += " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT);
			
			rs = dbm.select(sql, conn);
			dbm.clear();
			if(rs.next()){
			    SimpleDateFormat dateformatter = Function.getDateFormat(principal);
				SimpleDateFormat dateTimeformatter = Function.getDateTimeFormat(principal);
				String upd_datetime = "";
				String create_datetime = "";
				
				try {upd_datetime = dateTimeformatter.format(rs.getDate("upd_date"));} catch (Exception e) {}
				
				try {create_datetime = dateTimeformatter.format(rs.getDate("create_date"));} catch (Exception e) {}
				
				String upd = (rs.getString("UPD_BY") != null && !upd_datetime.isEmpty())? 
						Function2.getUserName(rs.getString("UPD_BY")) + " " + langObj.getNameDesc("ON") + " " +  upd_datetime : "-";
				
				String create = (rs.getString("create_By") != null && !create_datetime.isEmpty())? 
						Function2.getUserName(rs.getString("create_By")) + " " + langObj.getNameDesc("ON") + " " +  create_datetime : "-";
				funcCode = rs.getString("func_code");
				task = new HashMap<String, Object>();
				List<Object> taskId = new ArrayList<Object>();
				int verNo = rs.getInt("ver_no");
				String itemCode = rs.getString("item_code");
				String channelCode = rs.getString("channel_code");
				taskId.add(rs.getInt("task_code"));
				task.put("taskId", gson.toJsonTree(taskId));
				task.put("task_desc", rs.getString("task_desc"));
				task.put("func_code", funcCode);
				task.put("item_code", itemCode);
				task.put("channel_code", channelCode);
				task.put("ver_no", verNo);
				task.put("ref_no", rs.getString("ref_no"));
				task.put("status", rs.getString("status").equals("") ? "" : langObj.getNameDesc(rs.getString("status")));			
				task.put("statusCode", rs.getString("status_code"));
				task.put("effDate", rs.getDate("eff_date").getTime());
				task.put("upd", upd);
				task.put("create", create);
				
				if(module == null){
					module = dynData.getModuleByFuncCode(conn, funcCode);
				}
				
				//base version
				sql = null;
				String baseVersion = null;
				switch(funcCode){
					case Tasks.FuncCode.NEED:
						sql = "select base_version from need_trx where need_code=" + dbm.param(itemCode, DataType.TEXT) + " and version=" + dbm.param(verNo, DataType.INT) + " and comp_code=" + dbm.param(principal.getCompCode(), DataType.TEXT) + " and channel_code=" + dbm.param(channelCode, DataType.TEXT);
						baseVersion = dbm.selectSingle(sql, "base_version");
						break;
					default:
						JSONObject data = dynData.getDynTrxData(principal, conn, itemCode, verNo, module);
						if(data.has("baseVersion")){
							baseVersion = data.get("baseVersion").toString();
						}
				}
				if(sql != null){
					
					task.put("base_version", baseVersion == null || "".equals(baseVersion)?"-":baseVersion);
				}
			}
		} catch (SQLException e) {
			Log.error("{Tasks getTaskById 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{Tasks getTaskById 2} ----------->" + e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return task;
	}

	public ArrayList<Map<String, Object>> getList(ResultSet rs, HttpServletRequest request, UserPrincipalBean principal, Language langObj) throws Exception {
	    ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
	    String status = "";
	    String release = "";
	    String type = "";
	    String name = "";
	    String compCode = "";
	    String version = "";
	    String item_code = "";
	    String func_code = "";
	    String upd_date = "";
	    String create_date = "";
	    String upd_datetime = "";
	    String create_datetime = "";
	    String eff_date = "";
	    SimpleDateFormat dateformatter;
	    SimpleDateFormat dateTimeformatter;
	    
		Function2 func2 = new  Function2(); 
		Map<String, String> uriMap = func2.getFuncUriMap(); 
		UserPrincipalBean p = Function.getPrincipal(request);
		LoginExclusion exclude = new LoginExclusion(p);
	    	    
	    while (rs.next()) {
	    	Map<String, Object> row = new HashMap<String, Object>();
	    
	    	if(rs.getObject("status") != null && !rs.getObject("status").equals(""))
	    		status = rs.getObject("status").toString();

	    	if(rs.getObject("release") != null && !rs.getObject("release").equals(""))
	    		release = rs.getObject("release").toString();    

	    	if(rs.getObject("type") != null && !rs.getObject("type").equals(""))
	    		type = rs.getObject("type").toString();    
	    	
	    	if(rs.getObject("comp_code") != null && !rs.getObject("comp_code").equals(""))
	    		compCode = rs.getObject("comp_code").toString();	
	    	
	    	if(rs.getObject("ver_no") != null && !rs.getObject("ver_no").equals(""))
	    		version = rs.getObject("ver_no").toString();		
	    	
	    	if(rs.getObject("item_code") != null && !rs.getObject("item_code").equals(""))
	    		item_code = rs.getObject("item_code").toString();		
	    	
	    	if(rs.getObject("func_code") != null && !rs.getObject("func_code").equals(""))
	    		func_code = rs.getObject("func_code").toString();		
	    		    	
	    	if(func_code.equals(FuncCode.PROD)){//Product Name
	    		if(rs.getObject("prod_name") != null && !rs.getObject("prod_name").equals(""))
	    			name = rs.getObject("prod_name").toString();
	    	} else if (func_code.equals(FuncCode.NEED)) {//Need Name
	    		if(rs.getObject("need_name") != null && !rs.getObject("need_name").equals(""))
	    			name = rs.getObject("need_name").toString();
	    	}
	    	
			dateformatter = Function.getDateFormat(principal);
			dateTimeformatter = Function.getDateTimeFormat(principal);
									
			try {upd_date = dateformatter.format(rs.getDate("upd_date"));} catch (Exception e) {}
			try {create_date = dateformatter.format(rs.getDate("create_date"));} catch (Exception e) {}	
			try {upd_datetime = dateTimeformatter.format(rs.getDate("upd_date"));} catch (Exception e) {}
			try {create_datetime = dateTimeformatter.format(rs.getDate("create_date"));} catch (Exception e) {}
			try {eff_date = dateformatter.format(rs.getDate("eff_date"));} catch (Exception e) {}			
			
			row.put("comp_code", compCode);
			row.put("version", version);
			row.put("prod_code", item_code);
			row.put("id", rs.getObject("task_code"));
			row.put("item_code", item_code + " " + name + " (v." + version + ")");
			row.put("task_desc", rs.getObject("task_desc"));
			row.put("ref_no", rs.getObject("ref_no"));
			row.put("type", type.equals("") ? "" : langObj.getNameDesc(type));
			row.put("status", status.equals("") ? "" : langObj.getNameDesc(status));
			row.put("tooltipId", release.equals("") ? "" : langObj.getNameDesc("RELEASE_ASSIGNED_TO") + " " + rs.getObject("release_id") + " - " + rs.getObject("release_name") + " (" +langObj.getNameDesc(release) +")");			
			row.put("upd_date", upd_date);
			row.put("upd_datetime", upd_datetime);
			row.put("statusCode", rs.getObject("status_code"));
			row.put("effDate", eff_date);
			row.put("createDate", create_date);
			row.put("createDatetime", create_datetime);	     
			Boolean canAccess = true;
			try{
				String funcUri =(String) uriMap.get(func_code); 
				if(!funcUri.equals(""))
					canAccess = exclude.canAccess(request, funcUri); 
				row.put("accessible", canAccess);	
			}catch(Exception e){
				Log.error("func_code : " + func_code);
			}
			list.add(row);
			
	        //Reset all values before going next record
		    status = "";
		    release = "";
		    type = "";
		    name = "";
		    version = "";
		    item_code = "";
		    func_code = "";
		    upd_date = "";
		    create_date = "";
		    upd_datetime = "";
		    create_datetime = "";
		    eff_date = "";
	    }
	    
	    return list;
	}
		
	//Add New Task
	public Boolean addTask(UserPrincipalBean principal, TaskBean task) throws SQLException {
		Connection conn = null;
		boolean output = false;
		try{
			conn = DBAccess.getConnection();	
			output =  addTask(principal, task, conn);
		} catch (SQLException e) {
			Log.error("{Tasks addTask 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{Tasks addTask 2} ----------->" + e);
		} finally {
			if (conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		return output;
	}
	

	public Boolean addTask(UserPrincipalBean principal, TaskBean task, Connection conn) throws SQLException {
		boolean output = false;
		DBManager dbm = null;
		String sql = "";
		
		try {
			dbm = new DBManager(); 
			Date _effDate = new Date(task.getEffDate().getTime());
						
			sql = " insert into task_mst(COMP_CODE, FUNC_CODE, ITEM_CODE, VER_NO, TASK_DESC, EFF_DATE, REF_NO, STATUS, CREATE_DATE, CREATE_BY, UPD_DATE, UPD_BY, CHANNEL_CODE) values ("
					+ dbm.param(principal.getCompCode(), DataType.TEXT) + "," 	//company code
					+ dbm.param(task.getFuncCode(), DataType.TEXT) + "," 		//function code
					+ dbm.param(task.getItemCode(), DataType.TEXT) + "," 		//item code
					+ dbm.param(task.getVerNo(), DataType.TEXT) + "," 			//version number
					+ dbm.param(task.getTaskDesc(), DataType.TEXT) + "," 		//task description
					+ dbm.param(_effDate, DataType.DATE) + "," 					//task effective date
					+ ((task.getRefNo()==null)?"NULL":dbm.param(task.getRefNo(), DataType.TEXT)) + "," 			//reference number
					+ dbm.param("U", DataType.TEXT) + "," 						//status
					+ "sysdate, " 												//create date
					+ dbm.param(principal.getUserCode(), DataType.TEXT)	+ "," 	//create by
					+ "sysdate, " 												//Update date
					+ dbm.param(principal.getUserCode(), DataType.TEXT)	+ "," 	//Update by
					+ ((task.getChannelCode()==null)?"NULL":dbm.param(task.getChannelCode(), DataType.TEXT))			//channel code
					+ ")";
				
			output = dbm.insert(sql, conn);	
		} catch (SQLException e) {
			Log.error("{Tasks addTask 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{Tasks addTask 2} ----------->" + e);
		} finally {
			dbm = null;
		}
		
		return output;
	}		
	
	//Update Existing Task
	public Boolean updateTask(UserPrincipalBean principal, TaskBean task) throws SQLException {
		Connection conn = null;
		boolean output = false;
		try{
			conn = DBAccess.getConnection();	
			output =  updateTask(principal, task, conn);
		} catch (SQLException e) {
			Log.error("{Tasks addTask 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{Tasks addTask 2} ----------->" + e);
		} finally {
			if (conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		return output;
	}
	
	public Boolean updateTask(UserPrincipalBean principal, TaskBean task, Connection conn) throws SQLException {
		boolean output = false;
		DBManager dbm = null;
		String sql = "";
		int count = 0;
		
		try {
			dbm = new DBManager(); 
			Date _effDate = new Date(task.getEffDate().getTime());
			
			sql = " update task_mst set "
				+ " task_desc = "+ dbm.param(task.getTaskDesc(), DataType.TEXT) + "," 							//Task description
				+ " ref_no = "+ ((task.getRefNo()==null)?"NULL":dbm.param(task.getRefNo(), DataType.TEXT)) + ","//Reference number
				+ " eff_Date = "+ dbm.param(_effDate, DataType.DATE) + ","									//Effective date
				+ " upd_date = sysdate,"																		//Update date
				+ " upd_by = "+ dbm.param(principal.getUserCode(), DataType.TEXT)								//Update by
				+ " where task_code = "+ dbm.param(task.getTaskCode(), DataType.TEXT);							//Task Code
				
			count = dbm.update(sql, conn);	
			
			if (count > 0)
				output = true;
		} catch (SQLException e) {
			Log.error("{Tasks updateTask 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{Tasks updateTask 2} ----------->" + e);
		} finally {
			dbm = null;
		}
		
		return output;
	}
	
	public Boolean updateTaskDocCode(Connection conn, UserPrincipalBean principal, int taskId, String newItemCode) throws SQLException {
		boolean output = false;
		DBManager dbm = null;
		String sql = "";
		int count = 0;
		
		try {
			dbm = new DBManager();
			
			sql = " update task_mst set "
				+ " item_code = " + dbm.param(newItemCode, DataType.TEXT) + ", "					//Update doc code
				+ " upd_date = sysdate,"															//Update date
				+ " upd_by = "+ dbm.param(principal.getUserCode(), DataType.TEXT)					//Update by
				+ " where task_code = "+ dbm.param(taskId, DataType.INT);							//Task Code
				
			count = dbm.update(sql, conn);	
			
			if (count > 0)
				output = true;
		} catch (SQLException e) {
			Log.error("{Tasks updateTaskDocCode 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{Tasks updateTaskDocCode 2} ----------->" + e);
		} finally {
			dbm = null;
		}
		
		return output;
	}
	
	//Assign Tasks
	public Boolean assignTasks(int releaseID,  List<Object> rows, UserPrincipalBean principal) throws SQLException {
		boolean output = false;
		Connection conn = null;
		DBManager dbm = null;
		String sql = "";
		int taskCode = -1;
		int count = 0;
		
		try {
			dbm = new DBManager();
			
			//Get DB connection for update with begin transaction
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			
			for(int i = 0 ; i < rows.size(); i++){
				taskCode = (int) rows.get(i);
				
				sql = " delete from release_task"
					+ " where task_code = " + dbm.param(taskCode, DataType.INT);

				dbm.update(sql, conn);
				dbm.clear();

				sql = " insert into release_task(release_id, task_code, create_date, create_by) values ("
					+ dbm.param(releaseID, DataType.INT) + "," 			//release id
					+ dbm.param(taskCode, DataType.INT)					//task code
					+ ",sysdate, "										//create date
					+ dbm.param(principal.getUserCode(), DataType.TEXT)	//create by
					+ ")";
				
				output = dbm.insert(sql, conn);	
				dbm.clear();
				
				if (output == false)
					break;
			
				sql = " update task_mst set "
					+ " status = 'A'," 													//Assigned
					+ " upd_date = sysdate,"											//update date
					+ " upd_by = "+ dbm.param(principal.getUserCode(), DataType.TEXT)	//update by
					+ " where task_code = "+ dbm.param(taskCode, DataType.INT);			//task code

				count = dbm.update(sql, conn);	
				
				if (count < 1)
					break;
				
				dbm.clear();
			}			
			
			if (output && count > 0)
				conn.commit();
			else 
				conn.rollback();
			
		} catch (SQLException e) {
			Log.error("{Tasks assignTasks 1} ----------->" + e);
			conn.rollback();
		} catch(Exception e) {
			Log.error("{Tasks assignTasks 2} ----------->" + e);
			conn.rollback();
		} finally {
			dbm = null;
			
			if (conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
		return output;
	}		
	
	//Un-assign Tasks
	public Boolean unassignTasks(List<Object> taskList, UserPrincipalBean principal) throws SQLException {
		boolean output = false;
		Connection conn = null;
		DBManager dbm = null;
		String sql = "";
		
		try {
			dbm = new DBManager();
			
			// Get DB connection for update with begin transaction
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			
			String _taskList = "";
			for(int i = 0 ; i < taskList.size(); i++){
				_taskList += (int) taskList.get(i) + ",";
			}
			
			//Remove the last comma from a string
			_taskList = _taskList.substring(0, _taskList.length() - 1);
						
			/////////////////////////////////////////////////////////////////////////////////////////////////////////
			sql = " update task_mst set "
				+ " status = 'U'," 	//Unassigned
				+ " upd_date = sysdate,"
				+ " upd_by = "+ dbm.param(principal.getUserCode(), DataType.TEXT)
				+ " where task_code in ("+_taskList+")";

			dbm.update(sql, conn);
			dbm.clear();
			/////////////////////////////////////////////////////////////////////////////////////////////////////////

			/////////////////////////////////////////////////////////////////////////////////////////////////////////
			sql = " delete from release_task"
				+ " where task_code in ("+_taskList+")";

			dbm.update(sql, conn);	
			dbm.clear();
			/////////////////////////////////////////////////////////////////////////////////////////////////////////
						
			conn.commit();
			output = true;
		} catch (SQLException e) {
			Log.error("{Tasks unassignTasks 1} ----------->" + e);
			conn.rollback();
		} catch(Exception e) {
			Log.error("{Tasks unassignTasks 2} ----------->" + e);
			conn.rollback();
		} finally {
			dbm = null;
			
			if (conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
		return output;
	}	
	
	//Suspend Tasks
	public Boolean suspendTasks(List<Object> taskList, UserPrincipalBean principal) throws SQLException {
		boolean output = false;
		Connection conn = null;
		DBManager dbm = null;
		String sql = "";
		
		try {
			dbm = new DBManager();
			
			// Get DB connection for update with begin transaction
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			
			String _taskList = "";
			for(int i = 0 ; i < taskList.size(); i++){
				_taskList += (int) taskList.get(i) + ",";
			}
			
			//Remove the last comma from a string
			_taskList = _taskList.substring(0, _taskList.length() - 1);
						
			/////////////////////////////////////////////////////////////////////////////////////////////////////////
			sql = " update task_mst set "
				+ " status = 'D'," 	//Suspend
				+ " upd_date = sysdate,"
				+ " upd_by = "+ dbm.param(principal.getUserCode(), DataType.TEXT)
				+ " where task_code in ("+_taskList+")";

			dbm.update(sql, conn);
			dbm.clear();
			/////////////////////////////////////////////////////////////////////////////////////////////////////////

			/////////////////////////////////////////////////////////////////////////////////////////////////////////
			sql = " delete from release_task"
				+ " where task_code in ("+_taskList+")";

			dbm.update(sql, conn);	
			dbm.clear();
			/////////////////////////////////////////////////////////////////////////////////////////////////////////
						
			conn.commit();
			output = true;
		} catch (SQLException e) {
			Log.error("{Tasks suspendTasks 1} ----------->" + e);
			conn.rollback();
		} catch(Exception e) {
			Log.error("{Tasks suspendTasks 2} ----------->" + e);
			conn.rollback();
		} finally {
			dbm = null;
			
			if (conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
		return output;
	}	

	//Resume Tasks
	public Boolean resumeTasks(List<Object> taskList, UserPrincipalBean principal) throws SQLException {
		return unassignTasks(taskList, principal);
	}

    //Re-assign Tasks
    public Boolean reassignTasks(List<Object> taskList, UserPrincipalBean principal) throws SQLException {
        return unassignTasks(taskList, principal);
    }        

    //Get product name
	public String getProductName(String version, String prod_code, UserPrincipalBean principal, DBManager dbm, Connection conn) throws Exception{
		String sql = " select name_desc"
				   + " from prod_name"
				   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
				   + " and prod_code = " + dbm.param(prod_code, DataType.TEXT)
				   + " and version = " + dbm.param(version, DataType.TEXT)
				   + " and name_type = 'PRODNAME'"
				   + " and lang_code = " + dbm.param(Constants.LANG_DEFAULT, DataType.TEXT); 
	
		return dbm.selectSingle(sql, "name_desc", conn);
	}

    //Get benefits illustartions name
    public String getBIName(String version, String prod_code, UserPrincipalBean principal, DBManager dbm, Connection conn) throws Exception{
		String result = "";
		
//		String sql = " select name_desc"
//				   + " from prod_name"
//				   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
//				   + " and prod_code = " + dbm.param(prod_code, DataType.TEXT)
//				   + " and version = " + dbm.param(version, DataType.TEXT)
//				   + " and name_type = 'PRODNAME'"
//				   + " and lang_code = " + dbm.param(Constants.LANG_DEFAULT, DataType.TEXT); 
//	
//		result = dbm.selectSingle(sql, "name_desc", conn);
//	
		return result;
	}
    
    //Get fund name
    public String geFundName(String version, String prod_code, UserPrincipalBean principal, DBManager dbm, Connection conn) throws Exception{
		String result = "";
		
//		String sql = " select name_desc"
//				   + " from prod_name"
//				   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
//				   + " and prod_code = " + dbm.param(prod_code, DataType.TEXT)
//				   + " and version = " + dbm.param(version, DataType.TEXT)
//				   + " and name_type = 'PRODNAME'"
//				   + " and lang_code = " + dbm.param(Constants.LANG_DEFAULT, DataType.TEXT); 
//	
//		result = dbm.selectSingle(sql, "name_desc", conn);
//	
		return result;
	}

    //Get application form name
    public String geAppFormName(String version, String prod_code, UserPrincipalBean principal, DBManager dbm, Connection conn) throws Exception{
		String result = "";
		
//		String sql = " select name_desc"
//				   + " from prod_name"
//				   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
//				   + " and prod_code = " + dbm.param(prod_code, DataType.TEXT)
//				   + " and version = " + dbm.param(version, DataType.TEXT)
//				   + " and name_type = 'PRODNAME'"
//				   + " and lang_code = " + dbm.param(Constants.LANG_DEFAULT, DataType.TEXT); 
//	
//		result = dbm.selectSingle(sql, "name_desc", conn);
//	
		return result;
	}

    //Get campaigns name
    public String getCampaignsName(String version, String prod_code, UserPrincipalBean principal, DBManager dbm, Connection conn) throws Exception{
		String result = "";
		
//		String sql = " select name_desc"
//				   + " from prod_name"
//				   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
//				   + " and prod_code = " + dbm.param(prod_code, DataType.TEXT)
//				   + " and version = " + dbm.param(version, DataType.TEXT)
//				   + " and name_type = 'PRODNAME'"
//				   + " and lang_code = " + dbm.param(Constants.LANG_DEFAULT, DataType.TEXT); 
//	
//		result = dbm.selectSingle(sql, "name_desc", conn);
//	
		return result;
	}

    //Get Keys
    public Map<String, Object> getRedirectKeys(UserPrincipalBean principal, String taskID) throws SQLException {
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		Map<String, Object> list = new HashMap<String, Object>();
		
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = "select func_code, item_code, ver_no, channel_code from task_mst where task_code = " + dbm.param(taskID, DataType.TEXT); 			
			rs = dbm.select(sql, conn); 
				
		    while (rs.next()) {
		    	if(rs.getObject("ver_no") != null && !rs.getObject("ver_no").equals(""))
		    		list.put("version", rs.getObject("ver_no").toString());
		    	
		    	if(rs.getObject("item_code") != null && !rs.getObject("item_code").equals(""))
		    		list.put("item_code", rs.getObject("item_code").toString());
		    	
		    	if(rs.getObject("func_code") != null && !rs.getObject("func_code").equals(""))
		    		list.put("func_code", rs.getObject("func_code").toString());
		    	
		    	if(rs.getObject("channel_code") != null && !rs.getObject("channel_code").equals(""))
		    		list.put("channel_code", rs.getObject("channel_code").toString());
		    }
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return list;
	}
    
    //Get item name based on the fund code
    public String getItemName(String func_code, String version, String prod_code, UserPrincipalBean principal) throws Exception{
        String itemName = "";
        Connection conn = null;
        DBManager dbm = null;    
                
        try {    
            dbm = new DBManager();
            conn = DBAccess.getConnection();
            
            switch(func_code){
            case FuncCode.PROD://Products
                itemName = getProductName(version, prod_code, principal, dbm, conn);
                break;
            case FuncCode.BI://Benefits illustartions
                itemName = getBIName(version, prod_code, principal, dbm, conn);
                break;
            case FuncCode.FUND://Funds
                itemName = geFundName(version, prod_code, principal, dbm, conn);
                break;            
            case FuncCode.FORM://Application Forms
                itemName = geAppFormName(version, prod_code, principal, dbm, conn);
                break;
            case FuncCode.CAMP://Campaigns
                itemName = getCampaignsName(version, prod_code, principal, dbm, conn);
                break;
            default:
                break;    
            }
        } catch (Exception e) {
            Log.error(e);
        } finally {
            dbm = null;
            
            if (conn != null && !conn.isClosed())
                conn.close();
                conn = null;
        }    
                
        return itemName;
    }
    

    
    //Get Task List
    public List<Object> getTaskList(UserPrincipalBean principal, int releaseID) throws SQLException {
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		List<Object> taskList = new ArrayList<>();
		
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = "select task_code from release_task where release_id = " + dbm.param(releaseID, DataType.INT); 			
			rs = dbm.select(sql, conn); 
				
		    while (rs.next()) {
		    	taskList.add(Integer.parseInt(rs.getObject("task_code").toString()));
		    }
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return taskList;
	}
    
    public boolean addTaskFromTask(UserPrincipalBean principal, Connection conn, String module, String itemCode, int version, JSONObject modifyParam) throws SQLException{
		boolean isNew = false;
		DBManager dbm = new DBManager();
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
			}
			
			String compCode = principal.getCompCode();
			DynTemplate template = new DynTemplate();
			
			String sql = "select * from task_mst " +
					"where comp_code = " + dbm.param(compCode, DataType.TEXT) + 
					" and item_code = " + dbm.param(itemCode, DataType.TEXT) + 
					" and ver_no = " + dbm.param(version, DataType.INT) + 
					" and func_code = " + dbm.param(template.getFuncCodeByModule(conn, module), DataType.TEXT);
	
			JSONObject taskRecord = dbm.selectRecord(sql, conn);
			dbm.clear();
			insertTaskIntoRecord(principal, conn, dbm, taskRecord, modifyParam);
			if(isNew){
				conn.commit();
			}
			return true;
		}catch(Exception e){
			conn.rollback();
			Log.error("newVersions error: " + e.getMessage());	
		}finally{
			dbm = null;
			if(isNew){
				conn.close();
				conn = null;	
			}
		}
		return false;
	}
    
    public boolean addTaskFromTask(UserPrincipalBean principal, Connection conn, String taskId, JSONObject modifyParam) throws SQLException{
		boolean isNew = false;
		DBManager dbm = new DBManager();
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
			}
			
			String sql = "select * from task_mst " +
					"where taskId = " + dbm.param(Integer.parseInt(taskId), DataType.INT);
			
			JSONObject taskRecord = dbm.selectRecord(sql, conn);
			dbm.clear();
			insertTaskIntoRecord(principal, conn, dbm, taskRecord, modifyParam);
			if(isNew){
				conn.commit();
			}
			return true;
		}catch(Exception e){
			conn.rollback();
			Log.error("newVersions error: " + e.getMessage());	
		}finally{
			dbm = null;
			if(isNew){
				conn.close();
				conn = null;	
			}
		}
		return false;
	}
    
    private boolean insertTaskIntoRecord(UserPrincipalBean principal, Connection conn, DBManager dbm, JSONObject taskRecord, JSONObject modifyParam) throws SQLException{
    	String userCode = principal.getUserCode();
    	taskRecord.remove("TASK_CODE");
		taskRecord.put("UPD_BY", userCode);
		taskRecord.put("UPD_DATE", "sysdate");
		taskRecord.put("CREATE_BY", userCode);
		taskRecord.put("CREATE_DATE", "sysdate");
		taskRecord.put("STATUS", "U");
		
		for(String key: modifyParam.keySet()){
			taskRecord.put(key, modifyParam.get(key));
		}
		
		return dbm.insertRecord("task_mst", taskRecord, conn);
    }
	
  
}
