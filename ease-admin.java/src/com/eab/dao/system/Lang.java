package com.eab.dao.system;

public class Lang {
	private String code;				// Language Code
	private String name;				// Language Name
	private boolean isDefault = false;	// Is Default Language?
	private boolean isSelected = false;	// Is Current Selected Language?
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isDefault() {
		return isDefault;
	}
	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	
}
