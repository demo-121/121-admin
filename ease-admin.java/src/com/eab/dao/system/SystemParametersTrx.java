package com.eab.dao.system;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.eab.database.jdbc.DBManager;

public class SystemParametersTrx {
	public ResultSet get(Connection conn) throws SQLException {
		ResultSet output = null;
		DBManager dbm = null;
		
		dbm = new DBManager();
		String sql = "select param_code, param_value from sys_param "
					+ " where status='A'"
					+ " order by param_code";
		
		return dbm.select(sql, conn);
	}
}
