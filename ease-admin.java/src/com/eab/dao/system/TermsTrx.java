package com.eab.dao.system;

import java.sql.Connection;
import java.sql.SQLException;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class TermsTrx {
	
	public String getTermsAccept(String userCode) {
		String output = "0";
		DBManager dbm = null;
		
		try {
			dbm = new DBManager();
			String sql = "select count(1) as cnt from SYS_TERMS_ACCEPTED where user_code=" + dbm.param(userCode, DataType.TEXT);
			output = dbm.selectSingle(sql, "cnt");
		} catch(SQLException e) {
			Log.error(e);
		} catch(Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
		}
		
		return output;
	}
	
	public boolean setTermsAccept(String userCode, String ipAddr) throws SQLException {
		boolean output = false;
		DBManager dbm = null;
		Connection conn = null;
		String sql = "";
		
		try {
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			dbm = new DBManager();
			
			dbm.clear();
			sql = "delete from SYS_TERMS_ACCEPTED where user_code=" + dbm.param(userCode, DataType.TEXT);
			dbm.delete(sql, conn);
			
			dbm.clear();
			sql = "insert into SYS_TERMS_ACCEPTED(user_code, user_ip_addr) "
					+ " values (" + dbm.param(userCode, DataType.TEXT) + ", " + dbm.param(ipAddr, DataType.TEXT) + ")";
			output = dbm.insert(sql, conn);
			
			if (output) {
				conn.commit();
			} else {
				conn.rollback();
			}
		} catch(SQLException e) {
			conn.rollback();
			Log.error(e);
		} catch(Exception e) {
			conn.rollback();
			Log.error(e);
		} finally {
			dbm = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
		}
		
		return output;
	}
}
