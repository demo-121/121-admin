package com.eab.dao.products;

import java.sql.Connection;
import java.sql.ResultSet;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class ProductsDetailTask {

	DBManager dbm;
	Connection conn;
	
	public String getProducKey(int taskID)throws Exception {
		ResultSet rs = null;
		String productKey = "";
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			String sql = " select comp_code, item_code, ver_no"
					   + " from task_mst"
					   + " where task_code = " + dbm.param(taskID, DataType.INT); 
						rs = dbm.select(sql, conn);
			
			while (rs.next()) {
				productKey = rs.getObject("comp_code") + "--" + rs.getObject("item_code") + "--" + rs.getObject("ver_no");	    
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}	
		
		return productKey;
	}
	
}

