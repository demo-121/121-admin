package com.eab.dao.users;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.system.Name;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.SearchCondition;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.webservice.dynamic.DynamicFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class UsersDAO {
	private DBManager dbm = null;
	private Connection conn = null;
	private boolean newConn = false;
	String[] ids = null; 
	String[] keys = null;
	
	String[] accessIds = null;
	String[] accessKeys = null;

	public UsersDAO(){
		super();
		dbm = new DBManager();
		try {
			conn = DBAccess.getConnection();
			newConn = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public UsersDAO(Connection conn){
		super();
		dbm = new DBManager();
		this.conn = conn;
	}

	public UsersDAO(Connection conn, String[] ids, String[] keys, String[] aids, String[] akeys) {
		super();
		dbm = new DBManager();
		this.conn = conn;
		this.ids = ids;
		this.keys = keys;
		this.accessIds = aids;
		this.accessKeys = akeys;
	}

	public DBManager getManager() {
		return dbm;
	}

	public JsonArray getUserMaster(SearchCondition cond, String statusStr, String sortBy, String sortDir, JsonArray fields, DynamicUI ui, String compCode) throws Exception {
		JsonArray result;
		ResultSet rs = null;
		String condition = "";

		if (cond.getCriteria() != null && !cond.getCriteria().isEmpty()) {
			condition = DynamicFunction.buildCondition(condition, "(UPPER(u.user_code) LIKE " + dbm.param("%"+cond.getCriteria().toUpperCase()+"%", DataType.TEXT) + " OR UPPER(u.user_name) LIKE " + dbm.param("%"+cond.getCriteria().toUpperCase()+"%", DataType.TEXT) + ")"); 
		}

		if (statusStr.equals("All")) {
		} else if (statusStr.equals("E")) {
			condition = DynamicFunction.buildCondition(condition, "(u.action = 'N' OR u.status <> 'D')");
		} else if (statusStr.equals("P")) {
			condition = DynamicFunction.buildCondition(condition, "u.action = 'N'");
		} else { // A and D
			condition = DynamicFunction.buildCondition(condition, "(u.action <> 'N' OR u.action is null) AND u.status = " + dbm.param(statusStr, DataType.TEXT));
		}		
		
		String typeStr = cond.getTypeFilter();
		
		if (typeStr == null || typeStr.isEmpty() || typeStr.equals("All")) {
		} else {
			condition = DynamicFunction.buildCondition(condition, ui.getFilterKey() + " = "+dbm.param(typeStr, DataType.TEXT) );
		}
		
//		if (!isAdmin) {
//			condition = DynamicFunction.addCompanyFilter(dbm, condition, compCode);
//		}
		//add company filter 
		condition = DynamicFunction.addCompanyFilter(dbm, condition, ui, compCode, "uur");
		
		String sqlData = "select * FROM V_user_MST u join v_user_user_role uur on u.user_code = uur.user_code and uur.status = 'A'"
				+ " join v_user_role_mst ur on uur.role_code = ur.role_code and uur.comp_code = ur.comp_code";
		sqlData += condition + " ORDER BY u." + sortBy +  " "+ ("A".equals(sortDir)?"ASC":"DESC");
		result = new JsonArray();
		rs = dbm.select(sqlData, conn);
		dbm.clear();

		if (rs != null) {
			while (rs.next()) {

				JsonObject rec = new JsonObject();

				rec.addProperty("id", rs.getString("user_code"));
				
				for (int i = 0; i < fields.size(); i++) {
					JsonObject item = fields.get(i).getAsJsonObject();
					if (item.has("key") && !item.get("key").isJsonNull()) {
						String key = item.get("key").getAsString();
						if ( rs.getString(key) != null) {
							String id = item.get("id").getAsString();
							rec.addProperty(id, rs.getString(key));
						}
					}
				}
				result.add(rec);
			}
			rs.close();
		}

		if (newConn) {
			this.conn.close();
			this.conn = null;
		}
		return result;
	}

	public List<JsonObject> getUserById(JsonArray fields, String uid, String primaryKey) throws Exception {
		ArrayList<JsonObject> result = null;
		ResultSet rs = null;

		String sqlData = "SELECT a.*, '' AS action FROM sys_user_mst a WHERE user_code = "+ dbm.param(uid, DataType.TEXT)+" UNION SELECT * FROM sys_user_mst_tmp WHERE user_code = "+dbm.param(uid, DataType.TEXT);

		result = new ArrayList<JsonObject>();
		rs = dbm.select(sqlData, conn);
		dbm.clear();
		
		if (rs != null) {
			while (rs.next()) {
				JsonObject rec = new JsonObject();
				String action = rs.getString("action");
				String userCode = rs.getString(primaryKey);
			 	rec.addProperty("id", userCode);
			 	rec.addProperty("action", action);
				
				for (int i = 0; i < keys.length - 6; i++) {
					String key = keys[i];
					String id = ids[i];
					if ( rs.getString(key) != null) {
						rec.addProperty(id, rs.getString(key));
					}
				}
				
				if (rs.getDate("create_date") != null) {
					rec.addProperty("createDate", rs.getDate("create_date").getTime());
				}
				if (rs.getDate("modify_date") != null) {
					rec.addProperty("modifyDate", rs.getDate("modify_date").getTime());
				}
				if (rs.getDate("approv_date") != null) {
					rec.addProperty("approvDate", rs.getDate("approv_date").getTime());
				}
				
				// get related user name
				Map<String, String> userMap = new HashMap<String, String>();
				Map<String, String> nameMap = new HashMap<String, String>();

				String createBy = rs.getString("create_by");
				if (createBy != null && !createBy.isEmpty()) {
					userMap.put("createBy", createBy);
					if (!nameMap.containsKey(createBy)) {
						nameMap.put(createBy, createBy);
					}
				}
				String modifyBy = rs.getString("modify_by");
				if (modifyBy != null && !modifyBy.isEmpty()) {
					userMap.put("modifyBy", modifyBy);
					rec.addProperty("updateBy", modifyBy); // get for checking maker only
					if (!nameMap.containsKey(modifyBy)) {
						nameMap.put(modifyBy, modifyBy);
					}
				}
				String approveBy = rs.getString("approv_by");
				if (approveBy != null && !approveBy.isEmpty()) {
					userMap.put("approvBy", approveBy);
					if (!nameMap.containsKey(approveBy)) {
						nameMap.put(approveBy, approveBy);
					}
				}
				if (userMap.size() > 0) {
					String names = "";
					for (String name : nameMap.keySet()) {
						names += (names.isEmpty()?"":", ")+dbm.param(name, DataType.TEXT);
					}
					sqlData = "SELECT user_code, user_name FROM sys_user_mst WHERE user_code in ("+ names +")";
					ResultSet rs2 = dbm.select(sqlData, conn);
					dbm.clear();
					while (rs2.next()) {
						nameMap.put(rs2.getString(1), rs2.getString(2));
					}
					for (Map.Entry<String,String> item : userMap.entrySet()) {
						rec.addProperty(item.getKey(), nameMap.get(item.getValue()));
					}
					rs2.close();
				}

				// get access rights
				String defComp = null;
				if (rec.has("compCode")) {
					defComp = rec.get("compCode").getAsString();
				}
				String defChannel = null;
				if (rec.has("channelCode")) {
					defChannel = rec.get("channelCode").getAsString();
				}

				JsonArray accesses = new JsonArray();
				if (action == null || action.isEmpty()) {
					// mst
					sqlData = "SELECT * FROM sys_user_user_role WHERE user_code = " + dbm.param(userCode, DataType.TEXT);
				} else {
					// tmp
					sqlData = "SELECT * FROM sys_user_user_role_tmp WHERE user_code = " + dbm.param(userCode, DataType.TEXT);
				}
				ResultSet rs1 = dbm.select(sqlData, conn);
				dbm.clear();
				while (rs1.next()) {
					
					JsonObject access = new JsonObject();
					for (int i = 0; i < accessKeys.length - 6; i++) {
						String key = accessKeys[i];
						if ( rs1.getString(key) != null) {
							String id = accessIds[i];
							access.addProperty(id, rs1.getString(key));
						}
					}
					String company = access.get("company").getAsString();
					String channel = access.get("channel").getAsString();
					
					if (company.equals(defComp) && channel.equals(defChannel)) {
						access.addProperty("default", "Y");
					} else {
						access.addProperty("default", "N");
					}
					accesses.add(access);
				}
				rs1.close();
				
				rec.add("access", accesses);
				result.add(rec);
			}
			rs.close();
		}

		if (newConn) {
			this.conn.close();
			this.conn = null;
		}
		return result;
	}

	public boolean deleteUser(String uid, String userCode) throws Exception {

		String cols = "";
		for (int i = 0; i<keys.length - 4; i++) {
			cols += keys[i] + ", ";
		}
		Date now = new Date(new java.util.Date().getTime());
		
		//insert a row from mst to tmp
		String insertCols = "(" + cols + "modify_date, modify_by, action)";
		String selectSql = "select " + cols + dbm.param(now, DataType.DATE)+" AS modify_date, "+dbm.param(userCode, DataType.TEXT)+" AS modify_by, "+dbm.param("D", DataType.TEXT)+" AS action from sys_user_mst where user_code = " + dbm.param(uid, DataType.TEXT);						
		String tmpInsert = "insert into sys_user_mst_tmp " + insertCols + " "+ selectSql;

		boolean result = dbm.insert(tmpInsert);
		dbm.clear();
		
		if (result) {
			cols = "";
			for (int i = 0; i<accessKeys.length - 4; i++) {
				cols += accessKeys[i] + ", ";
			}
			insertCols = "("+cols+"modify_date, modify_by, action)";
			selectSql = "select " + cols + dbm.param(now, DataType.DATE)+" AS modify_date, "+dbm.param(userCode, DataType.TEXT)+" AS modify_by, "+dbm.param("D", DataType.TEXT)+" AS action from sys_user_user_role where user_code = " + dbm.param(uid, DataType.TEXT);						
			tmpInsert = "insert into sys_user_user_role_tmp " + insertCols + " "+ selectSql;
				
			dbm.insert(tmpInsert);
			dbm.clear();
		}
		
		return result;
	}

	public boolean saveInsert(String uid, JsonObject userJson, String updateBy, Date createDate, String createBy, String action) throws Exception {

		String cols = "";
		String values = "";
		Date now = new Date(new java.util.Date().getTime());

		int keyLen = keys.length;
		for (int i = 0; i<keyLen - 2; i++) {
			String key = keys[i];
			String id = ids[i];
			
			if (i < keyLen - 6) {
				if (userJson.has(id) && !userJson.get(id).isJsonNull()) {
					cols += key + ", ";
					values += dbm.param(userJson.get(id).getAsString(), DataType.TEXT) + ", ";
				}
			} else {
				cols += key + ", ";
			}
		}
		
		values += dbm.param(createDate != null? createDate: now, DataType.DATE) + ", "; // create date
		values += dbm.param(createBy != null? createBy: updateBy, DataType.TEXT) + ", ";	// create by
		values += dbm.param(now, DataType.DATE) + ", ";	// modify date
		values += dbm.param(updateBy, DataType.TEXT) + ", "; // modify be
		
		//insert a row from mst to tmp
		String insertCols = "(" + cols + "action)";
		String valuesField = "("+values + dbm.param(action, DataType.TEXT)+")";
		
		String userFuncSql = "";
		if (userJson.has("access") && !userJson.get("access").isJsonNull()) {
			JsonArray accesses = userJson.get("access").getAsJsonArray();

			String aCols = "";
			for (int i = 0; i<accessKeys.length - 2; i++) {
				aCols += accessKeys[i] + ", ";
			}

			String userFuncInsert = " INTO sys_user_user_role_tmp ("+aCols+"action) VALUES ";

			for (JsonElement entry : accesses) {
				JsonObject access = entry.getAsJsonObject(); 
//			accesses.forEach(action);
				String comp = access.get("company").getAsString();
				String channel = access.get("channel").getAsString();
				String role = access.get("role").getAsString();
				userFuncSql += userFuncInsert + "("+dbm.param(uid, DataType.TEXT)
						+","+ dbm.param(comp, DataType.TEXT)+","+ dbm.param(channel, DataType.TEXT)
						+","+ dbm.param(role, DataType.TEXT)+","+dbm.param("A", DataType.TEXT)
						+","+ dbm.param(now, DataType.DATE)+","+dbm.param(updateBy, DataType.TEXT) 
						+","+ dbm.param(now, DataType.DATE)+","+dbm.param(updateBy, DataType.TEXT) 
						+","+ dbm.param(action, DataType.TEXT)+ ")";
			}
		}
		
		String tmpInsert = "INSERT ALL INTO sys_user_mst_tmp " + insertCols + " VALUES "+ valuesField
							+ userFuncSql + " SELECT 1 FROM DUAL";
			
		boolean result = dbm.insert(tmpInsert);
		
		dbm.clear();
		return result;
	}

	public boolean saveUpdate(String uid, JsonObject userJson, String userCode, String action) throws Exception {

		String fields = "";
		Date now = new Date(new java.util.Date().getTime());

		for (int i = 1; i<keys.length - 6; i++) {
			String key = keys[i];
			String id = ids[i];
			
			if (userJson.has(id) && !userJson.get(id).isJsonNull()) {
				fields += (i==1?"":", ") + key +" = " + dbm.param(userJson.get(id).getAsString(), DataType.TEXT);
			}
		}
		
		fields += ", modify_date = " + dbm.param(now, DataType.DATE);
		fields += ", modify_by = " + dbm.param(userCode, DataType.TEXT);
		
		//insert a row from mst to tmp
		String tmpInsert = "UPDATE sys_user_mst_tmp SET " + fields + " WHERE user_code = " + dbm.param(uid, DataType.TEXT);
			
		int result = dbm.update(tmpInsert);
		
		dbm.clear();
		if (result == 1) {
			String removeCor = "DELETE FROM sys_user_user_role_tmp WHERE user_code = " + dbm.param(uid, DataType.TEXT);
			dbm.delete(removeCor);
			
			dbm.clear();
			
			if (userJson.has("access") && !userJson.get("access").isJsonNull()) {
				String userFuncSql = "";
				JsonArray accesses = userJson.get("access").getAsJsonArray();
				
				String aCols = "";
				for (int i = 0; i<accessKeys.length - 2; i++) {
					aCols += accessKeys[i] + ", ";
				}

				String userFuncInsert = " INTO sys_user_user_role_tmp ("+aCols+"action) VALUES ";
				
				for (JsonElement entry : accesses) {
					JsonObject access =entry.getAsJsonObject(); 
//					accesses.forEach(action);
						String comp = access.get("company").getAsString();
						String channel = access.get("channel").getAsString();
						String role = access.get("role").getAsString();
					userFuncSql += userFuncInsert + "("+dbm.param(uid, DataType.TEXT)
							+","+ dbm.param(comp, DataType.TEXT)+","+ dbm.param(channel, DataType.TEXT)
							+","+ dbm.param(role, DataType.TEXT)+","+dbm.param("A", DataType.TEXT)
							+","+ dbm.param(now, DataType.DATE)+","+dbm.param(userCode, DataType.TEXT) 
							+","+ dbm.param(now, DataType.DATE)+","+dbm.param(userCode, DataType.TEXT) 
							+","+ dbm.param(action, DataType.TEXT)+ ")";
				}
				tmpInsert = "INSERT ALL "+ userFuncSql + " SELECT 1 FROM DUAL";
				dbm.insert(tmpInsert);
				dbm.clear();
			}
		}
		
		return result > 0;
	}
	
	public boolean approveAdd(String uid, String userCode) throws Exception {

		String cols = "";
		for (int i = 0; i<keys.length - 2; i++) {
			cols += keys[i] + ", ";
		}
		String insertCols = "(" +cols+ "approv_date, approv_by)";
		Date now = new Date(new java.util.Date().getTime());
		// insert to sys_user
		String approveSQL = "INSERT INTO sys_user_mst "+insertCols
				+" SELECT "+cols + dbm.param(now, DataType.DATE) +" approv_date, "+dbm.param(userCode, DataType.TEXT)+" approv_by FROM sys_user_mst_tmp WHERE user_code = " +  dbm.param(uid, DataType.TEXT);
		
		boolean result = dbm.insert(approveSQL);
		dbm.clear();
		
		if (result) {
		// insert new func code for the user

			String aCols = "";
			for (int i = 0; i<accessKeys.length - 2; i++) {
				aCols += accessKeys[i] + ", ";
			}

			approveSQL = "INSERT INTO sys_user_user_role ("+aCols+"approv_date, approv_by) "
					+" SELECT "+aCols+dbm.param(now, DataType.DATE)+" AS approv_date, "+dbm.param(userCode, DataType.TEXT)+" AS approv_by FROM sys_user_user_role_tmp WHERE user_code = " +  dbm.param(uid, DataType.TEXT);
			
			dbm.insert(approveSQL);
		}
		
		dbm.clear();
		return result;
	}
	
	public boolean approveEdit(String uid, String userCode) throws Exception {

		String cols = "";
		for (int i = 1; i<keys.length - 2; i++) {
			cols += keys[i] + ", ";
		}
		String insertCols = "(" +cols+ "approv_date, approv_by)";
		Date now = new Date(new java.util.Date().getTime());
		String approveSQL = "UPDATE sys_user_mst SET "+insertCols
				+" = (SELECT "+cols + dbm.param(now, DataType.DATE) +" approv_date, "+dbm.param(userCode, DataType.TEXT)+" approv_by FROM sys_user_mst_tmp WHERE user_code = " +  dbm.param(uid, DataType.TEXT) +")"
				+" WHERE user_code = " +  dbm.param(uid, DataType.TEXT);
		
		int result = dbm.update(approveSQL);
		dbm.clear();
		
		if (result > 0) {
		
			// delete existing func code for the user
			String removeCor = "DELETE FROM sys_user_user_role WHERE user_code = " + dbm.param(uid, DataType.TEXT);
			dbm.delete(removeCor);
			dbm.clear();


			String aCols = "";
			for (int i = 0; i<accessKeys.length - 2; i++) {
				aCols += accessKeys[i] + ", ";
			}
			// insert new func code for the user
			approveSQL = "INSERT INTO sys_user_user_role ("+aCols+"approv_date, approv_by) "
					+" SELECT "+aCols+dbm.param(now, DataType.DATE)+" AS approv_date, "+dbm.param(userCode, DataType.TEXT)+" AS approv_by FROM sys_user_user_role_tmp WHERE user_code = " +  dbm.param(uid, DataType.TEXT);

			dbm.insert(approveSQL);
			dbm.clear();
		}		
		return result > 0;
	}
	

	public boolean approveDelete(String uid, String userCode) throws Exception {

		String deleteSQL = "DELETE FROM sys_user_mst WHERE user_code = " +  dbm.param(uid, DataType.TEXT);
		int result = dbm.delete(deleteSQL);
		dbm.clear();		
		
		// delete existing func code for the user
		String removeCor = "DELETE FROM sys_user_user_role WHERE user_code = " + dbm.param(uid, DataType.TEXT);
		dbm.delete(removeCor);
		dbm.clear();

		return result > 0;
	}
	
	public boolean removeTmp(String uid) throws Exception {

		String deleteSQL = "DELETE FROM sys_user_mst_tmp WHERE user_code = " +  dbm.param(uid, DataType.TEXT);
		int result = dbm.delete(deleteSQL);
		dbm.clear();
		
		// delete existing func code for the user
		String removeCor = "DELETE FROM sys_user_user_role_tmp WHERE user_code = " + dbm.param(uid, DataType.TEXT);
		dbm.delete(removeCor);
		dbm.clear();

		return result > 0;
	}

	public String getUserAction(String uid) throws Exception {
		String action = null;
		ResultSet rs = null;

		String sqlData = "SELECT action FROM sys_user_mst_tmp WHERE user_code = "+ dbm.param(uid, DataType.TEXT);

		rs = dbm.select(sqlData, conn);
		dbm.clear();
		if (rs != null) {
			while (rs.next()) {
				action = rs.getString("action");
			}
			rs.close();
		}

		if (newConn) {
			this.conn.close();
			this.conn = null;
		}
		return action;
	}


	public boolean insertAudit(String uid) throws Exception {
		
		String cols = "";
		for (int i = 0; i<keys.length; i++) {
			cols += keys[i] + ", ";
		}
		
		String audInsert = "INSERT INTO aud_sys_user_mst ("+cols+"action) SELECT "+cols+"action FROM sys_user_mst_tmp WHERE user_code = " +  dbm.param(uid, DataType.TEXT);
		boolean result = dbm.insert(audInsert);
		dbm.clear();
		
		cols = "";
		for (int i = 0; i<accessKeys.length; i++) {
			cols += accessKeys[i] + ", ";
		}
		audInsert = "INSERT INTO aud_sys_user_user_role ("+cols+"action) SELECT "+cols+"action FROM sys_user_user_role_tmp WHERE user_code = " +  dbm.param(uid, DataType.TEXT);
		dbm.insert(audInsert);

		dbm.clear();
		return result;
	}
	
	public boolean insertAudit(String uid, String userCode, String action) throws Exception {
		Date now = new Date(new java.util.Date().getTime());
		
		String cols = "";
		for (int i = 0; i<keys.length - 2; i++) {
			cols += keys[i] + ", ";
		}
		
		String audInsert = "insert into aud_sys_user_mst (" +cols+"approv_date, approv_by, action)"
				+" select "+cols 
				+ dbm.param(now, DataType.DATE) +" AS approv_date,"
				+ dbm.param(userCode, DataType.TEXT) +" AS approv_by,"
				+ dbm.param(action, DataType.TEXT) +" AS action from sys_user_mst_tmp WHERE user_code = " +  dbm.param(uid, DataType.TEXT);
		
		boolean result = dbm.insert(audInsert);
		dbm.clear();
		
		String roleFuncCols = "";
		for (int i = 0; i<accessKeys.length - 2; i++) {
			roleFuncCols += accessKeys[i] + ", ";
		}
		
		String roleFuncInsertStr = "INSERT INTO aud_sys_user_user_role ("+roleFuncCols+"approv_date, approv_by, action) "
				+ "SELECT " + roleFuncCols 
				+ dbm.param(now, DataType.DATE)+" AS approv_date, "
				+ dbm.param(userCode, DataType.TEXT)+" AS approv_by, "
				+ dbm.param(action, DataType.TEXT)+" AS action FROM sys_user_user_role_tmp WHERE user_code = " + dbm.param(uid, DataType.TEXT);

		dbm.insert(roleFuncInsertStr);
		dbm.clear();
		return result;
	}

	public boolean updateSamlUser(String uid, String name, String mail) throws Exception {
		
		String sql = "UPDATE SYS_USER_MST SET"
				+ " USER_NAME = " + dbm.param(name, DataType.TEXT)
				+ ", EMAIL = " + dbm.param(mail, DataType.TEXT)
				+ " WHERE USER_CODE = " + dbm.param(uid, DataType.TEXT);
		
		int result = dbm.update(sql, conn);
		dbm.clear();
		
		return result > 0;
	}

	public boolean insertSamlUser(String compCode, String channel, String uid, String name, String mail) throws Exception {
		String sql = "INSERT INTO  SYS_USER_MST (USER_TYPE, LANGUAGE, STATUS, CREATE_BY, MODIFY_BY, APPROV_BY,"
				+ "COMP_CODE, CHANNEL_CODE, USER_CODE, USER_NAME, EMAIL) VALUES ('U','en','A','ADMIN','ADMIN','ADMIN'"
				+ ", " + dbm.param(compCode, DataType.TEXT)
				+ ", " + dbm.param(channel, DataType.TEXT)
				+ ", " + dbm.param(uid, DataType.TEXT)
				+ ", " + dbm.param(name, DataType.TEXT)
				+ ", " + dbm.param(mail, DataType.TEXT)
				+ ")"; 
		
		boolean result = dbm.insert(sql, conn);
		dbm.clear();
		
		return result;
		
	}

	public boolean existsUserRole(String compCode, String roleCode) throws Exception {
		
		String sql = "select * from SYS_USER_ROLE_MST WHERE role_code = " + dbm.param(roleCode, DataType.TEXT) +
				" AND comp_code = " + dbm.param(compCode, DataType.TEXT);
		ResultSet list = dbm.select(sql, conn);
		dbm.clear();
		
		if (list != null) {
			return list.next();
		}
		return false;
	}

	public boolean createUserRole(String compCode, String roleCode, String roleName) throws Exception {
		String sql = "INSERT INTO  SYS_USER_ROLE_MST (STATUS, CREATE_BY, MODIFY_BY, APPROV_BY,"
				+ "COMP_CODE, ROLE_CODE, ROLE_NAME) VALUES ('A','ADMIN','ADMIN','ADMIN'"
				+ ", " + dbm.param(compCode, DataType.TEXT)
				+ ", " + dbm.param(roleCode, DataType.TEXT)
				+ ", " + dbm.param(roleName == null?roleCode:roleName, DataType.TEXT)
				+ ")"; 
		
		boolean result = dbm.insert(sql, conn);
		dbm.clear();
		return result;
	}

	public boolean addRoleFuncs(String compCode, String roleCode, List<String> rights) throws Exception {
		
		String sql = "INSERT ALL";
		
		for (String right : rights) {
			sql += " INTO SYS_USER_ROLE_FUNC (STATUS, CREATE_BY, MODIFY_BY, APPROV_BY,"
			+ "COMP_CODE, ROLE_CODE, FUNC_CODE) VALUES ('A','ADMIN','ADMIN','ADMIN'"
			+ "," + dbm.param(compCode, DataType.TEXT)
			+ "," + dbm.param(roleCode, DataType.TEXT)
			+ "," + dbm.param(right, DataType.TEXT)
			+ ")"; 
		}
		
		sql += " SELECT 1 FROM DUAL";
		
		boolean result = dbm.insert(sql, conn);
		dbm.clear();
		return result;
	}

	public boolean existsUserUserRole(String compCode, String channel, String uid, String roleCode) throws Exception {

		String sql = "select * from SYS_USER_USER_ROLE WHERE role_code = " + dbm.param(roleCode, DataType.TEXT)
			+ " AND COMP_CODE = " + dbm.param(compCode, DataType.TEXT)
			+ " AND CHANNEL_CODE = " + dbm.param(channel, DataType.TEXT)
			+ " AND USER_CODE = " + dbm.param(uid, DataType.TEXT);
		
		ResultSet list = dbm.select(sql, conn);
		dbm.clear();
		
		if (list != null) {
			return list.next();
		}
		return false;
	}

	public boolean createUserUserRole(String compCode, String channel, String uid, String roleCode) throws Exception {
		String sql = "INSERT INTO SYS_USER_USER_ROLE (STATUS, CREATE_BY, MODIFY_BY, APPROV_BY,"
				+ "COMP_CODE, CHANNEL_CODE, ROLE_CODE, USER_CODE) VALUES ('A','ADMIN','ADMIN','ADMIN'"
				+ ", " + dbm.param(compCode, DataType.TEXT)
				+ ", " + dbm.param(channel, DataType.TEXT)
				+ ", " + dbm.param(roleCode, DataType.TEXT)
				+ ", " + dbm.param(uid, DataType.TEXT)
				+ ")"; 
		
		boolean result = dbm.insert(sql, conn);
		dbm.clear();
		return result;
	}

	public boolean deleteUserUserRole(String compCode, String uid) throws Exception {
		String sql = "DELETE FROM SYS_USER_USER_ROLE WHERE "
				+ "COMP_CODE = " + dbm.param(compCode, DataType.TEXT)
				+ " AND USER_CODE = " + dbm.param(uid, DataType.TEXT);
		
		int result = dbm.delete(sql, conn);
		dbm.clear();
		return result > 0;
		
	}

	public boolean existRoleFunc(String compCode, String roleCode, List<String> list) throws Exception {
		
		if (list.size() > 0) {
			String sql = "select count(1) as crt from SYS_USER_ROLE_FUNC WHERE role_code = " + dbm.param(roleCode, DataType.TEXT)
			+ " AND COMP_CODE = " + dbm.param(compCode, DataType.TEXT)
			+ " AND ROLE_CODE = " + dbm.param(roleCode, DataType.TEXT)
			+ " AND ( ";
			
			String codeCond = "";
			for (String code : list) {
				codeCond += (StringUtils.isEmpty(codeCond)?"":" OR ") + "FUNC_CODE = " + dbm.param(code, DataType.TEXT);
			}
			sql+=codeCond+")";
			
			ResultSet rs = dbm.select(sql, conn);
			dbm.clear();
			if (rs.next()) {
				return rs.getInt("crt") == list.size();
			}
			return false;
		} else {
			return false;
		}
	}

	public boolean deleteRoleFunc(String compCode, String roleCode) throws Exception {
		String sql = "DELETE FROM SYS_USER_ROLE_FUNC WHERE "
				+ "COMP_CODE = " + dbm.param(compCode, DataType.TEXT)
				+ " AND ROLE_CODE = " + dbm.param(roleCode, DataType.TEXT);
		
		int result = dbm.delete(sql, conn);
		dbm.clear();
		return result > 0;
		
	}

	public boolean existUser(String uid) throws Exception {
		String sql = "Select count(1) as crt from SYS_USER_MST WHERE USER_CODE = " + dbm.param(uid, DataType.TEXT);
		
		ResultSet rs = dbm.select(sql, conn);
		dbm.clear();
		if (rs.next()) {
			return rs.getInt("crt") > 0;
		}
		return false;		
	}

	public int countAllRoleFunc(String compCode, String roleCode) throws Exception {
		
		String sql = "select count(1) as crt from SYS_USER_ROLE_FUNC WHERE role_code = " + dbm.param(roleCode, DataType.TEXT)
		+ " AND COMP_CODE = " + dbm.param(compCode, DataType.TEXT);

		ResultSet rs = dbm.select(sql, conn);
		dbm.clear();
		if (rs.next()) {
			return rs.getInt("crt");
		}
		return 0;
	}
}
