package com.eab.dao.about;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.model.about.AboutBean;
import com.eab.model.profile.UserPrincipalBean;

public class About {
	DBManager dbm;
	Connection conn;

	public AboutBean getAbout(HttpServletRequest request) throws SQLException {
		dbm = new DBManager();
		conn = DBAccess.getConnection();
		Language langObj = new Language(((UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL)).getUITranslation());
		ResultSet rs = null;
		AboutBean about = new AboutBean();
		HashMap<String, String> nameMap = new HashMap<String, String>();

		try {
			// From sys_param
			String rows = "'SYS_VER', 'SYS_NAME'";
			String sql = "select param_code, param_value from sys_param where param_code in (" + rows + ")";
			rs = dbm.select(sql, conn);
			if (rs != null) {
				while (rs.next()) {
					nameMap.put(rs.getString("param_code"), rs.getString("param_value"));
				}
			}

			// From sys_name_mst
			String[] items = { "LOGO_PATH", "ABOUT_TITLE", "ABOUT_DESC", "ABOUT_ADDRESS", "ABOUT_TEL", "ABOUT_FAX", "ABOUT_WEBSITE", "ABOUT_EMAIL", "ABOUT_COPYRIGHT_DESC", "ABOUT_COPYRIGHT" };
			for (int i = 0; i < items.length; i++) {
				nameMap.put(items[i], langObj.getNameDesc(items[i]));
			}

			about.setSysVer(nameMap.get("SYS_VER"));
			about.setSysName(nameMap.get("SYS_NAME"));
			about.setLogo(nameMap.get("LOGO_PATH"));
			about.setTitle(nameMap.get("ABOUT_TITLE"));
			about.setDesc(nameMap.get("ABOUT_DESC"));
			about.setTel(nameMap.get("ABOUT_TEL"));
			about.setAddress(nameMap.get("ABOUT_ADDRESS"));
			about.setFax(nameMap.get("ABOUT_FAX"));
			about.setWebsite(nameMap.get("ABOUT_WEBSITE"));
			about.setMail(nameMap.get("ABOUT_EMAIL"));
			about.setRightDesc(nameMap.get("ABOUT_COPYRIGHT_DESC"));
			about.setCopyRight(nameMap.get("ABOUT_COPYRIGHT"));
		} catch (Exception e) {
			return null;
		} finally {
			if (conn != null) {
				conn.close();
			}
			conn = null;
			dbm = null;
		}
		return about;
	}
}
