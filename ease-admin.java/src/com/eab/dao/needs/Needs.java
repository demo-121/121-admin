	package com.eab.dao.needs;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.common.DynMenu;
import com.eab.dao.common.Functions;
import com.eab.dao.tasks.Tasks;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Option;
import com.eab.model.needs.NeedsBean;
import com.eab.model.needs.NeedAttachBean;
import com.eab.model.products.ProductBean;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Needs {
	
	public Map<String, Object> getNeedStatusById(String needCode, String channelCode, int version, UserPrincipalBean principal) throws SQLException{
		Map<String, Object> result = null;
		DBManager dbm;
		Connection conn = null;
		
		try {
			String compCode = principal.getCompCode();
			
			if (needCode != null && !needCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
				String sql = " select"
						   + " status_code,"
						   + " version"
						   + " from v_need_list"
						   + " where need_code = " + dbm.param(needCode, DataType.TEXT) 
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT) 
						   + " and channel_code = " + dbm.param(channelCode, DataType.TEXT);
				
				ResultSet rs = dbm.select(sql, conn);
				dbm.clear();
				
				if (rs.next()) {
					result = new HashMap<String, Object>();
					result.put("status",rs.getString("status_code"));
					result.put("need_ver", rs.getInt("version"));
					
					sql = " select launch_status"
						+ " from need_trx"
						+ " where need_code = " + dbm.param(needCode, DataType.TEXT) 
						+ " and comp_code = " + dbm.param(compCode, DataType.TEXT) 
						+ " and channel_code = " + dbm.param(channelCode, DataType.TEXT) 
						+ " and version=" + dbm.param(version, DataType.INT);
					
					String launchStatus = dbm.selectSingle(sql, "launch_status");
				
					if(launchStatus!=null && !launchStatus.equals(""))
						result.put("launch_status", launchStatus);
					
					Tasks taskDao = new Tasks();
					Map<String, Object> taskBean = taskDao.getTaskById(-1, needCode, version, Tasks.FuncCode.NEED, channelCode, principal);
					
					if (taskBean!=null && taskBean.containsKey("statusCode"))
						result.put("task_status", taskBean.get("statusCode"));
				}
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}

		return result;
	}

	public ArrayList<Map<String, Object>> getNeedList(HttpServletRequest request, UserPrincipalBean principal, String criteria, String channelFilter, String statusFilter, String sortBy, String sortDir, Language langObj) throws SQLException {				
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;

		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			rs = searchNeedList(criteria, channelFilter, statusFilter, dbm, principal, sortBy, sortDir, conn);
			return getList(rs, request, principal, langObj);
		} catch (SQLException e) {
			Log.error("{Needs getNeedList 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{Needs getNeedList 2} ----------->" + e);
		} finally {
			dbm = null;
			rs = null;

			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}	

		return null;
	}

	public ResultSet searchNeedList(String criteria, String channelFilter, String statusFilter, DBManager dbm, UserPrincipalBean principal, String sortBy, String sortDir, Connection conn)throws Exception {
		ResultSet rs = null;

		/////////////////////////////////////////////////////////////////////////////////////////////////////			   										   
		String sql = " select *"
				   + " from v_need_list a"
				   + " inner join sys_name_mst b" 
				   + " on a.need_name = b.name_code"
				   + " and upper(b.lang_code) = " + dbm.param(principal.getLangCode().toUpperCase(), DataType.TEXT)
				   + " where a.comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT); 	

		//Need status
		if(statusFilter != null && !statusFilter.equals("")){
			if(!statusFilter.equals("*"))
				sql += " and a.status_code = " + dbm.param(statusFilter, DataType.TEXT); 	
		}

		//channel code
		if(channelFilter != null && !channelFilter.equals("")){
			if(!channelFilter.equals("*"))
				sql += " and a.channel_code = " + dbm.param(channelFilter, DataType.TEXT);
		}

		//Searching criteria by user input
		if(criteria != null && !criteria.equals("")){	
			criteria = '%' + criteria.toUpperCase() +'%';

			sql += " and (upper(a.need_code) like "+ dbm.param(criteria, DataType.TEXT) 
					+ " or upper(b.name_desc) like "+ dbm.param(criteria, DataType.TEXT)
					+ " or upper(a.remark) like " + dbm.param(criteria, DataType.TEXT) +")";
		}

		if (!sortBy.equals("")) {
			if (sortDir.equals("A"))
				sortDir = "asc";
			else
				sortDir = "desc";

			sql += " order by a." + sortBy + " " + sortDir;	
		} else {
			//Default sorting
			sql += " order by a.need_code";	
		}		
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		rs = dbm.select(sql, conn);

		return rs;
	}
	
	public JsonArray getMenu(String needCode, UserPrincipalBean principal, String channelCode, int version) throws SQLException{
		return DynMenu.getMenu(null, needCode, null, principal, channelCode, version);
	}


	public Map<String, Object> getTaskById(int id, String need_code, int version, UserPrincipalBean principal, Language langObj) throws SQLException{
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		Map<String, Object> task = null;
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();

			String sql = " select task_code,"
					   + " item_code,"
					   + " ver_no,"
					   + " task_desc,"
					   + " ref_no,"
					   + " eff_date,"
					   + " upd_date,"
					   + " create_date,"
					   + " upd_by,"
					   + " create_by,"
					   + " status,"
					   + " status_code"
					   + " from v_task_list "
					   + " where 1=1";

			if(id>=0) {
				sql += " and task_code = " + dbm.param(id, DataType.INT);
			} else if(need_code != null && !need_code.equals("") && version >0){
				sql += " and item_code = " + dbm.param(need_code, DataType.TEXT);
				sql += " and ver_no = " + dbm.param(version, DataType.INT);
			}
			sql += " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT);

			rs = dbm.select(sql, conn);

			if(rs.next()){
				SimpleDateFormat dateformatter = Function.getDateFormat(principal);
				SimpleDateFormat dateTimeformatter = Function.getDateTimeFormat(principal);
				String upd_datetime = "";
				String create_datetime = "";

				try {upd_datetime = dateTimeformatter.format(rs.getDate("upd_date"));} catch (Exception e) {}

				try {create_datetime = dateTimeformatter.format(rs.getDate("create_date"));} catch (Exception e) {}

				String upd = (rs.getString("UPD_BY") != null && !upd_datetime.isEmpty())?Function2.getUserName(rs.getString("UPD_BY")) + " " + langObj.getNameDesc("ON") + " " +  upd_datetime : "-";

				String create = (rs.getString("create_By") != null && !create_datetime.isEmpty())?Function2.getUserName(rs.getString("create_By")) + " " + langObj.getNameDesc("ON") + " " +  create_datetime : "-";

				task = new HashMap<String, Object>();
				List<Object> taskId = new ArrayList<Object>();
				taskId.add(rs.getInt("task_code"));
				task.put("taskId", gson.toJsonTree(taskId));
				task.put("task_desc", rs.getObject("task_desc"));
				task.put("item_code", rs.getString("item_code"));
				task.put("ver_no", rs.getInt("ver_no"));
				task.put("ref_no", rs.getObject("ref_no"));
				task.put("status", rs.getString("status").equals("") ? "" : langObj.getNameDesc(rs.getString("status")));			
				task.put("statusCode", rs.getObject("status_code"));
				task.put("effDate", rs.getDate("eff_date").getTime());
				task.put("upd", upd);
				task.put("create", create);
			}
		} catch (SQLException e) {
			Log.error("{Tasks getTaskById 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{Tasks getTaskById 2} ----------->" + e);
		} finally {
			dbm = null;
			rs = null;

			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}	

		return task;
	}

	public ArrayList<Map<String, Object>> getList(ResultSet rs, HttpServletRequest request, UserPrincipalBean principal, Language langObj) throws Exception {
		ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		String comp_code = "";
		String need_code = "";
		String status_code = "";
		String channel_code = "";
		String status = "";
		String version = "";
		String remark = "";
		String need_name = "";

		while (rs.next()) {
			Map<String, Object> row = new HashMap<String, Object>();

			if(rs.getObject("comp_code") != null && !rs.getObject("comp_code").equals(""))
				comp_code = rs.getObject("comp_code").toString();

			if(rs.getObject("need_code") != null && !rs.getObject("need_code").equals(""))
				need_code = rs.getObject("need_code").toString();    

			if(rs.getObject("need_name") != null && !rs.getObject("need_name").equals(""))
				need_name = rs.getObject("need_name").toString();

			if(rs.getObject("status_code") != null && !rs.getObject("status_code").equals(""))
				status_code = rs.getObject("status_code").toString();    

			if(rs.getObject("channel_code") != null && !rs.getObject("channel_code").equals(""))
				channel_code = rs.getObject("channel_code").toString();		

			if(rs.getObject("status") != null && !rs.getObject("status").equals(""))
				status = rs.getObject("status").toString();		

			if(rs.getObject("version") != null && !rs.getObject("version").equals(""))
				version = rs.getObject("version").toString();	

			if(rs.getObject("remark") != null && !rs.getObject("remark").equals(""))
				remark = rs.getObject("remark").toString();

			row.put("id", rs.getObject("need_code"));
			row.put("comp_code", comp_code);
			row.put("need_code", need_code);
			row.put("need_name", need_code.equals("") ? "" : langObj.getNameDesc(need_name));
			row.put("status_code", status_code);
			row.put("channel_code", channel_code);
			row.put("status", status.equals("") ? "" : langObj.getNameDesc(status));
			row.put("version", version);
			row.put("remark", remark);

			list.add(row);

			//Reset all values before going next record
			comp_code = "";
			need_code = "";
			status_code = "";
			channel_code = "";
			status = "";
			version = "";
			remark = "";
			need_name = "";
		}

		return list;
	}
 
	//Get user channel list
	public List<Option> getUserChannelList(String channelList, String compCode, Language langObj) throws SQLException {
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		List<Option> options = new ArrayList<Option>();

		try {    
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			String sql = " select channel_name,"
					   + " channel_code"
					   + " from sys_channel_mst"
					   + " where comp_code = " + dbm.param(compCode, DataType.TEXT)
					   + " order by channel_name";
			
			rs = dbm.select(sql, conn);

			String[] _channelList = channelList.split(",");
			String channel_code = "";
			String channel_name = "";
			String need_channel_filter = langObj.getNameDesc("NEED_CHANNEL_FILTER");
			
			while (rs.next()) {
				for(String channel: _channelList) {
					channel = channel.replace("[", "");
					channel = channel.replace("]", "");

					channel_code = rs.getObject("channel_code").toString();
					channel_name = rs.getObject("channel_name").toString();

					if (channel.equals(channel_code) || channel.equals("*")) {
						Option option = new Option(channel_name, channel_code, need_channel_filter);
						 
						options.add(option);
					}
				}
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;

			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}    

		return options;
	}
	
	public Map<String, JSONObject> getNeedsSections(String needCode, UserPrincipalBean principal, int version, String sectionId, String channelCode) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		Map<String, JSONObject> sections = null;
		ResultSet rs = null;
		String compCode = principal.getCompCode();
		
		try {
			if (needCode != null && !needCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				String sql = " select section_code,"
						   + " json_file"
						   + " from need_section"
						   + " where need_code=" + dbm.param(needCode, DataType.TEXT)
						   + " and comp_code=" + dbm.param(compCode, DataType.TEXT)
						   + " and version=" + dbm.param(version, DataType.INT)
						   + " and channel_code=" + dbm.param(channelCode, DataType.TEXT)
						   + " and status='A' ";
				 
				if (sectionId != null && !sectionId.isEmpty())
					sql += " and section_code = " + dbm.param(sectionId, DataType.TEXT);
				
				rs = dbm.select(sql, conn);
				dbm.clear();
				
				if (rs != null) {
					sections = new HashMap<String, JSONObject>();
				
					while (rs.next()) {
						sections.put(rs.getString("section_code"), new JSONObject(rs.getString("json_file")));
					}
				}

			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return sections;
	}
		
	
	public Map<String, Object> getNeedsAttachContent(String needCode, UserPrincipalBean principal, int version, String channelCode, String sectionCode) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		Map<String, Object> sections = null;
		ResultSet rs = null;
		String compCode = principal.getCompCode();
		
		try {
			if (needCode != null && !needCode.isEmpty() && compCode != null && !compCode.isEmpty() && !channelCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
				String sql = " select attach_code,"
						   + " attach_file"
						   + " from need_attach"
						   + " where need_Code = " + dbm.param(needCode, DataType.TEXT)
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " and version = " + dbm.param(version, DataType.INT)
						   + " and channel_code = " + dbm.param(channelCode, DataType.TEXT)
						   + " and status = 'A'"
						   + " and section_code = " + dbm.param(sectionCode, DataType.TEXT);
				 
				rs = dbm.select(sql, conn);
				dbm.clear();
				
				if (rs != null) {
					sections = new HashMap<String, Object>();
					
					while (rs.next()) {	
						sections.put(rs.getString("attach_code"), (Blob)rs.getBlob("ATTACH_FILE"));
					}
				}
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return sections;
	}
	
	public boolean saveNeedDetail(String needCode, int version, String channelCode, JSONObject needDetail, UserPrincipalBean principal) throws SQLException {

		DBManager dbm;
		Connection conn = null;
		boolean success = false;
		//String needCode = needDetail.getString("covCode");
		String compCode = principal.getCompCode();
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			
			// update need_trx record
			String sql = " update need_trx set "					
					   + " expiry_date="+((needDetail.has("expDate") && !needDetail.isNull("expDate") ) ? dbm.param(new Date(Long.parseLong(needDetail.get("expDate").toString())), DataType.DATE) : "NULL") + " , " // expiry date
					   + " base_version=" + (needDetail.has("baseVersion") ? dbm.param((int) needDetail.get("baseVersion"), DataType.INT) : "NULL") + " , " // base version
					   + " remark=" + (needDetail.has("remark")? dbm.param(needDetail.getString("remark"), DataType.TEXT) : "NULL") + ", "//remark
					   + " upd_date=CURRENT_TIMESTAMP, " //update date
					   + " upd_by = " + dbm.param(principal.getUserCode(), DataType.TEXT) // update By
					   + " where comp_code=" + dbm.param(compCode, DataType.TEXT) // comCode
					   + " and need_code=" + dbm.param(needCode, DataType.TEXT) //need code
					   + " and version=" + dbm.param(version, DataType.INT) //hardCode if null
					   + " and channel_code=" + dbm.param(channelCode, DataType.TEXT);
			
			if (dbm.update(sql, conn) > 0)
				success = true;
			
			if(!success)
				conn.rollback();
			
			conn.commit();
		} catch (Exception e) {
			Log.error(e);
			if (conn != null && !conn.isClosed())
				conn.rollback();
		} finally {
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}

		return success;
	}
	
	public boolean saveDynMenuName( String needCode, String nameCode ,   String langCode, String menuName , String status,UserPrincipalBean principal ,String channelCode, int version) throws SQLException {
		boolean success = false;
		DBManager dbm;
		Connection conn = null;

		try {
			String compCode = principal.getCompCode();
			
			if (needCode != null && !needCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
			 
				String sql =" select count(1) as cnt"
						  + " from dyn_menu_name" 
						  +	" where lang_code=" + dbm.param(langCode, DataType.TEXT)
						  +	" and comp_code=" + dbm.param(compCode, DataType.TEXT)
						  +	" and name_code=" + dbm.param(nameCode, DataType.TEXT)
						  +	" and channel_code=" + dbm.param(channelCode, DataType.TEXT)
						  +	" and version=" + dbm.param(version, DataType.INT);
				
				int count = Integer.parseInt(dbm.selectSingle(sql, "cnt", conn));

				dbm.clear();
				
				if (count > 0) {	
					sql = " update dyn_menu_name  " 
						+ " set item_code=" + dbm.param(needCode, DataType.TEXT) + " , "			
						+ " menu_name = " + dbm.param(menuName, DataType.TEXT)+ " , "	
						+ " status = " + dbm.param(status, DataType.TEXT)	
						+ " where lang_code=" + dbm.param(langCode, DataType.TEXT) 
						+ " and comp_code=" + dbm.param(compCode, DataType.TEXT)							 
						+ " and name_code=" + dbm.param(nameCode, DataType.TEXT)
						+ " and channel_code=" + dbm.param(channelCode, DataType.TEXT)
						+ " and version=" + dbm.param(version, DataType.INT);
					
					count = dbm.update(sql, conn);
					
					if (count > 0)
						success = true;	    
				} else {
						sql = "insert into dyn_menu_name values("+							 
							dbm.param(compCode, DataType.TEXT)
							+ " , "
							+ // compCode
							dbm.param(needCode, DataType.TEXT)
							+ " , "
							+ // itemCode							
							dbm.param(nameCode, DataType.TEXT)
							+ " , "
							+ // nameCode							
							dbm.param(langCode, DataType.TEXT) 
							+ " , "
							+ // langCode
							dbm.param(menuName, DataType.TEXT)
							+ " , "
							+ // menuName							 
							dbm.param(status, DataType.TEXT)
							+ " , "
							+ // status	
							
							dbm.param(channelCode, DataType.TEXT)
							+ " , "
							+ // channelCode								
							dbm.param(version, DataType.INT)
							+ "  "
							+ // version	
							")"; 
					success = dbm.insert(sql, conn);
				}
				
				if(success)
					conn.commit();
				else
					conn.rollback();
			}
		} catch (Exception e) {
			if (conn != null && !conn.isClosed())
				conn.rollback();
			
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}

		return success;
	}
	
	public int deleteAttachment(String compCode,String needCode, String channelCode,  String sectionCode, int version,List<String> idSet ) throws SQLException {
		DBManager dbm;
		Connection conn = null;

		int update = -1;
		String sqlUpdate = "delete from need_attach where comp_code = ? and need_code = ?  and channel_code = ?  and section_code = ?    and version = ?  and ( ";
		int listSize=idSet.size();
		for (int i =0;i<listSize;i++ ) {  
        	sqlUpdate=sqlUpdate+" attach_code = ? ";
        	if(i<listSize-1){
        		sqlUpdate=sqlUpdate+" or ";
        	} 
	    }
		sqlUpdate=sqlUpdate+" ) ";	 
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			dbm.param(compCode, DataType.TEXT);
			dbm.param( needCode, DataType.TEXT);			
			
			dbm.param(channelCode, DataType.TEXT);
			dbm.param(sectionCode, DataType.TEXT);			
			dbm.param(version, DataType.INT);
		 		
			
			for (Iterator<String> it = idSet.iterator(); it.hasNext(); ) {  
				 String name=it.next();
				 dbm.param(name, DataType.TEXT);		        	 
				 
		    }		
			
			update = dbm.update(sqlUpdate, conn);
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return update;
	}

	public boolean saveDynMenuList( String needCode, String menuId ,   String sectionId, String uplineMenuId, String nameCode, String sectionType, int disSeq, String status,UserPrincipalBean principal ,String channelCode, int version) throws SQLException {
		boolean success = false;
		DBManager dbm;
		Connection conn = null;

		try {
			String compCode = principal.getCompCode();
			if (needCode != null && !needCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
			 
				String sql ="select count(1) as cnt from dyn_menu_list " 
				+ "where item_code=" + dbm.param(needCode, DataType.TEXT) 
						+ " and comp_code=" + dbm.param(compCode, DataType.TEXT) 
						+ " and menu_id=" + dbm.param(menuId, DataType.TEXT)
						+ " and channel_code=" + dbm.param(channelCode, DataType.TEXT)
						+ " and version=" + dbm.param(version, DataType.INT);
				
				int count = Integer.parseInt(dbm.selectSingle(sql, "cnt", conn));

				dbm.clear();
				if (count > 0) {
					
					sql = "update dyn_menu_list " 
							+ " set section_id=" + dbm.param(sectionId, DataType.TEXT) + " , "							
							+ "   upline_menu_id=" + dbm.param(uplineMenuId, DataType.TEXT) + " , "	
							+ "   name_code=" + dbm.param(nameCode, DataType.TEXT) + " , "	
							+ "   section_type=" + dbm.param(sectionType, DataType.TEXT) + " , "	
							+ "   dis_seq=" + dbm.param(disSeq, DataType.INT) + " , "	
							+ "   status=" + dbm.param(status, DataType.TEXT)  
							+ "   where item_code=" + dbm.param(needCode, DataType.TEXT)  
							+ "   and comp_code=" + dbm.param(compCode, DataType.TEXT)   
							+ "   and menu_id=" + dbm.param(menuId, DataType.TEXT) 
							+ " and channel_code=" + dbm.param(channelCode, DataType.TEXT)  
							+ " and version=" + dbm.param(version, DataType.INT);

					count = dbm.update(sql, conn);
					if (count > 0) {
						success = true;
					}
					    
				} else {
					
					 sql = "insert into dyn_menu_list values("+							 
							dbm.param(compCode, DataType.TEXT)
							+ " , "
							+ // need_code
							dbm.param(menuId, DataType.TEXT)
							+ " , "
							+ // menuId							
							dbm.param(needCode, DataType.TEXT)
							+ " , "
							+ // itemCode
							
							dbm.param(sectionId, DataType.TEXT) 
							+ " , "
							+ // sectionId
							dbm.param(uplineMenuId, DataType.TEXT)
							+ " , "
							+ // uplineMenuId
							dbm.param(nameCode, DataType.TEXT)
							+ " , "
							+ // nameCode							 
							dbm.param(sectionType, DataType.TEXT)
							+ " , "
							+ // sectionType
							dbm.param(disSeq, DataType.INT)
							+ " , "
							+ // disSeq
							dbm.param(status, DataType.TEXT)
							+ " , "
							+ // status			
							dbm.param(channelCode, DataType.TEXT)
							+ " , "
							+ // channelCode		
							dbm.param(version, DataType.INT)
							+ "  "
							+ // version		
							")"; 
					success = dbm.insert(sql, conn);
				}
				if(success)
					conn.commit();
				else
					conn.rollback();
			}
		} catch (Exception e) {
			if (conn != null && !conn.isClosed())
				conn.rollback();
			Log.error(e);
		} finally {
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}

		return success;
	}
	
	
	public int removeUnusedAttachment(String compCode,String needCode  ,String channelCode, int version, String langCode, String sectionCode, List<String> attachCodeList ) throws SQLException {
		
		DBManager dbm;
		Connection conn = null;

		int update = -1;
		String sqlUpdate = 	" delete from need_attach  where "+
							" comp_code = ? and "+
							" need_code = ? and "+
							" channel_code = ? and "+
							" version  = ? and "+
							" lang_code  = ? and "+		
							" section_code  = ? ";
		
		if(attachCodeList.size()>0){
			sqlUpdate=sqlUpdate+" and attach_code not in ( ";
 
		  	for (int i =0;i<attachCodeList.size();i++ ) {  
		  		sqlUpdate=sqlUpdate+" ? ";
	        	if(i<attachCodeList.size()-1){
	        		sqlUpdate=sqlUpdate+" , ";
	        	} 
		    }		 
		  	sqlUpdate=sqlUpdate+" ) ";
		}
				
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			dbm.param(compCode, DataType.TEXT);
			dbm.param(needCode, DataType.TEXT);	
			dbm.param(channelCode, DataType.TEXT);	
			dbm.param(version, DataType.INT);	
			dbm.param(langCode, DataType.TEXT);
			dbm.param(sectionCode, DataType.TEXT);
			 
			if(attachCodeList.size()>0){
				for (int i =0;i<attachCodeList.size();i++ ) {  
					dbm.param(attachCodeList.get(i), DataType.TEXT);		
		        	 
			    }	
			}
			 
			update = dbm.update(sqlUpdate, conn);
			if(update>0)
				conn.commit();
		} catch (Exception e) {
			Log.error(e);
			conn.rollback();
		} finally {
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return update;
	}
	
	
 
	
	public boolean saveNeedSection(String needCode, int version, String channelCode, String sectionCode, JSONObject section, UserPrincipalBean principal) throws SQLException {
		boolean success = false;
		DBManager dbm;
		Connection conn = null;

		try {
			String compCode = principal.getCompCode();
			if (needCode != null && !needCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
				String jsonFile = section.toString();
				String sql ="select count(1) as cnt from need_section " + "where need_code=" + dbm.param(needCode, DataType.TEXT) + " and comp_code=" + dbm.param(compCode, DataType.TEXT) + " and version=" + dbm.param(version, DataType.INT) + " and section_code=" + dbm.param(sectionCode, DataType.TEXT) + " and channel_code=" + dbm.param(channelCode, DataType.TEXT)  + " and status='A'";
				int count = Integer.parseInt(dbm.selectSingle(sql, "cnt", conn));

				dbm.clear();
				if (count > 0) {
					sql = "update need_section " + " set json_file=" + dbm.param(jsonFile, DataType.CLOB) + " , " + "   upd_date=CURRENT_TIMESTAMP, " + "   upd_by=" + dbm.param(principal.getUserCode(), DataType.TEXT) + " where need_code=" + dbm.param(needCode, DataType.TEXT) + "   and comp_code=" + dbm.param(compCode, DataType.TEXT) + "   and version=" + dbm.param(version, DataType.INT) + "   and section_code=" + dbm.param(sectionCode, DataType.TEXT)  + " and channel_code=" + dbm.param(channelCode, DataType.TEXT);

					count = dbm.update(sql, conn);
					if (count > 0) {
						success = true;
					}
					    
				} else {
					sql = "insert into need_section values("
							+ dbm.param(compCode, DataType.TEXT)
							+ " , "
							+ // comCode
							dbm.param(needCode, DataType.TEXT)
							+ " , "
							+ // need_code
							dbm.param(version, DataType.INT)
							+ " , "
							+ // version
							
							dbm.param(channelCode, DataType.TEXT)
							+ " , "
							+ // channel_code
							
							dbm.param(sectionCode, DataType.TEXT) 
							+ " , "
							+ // section_code
							dbm.param(jsonFile, DataType.TEXT)
							+ " , "
							+ // json_file
							dbm.param("A", DataType.TEXT)
							+ " , "
							+ // status
							"CURRENT_TIMESTAMP, "
							+ // create date
							dbm.param(principal.getUserCode(), DataType.TEXT)
							+ " , "
							+ // create by
							"NULL, "
							+ // update date
							"NULL)"; // update By
					success = dbm.insert(sql, conn);
				}
				if(success)
					conn.commit();
				else
					conn.rollback();
			}
		} catch (Exception e) {
			if (conn != null && !conn.isClosed())
				conn.rollback();
			Log.error(e);
		} finally {
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}

		return success;
	}
	
	public JsonObject getNeedDetail(String needCode, int version, UserPrincipalBean principal, String channelCode, String langCode) throws SQLException {				
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;

		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			String compCode = principal.getCompCode();
	        String sql = " select b.need_code, c.name_desc as need_name , b.launch_date "+ 
	                  " from sys_lookup_mst a, need_trx b, sys_name_mst c "+ 
	                  " where 1=1 "+
	                  " and a.lookup_field =b.need_code "+ 
	                  " and a.name_code =c.name_code  "+ 
	                  " and a.lookup_key= 'NEED_NAME'  "+ 
	                  " and lookup_field="+dbm.param(needCode, DataType.TEXT)+ 
	                  " and need_code = " + dbm.param(needCode, DataType.TEXT) + 
	          		  " and version = " + dbm.param(version, DataType.INT) + 
	          		  " and comp_code = " + dbm.param(compCode, DataType.TEXT) + 
	          		  " and channel_code = " + dbm.param(channelCode, DataType.TEXT)+
	          		  " and lang_code= " + dbm.param(langCode, DataType.TEXT);
			rs = dbm.select(sql, conn);
			if(rs!=null && rs.next()){
				Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
				JsonObject result = new JsonObject();				 
				result.add("needCode", gson.toJsonTree(rs.getString("need_code")));
				result.add("needName", gson.toJsonTree(rs.getString("need_name")));
				
				if (rs.getTimestamp("launch_date") != null) {
					SimpleDateFormat dateTimeformatter = Function.getDateTimeFormat(principal);
					result.add("launch_date", gson.toJsonTree(dateTimeformatter.format(rs.getDate("launch_date"))));
				}
 
				return result;
			}
		} catch (SQLException e) {
			Log.error("{Needs getNeedList 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{Needs getNeedList 2} ----------->" + e);
		} finally {
			dbm = null;
			rs = null;

			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}	

		return null;
	}
	


	//TODO:BEAN
	public NeedAttachBean getNeedAttach(String compCode, String needCode, int version, String langCode, String channelCode, String attachCode, String sectionCode) throws SQLException {
		DBManager dbm = null;
		Connection conn = null;
		NeedAttachBean attach = new NeedAttachBean();
		String sqlAttach = "select comp_code, need_code, version, lang_code, channel_code, create_date, create_by, upd_date, upd_by, attach_code, attach_file, section_code from need_attach where comp_code = ? and need_code = ? and version = ? and lang_code = ? and channel_code = ? and attach_code = ? and section_code = ?";
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			dbm.param(compCode, DataType.TEXT);
			dbm.param(needCode, DataType.TEXT);
			dbm.param(version, DataType.INT);
			dbm.param(langCode, DataType.TEXT);
			dbm.param(channelCode, DataType.TEXT);
			dbm.param(attachCode, DataType.TEXT);
			dbm.param(sectionCode, DataType.TEXT);
			ResultSet rs = null;
			rs = dbm.select(sqlAttach, conn);

			if (rs.next()) {
				attach.setCompCode(rs.getString("comp_code"));
				attach.setNeedCode(rs.getString("need_code"));
				attach.setVersion(rs.getInt("version"));
				attach.setLangCode(rs.getString("lang_code"));
				attach.setLangCode(rs.getString("channel_code"));
				attach.setCreateDate(rs.getDate("create_date"));
				attach.setCreateBy(rs.getString("create_by"));
				attach.setUpdateDate(rs.getDate("upd_date"));
				attach.setUpdateBy(rs.getString("upd_by"));
				attach.setAttachCode(rs.getString("attach_code"));
				attach.setAttachFile(rs.getBlob("attach_file"));
				attach.setSectionCode(rs.getString("section_code"));
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm.clear();
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return attach;
	}

	//TODO:BEAN
	public boolean addNeedAttach(NeedAttachBean attach, String attachFile, String channelCode, String sectionCode) throws SQLException {
		DBManager dbm = null;
		Connection conn = null;
		boolean update = false;
		String sqlInsert = "insert into need_attach  (comp_code, need_code, version, lang_code,channel_code, attach_code, attach_file, status, create_date, create_by, upd_date, upd_by, section_code) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		try {

			dbm = new DBManager();
			conn = DBAccess.getConnection();
			dbm.param(attach.getCompCode(), DataType.TEXT);
			dbm.param(attach.getNeedCode(), DataType.TEXT);
			dbm.param(attach.getVersion(), DataType.INT);
			dbm.param(attach.getLangCode(), DataType.TEXT);
			dbm.param(channelCode, DataType.TEXT);
			dbm.param(attach.getAttachCode(), DataType.TEXT);
	
			dbm.param(attachFile.getBytes(), DataType.BLOB);
			dbm.param("A", DataType.TEXT);
			dbm.param(attach.getCreateDate(), DataType.DATE);
			dbm.param(attach.getCreateBy(), DataType.TEXT);
			dbm.param(attach.getCreateDate(), DataType.DATE);
			dbm.param(attach.getCreateBy(), DataType.TEXT);
			dbm.param(sectionCode, DataType.TEXT);
			
			update = dbm.insert(sqlInsert, conn);
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm.clear();
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return update;
	}
	//TODO:BEAN
	public int deleteNeedAttach(NeedAttachBean attach, String channelCode, String sectionCode) throws SQLException {
		DBManager dbm;
		Connection conn = null;

		int update = -1;
		String sqlUpdate = "delete from need_attach where comp_code = ? and need_code = ? and version = ? and lang_code = ? and channel_code = ? and attach_code = ?";
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			dbm.param(attach.getCompCode(), DataType.TEXT);
			dbm.param(attach.getNeedCode(), DataType.TEXT);
			dbm.param(attach.getVersion(), DataType.INT);
			dbm.param(attach.getLangCode(), DataType.TEXT);
			dbm.param(channelCode, DataType.TEXT);
			dbm.param(attach.getAttachCode(), DataType.TEXT);
			dbm.param(sectionCode, DataType.TEXT);
			update = dbm.update(sqlUpdate, conn);
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return update;
	}
	//TODO:BEAN
	public int updateNeedAttach(NeedAttachBean attach, String attachFile, String channelCode, String sectionCode) throws SQLException {
		DBManager dbm;
		Connection conn = null;

		int update = -1;
		String sqlUpdate = "update need_attach set attach_file = ?, upd_by = ?, upd_date = ? where comp_code = ? and need_code = ? and version = ? and lang_code = ? and channel_code = ? and attach_code = ? and section_code= ?";

		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			dbm.param(attachFile.getBytes(), DataType.BLOB);
			dbm.param(attach.getUpdateBy(), DataType.TEXT);
			dbm.param(attach.getUpdateDate(), DataType.DATE);
			dbm.param(attach.getCompCode(), DataType.TEXT);
			dbm.param(attach.getNeedCode(), DataType.TEXT);
			dbm.param(attach.getVersion(), DataType.INT);
			//TODO
			dbm.param("en", DataType.TEXT);
			dbm.param(channelCode, DataType.TEXT);
			dbm.param(attach.getAttachCode(), DataType.TEXT);
			dbm.param(sectionCode, DataType.TEXT);
			update = dbm.update(sqlUpdate, conn);
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return update;
	}
	
	public List<NeedsBean> getNeeds(String needCode, String channelCode, String compCode, UserPrincipalBean principal) throws SQLException {
		ResultSet rs = null;
		List<NeedsBean> needs = new ArrayList<NeedsBean>();
		DBManager dbm;
		Connection conn = null;

		try {
			if (needCode != null && !needCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
								
				String sql = " select c.name_desc,"
						   + " a.version,"
						   + " d.status as task_status,"
						   + " d.status_code" 
						   + " from need_trx a"
						   
						   + " inner join sys_lookup_mst b"
						   + " on a.need_code = b.lookup_field"
						   + " and b.lookup_key = 'NEED_NAME'"
						  
						   + " inner join sys_name_mst c"
						   + " on b.name_code = c.name_code"
						   + " and c.lang_code = " + dbm.param(principal.getLangCode(), DataType.TEXT)
						   
						   + " inner join v_task_list d"
					       + " on a.comp_code = d.comp_code"
					       + " and a.need_code = d.item_code"
					       + " and a.version = d.ver_no" 
					       + " and a.channel_code = d.channel_code"
					       + " and d.func_code = 'FN.NEEDS'"
						   
						   + " where a.need_code = " + dbm.param(needCode, DataType.TEXT)
						   + " and a.channel_code = " + dbm.param(channelCode, DataType.TEXT) 
						   + " and a.comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " order by a.version";
				
				rs = dbm.select(sql, conn);
				
				if (rs != null) {
					needs =  new ArrayList<NeedsBean>();
					
					while (rs.next()) {
						NeedsBean need = new NeedsBean();
						need.setNeedCode(needCode);
						need.setNeedName(rs.getString("name_desc"));
						need.setVersion(rs.getInt("version"));
						need.setTaskStatus(rs.getString("task_status"));
						needs.add(need);
					}
				}
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		
		return needs;
	}
	
	public int createNewVersion(UserPrincipalBean principal, String need_code, String new_channel_code, String old_channel_code, boolean isOverwrite) throws SQLException {
		DBManager dbm = null;
		Connection conn = null;
		int new_version = -1;
		int old_version = -1;
		String need_name = "";
		String sql = "";
		boolean output = false;
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			
			//Delete all records if the action is apply to with tentative record
			if (isOverwrite)
			{
				int currentVersion = -1;
				
				//////////////////////////////////////////////////////////////////
				//Get current version number for tentative record
				sql = " select version"
					+ " from need_trx "
					+ " where need_code = " + dbm.param(need_code, DataType.TEXT)
					+ " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					+ " and channel_code = " + dbm.param(new_channel_code, DataType.TEXT);
				
				String str_currentVersion = dbm.selectSingle(sql, "version", conn);
				
				if (str_currentVersion != null )
					currentVersion = Integer.parseInt(str_currentVersion);
				
				dbm.clear();
				//////////////////////////////////////////////////////////////////
				
				if (currentVersion == -1)  {
					conn.rollback();
					return 0;
				}

				//////////////////////////////////////////////////////////////////
				//delete need_trx for tentative record
				sql = " delete "
					+ " from need_trx "
					+ " where need_code = " + dbm.param(need_code, DataType.TEXT)
					+ " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					+ " and channel_code = " + dbm.param(new_channel_code, DataType.TEXT)
					+ " and version = " + dbm.param(currentVersion, DataType.INT);
				
				output = dbm.insert(sql, conn);
				dbm.clear();
				//////////////////////////////////////////////////////////////////
				
				if (output == false){
					conn.rollback();
					return 0;
				}
				
				//////////////////////////////////////////////////////////////////
				//delete need_section for tentative record
				sql = " delete "
					+ " from need_section "
					+ " where need_code = " + dbm.param(need_code, DataType.TEXT)
					+ " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					+ " and channel_code = " + dbm.param(new_channel_code, DataType.TEXT)
					+ " and version = " + dbm.param(currentVersion, DataType.INT);
					
				output = dbm.insert(sql, conn);
				dbm.clear();
				//////////////////////////////////////////////////////////////////

				if (output == false){
					conn.rollback();
					return 0;
				}
				
				//////////////////////////////////////////////////////////////////
				//delete need_section for tentative record
				sql = " delete "
					+ " from need_attach "
					+ " where need_code = " + dbm.param(need_code, DataType.TEXT)
					+ " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					+ " and channel_code = " + dbm.param(new_channel_code, DataType.TEXT)
					+ " and version = " + dbm.param(currentVersion, DataType.INT);
				
				output = dbm.insert(sql, conn);
				dbm.clear();
				//////////////////////////////////////////////////////////////////
				
				if (output == false){
					conn.rollback();
					return 0;
				}
				
				//////////////////////////////////////////////////////////////////
				//delete task_mst for tentative record
				sql = " delete "
					+ " from task_mst "
					+ " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					+ " and func_code = " + dbm.param("FN.NEEDS", DataType.TEXT)
					+ " and item_code = " + dbm.param(need_code, DataType.TEXT)
					+ " and channel_code = " + dbm.param(new_channel_code, DataType.TEXT)
					+ " and ver_no = " + dbm.param(currentVersion, DataType.INT);
					
				output = dbm.insert(sql, conn);
				dbm.clear();
				//////////////////////////////////////////////////////////////////
				
				if (output == false){
					conn.rollback();
					return 0;
				}
				
				//////////////////////////////////////////////////////////////////
				//delete dyn_menu_name for tentative record
				sql = " delete "
					+ " from dyn_menu_name "
					+ " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					+ " and item_code = " + dbm.param(need_code, DataType.TEXT)
					+ " and channel_code = " + dbm.param(new_channel_code, DataType.TEXT)
					+ " and version = " + dbm.param(currentVersion, DataType.INT);
				
				output = dbm.insert(sql, conn);
				dbm.clear();
				//////////////////////////////////////////////////////////////////

				if (output == false){
					conn.rollback();
					return 0;
				}
				
				//////////////////////////////////////////////////////////////////
				//delete dyn_menu_list for tentative record
				sql = " delete "
					+ " from dyn_menu_list "
					+ " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					+ " and item_code = " + dbm.param(need_code, DataType.TEXT)
					+ " and channel_code = " + dbm.param(new_channel_code, DataType.TEXT)
					+ " and version = " + dbm.param(currentVersion, DataType.INT);
								
				output = dbm.insert(sql, conn);
				dbm.clear();
				//////////////////////////////////////////////////////////////////

				if (output == false){
					conn.rollback();
					return 0;
				}
			}		
			
			//////////////////////////////////////////////////////////////////
			//Get new version number
			sql = " select max(version) + 1 as version "
				+ " from need_trx "
				+ " where need_code = " + dbm.param(need_code, DataType.TEXT)
				+ " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
				+ " and channel_code = " + dbm.param(new_channel_code, DataType.TEXT);
		
			String str_new_version = dbm.selectSingle(sql, "version", conn);
			
			if (str_new_version != null )
				new_version = Integer.parseInt(str_new_version);
			else
				new_version = 1;
			
			dbm.clear();
			//////////////////////////////////////////////////////////////////
						
			if (new_version == -1)  {
				conn.rollback();
				return 0;
			}
			
			//////////////////////////////////////////////////////////////////
			//Get production version number
			sql = " select max(version) as version"
				+ " from need_mst"
				+ " where need_code = " + dbm.param(need_code, DataType.TEXT)
				+ " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
				+ " and channel_code = " + dbm.param(new_channel_code, DataType.TEXT);
			
			String str_old_version = dbm.selectSingle(sql, "version", conn);
			
			if (str_old_version != null )
				old_version = Integer.parseInt(str_old_version);	
			else 
				old_version = 1;
			
			dbm.clear();
			//////////////////////////////////////////////////////////////////
			
			if (old_version == -1)  {
				conn.rollback();
				return 0;
			}
			
			//////////////////////////////////////////////////////////////////
			//Get production need name
			sql = " select b.name_desc"
				+ " from sys_lookup_mst a"
				+ " inner join sys_name_mst b"
				+ " on a.name_code = b.name_code"
				+ " and upper(b.lang_code) = " + dbm.param(Constants.DEFAULT_LANG, DataType.TEXT)
				+ " where a.lookup_field = " + dbm.param(need_code, DataType.TEXT)
				+ " and a.lookup_key = 'NEED_NAME'";
			
			need_name = dbm.selectSingle(sql, "name_desc");
			dbm.clear();
			//////////////////////////////////////////////////////////////////
				
			if (need_name.equals("")) {
				conn.rollback();
				return 0;
			}
							
			//////////////////////////////////////////////////////////////////
			//Insert new task_mst based on the production version
			sql = " insert into task_mst(COMP_CODE,FUNC_CODE,ITEM_CODE,VER_NO,TASK_DESC,EFF_DATE,REF_NO,STATUS,CREATE_DATE,CREATE_BY,UPD_DATE,UPD_BY,CHANNEL_CODE)  "
				+ "select "
				+ dbm.param(principal.getCompCode(), DataType.TEXT) + ","
				+ dbm.param("FN.NEEDS", DataType.TEXT) + ","
				+ dbm.param(need_code, DataType.TEXT) + ","
				+ dbm.param(new_version, DataType.INT) + ","
				+ dbm.param("Create from "+need_name+" (v."+old_version+") by channel " + Functions.getChannelName(old_channel_code, principal.getCompCode()) , DataType.TEXT) + ","
				+ "eff_date" + ","
				+ "ref_no" + ","
				+ dbm.param("U", DataType.TEXT) + ","
				+ "SYSDATE,"
				+ dbm.param(principal.getUserCode(), DataType.TEXT) + ","
				+ "SYSDATE,"
				+ dbm.param(principal.getUserCode(), DataType.TEXT) + ","
				+ dbm.param(new_channel_code, DataType.TEXT)
                + " from task_mst"
				+ " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
				+ " and func_code = " + dbm.param("FN.NEEDS", DataType.TEXT)
				+ " and item_code = " + dbm.param(need_code, DataType.TEXT)
				+ " and channel_code = " + dbm.param(old_channel_code, DataType.TEXT)
				+ " and ver_no = " + dbm.param(old_version, DataType.INT);
                
			output = dbm.insert(sql, conn);
			dbm.clear();
			//////////////////////////////////////////////////////////////////
		
			if (output == false){
				conn.rollback();
				return 0;
			}
			
			//////////////////////////////////////////////////////////////////
			//Insert new need_section based on the production version
			sql = " insert into need_section(COMP_CODE,NEED_CODE,VERSION,CHANNEL_CODE,SECTION_CODE,JSON_FILE,STATUS,CREATE_DATE,CREATE_BY,UPD_DATE,UPD_BY) "
				+ "select "
				+ dbm.param(principal.getCompCode(), DataType.TEXT) + ","
				+ dbm.param(need_code, DataType.TEXT) + ","
				+ dbm.param(new_version, DataType.INT) + ","
				+ dbm.param(new_channel_code, DataType.TEXT) + ","
				+ "section_code,"
				+ "json_file,"
				+ dbm.param("A", DataType.TEXT) + ","
				+ "SYSDATE,"
				+ dbm.param(principal.getUserCode(), DataType.TEXT) + ","
				+ "SYSDATE,"
				+ dbm.param(principal.getUserCode(), DataType.TEXT)
				+ " from need_section"
				+ " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
				+ " and need_code = " + dbm.param(need_code, DataType.TEXT)
				+ " and version = " + dbm.param(old_version, DataType.INT)
				+ " and channel_code = " + dbm.param(old_channel_code, DataType.TEXT);
				
			output = dbm.insert(sql, conn);
			dbm.clear();
			//////////////////////////////////////////////////////////////////
			
			if (output == false){
				conn.rollback();
				return 0;
			}
			
			//////////////////////////////////////////////////////////////////
			//Insert new need_attach based on the production version
			sql = "insert into need_attach(COMP_CODE,NEED_CODE,VERSION,LANG_CODE,CHANNEL_CODE,ATTACH_CODE,ATTACH_FILE,STATUS,CREATE_DATE,CREATE_BY,UPD_DATE,UPD_BY,SECTION_CODE) "
				+ "select "
				+ dbm.param(principal.getCompCode(), DataType.TEXT) + ","
				+ dbm.param(need_code, DataType.TEXT) + ","
				+ dbm.param(new_version, DataType.INT) + ","
				+ "lang_code,"
				+ dbm.param(new_channel_code, DataType.TEXT) + ","
				+ "attach_code,"
				+ "attach_file,"
				+ dbm.param("A", DataType.TEXT) + ","
				+ "SYSDATE,"
				+ dbm.param(principal.getUserCode(), DataType.TEXT) + ","
				+ "SYSDATE,"
				+ dbm.param(principal.getUserCode(), DataType.TEXT) + ","
				+ "section_code"
				+ " from need_attach"
				+ " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
				+ " and need_code = " + dbm.param(need_code, DataType.TEXT)
				+ " and version = " + dbm.param(old_version, DataType.INT)
				+ " and channel_code = " + dbm.param(old_channel_code, DataType.TEXT);
				
			output = dbm.insert(sql, conn);
			dbm.clear();
			//////////////////////////////////////////////////////////////////
				
			if (output == false){
				conn.rollback();
				return 0;
			}
			
			//////////////////////////////////////////////////////////////////
			//Insert new need_trx record			
			sql = "insert into need_trx(COMP_CODE,NEED_CODE,VERSION,CHANNEL_CODE,REMARK,STATUS,LAUNCH_DATE,EXPIRY_DATE,BASE_VERSION,LAUNCH_STATUS,CREATE_DATE,CREATE_BY,UPD_DATE,UPD_BY) "
				+ "select "
				+ dbm.param(principal.getCompCode(), DataType.TEXT) + ","
				+ dbm.param(need_code, DataType.TEXT) + ","
				+ dbm.param(new_version, DataType.INT) + ","
				+ dbm.param(new_channel_code, DataType.TEXT) + ","
				+ "remark,"
				+ dbm.param("A", DataType.TEXT) + ","
				+ "launch_date,"
				+ "expiry_date,"
				+ "base_version,"
				+ dbm.param("P", DataType.TEXT) + ","
				+ "SYSDATE,"
				+ dbm.param(principal.getUserCode(), DataType.TEXT) + ","
				+ "SYSDATE,"
				+ dbm.param(principal.getUserCode(), DataType.TEXT)
				+ " from need_trx"
				+ " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
				+ " and need_code = " + dbm.param(need_code, DataType.TEXT)
				+ " and version = " + dbm.param(old_version, DataType.INT)
				+ " and channel_code = " + dbm.param(old_channel_code, DataType.TEXT);
		
			output = dbm.insert(sql, conn);
			dbm.clear();
			//////////////////////////////////////////////////////////////////
			
			if (output == false){
				conn.rollback();
				return 0;
			}
			
			//////////////////////////////////////////////////////////////////
			//Insert new dyn_menu_name record			
			sql = "insert into dyn_menu_name(COMP_CODE,ITEM_CODE,VERSION,CHANNEL_CODE,NAME_CODE,LANG_CODE,MENU_NAME,STATUS) "
				+ "select "
				+ dbm.param(principal.getCompCode(), DataType.TEXT) + ","
				+ dbm.param(need_code, DataType.TEXT) + ","
				+ dbm.param(new_version, DataType.INT) + ","
				+ dbm.param(new_channel_code, DataType.TEXT) + ","
				+ "name_code,"
				+ "lang_code,"
				+ "menu_name,"
				+ dbm.param("A", DataType.TEXT)
				+ " from dyn_menu_name"
				+ " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
				+ " and item_code = " + dbm.param(need_code, DataType.TEXT)
				+ " and version = " + dbm.param(old_version, DataType.INT)
				+ " and channel_code = " + dbm.param(old_channel_code, DataType.TEXT);
			
			output = dbm.insert(sql, conn);
			dbm.clear();
			//////////////////////////////////////////////////////////////////
			
			if (output == false){
				conn.rollback();
				return 0;
			}
			
			//////////////////////////////////////////////////////////////////
			//Insert new dyn_menu_list record			
			sql = "insert into dyn_menu_list(COMP_CODE,ITEM_CODE,VERSION,CHANNEL_CODE,SECTION_ID,UPLINE_MENU_ID,NAME_CODE,SECTION_TYPE,DIS_SEQ,MENU_ID,STATUS) "
				+ "select "
				+ dbm.param(principal.getCompCode(), DataType.TEXT) + ","
				+ dbm.param(need_code, DataType.TEXT) + ","
				+ dbm.param(new_version, DataType.INT) + ","
				+ dbm.param(new_channel_code, DataType.TEXT) + ","
				+ "section_id,"
				+ "upline_menu_id,"
				+ "name_code,"
				+ "section_type,"
				+ "dis_seq,"
				+ "menu_id,"
				+ dbm.param("A", DataType.TEXT)
				+ " from dyn_menu_list"
				+ " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
				+ " and item_code = " + dbm.param(need_code, DataType.TEXT)
				+ " and version = " + dbm.param(old_version, DataType.INT)
				+ " and channel_code = " + dbm.param(old_channel_code, DataType.TEXT);
			
			output = dbm.insert(sql, conn);
			dbm.clear();
			//////////////////////////////////////////////////////////////////
			
			if (output == false){
				conn.rollback();
				return 0;
			}
			
			conn.commit();
		} catch (Exception e) {
			Log.error(e);
			conn.rollback();
			new_version = 0;
		} finally {
			dbm.clear();
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		
		return new_version;
	}
 
	public boolean isTentativeStatus(String needCode, String channelCode, UserPrincipalBean principal) throws SQLException{
		Connection conn = null;
		DBManager dbm = null;
		String launch_status = "";
		boolean result = false;
		
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select launch_status "
					   + " from need_trx "
					   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					   + " and need_code = " + dbm.param(needCode, DataType.TEXT)
					   + " and channel_code = " + dbm.param(channelCode, DataType.TEXT)
			   		   + " and launch_status = " + dbm.param("P", DataType.TEXT);

			launch_status = dbm.selectSingle(sql, "launch_status", conn);

			if (launch_status.equals("P"))//P = Tentative
				result = true;
			else 
				result = false;	
		} catch (SQLException e) {
			Log.error("{Needs isTentativeStatus 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{Needs isTentativeStatus 2} ----------->" + e);
		} finally {
			dbm = null;

			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}	

		return result;
	}
	 
	
	public void needDataInitial(String compCode, String channelCode) throws SQLException  {
		Connection conn = null;
		DBManager dbm = null;
		String sql = "";
		boolean output = false;
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			
			sql = " insert into need_trx (COMP_CODE, NEED_CODE, VERSION, CHANNEL_CODE, CREATE_DATE, CREATE_BY) "
  			    + " select "
  			    + dbm.param(compCode, DataType.TEXT)
  			    + " ,lookup_field"
  			    + " ,1,"
  			    + dbm.param(channelCode, DataType.TEXT)
  			    + " ,sysdate,"
  			    + " 'SYSTEM'  "
  			    + " from sys_lookup_mst "
  			    + " where lookup_key = 'NEED_NAME' "
  			    + " order by seq ";
			
			output = dbm.insert(sql, conn);
			dbm.clear();
			
			if (output == false){
				conn.rollback();
				return;
			}
			
			conn.commit();
		} catch(SQLException e) {
			Log.error("{needDataInitial 1} ----------->" + e);
			conn.rollback();
		} catch(Exception e) {
			Log.error("{needDataInitial 2} ----------->" + e);
			conn.rollback();
		} finally {
			dbm = null;
			
			if (conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
	}

	public boolean updateNeedMst(String needCode, int version, String compCode, String channelCode) throws SQLException{
		boolean success = false;
		DBManager dbm;
		Connection conn = null;

		try {
			if (needCode != null && !needCode.isEmpty() && compCode != null && !compCode.isEmpty() && channelCode != null && !channelCode.isEmpty() && version > 0) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
				
				String sql = " select count(1) as cnt "
						   + " from need_mst "
						   + " where need_code = " + dbm.param(needCode, DataType.TEXT)
						   + " and channel_code = " + dbm.param(channelCode, DataType.TEXT)
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT);
				int count = Integer.parseInt(dbm.selectSingle(sql, "cnt"));
				dbm.clear();
				
				if(count > 0){
					sql = " update need_mst "
						+ " set version = " + dbm.param(version, DataType.INT) + ","	
						+ " upd_date = sysdate,"
						+ " upd_by = " + dbm.param("SYSTEM", DataType.TEXT) 
						+ " where need_code = " + dbm.param(needCode, DataType.TEXT) 
						+ " and channel_code = " + dbm.param(channelCode, DataType.TEXT)
						+ " and comp_code = " + dbm.param(compCode, DataType.TEXT);
					
					if(dbm.update(sql)>0)
						success = true;
				}else{
					sql = " insert into need_mst (comp_code, need_code, version, channel_code, need_status, status, create_date, create_by, upd_date, upd_by) values (" +
							dbm.param(compCode, DataType.TEXT) + ", " +
							dbm.param(needCode, DataType.TEXT) + ", " +
							dbm.param(version, DataType.INT) + ", " +
							dbm.param(channelCode, DataType.TEXT) + ", " +
							dbm.param("E", DataType.TEXT) + ", " +
							dbm.param("A", DataType.TEXT) + ", " +
							"sysdate, " +
							dbm.param("SYSTEM", DataType.TEXT) + " , " +
							"sysdate, " +
							dbm.param("SYSTEM", DataType.TEXT) + ")" ;
					
					success = dbm.insert(sql);
				}
			}
			conn.commit();
		} catch (Exception e) {
			conn.rollback();
			Log.error(e);
		} finally {
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}

		return success;
	}
	
	public int deleteNeedsAnaylsis(String compCode,String needCode  ,String channelCode, int version, String langCode, String sectionCode, List<String> idSet , String restrictPrefix) throws SQLException {
		
		DBManager dbm;
		Connection conn = null;

		int update = -1;
		//delete menu list
		String deleteMenuListSql = "delete from dyn_menu_List where comp_code = ? and item_code = ?   and channel_Code = ?  and version = ?  and menu_id like ? ";
		int listSize=idSet.size();		 
		if(listSize>0){
			deleteMenuListSql=deleteMenuListSql+" and menu_id not in ( ";
			for (int i =0;i<listSize;i++ ) {  
				deleteMenuListSql=deleteMenuListSql+" ? ";
	        	if(i<listSize-1){
	        		deleteMenuListSql=deleteMenuListSql+" , ";
	        	} 
		    }
			deleteMenuListSql=deleteMenuListSql+" ) ";	 
		}
		//delete menu name
		String deleteMenuNameSql = "delete from dyn_menu_name where comp_code = ? and item_code = ? and lang_code = ?  and channel_code = ?  and version = ? and name_code like ? ";		 
		if(listSize>0){
			deleteMenuNameSql=deleteMenuNameSql+" and name_code not in ( ";
		
			for (int i =0;i<listSize;i++ ) {  
				deleteMenuNameSql=deleteMenuNameSql+" ? ";
	        
	        	if(i<listSize-1)
	        		deleteMenuNameSql=deleteMenuNameSql+" , ";
		    }
			deleteMenuNameSql=deleteMenuNameSql+" ) ";	 
		}
		//delete section
		String deleteSectionSql = "delete from need_section where comp_code = ? and need_code = ?   and channel_Code = ?  and version = ? and section_code like ? ";		 
		if(listSize>0){
			deleteSectionSql=deleteSectionSql+" and section_code not in ( ";
			for (int i =0;i<listSize;i++ ) {  
				deleteSectionSql=deleteSectionSql+" ? ";
	        	if(i<listSize-1){
	        		deleteSectionSql=deleteSectionSql+" , ";
	        	} 
		    }
			deleteSectionSql=deleteSectionSql+" ) ";	 
		} 
		//delete attach
		String deleteAttachSql = "delete from need_attach  where "+
		" comp_code = ? and "+
		" need_code = ? and "+
		" channel_code = ? and "+
		" version  = ? and "+ 
		" lang_code  = ? and "	+
		" section_code like ? ";
		if(idSet.size()>0){
			deleteAttachSql=deleteAttachSql+" and section_code not in ( "; 
		
		  	for (int i =0;i<idSet.size();i++ ) {  
		  		deleteAttachSql=deleteAttachSql+" ? ";
	        	if(i<idSet.size()-1){
	        		deleteAttachSql=deleteAttachSql+" , ";
	        	} 
		    }		 
		  	deleteAttachSql=deleteAttachSql+" ) ";
		}
				
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();		
			conn.setAutoCommit(false);
			//delete menu list
			dbm.param(compCode, DataType.TEXT);
			dbm.param(needCode, DataType.TEXT);	
			dbm.param(channelCode, DataType.TEXT);	
			dbm.param(version, DataType.INT);	
			dbm.param(restrictPrefix, DataType.TEXT);	 
			if(listSize>0){
				for (Iterator<String> it = idSet.iterator(); it.hasNext(); ) {  
					 String id=it.next();
					 dbm.param(id, DataType.TEXT);		        	 
					 
			    }
			}
			update = dbm.update(deleteMenuListSql, conn);
			dbm.clear(); 
			
			//delete dyn menu name
			dbm.param(compCode, DataType.TEXT);
			dbm.param(needCode, DataType.TEXT);
			dbm.param(langCode, DataType.TEXT);	
			dbm.param(channelCode, DataType.TEXT);	
			dbm.param(version, DataType.INT);	
			dbm.param(restrictPrefix, DataType.TEXT);				
			if(listSize>0){
				for (Iterator<String> it = idSet.iterator(); it.hasNext(); ) {  
					 String id=it.next();
					 dbm.param(id, DataType.TEXT);		        	 
					  
			    }
			}			
			update = dbm.update(deleteMenuNameSql, conn);
			dbm.clear();
			
			///delete section 
			dbm.param(compCode, DataType.TEXT);
			dbm.param(needCode, DataType.TEXT);	
			dbm.param(channelCode, DataType.TEXT);	
			dbm.param(version, DataType.INT);
			dbm.param(restrictPrefix, DataType.TEXT);
			if(listSize>0){
				for (Iterator<String> it = idSet.iterator(); it.hasNext(); ) {  
					 String id=it.next();
					 dbm.param(id, DataType.TEXT);		        	 
					 
			    }
			}			 
			update = dbm.update(deleteSectionSql, conn);
			dbm.clear();
			///delete attach
			dbm.param(compCode, DataType.TEXT);
			dbm.param(needCode, DataType.TEXT);	
			dbm.param(channelCode, DataType.TEXT);	
			dbm.param(version, DataType.INT);	
			dbm.param(langCode, DataType.TEXT);
			dbm.param(restrictPrefix, DataType.TEXT);			 
			if(idSet.size()>0){
				for (int i =0;i<idSet.size();i++ ) {  
					dbm.param(idSet.get(i), DataType.TEXT);		
		        	 
			    }	
			}			 
			update = dbm.update(deleteAttachSql, conn);
			dbm.clear();
			conn.commit();
		} catch (Exception e) {
			Log.error("dao.Needs.deleteNeedsAnaylsis:"+e);
			conn.rollback();
		} finally {
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return update;
	}

}

