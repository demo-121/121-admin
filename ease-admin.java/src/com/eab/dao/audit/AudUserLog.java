package com.eab.dao.audit;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class AudUserLog {
	
	/***
	 * 
	 * @param userCode: User Code
	 * @param compCode: Company Code
	 * @param ipAddrServ: Server IP Address
	 * @param ipAddrUser: Client IP Address
	 * @param sysVer: System Version
	 * @param success: Login Authentication is success
	 * @param failReason: Login Fail reason
	 * @param sourceLogin: Source Of Login
	 * @param sessionID: Session ID
	 * @return Status
	 * @throws SQLException 
	 */
	public boolean add(String userCode, String compCode, String ipAddrServ, String ipAddrUser, String sysVer, boolean success, String failReason, String sourceLogin, String sessionID) 
				throws SQLException {
		boolean result = false;
		Function2 func2 = new Function2();
		DBManager dbm = null;
		Connection conn = null;
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			
			// * for empty value (for not null)
			if (userCode == null || userCode.length() == 0)
				userCode = "*";
			if (compCode == null || compCode.length() == 0)
				compCode = "*";
			
			// "" for empty value
			if (ipAddrServ == null || ipAddrServ.length() == 0)
				ipAddrServ = "";
			if (ipAddrUser == null || ipAddrUser.length() == 0)
				ipAddrUser = "";
			if (sysVer == null || sysVer.length() == 0)
				sysVer = "";
			if (failReason == null || failReason.length() == 0)
				failReason = "";
			if (sourceLogin == null || sourceLogin.length() == 0)
				sourceLogin = "";
			if (sessionID == null || sessionID.length() == 0)
				sessionID = "";
			
			String sql = "insert into aud_user_log(user_code, comp_code, login_time, session_id, serv_ip_addr, user_ip_addr, sys_ver, success, fail_reason, source_login)"
					+ " values(" + dbm.param(userCode, DataType.TEXT)
					+ ", " + dbm.param(compCode, DataType.TEXT)
					+ ", sysdate"
					+ ", " + dbm.param(sessionID, DataType.TEXT)
					+ ", " + dbm.param(ipAddrServ, DataType.TEXT)
					+ ", " + dbm.param(ipAddrUser, DataType.TEXT)
					+ ", " + dbm.param(sysVer, DataType.TEXT)
					+ ", " + dbm.param((success)?"Y":"N", DataType.TEXT)
					+ ", " + dbm.param(failReason, DataType.TEXT)
					+ ", " + dbm.param(sourceLogin, DataType.TEXT)
					+ ")";
			
			result = dbm.insert(sql, conn);
			
			if (result) {
				conn.commit();
			} else {
				conn.rollback();
			}
			
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			Log.error(e);
		} finally {
			func2 = null;
			dbm = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
		}
		
		return result;
	}
	
	public boolean setLogoutTime(String sessionID) throws SQLException {
		boolean result = false;
		Function2 func2 = new Function2();
		DBManager dbm = null;
		Connection conn = null;
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			
			String sql = "update aud_user_log set logout_time=sysdate where session_id=" + dbm.param(sessionID, DataType.TEXT);
			int count = dbm.update(sql, conn);
			
			if (count > 0) {
				conn.commit();
				result = true;
			} else {
				conn.rollback();
				result = false;
			}
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			Log.error(e);
		} finally {
			func2 = null;
			dbm = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
		}
		
		return result;
	}
}
