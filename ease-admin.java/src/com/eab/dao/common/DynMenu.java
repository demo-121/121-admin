package com.eab.dao.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class DynMenu {
	public static JsonArray getMenu(JsonArray menu, String itemCode, String uplineCode, UserPrincipalBean principal, String channelCode, int version) throws SQLException{
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
			String compCode = principal.getCompCode();
			String langCode = principal.getLangCode();

			String sql = "select distinct section_id from v_menu_list where 1=1 and item_code = " + dbm.param(itemCode, DataType.TEXT) 
					+ " and comp_code in ('*', " + dbm.param(compCode, DataType.TEXT) 
					+ ") and lang_code = " + dbm.param(langCode, DataType.TEXT) 
					 
					+ " and upline_menu_id " + ((uplineCode==null)?"is null":(" = " + dbm.param(uplineCode, DataType.TEXT)));
			rs = dbm.select(sql, conn);
			if(rs!=null){
				if(menu == null) menu = new JsonArray();
				while (rs.next()) {
					dbm.clear();
					sql = "select distinct menu_id, section_id, dis_seq, name_desc,default_name_desc, section_type, has_submenu, upline_menu_id  from v_menu_list where 1=1 and item_code = " + dbm.param(itemCode, DataType.TEXT) 
							+ " and comp_code in ('*', " + dbm.param(compCode, DataType.TEXT) 
							+ ") and lang_code = " + dbm.param(langCode, DataType.TEXT) 
							+ " and upline_menu_id " + ((uplineCode==null)?"is null":(" = " + dbm.param(uplineCode, DataType.TEXT)));
							if(uplineCode!=null){
								if(channelCode!=null)
									sql=sql+  " and channel_code in ('*', " + dbm.param(channelCode, DataType.TEXT)+ " ) "; 
								sql=sql+  " and version = "  + dbm.param(version, DataType.INT);
							}
							 						
							sql=sql+  " and section_id = " + dbm.param(rs.getString("section_id"), DataType.TEXT) + " order by section_id, dis_seq";
					
					ResultSet rs2 = dbm.select(sql, conn);
					if(rs2!=null){
						JsonArray section = new JsonArray();
						while(rs2.next()){
							JsonObject menuItem = new JsonObject();
							menuItem.add("id", gson.toJsonTree(rs2.getString("menu_id")));
							menuItem.add("section_id", gson.toJsonTree(rs2.getString("section_id")));
							menuItem.add("seq", gson.toJsonTree(rs2.getInt("dis_seq")));
							if( rs2.getString("upline_menu_id")!=null&&rs2.getString("upline_menu_id").equals("NeedsConceptSelling") ){
								JsonObject titleObj=new JsonObject();
								titleObj.add("zh-Hant", gson.toJsonTree(rs2.getString("name_desc")==null?rs2.getString("default_name_desc"):rs2.getString("name_desc")));
								titleObj.add("en", gson.toJsonTree(rs2.getString("name_desc")==null?rs2.getString("default_name_desc"):rs2.getString("name_desc")));
								titleObj.add("id", gson.toJsonTree("title_"+rs2.getString("menu_id")));
								menuItem.add("title", titleObj);
							}else 
								menuItem.add("title", gson.toJsonTree(rs2.getString("name_desc")==null?rs2.getString("default_name_desc"):rs2.getString("name_desc")));
							menuItem.add("type", gson.toJsonTree(rs2.getString("section_type")));
							if(rs2.getString("has_submenu").equals("Y")){
								menuItem.add("items",  gson.toJsonTree(getMenu(null, itemCode, rs2.getString("menu_id"), principal, channelCode, version)));
							}
							section.add(menuItem);
						}
						menu.add(section);
					}

				}
			}
			return menu;
		} catch (SQLException e) {
			Log.error("{Needs getNeedsMenu 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{Needs getNeedsMenu 2} ----------->" + e);
		} finally {
			dbm = null;
			rs = null;

			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}	

		return null;
	}
	
	public Map<String, Object> getTaskById(int id, String bi_code, int version, UserPrincipalBean principal, Language langObj) throws SQLException{
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		Map<String, Object> task = null;
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();

			String sql = "select task_code, item_code, ver_no, task_desc, ref_no, eff_date, upd_date, create_date, upd_by, create_by, status, status_code from v_task_list where 1=1";

			if(id>=0) {
				sql += " and task_code = " + dbm.param(id, DataType.INT);
			} else if(bi_code != null && !bi_code.equals("") && version >0){
				sql += " and item_code = " + dbm.param(bi_code, DataType.TEXT);
				sql += " and ver_no = " + dbm.param(version, DataType.INT);
			}
			sql += " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT);

			rs = dbm.select(sql, conn);

			if(rs.next()){
				SimpleDateFormat dateformatter = Function.getDateFormat(principal);
				SimpleDateFormat dateTimeformatter = Function.getDateTimeFormat(principal);
				String upd_datetime = "";
				String create_datetime = "";

				try {upd_datetime = dateTimeformatter.format(rs.getDate("upd_date"));} catch (Exception e) {}

				try {create_datetime = dateTimeformatter.format(rs.getDate("create_date"));} catch (Exception e) {}



				String upd = (rs.getString("UPD_BY") != null && !upd_datetime.isEmpty())? 
						Function2.getUserName(rs.getString("UPD_BY")) + " " + langObj.getNameDesc("ON") + " " +  upd_datetime : "-";

				String create = (rs.getString("create_By") != null && !create_datetime.isEmpty())? 
						Function2.getUserName(rs.getString("create_By")) + " " + langObj.getNameDesc("ON") + " " +  create_datetime : "-";

				task = new HashMap<String, Object>();
				List<Object> taskId = new ArrayList<Object>();
				taskId.add(rs.getInt("task_code"));
				task.put("taskId", gson.toJsonTree(taskId));
				task.put("task_desc", rs.getObject("task_desc"));
				task.put("item_code", rs.getString("item_code"));
				task.put("ver_no", rs.getInt("ver_no"));
				task.put("ref_no", rs.getObject("ref_no"));
				task.put("status", rs.getString("status").equals("") ? "" : langObj.getNameDesc(rs.getString("status")));			
				task.put("statusCode", rs.getObject("status_code"));
				task.put("effDate", rs.getDate("eff_date").getTime());
				task.put("upd", upd);
				task.put("create", create);
			}
		} catch (SQLException e) {
			Log.error("{Tasks getTaskById 1} ----------->" + e);
		} catch(Exception e) {
			Log.error("{Tasks getTaskById 2} ----------->" + e);
		} finally {
			dbm = null;
			rs = null;

			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}	

		return task;
	}

}
