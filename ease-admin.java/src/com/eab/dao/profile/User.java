package com.eab.dao.profile;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.eab.common.CryptoUtil;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.dynamic.Companies;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.profile.UserBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.hibernate.HibernateUtil;

public class User {

//	public List get(String username) {
//		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
//		Session session = sessionFactory.openSession();
//		
//		Criteria criteria = session.createCriteria(UserBean.class);
//		
//		// Search criteria
//		if (username != null && username.length() > 0) {
//			criteria.add(Restrictions.eq("userCode", username));
//		}
//		
//		// Order by
//		criteria.addOrder(Order.asc("userCode"));
//		
//		return criteria.list();
//	}
	
	public UserBean get(String userCode, HttpServletRequest request) throws SQLException {
		UserBean userInfo = null;
		DBManager dbm = null;
		CryptoUtil crypto = new CryptoUtil();
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			UserPrincipalBean principal = new UserPrincipalBean();
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			String sql = "select comp_code, user_code, user_name, password, status "
					+ ", fail_cnt, gender, email "
					+ ", user_type, channel_code, pos_code, dep_code, home_no, office_no, mobile_no, language"
					+ " from sys_user_mst where upper(user_code)=" + dbm.param(userCode.toUpperCase(), DataType.TEXT);
			rs = dbm.select(sql, conn);
			if (rs != null) {
				if (rs.next()) {
					userInfo = new UserBean();
					userInfo.setCompCode(rs.getString("comp_code"));
					userInfo.setUserCode(rs.getString("user_code"));
					userInfo.setUserName(rs.getString("user_name"));
					userInfo.setStatus(rs.getString("status"));
					userInfo.setFailCnt(rs.getInt("fail_cnt"));
					userInfo.setGender(rs.getString("gender"));
					userInfo.setEmail(rs.getString("email"));
					userInfo.setUserType(rs.getString("user_type"));
					userInfo.setChannelCode(rs.getString("channel_code"));
					userInfo.setPosCode(rs.getString("pos_code"));
					userInfo.setDepCode(rs.getString("dep_code"));
					userInfo.setHomeNo(rs.getString("home_no"));
					userInfo.setOfficeNo(rs.getString("office_no"));
					userInfo.setMobileNo(rs.getString("mobile_no"));
					userInfo.setLangCode(rs.getString("language"));
					
					userInfo.setUserPassword(crypto.decryptUserData(rs.getString("user_code"), rs.getString("password")));
//					userInfo.setUserPassword(rs.getString("password"));
					
					//set user lang into session
					Language lang = new Language();
					lang.setLang(request, userInfo.getLangCode());
				}
			}
		} catch(Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			crypto = null;
			rs = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
		}
		
		return userInfo;
	}
	

	public String getRoleIdByUser(String userCode, String compCode) throws Exception {
		DBManager dbm = null;
		ResultSet rs = null;
		Connection conn = null;
		String result = null;
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			if (compCode != null && !compCode.isEmpty()) {
				String sql = "SELECT role_code FROM sys_user_user_role WHERE user_code = " + dbm.param(userCode, DataType.TEXT) 
				+ " AND comp_code = "+ dbm.param(compCode, DataType.TEXT);
	//			+ " AND channel_code = "+ dbm.param(principal.getChannelCode(), DataType.TEXT)
				
				rs = dbm.select(sql, conn);
				if (rs.next()) {
					result = rs.getString(1);
				}
				
				rs.close();
				dbm.clear();
			}
		} catch(Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
		}
		return result;
	}

	public List<String> getChannelCodeByUser(UserPrincipalBean principal) throws Exception {
		List<String> result = new ArrayList<String>();
		DBManager dbm = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			if (principal.getCompCode() != null) {
				String sql = "SELECT channel_code FROM sys_user_user_role WHERE user_code = " + dbm.param(principal.getUserCode(), DataType.TEXT) 
				+ " AND comp_code = "+ dbm.param(principal.getCompCode(), DataType.TEXT);
	//			+ " AND channel_code = "+ dbm.param(principal.getChannelCode(), DataType.TEXT);
				
				rs = dbm.select(sql, conn);
				while (rs.next()) {
					result.add(rs.getString(1));
				}
				
				rs.close();
				dbm.clear();
			}
		} catch(Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
		}

		return result;
	}
	
	//	public boolean set(UserBean user) {
//		Session session = null;
//		boolean status = false;
//		
//		SessionFactory sf = null;
//		
//		try {
//			sf = HibernateUtil.getSessionFactory();
//			
//			session = sf.openSession();
//			
//			session.beginTransaction();
//			session.save(user);
//			session.getTransaction().commit();
//			
//			status = true;
//		} catch(Exception e) {
//			Log.error(e);
//		} finally {
//			session = null;
//		}
//		
//		return status;
//	}
	
	public int changeUserLanguage(String userCode, String langCode) throws SQLException{
		DBManager dbm = null;
		int result = -1;
		try{
			dbm = new DBManager();
			String sql = "update sys_user_mst set language = " + dbm.param(langCode, DataType.TEXT) + " where user_code = " + dbm.param(userCode, DataType.TEXT);				
			result = dbm.update(sql);	
		}catch(Exception e){
			//no need to handle
		}finally{
			dbm.clear();
			dbm = null;
		}
		
		return result;
	}
	
	public int changeUserCompany(String userCode, String compCode){
		
		DBManager dbm = null;
		int result = -1;
		try{
			dbm = new DBManager();
			String sql = "update sys_user_mst set comp_code = " + dbm.param(compCode, DataType.TEXT) + " where user_code = " + dbm.param(userCode, DataType.TEXT);				
			result = dbm.update(sql);	
		}catch(Exception e){
			//no need to handle
		}finally{
			dbm.clear();
			dbm = null;
		}
		
		return result;
	}
	
}
