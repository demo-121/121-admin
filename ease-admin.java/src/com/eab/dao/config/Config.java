package com.eab.dao.config;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;

public class Config {

	DBManager dbm;
	Connection conn;

	public String getConfigJSON(String code) throws SQLException {
		if (code != null && !code.isEmpty()) {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			ResultSet rs = null;

			String sql = "select json_file from sys_config_json where func_code = '" + code + "'";

			try {
				rs = dbm.select(sql, conn);

				if (rs != null) {
					while (rs.next()) {
						return rs.getString("json_file");
					}
				}
			} catch (Exception e) {
				Log.error(e);
			} finally {
				dbm = null;
				rs = null;
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
				conn = null;
			}
		}

		return null;
	}

}
