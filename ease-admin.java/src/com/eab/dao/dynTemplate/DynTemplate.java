package com.eab.dao.dynTemplate;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Option;
import com.eab.model.dynTemplate.DynField;
import com.eab.model.dynTemplate.DynUI;
import com.eab.model.profile.UserPrincipalBean;

public class DynTemplate {
	public HashMap<String, Object> getPrimaryKeys(String taskId, Connection conn){
		ResultSet rs = null;
		DBManager dbm = new DBManager();
		boolean newConn = false;
		HashMap<String, Object> keys = new HashMap<String, Object>();
		try{
			if( conn == null){
				newConn = true;
				conn = DBAccess.getConnection();
			}
			String select = "select module, doc_code, version from task_mst where task_code = " + dbm.param(taskId, DataType.TEXT);
			rs = dbm.select(select, conn);
			if(rs != null){
				if(rs.next()){
					keys.put("module", rs.getString("module"));
					keys.put("docCode", rs.getString("doc_code"));
					keys.put("version", rs.getInt("version"));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			dbm.clear();
			dbm = null;
			rs = null;
			if(newConn){
				try {
					if(conn!= null && !conn.isClosed()){
						conn.close();
						conn = null;
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
			
		return keys;
	}
	
	public String getModuleByFuncCode(Connection conn, String funcCode){
		ResultSet rs = null;
		DBManager dbm = new DBManager();
		boolean isNew = false;
		String module = null;
		try{
			if(conn == null){
				conn = DBAccess.getConnection();
				isNew = true;
			}
			String sql = " select module "
						+" from sys_func_mst where "
						+" func_code = " + dbm.param(funcCode, DataType.TEXT)
						+ " order by access_ctrl ";
			rs = dbm.select(sql, conn);
			
			if(rs != null){
				if(rs.next()){
					module = rs.getString("module");
				}
			}
		}catch(Exception e){
			Log.error("getModuleByFuncCode : " + e.getMessage());
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				rs = null;
				if(isNew && conn != null && !conn.isClosed()){
					conn.close();
				}
			}
			catch (SQLException e) {
				Log.error("getModuleByFuncCode tc : " + e.getMessage());
			}
			dbm.clear();
			dbm = null;
		}
				
		return module;
	}
	
	public String getFuncCodeByModule(Connection conn, String module){
		ResultSet rs = null;
		DBManager dbm = new DBManager();
		boolean isNew = false;
		String funcCode = null;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
			}
			String sql = " select func_code "
						+" from sys_func_mst where "
						+" module = " + dbm.param(module, DataType.TEXT)
						+ " order by access_ctrl ";
			rs = dbm.select(sql, conn);
			
			if(rs != null){
				if(rs.next()){
					funcCode = rs.getString("func_code");
				}
			}
		}catch(Exception e){
			Log.error("getFundCodeByModule : " + e.getMessage());
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				rs = null;
				
				if(isNew && conn != null && !conn.isClosed())
					conn.close();
				
			}
			catch (SQLException e) {
				Log.error("getFundCodeByModule tc : " + e.getMessage());
			}
			dbm.clear();
			dbm = null;
		}
				
		return funcCode;
	}
	
	public List<DynField> getTrxFields(UserPrincipalBean principal, String module){
		ResultSet rs = null;
		DBManager dbm = new DBManager();
		Connection conn = null;
		List<DynField> list = new ArrayList<DynField>();
		try{
			conn = DBAccess.getConnection();
			String sql = " select id, key, type, mandatory "
						+" from dyn_trx_field_mst where "
						+" module = " + dbm.param(module, DataType.TEXT)
						+" and status = 'A'";
			rs = dbm.select(sql, conn);
			
			if(rs != null){
				while(rs.next()){
					DynField f = new DynField();
					f.setId(rs.getString("id"));
					f.setKey(rs.getString("key"));
					f.setType(rs.getString("type"));
					f.setMandatory(rs.getString("mandatory"));
					list.add(f);
					Log.error("id : " + f.getId());
				}
			}
		}catch(Exception e){
			Log.error("getTrxFields : " + e.getMessage());
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				rs = null;
				if(conn != null && !conn.isClosed())
					conn.close();
			}
			catch (SQLException e) {
				Log.error("getLKoptions tc : " + e.getMessage());
			}
			dbm.clear();
			dbm = null;
		}
				
		return list;
	}
	
	public List<Option> getLKoptions(String key){
		List<Option> options = new ArrayList<Option>();
		ResultSet rs = null;
		DBManager dbm = new DBManager();
		Connection conn = null;
		try{
			conn = DBAccess.getConnection();
			String sql = " select name_code from sys_lookup_mst"
						+ " where"
						+ " lookup_field = " + dbm.param(key, DataType.TEXT);			
			
			rs = dbm.select(sql, conn);
			
			if(rs != null){
				while(rs.next()){
					Option option = new Option(rs.getString("name_code"), rs.getString("lookup_field"));
					options.add(option);
				}
			}
		}catch(Exception e){
			Log.error("getLKoptions : " + e.getMessage());
			e.fillInStackTrace();
		}finally{
			try {
				if(rs != null && !rs.isClosed()){
					rs.close();
				}
				if(conn != null && !conn.isClosed())
					conn.close();
				
				rs = null;
			} 
			catch (SQLException e) {
				Log.error("getLKoptions tc : " + e.getMessage());
			}
		}
		return options;
	}
	
	public List<Option> getMstOptions(String key, String sql){
		List<Option> options = new ArrayList<Option>();
		ResultSet rs = null;
		DBManager dbm = new DBManager();
		Connection conn = null;
		try{
			String [] selects = key.split(",");
			conn = DBAccess.getConnection();
			rs = dbm.select(sql, conn);
			if(rs != null){
				while(rs.next()){
					Option option = new Option(rs.getString(selects[1]), rs.getString(selects[0]));
					options.add(option);
					
				}
			}
		}catch(Exception e){
			Log.error("getLKoptions : error");
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				rs = null;
				conn.close();
			}
			catch (SQLException e) {
				Log.error("getLKoptions tc : " + e.getMessage());
			}
		}
		return options;
	}
	
	public List<Option> getMstOptions(String key, String table, boolean hasLangFilter, UserPrincipalBean principal){
		String sql = " select " + key + " from " + table ;
		if(hasLangFilter)
			sql = " where lang_code = " + principal.getLangCode();
		return getMstOptions(key, sql);
	}
	
	public String getLookUpValue(DynField field, String value, String lang, Connection conn){
		DBManager dbm = new DBManager();
		ResultSet rs = null;
		try{
			String table = field.getOptionsLookUp();
			String lookupField = field.getLookUpField();
			String present = field.getPresent();
			String[] pField = present.split(",");
			String sql;

			if("sys_lookup_mst".equalsIgnoreCase(table)){
				sql = "select "
						+ "sys_name_mst.name_desc "
						+ "from "
						+ "sys_lookup_mst "
						+ "inner join sys_name_mst "
						+ "on  sys_lookup_mst.name_code = sys_name_mst.name_code "
						+ "where "
						+ "sys_lookup_mst.lookup_key = " + dbm.param(lookupField, DataType.TEXT) + " "
						+ "and lookup_field = " + dbm.param(value, DataType.TEXT) + " "
						+ "and sys_name_mst.lang_code = " + dbm.param(lang, DataType.TEXT);
				rs = dbm.select(sql, conn);
				if(rs != null && rs.next()){
					return rs.getString("name_desc");
				}
			}else{
				sql = "select " + pField[1] + " from " + table + " where " + pField[0] + " = " + dbm.param(lookupField, DataType.TEXT);	
				rs = dbm.select(sql, conn);
				if(rs != null && rs.next()){
					return rs.getString(pField[1]);
				}
			}	
		}catch(Exception e){
			Log.error("getLookUpValue error : " + e.getMessage());
			e.printStackTrace();
		}finally{
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			rs = null;
			dbm.clear();
			dbm = null;
		}
		return "";
	}
	
	public Map<String, String> getLookUpList(DynUI ui, DynField field, Connection conn, UserPrincipalBean principal, String docCode){
		
		Map<String, String> map = new HashMap<String, String>();
		DBManager dbm = new DBManager();
		ResultSet rs = null;
		try{
			String table = field.getOptionsLookUp();
			String lookupField = field.getLookUpField();
			String present = field.getPresent();
			//in table dyn_template_field_mst, column 'present' is in form [A,B], where A filter key and B is target value
			
			String sql;
			
			if("sys_lookup_mst".equalsIgnoreCase(table)){
				sql = "select "
						+ "sys_name_mst.name_desc, lookup_field "
						+ "from "
						+ "sys_lookup_mst "
						+ "inner join sys_name_mst "
						+ "on sys_lookup_mst.name_code = sys_name_mst.name_code "
						+ "where "
						+ "sys_lookup_mst.lookup_key = " + dbm.param(lookupField, DataType.TEXT) + " "
						+ "and sys_name_mst.lang_code = " + dbm.param(principal.getLangCode(), DataType.TEXT);
				rs = dbm.select(sql, conn);
				if(rs != null){
					while(rs.next()){
						map.put(rs.getString("lookup_field"), rs.getString("name_desc"));
					}
				}
			}else{
				String[] pField = present.split(",");
				String key = pField[0].replace(" ", "");
				String value = pField[1].replace(" ", "");
				sql = "select " + key + " , " + value + " from " + table;
				String filter = "";
				if(field.hasCompFilter()){
					filter = " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT);
				}
				if(field.hasLangFilter()){
					if(!"".equals(filter)){
						filter = filter + " and lang_code = " + dbm.param(principal.getLangCode(), DataType.TEXT);
					}else{
						filter = " where lang_code = " + dbm.param(principal.getLangCode(), DataType.TEXT);
					}
				}
				//if the lookupfiled = 'version', then filter by docCode
				if(docCode != null && field.getLookUpField() != null && "version".equals(field.getLookUpField())){
					String filByDoc =  " " + ui.getPrimaryKey() + " = " + dbm.param(docCode, DataType.TEXT);
					if(!"".equals(filter)){ 
						filter = filter + 	" and " + filByDoc;
					}else{
						filter = " where " + filByDoc;
					}
				}
				
				if(lookupField != null && !"".equals(lookupField) && field.getValue() != null){
					String lkFilter = lookupField + " = " + dbm.param(field.getValue(), DataType.TEXT);
					if(!"".equals(filter)){ 
						filter = filter + 	" and " + lkFilter;
					}else{
						filter = " where " + lkFilter;
					}
				}
				
				sql += filter;
				rs = dbm.select(sql, conn);
				if(rs != null ){
					while(rs.next()){
						String value_ = "";
						String key_ = rs.getString(key);
						if(value.equals("version")){
							value_ = "" + rs.getInt(value);
						}else
							value_ = rs.getString(value);
						map.put(key_, value_);
					}
				}
			}	
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			dbm.clear();
		}
		return map;
	}
	
	
	public DynUI getDynUIByTable(String table, Connection conn){
		DynUI ui = new DynUI();
		DBManager dbm = new DBManager();
		boolean newConn = false;
		try {
			if(conn == null){
				newConn = true;
				conn = DBAccess.getConnection();
			}
			String sql = "select * from dyn_template_ui_mst where master_view = " + dbm.param(table, DataType.TEXT)  + " and status = 'A'";
			ResultSet rs = dbm.select(sql, conn);
			
			if(rs != null){
				if(rs.next()){
					ui.setModule(rs.getString("module"));
					ui.setUiName(rs.getString("ui_name"));
					ui.setDataTable(rs.getString("data_table"));
					ui.setMstView(rs.getString("master_view"));
					ui.setPrimaryKey(rs.getString("primary_key"));
					ui.setSearchField(rs.getString("search_field"));
					ui.setCompFilter(rs.getString("comp_filter"));
					ui.setLangFilter(rs.getString("lang_filter"));
					ui.setPrimaryID(rs.getString("primary_id"));
					ui.setTrxSectionID(rs.getString("trx_section_id"));
					ui.setNameId(rs.getString("name_id"));
					ui.setTemplateConcat(rs.getString("template_concat"));
				}
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			if(dbm != null){
				dbm.clear();
			}
			if(newConn){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return ui;
		
	}
	
	public DynUI getDynUI(String module, Connection conn){
		DynUI ui = new DynUI();
		DBManager dbm = new DBManager();
		boolean newConn = false;
		try {
			if(conn == null){
				newConn = true;
				conn = DBAccess.getConnection();
			}
			String sql = "select * from dyn_template_ui_mst where module = " + dbm.param(module, DataType.TEXT)  + " and status = 'A'";
			ResultSet rs = dbm.select(sql, conn);
			
			if(rs != null){
				if(rs.next()){
					ui.setModule(module);
					ui.setUiName(rs.getString("ui_name"));
					ui.setDataTable(rs.getString("data_table"));
					ui.setMstView(rs.getString("master_view"));
					ui.setPrimaryKey(rs.getString("primary_key"));
					ui.setSearchField(rs.getString("search_field"));
					ui.setCompFilter(rs.getString("comp_filter"));
					ui.setLangFilter(rs.getString("lang_filter"));
					ui.setPrimaryID(rs.getString("primary_id"));
					ui.setTrxSectionID(rs.getString("trx_section_id"));
					ui.setNameId(rs.getString("name_id"));
					ui.setTemplateConcat(rs.getString("template_concat"));
				}
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			if(dbm != null){
				dbm.clear();
			}
			if(newConn){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return ui;
		
	}
	
	public List<DynField> getDynFields(String module, Connection conn){
		List<DynField> fields = new ArrayList<DynField>();
		DBManager dbm = new DBManager();
		try {

			String sql = "select * from DYN_TEMPLATE_FIELD_MST where module = " + dbm.param(module, DataType.TEXT)  + " and status = 'A'";
			ResultSet rs = dbm.select(sql, conn);
			
		if(rs != null){
			while(rs.next()){
				DynField field = new DynField();
				field.setModule(rs.getString("module"));
				field.setId(rs.getString("field_id"));
				field.setTitle(rs.getString("field_title"));
				field.setType(rs.getString("field_type"));
				field.setKey(rs.getString("field_key"));
				field.setSeq(rs.getInt("list_seq"));
				field.setOptionsLookUp(rs.getString("options_lookup"));
				field.setLookUpField(rs.getString("lookup_field"));
				field.setPresent(rs.getString("present"));
				field.setCompFilter(rs.getString("comp_filter"));
				field.setLangFilter(rs.getString("lang_filter"));
				fields.add(field);
			}
		}
			rs.close();
			dbm.clear();
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm.clear();
			dbm = null;
		}
		return fields;
	}
	 
}
