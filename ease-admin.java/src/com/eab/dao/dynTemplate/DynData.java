package com.eab.dao.dynTemplate;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import com.eab.common.ClassType;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.dynTemplate.DynUI;
import com.eab.model.dynTemplate.DynField;
import com.eab.model.dynTemplate.DynName;
import com.eab.model.profile.UserPrincipalBean;

public class DynData extends DynTemplate {
	
	public String getProductJson(Connection conn, String module, String docCode, int version){
		DBManager dbm = null;
		boolean isNew = false;
		try{
			dbm = new DBManager();
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
			}
			HashMap<String, Object> keys = new HashMap<String, Object>();
			keys.put("module", module);
			keys.put("doc_code", docCode);
			keys.put("version", version);
			
			String doc = getDynJson(keys, dbm, conn);
			return doc;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				if(isNew){
					conn.close();
				}
				if(dbm != null)
					dbm.clear();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public String getDynJson(HashMap<String, Object> keys, DBManager dbm, Connection conn){
		ResultSet rs = null;
		String result = null;
		try{
			String sql = " select json_file from dyn_json_mst ";
			
			String cond = "";
			for ( Map.Entry<String, Object> keyObj : keys.entrySet()) {
			    String key = keyObj.getKey();
			    Object value_ = keyObj.getValue();
			    String condSet = "";
			    if(value_ instanceof String){
			    	condSet = key + " = " + dbm.param((String)value_, DataType.TEXT);	    	
			    }else if(value_ instanceof Integer){
			    	condSet = key + " = " + dbm.param((int)value_, DataType.INT);
			    }
			    if("".equals(cond)){
		    		cond = condSet;
		    	}else{
		    		cond = cond + " and " + condSet;
		    	}
			}
			sql = sql + " where " + cond;
			
			rs = dbm.select(sql, conn);
			if(rs != null){
				if(rs.next()){
					String str = rs.getString("json_file");
					return str;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	public JSONArray getAllDynJson(int taskId, DBManager dbm, Connection conn){
		HashMap<String, Object> keys = new HashMap<String, Object>();
		keys.put("task_code", taskId);
		return getAllDynJson(keys, dbm, conn);
	}
	
	public JSONArray getAllDynJson(HashMap<String, Object> keys, DBManager dbm, Connection conn){
		ResultSet rs = null;
		JSONArray list = new JSONArray();
		try{
			String sql = " select doc_code, json_file, module, version from dyn_json_mst "
						+" where ";
			String cond = "";
			for ( Map.Entry<String, Object> keyObj : keys.entrySet()) {
			    String key = keyObj.getKey();
			    Object value_ = keyObj.getValue();
			    String condSet = "";
			    if(value_ instanceof String){
			    	condSet = key + " = " + dbm.param((String)value_, DataType.TEXT);	    	
			    }else if(value_ instanceof Integer){
			    	condSet = key + " = " + dbm.param((int)value_, DataType.INT);
			    }
			    if("".equals(cond)){
		    		cond = condSet;
		    	}else{
		    		cond = cond + " and " + condSet; 
		    	}
			}
			
			sql = sql + cond;
			
			rs = dbm.select(sql, conn);
			if(rs != null){
				while(rs.next()){
					JSONObject json = new JSONObject();
					json.put("id", rs.getString("doc_code"));
					json.put("json", new JSONObject(rs.getString("json_file")));
					json.put("module", rs.getString("module"));
					json.put("version", rs.getInt("version"));
					list.put(json);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			dbm.clear();
		}
		return list;
	}
	
	
	
	public boolean removeAttachments(DBManager dbm, Connection conn, String module, String docCode, int version, String attId){
		boolean result = false;
		boolean isNew = false;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
			}
			String sql = " delete from DYN_ATTACHMENT_MST"
						+" where "
						+" module = " + dbm.param(module, DataType.TEXT)
						+" and "
						+" doc_code = " + dbm.param(docCode, DataType.TEXT)
						+" and "
						+" version = " + dbm.param(version, DataType.INT)
						+" and "
						+" att_id = " + dbm.param(attId, DataType.TEXT);
			result  = (dbm.delete(sql) != -1);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(isNew){
				try {
					conn.isClosed();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	public JSONArray getAttachments(HashMap<String, Object> keys, DBManager dbm, Connection conn){
		return (JSONArray)getAttachments(keys, dbm, conn, ClassType.STRING_LIST);
	}
	
	public String getAttachment(HashMap<String, Object> keys, DBManager dbm, Connection conn){
		Object att = getAttachments(keys, dbm, conn, ClassType.STRING);
		return (String)att;
	}
	
	public Object getAttachments(HashMap<String, Object> keys, DBManager dbm, Connection conn, int returnType){
		Log.error("getAttachments: " + keys.toString());
		ResultSet rs = null;
		try{
			String sql = "select att_id, att_file from DYN_ATTACHMENT_MST ";
			String cond = "";
			for ( Map.Entry<String, Object> keyObj : keys.entrySet()) {
			    String key = keyObj.getKey();
			    Object value_ = keyObj.getValue();
			    String condSet = "";
			    if(value_ instanceof String){
			    	condSet = key + " = " + dbm.param((String)value_, DataType.TEXT);	    	
			    }else if(value_ instanceof Integer){
			    	condSet = key + " = " + dbm.param((int)value_, DataType.INT);
			    }
			    if("".equals(cond)){
		    		cond = condSet;
		    	}else{
		    		cond = cond + " and " + condSet; 
		    	}
			}
			
			
			String attId = null;
			if(keys.containsKey("attId")){
				attId = (String)keys.get("attId");
			}
			if(attId != null)
				cond =  cond + " and att_id = " + dbm.param(attId, DataType.TEXT);
			
			sql = sql + " where " + cond;
			
			rs = dbm.select(sql, conn);
			if(rs != null){
				if(returnType == ClassType.STRING){
					if(rs.next()){
						Blob b = rs.getBlob("att_file");
						String str = new String(b.getBytes(1l, (int) b.length()));
						return str;
					}
				}else if(returnType == ClassType.STRING_LIST){
					JSONArray list = new JSONArray();
					while(rs.next()){
						JSONObject listItem = new JSONObject();
						Blob b = rs.getBlob("att_file");
						String str = new String(b.getBytes(1l, (int) b.length()));
						listItem.put("attId", rs.getString("att_id"));
						listItem.put("attFile", str);
						list.put(listItem);
					}
					return list;
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean insertAttachment(int taskId, String compCode, String module, String docCode, int version, String attId, String file, DBManager dbm, Connection conn){
		boolean newConn = false;
		try{
			if(dbm == null){
				dbm = new DBManager();
			}
			if(conn == null){
				newConn = true;
				conn = DBAccess.getConnection();
			}
			
			String cols = "(task_code, comp_code, module, doc_code, version, att_id, att_file)";
			String values = "("
							+ dbm.param(taskId, DataType.INT)
							+ ", "
							+ dbm.param(compCode, DataType.TEXT)
							+ ","
							+ dbm.param(module, DataType.TEXT)
							+ ","
							+ dbm.param(docCode, DataType.TEXT)
							+ ", "
							+ dbm.param(version, DataType.INT)
							+ ", "
							+ dbm.param(attId, DataType.TEXT)
							+ ","
							+ dbm.param(file.getBytes(), DataType.BLOB)
							+ ")";
			String sql = "insert into DYN_ATTACHMENT_MST " + cols + " values " + values;
					
			return dbm.insert(sql, conn);
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			dbm.clear();
			if(newConn){
				try{
					conn.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
	public boolean updateAttachment(String compCode, String module, String docCode, int version, String attId, String file, DBManager dbm, Connection conn){
		boolean newConn = false;
		try{
			if(dbm == null){
				dbm = new DBManager();
			}
			if(conn == null){
				newConn = true;
				conn = DBAccess.getConnection();
			}
			
			String sql = "update DYN_ATTACHMENT_MST "
						+ "	set ATT_FILE = " +  dbm.param(file.getBytes(), DataType.BLOB)
						+ " where module = " + dbm.param(module, DataType.TEXT)
						+ " and doc_code = " + dbm.param(docCode, DataType.TEXT)
						+ " and version = " + dbm.param(version, DataType.INT)
						+ " and att_id = " + dbm.param(attId, DataType.TEXT);
			
			return (dbm.update(sql, conn) != -1);
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			dbm.clear();
			if(newConn){
				try{
					conn.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		
	}
	
	
	public boolean updateDynNameCode(Connection conn, String compCode, String module, String docCode, String newDocCode, int version){
		DBManager dbm = null;
		try{
			if(dbm == null){
				dbm = new DBManager();
			}
		String sql = " update dyn_name_mst set "
				+ " doc_code = " + dbm.param(newDocCode, DataType.TEXT)
				+ " where "
				+ " module = " + dbm.param(module,DataType.TEXT)
				+ " and doc_code = " + dbm.param(docCode,DataType.TEXT)
				+ " and version = " + dbm.param(version,DataType.INT)
				+ " and comp_code = "+ dbm.param(compCode,DataType.TEXT);
		dbm.update(sql,conn);
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			dbm.clear();
			if(dbm != null){
				dbm.clear();
			}
		}
		return true;		
	
	}
	public boolean insertDynName(DynName name, DBManager dbm, Connection conn){
		boolean newConn = false;
		try{
			if(dbm == null){
				dbm = new DBManager();
			}
			if(conn == null){
				newConn = true;
				conn = DBAccess.getConnection();
			}
			String sql = " insert into dyn_name_mst ";
			String columns = "( module, doc_code, version, field_id, lang_code, comp_code, name_desc)";
			String values = "(" 
							+ dbm.param(name.getModule(),DataType.TEXT)  + "," 
							+ dbm.param(name.getDocCode(),DataType.TEXT) + ","
							+ dbm.param(name.getVersion(),DataType.INT) + ","
							+ dbm.param(name.getFieldId(),DataType.TEXT) + ","
							+ dbm.param(name.getLangCode(),DataType.TEXT) + ","
							+ dbm.param(name.getCompCode(),DataType.TEXT) + ","
							+ dbm.param(name.getNameDesc(), DataType.TEXT)
							+ ")";
			sql +=  columns + " values " + values;
			dbm.insert(sql,conn);
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			dbm.clear();
			if(newConn){
				try{
					conn.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		return true;
	}
	
	public boolean updateDynName(DynName name, DBManager dbm, Connection conn){
		boolean newConn = false;
		try{
			if(dbm == null){
				dbm = new DBManager();
			}
			if(conn == null){
				newConn = true;
				conn = DBAccess.getConnection();
			}
			
			if(isNameExist(conn, name)){
				String sql = " update dyn_name_mst set "
							+ " name_desc = " + dbm.param(name.getNameDesc(), DataType.TEXT)
							+ " where "
							+ " module = " + dbm.param(name.getModule(),DataType.TEXT)
							+ " and doc_code = " + dbm.param(name.getDocCode(),DataType.TEXT)
							+ " and version = " + dbm.param(name.getVersion(),DataType.INT)
							+ " and field_id = "+ dbm.param(name.getFieldId(),DataType.TEXT)
							+ " and lang_code = "+ dbm.param(name.getLangCode(),DataType.TEXT)
							+ " and comp_code = "+ dbm.param(name.getCompCode(),DataType.TEXT);
							
				dbm.update(sql,conn);
			}else{
				String sql = " insert into dyn_name_mst ";
				String columns = "( module, doc_code, version, field_id, lang_code, comp_code, name_desc)";
				String values = "(" 
								+ dbm.param(name.getModule(),DataType.TEXT)  + "," 
								+ dbm.param(name.getDocCode(),DataType.TEXT) + ","
								+ dbm.param(name.getVersion(),DataType.INT) + ","
								+ dbm.param(name.getFieldId(),DataType.TEXT) + ","
								+ dbm.param(name.getLangCode(),DataType.TEXT) + ","
								+ dbm.param(name.getCompCode(),DataType.TEXT) + ","
								+ dbm.param(name.getNameDesc(), DataType.TEXT)
								+ ")";
				sql +=  columns + " values " + values;
				dbm.insert(sql,conn);
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			dbm.clear();
			if(newConn){
				try{
					conn.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		return true;
	}
	
	public boolean isNameExist(Connection conn, DynName name){
		HashMap<String,Object> condMap = new HashMap<String,Object>();
		condMap.put("module", name.getModule());
		condMap.put("doc_code", name.getDocCode());
		condMap.put("version", name.getVersion());
		condMap.put("field_id", name.getFieldId() );
		condMap.put("lang_code", name.getLangCode());
		condMap.put("comp_code", name.getCompCode());
		
		return Function2.hasRecordByType("dyn_name_mst", condMap, null, conn);
	}
	
	public boolean addTrxDataFromTrxData(UserPrincipalBean principal, Connection conn, String module, String docCode, int version, JSONObject modifyParam) throws SQLException{
		boolean isNew = false;
		DBManager dbm = new DBManager();
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
			}
			
			String compCode = principal.getCompCode();
			String userCode = principal.getUserCode();
			DynUI ui = getDynUI(module, conn);
			
			String sql = "select * from " + ui.getDataTable() +
					" where 1=1 " +
					(ui.hasCompFilter()? " and comp_code = " + dbm.param(compCode, DataType.TEXT):"") + 
					" and " + ui.getPrimaryKey() + " = " + dbm.param(docCode, DataType.TEXT) + 
					" and version = " + dbm.param(version, DataType.INT);
			
			//insert into data table
			JSONObject trxRecord = dbm.selectRecord(sql, conn);
			trxRecord.put("UPD_BY", userCode);
			trxRecord.put("UPD_DATE", "sysdate");
			trxRecord.put("CREATE_BY", userCode);
			trxRecord.put("CREATE_DATE", "sysdate");
			trxRecord.put("LAUNCH_STATUS", "P");
			
			for(String key: modifyParam.keySet()){
				trxRecord.put(key, modifyParam.get(key));
			}
			
			dbm.insertRecord(ui.getDataTable(), trxRecord, conn);
			
			//clear db manager
			dbm.clear();
			dbm.clearColList();

			
			
			if(isNew){
				conn.commit();
			}
			return true;
		}catch(Exception e){
			conn.rollback();
			Log.error("newVersions error: " + e.getMessage());	
		}finally{
			dbm = null;
			if(isNew){
				conn.close();
				conn = null;	
			}
		}
		return false;
	}
	
	public boolean addNameFromName(UserPrincipalBean principal, Connection conn, String module, String docCode, int version, JSONObject modifyParam) throws SQLException{
		boolean isNew = false;
		DBManager dbm = new DBManager();
		boolean success = true;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
			}
			String compCode = principal.getCompCode();
			DynUI ui = getDynUI(module, conn);
			
			String sql = "select * from dyn_name_mst" + 
					" where 1=1 " +
					(ui.hasCompFilter()? " and comp_code = " + dbm.param(compCode, DataType.TEXT):"") +
					" and doc_code = " + dbm.param(docCode, DataType.TEXT) +
					" and version = " + dbm.param(version, DataType.INT);
			
			//insert into data table
			JSONObject nameRecord = dbm.selectRecord(sql, conn);
			
			for(String key: modifyParam.keySet()){
				nameRecord.put(key, modifyParam.get(key));
			}
			
			dbm.insertRecord("dyn_name_mst", nameRecord, conn);
			
			//clear db manager
			dbm.clear();
			dbm.clearColList();
	
			if(isNew){
				conn.commit();
			}
		}catch(Exception e){
			conn.rollback();
			Log.error("addNameFromName error: " + e.getMessage());
			success = false;
		}finally{
			dbm = null;
			if(isNew){
				conn.close();
				conn = null;	
			}
		}
		return success;
	}
	
	public boolean addSectionFromSection(UserPrincipalBean principal, Connection conn, String module, String docCode, int version, JSONObject modifyParam) throws SQLException{
		boolean isNew = false;
		DBManager dbm = new DBManager();
		boolean success = true;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
			}
			
			String compCode = principal.getCompCode();
			DynUI ui = getDynUI(module, conn);
			
			String sql = "select * from dyn_section" + 
					" where 1=1 " +
					(ui.hasCompFilter()? " and comp_code = " + dbm.param(compCode, DataType.TEXT):"") +
					" and doc_code = " + dbm.param(docCode, DataType.TEXT) +
					" and version = " + dbm.param(version, DataType.INT);
			
			//insert into data table
			JSONArray sectionRecords = dbm.selectRecords(sql, conn);
			
			for(int i=0; i<sectionRecords.length(); i++){
				JSONObject sectionRecord = sectionRecords.getJSONObject(i);
				for(String key: modifyParam.keySet()){
					sectionRecord.put(key, modifyParam.get(key));
					if(key.equals(ui.getPrimaryKey().toUpperCase()) &&
							sectionRecord.getString("SECTION_CODE").toLowerCase().equals(ui.getTrxSectionID().toLowerCase())){
						JSONObject jsonFile = new JSONObject(sectionRecord.get("JSON_FILE").toString());
						jsonFile.put(ui.getPrimaryID(), modifyParam.get(key));
						sectionRecord.put("JSON_FILE", jsonFile.toString());
					}
				}
				dbm.insertRecord("dyn_section", sectionRecord, conn);
			}
			
			//clear db manager
			dbm.clear();
			dbm.clearColList();

			if(isNew){
				conn.commit();
			}
		}catch(Exception e){
			conn.rollback();
			Log.error("addSectionFromSection error: " + e.getMessage());	
			success = false;
		}finally{
			dbm = null;
			if(isNew){
				conn.close();
				conn = null;	
			}
		}
		return success;
	}
	
	public int getVersion(UserPrincipalBean principal, Connection conn, String docCode, String module ) throws SQLException{
		boolean isNew = false;
		DBManager dbm = null;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
			}
			dbm = new DBManager();
			String compCode = principal.getCompCode();
			String langCode = principal.getLangCode();
			DynTemplate template = new DynTemplate();
			DynUI ui = template.getDynUI(module, conn);
			
			String getVersionSql = "select version from " + ui.getMstView() + " WHERE 1=1 ";
			if(ui.hasCompFilter()){
				getVersionSql += " and comp_code = " + dbm.param(compCode, DataType.TEXT);
			}
			if(ui.hasLangFilter()){
				getVersionSql += " and lang_code = " + dbm.param(langCode, DataType.TEXT);
			}
			getVersionSql += " and " + ui.getPrimaryKey() + " = " + dbm.param(docCode, DataType.TEXT);
			
			return Integer.parseInt(dbm.selectSingle(getVersionSql, "version", conn));
		}catch(Exception e){
			Log.error("newVersion error: " + e.getMessage());	
		}finally{
			if(dbm != null){
				dbm.clear();
			}
			if(isNew){
				conn.close();
			}
		}
		return -1;
	}
	
	public String getNewDocCode(UserPrincipalBean principal, Connection conn, String docCode, String module, int recrusive, String targetCompCode)  throws SQLException{
		String result = docCode + "(" + recrusive + ")";
		boolean isNew = false;
		DBManager dbm;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
			}
			dbm = new DBManager();
			DynTemplate template = new DynTemplate();
			DynUI ui = template.getDynUI(module, conn);
			String primaryKey = ui.getPrimaryKey();
			String sql = "Select count(1) as cnt from " + ui.getDataTable() + 
					" where comp_code = " + dbm.param(targetCompCode, DataType.TEXT) + 
					" and " + primaryKey + " = " + dbm.param(result, DataType.TEXT);
			int count = Integer.parseInt(dbm.selectSingle(sql, "cnt"));
			if(count > 0){
				return getNewDocCode(principal, conn, docCode, module, recrusive + 1, targetCompCode);
			}
			return result;
			
		}catch(Exception e){
			Log.error("saveProdSection error: " + e.getMessage());
		}finally{
			dbm = null;
			if(isNew){
				conn.close();
				conn = null;		
			}
		}
		return result;
	}
	
	
	
	public boolean updateTrxDocCode(Connection conn, DynUI ui,String compCode, String docCode, String newDocCode, int version){
		boolean result = false;
		DBManager dbm = null;
		try{
			dbm = new DBManager();
			String primaryKey = ui.getPrimaryKey();
			String sql = "update " + ui.getDataTable() 
					+ " set "
					+ " " + primaryKey + " = " + dbm.param(newDocCode, DataType.TEXT)
					+ " where "
					+ " comp_code = " + dbm.param(compCode, DataType.TEXT) 
					+ " and version = " + dbm.param(version, DataType.INT)
					+ " and " + primaryKey + " = " + dbm.param(docCode, DataType.TEXT);
			
			result = dbm.update(sql, conn) != -1;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(dbm != null){
				dbm.clear();
			}
		}
		return result;
	}
	public JSONObject getDynTrxData(UserPrincipalBean principal, Connection conn, String docCode, int version, String module){
		ResultSet rs = null;
		JSONObject result = null;
		boolean isNew = false;
		DBManager dbm;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
			}
			dbm = new DBManager();
			String compCode = principal.getCompCode();
			DynTemplate template = new DynTemplate();
			DynUI ui = template.getDynUI(module, conn);
			String primaryKey = ui.getPrimaryKey();
			String sql = "select * from " + ui.getDataTable() + 
					" where comp_code = " + dbm.param(compCode, DataType.TEXT) + 
					" and version = " + dbm.param(version, DataType.INT)  +
					" and " + primaryKey + " = " + dbm.param(docCode, DataType.TEXT);
			rs = dbm.select(sql, conn);
			if(rs!=null && rs.next()){
				result = new JSONObject();
				result.put("compCode", compCode);
				result.put(primaryKey, docCode);
				result.put("modifyDate", rs.getDate("upd_date"));
				result.put("modifyBy", rs.getString("upd_by"));
				result.put("createDate", rs.getDate("create_date"));
				result.put("createBy", rs.getString("create_by"));
				result.put("baseVersion", rs.getInt("base_version"));
				result.put("templateCode", rs.getString("template_code"));
				List<DynField> trxFields = getTrxFields(principal, module);
				for(DynField field: trxFields){
					String type = field.getType();
					String fid = field.getId();
					String fkey = field.getKey();
					if("date".equals(type)){
						result.put(fid, rs.getDate(fkey));
					}else if("number".equals(type)){
						result.put(fid, rs.getInt(fkey));
					}else{
						result.put(fid, rs.getString(fkey));
					}
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			Log.error("saveProdSection error: " + e.getMessage());
		}finally{
			dbm = null;
			if(isNew){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				conn = null;		
			}
		}
		return result;
		
	}
	
	public Map<String, JSONObject> getSections(Connection conn, UserPrincipalBean principal, String module, String docCode,  int version, String sectionId) throws SQLException {
		DBManager dbm;
		Map<String, JSONObject> sections = null;
		ResultSet rs = null;
		String compCode = principal.getCompCode();
		boolean isNew = false;
		try {
			if (docCode != null && !docCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				if(conn == null){
					isNew = true;
					conn = DBAccess.getConnection();
				}
				
				String sql = " select section_code,"
						   + " json_file"
						   + " from dyn_section "
						   + " where doc_code = " + dbm.param(docCode, DataType.TEXT)
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " and version = " + dbm.param(version, DataType.INT)
						   + " and module = " + dbm.param(module, DataType.TEXT)
						   + " and status = 'A'";
		
				if (sectionId != null && !sectionId.isEmpty())
					sql += " and section_code = " + dbm.param(sectionId, DataType.TEXT);
				
				rs = dbm.select(sql, conn);
				dbm.clear();
				
				if (rs != null) {
					sections = new HashMap<String, JSONObject>();
				
					while (rs.next()) {
						sections.put(rs.getString("section_code"), new JSONObject(rs.getString("json_file")));
					}
				}

			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			if(isNew && conn != null && !conn.isClosed()){
				conn.close();
			}
		}
		return sections;
	}
	
	public boolean updateSectionDocCode(Connection conn, UserPrincipalBean principal, String docCode, String newDocCode, int version, String module) throws SQLException{
		DBManager dbm;
		boolean success = false;
		try{
			dbm = new DBManager();
			
			String userCode = principal.getUserCode();
			String compCode = principal.getCompCode();
			String sql = "UPDATE DYN_SECTION"
			    	+ " SET doc_code = " + dbm.param(newDocCode, DataType.TEXT) + ","
			    	+ " upd_date = CURRENT_TIMESTAMP" + ","
			    	+ " upd_by = " + dbm.param(userCode, DataType.TEXT)
			    	+ " WHERE comp_code = " + dbm.param(compCode, DataType.TEXT)
			    	+ " and doc_code = " + dbm.param(docCode, DataType.TEXT)
			    	+ " and version = " + dbm.param(version, DataType.INT)
					+ " and module = " + dbm.param(module, DataType.TEXT);
			success = dbm.update(sql, conn) != -1;
			
		}catch(Exception e){
			Log.error("updateSectioDocCode error: " + e.getMessage());
		}finally{
			dbm = null;
		}
		return success;
	}
	
	public boolean saveSection(UserPrincipalBean principal, Connection conn, JSONObject json, String docCode, String sectionCode, int version, String module) throws SQLException{
		//Log.error("saveProdSection: " + json);
		ResultSet rs = null;
		boolean isNew = false;
		DBManager dbm;
		boolean success = false;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
			}
			dbm = new DBManager();
			String userCode = principal.getUserCode();
			String compCode = principal.getCompCode();
			String checkSql = "select json_file from DYN_SECTION "
								+ " WHERE comp_code = " + dbm.param(compCode, DataType.TEXT)
						    	+ " and doc_code = " + dbm.param(docCode, DataType.TEXT)
						    	+ " and section_code = " + dbm.param(sectionCode, DataType.TEXT)
						    	+ " and version = " + dbm.param(version, DataType.INT)
								+ " and module = " + dbm.param(module, DataType.TEXT);
			rs = dbm.select(checkSql, conn);
			if(rs.next()){
				dbm.clear();
				String sql = "UPDATE DYN_SECTION"
				    	+ " SET json_file = " + dbm.param(json.toString(), DataType.TEXT)   
				    	+ " , upd_date = CURRENT_TIMESTAMP"
				    	+ " , upd_by = " + dbm.param(userCode, DataType.TEXT)
				    	+ " WHERE comp_code = " + dbm.param(compCode, DataType.TEXT)
				    	+ " and doc_code = " + dbm.param(docCode, DataType.TEXT)
				    	+ " and section_code = " + dbm.param(sectionCode, DataType.TEXT)
				    	+ " and version = " + dbm.param(version, DataType.INT)
						+ " and module = " + dbm.param(module, DataType.TEXT);
				success = dbm.update(sql, conn) != -1;
			}else{
				dbm.clear();
				String sqlInsert = " INSERT INTO DYN_SECTION (comp_code, doc_code, version, section_code, json_file, create_by, upd_by, module)"
						    		+ " VALUES (" 
						    		+ dbm.param(compCode, DataType.TEXT)  + ","
						    		+ dbm.param(docCode, DataType.TEXT) + ","
						    		+ dbm.param(version, DataType.INT) + ","
						    		+ dbm.param(sectionCode, DataType.TEXT) + ","
						    		+ dbm.param(json.toString(), DataType.TEXT) + ","
						    		+ dbm.param(userCode, DataType.TEXT) + ","
						    		+ dbm.param(userCode, DataType.TEXT) + ","
									+ dbm.param(module, DataType.TEXT) + ")";
				success = dbm.insert(sqlInsert, conn);
			}
			
		}catch(Exception e){
			Log.error("saveProdSection error: " + e.getMessage());
		}finally{
			dbm = null;
			if(isNew){
				conn.close();	
			}
		}
		return success;
	}
	
	public boolean updateJsonDocCode(Connection conn, UserPrincipalBean principal, String newDocCode, int taskId) throws SQLException{
		DBManager dbm = null;
		
		try{
			dbm = new DBManager();
	
			String userCode = principal.getUserCode();
			String sql = "update dyn_json_mst"
			    	+ " set doc_code = " + dbm.param(newDocCode, DataType.TEXT) + ","
			    	+ " upd_date = CURRENT_TIMESTAMP" + ","
			    	+ " upd_by = " + dbm.param(userCode, DataType.TEXT)
			    	+ " where task_code = " + dbm.param(taskId, DataType.INT);
			return dbm.update(sql, conn) != -1;
			
		}catch(Exception e){
			Log.error("updateJsonDocCode error: " + e.getMessage());
		}finally{
			if(dbm != null){
				dbm.clear();
			}
		}
		return false;
	}
	
	/*
	parentCode: doc where rate json related
	*/
	public boolean saveJsonFile(UserPrincipalBean principal, Connection conn, JSONObject json, String module, String parentCode, String docCode, int version, int taskId)throws SQLException{
		boolean isNew = false;
		DBManager dbm;
		String table = "dyn_json_mst";
		boolean result = true;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
			}
			dbm = new DBManager();
			String userCode = principal.getUserCode();
			String compCode = principal.getCompCode(); 
			 
			HashMap<String,Object> condMap = new HashMap<String,Object>();
			condMap.put("comp_code", principal.getCompCode());
			condMap.put("version", version);
			condMap.put("module", module);
			condMap.put("doc_code", docCode);
			condMap.put("parent_code", parentCode);
			boolean isExist = Function2.hasRecordByType(table, condMap, null, conn);

			if(isExist){
				String sql = "update " + table + " set "
							+ " json_file = " + dbm.param(json.toString(), DataType.TEXT) + ","
							+ " upd_by = " + dbm.param(userCode, DataType.TEXT) + ", "
							+ "	upd_date = CURRENT_TIMESTAMP"
							+ " where comp_code = "  + dbm.param(compCode, DataType.TEXT)
							+ " and doc_code = " +  dbm.param(docCode, DataType.TEXT)
							+ " and version = " + dbm.param(version, DataType.INT)
							+ " and module = " + dbm.param(module, DataType.TEXT)
							+ " and parent_code = " + dbm.param(parentCode, DataType.TEXT);
				
				result = (dbm.update(sql, conn) != -1);
			}else{
				String sqlInsert = "insert into " + table
									+" (task_code, comp_code, module, parent_code, doc_code, version, json_file, upd_by, create_by, upd_date, create_date)"
									+" values ("
									+ dbm.param(taskId, DataType.INT) + ","
									+ dbm.param(compCode, DataType.TEXT) + ","
									+ dbm.param(module, DataType.TEXT) + ","
									+ dbm.param(parentCode, DataType.TEXT) + ","
									+ dbm.param(docCode, DataType.TEXT) + ","
									+ dbm.param(version, DataType.INT) + ","
									+ dbm.param(json.toString(), DataType.TEXT) + ","
									+ dbm.param(userCode, DataType.TEXT) + ","
									+ dbm.param(userCode, DataType.TEXT) + ","
									+ "CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
				result = dbm.insert(sqlInsert, conn);
			}
			
		}catch(Exception e){
			Log.error("saveJson error: " + e.getMessage());	
			result = false;
		}finally{
			dbm = null;
			if(isNew){
				conn.close();
				conn = null;		
			}
		}
		return result;
	}
	
	public boolean checkExist(String docCode, String compCode, int version, String module) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		
		boolean found = false;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			DynTemplate template = new DynTemplate();
			DynUI ui = template.getDynUI(module, conn);
			
			String sql = " select count(1) as cnt"
					   + " from " + ui.getDataTable()
					   + " where comp_code = " + dbm.param(compCode, DataType.TEXT) 
					   + " and " + ui.getPrimaryKey() + " = " + dbm.param(docCode, DataType.TEXT);
			
			if (version > 0 )
				sql += " and version = " + dbm.param(version, DataType.INT);
	
			String cnt = dbm.selectSingle(sql, "cnt", conn);
	
			if (Integer.parseInt(cnt) > 0)
				found = true;
		} catch (Exception e) {
			Log.error("Products > checkExist" + e);
		} finally {
			dbm = null;	
			if (conn != null && !conn.isClosed())
				conn.close();
		}

		return found;
	}
	
	public boolean updateLaunchStatus(Map<String,Object> release) throws SQLException{
		boolean success = false;
		DBManager dbm;
		Connection conn = null;

		try {
			String docCode = release.get("ITEM_CODE").toString();
			int version = Integer.parseInt(release.get("VER_NO").toString());
			String compCode = release.get("COMP_CODE").toString();
			String  module = getModuleByFuncCode(null, release.get("FUNC_CODE").toString());
			DynUI dynUi = getDynUI(module, conn);
			

			dbm = new DBManager();
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);

			if (docCode != null && !docCode.isEmpty() && compCode != null && !compCode.isEmpty() && version >0) {
				String sql = " update " + dynUi.getDataTable() + " set"
						   + " launch_status = 'L'"
						   + " where " + dynUi.getPrimaryKey() + " = " + dbm.param(docCode, DataType.TEXT)
						   + " and version = " + dbm.param(version, DataType.INT)
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT);
				
				if (dbm.update(sql) > 0) {
					conn.commit();
					success = true;
				}
				
				conn.rollback();
			}
		} catch (Exception e) {
			conn.rollback();
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}

		return success;
	}
	
	public int getNextVersion(UserPrincipalBean principal, Connection conn, String docCode, String module) throws SQLException{
		
		boolean isNew = false;
		DBManager dbm;
		int result = 1;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
			}
			dbm = new DBManager();
			DynUI ui = getDynUI(module, conn);
			String compCode = principal.getCompCode();
			
			String sql = "select max(version)+1 as version from " + ui.getDataTable() + 
					" where comp_code = " + dbm.param(compCode, DataType.TEXT) +
					" and " + ui.getPrimaryKey() + " = " + dbm.param(docCode, DataType.TEXT); 
			result = Integer.parseInt(dbm.selectSingle(sql, "version"));
		}catch(Exception e){
			Log.error("newVersions error: " + e.getMessage());	
		}finally{
			if(isNew){
				conn.close();
				conn = null;	
			}
		}
		return result;
	}
	
	public boolean suspend(UserPrincipalBean principal, Connection conn, JSONArray docCodes, String module) throws SQLException{
		boolean isNew = false;
		boolean result = false;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
			}
			for(int i=0; i<docCodes.length(); i++){
				String docCode = docCodes.getString(i);
				if(suspend(principal, conn, docCode, module) < -1){
					result = false;
					break;
				}
			}
			if(isNew){
				conn.commit();
			}
			result = true;
		}catch(Exception e){
			conn.rollback();
			Log.error("newVersions error: " + e.getMessage());	
		}finally{
			if(isNew){
				conn.close();
				conn = null;	
			}
		}
		return result;
	}
	
	public int suspend(UserPrincipalBean principal, Connection conn, String docCode, String module) throws SQLException{
		boolean isNew = false;
		DBManager dbm;
		int result = -1;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
				conn.setAutoCommit(false);
			}
			dbm = new DBManager();
			String compCode = principal.getCompCode();
			String userCode = principal.getUserCode();
			int version = getVersion(principal, conn, docCode, module);
			int newVersion = getNextVersion(principal, conn, docCode, module);
			DynUI ui = getDynUI(module, conn);
			
			//create new task
			String sql = "insert into task_mst (comp_code, func_code, item_code, ver_no, task_desc, eff_date, ref_no, status, create_date, create_by, upd_date, upd_by, channel_code) " +
					"select " + dbm.param(compCode, DataType.TEXT) + ", func_code, item_code, " + dbm.param(newVersion, DataType.INT) + ", task_desc, eff_date, ref_no, 'A', sysdate, " + dbm.param(userCode, DataType.TEXT) + ", sysdate, " + 
					dbm.param(userCode, DataType.TEXT) + ", channel_code from task_mst where comp_code = " + dbm.param(compCode, DataType.TEXT) + 
					" and item_code = " + dbm.param(docCode, DataType.TEXT) + " and ver_no = " + dbm.param(version, DataType.INT) + " and func_code = " + dbm.param(getFuncCodeByModule(conn, module), DataType.TEXT);
	
			dbm.select(sql, conn);
			dbm.clear();
			
			//colNames
			String colNames = " (COMP_CODE, VERSION " +
					", BASE_VERSION, LAUNCH_STATUS, STATUS, CREATE_DATE, CREATE_BY, UPD_DATE, UPD_BY, TEMPLATE_CODE";
			String colValues = "(" + dbm.param(compCode, DataType.TEXT) + " , " + 
					dbm.param(newVersion, DataType.INT) + "," +
					dbm.param(version, DataType.INT) + "," +
					"'P', 'U', sysdate," + dbm.param(userCode,  DataType.TEXT) + ", sysdate," + 
					dbm.param(userCode,  DataType.TEXT)  + " 'NEW',";
			
			List<DynField> trxFields = getTrxFields(principal, module);
			JSONObject data = getDynTrxData(principal, conn, docCode, version, module);
			String colsName = "";
			for(DynField field: trxFields){
				colNames += ", " + field.getKey();
				colValues += ",";
				String type = field.getType();
				colValues+=dbm.param(data.get(field.getId()), 
						"date".equals(type)?DataType.DATE:
							"number".equals(type)?DataType.INT:DataType.TEXT);
			}
			colNames += ")";
			colValues +=")";
			sql = "insert into " + ui.getDataTable() + colNames + " VALUES " + colValues;		
			
			
			//insert into trx table
			dbm.select(sql, conn);
			dbm.clear();
			
			if(isNew){
				conn.commit();
			}
			result = -1;
		}catch(Exception e){
			conn.rollback();
			Log.error("suspend error: " + e.getMessage());	
		}finally{
			if(isNew){
				conn.close();
				conn = null;	
			}
		}
		return result;
	}

}
