package com.eab.dao.dynTemplate;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Template;
import com.eab.model.dynTemplate.DynField;
import com.eab.model.dynTemplate.DynUI;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;

public class DynLayout extends DynTemplate{
	public final String RETURN_TEMPLATE = "RETURN_TEMPLATE";
	public final String RETURN_JSON = "RETURN_JSON";
	
	/*
	 * 
	 * get options from mst table for template 
	*/
	public JSONArray getMstOptions(JSONObject template, UserPrincipalBean principal){
		
		String key = null, value = null;
		if(template.has("lookupField")){
			key = template.getString("lookupField");
		}
		if(template.has("lookupValue")){
			value = template.getString("lookupValue");
		}
		String table = template.getString("optionTable");
		DynUI ui = new DynUI();
		
		DynField field = new DynField();
		
		DynUI thisUI = getDynUIByTable(table, null);
		if(thisUI.hasCompFilter()){
			field.setCompFilter("Y");
		}else{
			field.setCompFilter("N");
		}
		
		if(thisUI.hasLangFilter()){
			field.setLangFilter("Y");
		}else{
			field.setLangFilter("N");
		}
		
		field.setLookUpField(key);
		field.setValue(value);
		//getOptionsLookUp
		field.setOptionsLookUp(table);
		String present = template.getString("presentation");
		field.setPresent(present);
		
		JSONArray list = new JSONArray();
		Connection conn = null;
		try{
			conn = DBAccess.getConnection();
			Map<String, String> map = getLookUpList(ui, field, conn, principal, null);
			for (Map.Entry<String, String> option : map.entrySet())
			{
				JSONObject obj = new JSONObject();
				obj.put("value", option.getKey());
				obj.put("title", option.getValue());
				list.put(obj);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return list;
	}
	
	/*
	 * get options from look up table for template
	*/
	public JSONArray getLookUpOptions(JSONObject template, UserPrincipalBean principal){
		String key = template.getString("subtype");
		DynUI ui = new DynUI();
		ui.setCompFilter("Y");
		ui.setLangFilter("Y");
		DynField field = new DynField();
		field.setLookUpField(key);
		//getOptionsLookUp
		field.setOptionsLookUp("SYS_LOOKUP_MST");
		JSONArray list = new JSONArray();
		Connection conn = null;
		try{
			conn = DBAccess.getConnection();
			Map<String, String> map = getLookUpList(ui, field, conn, principal, null);
			for (Map.Entry<String, String> option : map.entrySet())
			{
				JSONObject obj = new JSONObject();
				obj.put("value", option.getKey());
				obj.put("title", option.getValue());
				list.put(obj);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return list;
		
	}
	
	public JSONObject getTemplateJson(String module, UserPrincipalBean principal, String templateCode) throws SQLException{
		JSONObject temp = (JSONObject)getTemplate(module, principal, templateCode, RETURN_JSON);
		return temp;
	}
	
	public String getTaskInfoSection(Connection conn, String templateCode) throws SQLException{
		DBManager dbm;
		String taskSection = null;
		try {
			dbm = new DBManager();
			//get task section
			String sql = "select template_file "
					+ " from prod_template "
					+ " where type='TASK'"
					+ " and comp_code = '*' "
					+ " and template_code = " + dbm.param(templateCode, DataType.TEXT)
					+ " and version = (select max(version) from prod_template "
					+ "    where type = 'TASK'"
					+ "    and comp_code = '*'"
					+ "    and template_code = " + dbm.param(templateCode, DataType.TEXT)
					+ " )";
			
			taskSection = dbm.selectSingle(sql, "template_file", conn);
			return taskSection;
			
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbm = null;
		}
		return taskSection;
	}
	
	public Object getTemplate(String type, UserPrincipalBean principal, String templateCode, String returnType) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			//get other sections		
			String sql = " select template_file"
					   + " from prod_template"
					   + " where type = " + dbm.param(type, DataType.TEXT)
					   + " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					   + " and template_code = " + dbm.param(templateCode, DataType.TEXT)
					   + " and version = (select version from prod_template " 
					   + " where type = " + dbm.param(type, DataType.TEXT)
					   + " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					   + " and template_code = " + dbm.param(templateCode, DataType.TEXT)
					   + ")";
				
			String jsonFile = dbm.selectSingle(sql, "template_file", conn);
			
			//return template
				if(RETURN_TEMPLATE.equalsIgnoreCase(returnType)){
					Gson gson = new Gson();
					Template templ = gson.fromJson(jsonFile, Template.class);
					/*
					 * move all logic stuff to biz class
					List<Field> sections = templ.getItems();
					sections.add(0, gson.fromJson(taskSection, Field.class));
					templ.setItems(sections);
					*/
					return templ;
				}else{
					JSONObject json = new JSONObject(jsonFile);
					return json;
				}
			
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Products > get product template" + e);
		} finally {
			dbm = null;

			if (conn != null && !conn.isClosed())
				conn.close();

			conn = null;
		}
		return null;
	}
	
	public JSONObject getFunctionData() {
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;

		JSONObject data = new JSONObject();
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select template_code, template_file"
					   + " from prod_template"
					   + " where type = 'SYS_FUNC'";
				
			rs = dbm.select(sql, conn);
			
			if(rs != null){
				while (rs.next()){
					String code = rs.getString("template_code");
					String json = rs.getString("template_file");
					Log.info("getting:"+code);
					if ("API".equals(code)) {
						if (!json.isEmpty()) {
							JSONObject doc = new JSONObject(json);
							JSONArray api = doc.getJSONArray("api");
							data.put("functions", api);
						}
					} else if ("DefParams".equals(code)) {
						data.put("defParams", new JSONObject(json));						
					} else if ("DataDict".equals(code)) {
						data.put("dataDict", new JSONObject(json));																		
					}
				}
			}
		} catch (Exception e) {
			Log.error("function data > exception: " + e);
		} finally {
			dbm = null;
			rs = null;
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException ex) {
				
			}

			conn = null;
		}
		return data;
	}
}
