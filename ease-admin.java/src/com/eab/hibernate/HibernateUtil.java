package com.eab.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.eab.common.Log;

public class HibernateUtil {
	private static SessionFactory sessionFactory = buildSessionFactory();
	
//	public HibernateUtil() {
//		sessionFactory = buildSessionFactory();
//	}
	
	private static SessionFactory buildSessionFactory() {
		SessionFactory sf = null;
		
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			sf = new Configuration().configure().buildSessionFactory();
		} catch(Exception e) {
			Log.error(e);
		}
		
		return sf;
	}
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	public static void shutdown() {
		// Close caches and connection pools
		getSessionFactory().close();
	}
}
