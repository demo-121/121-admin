package com.eab.webservice.dynamic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;
import com.eab.biz.dynamic.DynamicManager;
import com.eab.biz.dynamic.LockManager;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.dynamic.DynamicDAO;
import com.eab.dao.dynamic.RecordLock;
import com.eab.dao.dynamic.TemplateDAO;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.json.model.AppBar;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.dynamic.RecordLockBean;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.JsonObject;

public class DynamicViewController {
	static final String details = "details";
	public static String path = "/Dynamic/Details";

	public Response getDetails(HttpServletRequest request, HttpServletResponse response) {
		String module = request.getParameter("p1");
		String id = request.getParameter("p0");
		DynamicManager dm = null;
		DynamicUI ui = null;
		JSONObject template = null;
		HttpSession session = request.getSession();
		DBManager dbm = null;
		Connection conn = null;
		Response resp = null;
		DynamicDAO dynamic = new DynamicDAO();
		try {
			UserPrincipalBean principal = Function.getPrincipal(request);
 
			conn = DBAccess.getConnection();
			dbm = new DBManager();
			String templateType = null;
			JSONObject values = null, changedValues = null;
			JsonObject valuesObject = null, changedValuesObject = null;
			JSONObject newTemplate = null;
			
			dm = new DynamicManager(request);
			
			ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !ui.getModule().equals(module)) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}
			
			HashMap<String,String> condMap = new HashMap<String,String>();
			condMap.put(ui.getPrimaryKey(), id);
			
			if(ui.getCompanyFilter() && !module.equals("company")){
				condMap.put("comp_code", principal.getCompCode());
			}
			
			String action = dynamic.getTempTableAction(dbm, ui.getTempTable(), condMap);
			
			if (action == null)
				action = details;
			
			if(action.equals(details)){
				resp = dm.getDetails(request, path);
			}else{
				Template templateOb = null; 
				JSONObject appBarOb = null;
				// get dynamic template
				try {
					template = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) != null ? (JSONObject) session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) : null;
				} catch (Exception ex) {
					template = null;
				}
				if (template == null || !module.equals(template.getString("id"))) {
					TemplateDAO templateDao = new TemplateDAO();
					template = templateDao.getTemplate(module, ui, conn);
					session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, template);
				}
				
				//get new template
				newTemplate = DynamicFunction.getDetailTemplate(template);
				newTemplate.put("action", action);
				
				//values
				values = dm.getMstValues(dbm, conn, ui, template, id);
				changedValues = dm.getTmpValues(dbm, conn, ui, newTemplate, id);
				if(values == null)
					values = new JSONObject(changedValues.toString());
				
				values.put("id", id);
				values.put("lang", principal.getLangCode());
				changedValues.put("id", id);
				changedValues.put("lang", principal.getLangCode());
	
				valuesObject = DynamicFunction.convertJsonToGson(values);
				changedValuesObject = DynamicFunction.convertJsonToGson(changedValues);
				
				switch (action) {
				case "N":
					//add record lock
					RecordLock rl = new RecordLock();
					RecordLockBean rlb = rl.newRecordLockBean(module, id);
					rlb = rl.getRecordLock(request, rlb);
					templateType = "add";
										
					if(rlb.canAccess()){
						rl.addRecordLock(request, rlb);
						//get app bar 
						if (dm.hasApproveRight(ui.getTempTable(), ui.getPrimaryKey(), id)) {
							appBarOb = dynamic.genAppBarByViewType(conn, ui, principal, "approve", null);
							//add actions
							JSONArray actions = dynamic.getActionsByType(conn, ui, "edit");
							JSONArray actionsArray = appBarOb.getJSONArray("actions");
							actionsArray.put(actions);
							appBarOb.put("actions", actionsArray);
																								
						} else {
							appBarOb = dynamic.genAppBarByViewType(conn, ui, principal, "edit", null);
							
						}

					}else{
						resp = dm.getDynTable();
						LockManager lm = new LockManager(request);
						resp.setDialog(lm.lockDialog(rlb));
						return resp;
					}
	
					break;
					
				case "E":
					RecordLock rlE = new RecordLock();
					RecordLockBean rlbE = rlE.newRecordLockBean(module, id);
					rlbE =  rlE.getRecordLock(request, rlbE);
					templateType = "edit";
					if(rlbE.canAccess()){						
						if(!rlbE.isCurrentUserAccess())
							rlE.addRecordLock(request, rlbE);
						
						//get app bar 
						if (dm.hasApproveRight(ui.getTempTable(), ui.getPrimaryKey(), id)) {							
							appBarOb = dynamic.genAppBarByViewType(conn, ui, principal, "approve", null);
							//add actions
							JSONArray actions = dynamic.getActionsByType(conn, ui, "edit");
							JSONArray actionsArray = appBarOb.getJSONArray("actions");
							actionsArray.put(actions);
							appBarOb.put("actions", actionsArray);
						} else {							
							appBarOb = dynamic.genAppBarByViewType(conn, ui, principal, "edit", null);
						}
						
						
					}else{		
						resp = dm.getDetails(request, path);
						LockManager lm = new LockManager(request);
						resp.setDialog(lm.lockDialog(rlbE));
						return resp;
					}
					break;
				case "D":
					templateType = "delete";
					RecordLock rlD = new RecordLock();
					RecordLockBean rlbD = rlD.newRecordLockBean(module, id);
					rlbD = rlD.getRecordLock(request, rlbD);
					if(rlbD.canAccess()){
						if(!rlbD.isCurrentUserAccess())
							rlD.addRecordLock(request, rlbD);
						// Edit record, get data from tmp and mst table, then edit
						JSONObject deleteTemplate = new JSONObject();
						JSONArray items = new JSONArray();
						JSONObject item = new JSONObject();
					    item.put("detailSeq", "1");
					    item.put("title", "removed");
					    item.put("id", "delete");
					    item.put("type", "readyOnly");
					    item.put("allowUpdate", "N");
					    items.put(item);
					    deleteTemplate.put("items", items);
					    deleteTemplate.put("title", "edit_"+module);
					    JSONObject deleteValue = new JSONObject();
					    deleteValue.put("delete", "Removed");
					    deleteTemplate.put("values", deleteValue);
					    newTemplate.put("delete", deleteTemplate);
					    newTemplate.put("type", "delete");
					    
					    //get app bar
						if (dm.hasApproveRight(ui.getTempTable(), ui.getPrimaryKey(), id)) {
							appBarOb = dynamic.genAppBarByViewType(conn, ui, principal, "approve", null);
						} else {
							appBarOb = dynamic.genAppBarByViewType(conn, ui, principal, "delete", null);
						}
					}else{
						resp = dm.getDetails(request, "details");
						LockManager lm = new LockManager(request);
						resp.setDialog(lm.lockDialog(rlbD));
						return resp;
					}
					break;
	
				}
				
				//define title and type
				newTemplate.put("title", (ui.getModule() + ".EDIT.TITLE").toUpperCase());
				newTemplate.put("type", templateType);
				
				 templateOb = DynamicFunction.getTemplateFromJson(request,newTemplate);
					// refine fields 
					for (Field item : templateOb.getItems()) {
						if (("add".equals(templateOb.getType()) || "edit".equals(templateOb.getType())) && !item.isAllowUpdate()) {
							item.setDisabled(true);
						}
					}
				templateOb.setType(templateType);
				
				//appbar
				AppBar appBar = DynamicFunction.getAppBarFromJson(appBarOb);
				appBar.getTitle().setId("/Dynamic");
				if(values.has("primaryName"))
					appBar.getTitle().setSecondary(values.getString("primaryName"));
				
				if(action.equals("N")){
					appBar.getTitle().setSecondary((module + ".NEW.MASTER.TITLE").toUpperCase());
				}
				//translate appbar
				try{
					List<String> barNameList = new ArrayList<String>();
					appBar.getNameCodes(barNameList);
					Map<String, String> lmap = Function2.getTranslateMap(request, barNameList);
					appBar.translate(lmap);
				}catch(Exception e){
					Log.error(e);
				}
				
				//content
				Content content = new Content(templateOb, valuesObject, changedValuesObject);
				
				//page 
				Page pageObject = new Page(path, ui.getModule());
				
				// Token
				String tokenID = Token.CsrfID(request.getSession());
				String tokenKey = Token.CsrfToken(request.getSession());
				//resp		
				resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);	
			}

		} catch (SQLException e) {
			Log.error(e);
			dm = new DynamicManager(request);
			resp = dm.getDynTable();		
			Field bt = new Field("btOk", "button", "OK");
			Dialog dialog = new Dialog("dialogError", "Notice", "Record Error", null, bt);
			resp.setDialog(dialog);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				Log.error(e);
			}
			conn = null;
			dbm = null;
		}
		return resp;

	}


}
