package com.eab.webservice.dynamic;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.dynamic.DynamicDAO;
import com.eab.dao.dynamic.TemplateDAO;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.AppBar;
import com.eab.json.model.Content;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class DynTableController {

	public void tableView(HttpServletRequest request, HttpServletResponse response) {
		Response resp = null;
		resp = getDynTable(request);
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.setDateHeader("Expires", 0);
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		PrintWriter out =  null;
		try {
			out = response.getWriter();
			Gson gson = new Gson();
			String ret = gson.toJson(resp); 
			Log.debug("end add request:" + ret);
			out.append(ret);
		} catch (IOException e) {
			Log.error(e);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

	public Response getDynTable(HttpServletRequest request) {
		
		String p0 = request.getParameter("p0");
		String module = request.getParameter("p1");
		Response resp = null;
		DynamicDAO dynamic = new DynamicDAO();
		// get search criteria
		String searchStr = "";
		String statusStr = "";
		String typeStr = "";
		String sortBy = "";
		String sortDir = "A";
		int offset = 0;
		int size = 10;
		JSONObject cond = null;
		boolean isNew = true;

		HttpSession session = request.getSession();
		JSONObject template = null;

		DynamicUI ui = null;
		JSONArray result = null;
		String errorMessage = "";

		DBManager dbm1 = null;
		Connection conn = null;
		Map<String, String> keyMap = new HashMap<String, String>();
		Map<String, String> idMap = new HashMap<String, String>();
		
		UserPrincipalBean principal = Function.getPrincipal(request);

		try {
			Log.debug("gen table View 1:" + p0);
			if (p0 != null) {
				try {
					p0 = new String(Base64.getDecoder().decode(p0), "UTF-8");
					Log.debug("gen table View 2:" + p0);
					cond = new JSONObject(p0);
					searchStr = cond.has("criteria") ? cond.getString("criteria") : "";
					statusStr = cond.has("statusFilter") ? cond.getString("statusFilter") : "";
					typeStr = cond.has("typeFilter") ? cond.getString("typeFilter") : "";
					sortBy = cond.has("sortBy") ? cond.getString("sortBy") : "";
					sortDir = cond.has("sortDir") ? cond.getString("sortDir").toUpperCase() : "A";
					offset = cond.has("recordStart") ? cond.getInt("recordStart") : 0;
					size = cond.has("pageSize") ? cond.getInt("pageSize") : Constants.SEARCH_RESULT_DEFAULT_PAGE_SIZE;
					isNew = offset == 0;
				} catch (Exception e) {
					cond = new JSONObject();
				}

			} else {
				cond = new JSONObject();
			}

			conn = DBAccess.getConnection();

			// get dynamic ui
			ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}

			// get dynamic template
			template = session.getAttribute(Constants.SessionKeys.CURRENT_TABLE_TEMPLATE) != null ? (JSONObject) session.getAttribute(Constants.SessionKeys.CURRENT_TABLE_TEMPLATE) : null;
			if (template == null || !module.equals(template.getString("id"))) {
				TemplateDAO tempalteDao = new TemplateDAO();
				template = tempalteDao.getTemplate(module, ui, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_TABLE_TEMPLATE, template);
			}

			JSONArray templateItems = (JSONArray) template.get("items");

			// create map field id and table field name
			for (Object obj : templateItems) {
				JSONObject item = (JSONObject) obj;
				String id = item.getString("id");
				if (item.has("key")) {
					String key = item.getString("key").toLowerCase();
					keyMap.put(id, key);
					idMap.put(key, id);
				} else {
					keyMap.put(id, id);
					idMap.put(id, id);
				}
			}

			// get default sort by as primarykey and set the id to cond
			if (sortBy.isEmpty()) {
				sortBy = ui.getPrimaryKey();
				// cond.put("sortBy", idMap.get(sortBy));
				if (idMap.containsKey(sortBy)) {
					cond.put("sortBy", idMap.get(sortBy));
				} else {
					cond.put("sortBy", sortBy);
				}
				cond.put("sortDir", sortDir);
			} else {
				// resolute the sort by to db field
				sortBy = keyMap.get(sortBy);
			}

			// get result from session if filterkey has not change
			String filterKey = module + "_" + searchStr + "_" + statusStr + "_" + typeStr + "_" + sortBy + "_" + sortDir;
			String curFilterKey = session.getAttribute(Constants.SessionKeys.FILTER_KEY) != null ? (String) session.getAttribute(Constants.SessionKeys.FILTER_KEY) : null;
			if (!isNew && filterKey.equals(curFilterKey)) {
				result = session.getAttribute(Constants.SessionKeys.SEARCH_RESULT) != null ? (JSONArray) session.getAttribute(Constants.SessionKeys.SEARCH_RESULT) : null;
			}

			// get result again otherwise
			if (result == null && ui != null && template != null) {
				// get template

				dbm1 = new DBManager();

				String selectFieldStr = getSelectField(templateItems, ui.getPrimaryKey());
				// selectFieldStr = ui.getPrimaryKey() + ", " + selectFieldStr;
				String sqlData = "";

				String condition = genKeywordCondition(searchStr, ui, dbm1, "");
				condition = DynamicFunction.buildCondition(condition, genTypeFilterCondition(typeStr, ui, dbm1, ""));
				// condition = DynamicFunction.buildCondition(condition, "a.ROWNUM <= " + Constants.SEARCH_RESULT_LIMIT);

				// filter by status first
//				Log.error("statusStr--------->" + statusStr);
				if (!DynamicFunction.isNotEmptyOrNull(statusStr)) {
					statusStr = "E";
				}

				if (statusStr.equals("All")) {
				} else if (statusStr.equals("E")) {
					condition = DynamicFunction.buildCondition(condition, "(action = 'N' OR status <> 'D')");
				} else if (statusStr.equals("P")) {
					condition = DynamicFunction.buildCondition(condition, "action = 'N'");
				} else { // A and D
					condition = DynamicFunction.buildCondition(condition, "(action <> 'N' OR action is null) AND status = " + dbm1.param(statusStr, DataType.TEXT));
				}
				condition = DynamicFunction.buildCondition(condition, "ROWNUM <= " + Constants.SEARCH_RESULT_LIMIT);

				// add company filter
				// admin can view all records at the same time
				// if (!Function.isCurrentUserAdmin(request)) { // Commented by Him, admin result also need to be filtered by compCode. ref: RDADMIN-129.
				String compCode = principal.getCompCode();
				if ("company".equals(module.toLowerCase()) && principal.isRegional()) {
					// skip company filter for regional user get company master
				} else {
					condition = DynamicFunction.addCompanyFilter(dbm1, condition, compCode);
				}
				
				// add language filter
				if(ui.getLangFilter()){
					condition = DynamicFunction.addLangFilter(dbm1, condition, principal.getLangCode());
				}
				// }
				
				if (ui.getAdditionalFilter() != null) {
					condition = DynamicFunction.buildCondition(condition, ui.getAdditionalFilter());
				}
				
				Log.debug("dyn ---------> condition:" + condition);

				sqlData = "SELECT " + selectFieldStr + " FROM " + ui.getMasterView() + condition + " ORDER BY " + sortBy + " " + ("A".equals(sortDir) ? "ASC" : "DESC");

				// get record list
				ResultSet rs = null;
				result = new JSONArray();
				rs = dbm1.select(sqlData, conn);
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
				String[] colNames = new String[columnCount];
				
				for (int i = 1; i <= columnCount; i++) {
					colNames[i - 1] = rsmd.getColumnName(i);
				}
				
				DBManager dbm = null;
				
				if (rs != null) {
					while (rs.next()) {
						JSONObject dataSet = new JSONObject();
				
						for (int j = 0; j < columnCount; j++) {
							String mapKey = (colNames[j]).toLowerCase();
							String mapValue = rs.getString(colNames[j]);
						
							// create id field
							if (ui.getPrimaryKey().toLowerCase().equals(mapKey)) {
								dataSet.put("id", mapValue);
							}
							
							// add other fields
							mapKey = idMap.get(mapKey);
							
							//Get product line name
							
							try {
								String plCode = rs.getString("pl_code");
								
								if (mapKey.toLowerCase().equals("plname") && module.toLowerCase().equals("prodline") && plCode != null) {
									dbm = new DBManager();
									mapValue = dynamic.getProducLinetName(plCode, principal, dbm, conn);
								}
							} catch (Exception e) {}
							
							if (mapKey != null) 
								dataSet.put(mapKey, mapValue);
						}
						result.put(dataSet);
					}
					rs.close();
				}

				dbm1.clear();

				session.setAttribute(Constants.SessionKeys.FILTER_KEY, filterKey);
				session.setAttribute(Constants.SessionKeys.SEARCH_RESULT, result);
			} else {
				errorMessage = "Fail to get list";
			}
		} catch (Exception e) {
			Log.error(e);
			errorMessage = e.getMessage();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				Log.error(e);
			}
			conn = null;
			dbm1 = null;
		}

		JSONObject ret = new JSONObject();
		if (result != null) {
			JSONObject jsonContent = new JSONObject();
			JSONArray shortedList = new JSONArray();
			String type = "table";
			Log.debug("set result : " + offset + " - " + size);
			for (int j = offset; j < offset + size && j < result.length(); j++) {
				shortedList.put(result.get(j));
			}
			template.put("type", type);

			JSONObject values = new JSONObject();
			values.put("list", shortedList);
			values.put("total", result.length());
			values.put("isMore", offset > 0);

			jsonContent.put("values", values);
			jsonContent.put("template", template);

			ret.put("content", jsonContent);

			if (isNew) {
				ret.put("action", Constants.ActionTypes.CHANGE_PAGE);
			} else {
				ret.put("action", Constants.ActionTypes.CHANGE_CONTENT);
			}

			JsonObject valuesObject = DynamicFunction.convertJsonToGson(values);

			Content content = new Content(DynamicFunction.getTemplateFromJson(request, template), valuesObject, valuesObject);

			Page pageObject = new Page("/Dynamic", ui.getModule(), ui.getModule());

//			Log.error("statusStr--------->" + statusStr);
			if (DynamicFunction.isNotEmptyOrNull(statusStr)) {
				cond.put("statusFilter", statusStr);
			}
			AppBar appBar = DynamicFunction.getAppBarFromJson(dynamic.genAppBarByViewType(conn, ui, principal, type, cond));
			// set dropdown menu value

			// translate appbar
			try {
				List<String> barNameList = new ArrayList<String>();
				appBar.getNameCodes(barNameList);
				Map<String, String> lmap = Function2.getTranslateMap(request, barNameList);
				appBar.translate(lmap);
			} catch (Exception e) {
				// cant translate app bar
			}
			// Token
			String tokenID = Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());

			String action = null;
			// if (cond.has) {
			// action = Constants.ActionTypes.CHANGE_PAGE;
			// } else {
			// action = Constants.ActionTypes.CHANGE_CONTENT;
			// }
			action = Constants.ActionTypes.CHANGE_CONTENT;
			resp = new Response(action, null, pageObject, tokenID, tokenKey, appBar, content);
		} else {
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;

	}

	// filter with type
	private String genTypeFilterCondition(String typeStr, DynamicUI ui, DBManager dbm1, String alias) throws Exception {
		String condition = "";
		if (DynamicFunction.isNotEmptyOrNull(ui.getFilterKey()) && !DynamicFunction.isNotEmptyOrNull(typeStr)) {
			condition = alias + ui.getFilterKey() + " = " + dbm1.param(typeStr, DataType.TEXT);
		}
		return condition;
	}

	private String genKeywordCondition(String searchStr, DynamicUI ui, DBManager dbm1, String alias) throws Exception {
		String sqlSearchStr = "";
		String condition2 = "";

		// search with String for temp table
		if (DynamicFunction.isNotEmptyOrNull(ui.getSearchField()) && DynamicFunction.isNotEmptyOrNull(searchStr)) {
			String[] searchArray = ui.getSearchField().split(",");
			int length = searchArray.length;
			for (int i = 0; i < length; i++) {
				String likeStr = "UPPER(" + alias + searchArray[i].trim() + ") like UPPER(" + dbm1.param("%" + searchStr + "%", DataType.TEXT) + ")";
				if (sqlSearchStr == "") {
					sqlSearchStr = likeStr;
				} else {
					sqlSearchStr += " or " + likeStr;
				}
			}
			if (sqlSearchStr != "") {
				condition2 = DynamicFunction.buildCondition(condition2, "(" + sqlSearchStr + ")");
			}
		}
		return condition2;
	}

	public JSONObject getResponse(JSONObject jsonContent) {
		JSONObject resJson = new JSONObject();
		try {
		} catch (JSONException e) {
			Log.error(e);
		}

		return resJson;
	}

	// get All template ids, then get related Data from data table
	public String getSelectField(JSONArray templateItems, String primaryKey) {
		String fieldStr = null;
		boolean hasPrimary = false;
		for (int i = 0; i < templateItems.length(); i++) {
			JSONObject item = templateItems.getJSONObject(i);
			String key = item.has("key") ? item.getString("key") : null;

			// ignore item with seq == 0 or null
			int seq = item.has("listSeq") ? item.getInt("listSeq") : 0;
			if (seq != 0 && key != null) {
				hasPrimary |= primaryKey.equals(key);
				if (fieldStr == null)
					fieldStr = key;
				else
					fieldStr += ", " + key;
			}
		}

		if (!hasPrimary) {
			// it must have fields
			fieldStr += ", " + primaryKey;
		}

		return fieldStr;
	}

	public void detailView(HttpServletRequest request) {

	}

}
