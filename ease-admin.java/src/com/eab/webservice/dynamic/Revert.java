package com.eab.webservice.dynamic;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.dynamic.DynamicDAO;
import com.eab.dao.dynamic.TemplateDAO;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.json.model.Field.Types;
import com.eab.model.dynamic.DynMultiLangField;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.profile.UserPrincipalBean;

/**
 * Servlet implementation class RevertAdd
 */
@WebServlet("/Dynamic/Revert")
public class Revert extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Revert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//1.update tmp action->"R"
		//2.copy the row from tmp to aud
		//3.delete record in tmp
		String module = request.getParameter("p1") ;
		String id = request.getParameter("p0") ;
		if(id != null && !id.equals("")){
			DynamicUI ui = null;
			HttpSession session = request.getSession();
			DBManager dbm = null;
			Connection conn = null;	
			DynamicDAO dynamic = new DynamicDAO();
			JSONObject template = null;
				try {
					conn = DBAccess.getConnection();
					dbm = new DBManager();
					ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
					if (ui == null || !ui.getModule().equals(module)) {
						ui = dynamic.getDynamicUI(module, conn);
						session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
					}
					
					// get dynamic template
					template = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) != null ? (JSONObject) session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) : null;
					if (template == null || !module.equals(template.getString("id"))) {
						TemplateDAO tempalteDao =  new TemplateDAO();
						template = tempalteDao.getTemplate(module, ui, conn);
						session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, template);
					}
					
					
					// current date
					Calendar calendar = Calendar.getInstance();
					java.util.Date currentDate = calendar.getTime();
					Date curDate = new java.sql.Date(currentDate.getTime());
					
					//1
					dynamic.updateTmpTableForCopy(request, dbm, ui, "V", id, curDate);
					
					//2
					UserPrincipalBean principal = Function.getPrincipal(request); 
					dynamic.copyFromTmpToAud(dbm, ui, id, principal);
					
					//3
					dynamic.deleteFromTmp(dbm, ui, id, principal);		
					
					//revert multi text fields
					DynMultiLangField dynMultiLangField = dynamic.getDynMultiLangField(ui.getModule());
					if(dynMultiLangField != null && dynMultiLangField.getTmpTable() != null){
						//update the action to approve "A", 
						dynamic.updateMULTmpTableForCopy(dbm, dynMultiLangField, ui, id, principal, curDate, "V");
						//then move to aud
						dynamic.copyMULfromTmpToAud(dbm, dynMultiLangField,  ui, id, principal.getCompCode());
						//delete from tmp table
						dynamic.deleteMULFromTmp(dbm, dynMultiLangField, ui, id, principal);			
					}
					
				}catch(Exception e){
					
				}finally{
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						Log.error(e);
					}
					dbm = null;
					conn = null;
				}	
		}
			//redirect to table 
			DynTableController tc = new DynTableController();
			tc.tableView(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
