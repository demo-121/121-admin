package com.eab.webservice.logout;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;

import com.eab.common.Constants;
import com.eab.common.Log;
import com.eab.common.LoginManager;
import com.eab.security.saml.IdPConfig;
import com.eab.security.saml.SAMLInit;

/**
 * Servlet implementation class WSEaseLogout
 */
@WebServlet("/ease/Logout")
public class WSEaseLogout extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public WSEaseLogout() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginManager loginMgr = null;
		boolean logoutSuccess = false;
		try {
			loginMgr = new LoginManager();
			loginMgr.logout(request, response);
		
			String domain = request.getServerName();	// e.g. 121.eabsystems.com
			int maxAge = 60;							// e.g. 30 seconds
			String path = "/"; 						//request.getContextPath();		// e.g. /admin
			ServletContext context = request.getServletContext();
			SAMLInit.initialize();
			IdPConfig idpConfig = new IdPConfig(new File(Constants.SAML_META_PATH + "idpmeta.xml"));
			
			String logoutPath = idpConfig.getLogoutUrl(); //EnvVariable.get("ADMIN_SAML_LOGOUT"); 
			if (!StringUtils.isEmpty(logoutPath)) {				
				path = logoutPath;
			}
			Log.debug("{/Logout} Session Expiry, domain="+domain+"; maxAge="+maxAge+"; path="+path);

			response.setStatus(200);
			response.sendRedirect(path);
			logoutSuccess = true;
		} catch(Exception e) {
			Log.error(e);
		} finally {
			loginMgr = null;
		}	
		if (!logoutSuccess) {
			response.sendRedirect("/");
		}
	}

}
