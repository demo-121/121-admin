package com.eab.webservice.logout;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;

import com.eab.common.Constants;
import com.eab.common.CookieManager;
import com.eab.common.EnvVariable;
import com.eab.common.Log;
import com.eab.common.LoginManager;
import com.eab.json.model.Response;
import com.eab.security.saml.IdPConfig;
import com.eab.security.saml.SAMLInit;
import com.google.gson.Gson;

/**
 * Servlet implementation class WSLogout
 */
@WebServlet("/Logout")
public class WSLogout extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSLogout() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginManager loginMgr = null;
		boolean logoutSuccess = false;
		try {
			loginMgr = new LoginManager();
			loginMgr.logout(request, response);
		
			String domain = request.getServerName();	// e.g. 121.eabsystems.com
			int maxAge = 60;							// e.g. 30 seconds
			String path = "/"; 						//request.getContextPath();		// e.g. /admin
			ServletContext context = request.getServletContext();
			SAMLInit.initialize();
//			IdPConfig idpConfig = new IdPConfig(context.getResourceAsStream("/WEB-INF/saml/idpmeta.xml"));
			IdPConfig idpConfig = new IdPConfig(new File(Constants.SAML_META_PATH + "idpmeta.xml"));
			
			String logoutPath = idpConfig.getLogoutUrl(); //EnvVariable.get("ADMIN_SAML_LOGOUT"); 
			if (!StringUtils.isEmpty(logoutPath)) {				
				path = logoutPath;
			}
			Log.debug("{/Logout} Session Expiry, domain="+domain+"; maxAge="+maxAge+"; path="+path);

			response.setStatus(200);
			response.sendRedirect(path);
			logoutSuccess = true;
		} catch(Exception e) {
			Log.error(e);
		} finally {
			loginMgr = null;
		}	
		if (!logoutSuccess) {
			response.sendRedirect("/");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
