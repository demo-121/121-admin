package com.eab.webservice.userroles;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eab.biz.userroles.UserRoleManager;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.google.gson.Gson;

@WebServlet("/UserRoles/Details")
public class WSDetails extends HttpServlet{
	private static final long serialVersionUID = 2L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Log.debug("on user role!!!!");
		UserRoleManager urMgr = new UserRoleManager(request);
		Response resp = urMgr.getDetailView();
		
		// Print response
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.setDateHeader("Expires", 0);
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		PrintWriter out = response.getWriter();

		Gson gson = new Gson();
		out.append(gson.toJson(resp));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
