package com.eab.webservice.bcp;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eab.common.Constants;
import org.json.JSONObject;

import com.eab.biz.bcp.BcpIndex;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.google.gson.Gson;

/**
 * Servlet implementation class WSBCProcessing
 */
@WebServlet("/BCProcessing")
public class WSBCProcessing extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSBCProcessing() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Log.debug("BCProcessing");
		Response resp 	= null;

		
		try {
			String param	= request.getParameter("p0");
			String action   = request.getParameter("p11");
//			String module 	= request.getParameter("p1");
			Log.debug("BCProcessing->para: "+param);
			
			
			JSONObject resultJSON = null;
			JSONObject actionJSON = null;
			
			// Get Parameter
			if (param != null && !param.isEmpty()) {
				String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
				resultJSON = new JSONObject(resultStr);	
				
			}
			
			if (action != null && !action.isEmpty()) {
				String actionStr = new String(Base64.getDecoder().decode(action.replace(" ", "+")), "UTF-8");
				actionJSON = new JSONObject(actionStr);	
				
			}

			
			
//			boolean init = true;
//			if(action != null) {
//				init = false;
//			}

			
			BcpIndex bcpm = new BcpIndex(request);
			resp = bcpm.getBcpApiStatus(resultJSON, actionJSON, request);

			response.setCharacterEncoding("utf-8");
			response.setContentType	("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");
			PrintWriter out = response.getWriter();
			Gson gson = new Gson();
			out.append(gson.toJson(resp));
		} catch(Exception e) {
			Log.error(e);
		} 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
