package com.eab.webservice;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Response;

import org.springframework.http.HttpStatus;

import com.eab.common.Log;

/**
 * Servlet implementation class WSError
 */
@WebServlet("/Error")
public class WSError extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSError() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 PrintWriter writer = response.getWriter();
		    writer.print("something");
		PrintWriter out = null;
		HttpSession session = null;
		String jsonString = "";
		String errorMessage = "Error";
		int status = 520;	// default 520 Unknown Error.  Refer to https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
		
		try {
			session = request.getSession(false);
			
			if (session != null && session.getAttribute("errorMessage") != null) {
				errorMessage = (String) session.getAttribute("errorMessage");
				jsonString = "{\"Message\"=\"" + errorMessage + "\"}";
				session.removeAttribute("errorMessage");
			}
			
			if (session != null && session.getAttribute("errorStatusCode") != null) {
				status = (int) session.getAttribute("errorStatusCode");
				session.removeAttribute("errorStatusCode");
			}
			
			Log.debug("{WSError} Status: " + status);
			Log.debug("{WSError} JSon Output: " + jsonString);
			
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");
			
			response.setStatus(status);
			
			out = response.getWriter();
			out.print(jsonString);
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
