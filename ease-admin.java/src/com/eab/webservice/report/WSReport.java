package com.eab.webservice.report;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eab.common.Constants;
import org.json.JSONObject;

import com.eab.biz.report.ReportManager;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.google.gson.Gson;

/**
 * Servlet implementation class WSChangeLang
 */
@WebServlet("/Report")
public class WSReport extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSReport() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Log.debug("Report");
		Response resp 	= null;
		String param	= request.getParameter("p0");
		String module 	= request.getParameter("p1");
		Log.debug("Report->para: "+param);

		JSONObject resultJSON =null;
		ReportManager  rm = new ReportManager(request);
		
		if(module != null){
			switch(module){
				case Constants.ReportPage.ONLINE_PAYMENT_REPORT:
					resp = rm.getOnlinePaymentReport(resultJSON, true);	
					break;
				case Constants.ReportPage.SUBMISSION_CASES_REPORT:
					resp = rm.getSubmissionCasesReport(resultJSON, true);
					break;
				case Constants.ReportPage.ALL_CHANNEL_REPORT:
					resp = rm.getAllChannelReport(resultJSON, true);
					break;
				case Constants.ReportPage.PROXY_HISTORY_REPORT:
					resp = rm.getProxyHistoryReport(resultJSON, true);
					break;
				case "session":
					resp = rm.getAllChannelReport(resultJSON, true);
					break;
			}
		}
		response.setCharacterEncoding("utf-8");
		response.setContentType	("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.setDateHeader("Expires", 0);
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		out.append(gson.toJson(resp));	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
