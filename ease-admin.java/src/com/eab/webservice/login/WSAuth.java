package com.eab.webservice.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.util.StringUtils;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.common.LoginManager;
import com.eab.common.MainMenu;
import com.eab.dao.audit.AudUserLog;
import com.eab.dao.profile.User;
import com.eab.model.MenuList;
import com.eab.model.RSAKeyPair;
import com.eab.model.profile.UserBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.system.SysParamBean;

/**
 * Servlet implementation class WSLogin
 */
@WebServlet("/Login/Auth")
public class WSAuth extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSAuth() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loginLang = null;
		PrintWriter out = null;
		String jsonString = "{\"View\": \"redirect:/Error\"}";
		String paramTimeZone = null;
		
		LoginManager loginMgr = new LoginManager(); 
		
		try {
			String paramLoginId = request.getParameter("data_001");
			String paramPassword = request.getParameter("data_002");
			paramTimeZone = request.getParameter("data_004");		

			loginLang = request.getParameter("lang");
			
//			Log.debug("Login ID: " + paramLoginId);
//			Log.debug("Password: " + paramPassword);
			jsonString = loginMgr.authenticate(request, response, loginLang);
			
			HttpSession session = request.getSession();
			UserPrincipalBean principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
			if(principal != null){
				paramTimeZone = request.getParameter("data_004");		
				if(paramTimeZone!=null){
					int tempTimeZone = Integer.valueOf(paramTimeZone) * -1;
					if (tempTimeZone > -1)
						paramTimeZone = "GMT+" + String.valueOf(tempTimeZone);
					else
						paramTimeZone = "GMT" + String.valueOf(tempTimeZone);
					
					principal.setTimezone(paramTimeZone);				
					session.setAttribute(Constants.USER_PRINCIPAL, principal);
				}
			}
			
			Log.debug("{WSAuth} JSon Output: " + jsonString);
			
			response.setCharacterEncoding("utf-8");
			response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");
			out = response.getWriter();
			out.print(jsonString);
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		}
		
	}
	
}
