package com.eab.webservice.login.terms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eab.common.Log;
import com.eab.common.LoginManager;
import com.eab.dao.audit.AudUserLog;
import com.eab.json.model.Response;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;

/**
 * Servlet implementation class WSTermsDecline
 */
@WebServlet("/Login/Terms/Decline")
public class WSTermsDecline extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSTermsDecline() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		Response resp = null;
		LoginManager loginMgr = null;
		
		try {
			loginMgr = new LoginManager();
			loginMgr.logout(request, response);	
			resp = loginMgr.getLoginDetails(request, "en");
		} catch(Exception e) {
			Log.error(e);
		} finally {
			loginMgr = null;
		}
		

		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.setDateHeader("Expires", 0);
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		out = response.getWriter();
		Gson gson = new Gson();
		out.append(gson.toJson(resp));
	}

}
