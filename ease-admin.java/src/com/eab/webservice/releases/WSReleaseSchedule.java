package com.eab.webservice.releases;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.eab.biz.releases.ReleaseDetailMgr;
import com.eab.biz.releases.ReleasePublishMgr;
import com.eab.common.Constants;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/Releases/Schedule")
public class WSReleaseSchedule extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSReleaseSchedule() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		boolean isMainPage = false;
		Response resp = null;
		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		
		String releaseID = "";
		String scheduleDate = "";
		String scheduleTime = "";
		
		try {
			try {
				String param = request.getParameter("p0");
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);
										
					//Parameter from Release Main Page
					if (resultJSON.has("rows")){
						if(resultJSON.getJSONArray("rows").length() > 0)
							releaseID = String.valueOf(resultJSON.getJSONArray("rows").get(0));
					}
					
					if (resultJSON.has("scheduleDate_Main")){
						isMainPage = true;
						Long timestamp = (Long)resultJSON.get("scheduleDate_Main");
						Date d = new Date(timestamp);
						SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
						scheduleDate = dt1.format(d);
					}
					
					if (resultJSON.has("scheduleTime_Main")){
						isMainPage = true;
						scheduleTime = (String) resultJSON.get("scheduleTime_Main");	
					}
						
					// Parameter from Release Detail Page	
					if (resultJSON.has("keyList"))
						releaseID = (String) resultJSON.get("keyList");
					
					if (resultJSON.has("scheduleDate_Detail")){
						Long timestamp = (Long)resultJSON.get("scheduleDate_Detail");
						Date d = new Date(timestamp);
						SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
						scheduleDate = dt1.format(d);
					}		
					
					if (resultJSON.has("scheduleTime_Detail"))
						scheduleTime = (String) resultJSON.get("scheduleTime_Detail");
				}
				
				if (request.getParameter("releaseID")!=null)
					releaseID = (String) request.getParameter("releaseID");
				
				if (request.getParameter("scheduleDate")!=null){
					Long timestamp = Long.getLong(request.getParameter("scheduleDate"));
					Date d = new Date(timestamp );
					scheduleDate = d.toString();
				}
				
				if (request.getParameter("scheduleTime")!=null)
					scheduleTime = (String) request.getParameter("scheduleTime");
				
				if(scheduleTime.length() > 19){
					scheduleDate = scheduleDate.substring(0 , 10) + scheduleTime.substring(10);
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Date checkGMTdate = sdf.parse(scheduleTime.substring(0 , 10));
					
					if(checkGMTdate.compareTo(new Date()) < 0){
						Calendar c = Calendar.getInstance(); 
						String tempScheduleDate = scheduleDate.replace("T", " ");
						tempScheduleDate = tempScheduleDate.substring(0,19);
							
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Date d = format.parse(tempScheduleDate);
						c.setTime(d); 
						//c.add(Calendar.DATE, -1);
						SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
						scheduleDate = dateformat.format(c.getTime()) + scheduleTime.substring(10);
					}
					
					String tempScheduleDate2 = scheduleDate.replace("T", " ");
					tempScheduleDate2 = tempScheduleDate2.substring(0,19);
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date d2 = format.parse(tempScheduleDate2);
					Calendar c2 = Calendar.getInstance(); 
					c2.setTime(d2);
					c2.add(Calendar.MINUTE, TimeZone.getDefault().getOffset(new Date().getTime()) / 1000 / 60);
					SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
					SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm:ss");
					scheduleDate = dateformat.format(c2.getTime()) + "T" +timeformat.format(c2.getTime()) + "Z";					
				}
			} catch (Exception e) {
				Log.error("{WSReleaseSchedule } ------------------> Exception ");
				Log.error(e);
			}
			
			ReleasePublishMgr releasePublish = new ReleasePublishMgr(request);		
			resp = releasePublish.ScheduleRelease(releaseID, scheduleDate, isMainPage);	

			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(gson.toJson(resp));
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error("{WSReleaseSchedule} ------------------>" + e);
		} finally {
			
		}
	}
}

