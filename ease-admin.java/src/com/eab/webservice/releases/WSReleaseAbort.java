package com.eab.webservice.releases;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eab.common.Constants;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.eab.biz.releases.ReleaseMgr;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.*;

/**
 * Servlet implementation class WSReleaseDetailTask
 */
@WebServlet("/Releases/Abort")
public class WSReleaseAbort extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSReleaseAbort() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		List<Object> releaseIDList = new ArrayList<>();
		
		Response resp = null;
		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		
		try {				
			// Get Parameter
			try {
				String param = request.getParameter("p0");				
								
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);
																					
					//Release ID List from release main
					if (resultJSON.has("rows")){    
						try {
							JSONArray jsonArray = (JSONArray)resultJSON.get("rows");

							for(Object obj : jsonArray){
								releaseIDList.add(Integer.toString((int) obj));
							}
						} catch(Exception e){
							HttpSession session = request.getSession();
							ArrayList<Map<String, Object>> list = (ArrayList<Map<String, Object>>) session.getAttribute(Constants.SessionKeys.SEARCH_RESULT);
														
							if (list != null) {
								for (int i = 0; i < list.size(); i++) {		
									releaseIDList.add(list.get(i).get("id").toString());
								}
							}	 	
						}	
					}
										
					//Release ID from release detail
					if (resultJSON.has("keyList"))
						releaseIDList.add((String) resultJSON.get("keyList"));
				}
			} catch (Exception e) {
				 Log.error(e);
			}
							
			ReleaseMgr release = new ReleaseMgr(request);
			resp = release.abortRelease(releaseIDList);
			        
			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(gson.toJson(resp));
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		} finally {
			
		}
	}
}
