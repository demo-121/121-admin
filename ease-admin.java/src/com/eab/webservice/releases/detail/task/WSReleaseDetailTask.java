package com.eab.webservice.releases.detail.task;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eab.common.Log;
import com.eab.biz.releases.ReleaseDetailTaskMgr;
import com.eab.biz.tasks.TaskReleaseMgr;

import org.json.*;

/**
 * Servlet implementation class WSReleaseDetailTask
 */
@WebServlet("/Releases/Detail/Task")
public class WSReleaseDetailTask extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSReleaseDetailTask() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		String outputJSON;
		String criteria = "";
		String sortBy = "";		
		String sortDir = "";	
		List<Object> rows = new ArrayList<>();
        int recordStart = 0;
        int pageSize = 0;
		
		try {				
			// Get Parameter
			try {
				String param = request.getParameter("p0");				
								
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);
																									
					//From release details
					if (resultJSON.has("rows")){    
						JSONArray jsonArray = (JSONArray)resultJSON.get("rows");
						
						for(Object obj : jsonArray){
							rows.add(obj);
						}
					}
					
					//From item page
					if (resultJSON.has("releaseID"))
						rows.add(resultJSON.has("releaseID"));
						
					
					//From sorting
					if (resultJSON.has("criteria"))
						criteria = (String) resultJSON.get("criteria");
					
					if (resultJSON.has("id")){    
						JSONArray jsonArray = (JSONArray)resultJSON.get("id");
						
						for(Object obj : jsonArray){
							rows.add(obj);
						}
					}
										
					if (resultJSON.has("sortBy"))
						sortBy = (String) resultJSON.get("sortBy");
					
					if (resultJSON.has("sortDir"))
						sortDir = (String) resultJSON.get("sortDir");
					
                    if (resultJSON.has("recordStart"))
                        recordStart = (int) resultJSON.get("recordStart");
                    
                    if (resultJSON.has("pageSize"))
                        pageSize = (int) resultJSON.get("pageSize");
				}
			} catch (Exception e) {
				 Log.error(e);
			}
																	
			ReleaseDetailTaskMgr releaseDetailTask = new ReleaseDetailTaskMgr(request);
			outputJSON = releaseDetailTask.getJson(criteria, rows, sortBy, sortDir, recordStart, pageSize);
			        
			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(outputJSON);
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		} finally {
			
		}
	}
}
