package com.eab.webservice.releases.detail.task;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eab.common.Constants;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.eab.biz.common.CommonManager;
import com.eab.biz.releases.ReleaseDetailMgr;
import com.eab.biz.releases.ReleaseDetailTaskMgr;
import com.eab.biz.tasks.TaskReleaseMgr;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.*;

/**
 * Servlet implementation class WSReleaseDetailTask
 */
@WebServlet("/Releases/Detail/Task/Add")
public class WSReleaseDetailTaskAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSReleaseDetailTaskAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		String outputJSON = "";
		List<Object> taskList = new ArrayList<>();
		List<Object> releaseID = new ArrayList<>();
		
        int recordStart = 0;
        int pageSize = 0;
        String releaseDetailId = "";
		String sortBy = "";
		String sortDir = "";

		try {				
			// Get Parameter
			try {
				String param = request.getParameter("p0");				
								
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);
						
					//Release ID
					if (resultJSON.has("keyList")){    
						JSONArray jsonArray = (JSONArray)resultJSON.get("keyList");
						
						for(Object obj : jsonArray){
							releaseID.add(obj);
						}
					}
					
					//Task List
					try {
						JSONArray jsonArray = (JSONArray)resultJSON.get("rows");
						
						for(Object obj : jsonArray){
							taskList.add(obj);
						}				
					} catch(Exception e){
						HttpSession session = request.getSession();
						ArrayList<Map<String, Object>> list = (ArrayList<Map<String, Object>>) session.getAttribute(Constants.SessionKeys.SEARCH_RESULT);
													
						if (list != null) {
							for (int i = 0; i < list.size(); i++) {		
								taskList.add(Integer.parseInt(list.get(i).get("id").toString()));
							}
						}	 	
					}
				}
				
				String reloadParam = request.getParameter("p10");
				if (reloadParam != null && !reloadParam.isEmpty()) {
					reloadParam = new String(Base64.getDecoder().decode(reloadParam.replace(" ", "+")), "UTF-8");
					JSONObject jsonObj = new JSONObject(reloadParam);
					JSONObject value = jsonObj.getJSONObject("value");
										
					//From release main page
					if (value.has("releaseID"))
						releaseDetailId = value.get("releaseID").toString();
					
					//From task selection
					if (value.has("value"))
						releaseDetailId = (String) value.get("value");
					
					//From table sorting
					if (value.has("id"))
						releaseDetailId = (String) value.get("id");
					
                    if (value.has("recordStart"))
                        recordStart = (int) value.get("recordStart");
                    
                    if (value.has("pageSize"))
                        pageSize = (int) value.get("pageSize");
                    
					if (value.has("sortBy"))
						sortBy = (String) value.get("sortBy");
					
					if (value.has("sortDir"))
						sortDir = (String) value.get("sortDir");
				}
			} catch (Exception e) {
				 Log.error(e);
			}
												
			ReleaseDetailTaskMgr releaseDetailTask = new ReleaseDetailTaskMgr(request);
			releaseDetailTask.AddTask((int) releaseID.get(0),taskList);
			
			//Get Json from task manager
			ReleaseDetailMgr release = new ReleaseDetailMgr(request);
			Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
			Response _response = release.getJson(releaseDetailId, false, sortBy, sortDir, recordStart, pageSize, false);
			_response.setAction(Constants.ActionTypes.CHANGE_CONTENT);
			outputJSON = gson.toJson(_response);
			
			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(outputJSON);
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		} finally {
			
		}
	}
}
