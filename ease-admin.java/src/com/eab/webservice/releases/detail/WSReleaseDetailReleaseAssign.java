package com.eab.webservice.releases.detail;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eab.common.Constants.ActionTypes;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.json.model.Response;
import com.eab.biz.common.CommonManager;
import com.eab.biz.releases.ReleaseDetailMgr;
import com.eab.biz.tasks.TaskMgr;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.*;

/**
 * Servlet implementation class WSTask
 */
@WebServlet("/Releases/Detail/Release/Assign")
public class WSReleaseDetailReleaseAssign extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSReleaseDetailReleaseAssign() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);// TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		String outputJSON;
		List<Object> taskList = new ArrayList<>();
		List<Object> releaseID = new ArrayList<>();

		try {
			try {
				String param = request.getParameter("p0");

				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);

					// Task List
					if (resultJSON.has("keyList")) {
						JSONArray jsonArray = (JSONArray) resultJSON.get("keyList");

						for (Object obj : jsonArray) {
							taskList.add(obj);
						}
					}

					// Release ID
					if (resultJSON.has("rows")) {
						JSONArray jsonArray = (JSONArray) resultJSON.get("rows");

						for (Object obj : jsonArray) {
							releaseID.add(obj);
						}
					}


				}
			} catch (Exception e) {
				Log.error(e);
			}

			ReleaseDetailMgr release = new ReleaseDetailMgr(request);
			release.assignRelease(taskList,(int) releaseID.get(0));
			
			CommonManager cMgr = new CommonManager(request);
			outputJSON = cMgr.reloadPage();
			// Return Json
			response.setCharacterEncoding("utf-8");
			response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");

			out = response.getWriter();
			out.print(outputJSON);
			out.flush();
			out.close();
		} catch (Exception e) {
			Log.error(e);
		} finally {

		}
	}
}
