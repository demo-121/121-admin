package com.eab.webservice;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.internal.util.StringHelper;

import com.eab.common.Constants;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.common.LoginManager;
import com.eab.json.model.Response;
import com.eab.security.saml.AttributeSet;
import com.eab.security.saml.IdPConfig;
import com.eab.security.saml.SAMLClient;
import com.eab.security.saml.SAMLException;
import com.eab.security.saml.SAMLInit;
import com.eab.security.saml.SPConfig;

/**
 * Servlet implementation class WSSaml
 */
@WebServlet("/saml")
public class WSSaml extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSSaml() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// Get parameter "SAMLResponse"
		PrintWriter out = response.getWriter();
		String samlResponse = request.getParameter("SAMLResponse");
		Response resp = null;
		if (!StringHelper.isEmpty(samlResponse)) {
			try {
				ServletContext context = request.getServletContext();
				SAMLInit.initialize();

//				IdPConfig idpConfig = new IdPConfig(context.getResourceAsStream("/WEB-INF/saml/idpmeta.xml"));
//				SPConfig spConfig = new SPConfig(context.getResourceAsStream("/WEB-INF/saml/spmeta.xml"));
				IdPConfig idpConfig = new IdPConfig(new File(Constants.SAML_META_PATH + "idpmeta.xml"));
				SPConfig spConfig = new SPConfig(new File(Constants.SAML_META_PATH + "spmeta.xml"));

				SAMLClient samlClient = new SAMLClient(spConfig, idpConfig);
				Log.debug("{LoginManager authenticate} idpConfig.getEntityId() = " + idpConfig.getEntityId());
				Log.debug("{LoginManager authenticate} spConfig.getEntityId() = " + spConfig.getEntityId());

				HttpSession session = request.getSession();
				Function2 func2 = new Function2();
				// Backup before current session is destroy
				Map<String, Object> sessionObjs = func2.backupSessionAttributes(request);

				// Destroy and Recreate Session ID (Backup any required data in
				// Session before destroy)
//				Log.debug("{LoginManager authenticate} Session ID (Current) = "
//						+ ((session != null) ? session.getId() : ""));
//				if (session != null) {
//					try {
//						session.invalidate(); // Destroy
//						session = request.getSession(true);
//					} catch (Exception e) {
//						Log.error(e);
//					}
//				}
//				Log.debug("{LoginManager authenticate} Session ID (New) = " + session.getId());

				// Restore after session is created
				func2.restoreSessionAttributes(request, sessionObjs);
				func2.stdoutSessionAttributes(request);

				// Validate
				AttributeSet attrSet = samlClient.validateResponse(samlResponse);

				Map<String, List<String>> attrs = attrSet.getAttributes();
				if (attrSet != null && attrs != null) {
					
					for(String key : attrs.keySet() ) {
						List<String> values = attrs.get(key);
						if (values != null) {
							Log.debug("Key in response:"+ key + " > list size:" + values.size());					
							if (values.size() > 0) {
								for (int i = 0; i < values.size(); i++) {
									Log.debug("    value:"+ i + " > list size:" + values.get(i));
								} 
							}
						} else {
							Log.debug("values is null");
						}		
					}
					
					List<String> mails = attrs.get("mail");
					List<String> names = attrs.get("displayName");
//					List<String> uids = attrs.get("nameID");
					List<String> groups = attrs.get("User.Group");
					
					String mail = null;
//					String uid = null;
					String name = null;
					if (mails != null) {
						mail = mails.get(0);
					}
//					if (uids != null) {
//						uid = uids.get(0);
//					}
					if (names != null) {
						name = names.get(0);
					}
					
					if (!StringHelper.isEmpty(mail)) {
						LoginManager lmgr = new LoginManager();

//						// hard code for test~
//						uid = "EASE_ADMIN";
//						if (lmgr.authenticate(request, uid)) {
//							out.println("<script>window.location.href = './'</script>");
//							return;
//						} else {
//							out.println("Login failure: invalid SAML Response");
//						}
						if (lmgr.loginBySaml(request, mail, name, groups, mail)) {
							out.println("<script>window.location.href = './ease/'</script>");
//							response.sendRedirect("./");
							Log.info("SAML>>> Goto Login process");
							return;
						} else {
							out.println("Login failure: invalid SAML Response");
							Log.error("SAML>>> Login failure: invalid SAML Response");
						}
					} else {
						out.println("Login failure: invalid SAML Response");
						Log.error("SAML>>> Login failure: invalid SAML Response");
					}
				} else {
					Log.debug("attrSet is null or attrs is null" + attrSet +" - " + attrs);
					Log.error("SAML>>> attrSet is null or attrs is null" + attrSet + " - " + attrs);
				}
			} catch (SAMLException e) {
				out.println("Login failure: invalid SAML Response:" + e.getMessage());
				Log.error("SAML Exception>>> Login failure: invalid SAML Response:" + e.getMessage());
				Log.error(e);
			} catch (Exception e) {
				out.println("Login failure: Others:" + e.getMessage());
				Log.error("SAML Exception>>> Login failure: Others:" + e.getMessage());
				//e.printStackTrace();  <-- stupid code here without using standard exception message output
				Log.error(e);
			}
		}

		out.flush();
		out.close();
	}

}
