package com.eab.webservice.webSettings;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eab.biz.users.UserManager;
import com.eab.biz.webSettings.WebSettingsManager;
import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.releases.ReleaseDetail;
import com.eab.json.model.Dialog;
import com.eab.json.model.DialogItem;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Response;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.webservice.dynamic.DynTableController;
import com.google.gson.Gson;

@WebServlet("/WebSettings")
public class WSWebSettings extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	UserPrincipalBean principal;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSWebSettings() {
	    super();
	    // TODO Auto-generated constructor stub
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
	    this.principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		
		DynTableController vc = new DynTableController();
		
		Response resp = null;
		resp = vc.getDynTable(request);
		
		WebSettingsManager wsMgr = new WebSettingsManager(request);
		wsMgr.addExtraTableProps(resp);		
		wsMgr.close();
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.setDateHeader("Expires", 0);
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		PrintWriter out =  null;
		try {
			out = response.getWriter();
			Gson gson = new Gson();
			String ret = gson.toJson(resp); 
//			Log.debug("end add request:" + ret);
			out.append(ret);
		} catch (IOException e) {
			Log.error(e);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
		}

		//		vc.tableView(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
