package com.eab.webservice.webSettings;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.eab.biz.dynamic.SaveEditManager;
import com.eab.biz.webSettings.WebSettingsManager;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.google.gson.Gson;

/**
 * Servlet implementation class SaveEdit
 */
@WebServlet("/WebSettings/SaveEdit")
public class SaveEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SaveEdit() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1. save a copy to aud table
		// 2. update tmp table for apprpval
		SaveEditManager sem = new SaveEditManager(request);
		Response resp = sem.saveEdit();

		WebSettingsManager wsMgr = new WebSettingsManager(request);
		wsMgr.addExtraTableProps(resp);		
		wsMgr.close();
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.setDateHeader("Expires", 0);
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		PrintWriter out =  null;
		try {
			out = response.getWriter();
			Gson gson = new Gson();
			String ret = gson.toJson(resp); 
			out.append(ret);
		} catch (IOException e) {
			Log.error(e);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	

}
