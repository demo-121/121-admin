package com.eab.webservice.needs.detail;

import java.io.IOException;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.biz.common.CommonManager;
import com.eab.biz.needs.NeedDetailMgr;
import com.eab.biz.products.ProductMgr;
import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class WSNewVersion
 */
@WebServlet("/Needs/NewVersion")
public class WSNeedNewVersion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSNeedNewVersion() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String needCode = null;
		String channelCode = null;
		// Get Parameter
		try {
			String param = request.getParameter("p10");
			if (param != null && !param.isEmpty()) {
				String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
				JSONObject resultJSON = new JSONObject(resultStr);
				
				JSONArray rows = new JSONArray();
				if (resultJSON.has("value")) {
					JSONObject value = resultJSON.getJSONObject("value");
					needCode = value.getString("id");
					channelCode = value.getString("channelCode");
				}

				NeedDetailMgr needDetailMgr = new NeedDetailMgr(request);
				Response resp = needDetailMgr.newVersion(needCode, channelCode);
				Gson gson = new GsonBuilder().create();

				// Print response
				response.setCharacterEncoding("utf-8");
		        response.setContentType("application/json; charset=utf-8");
		        response.setContentType("application/json; charset=utf-8");
				response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
				response.setDateHeader("Expires", 0);	
				response.addHeader("Cache-Control", "post-check=0, pre-check=0");
				response.setHeader("Pragma", "no-cache");
				response.getWriter().append(gson.toJson(resp));
			}
		} catch (Exception e) {
			Log.error(e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
