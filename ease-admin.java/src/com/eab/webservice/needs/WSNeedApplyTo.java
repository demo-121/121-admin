package com.eab.webservice.needs;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.biz.dynamic.LockManager;
import com.eab.biz.needs.NeedMgr;
import com.eab.common.Log;
import com.eab.dao.dynamic.RecordLock;
import com.eab.json.model.Dialog;
import com.eab.model.dynamic.RecordLockBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/Needs/Apply")
public class WSNeedApplyTo extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSNeedApplyTo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		String outputJSON = "";
		String newChannelCode = "";
		String typeFilter = "";
		String needCode = "";
		boolean confirmOverwrite = false;
		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		
		try {
			try {
				String param = request.getParameter("p0");
				
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);
														
					if (resultJSON.has("channelCode"))
						typeFilter = (String) resultJSON.get("channelCode");
					
					if (resultJSON.has("rows")){
						JSONArray jsonArray = (JSONArray)resultJSON.get("rows");
						
						for(Object obj : jsonArray){
							needCode = obj.toString();
						}					
					}
					
					if (resultJSON.has("radio_apply"))
						newChannelCode = (String) resultJSON.get("radio_apply");
					else {
						needCode = request.getSession().getAttribute("NEED_APPLY_NEED_CODE").toString();
						newChannelCode = request.getSession().getAttribute("NEED_APPLY_NEW_CHANNEL_CODE").toString();
						confirmOverwrite = true;
					}
				}
			} catch (Exception e) {
				 Log.error(e);
			}
			
			//Handle record lock
			RecordLock rlD = new RecordLock();
			
			RecordLockBean rlbD = rlD.newRecordLockBean("/Needs/Detail", needCode+typeFilter);
			rlD.getRecordLock(request, rlbD);
			Dialog dialog = null;
			
			if(!rlbD.canAccess()){
				LockManager lm = new LockManager(request);
				dialog = lm.lockDialog(rlbD);
			} else {
				if(!rlbD.isCurrentUserAccess())
					rlD.addRecordLock(request, rlbD);
			}
			
			//Get Json from needs manager
			NeedMgr need = new NeedMgr(request);
			outputJSON = gson.toJson(need.applyToNewChannel(needCode, typeFilter, newChannelCode, dialog, confirmOverwrite));
   
			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(outputJSON);
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		} finally {
			
		}
	}
}
