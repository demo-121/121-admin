package com.eab.webservice.password;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.eab.biz.system.ResetPassword;
import com.eab.biz.system.SystemParameters;
import com.eab.common.Constants;
import com.eab.common.Email;
import com.eab.common.Function;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.profile.User;
import com.eab.dao.system.Name;
import com.eab.model.profile.UserBean;

/**
 * Servlet implementation class ForgotRequest
 */
@WebServlet("/Password/ForgotRequest")
public class WSForgotRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String funcCode = "PWD.FORGOT_REQ";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSForgotRequest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String reqUserCode = null;
		PrintWriter out = null;
		String jsonString = "{\"Status\"=\"none\"}";
		String langCode = null;
		Language lang = null;
		boolean emailStatus = false;
		ResetPassword resetObj = null;
		
		try {
			reqUserCode = request.getParameter("uid");
			
			if (reqUserCode != null && reqUserCode.length() > 0) {
				lang = new Language();
				resetObj = new ResetPassword();
				
				// Get language code
				langCode = lang.getLang(request);
				Log.debug("(/Password/ForgotRequest) langCode=" + langCode);
				
				emailStatus = resetObj.apply(reqUserCode.toUpperCase(), langCode, funcCode, request);
			}
				ArrayList<Name> nameList = null;
				Name nameObj = null;
				Language langObj = null;
				
				langObj = new Language();
				nameList = new ArrayList<Name>();
				nameObj = new Name();
				
				JSONObject json = new JSONObject();		
				json.put("action", Constants.ActionTypes.CHANGE_CONTENT);
				Map<String , String>page = new HashMap<String, String>();
				page.put("id", "/Password/ForgotRequest");
				page.put("title", "Password");
				json.put("page", page);
				json.put("result", emailStatus);
				if(!emailStatus){
					if(reqUserCode != null && reqUserCode.length() > 0){
						json.put("message", "Username does not exist, please contact administrator.");
					}else{
						json.put("message", "Please enter your username");
					}
				}

				nameList = new ArrayList<Name>();
			    HashMap<String, String> nameMap;
			    List<String> labelList;			
			    if (!emailStatus) {
			    	labelList = Arrays.asList("PASSWORD.LABEL.FORGETTITLE", "PASSWORD.LABEL.FORGETSUBTITLE", "PASSWORD.BTN.REQUEST", "PASSWORD.HITS.USER", "PASSWORD.FLOATLABEL.USER", "PASSWORD.HITS.REPASSWORD", "PASSWORD.BTN.LOGIN" );
			    }else{
			    	labelList = Arrays.asList("PASSWORD.LABEL.FORGETTITLE", "PASSWORD.FORGET.LABEL.COMPILE", "PASSWORD.BTN.REQUEST", "PASSWORD.BTN.LOGIN");
			    }
				nameMap = new HashMap<String, String>();
				
				langObj = new Language();
				nameList = new ArrayList<Name>();
				
				for(String label: labelList) {
					nameObj = new Name();
					nameMap.put(label, "");
					nameObj.setCode(label);
					nameList.add(nameObj); 
				}
				
				if (langObj.getNameList(request, nameList) ) {
					for(Name dummy: nameList) {
						nameMap.replace(dummy.getCode(), dummy.getDesc());
					}
				}
				
				Map<String , String>template = new HashMap<String, String>();
				if (!emailStatus) {
					template.put("title", nameMap.get("PASSWORD.LABEL.FORGETTITLE"));
					template.put("subtitle", nameMap.get("PASSWORD.LABEL.FORGETSUBTITLE"));
					template.put("floatUser", nameMap.get("PASSWORD.FLOATLABEL.USER"));
					template.put("hintsUser", nameMap.get("PASSWORD.HITS.USER"));	
					template.put("btn", nameMap.get("PASSWORD.BTN.REQUEST"));		
					template.put("btn2", nameMap.get("PASSWORD.BTN.LOGIN"));		
				}else{
					template.put("subtitle", nameMap.get("PASSWORD.FORGET.LABEL.COMPILE"));
					template.put("title", nameMap.get("PASSWORD.LABEL.FORGETTITLE"));
					template.put("btn2", nameMap.get("PASSWORD.BTN.LOGIN"));		
				}
				
				json.put("template", template);
				
				response.setCharacterEncoding("utf-8");
		        response.setContentType("application/json; charset=utf-8");
				response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
				response.setDateHeader("Expires", 0);
				response.addHeader("Cache-Control", "post-check=0, pre-check=0");
				response.setHeader("Pragma", "no-cache");
				out = response.getWriter();
				out.print(json.toString());
				out.flush();
				out.close();
			
		} catch(Exception e) {
			Log.error(e);
		} finally {
			resetObj = null;
			lang = null;
		}
	}
	
}
