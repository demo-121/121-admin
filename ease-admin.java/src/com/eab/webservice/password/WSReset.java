package com.eab.webservice.password;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.eab.biz.system.ResetPassword;
import com.eab.common.Constants;
import com.eab.common.Log;
import com.eab.security.RSA;

/**
 * Servlet implementation class WSReset
 */
@Controller
@SessionAttributes("resetpassword")
@WebServlet("/Password/Reset")
public class WSReset extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final int TOKON_LENGTH = 30;
	private static final String RESET_PWD_TOKEN_NAME = "RESET_PWD_TOKEN";
	private static final String RESET_PWD_USERCODE_NAME = "RESET_PWD_USERCODE";
	public final static String LANG_NAME = "USER_LANG";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSReset() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String reqCode = null;
		ResetPassword resetBiz = null;
		boolean hasError = false;
		String userCode = null;
		String language = null;
		HttpSession session = null;
		String jsonString = "";
		PrintWriter out = null;
		String publicKeyJS = "";

		try {
			reqCode = request.getParameter("req");
			session = request.getSession();
			session.invalidate();
			session = request.getSession(true);

			//Parameter Validation
			if (reqCode == null || reqCode.length() != TOKON_LENGTH) {
				hasError = true;
			}

			//Verify Request Code (exist and non-expired).
			if (!hasError) {
				resetBiz = new ResetPassword();
				Map<String, String> resetMap = resetBiz.checkRequest(reqCode);
				if (resetMap != null) {
					userCode = resetMap.get("USER_CODE");
					language = resetMap.get("LANG_CODE");
				}

				if (userCode == null || userCode.length() == 0)
					hasError = true;
			}

			if (!hasError) {
				//Save Request Code & User Code into Session if valid request.
				session.setAttribute(RESET_PWD_TOKEN_NAME, reqCode);
				session.setAttribute(RESET_PWD_USERCODE_NAME, userCode);
				session.setAttribute(LANG_NAME, language);

				RSA cryptoObj = new RSA();
				if (cryptoObj.GenerateKey()) {
					publicKeyJS = cryptoObj.GetHTMLUsedModulus();

					session.setAttribute(Constants.CRYPTO_KEYPAIR_NAME, cryptoObj.Export());
				}

				//Generate JSON output based on verification result.
				jsonString = "{\"STATUS\"=\"OK\"}";
			} else {
				//Generate JSON output based on verification result.
				jsonString = "{\"STATUS\"=\"NONE\"}";
			}

			//Return JSON.

			out = response.getWriter();
			if (!hasError) {
				out.print((new com.eab.common.OutputPage()).getLayout(request, "Password/Reset", publicKeyJS));
			} else {
				out.print((new com.eab.common.OutputPage()).getLayout(request, "Password/ResetError"));
			}
			out.flush();
			out.close();
		} catch (Exception e) {
			Log.error(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	@RequestMapping(value = "/Password/Reset2", method = RequestMethod.GET)
	public String quotation(HttpServletRequest request, Locale locale, Model model) {

		return "reset";
	}

}
