package com.eab.webservice.password;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.eab.biz.system.PasswordPolicy;
import com.eab.biz.system.ResetPassword;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.system.Name;
import com.eab.model.RSAKeyPair;
import com.eab.model.system.PasswordCheckType;

/**
 * Servlet implementation class WSResetAuth
 */
@WebServlet("/Password/ResetAuth")
public class WSResetAuth extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String RESET_PWD_TOKEN_NAME = "RESET_PWD_TOKEN";
	private static final String RESET_PWD_USERCODE_NAME = "RESET_PWD_USERCODE";
	public final static String LANG_NAME = "USER_LANG";
	private static final int TOKON_LENGTH = 30;   
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSResetAuth() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResetPassword resetBiz = null;
		boolean hasError = false;
		String userCode = null;
		String token = null;
		HttpSession session = null;
		String jsonString = jsonString = "{\"code\"=\"-999\",\"message\"=\"\"}";
		PrintWriter out = null;
		String pwd01 = null;
		String pwd02 = null;
		String reqCode = null; 
		int checkResult = -1;
		ArrayList<Name> nameList = null;
		Name nameObj = null;
		Language langObj = null;
		RSAKeyPair keypair = null;

		try {
			resetBiz = new ResetPassword();
			langObj = new Language();
			nameList = new ArrayList<Name>();
			nameObj = new Name();

			session = request.getSession();
			//userCode = (session.getAttribute(RESET_PWD_USERCODE_NAME) != null) ? (String) session.getAttribute(RESET_PWD_USERCODE_NAME) : null;
			//token = (session.getAttribute(RESET_PWD_TOKEN_NAME) != null) ? (String) session.getAttribute(RESET_PWD_TOKEN_NAME) : null;

			// Get New Password Parameters.
			pwd01 = request.getParameter("pwd01"); // New Password
			pwd02 = request.getParameter("pwd02"); // Retype New Password
			reqCode = request.getParameter("p3");		// token
			
			if (session.getAttribute(Constants.CRYPTO_KEYPAIR_NAME) != null) {
				keypair = (RSAKeyPair) session.getAttribute(Constants.CRYPTO_KEYPAIR_NAME);
			}

			if (keypair != null) {
				pwd01 = Function.decryptJsText(keypair, pwd01);
				Log.debug("{WSRestAuth} Decrypted Password1 = " + pwd01);
				pwd02 = Function.decryptJsText(keypair, pwd02);
				Log.debug("{WSRestAuth} Decrypted Password2 = " + pwd02);
			}
			//Parameter Validation
			if (reqCode == null || reqCode.length() != TOKON_LENGTH) {
				hasError = true;
			}

			//Verify Request Code (exist and non-expired).
			if (!hasError) {
				resetBiz = new ResetPassword();
				Map<String, String> resetMap = resetBiz.checkRequest(reqCode);
				if (resetMap != null) {
					userCode = resetMap.get("USER_CODE"); 
					token = reqCode;
				} 
			}

			// Must have Token and User Code.
			if (token == null || userCode == null) {
				hasError = true;
			}

			// Validate.
			if (!hasError) {
				if (pwd01 != null && pwd02 != null && !pwd01.isEmpty() && !pwd01.isEmpty() && pwd01.equals(pwd02)) {
					checkResult = PasswordPolicy.validate(request, pwd01, userCode);

					if (checkResult == PasswordCheckType.Valid) {
						nameObj.setCode("PASSWORD.MSG.VALID");

					} else if (checkResult == PasswordCheckType.Invalid_Length) {
						nameObj.setCode("PASSWORD.MSG.INVALID_LENGTH");
						hasError = true;
					} else if (checkResult == PasswordCheckType.Invalid_Contain_Alpha) {
						nameObj.setCode("PASSWORD.MSG.INVALID_MISS_CHAR");
						hasError = true;
					} else if (checkResult == PasswordCheckType.Invalid_Contain_Num) {
						nameObj.setCode("PASSWORD.MSG.INVALID_MISS_Num");
						hasError = true;
					} else if (checkResult == PasswordCheckType.Invalid_Contain_AlphaNum) {
						nameObj.setCode("PASSWORD.MSG.INVALID_MISS_CHARNum");
						hasError = true;
					} else if (checkResult == PasswordCheckType.Invalid_UserCode_Pattern) {
						nameObj.setCode("PASSWORD.MSG.INVALID_CHAR_PATTERN");
						hasError = true;
					} else if (checkResult == PasswordCheckType.Invalid_History) {
						nameObj.setCode("PASSWORD.MSG.INVALID_HISTORY");
						hasError = true;
					} else {
						nameObj.setCode("PASSWORD.MSG.INVALID_OTHER");
						hasError = true;
					}

					// Clear Reset Password Transaction and update User Password from databse.
					if (!hasError) {
						if (!resetBiz.setNewPassword(request, token, userCode, pwd01)) {
							nameObj = new Name();
							nameObj.setCode("PASSWORD.MSG.INVALID_OTHER");
							checkResult = 998;
							hasError = true;
						}
					}

				}
			} else {
				if(!pwd01.equals(pwd02)){
					nameObj.setCode("PASSWORD.MSG.INVALID_MATCH");
				}else{
					nameObj.setCode("PASSWORD.MSG.INVALID_OTHER");
				}
				hasError = true;
			}

			JSONObject json = new JSONObject();

			if (nameObj.getCode() != null)
				nameList.add(nameObj);

			json.put("action", Constants.ActionTypes.CHANGE_CONTENT);
			Map<String, String> page = new HashMap<String, String>();
			page.put("id", "/Password/Reset");
			page.put("title", "Password");
			json.put("page", page);
			json.put("code", checkResult);

			if (nameList != null && nameList.size() > 0) {
				if (langObj.getNameList(request, nameList)) {
					if (nameList != null && nameList.size() > 0) {
						json.put("message", nameList.get(0).getDesc());

					}
				}
			}

			nameList = new ArrayList<Name>();
			HashMap<String, String> nameMap;
			List<String> labelList;
			if (hasError) {
				labelList = Arrays.asList("PASSWORD.LABEL.TITLE", "PASSWORD.LABEL.SUBTITLE", "PASSWORD.FLOATLABEL.PASSWORD", "PASSWORD.HITS.PASSWORD", "PASSWORD.FLOATLABEL.REPASSWORD", "PASSWORD.HITS.REPASSWORD", "PASSWORD.BTN.RESET", "PASSWORD.POPMSG.LENGTH", "PASSWORD.POPMSG.MISS_CHAR", "PASSWORD.POPMSG.MISS_Num", "PASSWORD.POPMSG.HISTORY", "PASSWORD.POPMSG.USERCODE_PATTERN");
			} else {
				labelList = Arrays.asList("PASSWORD.LABEL.SUCCESS", "PASSWORD.BTN.LOGIN");
			}
			nameMap = new HashMap<String, String>();

			langObj = new Language();
			nameList = new ArrayList<Name>();

			for (String label : labelList) {
				nameObj = new Name();
				nameMap.put(label, "");
				nameObj.setCode(label);
				nameList.add(nameObj);
			}

			if (langObj.getNameList(request, nameList)) {
				for (Name dummy : nameList) {
					nameMap.replace(dummy.getCode(), dummy.getDesc());
				}
			}

			Map<String, String> template = new HashMap<String, String>();
			if (hasError) {
				template.put("title", nameMap.get("PASSWORD.LABEL.TITLE"));
				template.put("subtitle", nameMap.get("PASSWORD.LABEL.SUBTITLE"));
				template.put("floatPW", nameMap.get("PASSWORD.FLOATLABEL.PASSWORD"));
				template.put("hintsPW", nameMap.get("PASSWORD.HITS.PASSWORD"));
				template.put("floatRPW", nameMap.get("PASSWORD.FLOATLABEL.REPASSWORD"));
				template.put("hintsRPW", nameMap.get("PASSWORD.HITS.REPASSWORD"));
				template.put("btn", nameMap.get("PASSWORD.BTN.RESET"));
				template.put("popLength", nameMap.get("PASSWORD.POPMSG.LENGTH"));
				template.put("popChar", nameMap.get("PASSWORD.POPMSG.MISS_CHAR"));
				template.put("popNum", nameMap.get("PASSWORD.POPMSG.MISS_Num"));
				template.put("popHistory", nameMap.get("PASSWORD.POPMSG.HISTORY"));
				template.put("popPattern", nameMap.get("PASSWORD.POPMSG.USERCODE_PATTERN"));
			} else {
				template.put("title", nameMap.get("PASSWORD.LABEL.SUCCESS"));
				template.put("btn", nameMap.get("PASSWORD.BTN.LOGIN"));
			}

			json.put("template", template);

			// Return JSON.
			response.setCharacterEncoding("utf-8");
			response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");
			out = response.getWriter();
			out.print(json.toString());
			out.flush();
			out.close();

		} catch (Exception e) {
			Log.error(e);
		} finally {
			resetBiz = null;
			nameList = null;
			nameObj = null;
			langObj = null;
		}

	}

}
