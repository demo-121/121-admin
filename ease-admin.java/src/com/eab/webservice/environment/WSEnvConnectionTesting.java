package com.eab.webservice.environment;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.eab.biz.environment.EnvironmentMgr;
import com.eab.common.Log;
import com.eab.model.environment.EnvBean;

@WebServlet("/Env/TestConn")
public class WSEnvConnectionTesting extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSEnvConnectionTesting() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		String outputJSON = "";
		EnvBean envBean = new EnvBean();
		
		try { 
			try {
				String param = request.getParameter("p0");
				
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);
					
					if (resultJSON.has("envId"))
						envBean.setEnvId((String) resultJSON.get("envId"));
					
					if (resultJSON.has("host"))
						envBean.setHost((String) resultJSON.get("host"));
					
					if (resultJSON.has("port"))
						envBean.setPort((String) resultJSON.get("port"));
					
					if (resultJSON.has("dbName"))
						envBean.setDBName((String) resultJSON.get("dbName"));
							
					if (resultJSON.has("dbLogin"))
						envBean.setDBLogin((String) resultJSON.get("dbLogin"));
							
					if (resultJSON.has("dbPassword"))
						envBean.setDBPassword((String) resultJSON.get("dbPassword"));
				}
			} catch (Exception e) {
				Log.error("{WSEnvConnectionTesting doPost} ----------->" + e);
			}
									
			//Get Json from release manager
			EnvironmentMgr environment = new EnvironmentMgr(request);
			outputJSON = environment.getJson(envBean);
   
			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(outputJSON);
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		} finally {
			
		}
	}
}
