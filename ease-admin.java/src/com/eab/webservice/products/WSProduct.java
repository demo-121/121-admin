package com.eab.webservice.products;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.eab.biz.products.ProductMgr;
import com.eab.common.JsonConverter;
import com.eab.common.Log;
import com.eab.json.model.ProductListCondition;
import com.eab.json.model.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class WSProduct
 */
@WebServlet("/Products/Detail")
public class WSProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSProduct() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String prodCode = "";
		int version = 1;
		String selectPageId = "";
		PrintWriter out = null;
		Gson gson = new GsonBuilder().create();
		
		try {

			String param = request.getParameter("p0");
			if (param != null && !param.isEmpty()) {
				String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
				JSONObject resultJSON = new JSONObject(resultStr);
				
				if (resultJSON.has("id"))
					prodCode = resultJSON.getString("id");
				
				if (resultJSON.has("version"))
					version = resultJSON.getInt("version");
				
				if(resultJSON.has("selectPageId"))
					selectPageId = resultJSON.getString("selectPageId");		
			}
			ProductMgr productManager = new ProductMgr(request);
	        response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");
			
			response.getWriter().append(gson.toJson(productManager.getProdDetail(prodCode, version, selectPageId)));
//			out = response.getWriter();
//			// out.print(jsonString);
//			
//			Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
//
//			out.append(gson.toJson(productManager.getProdDetail(prodCode, version, selectPageId)));		
//			out.flush();
//			out.close();
		} catch (Exception e) {
			Log.error(e);
		}	
	}

}
