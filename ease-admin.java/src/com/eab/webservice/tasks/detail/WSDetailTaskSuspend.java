package com.eab.webservice.tasks.detail;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eab.common.Log;
import com.eab.json.model.Response;
import com.eab.biz.tasks.TaskInfoMgr;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.*;

/**
 * Servlet implementation class WSReleaseDetailTask
 */
@WebServlet("/Tasks/Detail/Suspend")
public class WSDetailTaskSuspend extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSDetailTaskSuspend() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		String module = null;
		List<Object> taskID = new ArrayList<>();
		Gson gson = new GsonBuilder().create();
		Response resp = null;
				
		try {				
			// Get Parameter
			try {
				String param = request.getParameter("p0");				
								
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);
					//Task ID 
					if (resultJSON.has("rows")){    
						JSONArray jsonArray = (JSONArray)resultJSON.get("rows");
						
						for(Object obj : jsonArray){
							taskID.add(obj);
						}
					}
					
				}
				
				String param2 = request.getParameter("p10");	
				
				if (param2 != null && !param2.isEmpty()) {
					String resultStr2 = new String(Base64.getDecoder().decode(param2.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON2 = new JSONObject(resultStr2);
																	
					//module
					if (resultJSON2.has("id")){    
						module = resultJSON2.getString("id");
					}			
				}
				
			} catch (Exception e) {
				 Log.error(e);
			}
			
			TaskInfoMgr taskInfoMgr = new TaskInfoMgr(request, module);
			resp = taskInfoMgr.taskDetailUpdate(taskID, 0, "D");
			
			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(gson.toJson(resp));
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		} finally {
			
		}
	}
}

