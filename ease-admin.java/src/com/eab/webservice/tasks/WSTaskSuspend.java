package com.eab.webservice.tasks;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eab.common.Constants;
import com.eab.common.Log;
import com.eab.biz.tasks.TaskMgr;

import org.json.*;

/**
 * Servlet implementation class WSTask
 */
@WebServlet("/Tasks/Suspend")
public class WSTaskSuspend extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSTaskSuspend() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		String outputJSON;
		List<Object> taskList = new ArrayList<>();
		TaskMgr task = new TaskMgr(request);
		
		try {				
			try {
				String param = request.getParameter("p0");
				
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);
											
					//Task List
					if (resultJSON.has("rows")){						
						try {
							JSONArray jsonArray = (JSONArray)resultJSON.get("rows");
							
							for(Object obj : jsonArray){
								taskList.add(obj);
							}				
						} catch(Exception e){
							HttpSession session = request.getSession();
							ArrayList<Map<String, Object>> list = (ArrayList<Map<String, Object>>) session.getAttribute(Constants.SessionKeys.SEARCH_RESULT);
														
							if (list != null) {
								for (int i = 0; i < list.size(); i++) {		
									taskList.add(Integer.parseInt(list.get(i).get("id").toString()));
								}
							}	 	
						}
					}						
				}
			} catch (Exception e) {
				 Log.error(e);
			}						
						
			outputJSON = task.updateTask(taskList, "S", -1);
        
			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(outputJSON);
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		} finally {
			
		}
	}
}
