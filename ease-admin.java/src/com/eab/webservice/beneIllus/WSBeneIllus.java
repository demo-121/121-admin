package com.eab.webservice.beneIllus;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.eab.biz.beneIllus.BeneIllusMgr;
import com.eab.common.Log;
import com.eab.json.model.BiListCondition;
import com.eab.json.model.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/BeneIllus")
public class WSBeneIllus  extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSBeneIllus() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Gson gson = new GsonBuilder().create();
			BiListCondition biListCondition = null;
			
			// Get Parameter
			String param = request.getParameter("p0");
			if (param != null && !param.isEmpty()) {
				String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
				if (resultStr != null && !resultStr.isEmpty()) {
					biListCondition = gson.fromJson(resultStr, BiListCondition.class);									
				}
			}

			// ProductManager
			BeneIllusMgr biMgr = new BeneIllusMgr(request);
			Response rs = biMgr.biList(biListCondition);

			// Print response
			response.setCharacterEncoding("utf-8");
			response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");

			response.getWriter().append(gson.toJson(rs));
		} catch (Exception e) {
			Log.error(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
