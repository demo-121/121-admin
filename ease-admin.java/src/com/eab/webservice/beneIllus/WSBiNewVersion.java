package com.eab.webservice.beneIllus;

import java.io.IOException;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.biz.beneIllus.BeneIllusMgr;
import com.eab.biz.common.CommonManager;
import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class WSNewVersion
 */
@WebServlet("/BeneIllus/NewVersion")
public class WSBiNewVersion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSBiNewVersion() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String biCode = null;
		
		// Get Parameter
		try {
			String param = request.getParameter("p0");
			if (param != null && !param.isEmpty()) {
				String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
				JSONObject resultJSON = new JSONObject(resultStr);
				
				JSONArray rows = new JSONArray();
				if (resultJSON.has("rows")) {
					rows = resultJSON.getJSONArray("rows");
				}

				// Fetch rows
				for (int i = 0; i < rows.length(); i++) {
					String row = (String) rows.get(i);
					if (row != null && !row.isEmpty()) {
						biCode = row;
					}
				}

				BeneIllusMgr biMgr = new BeneIllusMgr(request);
				int version = biMgr.newVersion(biCode);
				
				Gson gson = new GsonBuilder().create();

				// Print response
				response.setCharacterEncoding("utf-8");
		        response.setContentType("application/json; charset=utf-8");
		        response.setContentType("application/json; charset=utf-8");
				response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
				response.setDateHeader("Expires", 0);	
				response.addHeader("Cache-Control", "post-check=0, pre-check=0");
				response.setHeader("Pragma", "no-cache");
				if(version > 1){
					response.getWriter().append(gson.toJson(biMgr.getBiDetail(biCode, version, null)));
				}else{
					CommonManager cMgr = new CommonManager(request);
					Language langObj = new Language(((UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL)).getUITranslation());
					response.getWriter().append(cMgr.returnMsg(langObj.getNameDesc("ERROR"), langObj.getNameDesc("PROD.ERR_MSG.NEW_VERSION_FAIL")));
				}
				//response.getWriter().append(gson.toJson(rs));
			}
		} catch (Exception e) {
			Log.error(e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
