package com.eab.webservice.downloadMaterial;

import java.io.IOException;
import java.io.PrintWriter;
//import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.eab.common.Constants;
import org.json.JSONObject;

import com.eab.biz.downloadMaterial.DownloadMaterialManager;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.google.gson.Gson;

/**
 * Servlet implementation class WSChangeLang
 */
@WebServlet("/DownloadMaterial")
public class WSDownloadMaterial extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSDownloadMaterial() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Log.debug("Report");
		Response resp 	= null;
		String param	= request.getParameter("p0");
		String module 	= request.getParameter("p1");
		Log.debug("Report->para: "+param);

		JSONObject resultJSON =null;
		DownloadMaterialManager  dmm = new DownloadMaterialManager(request);
		
		if(module != null){
			switch(module){
				case "session":
					resp = dmm.getSession(resultJSON, true);
					break;
			}
		}
		response.setCharacterEncoding("utf-8");
		response.setContentType	("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.setDateHeader("Expires", 0);
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		out.append(gson.toJson(resp));	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
