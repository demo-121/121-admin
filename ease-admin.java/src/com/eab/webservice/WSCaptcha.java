package com.eab.webservice;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eab.common.Constants;
import com.eab.common.Log;
import com.eab.model.CaptchaData;
import com.google.code.kaptcha.Producer;
import com.google.code.kaptcha.util.Config;

/**
 * Servlet implementation class captcha
 */
//@WebServlet("/captcha.jpg")
public class WSCaptcha extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static Properties props = new Properties();
    private static Producer kaptchaProducer = null;
       
    @Override
    public void init(ServletConfig conf) throws ServletException
    {
    	Log.info("{Captcha}{init} ------Start------");
    	
        super.init(conf);

        // Switch off disk based caching.
        ImageIO.setUseCache(false);

        Enumeration<?> initParams = conf.getInitParameterNames();
        while (initParams.hasMoreElements())
        {
            String key = (String) initParams.nextElement();
            String value = conf.getInitParameter(key);
            props.put(key, value);
        }

        Config config = new Config(this.props);
        kaptchaProducer = config.getProducerImpl();
        
//        Log.debug("{Captcha}{init} Test Captcha: " + kaptchaProducer.createText());
        
        Log.info("{Captcha}{init} ------End------");
    }
    
    public CaptchaData Generate() {
    	CaptchaData result = new CaptchaData();
    	
        // create the text for the image
    	String capText = kaptchaProducer.createText();
    	result.setText(capText);
        Log.debug("Request Captcha: "+capText);
        
        // create the image with the text
        result.setImage(kaptchaProducer.createImage(capText));
        
        result.setCreateTime(new Date());
        
        return result;
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	ServletOutputStream out = response.getOutputStream();
    	
    	try {
	    	// Generate Captcha
	    	CaptchaData captcha = Generate();
	    	String capText = captcha.getText();
	    	BufferedImage bi = captcha.getImage();
	        
	        // store the text in the session
	        request.getSession().setAttribute(Constants.CAPTCHA_TEXT, capText);
	        
	        // store the date in the session so that it can be compared
	        // against to make sure someone hasn't taken too long to enter
	        // their kaptcha
	        request.getSession().setAttribute(Constants.CAPTCHA_DATETIME, captcha.getCreateTime());
	        
	        // Set to expire far in the past.
	        response.setDateHeader("Expires", 0);
	        // Set standard HTTP/1.1 no-cache headers.
	        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
	        // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
	        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
	        // Set standard HTTP/1.0 no-cache header.
	        response.setHeader("Pragma", "no-cache");
	        response.setContentType("image/jpeg");
	        
	        // write the data out
	        ImageIO.write(bi, "jpg", out);
    	} catch(Exception e) {
    		Log.error(e);
    	} finally {
	        out = response.getOutputStream();
	        out.flush();
	        out.close();
    	}
        
    }

}