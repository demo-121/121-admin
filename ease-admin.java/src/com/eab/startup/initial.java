package com.eab.startup;

import java.io.File;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.sql.Connection;
import java.util.Enumeration;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.core.env.SystemEnvironmentPropertySource;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.dao.dynamic.RecordLock;
import com.eab.database.jdbc.DBAccess;
import com.eab.model.CaptchaData;
import com.eab.model.system.SysParamBean;
import com.eab.util.jobrunner.JobRunner;
import com.eab.webservice.WSCaptcha;

public class initial extends HttpServlet {

	private JobRunner runner;
	private static final String filesep = File.separator;
	
	public void destroy() {
		//Nothing
	}
	
	public void init(ServletConfig config) throws ServletException {
		Connection connection = null;
		DataSource dataSource = null;
		Function2 func2 = new Function2();
		Map<String, SysParamBean> sysParamList = null;
		
		Log.info("Initialzation ......Start");
		
		super.init(config);
		
		try {
			// Load Datasource
			Constants.DATABASE_TYPE = config.getInitParameter("dbtype");
			Log.info("Database Type: " + Constants.DATABASE_TYPE);
			
			Constants.DATASOURCE_STRING = config.getInitParameter("datasource");
			Log.info("DataSource: " + Constants.DATASOURCE_STRING);

			Constants.DATASOURCE_API_STRING = config.getInitParameter("datasourceApi");
			Log.info("DataSourceAPI: " + Constants.DATASOURCE_API_STRING);
			
			Context context = new InitialContext(); 
			DBAccess.setDatasource((DataSource) context.lookup(Constants.DATASOURCE_STRING));
			DBAccess.setDatasourceApi((DataSource) context.lookup(Constants.DATASOURCE_API_STRING));

			// Inital Constants
			Constants.SERVER_IP_ADDR = getServerIp();
			
			// Mark logout time to AUD_USER_LOG being created by current server.
			int count = func2.setLogoutTimeToAud(Constants.SERVER_IP_ADDR);
			Log.info("Server: " + Constants.SERVER_IP_ADDR + ", Marked Logout Time record: " + count);
			
			//clear record lock
			RecordLock rl = new RecordLock();
			boolean removeRecord = rl.removeRecordLockByIp(Constants.SERVER_IP_ADDR);
			
			// Generate Random Key for disable browser cached contents after restart.
			Constants.APP_STARTUP_KEY = Function.GenerateRandomString(10);
			
			// Get Real Path (war location)
			Constants.APP_REAL_PATH = config.getServletContext().getRealPath("/");
			if (Constants.APP_REAL_PATH != null && Constants.APP_REAL_PATH.length() > 0) {
				String lastChar = Constants.APP_REAL_PATH.substring(Constants.APP_REAL_PATH.length()-1);
				if (!File.separator.equals(lastChar))
					Constants.APP_REAL_PATH = Constants.APP_REAL_PATH + File.separator;
			}
			Log.info("App Real Path: " + Constants.APP_REAL_PATH);
			
			// Load Max. Login Fail from System parameters
			sysParamList = func2.getSysParam();
			String maxFail = sysParamList.get("MAX_LOGIN_FAIL").getParamValue();
			if (maxFail != null && maxFail.length() > 0) {
				Constants.LOGIN_FAIL_COUNT_MAX = Integer.parseInt(maxFail);
			}
			Log.info("Max Fail Count: " + Constants.LOGIN_FAIL_COUNT_MAX);
			
			Constants.APP_STARTUP_COMPLETE = true;		//Mark true when all startup processes are completed.
			
//			Constants.SAML_META_PATH = config.getServletContext().getRealPath(filesep + "WEB-INF" + filesep + "classes" + filesep + "resource" + filesep + "saml" + filesep);
			Constants.SAML_META_PATH = config.getServletContext().getRealPath("/WEB-INF/classes/resource/saml/");
			Log.info("SAML Meta Path: " + Constants.SAML_META_PATH);

			if (Constants.SAML_META_PATH != null && Constants.SAML_META_PATH.length() > 0) {
				String lastChar = Constants.SAML_META_PATH.substring(Constants.SAML_META_PATH.length()-1);
				if (!filesep.equals(lastChar))
					Constants.SAML_META_PATH = Constants.SAML_META_PATH + filesep;
			}
			
			Constants.RESOURCE_ROOT = config.getServletContext().getRealPath("/WEB-INF/classes/resource/");
			if (Constants.RESOURCE_ROOT != null && Constants.RESOURCE_ROOT.length() > 0) {
				String lastChar = Constants.RESOURCE_ROOT.substring(Constants.RESOURCE_ROOT.length()-1);
				if (!filesep.equals(lastChar))
					Constants.RESOURCE_ROOT = Constants.RESOURCE_ROOT + filesep;
			}
			
		} catch(Exception e) {
			Log.error(e);
			Log.error("Initialzation ......Incomplete.  Please check.");
		} finally {
			func2 = null;
			sysParamList = null;
		}
		
		Log.info("Initialzation ......Finish");
		
        ServletContext scontext = this.getServletContext();
        String path = scontext.getRealPath("." + File.separator);
        Log.info("path=..........................." + path);
        runner = new JobRunner(path + "/WEB-INF");
        runner.run();
	}
	
	public String getServerIp() {
    	String ipAddr = "0.0.0.0";
    	
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface) networkInterfaces
                        .nextElement();
                Enumeration<InetAddress> nias = ni.getInetAddresses();
                while(nias.hasMoreElements()) {
                    InetAddress ia= (InetAddress) nias.nextElement();
                    if (!ia.isLinkLocalAddress() 
                     && !ia.isLoopbackAddress()
                     && ia instanceof Inet4Address) {
                    	return ia.getHostAddress();
                    }
                }
            }
        } catch (Exception e) {
        	Log.error(e);
        }
        return ipAddr;
    }
}
