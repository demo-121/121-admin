package com.eab.util.jobrunner;

import java.sql.*;
import java.lang.reflect.Method;
import java.util.*;

public class BatchJobThread extends Thread
{
    private String comp_code = "";
    private String batch_no = "";
    private boolean isOnlineBatch = false;
    private String func_code = "";
    public BatchJobThread(String compCode , String batchNo)
    {
        this.comp_code = compCode ;
        this.batch_no = batchNo;
    }
	
	
    public void run(String func_type)
    {
        Connection con = null ;
        Statement stmt = null ;
        String jobname = "" ;
        String func_code = "";
   

        if ( jobname != null && !("").equals(jobname) )
        {
            int lastChr = jobname.lastIndexOf(".");
            try
            {
                if ( lastChr >= 0 )
                {
                    String classname = jobname.substring( 0, lastChr) ;
                    String methodname = jobname.substring( lastChr+1 ) ;
                    
                    System.out.println("@@ "+classname);
					System.out.println("@@ "+classname);

                    Class executeClass = Class.forName(classname);
                    Object runnableObject = executeClass.newInstance();

                    Method executeMethod=null;
                    //Call method
                    if(methodname!=null)
                    {
                        Class [] arcs = { Class.forName("java.lang.String") , Class.forName("java.lang.String") };
                        executeMethod = executeClass.getMethod(methodname,arcs);
                    }
                    else
                    {
                        return ;
                    }
                    String [] args = {this.comp_code,func_code };
					System.out.println("RUN:" + classname + "." + methodname + "(" + this.comp_code + "," + func_code + ")........" );
                    executeMethod.invoke(runnableObject,args);
                }
                else
                {
                    //throw new Exception("error:function name is invalid:" + jobname );
                }
            }
            catch(Exception e)
            {}
        }
    }
	   
    public static void main(String[] args)
    {
        BatchJobThread job = new BatchJobThread("01","424");
        job.start() ;
    }
}
