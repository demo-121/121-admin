/* This source has been formatted by an unregistered SourceFormatX */
/* If you want to remove this info, please register this shareware */
/* Please visit http://www.textrush.com to get more information    */
/* This source has been formatted by an unregistered SourceFormatX */
/* If you want to remove this info, please register this shareware */
/* Please visit http://www.textrush.com to get more information    */
/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 2002 - 4 - 27
 * Time: 15:25:58
 * To change template for new class use
 * Code Style | Class Templates options ( Tools | IDE Options ) .
 */
package com.eab.util.jobrunner;

//import org.apache.commons.digester.Digester;
import org.apache.commons.digester3.Digester;

import com.eab.common.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;


/**
 * The main class to start the server, parse the configuration XML file
 * and initiate the jod threads
 */
public class JobRunner {
    /**
     * Collection of clas Job
     */
    private Collection jobQueue = null;

    /**
     * jobclass
     */
    private final String jobClassName = "com.eastpro.util.jobrunner.Job";

    /**
     * config
     */
    private String configFile = "." + File.separator
        + "Jobrunner.xml";

    /**
     *
     * @param path String
     */
    public JobRunner(String path) {
        this.configFile = path + File.separator + "Jobrunner.xml";
    }

    /**
     *
     * @param configFile String
     * @param i int
     */
    public JobRunner(String configFile, int i) {
        this.configFile = configFile;
    }

    /**
     * comnstructor
     */
    public JobRunner() {
    }

    /**
     * Parse the configuration XML file and initiate the Log
     * @throws Exception err
     */
    public void init() throws Exception {
        //Initialize //Logger
        //Logger.init();
        //Logger.debug("log4j //Logger have been initialize successly");
        //Logger.info("jobRunner begin to initialize");

        //initiate the digester
        jobQueue = new ArrayList();

        Digester digester = new Digester();
        digester.push(this);
        digester.setValidating(false);

        //add rules to digester
        digester.addObjectCreate("JobRunner/Job", jobClassName, "Type");
        digester.addSetProperties("JobRunner/Job");
        digester.addSetProperty("JobRunner/Job/Property", "Name", "Value");
        digester.addSetNext("JobRunner/Job", "addJob", jobClassName);

        //begin to parse
        InputStream input = null;

        try {
            input = new FileInputStream(configFile);
            digester.parse(input);
        } catch (Exception e) {
            //Logger.error(e.getMessage());
            e.printStackTrace();
            throw new Exception("error:can not parse XML file");
        } finally {
            if (input != null) {
                input.close();
            }
        }
    }

    /**
     * Start the thread for each job described in the configuration
     */
    public void startJobs() {
    	Log.info("jobRunner start Job");
        if (this.jobQueue == null) {
        	Log.info("jobRunner have no job to run!");
            //Logger.info("jobRunner have no job to run!");

            return;
        }

        Iterator jobList = this.jobQueue.iterator();
        while (jobList.hasNext()) {
            Job job = (Job) jobList.next();
            Log.info(job.getClassName() + "."  + job.getMethodName() + "() " + "type:" + job.getPeriodType() + " job is ready to start.");
            //Logger.debug(" ");
            //Logger.debug(
               // "************************************************");
            //Logger.debug(job.getClassName() + "."
                //+ job.getMethodName() + "() " + "type:"
                //+ job.getPeriodType() + " job is ready to start.");
            //Logger.debug("startTime=" + job.getStartTime()
               // + ";startDelay=" + job.getStartDelay() + ";delay="
                //+ job.getDelay() + ";");

            job.startup();
        }

        //Logger.info(" ");
        //Logger.info("Initializing the jobs have been finished!");
        //Logger.info(" ");
    }

    /**
     * This function is called by the XML parser to add a Job to the jobQueue
     *
     * @param aJob the Job object that will be added to jobQueue
     */
    public void addJob(Job aJob) {
        if (jobQueue != null) {
            jobQueue.add(aJob);
        }
    }

    /**
     * This function can cancel all jobs by cancel() of TimerTask
     */
    public void closeAllJobs() {
        if (this.jobQueue == null) {
            //Logger.info("jobRunner have no job to close!");

            return;
        }

        Iterator jobList = this.jobQueue.iterator();
        //Logger.info("cancel all jods");

        while (jobList.hasNext()) {
            Job job = (Job) jobList.next();

            if (job.cancel()) {
                //Logger.info(job.getClassName() + "."
                    //+ job.getMethodName() + "() " + "type:"
                   // + job.getPeriodType() + " job has been canceled.");
            } else {
                //Logger.info(job.getClassName() + "."
                    //+ job.getMethodName() + "() " + "type:"
                   // + job.getPeriodType()
                   // + " job cancelling is not successful.");
            }
        }

        //Logger.info(" jobs cancelling finished.");
    }

    /**
     * Call init() and startJobs() to start the runner
     */
    public void run() {
        try {
        	System.out.println("jobRunner Start init");
            this.init();
            //Logger.info("jobRunner initialize successfully");
            //Logger.info("jobRunner is ready to execute the job");
            this.startJobs();
            System.out.println("jobRunner Started");
        } catch (Exception e) {
            //Logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Staic main() called by JVM from command line
     *
     * @param args array of arguments that transfer 
     *   to JobRunner from command line
     */
    public static void main(String[] args) {
        JobRunner jobRunner;

        if (args.length != 0) {
            jobRunner = new JobRunner(args[0]);
            jobRunner.run();
        } else {
            jobRunner = new JobRunner();
            jobRunner.run();
        }

        try {
            Thread.sleep(1000 * 10);
        } catch (Exception e) {
            //Logger.debug("error when sleeping");
        }

        //Logger.debug("close all jobs");
        jobRunner.closeAllJobs();
        //Logger.debug("end closing");
        System.out.println(" main end");
    }
}
