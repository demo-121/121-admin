package com.eab.util.jobrunner;

public class BatchJob {

    private String funcCode ;
    private String className ;
    private String methodName ;
    private String dependFuncCode ;
    private String sucess = "N" ;
    private String errorHandleMethod ;

    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDependFuncCode() {
        return dependFuncCode;
    }

    public void setDependFuncCode(String dependFuncCode) {
        this.dependFuncCode = dependFuncCode;
    }

    public String getSucess() {
        return sucess;
    }

    public void setSucess(String sucess) {
        this.sucess = sucess;
    }

    public String getErrorHandleMethod() {
        return errorHandleMethod;
    }

    public void setErrorHandleMethod(String errorHandleMethod) {
        this.errorHandleMethod = errorHandleMethod;
    }
}
