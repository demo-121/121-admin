package com.eab.util.jobrunner;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

//import com.eastpro.admin.biz.ManualBatchRunner;

public class BatchJobTimer extends TimerTask
{
    public static String ONCE_A_DAY = "D";
    public static String ONCE_A_MONTH = "M";
    public static String ONCE_A_WEEK = "W";
    public static String BATCH = "BATCH";
    
    public static Boolean SYNC = Boolean.FALSE;

    private String batchGroup ;
    private String compCode = "";
    private String funcCode="";
    private Collection colBatchJob = null ;
    private Object runnableObject=null;
    private String periodType=null;
    private String startTime=null;
    private Timer myTimer=null;

    public BatchJobTimer nextJob = null;


    public BatchJobTimer(String batchgroup)
    {
        ////Logger.info("[" + compCode + "][" + batchGroup + "]" + "===========================new BatchJob()=================");
        batchGroup = batchgroup ;
        colBatchJob = new ArrayList();
    }

    public void AddJob( BatchJob job )
    {
        colBatchJob.add( job ) ;
    }

    public void setNextJob( BatchJobTimer job2)
    {
        nextJob = job2;
    }

    public void startup()
    {
        myTimer=new Timer();
        try
        {
            if(this.getPeriodType()==null)
            {
                throw new Exception("[" + compCode + "][" + batchGroup + "]" +"batch_job_schedule.batch_grp :" + this.batchGroup +" "+"type:"+this.getPeriodType()+" error:periodType is null!");
            }
            if(this.getPeriodType().trim().equalsIgnoreCase( BatchJobTimer.ONCE_A_DAY ))
            {
                setTimerOAD(myTimer);
            }
            if(this.periodType.trim().equalsIgnoreCase( BatchJobTimer.ONCE_A_WEEK ))
            {
                setTimerOAW(myTimer);
            }
            if(this.periodType.trim().equalsIgnoreCase( BatchJobTimer.ONCE_A_MONTH ))
            {
                setTimerOAD(myTimer);
            }
            if(!(this.getPeriodType().trim().equalsIgnoreCase( BatchJobTimer.ONCE_A_DAY ))&&
               !(this.getPeriodType().trim().equalsIgnoreCase( BatchJobTimer.ONCE_A_WEEK ))&&
               !(this.getPeriodType().trim().equalsIgnoreCase( BatchJobTimer.ONCE_A_MONTH )))
            {
                throw new Exception("[" + compCode + "][" + batchGroup + "]" +"batch_job_schedule.batch_grp :" + this.batchGroup +" type:"+this.getPeriodType()+"  error:periodType is not correct!");
            }
        }
        catch(Exception e)
        {
            //Logger.error(e.getMessage());
            e.printStackTrace();
            //Logger.error("[" + compCode + "][" + batchGroup + "]" +"batch_job_schedule.batch_grp :" + this.batchGroup +" "+"type:"+this.getPeriodType()+"  can not be started");
        }
    }

     /**
     *Call the specified method of the specified class described in
     * BATCH_JOB_SCHEDULE
     */
    public void run()
    {
         BatchSync syncRunner = new BatchSync(compCode,periodType,startTime,batchGroup,colBatchJob);
         //syncRunner.runTimer();
         //runTimer();
    }
    private void  runTimer()
    {
        try
        {
            if ( BatchJobTimer.SYNC.booleanValue() )
                wait();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        if ( !BatchJobTimer.SYNC.booleanValue() )
            BatchJobTimer.SYNC = Boolean.TRUE ;
        synchronized(BatchJobTimer.SYNC)
        {
            String classname = "";
            String methodname = "";
            if ( this.periodType.equalsIgnoreCase( BatchJobTimer.ONCE_A_MONTH ))
            {
                try
                {
                    SimpleDateFormat formatter= new SimpleDateFormat ("dd HH:mm:ss");
                    Date dstartTime=formatter.parse(this.getStartTime());
                    if ( dstartTime.getDate() != (new Date()).getDate() )
                    {
                        return ;
                    }
                }
                catch(Exception e)
                {
                    //Logger.error(e.getMessage() );
                    e.printStackTrace();
                }
            }
            try
            {
                //set success flag = 'N'
                Iterator itSetSuccess = colBatchJob.iterator() ;
                while ( itSetSuccess.hasNext() )
                {
                    BatchJob setSuccess = (BatchJob) itSetSuccess.next() ;
                    setSuccess.setSucess("N");
                }

                //run all batchs in batch group
                Iterator itBatchJob = colBatchJob.iterator() ;
                BatchJob currBatchJob = null ;
                boolean isStopGroup = false ;
                int j=0;
                while( itBatchJob.hasNext() && !isStopGroup )
                {
                    currBatchJob = (BatchJob)itBatchJob.next() ;
                    boolean canRun = true ;
                    if ( currBatchJob.getDependFuncCode() != null && !("").equals( currBatchJob.getDependFuncCode() ) )
                    {
                        Iterator it = colBatchJob.iterator() ;
                        while( it.hasNext() )
                        {
                            BatchJob funcJob = (BatchJob)it.next() ;
                            if ( funcJob.getFuncCode().equalsIgnoreCase( currBatchJob.getDependFuncCode() ) )
                                if (funcJob.getSucess().equals("N"))
                                {
                                    canRun = false  ;
                                }
                        }
                    }

                    classname = currBatchJob.getClassName() ;
                    methodname = currBatchJob.getMethodName() ;
                    if ( canRun )
                    {
                        /*BatBatchLogFactory logFactory = new BatBatchLogFactory();
                        BatBatchLog log = null ;*/
                        String seq = "";
                        try
                        {
                            //Logger.info( "[" + compCode + "][" + batchGroup + "]" + ":" + classname + "." + methodname + "() run......" );
                            Class executeClass=Class.forName(classname);
                            this.runnableObject=executeClass.newInstance();

                            Method executeMethod=null;
                            //Call method
                            if(methodname!=null)
                            {
                                Class [] arcs = { Class.forName("java.lang.String") , Class.forName("java.lang.String") };
                                //executeMethod=executeClass.getDeclaredMethod(methodname,arcs);
								executeMethod=executeClass.getMethod(methodname,arcs);						
                            }
                            else
                            {
                                throw new Exception("[" + compCode + "][" + batchGroup + "]:" + "error:methodName is null");
                            }
                            String [] args = {this.compCode,currBatchJob.getFuncCode() };
                            //log when start

                            /*log = new BatBatchLog();

                            DynamicFunction func = new DynamicFunction();
                            seq = func.getNextSeq("BAT_BATCH_LOG",null);

                            log.setBatchNo( Double.parseDouble(seq) );
                            log.setCompCode( compCode );
                            log.setFuncCode( currBatchJob.getFuncCode() );
                            log.setBatchStatus("R");
                            log.setBatchDateSt(new Date());
                            log.setRunBy( BatchJobTimer.BATCH );
                            log.setCreateBy( BatchJobTimer.BATCH );
                            log.setCreateDate( new Date() );
                            logFactory.insert( log );*/
							//ManualBatchRunner.updFuncLog(null,currBatchJob.getFuncCode(),j);
							//ManualBatchRunner.insertFuncLog(null,currBatchJob.getFuncCode(),j);
                            executeMethod.invoke(this.runnableObject,args);
                           // ManualBatchRunner.updFuncLog(null,currBatchJob.getFuncCode(),j++);
//							NumberBean nb = new NumberBean();
//							nb.getNextSeq("BAT_REP_LOG",conn);
//                            ManualBatchRunner.insertFuncLog();

                            /*//log when success-end
                            log.setBatchStatus("C");
                            log.setBatchDateEnd( new Date());
                            log.setUpdBy( BatchJobTimer.BATCH );
                            log.setUpdDate(new Date());
                            logFactory.update( log ) ;*/
                            currBatchJob.setSucess("Y");
                            //Logger.error( "[" + compCode + "][" + batchGroup + "]:" +classname + "." + methodname + "() END======" );
                        }
                        catch(Exception e)
                        {
                            /*//log when fail-end
                            log.setBatchStatus("C");
                            log.setBatchDateEnd( new Date());
                            log.setUpdBy( BatchJobTimer.BATCH );
                            log.setUpdDate(new Date());
                            logFactory.update( log ) ;*/

                            /*BatBatchErrorLogFactory errorFactory = new BatBatchErrorLogFactory();
                            BatBatchErrorLog error = new BatBatchErrorLog();
                            error.setCompCode( compCode );
                            error.setBatchNo( Double.parseDouble(seq) );

                            DynamicFunction func = new DynamicFunction();
                            seq = func.getNextSeq("BAT_BATCH_ERROR_LOG",null);
                            error.setRecNo( Double.parseDouble(seq) );
                            error.setErrInd( currBatchJob.getErrorHandleMethod() );
                            error.setErrMsg( e.getMessage() );
                            error.setErrType("E");
                            error.setCreateBy( BatchJobTimer.BATCH );
                            error.setCreateDate( new Date() );
                            errorFactory.insert( error ) ;*/

                            currBatchJob.setSucess("N");
                            //Logger.error(e.getMessage());
                            e.printStackTrace();
                            //Logger.error( "[" + compCode + "][" + batchGroup + "]:" + classname + "." + methodname + "() job end abnormally" );

                            if ( ("SG").equals( currBatchJob.getErrorHandleMethod()) )
                            {
                                isStopGroup = true ;
                                //Logger.error( "[" + compCode + "][" + batchGroup + "]:" +"ERROR_HANDLING_END is SG: Stop the current group!" );
                            }
                        }
                    }
                    else
                    {
                        //Logger.error( "[" + compCode + "][" + batchGroup + "]:" + classname + "." + methodname + "() job cancel:depended function fail." );
                    }
                }
            }
            catch(Exception e)
            {
                //Logger.error(e.getMessage());
                e.printStackTrace();
            }

            //Logger.info("[" + compCode + "][" + batchGroup + "]" +"******************************************************************************");
            BatchJobTimer.SYNC = Boolean.FALSE;
            if ( nextJob != null )
                nextJob.notify();
        }
    }
    
    //20051019-S Jeff added function for batch job run immediately
	public void runNow(int days) throws Exception
	{
		synchronized(BatchJobTimer.SYNC)
		{
			String classname = "";
			String methodname = "";
			//try
			//{
				//set success flag = 'N'
				Iterator itSetSuccess = colBatchJob.iterator() ;
				while ( itSetSuccess.hasNext() )
				{
					BatchJob setSuccess = (BatchJob) itSetSuccess.next() ;
					setSuccess.setSucess("N");
				}

				//run all batchs in batch group
				Iterator itBatchJob = colBatchJob.iterator() ;
				BatchJob currBatchJob = null ;
				boolean isStopGroup = false ;
				
				while( itBatchJob.hasNext() && !isStopGroup )
				{
					currBatchJob = (BatchJob)itBatchJob.next() ;
					//20070312--zengfanzhong--S
/*					if(!ManualBatchRunner.needRun(null,currBatchJob.getFuncCode(),days))
					{
						System.out.println(currBatchJob.getFuncCode()+" batch job has been completed");
						continue;
					}*/
					//20070312--zengfanzhong--E
						
					boolean canRun = true ;
					if ( currBatchJob.getDependFuncCode() != null && !("").equals( currBatchJob.getDependFuncCode() ) )
					{
						Iterator it = colBatchJob.iterator() ;
						while( it.hasNext() )
						{
							BatchJob funcJob = (BatchJob)it.next() ;
							if ( funcJob.getFuncCode().equalsIgnoreCase( currBatchJob.getDependFuncCode() ) )
								if (funcJob.getSucess().equals("N"))
								{
									canRun = false  ;
								}
								
								
						}
					}

					classname = currBatchJob.getClassName() ;
					methodname = currBatchJob.getMethodName() ;
					if ( canRun )
					{
						/*BatBatchLogFactory logFactory = new BatBatchLogFactory();
						BatBatchLog log = null ;*/
						String seq = "";
						try
						{
							//Logger.info( "[" + compCode + "][" + batchGroup + "]" + ":" + classname + "." + methodname + "() run......" );
							Class executeClass=Class.forName(classname);
							this.runnableObject=executeClass.newInstance();

							Method executeMethod=null;
							//Call method
							if(methodname!=null)
							{
								Class [] arcs = { Class.forName("java.lang.String") , Class.forName("java.lang.String") };
								//executeMethod=executeClass.getDeclaredMethod(methodname,arcs);
								executeMethod=executeClass.getMethod(methodname,arcs);
							}    
							else
							{
								throw new Exception("[" + compCode + "][" + batchGroup + "]:" + "error:methodName is null");
							}
							String [] args = {this.compCode,currBatchJob.getFuncCode() };
							//log when start

							/*log = new BatBatchLog();

							DynamicFunction func = new DynamicFunction();
							seq = func.getNextSeq("BAT_BATCH_LOG",null);

							log.setBatchNo( Double.parseDouble(seq) );
							log.setCompCode( compCode );
							log.setFuncCode( currBatchJob.getFuncCode() );
							log.setBatchStatus("R");
							log.setBatchDateSt(new Date());
							log.setRunBy( BatchJobTimer.BATCH );
							log.setCreateBy( BatchJobTimer.BATCH );
							log.setCreateDate( new Date() );
							logFactory.insert( log );*/
							//ManualBatchRunner.insertFuncLog(null,currBatchJob.getFuncCode(),days);
							executeMethod.invoke(this.runnableObject,args);
							//ManualBatchRunner.updFuncLog(null,currBatchJob.getFuncCode(),days,"C");
							//cnt++;
							/*//log when success-end
							log.setBatchStatus("C");
							log.setBatchDateEnd( new Date());
							log.setUpdBy( BatchJobTimer.BATCH );
							log.setUpdDate(new Date());
							logFactory.update( log ) ;*/
							currBatchJob.setSucess("Y");
							//System.err.println("----xxxxxxxxxxxxxx----");
							//Logger.error( "[" + compCode + "][" + batchGroup + "]:" +classname + "." + methodname + "() END======" );
						}
						catch(Exception e)
						{
							/*//log when fail-end
							log.setBatchStatus("C");
							log.setBatchDateEnd( new Date());
							log.setUpdBy( BatchJobTimer.BATCH );
							log.setUpdDate(new Date());
							logFactory.update( log ) ;*/

							/*BatBatchErrorLogFactory errorFactory = new BatBatchErrorLogFactory();
							BatBatchErrorLog error = new BatBatchErrorLog();
							error.setCompCode( compCode );
							error.setBatchNo( Double.parseDouble(seq) );

							DynamicFunction func = new DynamicFunction();
							seq = func.getNextSeq("BAT_BATCH_ERROR_LOG",null);
							error.setRecNo( Double.parseDouble(seq) );
							error.setErrInd( currBatchJob.getErrorHandleMethod() );
							error.setErrMsg( e.getMessage() );
							error.setErrType("E");
							error.setCreateBy( BatchJobTimer.BATCH );
							error.setCreateDate( new Date() );
							errorFactory.insert( error ) ;*/

							currBatchJob.setSucess("N"); 
							//Logger.error(e.getMessage());
							e.printStackTrace();
							//Logger.error( "[" + compCode + "][" + batchGroup + "]:" + classname + "." + methodname + "() job end abnormally" );

							if ( ("SG").equals( currBatchJob.getErrorHandleMethod()) )
							{
								isStopGroup = true ;
								//Logger.error( "[" + compCode + "][" + batchGroup + "]:" +"ERROR_HANDLING_END is SG: Stop the current group!" );
								//add by cxz 20090209 s ,issue1071
								System.err.println("----Exception occu: break all batch----");
								throw new Exception(e.toString());
								//add by cxz 20090209 e
							}

						}
					}
					else
					{
						//Logger.error( "[" + compCode + "][" + batchGroup + "]:" + classname + "." + methodname + "() job cancel:depended function fail." );
					}
				}
			//}
			//catch(Exception e)
			//{
				////Logger.error(e.getMessage());
				//e.printStackTrace();
				//throw e;
			//}

			//Logger.info("[" + compCode + "][" + batchGroup + "]" +"******************************************************************************");
		}
	}
    //20051019-E Jeff

    private void setTimerOAD(Timer myTimer) throws Exception
    {
        if(this.getStartTime()!=null)
        {
            SimpleDateFormat formatter= new SimpleDateFormat ("dd HH:mm:ss");
            Date dstartTime=formatter.parse(this.getStartTime());
            Date currentDT=new Date(System.currentTimeMillis());
            Calendar calendar=new GregorianCalendar();
            calendar.setTime(currentDT);
            int month=calendar.get(calendar.MONTH);
            int year=calendar.get(calendar.YEAR);
            int day=calendar.get(calendar.DAY_OF_MONTH);
            calendar.setTime(dstartTime);
            calendar.set(calendar.MONTH,month);
            calendar.set(calendar.YEAR,year);
            calendar.set(calendar.DAY_OF_MONTH,day);
            if(currentDT.getTime()>calendar.getTime().getTime())
            {
                calendar.add(calendar.DAY_OF_MONTH,1);
            }
            //Logger.info("[" + compCode + "][" + batchGroup + "]:" +"batch_job_schedule.batch_grp :" + this.batchGroup +" type:"+this.getPeriodType()+" startTime="+calendar.getTime());
            //System.out.println(calendar.getTime());
            myTimer.schedule(this,calendar.getTime(),1000*3600*24);
        }
        else
        {
            throw new Exception("[" + compCode + "][" + batchGroup + "]:" +"batch_job_schedule.batch_grp :" + this.batchGroup +" type:"+this.getPeriodType()+" error:startTime is null");
        }
    }

    private void setTimerOAW(Timer myTimer) throws Exception
    {
        if(this.getStartTime()!=null)
        {
            int weekDay=stringToWeekDay(this.getStartTime().substring(0,this.getStartTime().indexOf(',')));
            //System.out.println(this.getStartTime().substring(0,this.getStartTime().indexOf(',')));
            if(weekDay==0)
            {
                throw new Exception("[" + compCode + "][" + batchGroup + "]:" +"batch_job_schedule.batch_grp :" + this.batchGroup +" type:"+this.getPeriodType()+" error:week day of startTime is not correct");
            }
            SimpleDateFormat formatter= new SimpleDateFormat ("HH:mm:ss");
            Date dstartTime=formatter.parse(this.getStartTime().substring(this.getStartTime().indexOf(',')+1));
            Date currentDT=new Date(System.currentTimeMillis());
            Calendar calendar=new GregorianCalendar();
            calendar.setTime(currentDT);
            int month=calendar.get(calendar.MONTH);
            int year=calendar.get(calendar.YEAR);
            int day=calendar.get(calendar.DAY_OF_MONTH);
            calendar.setTime(dstartTime);
            calendar.set(calendar.MONTH,month);
            calendar.set(calendar.YEAR,year);
            calendar.set(calendar.DAY_OF_MONTH,day);
            if(calendar.get(calendar.DAY_OF_WEEK)==weekDay)
            {
                if(currentDT.getTime()>calendar.getTime().getTime())
                {
                    calendar.add(calendar.DAY_OF_MONTH,7);
                }
            }
            if(calendar.get(calendar.DAY_OF_WEEK)>weekDay)
            {
                calendar.add(calendar.DAY_OF_MONTH,7);
                calendar.set(calendar.DAY_OF_WEEK,weekDay);
            }
            if(calendar.get(calendar.DAY_OF_WEEK)<weekDay)
            {
                calendar.set(calendar.DAY_OF_WEEK,weekDay);
            }
            //Logger.info("[" + compCode + "][" + batchGroup + "]:" +"batch_job_schedule.batch_grp :" + this.batchGroup +" type:"+this.getPeriodType()+" startTime="+calendar.getTime());
            //System.out.println(calendar.getTime());
            myTimer.schedule(this,calendar.getTime(),1000*3600*24*7);
        }
        else
        {
            throw new Exception("[" + compCode + "][" + batchGroup + "]:" +"batch_job_schedule.batch_grp :" + this.batchGroup +" type:"+this.getPeriodType()+" error:startTime is null");
        }
    }

    private int stringToWeekDay(String input)
    {
        if(input.trim().equalsIgnoreCase("SUN"))
        {
            return 1;
        }
        if(input.trim().equalsIgnoreCase("MON"))
        {
            return 2;
        }
        if(input.trim().equalsIgnoreCase("TUE"))
        {
            return 3;
        }
        if(input.trim().equalsIgnoreCase("WED"))
        {
            return 4;
        }
        if(input.trim().equalsIgnoreCase("THU"))
        {
            return 5;
        }
        if(input.trim().equalsIgnoreCase("FRI"))
        {
            return 6;
        }
        if(input.trim().equalsIgnoreCase("SAT"))
        {
            return 7;
        }
        return 0;
    }

    public void addBatchJob(BatchJob batchJob) {
        if (colBatchJob == null)
            colBatchJob =  new ArrayList();
        colBatchJob.add( batchJob );
    }

    public String getBatchGroup() {
        return batchGroup;
    }

    public void setBatchGroup(String batchGroup) {
        this.batchGroup = batchGroup;
    }

    public String getCompCode() {
        return compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public Collection getColBatchJob() {
        return colBatchJob;
    }

    public void setColBatchJob(Collection colBatchJob) {
        this.colBatchJob = colBatchJob;
    }

    public Object getRunnableObject() {
        return runnableObject;
    }

    public void setRunnableObject(Object runnableObject) {
        this.runnableObject = runnableObject;
    }

    public String getPeriodType() {
        return periodType;
    }

    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
	/**
	 * @return
	 */
	public String getFuncCode()
	{
		return funcCode;
	}

	/**
	 * @param string
	 */
	public void setFuncCode(String string)
	{
		funcCode = string;
	}

}
