package com.eab.util.jobrunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.lang.reflect.Method;

public class BatchSync extends Thread {

    class batch
    {
        private String batchGroup = "";
        private String compCode = "";
        private Collection colBatchJob = null ;
        private Object runnableObject=null;
        private String periodType=null;
        private String startTime=null;
    }

    public static Collection BATCH_COLLECTION = new ArrayList();
    public BatchSync()
    {}
    public BatchSync(String compCode , String periodType , String startTime , String batchGroup ,  Collection colBatchJob)
    {
        batch batch1 = new batch();
        batch1.compCode = compCode ;
        batch1.periodType = periodType ;
        batch1.startTime = startTime ;
        batch1.batchGroup = batchGroup ;
        batch1.colBatchJob = colBatchJob ;
        BATCH_COLLECTION.add( batch1 ) ;
    }

    public void run()
    {
        while(true)
        {
            try
            {
                runTimer();
                sleep(1000);
            }
            catch(Exception e)
            {
                //Logger.error(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void  runTimer( )
    {
        synchronized("AAA")
        {
            batch batch1 = null ;
            if ( BatchSync.BATCH_COLLECTION != null && BatchSync.BATCH_COLLECTION.size() > 0 )
            {
                Iterator its =  BatchSync.BATCH_COLLECTION.iterator();
                batch1 = (batch) its.next();

                String classname = "";
                String methodname = "";
                if ( batch1.periodType.equalsIgnoreCase( BatchJobTimer.ONCE_A_MONTH ))
                {
                    try
                    {
                        SimpleDateFormat formatter= new SimpleDateFormat ("dd HH:mm:ss");
                        Date dstartTime=formatter.parse(batch1.startTime);
                        if ( dstartTime.getDate() != (new Date()).getDate() )
                        {
                            return ;
                        }
                    }
                    catch(Exception e)
                    {
                        ////Logger.error(e.getMessage() );
                        e.printStackTrace();
                    }
                }
                try
                {
                    //set success flag = 'N'
                    Iterator itSetSuccess = batch1.colBatchJob.iterator() ;
                    while ( itSetSuccess.hasNext() )
                    {
                        BatchJob setSuccess = (BatchJob) itSetSuccess.next() ;
                        setSuccess.setSucess("N");
                    }

                    //run all batchs in batch group
                    Iterator itBatchJob = batch1.colBatchJob.iterator() ;
                    BatchJob currBatchJob = null ;
                    boolean isStopGroup = false ;
                    while( itBatchJob.hasNext() && !isStopGroup )
                    {
                        currBatchJob = (BatchJob)itBatchJob.next() ;
                        boolean canRun = true ;
                        if ( currBatchJob.getDependFuncCode() != null && !("").equals( currBatchJob.getDependFuncCode() ) )
                        {
                            Iterator it = batch1.colBatchJob.iterator() ;
                            while( it.hasNext() )
                            {
                                BatchJob funcJob = (BatchJob)it.next() ;
                                if ( funcJob.getFuncCode().equalsIgnoreCase( currBatchJob.getDependFuncCode() ) )
                                    if (funcJob.getSucess().equals("N"))
                                    {
                                        canRun = false  ;
                                    }
                            }
                        }

                        classname = currBatchJob.getClassName() ;
                        methodname = currBatchJob.getMethodName() ;
                        if ( canRun )
                        {
                            /*BatBatchLogFactory logFactory = new BatBatchLogFactory();
                            BatBatchLog log = null ;*/
                            //String seq = "";
                            try
                            {
                                //Logger.info( "[" + batch1.compCode + "][" + batch1.batchGroup + "]" + ":" + classname + "." + methodname + "() run......" );
                                Class executeClass=Class.forName(classname);
                                batch1.runnableObject=executeClass.newInstance();

                                Method executeMethod=null;
                                //Call method
                                if(methodname!=null)
                                {
                                    Class [] arcs = { Class.forName("java.lang.String") , Class.forName("java.lang.String") };
                                    //executeMethod=executeClass.getDeclaredMethod(methodname,arcs);
									executeMethod=executeClass.getMethod(methodname,arcs); 
                                }
                                else
                                {
                                    throw new Exception("[" + batch1.compCode + "][" + batch1.batchGroup + "]:" + "error:methodName is null");
                                }
                                String [] args = {batch1.compCode,currBatchJob.getFuncCode() };

                                executeMethod.invoke(batch1.runnableObject,args);

                                currBatchJob.setSucess("Y");
                                //Logger.error( "[" + batch1.compCode + "][" + batch1.batchGroup + "]:" +classname + "." + methodname + "() END======" );
                            }
                            catch(Exception e)
                            {
                                currBatchJob.setSucess("N");
                                //Logger.error(e.getMessage());
                                e.printStackTrace();
                                //Logger.error( "[" + batch1.compCode + "][" + batch1.batchGroup + "]:" + classname + "." + methodname + "() job end abnormally" );

                                if ( ("SG").equals( currBatchJob.getErrorHandleMethod()) )
                                {
                                    isStopGroup = true ;
                                    //Logger.error( "[" + batch1.compCode + "][" + batch1.batchGroup + "]:" +"ERROR_HANDLING_END is SG: Stop the current group!" );
                                }
                            }
                        }
                        else
                        {
                            //Logger.error( "[" + batch1.compCode + "][" + batch1.batchGroup + "]:" + classname + "." + methodname + "() job cancel:depended function fail." );
                        }
                    }
                }
                catch(Exception e)
                {
                    //Logger.error(e.getMessage());
                    e.printStackTrace();
                }

                //Logger.info("[" + batch1.compCode + "][" + batch1.batchGroup + "]" +"******************************************************************************");
                BatchSync.BATCH_COLLECTION.remove( batch1 );
            }
        }
    }

}
