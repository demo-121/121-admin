/* This source has been formatted by an unregistered SourceFormatX */
/* If you want to remove this info, please register this shareware */
/* Please visit http://www.textrush.com to get more information    */
/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 2002-4-30
 * Time: 14:03:07
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.eab.util.jobrunner;

//import java.io.OutputStream;
//import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * JobRunnerServlet
 */
public class JobRunnerServlet extends HttpServlet {
    /**
     * jobRunner
     */
    private JobRunner runner;

    /**
     *
     * @throws ServletException err
     */
    public void init() throws ServletException {
        /*ServletContext context=this.getServletContext();
           String path=context.getRealPath("."+File.separator);
           System.out.println(path);
           String[] args=new String[1];
           args[0]=path.substring(0,path.lastIndexOf(File.separator));
           System.out.println(args[0]);
           JobRunner.main(args);*/

	        ServletContext context = this.getServletContext();
	        String path = context.getRealPath("." + File.separator);
	        System.out.println("path=..........................." + path);
	        runner = new JobRunner(path + "/WEB-INF");
	        //runner=new JobRunner();
	        runner.run();
		
    }

    /**
     *
     * @param req HttpServletRequest
     * @param res HttpServletResponse
     * @throws IOException io err
     * @throws ServletException servlet exp
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException {
        //runner.closeAllJobs();
        //runner.main(null);
        //ServletContext context=this.getServletContext();
        //res.setContentType("text/plain");
        //PrintWriter op=res.getWriter();
        //op.print("<h1>JobRunnerServlet initializing again</h1>");
        //op.print(context.getRealPath(File.separator));
    }

    /**
     *
     * @param req HttpServletRequest
     * @param res HttpServletResponse
     * @throws IOException io err
     * @throws ServletException servlet exp
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException {
        this.doPost(req, res);
    }

    /**
     * destroy
     */
    public void destroy() {
        runner.closeAllJobs();
    }
}
