package com.eab.database.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.eab.common.Log;

public class DBAccess {

	protected java.sql.Connection conn = null;
	static protected javax.sql.DataSource datasource = null;
	static protected javax.sql.DataSource datasourceApi = null;
	
	static public void setDatasource(javax.sql.DataSource ds) {
		if (ds == null) {
			Log.info("[DBAccess] Datasrouce is empty.  Please check.");
		}
		datasource = ds;
	}
	
	static public Connection getConnection() throws SQLException {
		return datasource.getConnection();
	}

	static public void setDatasourceApi(javax.sql.DataSource ds) {
		if (ds == null) {
			Log.info("[DBAccess] DatasrouceApi is empty.  Please check.");
		}
		datasourceApi = ds;
	}

	static public Connection getConnectionApi() throws SQLException {
		return datasourceApi.getConnection();
	}

}
