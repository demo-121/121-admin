package com.eab.database.jdbc;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.sql.Date;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.sql.rowset.serial.SerialBlob;

import org.json.JSONArray;
import org.json.JSONObject;

import com.couchbase.client.deps.io.netty.handler.codec.http.HttpContentEncoder.Result;
import com.eab.common.Log;

public class DBManager {
	private List<Object> dataObj;
	private List<Integer> dataType;
	private List<Map<String, Object>> callList;
	private PreparedStatement stmt = null;
	private boolean AUTO_COMMMIT = false;
	private CallableStatement stmtCall = null;
	
	private List<Map<String, String>> colList;
	
	/***
	 * DB Manager Initialization
	 */
	public DBManager() {
		AUTO_COMMMIT = true;
		dataObj = new ArrayList<>();
		dataType = new ArrayList<>();
		callList = new ArrayList<>();
		colList = new ArrayList<>();
		stmt = null;
		stmtCall = null;
	}
	
//	public DBManager(boolean autoCommit) {
//		AUTO_COMMMIT = autoCommit;
//		dataObj = new ArrayList<>();
//		dataType = new ArrayList<>();
//		stmt = null;
//	}
	
//	public void destroy() {
//		try {
//			if (conn != null && !conn.isClosed()) {
//				conn.close();
//			}
//		} catch(SQLException e) {
//			///TODO
//		}
//		conn = null;
//	}
	
	/***
	 * Remove all parameters being set
	 */
	public void clear() {
		dataObj = null;
		dataType = null;
		callList = null;
		
		dataObj = new ArrayList<>();
		dataType = new ArrayList<>();
		callList = new ArrayList<>();
		stmt = null;
		stmtCall = null;
	}
	
	public void clearColList(){
		colList = new ArrayList<>();
		
	}
//	public void commit() throws SQLException{
//		if (conn != null)
//			conn.commit();
//	}
//	
//	public void rollback() throws SQLException{
//		if (conn != null)
//			conn.rollback();
//	}
//	
//	public void close() throws SQLException{
//		if (conn != null && !conn.isClosed())
//			conn.close();
//	}
	
		
	/***
	 * Set Parameter Value
	 * @param value - String or byte[]
	 * @param type - Support Text, Int, Double, Clob, and Blob(byte[])
	 * @param ioType - input or ouput
	 * @return ?
	 * @throws Exception
	 */
	public String param(Object value, int type, int ioType) throws Exception {
		boolean hasError = false;
		//Log.debug("para: " + value + " - " + type);
		if(ioType == IOType.IN){
			if (value != null) {
				Map<String, Object> data = new HashMap<String, Object>();
				if (type == DataType.TEXT || type == DataType.INT || type == DataType.DOUBLE || type == DataType.CLOB
						|| type == DataType.BLOB || type == DataType.DATE) {
					data.put("value", value);
					data.put("type", type);
					data.put("ioType", ioType);
					callList.add(data);
				} else {
					hasError = true;
					Log.debug("Invalid DataType=" + type);
				}
			} else {
				hasError = true;
			}
		}else if(ioType == IOType.OUT){
			Map<String, Object> data = new HashMap<String, Object>();
			if (type == DataType.TEXT || type == DataType.INT || type == DataType.DOUBLE || type == DataType.CLOB
					|| type == DataType.BLOB || type == DataType.DATE) {
				data.put("type", type);
				data.put("ioType", ioType);
				callList.add(data);
			}
		}
		
		if (hasError)
			throw new Exception("DBM00001");		//DBM00001: Parameter Type Not Support
		
		return "?";
	}
	
	/***
	 * Set call Parameter Value
	 * @param value - String or byte[]
	 * @param type - blob, clob
	 * @return ?
	 * @throws Exception
	 */
	public String param(Object value, int type) throws Exception {
		boolean hasError = false;
		if(value.toString().length() < 1000)
			Log.debug("para: " + value + " - " + type);
		if (value != null) {
			if (type == DataType.TEXT) {
				dataObj.add(value);
				dataType.add(type);
			} else if (type == DataType.INT) {
				dataObj.add(value);
				dataType.add(type);
			} else if (type == DataType.DOUBLE) {
				dataObj.add(value);
				dataType.add(type);
			} else if (type == DataType.CLOB) {
				dataObj.add(value);
				dataType.add(type);
			} else if (type == DataType.BLOB) {
				dataObj.add(value);
				dataType.add(type);
			} else if (type == DataType.DATE) {
				dataObj.add(value);
				dataType.add(type);
			} else if (type == DataType.BIG_DECIMAL){
				dataObj.add(value);
				dataType.add(type);
			} else if (type == DataType.TIMESTAMP){
				dataObj.add(value);
				dataType.add(type);
			}else {
				hasError = true;
				Log.debug("Invalid DataType=" + type);
			}
		} else {
			hasError = true;
		}
		
		if (hasError)
			throw new Exception("DBM00001");		//DBM00001: Parameter Type Not Support
		
		return "?";
	}
	
	private void ParamSQL(Connection conn, Integer NumOfQuestionmask) throws SQLException {
		Integer dataObjSize = dataObj.size();
		if(dataObj != null && dataObjSize > 0) {
			for (int i=0; i< NumOfQuestionmask; i++) {
				Integer index = i;
				if(dataObjSize < NumOfQuestionmask) {
					index = dataObjSize - 1;
				}
				if(dataType.get(index) == DataType.TEXT) {
					stmt.setString(i + 1, (String) dataObj.get(index));
				}
			}
		}
	}
	
	/***
	 * Database Query - SELECT statement
	 * @param sql - SELECT query
	 * @param dbconn - Database Connection
	 * @return ResultSet
	 * @throws SQLException
	 */
	public List<ResultSet> selectMultipleQuery(List<String> sql, Connection dbconn) throws SQLException {
		stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		boolean useOwnConn = false;
		List<ResultSet> result = new ArrayList<ResultSet>();
		try {
			if (dbconn != null)
				conn = dbconn;
			
			if (conn == null) {
				conn = DBAccess.getConnection();
				conn.setAutoCommit(AUTO_COMMMIT);
				useOwnConn = true;
			}
			
			
			for(String stat : sql) {
				
				if(stat != null && !stat.isEmpty()) {
					stmt = conn.prepareStatement(stat, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
					
					// Log SQL (for debug only)
//					LogSQL(stat);
					
					// Set Parameter(s)
					Pattern p = Pattern.compile("\\?");
					Matcher m = p.matcher(stat);
					int NumOfQuestionmask = 0;
					while (m.find()) {
						NumOfQuestionmask++;
					}
					ParamSQL(conn, NumOfQuestionmask);
					// Execution
					rs = stmt.executeQuery();
					result.add(rs);
				}
				
			}
			
			
		} catch(SQLException e) {
			Log.error(e);
		} catch(Exception e) {
			Log.error(e);
		} finally {
			if (stmt != null) 
				stmt = null;
			if (useOwnConn && conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
//		return rs;
		return result;
	}
	
	/***
	 * Database Query - SELECT statement
	 * @param sql - SELECT query
	 * @param dbconn - Database Connection
	 * @return ResultSet
	 * @throws SQLException
	 */
	public ResultSet select(String sql, Connection dbconn) throws SQLException {
		stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		boolean useOwnConn = false;
		
		try {
			if (dbconn != null)
				conn = dbconn;
			
			if (conn == null) {
				conn = DBAccess.getConnection();
				conn.setAutoCommit(AUTO_COMMMIT);
				useOwnConn = true;
			}
			stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			
			// Log SQL (for debug only)
			LogSQL(sql);

			// Set Parameter(s)
				ParamSQL(conn);
			
			
			// Execution
			rs = stmt.executeQuery();
			
		} catch(SQLException e) {
			Log.error(e);
		} catch(Exception e) {
			Log.error(e);
		} finally {
			if (stmt != null) 
				stmt = null;
			if (useOwnConn && conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
		return rs;
	}
	
	/***
	 * Database Query - SELECT statement with single column
	 * @param sql - SELECT query
	 * @param colName - Column Name in SELECT statement
	 * @return SQL Output
	 * @throws SQLException
	 */
	public String selectSingle(String sql, String colName) throws SQLException {
		return selectSingle(sql, colName, null);
	}
	
	/***
	 * Database Query - SELECT statement with single column
	 * @param sql - SELECT query
	 * @param colName - Column Name in SELECT statement
	 * @param dbconn - Database Connection
	 * @return SQL Output
	 * @throws SQLException
	 */
	public String selectSingle(String sql, String colName, Connection dbconn) throws SQLException {
		stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		boolean useOwnConn = false;
		String result = null;
		
		try {
			if (dbconn != null)
				conn = dbconn;
			
			if (conn == null) {
				conn = DBAccess.getConnection();
				conn.setAutoCommit(AUTO_COMMMIT);
				useOwnConn = true;
			}
			stmt = conn.prepareStatement(sql);
			
			// Log SQL (for debug only)
			LogSQL(sql);
			
			// Set Parameter(s)
			ParamSQL(conn);
			
			// Execution
			rs = stmt.executeQuery();
			
			if (rs != null) {
				if (rs.next()) {	// Get first one and specified only
					if (colName != null)
						result = rs.getString(colName);
					else
						result = rs.getString(1);
				}
			}
			
		} catch(SQLException e) {
			Log.error(e);
		} catch(Exception e) {
			Log.error(e);
		} finally {
			if (rs != null) 
				rs = null;
			if (stmt != null) 
				stmt = null;
			if (useOwnConn && conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
		return result;
	}
	
	/***
	 * Database Query - INSERT statement
	 * @param sql - INSERT query
	 * @return boolean
	 * @throws SQLException
	 */
	public boolean insert(String sql) throws SQLException {
		return insert(sql, null);
	}
	
	public boolean insert(String sql, Connection dbconn) throws SQLException {
		boolean success = false;
		stmt = null;
		Connection conn = null;
		boolean useOwnConn = false;
		
		try {
			if (dbconn != null)
				conn = dbconn;
			
			if (conn == null) {
				conn = DBAccess.getConnection();
				conn.setAutoCommit(AUTO_COMMMIT);
				useOwnConn = true;
			}
			stmt = conn.prepareStatement(sql);
			
			// Log SQL (for debug only)
			LogSQL(sql);
			
			// Set Parameter(s)
			ParamSQL(conn);
			
			// Execution
			stmt.executeQuery();
			
			success = true;
		} catch(SQLException e) {
			Log.error(e);
		} catch(Exception e) {
			Log.error(e);
		} finally {
			if (stmt != null) 
				stmt = null;
			if (useOwnConn && conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
		return success;
	}
	
	/***
	 * Database Query - UPDATE statement
	 * @param sql - UPDATE query
	 * @return boolean
	 * @throws SQLException
	 */
	public int update(String sql) throws SQLException {
		return update(sql, null);
	}
	
	/***
	 * Database Query - UPDATE statement
	 * @param sql - UPDATE query
	 * @param dbconn - Database Connection
	 * @return boolean
	 * @throws SQLException
	 */
	public int update(String sql, Connection dbconn) throws SQLException {
		int rowAffect = -1;
		stmt = null;
		Connection conn = null;
		boolean useOwnConn = false;
		
		try {
			if (dbconn != null)
				conn = dbconn;
			
			if (conn == null) {
				conn = DBAccess.getConnection();
				conn.setAutoCommit(AUTO_COMMMIT);
				useOwnConn = true;
			}
			stmt = conn.prepareStatement(sql);
			
			// Log SQL (for debug only)
			LogSQL(sql);
			
			// Set Parameter(s)
			ParamSQL(conn);
			
			// Execution
			rowAffect = stmt.executeUpdate();
			
		} catch(SQLException e) {
			Log.error(e);
			rowAffect = -1;
		} catch(Exception e) {
			Log.error(e);
			rowAffect = -1;
		} finally {
			if (stmt != null) 
				stmt = null;
			if (useOwnConn && conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
		return rowAffect;
	}
	
	/***
	 * Database Query - DELETE statement
	 * @param sql - DELETE query
	 * @return boolean
	 * @throws SQLException
	 */
	public int delete(String sql) throws SQLException {
		return update(sql, null);
	}
	
	/***
	 * Database Query - DELETE statement
	 * @param sql - DELETE query
	 * @param dbconn - Database Connection
	 * @return boolean
	 * @throws SQLException
	 */
	public int delete(String sql, Connection dbconn) throws SQLException {
		return update(sql, dbconn);
	}
	
	private void LogSQL(String sql) {
		try {
			Log.debug(sql);
			
			if (dataObj != null && dataObj.size() > 0) {
				for (int i = 0; i < dataObj.size(); i++) {
					Log.debug("Param[" + i + "]: Type=" + dataType.get(i) + ", Value=" + (dataType.get(i) == DataType.TEXT &&  dataObj.get(i).toString().length() < 1000 ? "" : dataObj.get(i)));
				}
			}
		} catch(Exception e) {
			Log.error(e);
		}
	}
	
	private void ParamSQL(Connection conn) throws SQLException {
		if (dataObj != null && dataObj.size() > 0) {
			for (int i = 0; i < dataObj.size(); i++) {
				int seq = i + 1;
				if (dataType.get(i) == DataType.TEXT) {
					stmt.setString(seq, (String) dataObj.get(i));
				} else if (dataType.get(i) == DataType.INT) {
					stmt.setInt(seq, (int) dataObj.get(i));
				} else if (dataType.get(i) == DataType.DOUBLE) {
					stmt.setDouble(seq, (double) dataObj.get(i));
				} else if (dataType.get(i) == DataType.CLOB) {
					if(dataObj.get(i) instanceof Clob){
						stmt.setClob(seq, (Clob)dataObj.get(i));
					}else{
						Clob myClob = conn.createClob();
						myClob.setString(seq, (String) dataObj.get(i));
						stmt.setClob(seq, myClob);
					}
				} else if (dataType.get(i) == DataType.BLOB) {
					if(dataObj.get(i) instanceof Blob){
						stmt.setBlob(seq, (Blob) dataObj.get(i));
					}else{
						stmt.setBytes(seq, (byte[]) dataObj.get(i));
					}
				} else if (dataType.get(i) == DataType.DATE) {
					stmt.setDate(seq,(Date) dataObj.get(i));
				} else if (dataType.get(i) == DataType.BIG_DECIMAL){
					stmt.setBigDecimal(seq,  (BigDecimal) dataObj.get(i));
				} else if (dataType.get(i) == DataType.TIMESTAMP){
					stmt.setTimestamp(seq, (Timestamp) dataObj.get(i));
				}
			}
		}
	}
	
	private void CallParamSQL(Connection conn) throws SQLException {
		if (callList != null && callList.size() > 0) {
			for (int i = 0; i < callList.size(); i++) {
				Map<String, Object> data = callList.get(i);
				int type = (int) data.get("type");
				int ioType = (int) data.get("ioType");
				int seq = i + 1;
				if (type == DataType.TEXT) {
					if(ioType == IOType.IN){
						stmtCall.setString(seq, (String) data.get("value"));
					}else if(ioType == IOType.OUT){
						stmtCall.registerOutParameter(seq, java.sql.Types.VARCHAR);
					}
				} else if (type == DataType.INT) {
					if(ioType == IOType.IN){
						stmtCall.setInt(seq, (int) data.get("value"));
					}else if(ioType == IOType.OUT){
						stmtCall.registerOutParameter(seq, java.sql.Types.INTEGER);
					}
				} else if (type == DataType.DOUBLE) {
					if(ioType == IOType.IN){
						stmtCall.setDouble(seq, (double) data.get("value"));
					}else if(ioType == IOType.OUT){
						stmtCall.registerOutParameter(seq, java.sql.Types.DOUBLE);
					}
					
				} else if (type == DataType.CLOB) {
					if(ioType == IOType.IN){
						Clob myClob = conn.createClob();
						myClob.setString(seq, (String) data.get("value"));
						stmtCall.setClob(seq, myClob);
					}else if(ioType == IOType.OUT){
						stmtCall.registerOutParameter(seq, java.sql.Types.CLOB);
					}
					
					
				} else if (type == DataType.BLOB) {
					if(ioType == IOType.IN){
						stmtCall.setInt(seq, (int) data.get("value"));
					}else if(ioType == IOType.OUT){
						stmtCall.registerOutParameter(seq, java.sql.Types.INTEGER);
					}
						stmt.setBytes(seq, (byte[]) dataObj.get(i));
				} else if (type == DataType.DATE) {
					if(ioType == IOType.IN){
						stmtCall.setDate(seq, (Date) data.get("value"));
					}else if(ioType == IOType.OUT){
						stmtCall.registerOutParameter(seq, java.sql.Types.DATE);
					}
				}
			}
		}
	}
	
	public List<Object> executeCall(String sql, Connection dbconn) throws SQLException {
		stmt = null;
		Connection conn = null;
		boolean useOwnConn = false;
		
		try {
			if (dbconn != null)
				conn = dbconn;
			
			if (conn == null) {
				conn = DBAccess.getConnection();
				conn.setAutoCommit(AUTO_COMMMIT);
				useOwnConn = true;
			}
			stmtCall = conn.prepareCall(sql);
			
			// Log SQL (for debug only)
			LogSQL(sql);
			
			// Set Parameter(s)
			CallParamSQL(conn);
			
			// Execution
			stmtCall.executeQuery();
			List<Object> result = new ArrayList<Object>();
			for (int i = 0; i < callList.size(); i++) {
				Map<String, Object> data = callList.get(i);
				int ioType = (int) data.get("ioType");
				int seq = i + 1;
				if(ioType == IOType.OUT){
					result.add(stmtCall.getObject(seq));
				}
			}
			return result;
		} catch(SQLException e) {
			Log.error(e);
		} catch(Exception e) {
			Log.error(e);
		} finally {
			if (stmtCall != null) 
				stmtCall = null;
			if (useOwnConn && conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
		return null;
	}
	
	public JSONArray selectRecords(String sql, Connection dbconn) throws SQLException {
		ResultSet rs = select(sql, dbconn);
		if(rs != null){
			//save column into colList
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCnt = rsmd.getColumnCount();
			for(int i=1; i<=columnCnt; i++){
				Map<String, String> column = new HashMap<>();
				column.put("name", rsmd.getColumnName(i));
				column.put("type", rsmd.getColumnTypeName(i));
				this.colList.add(column);
			}
			
			JSONArray result = new JSONArray();
			while(rs.next()){
				JSONObject row = new JSONObject();
				for(int i=1; i<=columnCnt; i++){
					if("CLOB".equals(rsmd.getColumnTypeName(i))){
						row.put(rsmd.getColumnName(i), rs.getString(i));
					}else{
						row.put(rsmd.getColumnName(i), rs.getObject(i));
					}
				}
				result.put(row);
			}
			
			return result;
		}else{
			return null;
		}
	}
	
	public JSONObject selectRecord(String sql, Connection dbconn) throws SQLException {
		ResultSet rs = select(sql, dbconn);
		if(rs != null){
			//save column into colList
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCnt = rsmd.getColumnCount();
			for(int i=1; i<=columnCnt; i++){
				Map<String, String> column = new HashMap<>();
				column.put("name", rsmd.getColumnName(i));
				column.put("type", rsmd.getColumnTypeName(i));
				this.colList.add(column);
			}
			
			
			if(rs.next()){
				JSONObject row = new JSONObject();
				for(int i=1; i<=columnCnt; i++){
					if("CLOB".equals(rsmd.getColumnTypeName(i))){
						row.put(rsmd.getColumnName(i), rs.getString(i));
					}else{
						row.put(rsmd.getColumnName(i), rs.getObject(i));
					}
				}
				return row;
			}
			
		}
		
		return null;
	}
	
	public boolean insertRecord(String tableName, JSONObject record, Connection dbconn) throws SQLException {
		boolean success = false;
		stmt = null;
		Connection conn = null;
		boolean useOwnConn = false;
		
		try {
			if (dbconn != null)
				conn = dbconn;
			
			if (conn == null) {
				conn = DBAccess.getConnection();
				conn.setAutoCommit(AUTO_COMMMIT);
				useOwnConn = true;
			}
			
			clear();
			
			String sql = "INSERT INTO " + tableName + " (";
			
			//filter all column which is not null
			List<Map<String, String>> cols = new ArrayList<>();
			for(Map<String, String> col : colList){
				if(record.has(col.get("name"))){
					cols.add(col);
				}
			}
			
			for(int i=0; i<cols.size(); i++){
				sql += (i!=0?",":"") + cols.get(i).get("name"); 
			}
			sql+=") VALUES (";
			for(int i=0; i<cols.size(); i++){
				Map<String, String> col = cols.get(i);
				sql += (i!=0?",":"") + recordParam(record.get(col.get("name")), col.get("type"));
			}
			sql+=")";
			
			
			stmt = conn.prepareStatement(sql);
			
			// Log SQL (for debug only)
			LogSQL(sql);
			
			// Set Parameter(s)
			ParamSQL(conn);
			
			// Execution
			stmt.executeQuery();
			
			success = true;
		} catch(SQLException e) {
			Log.error(e);
		} catch(Exception e) {
			Log.error(e);
		} finally {
			if (stmt != null) 
				stmt = null;
			if (useOwnConn && conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		
		return success;
	}
	
	private String recordParam(Object data, String colType) throws Exception{
		
		switch(colType.toLowerCase()){
			case "clob":
				return param(data, DataType.CLOB);
			case "number":
				if (data instanceof Integer)
				    return param(data, DataType.INT);
				else if(data instanceof BigDecimal)
					return param(data, DataType.BIG_DECIMAL);
				else
					return param(data, DataType.DOUBLE);
			case "blob":
				return param(data, DataType.BLOB);
			case "date":
				if("sysdate".equals(data.toString().toLowerCase()))
					return "sysdate";
				else if(data instanceof Timestamp)
					return param(data, DataType.TIMESTAMP);
				else
					return param(data, DataType.DATE);
			case "varchar2":
			case "varchar":
			case "char":
				return param(data, DataType.TEXT);
			default:
				return "null";
		}
	}
}
