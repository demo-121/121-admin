package com.eab.database.jdbc;

public class DataType extends Object {
	public final static int TEXT = 1;
	
	public final static int INT = 2;

	public final static int DOUBLE = 3;
	
	public final static int CLOB = 4;
	
	public final static int BLOB = 5;
	
	public final static int DATE = 6;
	
	
	//for record
	public final static int BIG_DECIMAL = 7;
	
	public final static int TIMESTAMP = 8;
}
