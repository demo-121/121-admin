package com.eab.model.releases;

import java.util.ArrayList;
import java.util.Date;

import com.eab.model.tasks.TaskBean;

public class ReleasesBean {
	private String releaseID;
	private String releaseName;
	private String status;
	private Date targetReleaseDate;
	private Date scheduledReleaseDate;
	private String remark;
	private Date createDate;
	private String createBy;
	private Date updateDate;
	private String updateBy;
	private ArrayList<TaskBean> taskList;
	
	public String getReleaseID() {
		return releaseID;
	}
	public void setReleaseID(String releaseID) {
		this.releaseID = releaseID;
	}
	public String getReleaseName() {
		return releaseName;
	}
	public void setReleaseName(String releaseName) {
		this.releaseName = releaseName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getTargetReleaseDate() {
		return targetReleaseDate;
	}
	public void setTargetReleaseDate(Date targetReleaseDate) {
		this.targetReleaseDate = targetReleaseDate;
	}
	public Date getScheduledReleaseDate() {
		return scheduledReleaseDate;
	}
	public void setScheduledReleaseDate(Date scheduledReleaseDate) {
		this.scheduledReleaseDate = scheduledReleaseDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public ArrayList<TaskBean> getTaskList() {
		return taskList;
	}
	public void setTaskList(ArrayList<TaskBean> taskList) {
		this.taskList = taskList;
	}
	
	
}
