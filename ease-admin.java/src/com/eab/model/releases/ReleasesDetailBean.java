package com.eab.model.releases;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.eab.json.model.Dialog;

public class ReleasesDetailBean {

	protected Map<String, Object> detail;
	protected List<Map<String, Object>> list;
	protected int total;
	protected boolean isMore;
	protected String id;
	protected ArrayList<Map<String, Object>> rows;
	protected Dialog unassignDialog;
	protected Dialog reassignDialog;
	protected Dialog suspendDialog;
	protected Dialog editReleaseDialog;
	protected Map<String, Object> text;
	protected String isRecordLocked;

	
	public ReleasesDetailBean(Map<String, Object> detail, List<Map<String, Object>> list, int total, boolean isMore, String id, ArrayList<Map<String, Object>> rows, Dialog unassignDialog, Dialog reassignDialog, Dialog suspendDialog, Dialog editReleaseDialog, Map<String, Object> text, String isRecordLocked) {
		super();
		this.detail = detail;
		this.list = list;
		this.total = total;
		this.isMore = isMore;
		this.id = id;
		this.rows = rows;
		this.unassignDialog = unassignDialog;
		this.reassignDialog = reassignDialog;
		this.suspendDialog = suspendDialog;
		this.editReleaseDialog = editReleaseDialog;
		this.text = text;
		this.isRecordLocked = isRecordLocked;
	}
	
	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	
	public boolean getIsMore() {
		return isMore;
	}

	public void setTotal(boolean isMore) {
		this.isMore = isMore;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public ArrayList<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(ArrayList<Map<String, Object>> rows) {
		this.rows = rows;
	}
	
	public Dialog getUnassignDialog() {
		return unassignDialog;
	}

	public void setUnassignDialog(Dialog unassignDialog) {
		this.unassignDialog = unassignDialog;
	}
	
	public Dialog getReassignDialog() {
		return reassignDialog;
	}

	public void setReassignDialog(Dialog reassignDialog) {
		this.reassignDialog = reassignDialog;
	}
	
	public Dialog getSuspendDialog() {
		return suspendDialog;
	}

	public void setSuspendDialog(Dialog suspendDialog) {
		this.suspendDialog = suspendDialog;
	}
	
	public Dialog getEditReleaseDialog() {
		return editReleaseDialog;
	}

	public void setEditReleaseDialog(Dialog editReleaseDialog) {
		this.editReleaseDialog = editReleaseDialog;
	}
	
	public Map<String, Object> getText() {
		return text;
	}

	public void setText(Map<String, Object> text) {
		this.text = text;
	}
	
	
	
}
