package com.eab.model.needs;

public class NeedsBean {
	private String needCode;
	private String needName;
	private int version;
	private String taskStatus;

	public String getNeedCode() {
		return needCode;
	}

	public void setNeedCode(String needCode) {
		this.needCode = needCode;
	}

	public String getNeedName() {
		return needName;
	}

	public void setNeedName(String needName) {
		this.needName = needName;
	}
	
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
}
