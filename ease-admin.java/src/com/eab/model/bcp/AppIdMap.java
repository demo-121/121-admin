package com.eab.model.bcp;

import java.util.ArrayList;

public class AppIdMap {
	public String appId;
	public ArrayList<String> policyNumbers;

	
	public String toJsonStr(){
		int count = 0;
		StringBuilder sb = new StringBuilder();
	
		sb.append("{\"appId\":\"").append(appId);
		sb.append("\",\"policyNumbers\":[");
		
		for (String cur_entry : policyNumbers){
			if (count > 0){sb.append(",");}
			sb.append("\"").append(cur_entry).append("\"");
			count++;
		}
		
		sb.append("]}");
		return sb.toString();
	}
}
