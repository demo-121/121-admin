package com.eab.model.bcp;

import java.util.ArrayList;

public class BcpPolicyNumbers {
	public ArrayList<String> 	policyNumbers;
	public ArrayList<String> 	appIds;
	public ArrayList<AppIdMap> 	appIdMapList;
	
	public boolean				hasError;
	
	private StringBuilder sbLastError;
	
	
	public BcpPolicyNumbers(){
		sbLastError = new StringBuilder();
		hasError	= false;
	}
	
	public void appendErrorStr(String errorStr){
		sbLastError.append(errorStr);
		hasError	= true;
	}
	
	public String getErrorString(){
		return sbLastError.toString();
	}
}
