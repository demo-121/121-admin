package com.eab.model.bcp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.eab.json.model.Dialog;

public class BcpBean {

	protected List<Map<String, Object>> list;
	protected String result;
	
	public BcpBean(List<Map<String, Object>> list, String result) {
		super();
		this.list = list;
		this.result = result;
	}	
}
