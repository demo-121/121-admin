package com.eab.model;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MenuItem {
	
	private String code = null;
	private String title = null;
	private String id = null;
	private boolean disabled = false;
	private int seq = 0;
	private String module = null;

	private ArrayList<MenuItem> subItems = null;
	
	public MenuItem() {
		
	}
	
	public MenuItem(String id, String code, String title, boolean disabled,
			int seq, String module, ArrayList<MenuItem> subItems) {
		super();
		this.code = code;
		this.title = title;
		this.id = id;
		this.disabled = disabled;
		this.seq = seq;
		this.module = module;
		this.subItems = subItems;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String type) {
		this.module = type;
	}

	public ArrayList<MenuItem> getSubItems() {
		return subItems;
	}
	
	public void setSubItems(ArrayList<MenuItem> subItem) {
		this.subItems = subItem;
	}

	public void createSubItems() {
		subItems = new ArrayList<MenuItem>();
	}
	
	public void removeSubItems() {
		subItems = null;
	}
	
	
	
	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	// parse object to map
	public Map<String, Object> toMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			BeanInfo info = Introspector.getBeanInfo(this.getClass());
			for (PropertyDescriptor pd: info.getPropertyDescriptors()) {
				Method reader = pd.getReadMethod();
				if (reader != null) {
					Object value = reader.invoke(this);
					String name = pd.getName();
					if (!name.equals("class") && !name.equals("code")) {
						if (value instanceof MenuList) {
							MenuList list = (MenuList) value;
							map.put(name, list.toList());
						} else {
							map.put(name, value);
						}
					}
				}
			}
		} catch (IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
			ex.printStackTrace();
		}
		return map;
	}
	
}
