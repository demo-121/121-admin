package com.eab.model.environment;

public class EnvBean {
	
	private String env_id;
	private String host;
	private String port;
	private String db_name;
	private String db_login;
	private String db_password;
		
	public String getEnvId() {
		return env_id;
	}

	public void setEnvId(String env_id) {
		this.env_id = env_id;
	}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDBName() {
		return db_name;
	}

	public void setDBName(String db_name) {
		this.db_name = db_name;
	}
	
	public String getDBLogin() {
		return db_login;
	}

	public void setDBLogin(String db_login) {
		this.db_login = db_login;
	}
	
	public String getDBPassword() {
		return db_password;
	}

	public void setDBPassword(String db_password) {
		this.db_password = db_password;
	}
}
