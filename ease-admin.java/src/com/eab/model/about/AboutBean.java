package com.eab.model.about;

public class AboutBean {
	String logo, title, desc, sysVer, sysName, email, address, tel, fax, website, rightDesc, copyRight;
	public String getLogo(){
		return logo;
	}
	public void setLogo(String logo){
		this.logo = logo;
	}

	public void setTitle(String title){
		this.title = title;
	}
	public String getTitle(){
		return title;
	}
	public void setDesc(String desc){
		this.desc = desc;
	}
	public String getDesc(){
		return desc;
	}
	public void setSysVer(String sysVer){
		this.sysVer = sysVer;
	}
	public String getSysVer(){
		return sysVer;
	}
	public void setSysName(String logo){
		this.logo = logo;
	}
	public String getSysName(){
		return sysName;
	}
	public void setMail(String email){
		this.email = email;
	}
	
	public String getMail(){
		return email;
	}
	public void setAddress(String address){
		this.address = address;
	}
	
	public String getAddress(){
		return address;
	}
	public void setTel(String tel){
		this.tel = tel;
	}
	public String getTel(){
		return tel;
	}
	public void setFax(String fax){
		this.fax = fax;
	}
	public String getFax(){
		return fax;
	}
	public void setWebsite(String website){
		this.website = website;
	}
	public String getWebsite(){
		return website;
	}
	public void setRightDesc(String rightDesc){
		this.rightDesc = rightDesc;
	}
	public String getRightDesc(){
		return rightDesc;
	}
	public void setCopyRight(String copyRight){
		this.copyRight = copyRight;
	}
	public String getCopyRight(){
		return copyRight;
	}
		
}
