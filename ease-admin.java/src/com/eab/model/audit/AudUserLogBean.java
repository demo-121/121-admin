package com.eab.model.audit;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "aud_user_log")
public class AudUserLogBean implements Serializable {
	@Column(name = "USER_CODE")
	private String userCode;
	
	@Column(name = "COMP_CODE")
	private String compCode;

	@Column(name = "LOGIN_TIME")
	private Date loginTime;

	@Column(name = "LOGOUT_TIME")
	private Date logoutTime;

	@Column(name = "SERV_IP_ADDR")
	private String servIpAddress;

	@Column(name = "USER_IP_ADDR")
	private String userIpAddress;

	@Column(name = "SYS_VER")
	private Date sysVer;

	@Column(name = "SUCCESS")
	private boolean success;

	@Column(name = "FAIL_REASON")
	private Date failReason;

	@Column(name = "SOURCE_LOGIN")
	private Date sourceLogin;

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getCompCode() {
		return compCode;
	}

	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}

	public Date getSysVer() {
		return sysVer;
	}

	public void setSysVer(Date sysVer) {
		this.sysVer = sysVer;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Date getFailReason() {
		return failReason;
	}

	public void setFailReason(Date failReason) {
		this.failReason = failReason;
	}

	public Date getSourceLogin() {
		return sourceLogin;
	}

	public void setSourceLogin(Date sourceLogin) {
		this.sourceLogin = sourceLogin;
	}

	public String getServIpAddress() {
		return servIpAddress;
	}

	public void setServIpAddress(String servIpAddress) {
		this.servIpAddress = servIpAddress;
	}

	public String getUserIpAddress() {
		return userIpAddress;
	}

	public void setUserIpAddress(String userIpAddress) {
		this.userIpAddress = userIpAddress;
	}
}
