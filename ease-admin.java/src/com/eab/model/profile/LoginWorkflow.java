package com.eab.model.profile;

import java.util.HashMap;
import java.util.Map.Entry;

import com.eab.common.Log;

public class LoginWorkflow {
	private String[] seqflow;
	private HashMap<String, String> statusflow;
	private HashMap<String, String> nextflow;
	private boolean isComplete = false;
	
	public LoginWorkflow() {
		if (seqflow == null || statusflow == null || nextflow == null) {
			seqflow = new String[2];
			statusflow = new HashMap<String, String>();
			nextflow = new HashMap<String, String>();
			
			// Pre-define Login workflow items.  All items should be marked "Y".
			seqflow[0] = "LOGIN";						// #1: Login
			statusflow.put("LOGIN", "N");
			nextflow.put("LOGIN", "/Login");

			seqflow[1] = "TERMS";						// #2: Terms & Conditions
			statusflow.put("TERMS", "N");
			nextflow.put("TERMS", "/Login/Terms");
		}
	}
	
	private String finalFlow() {
		// Final destination after all Login workflow is completed.
		return "/Home";
	}
	
	public boolean isLoginReady() {
		return isComplete;
	}
	
	public String nextFlow() {
		String nextstep = null;
		
		try {
			// Check login workflow
			for (int i=0; i<seqflow.length; i++) {
				String key = seqflow[i];
				String status = statusflow.get(key);
				String value = nextflow.get(key);
				
				Log.debug("{Login Nextflow} Key: " + key + ", Status: " + status + ", Value: " + value);
				
				if ("N".equals(status)) {
					nextstep = value;
					Log.debug("{Login Nextflow} Key: " + key + ", Next: " + nextstep);
					break;
				}
			}
			
			if (nextstep == null) {
				isComplete = true;
				nextstep = finalFlow();
			}
		} catch(Exception e) {
			Log.error(e);
			nextstep = "/Error";		//Go to Error page if any exception
		}
		
		return nextstep;		// Return null if no more login steps to be completed.
	}
	
	public void setLogin(boolean value) {
		statusflow.put("LOGIN", (value)?"Y":"N");
		nextFlow();  // Force check <isComplete>
	}
	
//	public boolean getLogin() {
//		return "Y".equalsIgnoreCase(this.statusflow.get("LOGIN"))?true:false;
//	}

	public void setTerms(boolean value) {
		statusflow.put("TERMS", (value)?"Y":"N");
		nextFlow();  // Force check <isComplete>
	}
}
