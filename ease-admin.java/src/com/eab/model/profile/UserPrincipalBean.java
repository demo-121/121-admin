package com.eab.model.profile;

import java.util.List;
import java.util.Map;

public class UserPrincipalBean {
	private String userCode;

	private String compCode;
	
	private boolean regional;

	private String userName;

	private String status;

	private LoginWorkflow workflow = new LoginWorkflow();

	private String userType;

	private String posCode;

	private String depCode;

	private String langCode;

	private String roleCode;

	public List<String> channelList;

	private String dateFormat;

	private String timeFormat;

	private Map<String, Object> nameMap;

	private String timezone;

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getCompCode() {
		return compCode;
	}

	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LoginWorkflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(LoginWorkflow workflow) {
		this.workflow = workflow;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getPosCode() {
		return posCode;
	}

	public void setPosCode(String posCode) {
		this.posCode = posCode;
	}

	public String getDepCode() {
		return depCode;
	}

	public void setDepCode(String depCode) {
		this.depCode = depCode;
	}

	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public List<String> getChannelList() {
		return channelList;
	}

	public void setChannelList(List<String> channelList) {
		this.channelList = channelList;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public Map<String, Object> getUITranslation() {
		return nameMap;
	}

	public void setUITranslation(Map<String, Object> nameMap) {
		this.nameMap = nameMap;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String paramTimeZone) {
		this.timezone = paramTimeZone;
	}

	public String getTimeFormat() {
		return timeFormat;
	}

	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}

	public boolean isRegional() {
		return regional;
	}

	public void setRegional(boolean regional) {
		this.regional = regional;
	}

}
