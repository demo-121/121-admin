package com.eab.model;

import java.util.ArrayList;
import java.util.List;

public class MenuList {
	
	private ArrayList<MenuItem> menu = null;
	
	public MenuList() {
		menu = new ArrayList<MenuItem>();
	}
	
	public void put(MenuItem item) {
		menu.add(item);
	}
	
	public MenuItem get(int index) {
		return menu.get(index);
	}
	
	public int size() {
		return menu.size();
	}
	
	public ArrayList<MenuItem> getList(){
		return menu;
	}
	
	
	// parse the object to list
	public List<Object> toList() {
		List<Object> list = new ArrayList<Object>();
		for(MenuItem item : menu) {
			list.add(item.toMap());
		}
		return list;
	}
}
