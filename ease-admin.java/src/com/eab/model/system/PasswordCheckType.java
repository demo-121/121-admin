package com.eab.model.system;

public class PasswordCheckType extends Object {
	final static public int Valid = 0;
	
	final static public int Invalid_Other = 999;
	
	final static public int Invalid_Length = 1;
	
	final static public int Invalid_Contain_Alpha = 2;
	
	final static public int Invalid_Contain_Num = 3;
	
	final static public int Invalid_Contain_AlphaNum = 4;
	
	final static public int Invalid_History = 5;
	
	final static public int Invalid_UserCode_Pattern = 6;
			
	
}
