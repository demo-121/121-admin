package com.eab.model.system;

public class FuncBean {
	private String code;
	private String name;
	private String url;
	private boolean isShowMenu;
	private int accessCtrl;		/* 1: Public, 2: Login In Progress, 3: After Login */
	private int displaySeq;
	private String upline;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public boolean isShowMenu() {
		return isShowMenu;
	}
	public void setShowMenu(boolean isShowMenu) {
		this.isShowMenu = isShowMenu;
	}
	public String getUpline() {
		return upline;
	}
	public void setUpline(String upline) {
		this.upline = upline;
	}
	public int getDisplaySeq() {
		return displaySeq;
	}
	public void setDisplaySeq(int displaySeq) {
		this.displaySeq = displaySeq;
	}
	public int getAccessCtrl() {
		return accessCtrl;
	}
	public void setAccessCtrl(int accessCtrl) {
		this.accessCtrl = accessCtrl;
	}
	
}
