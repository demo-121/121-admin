package com.eab.model.products;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

public class ProductBean implements Serializable {
	private String prodCode;
	private String prodName;
	private String prodLine;
	private int version;
	private String launchStatus;
	private String taskStatus;
	private String status;
	private String id;
	private Boolean isClone;
	private Boolean isNewVersion;

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdLine() {
		return prodLine;
	}

	public void setProdLine(String prodLine) {
		this.prodLine = prodLine;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getLaunchStatus() {
		return launchStatus;
	}

	public void setLaunchStatus(String launchStatus) {
		this.launchStatus = launchStatus;
	}

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getIsClone() {
		return isClone;
	}

	public void setIsClone(Boolean isClone) {
		this.isClone = isClone;
	}

	public Boolean getIsNewVersion() {
		return isNewVersion;
	}

	public void setIsNewVersion(Boolean isNewVersion) {
		this.isNewVersion = isNewVersion;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
	
	

}
