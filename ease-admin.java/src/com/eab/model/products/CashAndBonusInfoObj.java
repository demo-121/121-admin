package com.eab.model.products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 15-7-16.
 */
public class CashAndBonusInfoObj implements Serializable {
    private String type;
    private List<String> dealerGroup;
    private String compCode;
    private String covCode;
    private String planInd;
    private int issAge;
    private String gender;
    private String smoke;
    private List<String> ccy;
    private String payMode;
    private int premTerm;
    private int benefitTerm;
    private int refundAge;
    private List<CashAndBonusRateValueObj> cvRate;
    private List<CashAndBonusRateValueObj> bonusRate;

    public CashAndBonusInfoObj(String type, List<String> dealerGroup2, String compCode, String covCode,String planInd,
    		int issAge,String gender,String smoke,List<String> ccy2,String payMode, int premTerm,Integer benefitTerm,int refundAge) {
        this.type = type;
        this.dealerGroup = dealerGroup2;
        this.compCode = compCode;
        this.covCode = covCode;
        this.planInd = planInd;
        this.issAge = issAge;
        this.gender = gender;
        this.smoke = smoke;
        this.ccy = ccy2;
        this.payMode = payMode;
        this.premTerm = premTerm;
        this.benefitTerm = benefitTerm;
        this.refundAge = refundAge;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getDealerGroup() {
        return dealerGroup;
    }

    public void setDealerGroup(ArrayList<String> dealerGroup) {
        this.dealerGroup = dealerGroup;
    }

    public String getCompCode() {
        return compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getCovCode() {
        return covCode;
    }

    public void setCovCode(String covCode) {
        this.covCode = covCode;
    }

    public String getPlanInd() {
        return planInd;
    }

    public void setPlanInd(String planInd) {
        this.planInd = planInd;
    }

    public Integer getIssAge() {
        return issAge;
    }

    public void setIssAge(Integer issAge) {
        this.issAge = issAge;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSmoke() {
        return smoke;
    }

    public void setSmoke(String smoke) {
        this.smoke = smoke;
    }

    public List<String> getCcy() {
        return ccy;
    }

    public void setCcy(ArrayList<String> ccy) {
        this.ccy = ccy;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public Integer getPremTerm() {
        return premTerm;
    }

    public void setPremTerm(Integer premTerm) {
        this.premTerm = premTerm;
    }

    public Integer getBenefitTerm() {
        return benefitTerm;
    }

    public void setBenefitTerm(Integer benefitTerm) {
        this.benefitTerm = benefitTerm;
    }

    public Integer getRefundAge() {
        return refundAge;
    }

    public void setRefundAge(Integer refundAge) {
        this.refundAge = refundAge;
    }

    public List<CashAndBonusRateValueObj> getCvRate() {
        return cvRate;
    }

    public void setCvRate(ArrayList<CashAndBonusRateValueObj> cvRate) {
        this.cvRate = cvRate;
    }

    public List<CashAndBonusRateValueObj> getBonusRate() {
        return bonusRate;
    }

    public void setBonusRate(ArrayList<CashAndBonusRateValueObj> bonusRate) {
        this.bonusRate = bonusRate;
    }
}
