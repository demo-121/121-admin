package com.eab.model.products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class ProductDetailBean implements Serializable {
	private String prodLine;
	private String covCode;
	private int version;
	private Map<String, Object> covName;
	private Date expDate;
	private Date launchDate;
	private Map<String, Object> channels;
	public String getProdLine() {
		return prodLine;
	}
	public void setProdLine(String prodLine) {
		this.prodLine = prodLine;
	}
	public String getCovCode() {
		return covCode;
	}
	public void setCovCode(String covCode) {
		this.covCode = covCode;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public Map<String, Object> getCovName() {
		return covName;
	}
	public void setCovName(Map<String, Object> covName) {
		this.covName = covName;
	}
	public Date getExpDate() {
		return expDate;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	public Date getLaunchDate() {
		return launchDate;
	}
	public void setLaunchDate(Date launchDate) {
		this.launchDate = launchDate;
	}
	public Map<String, Object> getChannels() {
		return channels;
	}
	public void setChannels(Map<String, Object> channels) {
		this.channels = channels;
	}
	
	
}
