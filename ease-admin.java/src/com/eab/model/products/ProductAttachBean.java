package com.eab.model.products;

import java.sql.Blob;
import java.sql.Date;

public class ProductAttachBean {

	Blob attachFile;
	String attachCode;
	String compCode, prodCode, langCode, status;
	Date createDate, updateDate;
	String createBy, updateBy;
	
	int version;
	
	public void setCompCode(String compCode){
		this.compCode = compCode;
	}
	public String getCompCode(){
		return compCode;
	}
	
	public void setProdCode(String prodCode){
		this.prodCode = prodCode;
	}
	public String getProdCode(){
		return prodCode;
	}
	
	public void setLangCode(String langCode){
		this.langCode = langCode;
	}
	public String getLangCode(){
		return langCode;
	}
	
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	public Date getCreateDate(){
		return createDate;
	}
	
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	public Date getUpdateDate(){
		return updateDate;
	}
	
	public void setCreateBy(String createBy){
		this.createBy = createBy;
	}
	public String getCreateBy(){
		return createBy;
	}
	
	public void setUpdateBy(String updateBy){
		this.updateBy = updateBy;
	}
	public String getUpdateBy(){
		return updateBy;
	}
	
	public void setVersion(int version){
		this.version = version;
	}
	public int getVersion(){
		return version;
	}
	
	public void setAttachFile(Blob attachFile){
		this.attachFile = attachFile;
	}
	public Blob getAttachFile(){
		return attachFile;
	}
	
	public void setAttachCode(String attachCode){
		this.attachCode = attachCode;
	}
	
	public String getAttachCode(){
		return attachCode;
	}
}
