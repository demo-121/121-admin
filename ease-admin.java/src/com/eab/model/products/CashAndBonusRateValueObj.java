package com.eab.model.products;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 15-7-16.
 */
public class CashAndBonusRateValueObj implements Serializable {
    private Integer polYrMin;
    private Integer polYrMax;
    private Double scale;
    private ArrayList<Integer> rateList;

    public Integer getPolYrMin() {
        return polYrMin;
    }

    public void setPolYrMin(Integer polYrMin) {
        this.polYrMin = polYrMin;
    }

    public Integer getPolYrMax() {
        return polYrMax;
    }

    public void setPolYrMax(Integer polYrMax) {
        this.polYrMax = polYrMax;
    }

    public Double getScale() {
        return scale;
    }

    public void setScale(Double scale) {
        this.scale = scale;
    }

    public ArrayList<Integer> getRateList() {
        return rateList;
    }

    public void setRateList(ArrayList<Integer> rateList) {
        this.rateList = rateList;
    }
}
