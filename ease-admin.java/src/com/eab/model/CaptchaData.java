package com.eab.model;

import java.awt.image.BufferedImage;
import java.util.Date;

public class CaptchaData {
	private String text;
	
	private BufferedImage image;
	
	private Date createTime;
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public BufferedImage getImage() {
		return image;
	}
	public void setImage(BufferedImage image) {
		this.image = image;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
