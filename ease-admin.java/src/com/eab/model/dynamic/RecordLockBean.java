package com.eab.model.dynamic;

public class RecordLockBean {
	
	boolean canAccess, isCurrentUserLocker;
	String sessionId, module, recordId, userCode;
	int recordVersion = 0;
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public int getRecordVersion() {
		return recordVersion;
	}

	public void setRecordVersion(int recordVersion) {
		this.recordVersion = recordVersion;
	}

	
	public boolean canAccess() {
		return canAccess;
	}

	public void setCanAccess(boolean canAccess) {
		this.canAccess = canAccess;
	}
	public boolean isCurrentUserAccess() {
		return isCurrentUserLocker;
	}

	public void setIsCurrentUserAccess(boolean isCurrentUserLocker) {
		this.isCurrentUserLocker = isCurrentUserLocker;
	}
	
	
}
