package com.eab.model.dynamic;

public class DynMultiLangField {
	String module, fieldId, mstTable, tmpTable, audTable;

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public String getMstTable() {
		return mstTable;
	}

	public void setMstTable(String mstTable) {
		this.mstTable = mstTable;
	}

	public String getTmpTable() {
		return tmpTable;
	}

	public void setTmpTable(String tmpTable) {
		this.tmpTable = tmpTable;
	}

	public String getAudTable() {
		return audTable;
	}

	public void setAudTable(String audTable) {
		this.audTable = audTable;
	}
}
