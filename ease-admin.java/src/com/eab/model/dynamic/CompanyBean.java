package com.eab.model.dynamic;

import java.util.ArrayList;
import java.util.List;

public class CompanyBean {
	String langCode, compCode, compName, compAlias, dateFormat;
	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getCompAlias() {
		return compAlias;
	}

	public void setCompAlias(String compAlias) {
		this.compAlias = compAlias;
	}
	boolean isActive;
	
	public void setCompCode(String compCode){
		this.compCode = compCode;
	}
	
	public String getCompCode(){
		return compCode;
	}
	
	public void setCompName(String compName){
		this.compName = compName;
	}
	
	public String getCompName(){
		return compName;
	}
	
	
	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}
	
	public boolean getIsActive(){
		return isActive;
	}
	
	public void setLang(String langCode){
		this.langCode = langCode;
	}
	public List<String> getLangList(){
		List<String> langList = new ArrayList<String>();
		if(langCode != null){
			String[] langArray = langCode.split(",");
			for(int i = 0; i < langArray.length; i++){
				langList.add(langArray[i]);
			}
		}else{
			langList.add("en");
		}
		return langList;
	}
}
