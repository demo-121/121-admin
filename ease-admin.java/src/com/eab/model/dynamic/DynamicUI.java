package com.eab.model.dynamic;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.eab.common.Log;

@Entity
@Table(name = "dyn_ui_mst")
public class DynamicUI implements Serializable{
	@Id
	@Column(name = "UI_NAME")
	String uiName;
	
	@Id
	@Column(name = "MODULE")
	String module;

	@Id
	@Column(name = "DATA_TABLE")
	String dataTable;
	
	@Id
	@Column(name = "TEMP_TABLE")
	String tempTable;
	
	@Id
	@Column(name = "AUD_TABLE")
	String audTable;

	@Id
	@Column(name = "SEARCH_FIELD")
	String searchField;
	
	@Id
	@Column(name = "FILTER_LOOKUP")
	String filterLookup;

	@Id
	@Column(name = "FILTER_KEY")
	String filterKey;
	
	
	@Id
	@Column(name = "PRIMARY_KEY")
	String primaryKey;
	
	@Id
	@Column(name = "MASTER_VIEW")
	String masterView;
	
	String primaryName;
	
	Boolean companyFilter;
	
	Boolean langFilter;
	
	String additionalFilter;
	
	public Boolean getCompanyFilter() {
		return companyFilter;
	}

	public void setCompanyFilter(Boolean companyFilter) {
		this.companyFilter = companyFilter;
	}
	
	public Boolean getLangFilter() {
		return langFilter;
	}
	
	public void setLangFilter(Boolean langFilter){
		this.langFilter = langFilter;
	}

	public String getUiName(){
		return uiName;
	}
	
	public void setUiName(String uiName){
		this.uiName = uiName;
	}
	
	
	
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getDataTable(){
		return dataTable;
	}
	
	public void setDataTable(String dataTable){
		this.dataTable = dataTable;
	}
	
	public String getTempTable(){
		return tempTable;
	}
	
	public void setTempTable(String tempTable){
		this.tempTable = tempTable;
	}
	
	public String getAudTable(){
		return audTable;
	}
	
	public void setAudTable(String audTable){
		this.audTable = audTable;
	}
	
	public String getSearchField(){
		return searchField;
	}
	
	public void setSearchField(String searchField){
		this.searchField = searchField;
	}
	
	public String getFilterLookup(){
		return filterLookup;
	}
	
	public void setFilterLookup(String filterLookup){
		this.filterLookup = filterLookup;
	}
	
	public String getFilterKey(){
		return filterKey;
	}
	
	public void setFilterKey(String filterKey){
		this.filterKey = filterKey;
	}
	
	public String getPrimaryKey(){
		return primaryKey;
	}
	
	public void setPrimaryKey(String primaryKey){
		this.primaryKey = primaryKey;
	}
	
	
	
	public String getMasterView() {
		return masterView;
	}

	public void setMasterView(String masterView) {
		this.masterView = masterView;
	}
	
	public void setPrimaryName(String primaryName){
		this.primaryName = primaryName;
	}
	
	public String getPrimaryName(){
		return primaryName;
	}

	
	public String getAdditionalFilter() {
		return additionalFilter;
	}

	public void setAdditionalFilter(String additionalFilter) {
		this.additionalFilter = additionalFilter;
	}

	public static DynamicUI getDynamicUIFromResultSet(ResultSet rs){
		DynamicUI ui = new DynamicUI();
		if(rs != null){
			try {
				while(rs.next()){
					ui.setModule(rs.getString("module"));
					ui.setUiName(rs.getString("UI_NAME"));
					ui.setTempTable(rs.getString("temp_table"));
					ui.setDataTable(rs.getString("data_table"));
					ui.setAudTable(rs.getString("aud_table"));			
					ui.setSearchField(rs.getString("search_field"));
					ui.setFilterLookup(rs.getString("filter_lookup"));
					ui.setFilterKey(rs.getString("filter_key"));
					ui.setPrimaryKey(rs.getString("primary_key"));
					ui.setPrimaryName(rs.getString("primary_name"));
					ui.setMasterView(rs.getString("master_view"));
					String compFilter = rs.getString("filter_by_comp");
					String langFilter = rs.getString("filter_by_lang");
					ui.setAdditionalFilter(rs.getString("filter"));
					if(compFilter != null && compFilter.equals("Y"))
						ui.setCompanyFilter(true);
					else
						ui.setCompanyFilter(false);
					if(langFilter != null && langFilter.equals("Y"))
						ui.setLangFilter(true);
					else
						ui.setLangFilter(false);
					
				}
			} catch (SQLException e) {
				Log.error(e);
			}
		}
		return ui;
	}
	
	
}
