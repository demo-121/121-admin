package com.eab.model.beneIllus;

import java.io.Serializable;

public class BiBean implements Serializable {
	private String biCode;
	private String biName;
	private String typeFilter;
	private int version;
	private String launchStatus;
	private String status;
	private String id;
	private Boolean isClone;
	private Boolean isNewVersion;
	private String taskStatus;
	
	public String getBiCode() {
		return biCode;
	}
	
	public void setBiCode(String biCode) {
		this.biCode = biCode;
	}
	
	public String getBiName() {
		return biName;
	}
	
	public void setBiName(String biName) {
		this.biName = biName;
	}
	
	public String getTypeFilter() {
		return typeFilter;
	}
	
	public void setTypeFilter(String typeFilter) {
		this.typeFilter = typeFilter;
	}
	
	public int getVersion() {
		return version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}
	
	public String getLaunchStatus() {
		return launchStatus;
	}
	
	public void setLaunchStatus(String launchStatus) {
		this.launchStatus = launchStatus;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public Boolean getIsClone() {
		return isClone;
	}
	
	public void setIsClone(Boolean isClone) {
		this.isClone = isClone;
	}
	
	public Boolean getIsNewVersion() {
		return isNewVersion;
	}
	
	public void setIsNewVersion(Boolean isNewVersion) {
		this.isNewVersion = isNewVersion;
	}
	
	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
}
