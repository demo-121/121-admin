package com.eab.couchbase;

import com.eab.common.Log;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Map;

public class CBServer {
	public final String SAVE_FAILURE = "Cannot save document";
	public final String UPDATE_SUCCESS = "Update Success";
	public final String DELETE_FAILURE = "Delete Failure";
	public final String DELETE_SUCCESS = "Delete Success";
	public final String CONTENT_TYPE_JSON = "application/json";
	public final String CONTENT_TYPE_PDF = "application/pdf";
	public final String CONTENT_TYPE_PNG = "img/png";
	public final String CONTENT_TYPE_JPG = "img/jpeg";
	private static String gatewayURL;
	private static String gatewayPort;
	private static String gatewayDBName;
	private static String gatewayUser;
	private static String gatewayPW;

	public CBServer(String gatewayURL, String gatewayPort, String gatewayDBName, String gatewayUser, String gatewayPW){
		this.gatewayURL = gatewayURL;
		this.gatewayPort = gatewayPort;
		this.gatewayDBName = gatewayDBName;		
		this.gatewayUser = gatewayUser;
		this.gatewayPW = gatewayPW;
	}

	public int CreateDoc() {
		return CreateDoc("CBserver");    	
	}

	public int CreateDoc(String pk){
		return CreateDoc("CBserver", "{}"); 
	}

	public int CreateDoc(String pk, String jsonFile) {
		HttpURLConnection con = null;
		int resCode = 400;
		try {
			con = (HttpURLConnection)getURLConnection(pk, gatewayUser, gatewayPW);
			
			if (con != null) {
				con.setRequestMethod("PUT");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
				
				String revId = getRevId(pk);;
				
				if (!revId.isEmpty())
					con.setRequestProperty("If-Match", revId);

				BufferedOutputStream bos = new BufferedOutputStream(con.getOutputStream());
				InputStream inputStream = new ByteArrayInputStream(jsonFile.getBytes(Charset.forName("UTF-8")));
				BufferedInputStream bis = new BufferedInputStream(inputStream);
				byte[] buffer = jsonFile.getBytes(Charset.forName("UTF-8"));
				int b;
				
				while ((b = bis.read(buffer)) > 0) {
					bos.write(buffer, 0, b);
				}
				
				bos.close();
				bis.close();
				
				


				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();
				resCode =  con.getResponseCode();
				Log.info("{CBServer CreateDoc} ------------ Document:" + pk + "\t" + con.getResponseCode() + " - " + con.getResponseMessage());
			}
		} catch (IOException ex) {
			Log.error("{CBServer CreateDoc} --------> Exception");
			Log.error(ex);
			return 400;
		} finally {
			if(con != null)
				con.disconnect();
		}
	
		return resCode;
	}

	public String updateRole(String roleName, String roleJSON){
		HttpURLConnection con = null;
		
		try {
			con = (HttpURLConnection)getURLConnection("_role/" + roleName ,gatewayUser, gatewayPW);
			if(con != null){	
				con.setRequestMethod("PUT");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);

				BufferedOutputStream bos = new BufferedOutputStream(con.getOutputStream());
				InputStream inputStream = new ByteArrayInputStream(roleJSON.getBytes(Charset.forName("UTF-8")));
				BufferedInputStream bis = new BufferedInputStream(inputStream);
				byte[] buffer = roleJSON.getBytes(Charset.forName("UTF-8"));
				int b;
				
				while ((b = bis.read(buffer)) >0) {
					bos.write(buffer, 0, b);
				}
				bos.close();

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();

				return response.toString();
			}
			return null;
		} catch (IOException ex) {
			Log.error("{CBServer UpdateRole} --------> " + ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}	
	}

	public String getListByKey(String startkey, String endkey){
		HttpURLConnection con = null;
		
		try {
			String tag = "_all_docs?startkey=" + startkey + "&endkey=" + endkey;
			con = (HttpURLConnection)getURLConnection(tag,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("GET");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
				con.setConnectTimeout(10000);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();

				return response.toString();
			}
			return null;
		} catch (IOException ex) {
			Log.error("{CBServer getListByKey} --------> " + ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}			
	} 

	public String UpdateDoc(String pk, String value) {
		if(CreateDoc(pk, value) == 400)
			return SAVE_FAILURE;
		
		return  UPDATE_SUCCESS;
	}

	public boolean UpdateDocByDict(String pk, Map<String, Object> value){
		return true;
	}

	public String RemoveDoc(String pk) {
		HttpURLConnection con = null;
		
		try {
			con = (HttpURLConnection)getURLConnection(pk,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("DELETE");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
				con.setRequestProperty("If-Match", getRevId(pk));

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();

				return response.toString();
			}
			return null;
		} catch (IOException ex) {
			Log.error("{CBServer RemoveDoc} --------> " + ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}		
	}

	public void CreateAttach(String pk, String imageName, String fileName, InputStream fileIn) {
		HttpURLConnection con = null;
		
		try {
			String content_type = "";

			if(fileName.equals("application/pdf")){
				content_type = "application/pdf";
			}else if(fileName.equals("image/png")){
				content_type = "image/png";
			}else if(fileName.equals("image/jpeg")){
				content_type = "image/jpeg";
			}else{      		
				if (fileName.lastIndexOf(".") > 0) {    			  		
					String format = fileName.substring(fileName.lastIndexOf("."), fileName.length());
					if (format.equals(".png")) {
						content_type = "image/png";
					} else if (format.equals(".jpg")) {
						content_type = "image/jpeg";
					} else if (format.equals(".pdf")) {
						content_type = "application/pdf";
					} else if (format.equals(".json")) {
						content_type = "application/json";
					}
				}
			}
		
			String tag = pk + "/" + imageName;
			con = (HttpURLConnection)getURLConnection(tag,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("PUT");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", content_type);
				con.setRequestProperty("If-Match", getRevId(pk));

				BufferedOutputStream bos = new BufferedOutputStream(con.getOutputStream());
				BufferedInputStream bis = new BufferedInputStream(fileIn);
			
				int len = 0;
				byte[] buf = new byte[1024];
			
				while ((len = fileIn.read(buf, 0, 1024)) != -1) {
					bos.write(buf, 0, len);
				}
				bos.close();
			    		
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();
				bis.close();
			}
		} catch (IOException ex) {
			Log.error("{CBServer CreateAttach} --------> " + ex);
		}finally{
			if(con!=null)
				con.disconnect();
		}	
	}

	public String RemoveAttach(String pk, String iamgeName) {
		HttpURLConnection con=null;
		
		try {
			con = (HttpURLConnection)getURLConnection(pk + "/" + iamgeName,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("DELETE");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
				con.setRequestProperty("If-Match", getRevId(pk));

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();

				return response.toString();
			}
			
			return null;
		} catch (IOException ex) {
			Log.error("{CBServer RemoveDoc} --------> " + ex);
			return null;
		} finally{
			if(con!=null)
				con.disconnect();
		}	
	}   

	public byte[] GetAttach(String pk, String attachName) {
		HttpURLConnection con =null;
		
		try {
			String url;             
			url = gatewayURL + "/" + gatewayDBName + "/" + pk + "/" + attachName;
			String destinationFile = "d:/" + attachName + ".jpg";

			URL obj;

			obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
			
			if(con!=null){
				con.setRequestMethod("GET");             

				InputStream  is = con.getInputStream();                                  
				OutputStream os = new FileOutputStream(destinationFile);
				ByteArrayOutputStream buffer = new ByteArrayOutputStream();

				byte[] b = new byte[16384];
				int length;

				while ((length = is.read(b, 0, b.length)) != -1) {
					os.write(b, 0, length);
				}

				buffer.writeTo(os);
				is.close();     		
				os.close();       
				return buffer.toByteArray();  
			}
			return null;

		} catch (IOException ex) {
			Log.error("{CBServer GetAttach} --------> " + ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}	
	}  

	private static String getRevId(String docId){
		String rev = "";
		HttpURLConnection con = null;
		
		try {
			con = (HttpURLConnection)getURLConnection(docId, gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("GET");           

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				// JSON
//				JSONObject jo_doc = new JSONObject(response.toString());
//				rev = jo_doc.getString("_rev");
				JSONObject jo_doc = null;
				try {
					jo_doc = new JSONObject(response.toString());
					rev = jo_doc.getString("_rev");
				} catch (JSONException ex) {
					String target = "\"_rev\":\"";
					String dummy = response.toString();
					int startpos = dummy.indexOf(target) + target.length();
					dummy = dummy.substring(startpos, startpos + 100);
					int endpos = dummy.indexOf("\"");
					dummy = dummy.substring(0, endpos);
					Log.debug("[kaming] customized _rev: " + dummy);
					
					rev = dummy;
				}
			}

		} catch (IOException ex) {
			Log.info("{CBServer getRevId} -------- Revision Not Find Create new Document ");
			return "";
		} catch (JSONException ex) {
			Log.error("{CBServer getRevId} --------> JSONException " + ex);
			Log.error(ex);
			return "";
		} finally {
			if(con!=null)
				con.disconnect();
		}	
		
		return rev;
	}

	private String getMIMEtype(String fileType)
	{
		String mimeType = "";

		if (fileType != null) {            
			if (fileType.equals(".png"))
				mimeType = "image/png";
			else if (fileType.equals(".jpg"))
				mimeType = "image/jpeg";
			else if (fileType.equals(".pdf")) 
				mimeType = "application/pdf";
			else if (fileType.equals(".json")) 
				mimeType = "application/json";
		}

		return mimeType;
	}

	public boolean checkDBConnection() {
		HttpURLConnection con=null;
		
		try {
			con = (HttpURLConnection)getURLConnection("" ,gatewayUser, gatewayPW);

			if(con != null)
				return true;
			else
				return false;
		} catch (IOException ex) {
			return false;
		} finally{
			if(con != null)
				con.disconnect();
		}	
	}
	
	private static URLConnection getURLConnection(String tag, String user, String password) throws IOException  {		 
		URLConnection urlConnection=null;
		
		try {		
			String authString = user + ":" + password;

			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
			String authStringEnc = new String(authEncBytes);				

			URL url = new URL(gatewayURL + ":" + gatewayPort + "/" + gatewayDBName + "/" + tag);
			urlConnection = url.openConnection();
			urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
		} catch (MalformedURLException e) {
			e.printStackTrace();				
		} catch (IOException e) {
			e.printStackTrace();				
		}  
		
		return urlConnection;
	}

	public JSONObject getDoc(String docId){
		HttpURLConnection con = null;
		JSONObject doc = null;
		
		try {
			con = (HttpURLConnection)getURLConnection(docId,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("GET");           

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
				String inputLine;
				StringBuffer response = new StringBuffer();
				
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				// JSON
				doc = new JSONObject(response.toString());
			}
		} catch (IOException ex) {
			Log.info("{CBServer getDoc} -------- Document Not Found " + ex);
			return null;
		} catch (JSONException ex) {
			Log.error("{CBServer getDoc} --------> JSONException " + ex);
			Log.error(ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}	
		
		return doc;
	}
	
	public JSONArray getDocsByBatch(CBServer cb, String view, String key1, ArrayList<String> ids, String key2,   String stale){ 
		JSONObject result = new JSONObject();
		JSONArray resultRows = new JSONArray(); 
		try {
			
			int listSize = ids.size();
			// Couchbase Max View Key Size	4096 bytes
			// Couchbase is using UTF-8 to encode
			// use 2048 characters to get the view
			int urlMaxLength = 2048;
			int txtLength = 100;//slash and buffer
			String preFixUrl = view +CBServer.gatewayURL +CBServer.gatewayPort +CBServer.gatewayDBName +CBServer.gatewayUser+CBServer.gatewayPW +stale;

			String presetUrl = "_design/main/_view/"+view+"?keys=[";
			String url = presetUrl;
			int i = 0;
			while (i < ids.size()) {
				// url.length equals to the length of static view string
				// second one equals to the length of key value
				// txtLength equals to the length of the domain and db name
				if (URLEncoder.encode(url, "UTF-8").length() + URLEncoder.encode(ids.get(i) + key1 + key2, "UTF-8").length() +1 + URLEncoder.encode(preFixUrl, "UTF-8").length() + txtLength > urlMaxLength) {
					// Get the result first when the url is going to be longer than max length
					url = url.substring(0,url.length()-1 )+"]&stale="+stale; 
					JSONObject doc = cb.getDoc(url); 
					Log.debug("getDocsByBatch url:"+url);
					if(doc!=null){
						JSONArray rows = doc.getJSONArray("rows");			
						for( int j = 0; j < rows.length(); j++){
							resultRows.put(rows.get(j));
						}
					}
					
					url = presetUrl;
					url = url+key1+ ids.get(i)+key2+",";
				} else {
					url = url+key1+ ids.get(i)+key2+",";
				}
				i++;
			}
		
			result.put("rows", resultRows);			 
			return resultRows;
		} catch (UnsupportedEncodingException e) {
			Log.error(e);
			return resultRows;
		}
	}

	public JSONObject queryView(String dsname, String view, String startKey, String endKey) {
		HttpURLConnection con = null;
		
		try {
			String tag = "_design/main/_view/" + view;
		
			if (startKey != null) {
				if (endKey != null) {
					tag+="?startkey=" + startKey + "&endkey=" + endKey + "&stale=ok";
				} else {
					tag+="?key=" + startKey + "&stale=ok";
				}
			} else {
				tag+= "?stale=ok";
			}
			
			con = (HttpURLConnection)getURLConnection(tag,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("GET");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
				con.setConnectTimeout(10000);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();

				return new JSONObject(response.toString());
			}
			return null;
		} catch (IOException ex) {
			Log.error("{CBServer getListByKey} --------> " + ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}
	}
	
}