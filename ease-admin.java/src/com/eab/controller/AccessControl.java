package com.eab.controller;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.common.LoginManager;
import com.eab.common.MainMenu;
import com.eab.dao.audit.AudUserLog;
import com.eab.dao.profile.User;
import com.eab.model.MenuList;
import com.eab.model.RSAKeyPair;
import com.eab.model.profile.UserBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.system.SysParamBean;
import com.eab.security.RSA;

@Controller
public class AccessControl {
//	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
//	public String RootPage(@RequestParam Map<String,String> allRequestParams, ModelMap model
//			, HttpServletRequest request, HttpServletResponse response) {
//		///TODO: Setup like default language
//		
//		return "redirect:/Login";
//	}
	
//	@RequestMapping(value = { "/Login**" }, method = RequestMethod.GET)
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public ModelAndView LoginPage(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		HttpSession session = request.getSession(false);
		Function2 func2 = new Function2();
		String errMessage = null;
		String errCode = null;

		Log.info("[/Login] ------Start------");
		
		try {
			/* 
			 * Backup before current session is destroy (START) 
			 */
			
			// Login Error Code
			if (session != null) {
				errCode = (String) session.getAttribute(Constants.LOGIN_ERROR_CODE);
				if (errCode != null) {
					errMessage = func2.getErrorMsg(errCode);
				}
			}
			/* 
			 * Backup before current session is destroy (END) 
			 */

			if(session!=null) {
				UserPrincipalBean user = Function.getPrincipal(session);
				Object loginBySaml = session.getAttribute("loginBySaml");
				if (user != null && loginBySaml != null && (boolean) loginBySaml) {
					Log.info("************** login by saml, user found  ************** login page");
					session.removeAttribute("loginBySaml");
				} else {
					Log.info("************** session going to destroy ************** login page");
					session.invalidate();
					session = request.getSession(true);
				}
			} else {
				Log.info("************** new session  ************** login page");
				session = request.getSession(true);
			}
			
			RSA cryptoObj = new RSA();
			if (cryptoObj.GenerateKey()) {
				// Generate Public Key for JavaScript Encryption
				String publicKeyJS = cryptoObj.GetHTMLUsedModulus();
				model.addObject("publicKey", publicKeyJS);
				// Save KeyPair into Session
				session.setAttribute(Constants.CRYPTO_KEYPAIR_NAME, cryptoObj.Export());
				
				// get language options
				Map<String, SysParamBean> sysParamList = func2.getSysParam();
				String sysName = sysParamList.get("SYS_NAME").getParamValue();
//				String ver =  sysParamList.get("SYS_VER").getParamValue();
				
				model.addObject("title", sysName);
				model.addObject("companyName", Constants.COMPANY_NAME);
				
				// Set Error message if having
				if (errMessage != null) {
					model.addObject("errorMessage", errMessage);
				}
				
				model.setViewName("login");		// VIEW name referring to /views/login.jsp
			} else {
				///TODO: Go to Error page
				model.setViewName("error");
			}
			
		} catch(Exception e) {
			Log.error(e);
			///TODO: Go to Error page
			model.setViewName("error");
		}
		
		Log.info("[/Login] ------End------");
		
		return model;
	}
	
//	@RequestMapping(value = { "/Login/Auth**" }, method = RequestMethod.POST)
//	public String AuthLogin(@RequestParam Map<String,String> allRequestParams, ModelMap model
//			, HttpServletRequest request, HttpServletResponse response) {
//		boolean hasError = false;
//		boolean validCaptcha = false;
//		boolean validUsername = false;
//		boolean validPassword = false;
//		boolean validStatus = false;
//		boolean validSingleLogin = false;
//		String nextModel = "redirect:/Error";
//		String sourceOfLogin = "LOGIN";
//		
//		HttpSession session = request.getSession();
//		Function2 func2 = new Function2();
//		AudUserLog audUserLog = new AudUserLog();
//		Menu menu = null;
//
//		Map<String, SysParamBean> sysParamList = null;
//		String failReason = null;
//		UserBean userInfo = null;
//		String usercode = null;
//		String password = null;
//		String captcha = null;
//		LoginManager loginMgr = null;
//		String sysVer = null;
//		Map<String, Object> sessionObjs = null;
//		
//		Log.info("[/Login/Auth] User Login - authentication (START)");
//		
//		try {
//			sysParamList = func2.getSysParam();
//			sysVer = sysParamList.get("SYS_VER").getParamValue();
//			loginMgr = new LoginManager();
//			
//			//Capture Input parameter
//			usercode = allRequestParams.get("data_001");		//Username
//			usercode = (usercode != null)?usercode.toUpperCase():null;	//Username is uppercase in database record.
//			password = allRequestParams.get("data_002");		//Password
//			captcha = allRequestParams.get("data_003");			//Captcha
//			
//			Log.debug("[/Login/Auth] Username = " + usercode);
//			Log.debug("[/Login/Auth] Password = " + password);
//			Log.debug("[/Login/Auth] Captcha = " + captcha);
//			
//			/* Backup before current session is destroy */
//			sessionObjs = func2.backupSessionAttributes(request);
//			
//			//Destroy and Recreate Session ID (Backup any required data in Session before destroy)
//			Log.debug("[/Login/Auth] Session ID (Current) = " + ((session!=null)?session.getId():"") );
//			if (session != null) {
//				try {
//					session.invalidate();	// Destroy
//					session = request.getSession(true);
//				} catch(Exception e) {
//		    		Log.error(e);
//		    	}
//			}
//			Log.debug("[/Login/Auth] Session ID (New) = " + session.getId());
//			
//			/* Restore after session is created */
//			func2.restoreSessionAttributes(request, sessionObjs);
//			func2.stdoutSessionAttributes(request);
//			
//			//Retrieve Captcha from session
//			String captchaInSession = null;
//			if (session.getAttribute(Constants.CAPTCHA_TEXT) != null) {
//				captchaInSession = (String) session.getAttribute(Constants.CAPTCHA_TEXT);
//			}
//			Date captchaDateInSession = null;
//			if (session.getAttribute(Constants.CAPTCHA_DATETIME) != null) {
//				captchaDateInSession = (Date) session.getAttribute(Constants.CAPTCHA_DATETIME);
//			}
//			
//			//Retrieve RSA Keypair from session
//			RSAKeyPair keypair = null;
//			if (session.getAttribute(Constants.CRYPTO_KEYPAIR_NAME) != null) {
//				keypair = (RSAKeyPair) session.getAttribute(Constants.CRYPTO_KEYPAIR_NAME);
//			}
//			
//			//Get User Profile by usercode
//			if (!hasError) {
//				User userObj = new User();
//				userInfo = userObj.get(usercode);
//				Log.debug("[/Login/Auth] userInfo.getCompCode() = " + userInfo.getCompCode());
//				Log.debug("[/Login/Auth] userInfo.getUserCode() = " + userInfo.getUserCode());
//				Log.debug("[/Login/Auth] userInfo.getUserName() = " + userInfo.getUserName());
//				Log.debug("[/Login/Auth] userInfo.getUserPassword() = " + userInfo.getUserPassword());
//			}
//			
//			//Validate Captcha
//			if (!hasError) {
//				Log.debug("[/Login/Auth] Captcha Compare: " + captchaInSession + " <--> " + captcha);
//				if (captchaInSession != null && captchaInSession.equalsIgnoreCase(captcha)) {
//					validCaptcha = true;
//				} else {
//					hasError = true;
//					session.setAttribute(Constants.LOGIN_ERROR_CODE, "CAPTCHA_FAIL");
//					Log.error("[/Login/Auth] Captcha is invalid.");
//					failReason = "CAPTCHA_FAIL";
//				}
//			}
//			
//			//Validate User Name, User status...etc
//			if (!hasError) {
//				///TODO: User data from Database and validate
//				Log.debug("[/Login/Auth] Compare '" + usercode + "' <--> '" + userInfo.getUserCode() + "'");
//				if (usercode != null && usercode.equalsIgnoreCase(userInfo.getUserCode())) {
//					validUsername = true;
//				} else {
//					hasError = true;
//					validUsername = false;
//					session.setAttribute(Constants.LOGIN_ERROR_CODE, "LOGIN_FAIL");
//					Log.error("[/Login/Auth] Username is invalid.");
//					failReason = "LOGIN_FAIL_USERCODE";
//					
//					loginMgr.addFailCount(usercode);
//				}
//			}
//			
//			//Decrypt Password
//			String decryptedPassword = null;
//			if (!hasError && keypair != null) {
//				decryptedPassword = Function.decryptJsText(keypair, password);
//				Log.debug("[/Login/Auth] Decrypted Password = " + decryptedPassword);
//			}
//			
//			//Validate Password
//			if (!hasError) {
//				if (decryptedPassword != null && decryptedPassword.equals(userInfo.getUserPassword())) {
//					validPassword = true;
//				} else {
//					hasError = true;
//					validPassword = false;
//					session.setAttribute(Constants.LOGIN_ERROR_CODE, "LOGIN_FAIL");
//					Log.error("[/Login/Auth] Password is invalid.");
//					failReason = "LOGIN_FAIL_PASSWORD";
//					
//					loginMgr.addFailCount(usercode);
//				}
//			}
//			
//			//Validate Fail Count
//			if (!hasError) {
//				int failCount = loginMgr.getFailCount(usercode);
//				if (failCount < Constants.LOGIN_FAIL_COUNT_MAX) {
//					validStatus = true;
//				} else {
//					hasError = true;
//					session.setAttribute(Constants.LOGIN_ERROR_CODE, "STATUS_FAIL");
//					Log.error("[/Login/Auth] Fail Count is exceeded maximum.");
//					failReason = "LOGIN_FAIL_COUNT";
//					
//					loginMgr.addFailCount(usercode);
//				}
//			}
//			
//			//Validate User Status
//			if (!hasError) {
//				if ("A".equals(userInfo.getStatus())) {
//					validStatus = true;
//				} else {
//					hasError = true;
//					session.setAttribute(Constants.LOGIN_ERROR_CODE, "STATUS_FAIL");
//					Log.error("[/Login/Auth] User Status is invalid.");
//					failReason = "STATUS_FAIL";
//					
//					loginMgr.addFailCount(usercode);
//				}
//			}
//			
//			//Validate Single Login Session
//			if (!hasError && Constants.ENABLE_SINGLE_LOGIN) {
//				if (func2.hasLoginSession(usercode)) {
//					hasError = true;
//					session.setAttribute(Constants.LOGIN_ERROR_CODE, "LOGIN_SESSION_FAIL");
//					Log.error("[/Login/Auth] User Code has exist login session.");
//					failReason = "LOGIN_SESSION_FAIL";
//				} else {
//					validSingleLogin = true;	// No exist login session.
//				}
//			} else {
//				validSingleLogin = true;
//			}
//			
//			if (!hasError) {
//				//******Success Login******: All validation has been passed
//				
//				//Put user profile / info into principal
//				UserPrincipalBean principal = new UserPrincipalBean();
//				principal.setCompCode(userInfo.getCompCode());
//				principal.setUserCode(userInfo.getUserCode());
//				principal.setUserName(userInfo.getUserName());
//				principal.setStatus(userInfo.getStatus());
//				principal.getWorkflow().setLogin(true);
//				session.setAttribute(Constants.USER_PRINCIPAL, principal);
//				
//				//Mark Last Login Time
//				loginMgr.setLastLoginTime(usercode);
//				
//				//Audit Trail (Success)
//				audUserLog.add(usercode, userInfo.getCompCode(), Function.getServerIp(request)
//						, Function.getClientIp(request), sysVer
//						, true, null, sourceOfLogin, session.getId());
//				
//				// Preload Menu items
//				menu = new Menu();
//				MenuList menuList = menu.loadFromDB();
//				menu.putToSession(session, menuList);
//				
//				// Get next steps of Login process
//				nextModel = "redirect:" + principal.getWorkflow().nextFlow();
//				Log.info("[/Login/Auth] Go to " + nextModel);
//			} else {
//				//******Fail Login******
//				
//				nextModel = "redirect:/Login";
//				
//				//Audit Trail (Fail)
//				audUserLog.add(usercode, userInfo.getCompCode(), Function.getServerIp(request)
//						, Function.getClientIp(request), sysVer
//						, false, failReason, sourceOfLogin, session.getId());
//			}
//			
//		} catch(Exception e) {
//			try {
//				audUserLog.add(usercode, userInfo.getCompCode(), Function.getServerIp(request)
//						, Function.getClientIp(request), sysVer
//						, false, "EXCEPTION:" + e.getMessage(), sourceOfLogin, session.getId());
//			} catch(Exception ex) {
//				Log.error(ex);
//			}
//			Log.error(e);
//			return "redirect:/Error";
//		} finally {
//			///TODO: Initialize all object and sensitive data in memory
//			menu = null;
//		}
//
//		Log.info("[/Login/Auth] User Login - authentication (END)");
//		
//		return nextModel;
//	}
	
//	@RequestMapping(value = { "/Error**" }, method = RequestMethod.GET)
//	public ModelAndView Error() {
//		
//		Log.info("[/Error] ------Start------");
//		ModelAndView model = new ModelAndView();
//		model.setViewName("error");
//
//		Log.info("[/Error] ------End------");
//		
//		return model;
//	}
	
	@RequestMapping(value = { "/Logout**" }, method = RequestMethod.GET)
	public String Logout(@RequestParam Map<String,String> allRequestParams, ModelMap model
			, HttpServletRequest request, HttpServletResponse response) {
		LoginManager loginMgr = null;
		
		Log.info("[/Logout] ------Start------");
		try {
			loginMgr = new LoginManager();
			loginMgr.logout(request, response);
		} catch(Exception e) {
			Log.error(e);
		} finally {
			loginMgr = null;
		}
		Log.info("[/Logout] ------End------");
		
		return "redirect:/";
	}
	
//	@RequestMapping(value = { "/Login/Terms**" }, method = RequestMethod.GET)
//	public ModelAndView Terms(HttpServletRequest request, HttpServletResponse response) {
//		ModelAndView model = new ModelAndView();
//		model.setViewName("terms");
//		model.addObject("title", Constants.APP_NAME + ": Login");
//		model.addObject("companyName", Constants.COMPANY_NAME);
//		
//		return model;
//	}
	
//	@RequestMapping(value = { "/Login/Terms/Accept**" }, method = RequestMethod.POST)
//	public String TermsAccept(@RequestParam Map<String,String> allRequestParams, ModelMap model
//			, HttpServletRequest request, HttpServletResponse response) {
//		
//		HttpSession session = request.getSession();
//		AudUserLog audUserLog = new AudUserLog();
//		String nextModel = null;
//		
//		UserPrincipalBean principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
//		
//		
//		principal.getWorkflow().setTerms(true);
//		
//		// Get next steps of Login process
//		nextModel = "redirect:" + principal.getWorkflow().nextFlow();
//		Log.info("[/Login/TermsAccept] Go to " + nextModel);
//		
//		return nextModel;
//	}
	
//	@RequestMapping(value = { "/Login/Terms/Reject**" }, method = RequestMethod.POST)
//	public String TermsReject() {
//		return "redirect:/Logout"; 
//	}
}
