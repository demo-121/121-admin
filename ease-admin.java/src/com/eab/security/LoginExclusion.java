package com.eab.security;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.system.FuncBean;

public class LoginExclusion {
	private static Map<String, Integer> exList = null;
	private List<String> grandedUriList = null;
	private static final int MODE_PUBLIC = AccessType.PUBLIC;			// Public Access: Login is not required.
	private static final int MODE_LOGIN_WIP = AccessType.LOGIN_WIP;		// Login Required: Login Process is still in progress like only completed Username / Password authentication.
	private static final int MODE_LOGIN_READY = AccessType.LOGIN_READY;		// Login Required: All Login Process is completed.  Ready to go Homepage.
	
	public LoginExclusion(UserPrincipalBean p) {
		SysPrivilegeMgr privilegeMgr = null;
		ArrayList<FuncBean> funcList = null;
		Log.debug("{LoginExclusion} contructor start");
		privilegeMgr = new SysPrivilegeMgr();
		
		if (exList == null) {
			try {
				// Register exclusion list of URL before Login is completed.
				exList = new HashMap<String, Integer>();
				
				// Load all functions' access control from database.  Ignore user role
				funcList = privilegeMgr.getFunc(null, "en");
								
				exList.put("/", AccessType.PUBLIC);	// default path "/".
				
				for(FuncBean func: funcList) {
					if (func.getUrl() != null && func.getUrl().length() > 0) {
						exList.put(func.getUrl(), func.getAccessCtrl());
						/*
						Log.debug("{LoginExclusion} Code: " + func.getCode() + " Path: " + func.getUrl() 
									+ ", Access: " + ((func.getAccessCtrl()==AccessType.PUBLIC)?"Public"
											:(func.getAccessCtrl()==AccessType.LOGIN_WIP)?"Login WIP"
													:(func.getAccessCtrl()==AccessType.LOGIN_READY)?"Login Required"
															:"Unknown"));
						*/
					}
				}
				
			} catch(SQLException e) {
				Log.error(e);
			} finally {
				privilegeMgr = null;
				funcList = null;
			}
		}
		
		if (p != null && p.getRoleCode() != null) {
			grandedUriList = new ArrayList<String>();
			privilegeMgr.getGrandedURIList(p.getRoleCode(), grandedUriList);
		} else {
			Log.debug("pricicipal == null !!!!!!!!");
		}
		
		Log.debug("{LoginExclusion} contructor end");

	}
	
	public boolean canAccess(HttpServletRequest request, String pathUrl) {
		boolean result = false;
		int mode = 0;
		HttpSession session = null;
		Object principal = null;
		String matchPath = null;
		String nextWorkflow = null;
		boolean isComplete = false;
		
		try {
			Log.debug("{LoginExclusion canAccess} Request Path: " + pathUrl);
			
			if ("/".equals(pathUrl)) {		// Root Path does not require checking.
				result = true;
				Log.debug("{LoginExclusion canAccess} Bypass Root Path");
			} else {
				session = request.getSession(false);
				
				if (exList.containsKey(pathUrl)) {
					mode = exList.get(pathUrl);
					matchPath = pathUrl;
//				} else {
//					for(Entry<String, Integer> entry : exList.entrySet()) {
//						String key = entry.getKey();
//						if (!("/".equals(key)) && pathUrl.indexOf(key) == 0) {
//							mode = exList.get(key);
//							matchPath = key;
//							break;
//						}
//					}
				} else if (Function.countString(pathUrl, "/") > 1 && exList.containsKey(pathUrl.substring(0, pathUrl.lastIndexOf("/")))) {
					mode = exList.get(pathUrl.substring(0, pathUrl.lastIndexOf("/")));
					matchPath = pathUrl.substring(0, pathUrl.lastIndexOf("/"));
					
					Log.debug("{LoginExclusion canAccess} Smoke Path is identified.");
				}
				Log.debug("{LoginExclusion canAccess} Match Exclusion Path: " + matchPath + ", Mode: " + mode);
	
				// Step 1: Get Access Mode of Servlet URL.  Return TRUE if PUBLIC mode.
				if (!result && mode == MODE_PUBLIC) {
					result = true;
					Log.debug("{LoginExclusion canAccess} Public Path");
				}
				
				// Get additional data if Step 1 is not passed.
				if (!result) {
					try { principal = session.getAttribute(Constants.USER_PRINCIPAL); } catch(NullPointerException e) { principal = null; }
					if (principal != null) {
						isComplete = ((UserPrincipalBean) principal).getWorkflow().isLoginReady();
						nextWorkflow = ((UserPrincipalBean) principal).getWorkflow().nextFlow();
						Log.debug("{LoginExclusion canAccess} isComplete: " + isComplete + ", nextWorkflow: " + nextWorkflow);
					}
				}
				
				// Step 2: If LOGIN_WIP mode, we should check User Principal and its Login Workflow.  Workflow must not be <isComplete>.
				//         No User Principal is not allowed.
				//         The URL Path should match to head string of Next Workflow URL.
				//         E.g. Request URL is "/Login/TermsAccept".  And next workflow is "/Login/Terms".  It is valid.
				//         E.g. Request URL is "/Login/Terms".  And next workflow is "/Login/OTP".  It is invalid.
				if (!result && mode == MODE_LOGIN_WIP) {
					if (principal != null) {
						if (!isComplete && nextWorkflow != null) {
							if (matchPath.indexOf(nextWorkflow) == 0) {
								result = true;
								Log.debug("{LoginExclusion canAccess} Login WIP Path (1) " + result);
							}
						} else {
							result = false;
							Log.debug("{LoginExclusion canAccess} Login WIP Path (2) " + result);
						}
					} else {
						result = false;
						Log.debug("{LoginExclusion canAccess} Login WIP Path (3) " + result);
					}
				}
				
				// Step 3: If LOGIN_READY or Not Found in Exclusion List, we should check User Principal and its Login Workflow.  Workflow must be <isComplete>.
				//         No User Principal is not allowed.
				if (!result && mode >= MODE_LOGIN_READY) {
					if (principal != null) {
						if (isComplete) {
							///TODO: Call method "SysPrivilege.hasPrivilege" to check access privilege even Already Login.
							result = true;
							Log.debug("{LoginExclusion canAccess} Login Ready Path (1) " + result);
							
							// for mode > MODE_LOGIN_READY, it require specific role which has access right on specific path
							if (mode > MODE_LOGIN_READY) {
								if (grandedUriList != null) {
									String module = request.getParameter("p1");
									String uri = pathUrl;
									if (module != null && !module.isEmpty()) {
										uri += "/"+ module;
									}
									result = grandedUriList.contains(uri);
									Log.debug("{LoginExclusion canAccess} Login Ready Path (4) " + result + " from " + uri);
									//TODO-s: John: for temporary use. This should be commented after  Step 4 code is removed
									if(uri.equals("/Releases/Detail")||uri.equals("/Needs")||uri.equals("/Products")||uri.equals("/BeneIllus"))
										return result;
									//TODO-e
								}
							}
						} else {
							result = false;
							Log.debug("{LoginExclusion canAccess} Login Ready Path (2) " + result);
						}
					} else {
						result = false;
						Log.debug("{LoginExclusion canAccess} Login Ready Path (3) " + result);
					}
				}				
				
				// TODO this should be commented!!, it above situation all failure, it should be failure. Otherwise, login can access all function
				// Step 4: Not match to all above situation. Must have Login session and Login is completed.
				
				
				if (!result) {
					if (principal != null) {	//Login session.
						if (isComplete) {	//Login is completed.
							result = true; 
							Log.debug("{LoginExclusion canAccess} Unknown Path (1) " + result);
						} else {
							result = false;
							Log.debug("{LoginExclusion canAccess} Unknown Path (2) " + result);
						}
					} else {
						result = false;
						Log.debug("{LoginExclusion canAccess} Unknown Path (3) " + result);
					}
				}
				
			}
		} catch(Exception e) {
			Log.error(e);
		}
		
		return result;
	}
}
