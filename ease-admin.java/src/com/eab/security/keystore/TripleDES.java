package com.eab.security.keystore;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.eab.common.CryptoUtil;
import com.eab.common.Function;
import com.eab.security.AES;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class TripleDES {
	private final static String CRYPTOKEY_ALGORITHM = "TripleDES";
	private final static String CRYPTOKEY_TRANSFORMATION = "TripleDES/ECB/PKCS5Padding";
	private final static String KEYSTORE_TYPE = "JCEKS";	// Use JKS will have exception "Cannot store non-PrivateKeys".
	private final static String KEYSTORE_ALIAS = "key.121.eabsystems";
	private final static String FILENAME = "d:\\keystore.eab";
	private final static String KEYSTORE_PIN = "ko7K5x2V";
	
	public static void main(String[] args) throws Exception{
		String pin = KEYSTORE_PIN;
		
		// Convert the password into a char array
	    char[] password = new char[pin.length()];
	    pin.getChars(0, pin.length(), password, 0);
	    
	    // Generate Keystore
//	    initialize(password);
	    
	    // Testing
//	    com.eab.security.DESede cipher = com.eab.security.DESede.getInstance();
//	    String enText = "AAbb1122";
//	    String salt = "qV1g5zEGF14tipS4";	// Hash original by salt (must be 16 chars = 128bit)
//	    enText = AES.encrypt(enText, salt);	
//	    System.out.println("Encrypted (aes): " + enText);
//	    enText = cipher.encrypt(enText);
//	    System.out.println("Encrypted (3des): " + enText);
//	    String deText = cipher.decrypt(enText);
//	    System.out.println("Decrypted (3des): " + deText);
//	    deText = AES.decrypt(deText, salt);
//	    System.out.println("Decrypted (aes): " + deText);
	    
	    CryptoUtil crypto = new CryptoUtil();
	    System.out.println("Encrypted String: " + crypto.encryptUserData("EASE_ADMIN", "AAbb1122"));
	}
	
	public static void initialize(char[] password) throws Exception {
		// Create a Blowfish key
		KeyGenerator keyGenerator = KeyGenerator.getInstance(CRYPTOKEY_ALGORITHM);
		keyGenerator.init(168);		// 168 bit (DES uses 56 bit, and TripleDES uses 3 times)
		Key key = keyGenerator.generateKey();
		System.out.println("Primary Key: " + Function.ByteToHex(key.getEncoded()));
		
		create(password, key.getEncoded());
	}
	
	public static void create(char[] password, byte[] primaryKey) throws Exception {
		// Convert Primary Key from byte[] to Key
//		SecretKey originalKey = new SecretKeySpec(primaryKey, 0, primaryKey.length, CRYPTOKEY_ALGORITHM);
		
		// Create a Blowfish key
		KeyGenerator keyGenerator = KeyGenerator.getInstance(CRYPTOKEY_ALGORITHM);
		keyGenerator.init(168);		// 168 bit (DES uses 56 bit, and TripleDES uses 3 times)
		Key key = keyGenerator.generateKey();
		System.out.println("Secondary Key: " + Function.ByteToHex(key.getEncoded()));
		
		// Create a cipher using that key to initialize it
	    Cipher cipher = Cipher.getInstance(CRYPTOKEY_TRANSFORMATION);
	    cipher.init(Cipher.ENCRYPT_MODE, key);
	    
	    
	    
	    /*
	     * Generate Key String for storing into Database
	     * It requires decrypting by Secondary Key storing in Keystore file
	     */
	    // Perform the actual encryption
	    byte[] cipherText = cipher.doFinal(primaryKey);
		System.out.println("Final Key: " + Function.ByteToHex(cipherText));
	    
	    // Now we need to Base64-encode it for ascii display
	    BASE64Encoder encoder = new BASE64Encoder();
	    String output = encoder.encode(cipherText);
	    
	    // Output: Encrypt Primary Key by Secondary Key
	    System.out.println("Ciphertext: "+output);
	    
	    
	    
	    /* Create a keystore and place the key in it
	     * We're using a jceks keystore, which is provided by Sun's JCE.
	     * If you don't have the JCE installed, you can use "JKS",
	     * which is the default keystore. It doesn't provide
	     * the same level of protection however.
	     */
	    KeyStore keyStore = KeyStore.getInstance(KEYSTORE_TYPE);

	    // This initializes a blank keystore
	    keyStore.load(null, null);

	    // Now we add the key to the keystore, protected
	    // by the password.
	    keyStore.setKeyEntry(KEYSTORE_ALIAS, key, password, null);

	    // Store the password to the filesystem, protected
	    // by the same password.
	    FileOutputStream fos = new FileOutputStream(FILENAME);
	    keyStore.store(fos, password);
	    fos.close();
	}
	
	public static byte[] decryptPrimaryKey(String key, String keystoreFile) throws Exception {
		String encryptText = null;
		String pin = KEYSTORE_PIN;
		char[] password = new char[pin.length()];
		pin.getChars(0, pin.length(), password, 0);

		KeyStore ks = KeyStore.getInstance(KEYSTORE_TYPE);
		InputStream readStream = new FileInputStream(keystoreFile);
		ks.load(readStream, password);
		Key key2 = ks.getKey(KEYSTORE_ALIAS, password);
		readStream.close();
		
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] key1 = decoder.decodeBuffer(key);
		
		Cipher cipher = Cipher.getInstance(CRYPTOKEY_TRANSFORMATION);
	    cipher.init(Cipher.DECRYPT_MODE, key2);

	    return cipher.doFinal(key1);
	}
	
}
