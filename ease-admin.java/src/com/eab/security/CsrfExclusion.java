package com.eab.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.eab.common.Constants;
import com.eab.common.Log;

public class CsrfExclusion {
	private static Map<String, Integer> exList = null;
	private static final int MODE_FREE = 0;				// No validation on CSRF Token.
	
	public CsrfExclusion() {
		initial();
	}
	
	public static void initial() {
		if (exList == null) {
			// Register exclusion list of URL before Login is completed.
			exList = new HashMap<String, Integer>();
			
			// Exclusion List.  Non-defined Servlet must have valid CSRF token when submission (default: MODE_ONCE).
			SysPrivilegeMgr prMgr = new SysPrivilegeMgr();
			ArrayList<String> urlList = null;
			try {
				urlList = prMgr.getPublicList(Constants.LANG_DEFAULT);
				exList.put("/", MODE_FREE);	//Root Path
				
				if (urlList != null && urlList.size() > 0) {
					for(String url : urlList) {
						exList.put(url, MODE_FREE);
						Log.debug("Public URL: " + url);
					}
				}
			} catch(Exception e) {
				Log.error(e);
			}
			
			urlList = null;
			prMgr = null;
		}
	}
	
	public static boolean needTokenCheck(String pathUrl) {
		boolean result = true;	// Default: need checking.
		
		try {
			initial();
			if (pathUrl != null) {
				if ("/".equals(pathUrl)) {
					result = false;		// No Need for root path.
					Log.debug("{CsrfExclusion needTokenCheck} result(1)=" + result + " (No Need for root path)");
				} else {
					if (exList.containsKey(pathUrl)) {
						if (MODE_FREE == exList.get(pathUrl)) {
							result = false;		// No Need for MODE_FREE.
							Log.debug("{CsrfExclusion needTokenCheck} result(2)=" + result + " (No Need for MODE_FREE)");
						} else {
							result = true;
							Log.debug("{CsrfExclusion needTokenCheck} result(3)=" + result);
						}
					} else {
						result = true;		// Need for non-exist in Exclusion List 
						Log.debug("{needTokenCheck} result(4)=" + result + " (Need for non-exist in Exclusion List)");
					}
				}
			}
		} catch(Exception e) {
			Log.error(e);
			result = true;
		}
		
		return result;
	}
}
