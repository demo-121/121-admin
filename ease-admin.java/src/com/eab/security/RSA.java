package com.eab.security;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.eab.common.Log;
import com.eab.model.RSAKeyPair;

public class RSA {
	public final static String CRYPTO_ALGORITHM = "RSA/ECB/OAEPWithSHA-1AndMGF1Padding";
	public final static int CRYPTO_KEYSIZE = 2048;
	public final static String CRYPTO_JAVASCRIPT = "RSA/ECB/OAEPWithSHA-1AndMGF1Padding";
	
	private RSAKeyPair keypair = null;
	private KeyPairGenerator kpg = null;
	private KeyFactory fact = null;
	private RSAPublicKeySpec pub = null;
	
	public boolean GenerateKey() {
		boolean success = false;
		
		try {
			kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(CRYPTO_KEYSIZE);
			KeyPair kp = kpg.generateKeyPair();
			
			keypair = new RSAKeyPair();
			keypair.setPublicKey(kp.getPublic());
			keypair.setPrivateKey(kp.getPrivate());
			
			pub = new RSAPublicKeySpec(BigInteger.ZERO, BigInteger.ZERO);
			fact = KeyFactory.getInstance("RSA");
			pub = fact.getKeySpec(keypair.getPublicKey(), RSAPublicKeySpec.class);
			
			keypair.setHtmlUsedModulus(pub.getModulus().toString(16));
			
			success = true;
		} catch(NoSuchAlgorithmException e) {
			Log.error(e);
	    } catch(InvalidKeySpecException e) {
	    	Log.error(e);
	    } catch(Exception e) {
			Log.error(e);
		}
		
		return success;
	}
	
	public boolean Import(RSAKeyPair keypair) {
		boolean success = false;
		
		this.keypair = keypair;
		success = true;
		
		return success;
	}
	
	public RSAKeyPair Export() {
		return keypair;
	}
	
	public PublicKey GetPublicKey() {
		return keypair.getPublicKey();
	}
	
	public PrivateKey GetPrivateKey() {
		return keypair.getPrivateKey();
	}
	
	public String GetHTMLUsedModulus() {
		return keypair.getHtmlUsedModulus();
	}
	
	public String decryptJS(String encryptedText) {
		Key privateKey = keypair.getPrivateKey();
		Cipher cipher = null;
		BigInteger textInt = new BigInteger(encryptedText, 16);
		byte[] decryptedText = new byte[1];
		
		try {
			cipher = Cipher.getInstance(CRYPTO_JAVASCRIPT);
			byte[] textBytes = textInt.toByteArray();
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			decryptedText = cipher.doFinal(textBytes);
		} catch(NoSuchAlgorithmException e) {
			Log.error(e);
		} catch(NoSuchPaddingException e) { 
			Log.error(e);
		} catch(InvalidKeyException e) {
			Log.error(e);
		} catch(IllegalBlockSizeException e) {
			//Log.error(e);
			Log.error("IllegalBlockSizeException: " + e.getMessage());
		} catch(BadPaddingException e) {
			Log.error(e);
		} catch(Exception e) {
			Log.error(e);
		}
		
		return new String(decryptedText);
	}
	
	public static byte[] encrypt(String text, PublicKey key) {
		byte[] cipherText = null;
		try {
			// get an RSA cipher object and print the provider
			final Cipher cipher = Cipher.getInstance(CRYPTO_ALGORITHM);
			// encrypt the plain text using the public key
			cipher.init(Cipher.ENCRYPT_MODE, key);
			cipherText = cipher.doFinal(text.getBytes());
		} catch (Exception e) {
			Log.error(e);
		}
		return cipherText;
	}
	
	public static String decrypt(byte[] text, PrivateKey key) {
		byte[] dectyptedText = null;
		try {
			// get an RSA cipher object and print the provider
			final Cipher cipher = Cipher.getInstance(CRYPTO_ALGORITHM);
		
			// decrypt the text using the private key
			cipher.init(Cipher.DECRYPT_MODE, key);
			dectyptedText = cipher.doFinal(text);
		} catch (Exception e) {
			Log.error(e);
		}
		return new String(dectyptedText);
	}
}
