package com.eab.security;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import com.eab.common.Constants;
import com.eab.common.Log;
import com.eab.security.keystore.KeystoreUtil;

public class DESede {
//	private final static String DESEDE_ALGORITHM = "DESede";
//	private final static String DESEDE_TRANSFORMATION = "DESede/CBC/PKCS5Padding";
	private final static String DESEDE_ALGORITHM = "TripleDES";
	private final static String DESEDE_TRANSFORMATION = "TripleDES/ECB/PKCS5Padding";
	
	private final static String KEYSTORE_ALIAS = "key.121.eabsystems";
	private final static String KEYSTORE_FILENAME = Constants.APP_REAL_PATH + "WEB-INF/keystore.eab";
	private final static String KEYSTORE_PIN = "ko7K5x2V";
	
	private Cipher ecipher = null;
	private Cipher dcipher = null;
	private Key key = null;

	private static DESede desede;

	public static DESede getInstance() { //Singleton
		if (desede == null)
			desede = new DESede();

		return desede;
	}

	private DESede() {
		try {

			String primKey = "iIkyj8oubB4oA3GhJw1mkgWDo6OAN2gGfRaTLlPSXMY=";
			String keyfile = Constants.APP_REAL_PATH;
			if (!"/".equals(Constants.APP_REAL_PATH.substring(Constants.APP_REAL_PATH.length()-1)) 
					&& !"\\".equals(Constants.APP_REAL_PATH.substring(Constants.APP_REAL_PATH.length()-1)))
				keyfile += File.separator;
			keyfile += "WEB-INF/keystore.eab";
//			key = KeystoreUtil.decryptPrimaryKey(primKey, KEYSTORE_FILENAME);
			key = KeystoreUtil.decryptPrimaryKey(primKey, keyfile);
			
			// Create a cipher using that key to initialize it
			ecipher = Cipher.getInstance(DESEDE_TRANSFORMATION);
		    ecipher.init(Cipher.ENCRYPT_MODE, key);
			dcipher = Cipher.getInstance(DESEDE_TRANSFORMATION);
			dcipher.init(Cipher.DECRYPT_MODE, key);
		} catch (Exception e) {
			Log.error(e);
		}
	}
	
	public String encrypt(String text) {
		String output = null;
		
		try {
			// Encode the string into bytes using utf-8
			byte[] utf8 = text.getBytes("UTF8");

			// Encrypt
			byte[] enc = ecipher.doFinal(utf8);

			// Encode bytes to base64 to get a string
			output = new sun.misc.BASE64Encoder().encode(enc);
		} catch (Exception e) {
			Log.error(e);
		}
		return output;
	}
	
	public String decrypt(String encryptText) {
		String output = null;
		
		try {
			// Decode base64 to get bytes
			byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(encryptText);
		
			// Decrypt
			byte[] utf8 = dcipher.doFinal(dec);
		
			// Decode using utf-8
			output = new String(utf8, "UTF8");
		} catch (Exception e) {
			Log.error(e);
		}
		return output;
	}
}
