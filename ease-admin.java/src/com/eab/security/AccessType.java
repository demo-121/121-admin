package com.eab.security;

public class AccessType extends Object {
	public final static int PUBLIC = 1;
	
	public final static int LOGIN_WIP = 2;
	
	public final static int LOGIN_READY = 3;
}
