package com.eab.security;

import java.util.Arrays;
import java.util.List;

public class CharBlacklist {
	private final static List<String> CHAR_LIST = Arrays.asList(";", "<", ">", "\"", "(", ")", "'");
	private final static List<String> CHAR_MAP = Arrays.asList("&#59;", "&lt;", "&gt;", "&quot;", "&#40;", "&#41;", "&#39;");
	
	public static List<String> list() {
		return CHAR_LIST;
	}
	
	public static String map(String character) {
		if (CHAR_LIST.contains(character)) {
			return CHAR_MAP.get(CHAR_LIST.indexOf(character));
		}
		
		return character;	// Return original if not in list.
	}
	
	public static String encode(String text) {
		StringBuffer sb = new StringBuffer(text.length());
		// true if last char was blank
		boolean lastWasBlankChar = false;
		int len = text.length();
		char c;
		
		for (int i = 0; i < len; i++) {
			c = text.charAt(i);
			boolean isMatch = false;
			
			if ( CHAR_LIST.indexOf(c) >= 0 ) {
				sb.append(CHAR_MAP.get(CHAR_LIST.indexOf(c)));
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}
}
