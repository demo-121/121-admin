package com.eab.security;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Field;
import com.eab.model.MenuItem;
import com.eab.model.MenuList;
import com.eab.model.system.FuncBean;
import com.eab.model.system.SysParamBean;

public class SysPrivilegeMgr {
	
	/***
	 * Load Left Menu function list.
	 * @param roleCode: Role Code
	 * @param langCode: Language Code (ISO 639-1)
	 * @return Function Object
	 * @throws SQLException
	 */
	public MenuList getMenuList(String roleCode, String langCode) throws SQLException {
		MenuList output = null;
		
		ArrayList<FuncBean> result = getFunc(roleCode, langCode, "Y", null, null);
		if (result != null && result.size() > 0) {
			output = new MenuList();
			
			for(FuncBean func: result) {
				MenuItem item = new MenuItem();
				item.setCode( func.getCode() );
				item.setTitle( func.getName() );
				item.setId( func.getUrl() );
				item.setSeq( func.getDisplaySeq() );
				
				// Try to get sub level by Code
				MenuList subMenu = getSubMenu(roleCode, langCode, func.getCode(), 0);
			}
		}
		
		return output;
	}
	
	/***
	 *  Recursive function for drill down all sub menu.
	 */
	private MenuList getSubMenu(String roleCode, String langCode, String uplineCode, int level) throws SQLException {
		MenuList output = null;
		
		// Prevent endless loop, limit recursive not more than 10 level.
		if (level <= 10) {
			ArrayList<FuncBean> result = getFunc(null, langCode, "Y", null, uplineCode);
			if (result != null && result.size() > 0) {
				for(FuncBean func: result) {
					output = new MenuList();
					
					MenuItem item = new MenuItem();
					item.setCode( func.getCode() );
					item.setTitle( func.getName() );
					item.setId( func.getUrl() );
					item.setSeq( func.getDisplaySeq() );
					
					// Try to get sub level by Code
					MenuList subMenu = getSubMenu(roleCode, langCode, func.getCode(), level + 1);
				}
			}
		}
		
		return output;
	}
	
	/***
	 * Load Public URL list which allows access without login.
	 * @param langCode: Language Code (ISO 639-1)
	 * @return List of URL
	 * @throws SQLException
	 */
	public ArrayList<String> getPublicList(String langCode) throws SQLException {
		ArrayList<String> output = new ArrayList<String>();
		
		ArrayList<FuncBean> result = getFunc(null, langCode, null, new int[]{1}, null);
		if (result != null && result.size() > 0) {
			for(FuncBean func: result) {
				output.add(func.getUrl());
			}
		}
		
		return output;
	}
	
	public ArrayList<FuncBean> getFunc(String roleCode, String langCode) throws SQLException {
		return getFunc(roleCode, langCode, null, null, null);
	}
	
	public ArrayList<FuncBean> getFunc(String roleCode, String langCode, String showMenu, int[] accessCtrl, String uplineCode) throws SQLException {
		ArrayList<FuncBean> output = new ArrayList<FuncBean>();
		DBManager dbm = null;
		ResultSet rs = null;
		Connection conn = null;
    	
    	try {
    		dbm = new DBManager();
			conn = DBAccess.getConnection();
    		String sql = "select func.func_code ";
    		sql += ", nvl((select name_desc from sys_name_mst name where name.lang_code = " + dbm.param(langCode, DataType.TEXT) + " and name.status = 'A' and name.name_code = func.name_code)";
    		sql += "     ,(select name_desc from sys_name_mst name where name.lang_code = 'en' and name.status = 'A' and name.name_code = func.name_code)) func_name";
    		sql += ", func.func_uri";
    		sql += ", func.show_menu";
    		sql += ", func.access_ctrl";
    		sql += ", func.disp_seq";
    		sql += ", func.upline_func_code";
    		sql += " from sys_func_mst func";
    		if (roleCode != null) sql += ", sys_user_role_func ctrl";
    		sql += " where 1 = 1";
    		if (roleCode != null) sql += " and ctrl.role_code = " + dbm.param(roleCode, DataType.TEXT) + "";
    		sql += " and func.status = 'A'";
    		if (uplineCode != null) sql += " and func.upline_func_code = " + dbm.param(uplineCode, DataType.TEXT) + "";
    		if (roleCode != null) sql += " and ctrl.status = 'A'";
    		if (roleCode != null) sql += " and func.func_code = ctrl.func_code";
    		
    		if ("Y".equals(showMenu) || "N".equals(showMenu)) {
    			sql += " and func.show_menu = " + dbm.param(showMenu, DataType.TEXT) + "";
    		}
    		/***
    		 *  1: Public
    		 *  2: Login In Progress (before able to access home function like T&C is not yet accepted.)
    		 *  3: After Login
    		 */
    		if (accessCtrl != null && accessCtrl.length > 0) {
    			sql += " and func.access_ctrl in (";
    			for (int i=0; i<accessCtrl.length; i++) {
    				sql += dbm.param(accessCtrl[i], DataType.INT);
    				if (i < accessCtrl.length - 1) {
    					sql += ",";	// add comma if not last item
    				}
    			}
    			sql += ") ";
    		}
    		
    		sql += " order by func.disp_seq";
    		rs = dbm.select(sql, conn);
			if (rs != null) {
				while (rs.next()) {
					String outCode = rs.getString("func_code");
					String outName = rs.getString("func_name");
					String outUri = rs.getString("func_uri");
					String outShow = rs.getString("show_menu");
					String outoutAccessCtrlStr = rs.getString("access_ctrl");
					int outAccessCtrl = (outoutAccessCtrlStr != null)? Integer.parseInt(outoutAccessCtrlStr) : 0;
					String outDispSeqStr = rs.getString("disp_seq");
					int outDispSeq = (outDispSeqStr != null)? Integer.parseInt(outDispSeqStr) : 0;
					String outUpline = rs.getString("upline_func_code");
					
					FuncBean funcObj = new FuncBean();
					funcObj.setCode(outCode);
					funcObj.setName(outName);
					funcObj.setUrl(outUri);
					funcObj.setShowMenu( ("Y".equals(outShow))? true : false );
					funcObj.setAccessCtrl(outAccessCtrl);
					funcObj.setDisplaySeq(outDispSeq);
					funcObj.setUpline(outUpline);
					
					output.add(funcObj);
				}
			}
    	} catch(SQLException e) {
    		throw e;
    	} catch(Exception e) {
    		Log.error(e);
    	} finally {
    		dbm = null;
    		rs = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
    	}
    	return output;
	}
	
	public List<String> getGrandedURIList(String userRole, List<String> uriList) {
		
		
//		List<String> gUriList = new ArrayList<String>();
		List<String> afuncList = new ArrayList<String>();

		DBManager dbm = null;
		ResultSet rs = null;
		Connection conn = null;
    	
    	try {
    		dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			// get available user func code
			List<String> roleFuncList = new ArrayList<String>();
			String sql = "SELECT func_code FROM sys_user_role_func WHERE status = 'A' AND role_code = " + dbm.param(userRole, DataType.TEXT);
			rs = dbm.select(sql, conn);
			dbm.clear();
			while(rs.next()) {
				roleFuncList.add(rs.getString(1));
			}
			rs.close();
			
			// build the func code map tree
			sql = "SELECT func_code ";
    		sql += ", func_uri, module";
    		sql += ", name_code";
    		sql += ", access_ctrl";
    		sql += ", upline_func_code";
    		sql += " FROM sys_func_mst";
    		sql += " WHERE status = 'A'";
    		sql += " AND access_ctrl > 3";
    		sql += " ORDER BY disp_seq ASC";
			rs = dbm.select(sql, conn);
			dbm.clear();
			
			Map<String, Field> rightMap = new HashMap<String, Field>();
			List<Field> rights = new ArrayList<Field>();
			// id 			is code 
			// title 		is name
			// type 		is parent code(upline)
			// value		is uri
			// displaySeq 	is access control
			// items 		is sub right
			Field right = null;
			while(rs.next()) {
				int level = rs.getInt("access_ctrl");
				String code = rs.getString("func_code");
				String name = rs.getString("name_code");
				String module = rs.getString("module");
				String uri = rs.getString("func_uri");
				if (uri != null && !uri.isEmpty()) { 
					uri += (module != null && !module.isEmpty())?("/"+ module):"";
				}
				String upline = rs.getString("upline_func_code");
								
				if (upline == null || upline.isEmpty()) {
					if (rightMap.containsKey(code)) {
						right = rightMap.get(code);
						right.setTitle(name);
					} else {
						right = new Field(code, null, name);
						right.setItems(new ArrayList<Field>());
						rightMap.put(code, right);	
					}
					right.setValue(uri);
					right.setDetailSeq(level);
					rights.add(right);
				} else {
					Field parent = null;
					if (rightMap.containsKey(upline)) {
						parent = rightMap.get(upline);
					} else {
						parent = new Field(upline);
						parent.setItems(new ArrayList<Field>());						
						rightMap.put(upline, parent);
					}
					
					if (rightMap.containsKey(code)) {
						right = rightMap.get(code);
						right.setTitle(name);
						right.setType(upline);
					} else {
						right = new Field(code, upline, name);
						right.setItems(new ArrayList<Field>());
						rightMap.put(code, right);	
					}
					
					right.setValue(uri);
					right.setDetailSeq(level);
					parent.getItems().add(right);
				};
			}
			rs.close();
			
			// looping the right root
			for (Field rr : rights) {
				checkRights(rr, roleFuncList, afuncList);
			}
			
			// get granded uri
			if (uriList != null) {
				for (String func : afuncList) {
					if (rightMap.containsKey(func)) {
						String uri = (String) rightMap.get(func).getValue();
						if (uri != null && !uri.isEmpty() && !uriList.contains(uri)) {
							uriList.add(uri);
						}
					}
				}
			}
			
    	} catch (Exception ex) {
    		Log.error(ex);
    	} finally {
    		try {
    			if (conn != null && !conn.isClosed()) {
    				conn.close();
    			}
    		} catch (Exception ex) {
    			Log.error(ex);
    		}
    	}
    	
		return afuncList;
	}
	
	private List<String> checkRights(Field right, List<String> roleFuncList, List<String> afList) {
		
		if (right.getDetailSeq() == 4) {
			List<String> tempAFL = new ArrayList<String>();
			if (right.getItems() != null) {
	 			for (Field sr : right.getItems()) {
//					Log.debug("checking 4: " + sr.getId() + " - " + sr.getType() );
					checkRights(sr, roleFuncList, tempAFL);
				}
			}
			// for access control == 4, access allowed if any child right allowed
			if (tempAFL.size()>0) {
				afList.add(right.getId());
				afList.addAll(tempAFL);
			}
		} else if (right.getDetailSeq() == 5) { 
			// for access control == 5, access allowed if it is link with role
			if (roleFuncList.contains(right.getId())) {
				afList.add(right.getId());
			}
			if (right.getItems() != null) {
				for (Field sr : right.getItems()) {
//					Log.debug("checking 5: " + sr.getId() + " - " + sr.getType() );
					checkRights(sr, roleFuncList, afList);
				}
			}
		} else if (right.getDetailSeq() == 6) { 
			// for access control == 6, access allowed if its parent is allowed
			if (afList.contains(right.getType())) {
				afList.add(right.getId());
			}
			if (right.getItems() != null) {
				for (Field sr : right.getItems()) {
//					Log.debug("checking 6: " + sr.getId() + " - " + sr.getType() );
					checkRights(sr, roleFuncList, afList);
				}
			}
		}
				
		return afList;
	}

	public boolean hasPrivilege(String roleCode, String uriPath) throws SQLException {
		boolean output = false;
		DBManager dbm = null;
		Connection conn = null;
		
		try {
    		dbm = new DBManager();
			conn = DBAccess.getConnection();
    		String sql = "select 1 cnt ";
    		sql += " from sys_func_mst func, user_role_func ctrl";
    		sql += " where 1 = 1";
    		sql += "   and ctrl.role_code = " + dbm.param(roleCode, DataType.TEXT);
    		sql += "   and func.func_uri = " + dbm.param(roleCode, DataType.TEXT);
    		sql += "   and func.status = 'A'";
    		sql += "   and ctrl.status = 'A'";
    		sql += "   and func.func_code = ctrl.func_code";
//    		sql += " union";
//    		sql += " select 1 cnt";
//    		sql += " from sys_func_mst func";
//    		sql += " where 1 = 1";
//    		sql += "   and func.func_uri = " + dbm.param(roleCode, DataType.TEXT);
//    		sql += "   and access_ctrl in (1,2)";	// 1 & 2 are public functions or Login Process.
//    		sql += "   and func.status = 'A'";
    		
    		String result = dbm.selectSingle(sql, "cnt");
    		
    		output = ("1".equals(result))? true : false;	// Get "1" = Has Privilege
		} catch(SQLException e) {
    		throw e;
    	} catch(Exception e) {
    		Log.error(e);
    	} finally {
    		dbm = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
    	}
    	
    	return output;
	}
}
