package com.eab.security;

import java.math.BigInteger;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import com.eab.common.Function;

public class AES {
	public final static String CRYPTO_ALGORITHM = "AES";
	public final static String CRYPTO_TRANSFORMATION = "AES/ECB/PKCS5Padding";
	
	/***
	 * Generate Random Salt String
	 * @return Random Salt
	 */
    public static String generateSalt() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
		String s = new Base64().encodeAsString(bytes);
        return s;
    }
    
    public static String generateKey128() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[16];
        random.nextBytes(bytes);
        return Function.ByteToStr(bytes);
    }
    
    public static String generateKey256() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[32];
        random.nextBytes(bytes);
        return Function.ByteToStr(bytes);
    }
    
    /***
     * All encryption is governed by laws of each country and often have restrictions on the strength of the encryption. 
     * One example is that in the United States, all encryption over 128-bit is restricted if the data is traveling 
     * outside of the boarder. By default, the Java JCE implements a strength policy to comply with these rules. If a 
     * stronger encryption is preferred, and adheres to the laws of the country, then the JCE needs to have access to 
     * the stronger encryption policy. Very plainly put, if you are planning on using AES 256-bit encryption, you must 
     * install the <<Unlimited Strength Jurisdiction Policy Files>>. Without the policies in place, 256-bit encryption is 
     * not possible.    
     */
//    private static byte[] key = {
//        0x74, 0x68, 0x69, 0x73, 0x49, 0x73, 0x41, 0x53, 0x65, 0x63, 0x72, 0x65, 0x74, 0x4b, 0x65, 0x79		//128-bit
//    };	//"thisIsASecretKey";
    
    /***
     * Encrypt Text String by AES-256
     * @param plainText - Text String to be encrypted
     * @return String Array Encrypted String
     * @throws Exception
     */
//    public static String encrypt(String plainText) throws Exception {
//    	return encrypt(plainText, Function.ByteToStr(key));
//    }
    
	public static String encrypt(String plainText, String keyText) throws Exception {
    	byte[] key = Function.StrToByte(keyText);
    	
        Cipher cipher = Cipher.getInstance(CRYPTO_TRANSFORMATION);
        final SecretKeySpec secretKey = new SecretKeySpec(key, CRYPTO_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        final String encryptedString = Base64.encodeBase64String(cipher.doFinal(plainText.getBytes()));
        return encryptedString;
	}
	
	/***
	 * Decrypt Text String by AES
	 * @param encryptedText - Text String to be decrypted
	 * @return Decrypted Text String
	 * @throws Exception
	 */
//	public static String decrypt(String encryptedText) throws Exception {
//		 return decrypt(encryptedText, Function.ByteToStr(key));
//	}
	
    public static String decrypt(String encryptedText, String keyText) throws Exception {
    	byte[] key = Function.StrToByte(keyText);
    	
        Cipher cipher = Cipher.getInstance(CRYPTO_TRANSFORMATION);
        final SecretKeySpec secretKey = new SecretKeySpec(key, CRYPTO_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        final String decryptedString = new String(cipher.doFinal(Base64.decodeBase64(encryptedText.replace(" ", "+"))));
        return decryptedString;
    }
    
}
