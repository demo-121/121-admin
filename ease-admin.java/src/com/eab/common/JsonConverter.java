package com.eab.common;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonConverter {

	public static JsonElement convertObj(Object obj) {
		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		return gson.toJsonTree(_convertObj(obj));
	}

	private static Object _convertObj(Object obj) {
		if(obj == null){
			return null;
		}
		if (obj.getClass() == org.json.JSONObject.class) {
			return new JsonParser().parse(obj.toString()).getAsJsonObject();
		} else if (obj.getClass() == org.json.JSONArray.class) {
			List<Object> list = new ArrayList<Object>();
			for (int i = 0; i < ((JSONArray) obj).length(); i++) {
				list.add(_convertObj(((JSONArray) obj).get(i)));
			}
			return list;
		} else if (obj.getClass() == String.class) {
			try {
				return new JsonParser().parse(obj.toString()).getAsJsonObject();
			} catch (Exception e) {
				return obj;
			}
		} else {
			return obj;
		}
	}

	public static JsonObject readJsonFromFile(String filePath) {

		JsonObject jsonFile = null;
		BufferedReader br = null;
		StringBuilder sb;

		try {
			br = new BufferedReader(new FileReader(filePath));
			sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}

			jsonFile = convertObj(sb.toString()).getAsJsonObject();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}
		return jsonFile;
	}

}
