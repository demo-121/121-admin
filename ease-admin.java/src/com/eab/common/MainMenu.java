package com.eab.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.dao.system.Name;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.MenuItem;
import com.eab.model.MenuList;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.security.SysPrivilegeMgr;

public class MainMenu {

	private static int MAX_RECUR_LEVEL = 10;
	
	public MainMenu() {}

	public MenuList loadFromDB(HttpServletRequest request, String roleCode) throws SQLException {
		Connection conn = null;
		MenuList list = null;

		try {
			conn = DBAccess.getConnection();
			SysPrivilegeMgr privilegeMgr = new SysPrivilegeMgr();
			List<String> funcList = privilegeMgr.getGrandedURIList(roleCode, null);
			list = loadFromDB(request, funcList, 0, conn);
		} catch (SQLException e) {
			throw e;
		} finally {
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}

		return list;
	}

	private MenuList loadFromDB(HttpServletRequest request, List<String> grandedUriList, int level, Connection conn) throws SQLException {
		String message = null;
		DBManager dbm = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		MenuList list = null;
		
		String uriStr = "";
		for (String uri : grandedUriList) {
			uriStr += (uriStr.length()==0?"(":",") + "'" + uri + "'";
		}
		uriStr += ")";

		// Prevent endless recursive. Max level 10;
		if (level >= MAX_RECUR_LEVEL)
			return null;

		try {
			dbm = new DBManager();

			String sql = " select func_code, name_code, func_uri, upline_func_code, disp_seq, module";
			sql += " from sys_func_mst ";
			sql += " where status='A' ";
			sql += " and show_menu='Y' ";
			
			if (grandedUriList.size() > 0) {
				sql += " and (func_code in " + uriStr +" or access_ctrl <= 3)";
			} else {
				sql += " and access_ctrl <= 3";
			}
			// if (upFunCode != null && upFunCode.length() > 0)
			// sql += " and upline_func_code=" + dbm.param(upFunCode, DataType.TEXT) + " ";
			// else
			// sql += " and upline_func_code is null ";
			sql += " order by disp_seq ";

			rs = dbm.select(sql, conn);
//			rs1 = dbm.select(sql, conn);

			if (rs != null) {
				list = new MenuList(); // Initial "list" when record is found.
				Map<String, MenuItem> map = new HashMap<String, MenuItem>();

//				List<String> menuItem = new ArrayList<>();
//				while (rs1.next()) {
//					menuItem.add(rs1.getString("name_code"));
//				}
				
				HttpSession session = request.getSession();
				UserPrincipalBean principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
				Language langObj = new Language(principal.getUITranslation());

				while (rs.next()) {
					String funcCode = rs.getString("func_code");
					String nameDesc = langObj.getNameDesc(rs.getString("name_code"));
					String uri = rs.getString("func_uri");
					String module = rs.getString("module");
					String upline = rs.getString("upline_func_code");
					int seq = rs.getInt("disp_seq");

					MenuItem item = null;
					if (map.containsKey(funcCode)) {
						item = map.get(funcCode);
					} else {
						item = new MenuItem();
					}

					Log.debug("{Menu loadFromDB} func_code=" + funcCode + ", name_code=" + nameDesc + ", uri=" + uri + ", seq=" + seq);

					item.setCode(funcCode);
					item.setTitle(nameDesc);
					item.setId(uri);
					item.setSeq(seq);
					item.setModule(module);

					// Search downline
					// item.setSubItem(loadFromDB(funcCode, level+1, conn));

					map.put(funcCode, item);

					// Add to Menu List if it is root node
					if (upline == null || upline.isEmpty()) {
						list.put(item);
					} else {
						// otherwise add to parent node
						MenuItem par = map.get(upline);
						if (par == null) {
							par = new MenuItem();
							map.put(upline, par);
						}
						if (par.getSubItems() == null) {
							par.createSubItems();
						}
						par.getSubItems().add(item);
					}
				}
			}
		} catch (SQLException e) {
			throw e;
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
		}

		return list;
	}

	public MenuList loadProductMenuFromDB(boolean isNewProduct) throws SQLException {
		Connection conn = null;
		MenuList list = null;

		try {
			conn = DBAccess.getConnection();
			list = loadProductMenuFromDB("FN.PRO.PRODS.DETAIL", 0, isNewProduct, conn);
		} catch (SQLException e) {
			throw e;
		} finally {
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}

		return list;
	}

	private MenuList loadProductMenuFromDB(String upFunCode, int level, boolean isNewProduct, Connection conn) throws SQLException {
		String message = null;
		DBManager dbm = null;
		ResultSet rs = null;
		MenuList list = null;

		// Prevent endless recursive. Max level 10;
		if (level >= MAX_RECUR_LEVEL)
			return null;

		try {
			dbm = new DBManager();

			String sql = " select func_code, name_code, func_uri, upline_func_code, disp_seq, module";
			sql += " from sys_func_mst ";
			sql += " where status='A' ";
			sql += " and show_menu='I' ";
			// if (upFunCode != null && upFunCode.length() > 0)
			sql += " and upline_func_code=" + dbm.param(upFunCode, DataType.TEXT) + " ";
			// else
			// sql += " and upline_func_code is null ";
			sql += " order by disp_seq ";

			rs = dbm.select(sql, conn);
			if (rs != null) {
				list = new MenuList(); // Initial "list" when record is found.
				Map<String, MenuItem> map = new HashMap<String, MenuItem>();
				while (rs.next()) {
					String funcCode = rs.getString("func_code");
					String nameCode = rs.getString("name_code");
					String uri = rs.getString("func_uri");
					String module = rs.getString("module");
					String upline = rs.getString("upline_func_code");
					int seq = rs.getInt("disp_seq");

					MenuItem item = null;
					if (map.containsKey(funcCode)) {
						item = map.get(funcCode);
					} else {
						item = new MenuItem();
					}

					Log.debug("{Menu loadFromDB} func_code=" + funcCode + ", name_code=" + nameCode + ", uri=" + uri + ", seq=" + seq);

					item.setCode(funcCode);
					item.setTitle(nameCode); // TODO: Get description from SYS_NAME_MST
					item.setId(uri);
					item.setSeq(seq);
					item.setModule(module);
					if (isNewProduct && !funcCode.equals("FN.PRO.PRODS.DETAIL.TASK") && !funcCode.equals("FN.PRO.PRODS.DETAIL.DETAIL")) {
						item.setDisabled(true);
					} else {
						item.setDisabled(false);
					}

					// Search downline
					// item.setSubItem(loadFromDB(funcCode, level+1, conn));

					map.put(funcCode, item);

					// Add to Menu List if it is root node
					if (upline.equals("FN.PRO.PRODS.DETAIL")) {
						list.put(item);
					} else {
						// otherwise add to parent node
						MenuItem par = map.get(upline);
						if (par == null) {
							par = new MenuItem();
							map.put(upline, par);
						}
						if (par.getSubItems() == null) {
							par.createSubItems();
						}
						par.getSubItems().add(item);
					}
				}
			}
		} catch (SQLException e) {
			throw e;
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
		}

		return list;
	}

	public boolean putToSession(HttpSession session, MenuList list) {
		boolean result = false;

		try {
			if (session != null) {
				session.setAttribute(Constants.MENU_NAME, list);
				result = true;
			}
		} catch (Exception e) {
			Log.error(e);
		}

		return result;
	}

	public MenuList getFromSession(HttpSession session) {
		MenuList output = null;

		try {
			output = (MenuList) session.getAttribute(Constants.MENU_NAME);
		} catch (Exception e) {
			Log.error(e);
		}
		
		return output;
	}
}
