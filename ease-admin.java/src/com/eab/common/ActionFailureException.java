package com.eab.common;

public class ActionFailureException extends Exception {
	/**
	 * for throw when specific handling required.
	 */
	private static final long serialVersionUID = 1L;
	
	boolean requireAlert = false;
	boolean allowRetry = false;
	
	public ActionFailureException(String message, boolean requireAlert, boolean allowRetry) {
		super(message);
		this.requireAlert = requireAlert;
		this.allowRetry = allowRetry;
	}
}
