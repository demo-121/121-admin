package com.eab.common;

import javax.servlet.http.HttpServletRequest;

public class OutputPage {
	public String getLayout(HttpServletRequest request, String pageSelection){
		return getLayout (request, pageSelection, "");
	}
		
	public String getLayout(HttpServletRequest request, String pageSelection, String rsaKey){
		 String htmlbody = "<html class=\"no-js\" lang=\"\">"
				 + "<head>"
					
			  	 + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
				 + "<meta charset=\"utf-8\">"
				 + "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"
				 + "<link rel=\"icon\" type=\"image/png\" href=\"" + request.getContextPath() + "/web/img/favicon.png\" />"
				 + "<link rel=\"apple-touch-icon\" href=\"" + request.getContextPath() + "/web/img/favicon.png\" />"
				 + "<title>121 Admin</title>"
				 + "<meta name=\"description\" content=\"\">"
				 + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
				 + "<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">"
				 + "<link rel=\"stylesheet\" type=\"text/css\" href=\"../web/main.css\">"		 
				 + "<script src=\"" + request.getContextPath() + "/web/js/crypto/jsbn.js\"></script>"
				 + "<script src=\"" + request.getContextPath() + "/web/js/crypto/jsbn2.js\"></script>"
				 + "<script src=\"" + request.getContextPath() + "/web/js/crypto/prng4.js\"></script>"
				 + "<script src=\"" + request.getContextPath() + "/web/js/crypto/rng.js\"></script>"				 
				 + "<script src=\"" + request.getContextPath() + "/web/js/crypto/rsa.js\"></script>"
				 + "<script src=\"" + request.getContextPath() + "/web/js/crypto/rsa2.js\"></script>"
					
				 + "</head>"
					
				 + "<body>"					
				 + "<script>"
				 + " var ROOT_CONTEXT_PATH = \"" + request.getContextPath() + "\";"
				 + " var PAGE_SELECTION = \"" + pageSelection + "\";"
				 + " var publicKey = \"" + rsaKey + "\";"
				 + " var WebFontConfig = {"
				 + "   google: { families: [ 'Roboto:400,300,500:latin' ] }"
				 + " };"
				 + "  (function() {"
				 + "	var wf = document.createElement('script');"
				 + "	wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +"
				 + "	'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';"
				 + "	wf.type = 'text/javascript';"
				 + "	wf.async = 'true';"
				 + "	var s = document.getElementsByTagName('script')[0];"
				 + "	s.parentNode.insertBefore(wf, s);"
				 + "	})();"
				 + " function encryptPassword(password) {"
/*				 + " 		var rsa = new RSAKey();"
				 + " 		rsa.setPublic(publicKey, '10001');"
				 + " 		var res = rsa.encrypt(password);"
				 + " 		return res;"*/
				 + " 		return password;"
				 + " 	}"				 
				 + "</script>"
				 + "<section id=\"content\"> </section>"
				 + "<script src=\"../web/app.js\"></script>"
				 + "</body>"
					
				 + "</html>";
		 
		 return htmlbody;
	}
}
