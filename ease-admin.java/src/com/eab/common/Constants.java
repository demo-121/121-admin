package com.eab.common;

import java.util.Arrays;
import java.util.List;

public class Constants extends Object {

	// Static Value which cannot be changed
	public final static String 	APP_NAME 			= "121 Admin";
	public final static String 	COMPANY_NAME 		= "EAB Systems Hong Kong Limited";

	public final static String 	TOKEN_KEY_PREFIX 	= "CSRF_";
	public final static String 	TOKEN_KEY_PENDING 	= "CSRF_PENDING_KEY";
	public final static String 	TOKEN_NOT_DELETE 	= "_NOTDELETE";

	public final static String 	CAPTCHA_TEXT 		= "CaptchaText";
	public final static String 	CAPTCHA_DATETIME 	= "CaptchaDateTime";
	public final static int 	CAPTCHA_SIZE 		= 5;

	public final static String 	CRYPTO_KEYPAIR_NAME = "CRYPTO_KEYPAIR";
	public final static String 	USER_PRINCIPAL 		= "UserPrincipal";
	public final static int 	CRYPTO_KEY_LENGTH 	= 16;

	public final static String 	LOGIN_ERROR_CODE 	= "LOGIN_ERROR";
	public final static String 	DEFAULT_LANG 		= "EN";
	
	public final static String ZONE_ID = "Singapore";

	public final static String 	MENU_NAME 			= "USER_MENU_TREE";

	public final static String 	MESSAGE_DONT_SHOW_DEBUG = " should not be shown in UAT / Production environment.";

	public final static String 	LANG_NAME 			= "USER_LANG";

	public static final String 	USERTYPE_ADMIN 		= "A";

	// Dynamic Value which can be changed
	public static boolean APP_STARTUP_COMPLETE 		= false;

	public static int LOGIN_FAIL_COUNT_MAX 			= 3; // 0 = No One allows login; -1 = Disable Fail Count;

	public static String DATABASE_TYPE 				= ""; // e.g. oracle
	public static String DATASOURCE_STRING 			= ""; // e.g. java:jboss/ICONFIG-DS
	public static String DATASOURCE_API_STRING 		= ""; // e.g. java:jboss/ICONFIG-DS
	public static String XFRAME_OPTION_DEFAULT 		= "SAMEORIGIN";

	public static boolean ENABLE_CSRF_TOKEN 		= true;
	public static boolean ENABLE_SINGLE_LOGIN 		= true; // Each User ID only allow single login session.

	public static String SECURITY_SCAN_EXT_PATH_WEB 		= "/web/";
	public static String SECURITY_SCAN_EXT_PATH_EASE 		= "/ease/";
	public static List<String> SECURITY_SCAN_EXT_EXCLUSION 	= Arrays.asList("js", "css", "htm", "html", "png", "bmp", "jpg", "gif", "pdf", "ico");
	public static List<String> SECURITY_SCAN_SPECIAL_CHARS 	= Arrays.asList("<", ">", "\"", "(", ")", "'", ";");
	public static String SERVER_IP_ADDR 			= "0.0.0.0";
	public static String APP_STARTUP_KEY 			= "a1B2c3D4E911ABdfg3fd2jff0932j";

	public static String APP_REAL_PATH 				= "D:\\Workplace_Eclipse_JEE\\ease-admin\\WebContent";

	public static String LANG_DEFAULT 				= "en"; // SYS_LANG_MST
	public static int SEARCH_RESULT_DEFAULT_PAGE_SIZE = 10;
	public static int SEARCH_RESULT_LIMIT 			= 100;
	
	public static String DATETIME_PATTERN_AS_AXA 	= "dd-MM-yyyy HH:mm:ss";
	public static String DATETIME_PATTERN_AS_DATE_ONLY ="yyyy-MM-dd";
	public static String DATETIME_PATTERN_AS_ADMIN_DATE ="dd/MM/yyyy";
	public static String DATETIME_PATTERN_AS_SQL_DATE ="yyyy-MM-dd HH:mm:ss";
	public static String DATE_PATTERN_AS_REPORT_DATE ="yyyy-MM-dd";

	//public static int RELEASE_SCHEDULE_HOURS = 2;
	public static String TIMEZONE_ID = "Singapore"; 
	
	public class dataFormats{
		public final static String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	}

	// Action Types for mapping the actions on client side
	public class ActionTypes {
		public final static String CONFIG_LOGIN 		= "LOGIN";
		public final static String CONFIG_LOGOUT 		= "LOGOUT";
		public final static String CONFIG_SHOW_ERROR 	= "SHOW_ERROR";
		public final static String CONFIG_SHOW_TERMS 	= "SHOW_TERMS";
		public final static String CONFIG_SHOW_HOME 	= "SHOW_HOME";
		public final static String CONFIG_ACCEPT 		= "ACCEPT";
		public final static String CONFIG_DECLINE 		= "DECLINE";
		public final static String PAGE_REDIRECT 		= "REDIRECT";
		public final static String CHANGE_PAGE 			= "CHANGE_PAGE";
		public final static String UPDATE_MENU_STATE 	= "UPDATE_MENU_STATE";
		public final static String CHANGE_COMPANY 		= "CHANGE_COMPANY";
		public final static String CHANGE_CONTENT 		= "CHANGE_CONTENT";
		public final static String TASK_LIST 			= "TASK_LIST";
		public final static String TASK_INFO 			= "TASK_INFO";
		public final static String SHOW_DIALOG 			= "SHOW_DIALOG";
		public final static String BACK_PAGE 			= "BACK_PAGE";
		public final static String ASSIGN_PAGE 			= "ASSIGN_PAGE";
		public final static String ASIGN_UPDATE_VALUES 	= "ASIGN_UPDATE_VALUES";
		public final static String RELOAD_PAGE 			= "RELOAD_PAGE";
		public final static String RESET_VALUES 		= "RESET_VALUES";
		public final static String NEEDS_CHANGE_PAGE 	= "NEEDS_CHANGE_PAGE";
		public final static String PREVIEW_PDF 			= "PREVIEW_PDF";
		
		public final static String UPDATE_UPLOAD_STATE 	= "UPDATE_UPLOAD_STATE";
		
	}

	public class SessionKeys {
		public final static String SEARCH_RESULT 		= "SEARCH_RESULT";
		public final static String CURRENT_MODULE 		= "CURRENT_MODULE";
		public final static String CURRENT_TEMPLATE 	= "CURRENT_TEMPLATE";
		public final static String CURRENT_TABLE_TEMPLATE = "CURRENT_TABLE_TEMPLATE";
		public final static String FILTER_KEY 			= "FILTER_KEY";
	}

	public class PageIDs {
		public final static String PRODUCT_LIST 		= "/Products";
		public final static String BI_LIST 				= "/BeneIllus";
	}

	public class LoginMsg {
		public final static String UPDATE_FAILED 		= "UPDATE_FAILED";
		public final static String REPEATED_PASSWORD 	= "DUPLICATED_PASSWORD";
		public final static String WRONG_PARAMETER 		= "WRONG_PARAMETER";
		public final static String WRONG_PASSWORD 		= "WRONG_PASSWORD";
		public final static String INVALID_INPUT 		= "INVALID_INPUT";
		public final static String EMPTY_INPUT 			= "EMPTY_INPUT";

	}
	
	public class ProductPage {
		public final static String TASK 				= "TaskInfo";
		public final static String DETAIL 				= "ProdDetail";
		public final static String FEATURES 			= "ProdFeatures";
		public final static String CHARGES 				= "ProdCharges";
		public final static String PROPERTIES 			= "ProdProperties";
		public static final String LIMITATION 			= "ProdLimitation";
		public class Limitation{
			public static final String BEN_LIM 			= "benLim";
			public static final String PREM_LIM 		= "premLim";
		} 

		public static final String RATE 				= "ProdRate";
		public static final String FUND_PARAM 			= "ProdFundParam";
		
		public class FundParam{
			public static final String FUND_PARAMTER 	= "fundParam";
			public static final String NET_INVEST_RETURN = "netInvestReturn";
			public static final String STARTUP_BONUS 	= "startUpBonus";
			public static final String LOYALTY_BONUS 	= "loyaltyBonus";
			public static final String COI_RATE 		= "coiRate";
			public static final String SURRENDER_CHARGE = "surrenderChange";		
			
		}
		
		public class Rate{
			public static final String PREM_RATE 		= "premRate";
			public static final String TDC_RATE 		= "tdcRate";
			public static final String CV_RATE 			= "cvRate";
			public static final String BON_RATE 		= "bonRate";
		}

		public static final String FORMULA 				= "ProdFormulas";
		public class Formula{
			public static final String PREM_FUNC 		= "premFunc";
			public static final String SUM_INS_FUNC 	= "sumInsFunc";
			public static final String COMM_FUNC 		= "commFunc";
			public static final String DA_FUNC 			= "deathAggregationFunc";
			public static final String TSV_FUNC 		= "totalsvFunc";
			public static final String GDB_FUNC 		= "gdbFunc";
			public static final String NGDB_FUNC 		= "ngdbFunc";
			public static final String PB_FUNC 			= "pbFunc";
			public static final String RB_FUNC 			= "rbFunc";
			public static final String GSV_FUNC 		= "gsvFunc";
			public static final String NGSV_FUNC 		= "ngsvFunc";
			public static final String TDB_FUNC 		= "totaldbFunc";
			public static final String SUB_FUNC 		= "subFunc";
			public static final String LB_FUNC 			= "lbFunc";
			public static final String MATY_FUNC 		= "matyFunc";
		}

		public static final String RIDER 				= "ProdRider";
		public class Rider {
			public static final String RIDER_LIST 		= "riderList";
			public static final String AUTO_ATTACH 		= "autoAttach";
			public static final String COEXIST 			= "coexist";
		}
		public final static String ALT_OPTION 			= "ProdAltOption";
		public final static String FUND_LIST 			= "fundList";
		public final static String SELECTED_FUND 		= "selectedFund";
		public static final String COVER 				= "ProdBrochureNCover";
		
		
	}
	
	public class NeedPage {
		public final static String TASK 				= "TaskInfo";
		public final static String DETAIL 				= "NeedsDetail";
		public final static String NEEDS_TAB 			= "NeedsTab";
		public final static String ATTACHMENTS 			= "Attachments";
		public final static String NEEDS_FIN_EVAL 		= "NeedsFinEval";
		public final static String NEEDS_SELECTION 		= "NeedsSelection";
		public final static String NEEDS_PRIORITIZATION = "NeedsPrior";
		public final static String NEEDS_CONCEPT_SELLING = "NeedsConceptSelling";	
		public final static String NEEDS_RA_QUESTIONNAIRE = "NeedsRAQuest";
		public final static String NEEDS_RA_TOLERANCE 	= "NeedsRATolerance";
		
		public final static String SELECTIONS 			= "selections";
		
		
	}
	
	public class BiPage {
		public final static String TASK 				= "TaskInfo";
		public final static String DETAIL 				= "BiDetail";
		public final static String CSS_HEADER_FOOTER 	= "BiCssHeaderFooter";
		public final static String BI_PAGES 			= "BiPages";
		public final static String CLIENT_SIGN 			= "BiClientSign";
		public final static String AGENT_SIGN 			= "BiAgentSign";
		public final static String BI_IMAGE 			= "BiImage";
	}

	public class ReportPage {
		public static final String ONLINE_PAYMENT_REPORT = "onlinePaymentReport";
		public static final String SUBMISSION_CASES_REPORT = "submissionCasesReport";
		public static final String ALL_CHANNEL_REPORT 	= "allChannelReport";
		public static final String PROXY_HISTORY_REPORT = "proxyReport";
		
		public static final String BCP_REQUEST			= "bcpRequest";
		
	}

	public class PaymentMethod {
		public static final String CREDIT_CARD 			= "crCard";
		public static final String ENETS 				= "eNets";
		public static final String IPP 					= "dbsCrCardIpp";
	}

	public static final String DUMMY_PASSWORD 			= "";
	public static final String COL_COMP_CODE 			= "comp_code";
	public static final String COL_LANG_CODE 			= "lang_code";
	
	public static String SAML_META_PATH 				= "";
	public static String RESOURCE_ROOT 					= "";
}
