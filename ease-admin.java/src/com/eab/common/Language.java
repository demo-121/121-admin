package com.eab.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.dao.common.Functions;
import com.eab.dao.system.Lang;
import com.eab.dao.system.Name;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.profile.UserPrincipalBean;

public class Language {
	
	Map<String, Object> nameMap = null;
	
	public Language() {
		
	}
	
	public Language(Map<String, Object> nameMap) {
		this.nameMap = nameMap;
	}
	
	/***
	 * Get name map
	 * @return name list
	 */	
	public Map<String, Object> getNameMap(){
		return nameMap;
	}
	
	/***
	 * Get Default Language Code
	 * @return Langauge Code
	 */
	public String getDefaultLang() {
		Log.debug("{Language getDefaultLang} --> " + Constants.LANG_DEFAULT);
		return Constants.LANG_DEFAULT;
	}
	
	/***
	 * Get select language from Session
	 * @param request - HTTP Request
	 * @return Langauge Code
	 */
	public String getLang(HttpServletRequest request) {
		String output = null;
		HttpSession session = request.getSession(false);
		UserPrincipalBean principal = (session != null&&session.getAttribute(Constants.USER_PRINCIPAL)!=null)? ((UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL)): null; 
		
		if (session != null && session.getAttribute(Constants.LANG_NAME) != null) {
			output = (String) session.getAttribute(Constants.LANG_NAME);	
			Log.debug("{Language getLang} Lang=" + output + " (session)");
		} else if (principal != null && principal.getLangCode() != null) {
			output = principal.getLangCode();	
			Log.debug("{Language getLang} Lang=" + output + " (user)");
		} else {
			output = getDefaultLang();
			Log.debug("{Language getLang} Lang=" + output + " (default)");
		}
		
		if (output == null || output.length() == 0) {
			output = getDefaultLang();
		}
		Log.debug("{Language getLang} Lang=" + output + " (final)");
		
		return output;
	}

	/***
	 * Set selected default language into Session
	 * @param request - HTTP Request
	 * @return True / False
	 */
	public boolean setLang(HttpServletRequest request) {
		return setLang( request, getDefaultLang() );
	}
	
	/***
	 * Set selected language into Session
	 * @param request - HTTP Request
	 * @return True / False
	 */
	public boolean setLang(HttpServletRequest request, String langCode) {
		boolean output = false;
		//create a new session if session already does not exist
		if (request != null ) {
			HttpSession session = request.getSession(true);
			if (session != null && langCode != null) {
				session.setAttribute(Constants.LANG_NAME, langCode);
				output = true;
				
				Log.debug("{Language setLang} " + Constants.LANG_NAME + "=" + langCode);
			}
		}
		return output;
	}
	
	/***
	 * Check language code being supported by system
	 * @param langCode - Language Code
	 * @return True / False
	 */
	public boolean checkSupportLang(String langCode) {
		boolean output = false;
		DBManager dbm = null;
    	
    	try {
			dbm = new DBManager();
			
			String sql = " select count(1) cnt from SYS_LANG_MST "
					+ " where lang_code=" + dbm.param(langCode, DataType.TEXT)
					+ " and status='A'";
			String result = dbm.selectSingle(sql, "cnt");
			if ("1".equals(result)) {
				output = true;
			}
    	} catch(Exception e) {
    		Log.error(e);
    	} finally {
    		dbm = null;
    	}
		
		return output;
	}
	
	/***
	 * Get 121 System Supported Language
	 * @param request - HTTP Request
	 * @return ArrayList of Object "Lang"
	 */
	public ArrayList<Lang> getLangList(HttpServletRequest request) throws SQLException {
		ArrayList<Lang> output = null;
		DBManager dbm = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			//Get language code
			String langCode = getLang(request);
			
			String sql = "select l.lang_code, l.is_default, n.name_desc "
					+ " from sys_lang_mst l, sys_name_mst n"
					+ " where 1=1"
					+ "   and l.status = 'A'"
					+ "   and n.status = 'A'"
					+ "   and n.lang_code = " + dbm.param(langCode, DataType.TEXT)
					+ "   and l.name_code = n.name_code"
					+ " union all"
					+ " select l.lang_code, l.is_default, n.name_desc"
					+ " from sys_lang_mst l, sys_name_mst n"
					+ " where 1=1"
					+ "   and l.status = 'A'"
					+ "   and n.status = 'A'"
					+ "   and n.lang_code = " + dbm.param(Constants.LANG_DEFAULT, DataType.TEXT)
					+ "   and l.name_code = n.name_code"
					+ "   and n.name_code not in ("		// Subquery to get missing NAME using session language code.
					+ "     select nb.name_code"
					+ "     from sys_lang_mst lb, sys_name_mst nb"
					+ "     where 1=1"
					+ "       and lb.status = 'A'"
					+ "       and nb.status = 'A'"
					+ "       and nb.lang_code = " + dbm.param(langCode, DataType.TEXT)
					+ "       and lb.name_code = nb.name_code"
					+ "   )"
					+ " order by lang_code";
			rs = dbm.select(sql, conn);
			if (rs != null) {
				output = new ArrayList<Lang>();
				while (rs.next()) {
					String outLangCode = rs.getString("lang_code");
					String outIsDefault = rs.getString("is_default");
					String outNameDesc = rs.getString("name_desc");
					Lang obj = new Lang();
					obj.setCode(outLangCode);
					obj.setName(outNameDesc);
					obj.setDefault( ("Y".equals(outIsDefault)?true:false) );	// Only "Y" = Default.
					obj.setSelected( (langCode.equals(outLangCode)?true:false) );	// True if Session's Language Code is matched with current record.
					output.add(obj);
				}
			}
		} catch(SQLException e) {
    		throw e;
    	} catch(Exception e) {
    		Log.error(e);
    	} finally {
    		dbm = null;
    		rs = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
    	}
		
		return output;
	}
	
	/***
	 * Get multi-language by Name Code
	 * @param request - HTTP Request
	 * @param nameList - Name List (pre-defined Name Code)
	 * @return Ture / False
	 * @throws SQLException
	 */
	public boolean getNameList(HttpServletRequest request, List<Name> nameList) throws SQLException {
		return getNameList(request, nameList, getLang(request));
	}
	
	/***
	 * added by nathan.chan, langCode can be passed from front end
	 * @param request
	 * @param nameList
	 * @return
	 * @throws SQLException
	 */
	public boolean getNameList(HttpServletRequest request, List<Name> nameList, String langCode) throws SQLException {
		boolean output = false;
		DBManager dbm = null;
		ResultSet rs = null;
		Connection conn = null;
		Map<String, Name> nameMap = new HashMap<String, Name>();
		try {
			if (nameList != null && nameList.size() > 0) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
				//Get language code
				//String langCode = getLang(request);
				
				String sql = "select name_code, name_desc from sys_name_mst "
						+ " where status = 'A'"
						+ "   and lang_code = " + dbm.param(langCode, DataType.TEXT);
				
				String sqlNameList1 = "";
				for(Name name: nameList) {
					nameMap.put(name.getCode(), name);					
					sqlNameList1 += dbm.param(name.getCode(), DataType.TEXT);
					sqlNameList1 += ",";
				}
				sqlNameList1 = sqlNameList1.substring(0, sqlNameList1.length() - 1);
				
				sql = sql + "   and name_code in (" + sqlNameList1 + ")"
						+ " union all"
						+ " select name_code, name_desc from sys_name_mst"
						+ " where status = 'A'"
						+ "   and lang_code = " + dbm.param(Constants.LANG_DEFAULT, DataType.TEXT);
				
				String sqlNameList2 = "";
				for(Name name: nameList) {
					sqlNameList2 += dbm.param(name.getCode(), DataType.TEXT);
					sqlNameList2 += ",";
				}
				sqlNameList2 = sqlNameList2.substring(0, sqlNameList2.length() - 1);
				
				sql = sql +  "   and name_code in (" + sqlNameList2 + ")"
						+ "   and name_code not in ("
						+ "     select name_code from sys_name_mst"
						+ "     where status = 'A'"
						+ "       and lang_code = " + dbm.param(langCode, DataType.TEXT);
				
				String sqlNameList3 = "";
				for(Name name: nameList) {
					sqlNameList3 += dbm.param(name.getCode(), DataType.TEXT);
					sqlNameList3 += ",";
				}
				sqlNameList3 = sqlNameList3.substring(0, sqlNameList3.length() - 1);
				
				sql = sql +  "       and name_code in (" + sqlNameList3 + ")"
						+ "   )";
				
				rs = dbm.select(sql, conn);
				if (rs != null) {
					while (rs.next()) {
						updateName(nameMap, rs.getString("name_code"), rs.getString("name_desc"));
					}
					output = true;
				}
			}
		} catch(SQLException e) {
    		throw e;
    	} catch(Exception e) {
    		Log.error(e);
    	} finally {
    		dbm = null;
    		rs = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
    	}
		
		return output;
	}
	
	public void updateName(Map<String, Name> nameMap, String nameCode, String nameDesc) {
		Name name = nameMap.get(nameCode);
		
		if (name != null) {
			name.setDesc(nameDesc);
		}
	}
	
	public Map<String, Object> getNameMap(HttpServletRequest request, List<String> labelList ){
		try{
			ArrayList<Name> nameList = new ArrayList<Name>();
			Map<String, Object> nameMap = new HashMap<String, Object>();
			
			for(String label: labelList){
				Name nameObj = new Name();
				nameMap.put(label,"");
				nameObj.setCode(label);
				nameList.add(nameObj);
			}
			
			if(getNameList(request, nameList)){
				for(Name name: nameList){
					nameMap.replace(name.getCode(), name.getDesc());
				}
			}
			return nameMap;
		}catch(Exception e){
			Log.error(e);
		}
		return null;
	}

	public Map<String, Object> getUITranslation(HttpServletRequest request){
		DBManager dbm = null;
		Connection conn = null;
		ResultSet rs = null;
		String sql = "";
		Map<String, Object> nameMap = new HashMap<String, Object>();
		String name_code = "";
		String name_desc = "";
		
		try {
			String currentLang = getLang(request);
			
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			sql = " select name_code, name_desc "
				+ " from sys_name_mst "
				+ " where lang_code = " + dbm.param(currentLang, DataType.TEXT)
				+ " and status = 'A' ";		
							
			rs = dbm.select(sql, conn);
						
			while (rs.next()) {
				if (!rs.getString("name_code").isEmpty()) 
					name_code = rs.getString("name_code").toString();
					
				if (!rs.getString("name_desc").isEmpty()) 
					name_desc = rs.getString("name_desc").toString();
				
				nameMap.put(name_code, name_desc);
			}

		} catch (SQLException e) {
			Log.error("Language getUITranslation 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Language getUITranslation 2 ----------->" + e);
    	} finally {
    		dbm = null;
    		rs = null;
			
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
			
				}
				conn = null;
			}
    	}
		
		return nameMap;
	}
	
	public String getNameDesc(String nameCode){
		try {
			return nameMap.get(nameCode).toString();	
		} catch(Exception e) {
			Log.error("Error - Name Code Missing - " + nameCode);
			return nameCode;
		}
	}
	
	public String getNameDescWithLang(String nameCode, String langCode){
		DBManager dbm = null;
		Connection conn = null;
		String sql = "";

		try {			
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			sql = " select name_desc "
				+ " from sys_name_mst "
				+ " where lang_code = " + dbm.param(langCode, DataType.TEXT)
				+ " and name_code = " + dbm.param(nameCode, DataType.TEXT)
				+ " and status = 'A' ";		
							
			return dbm.selectSingle(sql, "name_desc", conn);
		} catch (SQLException e) {
			Log.error("Language getNameDescWithLang 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Language getNameDescWithLang 2 ----------->" + e);
    	} finally {
    		dbm = null;
			
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
			
				}
				conn = null;
			}
    	}
		return "";
	}

	public Map<String, Object> getRegularLangList(Map<String, Object> nameMap) throws Exception{
		Map<String, Object> output = new HashMap<String, Object>();
		DBManager dbm = null;
		ResultSet rs = null;
		Connection conn = null;
    	
    	try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String sql = " select name_code from sys_name_mst where is_often_use = 'Y' and status='A'";
					
			rs = dbm.select(sql, conn);
			
			if (rs != null) {
				while (rs.next()) {
					output.put(rs.getString("name_code"), nameMap.get(rs.getString("name_code")));	
				}	
				
				List<String> langNameCodes = Functions.getLangNameCode();
				
				for(int i = 0; i < langNameCodes.size(); i++) {
					output.put(langNameCodes.get(i), nameMap.get(langNameCodes.get(i)));
				}	
			}
    	} catch (SQLException e) {
			Log.error("Language getRegularLangList 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Language getRegularLangList 2 ----------->" + e);
    	} finally {
    		dbm = null;
    		rs = null;
			
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
			
				}
				conn = null;
			}
    	}
			
		return output;
	}

}
