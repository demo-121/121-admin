package com.eab.common;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;

import com.googlecode.htmlcompressor.compressor.HtmlCompressor;
import com.yahoo.platform.yui.compressor.CssCompressor;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;

public class Compressor {
	private static Logger logger = Logger.getLogger(Compressor.class.getName());

	private static class YuiCompressorErrorReporter implements ErrorReporter {
	    public void warning(String message, String sourceName, int line, String lineSource, int lineOffset) {
	        if (line < 0) {
	            logger.log(Level.WARNING, message);
	        } else {
	            logger.log(Level.WARNING, line + ':' + lineOffset + ':' + message);
	        }
	    }
	 
	    public void error(String message, String sourceName, int line, String lineSource, int lineOffset) {
	        if (line < 0) {
	            logger.log(Level.SEVERE, message);
	        } else {
	            logger.log(Level.SEVERE, line + ':' + lineOffset + ':' + message);
	        }
	    }
	 
	    public EvaluatorException runtimeError(String message, String sourceName, int line, String lineSource, int lineOffset) {
	        error(message, sourceName, line, lineSource, lineOffset);
	        return new EvaluatorException(message);
	    }
	}
	
	public static class Options {
	    public String charset = "UTF-8";
	    public int lineBreakPos = -1;
	    public boolean munge = false;
	    public boolean verbose = true;
	    public boolean preserveAllSemiColons = false;
	    public boolean disableOptimizations = false;
	}
	
	public static String compressJavaScript(String js, Options option)  {
	    Reader in = null;
	    Writer out = null;
	    try {
	        in = new InputStreamReader(new ByteArrayInputStream(js.getBytes()), option.charset);
	 
	        JavaScriptCompressor compressor = new JavaScriptCompressor(in, new YuiCompressorErrorReporter());
	        in.close(); in = null;
	 
	        out = new StringWriter();
	        compressor.compress(out, option.lineBreakPos, option.munge, option.verbose, option.preserveAllSemiColons, option.disableOptimizations);
	        return  out.toString();
		} catch (Exception e) {
			Log.error(e);
			return js;
	    } finally {
	        IOUtils.closeQuietly(in);
	        IOUtils.closeQuietly(out);
	    }
	}
	
	public static String compressCss(String css, Options option){
		Reader in = null;
	    Writer out = null;
	    String data = "";
	    try {
	        in = new InputStreamReader(new ByteArrayInputStream(css.getBytes()), option.charset);
	 
	        CssCompressor compressor = new CssCompressor(in);
	        in.close(); in = null;
	 
	        out = new StringWriter();
	        compressor.compress(out, option.lineBreakPos);
	        return out.toString();
	    } catch(Exception e){
	    	Log.error(e);
	    	return css;
	    	
	    }finally{
	        IOUtils.closeQuietly(in);
	        IOUtils.closeQuietly(out);
	    }
	}
	
	public static String compressHtml(String html){
		HtmlCompressor compressor = new HtmlCompressor();
		try{
			compressor.setEnabled(true);                   //if false all compression is off (default is true)
			compressor.setRemoveComments(false);            //if false keeps HTML comments (default is true)
			compressor.setRemoveMultiSpaces(true);         //if false keeps multiple whitespace characters (default is true)
			compressor.setRemoveIntertagSpaces(true);      //removes iter-tag whitespace characters
			compressor.setRemoveQuotes(false);              //removes unnecessary tag attribute quotes
			compressor.setSimpleDoctype(false);             //simplify existing doctype
			compressor.setRemoveScriptAttributes(false);    //remove optional attributes from script tags
			compressor.setRemoveStyleAttributes(false);     //remove optional attributes from style tags
			compressor.setRemoveLinkAttributes(false);      //remove optional attributes from link tags
			compressor.setRemoveFormAttributes(false);      //remove optional attributes from form tags
			compressor.setRemoveInputAttributes(false);     //remove optional attributes from input tags
			compressor.setSimpleBooleanAttributes(false);   //remove values from boolean tag attributes
			compressor.setRemoveJavaScriptProtocol(false);  //remove "javascript:" from inline event handlers
			compressor.setRemoveHttpProtocol(false);        //replace "http://" with "//" inside tag attributes
			compressor.setRemoveHttpsProtocol(false);       //replace "https://" with "//" inside tag attributes
			compressor.setPreserveLineBreaks(true);        //preserves original line breaks
			compressor.setRemoveSurroundingSpaces("br,p"); //remove spaces around provided tags
	
			compressor.setCompressCss(true);               //compress inline css 
			compressor.setCompressJavaScript(true);        //compress inline javascript
			compressor.setYuiCssLineBreak(80);             //--line-break param for Yahoo YUI Compressor 
			compressor.setYuiJsDisableOptimizations(false); //--disable-optimizations param for Yahoo YUI Compressor 
			compressor.setYuiJsLineBreak(-1);              //--line-break param for Yahoo YUI Compressor 
			compressor.setYuiJsNoMunge(true);              //--nomunge param for Yahoo YUI Compressor 
			compressor.setYuiJsPreserveAllSemiColons(true);//--preserve-semi param for Yahoo YUI Compressor 
	
			return compressor.compress(html);
		}catch(Exception e){
			return html;
		}
	}

}
