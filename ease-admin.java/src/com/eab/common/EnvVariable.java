package com.eab.common;

import java.util.HashMap;
import java.util.Map;

public class EnvVariable {

	private static Map<String, String> defaultVars = new HashMap<String, String>();

	private static void init() {
		defaultVars = new HashMap<String, String>();
		defaultVars.put("INTERNET_PROXY", "");		//Empty String for disable proxy
		defaultVars.put("INTRANET_PROXY", "");		//Empty String for disable proxy
		defaultVars.put("WEB_API_MAAM", "https://maam-dev.axa.com/dev/token");
		defaultVars.put("MAAM_USERNAME", "866279d0-cd4e-4211-a4b7-85c3a707346a");
		defaultVars.put("MAAM_SECRET", "3651852b-3506-4c78-bcf5-e19784c516ee");
		defaultVars.put("GATEWAY_URL", "http://192.168.222.159");
		defaultVars.put("GATEWAY_PORT", "4985");
		defaultVars.put("GATEWAY_DBNAME", "axasg_dev");
		defaultVars.put("GATEWAY_USER", "");
		defaultVars.put("GATEWAY_PW", "");
		defaultVars.put("WEB_API_SMTP_HOST", "192.168.222.127");
		defaultVars.put("WEB_API_SMTP_PORT", "25");
		defaultVars.put("INTERNAL_API_DOMAIN", "http://localhost:8080/ease-api");
		defaultVars.put("INTERNET_PROXY_HOSTS"
				, "{\"hostname\": [\"preprodapwsg.axa-tech.com:10443\", \"maam-dev.axa.com\", \"api.infinite-convergence.com\"]}");
				//Empty String for no config, example: {"hostname": ["preprodapwsg.axa-tech.com:10443", "maam-dev.axa.com"]}
		defaultVars.put("INTRANET_PROXY_HOSTS"
				, "{\"hostname\": [\"ease-sit.intranet\", \"ease-sit2.intranet\"]}");
				//Empty String for no config, example: {"hostname": ["ease-sit.intranet", "ease-sit2.intranet"]}
		defaultVars.put("ADMIN_SAML_ENTRY_POINT", "");
		defaultVars.put("ADMIN_SAML_LOGOUT", "");
	}

	public static String get(String name) {
		if (defaultVars == null || defaultVars.size() == 0) {
			init();
		}

		String result = System.getenv(name);
		if (result == null) {
			result = defaultVars.get(name);
			Log.debug("Warning: Env Var. " + name + " uses default.");
		}
		return result;
	}

}