package com.eab.common;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Token {

	public static String GenerateToken() {
//		return Function.GenerateRandomString(32);
		return Function.GenerateRandomASCII(32);		//symbol + becomes white space when AJAX sending back.
	}
	
	public static String CsrfID(HttpSession session) {
//		return SaveCsrfID(session, Constants.TOKEN_KEY_PREFIX + Function.GenerateRandomString(16) );
		return SaveCsrfID(session, Constants.TOKEN_KEY_PREFIX + Function.GenerateRandomASCII(16) );
	}
	
	private static String SaveCsrfID(HttpSession session, String tokenID) {
		session.setAttribute(Constants.TOKEN_KEY_PENDING, tokenID);
		return tokenID;
	}
	
	public static String CsrfToken(HttpSession session) {
		return CsrfToken(session, false);
		
	}
	public static String CsrfToken(HttpSession session, boolean notDelete) {
		if (notDelete) {
			return saveCsrfToken(session, getPendingCsrfID(session), GenerateToken() + Constants.TOKEN_NOT_DELETE);
		}
		return saveCsrfToken(session, getPendingCsrfID(session), GenerateToken());
	}
	
	private static String saveCsrfToken(HttpSession session, String tokenID, String tokenValue) {
		session.setAttribute(tokenID, tokenValue);
		Log.debug("{Token saveCsrfToken} Session update: " + tokenID + "='" + tokenValue + "'");
		removePendingCsrfID(session);
		return tokenValue;
	}
	
	private static String getPendingCsrfID(HttpSession session) {
		return (String) session.getAttribute(Constants.TOKEN_KEY_PENDING);		// Get pending CSRF Token ID
	}
	
	private static void removePendingCsrfID(HttpSession session) {
    	session.removeAttribute(Constants.TOKEN_KEY_PENDING);					// Remove pending ID after retrieve
	}
	
	public static String withdrawToken(HttpSession session, String tokenID) {
		try {
			if (session.getAttribute(tokenID) != null) {
				Log.debug("{Token withdrawToken} session.getAttribute(\"" + tokenID + "\") = " + session.getAttribute(tokenID));
				
				if ( ((String) session.getAttribute(tokenID)).indexOf(Constants.TOKEN_NOT_DELETE) >= 0 ) {
					return (
							((String) session.getAttribute(tokenID)).substring(0
									, ((String) session.getAttribute(tokenID)).indexOf(Constants.TOKEN_NOT_DELETE))
							);
				} else {
					return removeCsrfToken(
							session
							, tokenID
							,((String) session.getAttribute(tokenID))
							);
				}
			}
		} catch(Exception e) {
			Log.error(e);
		}
		return null;
	}
	
	private static String removeCsrfToken(HttpSession session, String tokenID, String tokenValue) {
		Log.debug("{Token removeCsrfToken} Remove CSRF Token: " + tokenID);
    	session.removeAttribute(tokenID);
    	return tokenValue;
	}
}
