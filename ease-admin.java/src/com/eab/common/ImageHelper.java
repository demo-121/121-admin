package com.eab.common;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
 
public class ImageHelper {
	
	public BufferedImage resizeImage(BufferedImage original, int newWidth, int newHeight)
    {     
	    BufferedImage resized = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
	    Graphics2D g2d = resized.createGraphics();
	    g2d.drawImage(original.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH), 0, 0, newWidth, newHeight, null);
	    g2d.dispose();
    	    
        return resized;
    }
	  
	public BufferedImage convertPDFToJPG(InputStream src, int page) throws FileNotFoundException, IOException{
		BufferedImage bim=null;

		try{
			//load pdf file in the document object
			PDDocument document=PDDocument.load(src);

			PDFRenderer pdfRenderer = new PDFRenderer(document); 
			bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB); 
			document.close(); 
			System.out.println("Conversion complete");
		}catch(IOException ie){
			ie.printStackTrace();
		}
		return bim; 
	}
}