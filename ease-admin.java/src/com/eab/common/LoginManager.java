package com.eab.common;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.hibernate.internal.util.StringHelper;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.util.StringUtils;
import org.xml.sax.SAXException;

import com.couchbase.client.deps.io.netty.util.internal.StringUtil;
import com.eab.biz.dynamic.CompanyManager;
import com.eab.biz.settings.SettingsManager;
import com.eab.biz.system.Terms;
import com.eab.dao.audit.AudUserLog;
import com.eab.dao.dynamic.Companies;
import com.eab.dao.profile.User;
import com.eab.dao.system.Name;
import com.eab.dao.users.UsersDAO;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Content;
import com.eab.json.model.Field;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.model.MenuItem;
import com.eab.model.MenuList;
import com.eab.model.RSAKeyPair;
import com.eab.model.dynamic.CompanyBean;
import com.eab.model.profile.UserBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.system.SysParamBean;
import com.eab.security.saml.IdPConfig;
import com.eab.security.saml.SAMLException;
import com.eab.security.saml.SAMLInit;
import com.eab.security.saml.SPConfig;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class LoginManager {

	/***
	 * This is function to get Login Fail Count from database.
	 * @param userCode - User ID
	 * @return Fail Count
	 */
	public int getFailCount(String userCode) {
		int failCnt = -1;
		DBManager dbm = null;

		try {
			dbm = new DBManager();

			String sql = "select fail_cnt from sys_user_mst where user_code=" + dbm.param(userCode, DataType.TEXT);
			failCnt = Integer.parseInt(dbm.selectSingle(sql, "fail_cnt"));

		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
		}

		return failCnt;
	}

	/***
	 * This is function to increase Login Fail Count of target User.  If Fail Count hits Max, Deactivate status will be set.
	 * @param userCode - User ID
	 * @return Status of Max validation
	 */
	public boolean addFailCount(String userCode) {
		boolean status = false;
		DBManager dbm = null;

		try {
			dbm = new DBManager();
			
			String sql = "update sys_user_mst set fail_cnt = fail_cnt + 1 where user_code=" + dbm.param(userCode, DataType.TEXT);
			int count = dbm.update(sql);

			if (count > 0) {
				int failCount = getFailCount(userCode);

				if (Constants.LOGIN_FAIL_COUNT_MAX < 0) {
					/// TODO: disabled fail count
					status = true;
				} else {
					if (Constants.LOGIN_FAIL_COUNT_MAX > failCount) {
						status = true; // User account is still valid
					} else {
						status = false; // User account hits maximum login fail, deactivate.
						deactiveUser(userCode);
					}
				}
			} else {
				status = false;
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
		}

		return status;
	}

	/***
	 * This is function to reset Login Fail Count.
	 * @param userCode - User ID
	 * @return Status of Reset process
	 */
	public boolean resetFailCount(String userCode) {
		boolean status = false;
		DBManager dbm = null;

		try {
			dbm = new DBManager();
			
			///TODO: criteria that we can rest.  In some case like terminated should not reset.
			String sql = "update sys_user_mst set fail_cnt = 0, status='A' where user_code=" + dbm.param(userCode, DataType.TEXT);
			int count = dbm.update(sql);

			if (count > 0)
				status = true;

		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
		}

		return status;
	}

	/***
	 * This is function to deactivate target User.
	 * @param userCode - User ID
	 * @return Status of Update process
	 */
	public boolean deactiveUser(String userCode) {
		boolean status = false;
		DBManager dbm = null;

		try {
			dbm = new DBManager();
			
			String sql = "update sys_user_mst set status='D' where user_code=" + dbm.param(userCode, DataType.TEXT) + " and status='A'";
			int count = dbm.update(sql);

			if (count > 0) {
				status = true;
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
		}

		return status;
	}

	/***
	 * This is function to update Last Login Time to user record in database.  Reset Fail Count will be applied at the same time.
	 * @param userCode - User ID
	 * @return Status of Update process
	 */
	public boolean setLastLoginTime(String userCode) {
		boolean status = false;
		DBManager dbm = null;
		ResultSet rs = null;

		try {
			dbm = new DBManager();
			
			String sql = "update sys_user_mst set last_login_dt=sysdate where user_code=" + dbm.param(userCode, DataType.TEXT);
			int count = dbm.update(sql);

			if (count > 0) {
				status = true;
				resetFailCount(userCode);
			}

		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
		}

		return status;
	}

	/***
	 * This is function to get the company regional
	 * @param compCode - compCode
	 * @return boolean
	 */
	public boolean checkRegional(String compCode) {
		boolean status = false;
		DBManager dbm = null;
		Connection conn = null;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			String result = (String) Function2.getSingleColumnFromRecord("SYS_COMPANY_MST", "regional", "comp_code", compCode, DataType.TEXT, dbm, conn);
			Log.debug("check regional :"+ result);
			if (result.toUpperCase().equals("Y")) {
				status = true;
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			dbm = null;
		}

		return status;
	}

	public boolean authenticate(HttpServletRequest request, String usercode) {
		boolean hasError = false;
		HttpSession session = request.getSession();
		String failReason = null;
		String sourceOfLogin = "LOGIN";
		UserBean userInfo = null;
		LoginManager loginMgr = null;

		Function2 func2 = new Function2();
		AudUserLog audUserLog = new AudUserLog();

		Map<String, SysParamBean> sysParamList = null;

		Log.info("{LoginManager authenticate} User Login - authentication (START)");

		try {
			sysParamList = func2.getSysParam();
			String sysVer = sysParamList.get("SYS_VER").getParamValue();

			loginMgr = new LoginManager();

			// Get User Profile by usercode
			User userObj = new User();
			userInfo = userObj.get(usercode, request);
			if (userInfo != null) {
				Log.debug("{LoginManager SAML authenticate} userInfo.getCompCode() = " + userInfo.getCompCode());
				Log.debug("{LoginManager SAML authenticate} userInfo.getUserCode() = " + userInfo.getUserCode());
				Log.debug("{LoginManager SAML authenticate} userInfo.getUserName() = " + userInfo.getUserName());
			} else {
				hasError = true;
				failReason = "LOGIN_FAIL_PROFILE";
			}

			// Validate User Status
			if (!hasError) {
				if ("A".equals(userInfo.getStatus())) {
				} else {
					hasError = true;
					failReason = "STATUS_FAIL";
				}
			}

			// check if the company is active
			if (!hasError) {
				Companies cm = new Companies();
				CompanyBean cb = cm.getCompany(userInfo.getCompCode());
				if (cb == null || (cb != null && !cb.getIsActive())) {
					hasError = true;
					failReason = "LOGIN_FAIL_COMAPNY";
				}
			}

			if (!hasError) {
				// ******Success Login******: All validation has been passed

				// Put user profile / info into principal
				UserPrincipalBean principal = new UserPrincipalBean();
				principal.setCompCode(userInfo.getCompCode());
				principal.setRegional(this.checkRegional(userInfo.getCompCode()));
				principal.setUserCode(userInfo.getUserCode());
				principal.setUserName(userInfo.getUserName());
				principal.setStatus(userInfo.getStatus());
				principal.setUserType(userInfo.getUserType());
				// principal.setChannelCode(userInfo.getChannelCode());
				principal.setPosCode(userInfo.getPosCode());
				principal.setDepCode(userInfo.getDepCode());
				principal.setLangCode(userInfo.getLangCode());

				Companies companies = new Companies();
				principal.setDateFormat(companies.getCompanyDateFormat(userInfo.getCompCode()));
				principal.setTimeFormat(companies.getCompanyTimeFormat(userInfo.getCompCode()));

				principal.getWorkflow().setLogin(true);

				String role = userObj.getRoleIdByUser(principal.getUserCode(), principal.getCompCode());

				if (role != null) {
					principal.setRoleCode(role);
				}

				principal.setChannelList(userObj.getChannelCodeByUser(principal));

				session.setAttribute(Constants.USER_PRINCIPAL, principal);

				session.setAttribute("loginBySaml", true);

				// Mark Last Login Time
				loginMgr.setLastLoginTime(usercode);

				// set user lang into session
				Language lang = new Language();
				lang.setLang(request, userInfo.getLangCode());

				// Audit Trail (Success)
				audUserLog.add(usercode, userInfo.getCompCode(), Function.getServerIp(request),
						Function.getClientIp(request), sysVer, true, null, sourceOfLogin, session.getId());

			} else {
				// ******Fail Login******
				// nextModel = "/Login";
				// Audit Trail (Fail)
				audUserLog.add(usercode, userInfo.getCompCode(), Function.getServerIp(request),
						Function.getClientIp(request), sysVer, false, failReason, sourceOfLogin, session.getId());
			}

		} catch (Exception e) {
			Log.error(e);
		} finally {
		}

		session.setAttribute(Constants.LOGIN_ERROR_CODE, failReason);

		Log.info("{LoginManager authenticate} User SAML - authentication (END)");
		return !hasError;
	}

	// public String authenticate(HttpServletRequest request, HttpServletResponse
	// response, String loginLang) {
	public String landing(HttpServletRequest request, String loginLang) {
		boolean hasError = false;
		String nextModel = "redirect:/Error";

		HttpSession session = request.getSession();
		MainMenu menu = null;

		String failMessage = null;
		// UserBean userInfo = null;
		String usercode = null;
		Map<String, Object> json = new HashMap<String, Object>();
		Terms terms = null;

		Log.info("{LoginManager authenticate} User Login - authentication (START)");

		try {
			terms = new Terms();
			UserPrincipalBean principal = Function.getPrincipal(request);
			if (principal != null) {
				// ******Success Login******: All validation has been passed
				usercode = principal.getUserCode();

				// Check Terms & Condition is acception?
				// boolean termsAccept = terms.getTermsAccept(usercode);
				// if (termsAccept) {
				principal.getWorkflow().setTerms(true);
				// }

				// set user lang into session
				Language lang = new Language();
				if (loginLang != null && !loginLang.equals("")) {
					lang.setLang(request, loginLang);
					principal.setLangCode(loginLang);
				}

				// set UI translation
				Map<String, Object> nameMap = lang.getUITranslation(request);
				principal.setUITranslation(nameMap);
				json.put("langMap", lang.getRegularLangList(nameMap));

				// Get next steps of Login process
				nextModel = principal.getWorkflow().nextFlow();
				Log.info("{LoginManager authenticate} Go to " + nextModel);
				json.put("tokenID", Token.CsrfID(session));
				json.put("tokenKey", Token.CsrfToken(session));

				if (nextModel.equals("/Home")) {
					menu = new MainMenu();
					json.putAll(prepareHomeLanding(session, menu, request));
				} else {
					// get term and condition details
					json.putAll(prepareTAC(request, session));
				}

			} else {
				// ******Fail Login******

				nextModel = "/Login";
				// translate err msg
				List<Name> nameList = new ArrayList<Name>();
				Name name = new Name();
				name.setCode(failMessage);
				name.setDesc("");
				nameList.add(name);

				Language lang = new Language();
				lang.getNameList(request, nameList, loginLang);
				String srrMsg = (nameList.get(0).getDesc() != null) ? nameList.get(0).getDesc() : failMessage;
				Map<String, Object> content = new HashMap<String, Object>();
				content.put("failMessage", srrMsg);
				json.put("content", content);
			}

		} catch (Exception e) {
			Log.error(e);
			nextModel = "/Error";
			Map<String, Object> content = new HashMap<String, Object>();
			content.put("failure", true);

			// translate err msg
			String failTitle = "UNKNOWN_ERROR";
			List<String> labelList = new ArrayList<String>();
			labelList.add(failTitle);
			Language lang = new Language();
			Map<String, Object> labelMap = lang.getNameMap(request, labelList);
			failTitle = (labelMap.get(failTitle) != null) ? labelMap.get(failTitle).toString() : failTitle;

			content.put("title", failTitle);
			content.put("message", e.getMessage());
			json.put("content", content);

			// return "/Error";
		} finally {
			/// TODO: Initialize all object and sensitive data in memory
			menu = null;
			terms = null;
		}

		Log.info("{LoginManager authenticate} User Login - authentication (END)");
		json.put("action", Constants.ActionTypes.PAGE_REDIRECT);
		json.put("path", nextModel);
		json.put("loggedOn", "Y");
		
		String version = "1.0";
		
		File verfile = new File(Constants.RESOURCE_ROOT+"version.xml");
		
		org.w3c.dom.Document document;
		try {
			document = DocumentBuilderFactory
			        .newInstance().newDocumentBuilder().parse(verfile);
			json.put("version", document.getElementsByTagName("version").item(0).getTextContent());

		} catch (SAXException | IOException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JSONObject jsObj = new JSONObject(json);

		return jsObj.toString();
	}

	public String authenticate(HttpServletRequest request, HttpServletResponse response, String loginLang) {
		boolean hasError = false;
		boolean validUsername = false;
		boolean validPassword = false;
		boolean validStatus = false;
		boolean validSingleLogin = false;
		String nextModel = "redirect:/Error";
		String sourceOfLogin = "LOGIN";

		HttpSession session = request.getSession();
		Function2 func2 = new Function2();
		AudUserLog audUserLog = new AudUserLog();
		MainMenu menu = null;

		Map<String, SysParamBean> sysParamList = null;
		String failReason = null;
		String failMessage = null;
		UserBean userInfo = null;
		String usercode = null;
		String password = null;
		String captcha = null;
		LoginManager loginMgr = null;
		String sysVer = null;
		Map<String, Object> sessionObjs = null;
		Map<String, Object> json = new HashMap<String, Object>();
		CryptoUtil crypto = null;
		Terms terms = null;

		Log.info("{LoginManager authenticate} User Login - authentication (START)");

		try {
			sysParamList = func2.getSysParam();
			sysVer = sysParamList.get("SYS_VER").getParamValue();
			loginMgr = new LoginManager();
			crypto = new CryptoUtil();
			terms = new Terms();

			// Capture Input parameter
			usercode = request.getParameter("data_001"); // Username
			usercode = (usercode != null) ? usercode.toUpperCase() : null; // Username is uppercase in database record.
//			password = request.getParameter("data_002"); // Password

			Log.debug("{LoginManager authenticate} Username = " + usercode);
			Log.debug("{LoginManager authenticate} Password = " + password);

			// Parameter validation
			// Backup before current session is destroy
			sessionObjs = func2.backupSessionAttributes(request);

			// Destroy and Recreate Session ID (Backup any required data in Session before
			// destroy)
			Log.debug("{LoginManager authenticate} Session ID (Current) = " + ((session != null) ? session.getId() : ""));

			if (session != null) {
				try {
					Log.info("************** session going to destroy ************** auth normal");
					session.invalidate(); // Destroy
					session = request.getSession(true);
				} catch (Exception e) {
					Log.error(e);
				}
			}
			Log.debug("{LoginManager authenticate} Session ID (New) = " + session.getId());

			// Restore after session is created
			func2.restoreSessionAttributes(request, sessionObjs);
			func2.stdoutSessionAttributes(request);

			// Retrieve RSA Keypair from session
			RSAKeyPair keypair = null;
			if (session.getAttribute(Constants.CRYPTO_KEYPAIR_NAME) != null) {
				keypair = (RSAKeyPair) session.getAttribute(Constants.CRYPTO_KEYPAIR_NAME);
			}

			User userObj = new User();
			// Get User Profile by usercode
			if (!hasError) {
				userInfo = userObj.get(usercode, request);
				if (userInfo != null) {
					Log.debug("{LoginManager authenticate} userInfo.getCompCode() = " + userInfo.getCompCode());
					Log.debug("{LoginManager authenticate} userInfo.getUserCode() = " + userInfo.getUserCode());
					Log.debug("{LoginManager authenticate} userInfo.getUserName() = " + userInfo.getUserName());
					Log.debug("{LoginManager authenticate} userInfo.getUserPassword() = " + userInfo.getUserPassword());
				} else {
					hasError = true;
					validUsername = false;
					session.setAttribute(Constants.LOGIN_ERROR_CODE, "LOGIN_FAIL");
					Log.error("{LoginManager authenticate} Username is invalid." + usercode);
					failReason = "LOGIN_FAIL_USERCODE";
					failMessage = "LOGIN_FAIL";
					loginMgr.addFailCount(usercode);
				}
			}

			// Validate User Name, User status...etc
			if (!hasError) {
				///TODO: User data from Database and validate
				Log.debug("{LoginManager authenticate} Compare '" + usercode + "' <--> '" + userInfo.getUserCode() + "'");
				if (userInfo!=null && usercode != null && usercode.equalsIgnoreCase(userInfo.getUserCode())) {
					validUsername = true;
				} else {
					hasError = true;
					validUsername = false;
					session.setAttribute(Constants.LOGIN_ERROR_CODE, "LOGIN_FAIL");
					Log.error("{LoginManager authenticate} Username is invalid.");
					failReason = "LOGIN_FAIL_USERCODE";
					failMessage = "LOGIN_FAIL";
					loginMgr.addFailCount(usercode);
				}
			}

			// Validate Fail Count
			if (!hasError) {
				int failCount = loginMgr.getFailCount(usercode);
				if (failCount < Constants.LOGIN_FAIL_COUNT_MAX) {
					validStatus = true;
				} else {
					hasError = true;
					session.setAttribute(Constants.LOGIN_ERROR_CODE, "STATUS_FAIL");
					Log.error("{LoginManager authenticate} Fail Count is exceeded maximum.");
					failReason = "LOGIN_FAIL_COUNT";
					failMessage = "LOGIN_FAIL_COUNT";
					loginMgr.addFailCount(usercode);
				}
			}

			// Validate Password
//			if (!hasError) {
//				Log.debug("{LoginManager authenticate} Password Compare: " + password + " <--> "
//						+ userInfo.getUserPassword());
//
//				if (password != null && password.equals(userInfo.getUserPassword())) {
//					validPassword = true;
//				} else {
//					hasError = true;
//					validPassword = false;
//					session.setAttribute(Constants.LOGIN_ERROR_CODE, "LOGIN_FAIL");
//					Log.error("{LoginManager authenticate} Password is invalid.");
//					failReason = "LOGIN_FAIL_PASSWORD";
//					failMessage = "LOGIN_FAIL";
//					loginMgr.addFailCount(usercode);
//				}
//			}

			// Validate User Status
			if (!hasError) {
				if ("A".equals(userInfo.getStatus())) {
					validStatus = true;
				} else {
					hasError = true;
					session.setAttribute(Constants.LOGIN_ERROR_CODE, "STATUS_FAIL");
					Log.error("{LoginManager authenticate} User Status is invalid.");
					failReason = "STATUS_FAIL";
					failMessage = "LOGIN_FAIL_STATUS";
					loginMgr.addFailCount(usercode);
				}
			}

			// Validate Single Login Session
			if (!hasError && Constants.ENABLE_SINGLE_LOGIN) {
				if (func2.hasLoginSession(usercode)) {
					int recUpdate = func2.setLogoutTimeByUser(usercode); // Close exist session
					if (recUpdate == 0) {
						hasError = true;
						session.setAttribute(Constants.LOGIN_ERROR_CODE, "LOGIN_FAIL_SESSION");
						Log.error("{LoginManager authenticate} User Code has exist login session.");
						failReason = "LOGIN_SESSION_FAIL";
						failMessage = "LOGIN_FAIL_SESSION";
					}
				} else {
					validSingleLogin = true; // No exist login session.
				}
			} else {
				validSingleLogin = true;
			}

			// check if the company is active
			if (!hasError) {
				Companies cm = new Companies();
				CompanyBean cb = cm.getCompany(userInfo.getCompCode());
				if (cb == null || (cb != null && !cb.getIsActive())) {
					hasError = true;
					session.setAttribute(Constants.LOGIN_ERROR_CODE, "LOGIN_FAIL_COMAPNY");
					Log.error("{LoginManager authenticate}  User company is Invalid");
					failReason = "LOGIN_SESSION_FAIL";
					failMessage = "LOGIN_FAIL_COMAPNY";
				}
			}

			if (!hasError) {
				// ******Success Login******: All validation has been passed

				// Put user profile / info into principal
				UserPrincipalBean principal = new UserPrincipalBean();
				principal.setCompCode(userInfo.getCompCode());
				principal.setRegional(this.checkRegional(userInfo.getCompCode()));
				principal.setUserCode(userInfo.getUserCode());
				principal.setUserName(userInfo.getUserName());
				principal.setStatus(userInfo.getStatus());
				principal.setUserType(userInfo.getUserType());
				// principal.setChannelCode(userInfo.getChannelCode());
				principal.setPosCode(userInfo.getPosCode());
				principal.setDepCode(userInfo.getDepCode());
				principal.setLangCode(userInfo.getLangCode());

				Companies companies = new Companies();
				principal.setDateFormat(companies.getCompanyDateFormat(userInfo.getCompCode()));
				principal.setTimeFormat(companies.getCompanyTimeFormat(userInfo.getCompCode()));

				principal.getWorkflow().setLogin(true);

				// Check Terms & Condition is acception?
				boolean termsAccept = terms.getTermsAccept(usercode);
				if (termsAccept) {
					principal.getWorkflow().setTerms(true);
				}

				String role = userObj.getRoleIdByUser(principal.getUserCode(), principal.getCompCode());

				if (role != null) {
					principal.setRoleCode(role);
				}

				principal.setChannelList(userObj.getChannelCodeByUser(principal));

				session.setAttribute(Constants.USER_PRINCIPAL, principal);

				// Mark Last Login Time
				loginMgr.setLastLoginTime(usercode);

				// set user lang into session
				Language lang = new Language();
				if (loginLang != null && !loginLang.equals("")) {
					lang.setLang(request, loginLang);
					principal.setLangCode(loginLang);
				} else
					lang.setLang(request, userInfo.getLangCode());


				// set UI translation
				Map<String, Object> nameMap = lang.getUITranslation(request);
				principal.setUITranslation(nameMap);
				json.put("langMap", lang.getRegularLangList(nameMap));

				// Audit Trail (Success)
				audUserLog.add(usercode, userInfo.getCompCode(), Function.getServerIp(request),
						Function.getClientIp(request), sysVer, true, null, sourceOfLogin, session.getId());

				// Get next steps of Login process
				nextModel = principal.getWorkflow().nextFlow();
				Log.info("{LoginManager authenticate} Go to " + nextModel);
				json.put("tokenID", Token.CsrfID(session));
				json.put("tokenKey", Token.CsrfToken(session));
				
				json.put("loggedOn", "Y");

				String version = "1.0";
				
				File verfile = new File(Constants.RESOURCE_ROOT+"version.xml");
				
				org.w3c.dom.Document document;
				try {
					document = DocumentBuilderFactory
					        .newInstance().newDocumentBuilder().parse(verfile);
					json.put("version", document.getElementsByTagName("version").item(0).getTextContent());

				} catch (SAXException | IOException | ParserConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (nextModel.equals("/Home")) {
					menu = new MainMenu();
					json.putAll(prepareHomeLanding(session, menu, request));
				} else {
					// get term and condition details
					json.putAll(prepareTAC(request, session));
				}

			} else {
				// ******Fail Login******

				nextModel = "/Login";
				// translate err msg
				List<Name> nameList = new ArrayList<Name>();
				Name name = new Name();
				name.setCode(failMessage);
				name.setDesc("");
				nameList.add(name);

				Language lang = new Language();
				lang.getNameList(request, nameList, loginLang);
				String srrMsg = (nameList.get(0).getDesc() != null) ? nameList.get(0).getDesc() : failMessage;
				Map<String, Object> content = new HashMap<String, Object>();
				content.put("failMessage", srrMsg);
				json.put("content", content);

				// Audit Trail (Fail)
				if (userInfo != null) {
					audUserLog.add(usercode, userInfo.getCompCode(), Function.getServerIp(request),
						Function.getClientIp(request), sysVer, false, failReason, sourceOfLogin, session.getId());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			if (!StringUtils.isEmpty(usercode)) {
				try {
					audUserLog.add(usercode, userInfo.getCompCode(), Function.getServerIp(request),
							Function.getClientIp(request), sysVer, false, "EXCEPTION:" + e.getMessage(), sourceOfLogin,
							session.getId());
				} catch (Exception ex) {
					Log.error(ex);
				}
			}
			Log.error(e);
			nextModel = "/Error";
			Map<String, Object> content = new HashMap<String, Object>();
			content.put("failure", true);

			// translate err msg
			String failTitle = "UNKNOWN_ERROR";
			List<String> labelList = new ArrayList<String>();
			labelList.add(failTitle);
			Language lang = new Language();
			Map<String, Object> labelMap = lang.getNameMap(request, labelList);
			failTitle = (labelMap.get(failTitle) != null) ? labelMap.get(failTitle).toString() : failTitle;

			content.put("title", failTitle);
			content.put("message", e.getMessage());
			json.put("content", content);

			// return "/Error";
		} finally {
			/// TODO: Initialize all object and sensitive data in memory
			menu = null;
			crypto = null;
			terms = null;
		}

		Log.info("{LoginManager authenticate} User Login - authentication (END)");
		json.put("action", Constants.ActionTypes.PAGE_REDIRECT);
		json.put("path", nextModel);

		JSONObject jsObj = new JSONObject(json);

		return jsObj.toString();
	}

	public Map<String, Object> prepareHomeLanding(HttpSession session, MainMenu menu, HttpServletRequest request)
			throws SQLException {
		Map<String, Object> map = new HashMap<String, Object>();
		Function2 func2 = new Function2();

		String sysName = null;
		Map<String, SysParamBean> sysParamList = null;
		sysParamList = func2.getSysParam();
		sysName = sysParamList.get("SYS_NAME").getParamValue();

		// load user
		UserPrincipalBean principal = Function.getPrincipal(session);
		Map<String, Object> user = new HashMap<String, Object>();
		user.put("username", principal.getUserName());
		user.put("sysName", sysName);

		Companies cp = new Companies();
		CompanyBean cb = cp.getCompany(principal.getCompCode());
		user.put("dateFormat", cp.getDateFormat(principal.getCompCode()));
		String compName = cb.getCompName();
		//
		// DBManager dbm = new DBManager();
		// Connection conn = DBAccess.getConnection();
		CompanyManager cm = new CompanyManager(request);
		Map<String, String> langMap = cm.getCompanyLangMap(principal.getCompCode());
		
		List<Map<String, String>> langList = new ArrayList<Map<String,String>>();
		Log.info("get langMap" + langMap.size() + " - " + cb.getLangList().size());
		for (String langKey : cb.getLangList()) {
			Map<String, String> lang = new HashMap<String, String>();
			lang.put("id", langKey);
			lang.put("title", langMap.get(langKey));
			langList.add(lang);
		}
		
		user.put("compName", compName);
		map.put("user", user);
		map.put("langList", langList);

		// load left menu
		MenuList menuList = menu.loadFromDB(request, principal.getRoleCode());
		menu.putToSession(session, menuList);
		map.put("mainMenu", menuList.toList());

		// default page
		MenuItem page = menuList.get(0);
		if (page.getId() != null) {
			map.put("page", page.toMap());
		} else {
			map.put("page", page.getSubItems().get(0).toMap());
		}

		// appbar

		// content
		Map<String, Object> content = new HashMap<String, Object>();

		// close connection
		// dbm.clear();
		// dbm = null;
		// conn.close();
		//
		menu = null;
		return map;
	}

	public Map<String, Object> prepareTAC(HttpServletRequest request, HttpSession session) {
		Map<String, Object> map = new HashMap<String, Object>();

		Language langObj = new Language();
		List<String> labelList = Arrays.asList("TERMS.AND.CONDITION", "TERMS.DETAILS", "ACCEPT", "DECLINE");
		Map<String, Object> nameMap = langObj.getNameMap(request, labelList);
		for (Map.Entry<String, Object> entry : nameMap.entrySet()) {
			if (entry.getValue() == null || entry.toString().equals("")) {
				entry.setValue(entry.getKey());
			}
		}
		Map<String, Object> content = new HashMap<String, Object>();
		content.put("title", nameMap.get("TERMS.AND.CONDITION"));
		content.put("details", nameMap.get("TERMS.DETAILS"));
		content.put("accept", nameMap.get("ACCEPT"));
		content.put("decline", nameMap.get("DECLINE"));

		map.put("content", content);

		return map;
	}

	public void logout(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession(false);
		Log.info("************** session going to destroy ************** logout");
		try {
			// Destroy current session
			if (session != null) {
				session.invalidate(); // Destroy
			}
			
			session = request.getSession(false);
			if (session != null && session.getAttribute(Constants.USER_PRINCIPAL) != null) {
				session.setAttribute(Constants.USER_PRINCIPAL, null);
				session.invalidate(); // destroy again
			}
		} catch (Exception e) {
			Log.error(e);
		}
	}

	// skip as not login anymore
	public Response getLoginDetails(HttpServletRequest request, String langCode) {

		Response resp = null;
		List<String> labelList = Arrays.asList("INPUT.HINT.USER.NAME", "INPUT.HINT.PASSWORD", "WELCOME_BACK",
				"SIGN_IN_ADMIN", "USER.NAME", "PASSWORD", "SIGN_IN", "FORGET_PASSWORD", "INPUT.ERROR.SESSION_EXPIRY");

		JsonObject values = new JsonObject();
		List<Name> nameList = new ArrayList<Name>();
		for (int i = 0; i < labelList.size(); i++) {
			String label = labelList.get(i);
			Name name = new Name();
			name.setCode(label);
			name.setDesc("");
			nameList.add(name);
		}
		try {
			Language lang = new Language();
			lang.getNameList(request, nameList, langCode);

		} catch (SQLException e) {
			Log.error(e.getMessage());
		}
		int nameCount = nameList.size();
		for (int j = 1; j < nameCount; j++) {
			Name newName = nameList.get(j);
			String code = newName.getCode();
			String desc = newName.getDesc();
			Log.debug("checkLoginTxt code :" + code);
			Log.debug("checkLoginTxt desc :" + desc);
			desc = (desc != null) ? desc : code;
			values.addProperty(code, desc);
		}
		values.addProperty("lang", langCode);

		SettingsManager sm = new SettingsManager(request);
		Template template = null;
		List<Field> fields = new ArrayList<Field>();
		fields.add(new Field("lang", "picker", "", sm.getLangOptions(), ""));
		template = new Template("welcome", fields, null, null);
		Page pageObject = new Page("/Login", "Login");
		Content content = new Content(template, values, values);
		resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Login", pageObject, null, null, null, content);
		return resp;

	}

	public String initApp(HttpServletRequest request, String langCode) {
		Response resp = null;
		HttpSession session = request.getSession();
		UserPrincipalBean principal = Function.getPrincipal(session);
		Log.debug("{LoginManager initApp} Session ID (Current) = " + session.getId());
		String jsonString = null;
	
		if (principal == null) {
			Log.info("LoginManager>>> without user principal");
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/", null);
			try {
				ServletContext context = request.getServletContext();
				SAMLInit.initialize();
//				IdPConfig idpConfig = new IdPConfig(context.getResourceAsStream("/WEB-INF/saml/idpmeta.xml"));
				IdPConfig idpConfig = new IdPConfig(new File(Constants.SAML_META_PATH + "idpmeta.xml"));
				String saml = idpConfig.getLoginUrl();// ''|| EnvVariable.get("ADMIN_SAML_ENTRY_POINT");
				resp.setSaml(saml);
			} catch (SAMLException e) {
				Log.error(e);
			} catch (Exception e) {
				Log.error(e);
			}
			String error = (String) session.getAttribute(Constants.LOGIN_ERROR_CODE);
			
			if (error != null) {
				resp.setErrorMsg(error);
				Log.info("initApp: ERROR:" + "saml" + error);
			}
			Gson gson = new Gson();
			jsonString = gson.toJson(resp);
		} else {
			Log.info("LoginManager>>> have user principal");
			jsonString = this.landing(request, "en");

			if (principal != null) {
				String paramTimeZone = request.getParameter("data_004");
				if (paramTimeZone != null) {
					int tempTimeZone = Integer.valueOf(paramTimeZone) * -1;
					if (tempTimeZone > -1)
						paramTimeZone = "GMT+" + String.valueOf(tempTimeZone);
					else
						paramTimeZone = "GMT" + String.valueOf(tempTimeZone);

					principal.setTimezone(paramTimeZone);
					session.setAttribute(Constants.USER_PRINCIPAL, principal);
				}
			}
			Log.debug("{WSAuth} JSon Output: " + jsonString);
		}

		return jsonString;

	}

	public boolean loginBySaml(HttpServletRequest request, String uid, String name, List<String> groups, String mail)
			throws Exception {
		String error = null;
		String compCode = "01";
		String channel = "*";
		Connection conn = null;
		try {
			conn = DBAccess.getConnection();
			
			Log.debug("[loginBySaml] uid: " + uid);
			Log.debug("[loginBySaml] name: " + name);
			Log.debug("[loginBySaml] groups size: " + ((groups!=null)?groups.size():-1));
			Log.debug("[loginBySaml] mail: " + mail);

			// check user
			if (StringUtils.isEmpty(uid)) {
				Log.debug("[loginBySaml] checkpoint #1 - no UID");
				throw new Exception("Invalid uid");
			}
			UsersDAO uDao = new UsersDAO(conn);

			Log.debug("[loginBySaml] checkpoint #2 - exist UID");
			if (uDao.existUser(uid)) {
				Log.debug("[loginBySaml] checkpoint #2.1 - exist & update");
				uDao.updateSamlUser(uid, name, mail);
			} else {
				Log.debug("[loginBySaml] checkpoint #2.2 - non-exist & insert");
				uDao.insertSamlUser(compCode, channel, uid, name, mail);
			}

			Log.debug("[loginBySaml] checkpoint #3 - create roleList");
			List<String> roleList = new ArrayList<String>() {
				{
					add("ease_sg_agt-amt");
					add("ease_sg_prd-egt");
					add("ease_sg_fin");
					add("ease_sg_cpf");
					add("ease_sg_prd-cnf");
					add("ease_sg_ita");
					add("ease_sg_ics");
					add("ease_sg_ops");
					add("ease_sg_prxy");
				}
			};

			Log.debug("[loginBySaml] checkpoint #4 - create roleMap");
			Map<String, List<String>> roleMap = new HashMap<String, List<String>>() {
				{
					List<String> rights = null;

					// Agent Achievement Upload
					rights = new ArrayList<String>() {
						{
							add("FN.WEB.AGTCERT.DETAILS");
							add("FN.WEB.AGTCERT.PUBLISH");
							add("FN.WEB.AGTCERT.UPD");
						}
					};
					put("ease_sg_agt-amt", rights);

					// Product Eligibility Upload
					rights = new ArrayList<String>() {
						{
							add("FN.WEB.PRODELIG.DETAILS");
							add("FN.WEB.PRODELIG.PUBLISH");
							add("FN.WEB.PRODELIG.UPD");
						}
					};
					put("ease_sg_prd-egt", rights);

					// Online Payment Report
					rights = new ArrayList<String>() {{
						add("FN.REPORT.ONLINEPAY");
						add("FN.REPORT.ONLINEPAY.GENERATE");
					}};
					put("ease_sg_fin", rights);

					// EASE Submission Report (Submitted and pending cases)
					rights = new ArrayList<String>() {{
						add("FN.REPORT.SUBMISSCASES");
						add("FN.REPORT.SUBMISSCASES.GENERATE");
					}};
					put("ease_sg_ops", rights);
					
					// Internal Case submission report _ All Channels
					rights = new ArrayList<String>() {{
						add("FN.REPORT.ALLCHANNEL");
						add("FN.REPORT.ALLCHANNEL.GENERATE");
					}};
					put("ease_sg_ics", rights);
					
					// Product Configurator
					rights = new ArrayList<String>() {{
						// uncommon for enable product config and release
//						add("FN.RELEASE.ADD");
//						add("FN.RELEASE.DETAIL");
//						add("FN.RELEASE.RELEASE");
//						add("FN.RELEASE.TEST");
//						add("FN.TASKS.UPDATE");
//						add("FN.PDF.DETAILS");
//						add("FN.PDF.UPD");
//						add("FN.PRO.FUNDS.APPR");
//						add("FN.PRO.FUNDS.DETAILS");
//						add("FN.PRO.FUNDS.UPD");
//						add("FN.PRO.PRODUCTS.DETAILS");
//						add("FN.PRO.PRODUCTS.UPD");
					}};
					put("ease_sg_prd-cnf", rights);


					// BCP
					rights = new ArrayList<String>() {{
						add("FN.BCP.BCP");
						add("FN.BCP.SUBMIT");
					}};
					// Audit Trail Reports
					//rights = new ArrayList<String>();
					put("ease_sg_ita", rights);
					
					// CPF Report
					rights = new ArrayList<String>();
					put("ease_sg_cpf", rights); 

					// Approval Proxy report
					rights = new ArrayList<String>() {{
						add("FN.REPORT.PROXY");
						add("FN.REPORT.PROXY.GENERATE");
					}};
					put("ease_sg_prxy", rights); 
					
				}
			};

			String roleCode = "SR";
			List<String> rightList = new ArrayList<String>();

			Log.debug("[loginBySaml] checkpoint #5 - create rightList");
			Log.debug("[loginBySaml] checkpoint #5.1 - rightList size: " + ((roleList!=null)?roleList.size():-1));
			for (int i = 0; i < roleList.size(); i++) {
				Log.debug("[loginBySaml] checkpoint #5.2 - ["+i+"] " + roleList.get(i));
				if (groups.contains(roleList.get(i))) {
					roleCode += "" + i;
					rightList.addAll(roleMap.get(roleList.get(i)));
					Log.debug("[loginBySaml] checkpoint #5.3 - added");
				}
			}

			Log.debug("[loginBySaml] checkpoint #6 - show user");
			Log.info("User " + uid + " login as " + roleCode + "(" + rightList.size() + ")");

			// check role
			Log.debug("[loginBySaml] checkpoint #7 - check exists user role");
			if (!uDao.existsUserRole(compCode, roleCode)) {
				// add role
				boolean output = uDao.createUserRole(compCode, roleCode, null);
				Log.debug("[loginBySaml] checkpoint #7.1 - " + output);
			}

			boolean hasDiff = false;
			// check role func
			Log.debug("[loginBySaml] checkpoint #8 - check exists role func");
			if (!uDao.existRoleFunc(compCode, roleCode, rightList)) {
				hasDiff = true;
				Log.debug("[loginBySaml] checkpoint #8.1 - existRoleFunc");
			} else if (uDao.countAllRoleFunc(compCode, roleCode) != rightList.size()) {
				hasDiff = true;
				Log.debug("[loginBySaml] checkpoint #8.2 - countAllRoleFunc");
			}
			// update role func if got changes
			Log.debug("[loginBySaml] checkpoint #9 - delete & update role func");
			if (hasDiff) {
				uDao.deleteRoleFunc(compCode, roleCode);
				if (rightList.size() > 0) {
					uDao.addRoleFuncs(compCode, roleCode, rightList);
				}
			}

			// check user role relation
			Log.debug("[loginBySaml] checkpoint #10 - check user exists role func");
			if (!uDao.existsUserUserRole(compCode, channel, uid, roleCode)) {
				uDao.deleteUserUserRole(compCode, uid);
				uDao.createUserUserRole(compCode, channel, uid, roleCode);
			}

			Log.debug("[loginBySaml] checkpoint #11 - authenticate");
			this.authenticate(request, uid);

			Log.debug("[loginBySaml] checkpoint #12 - finished");
		} catch (Exception e) {
			Log.debug("[loginBySaml] has Exception!!!");
			error = e.getMessage();
			Log.error(e);
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (error == null) {
				Log.debug("[loginBySaml] checkpoint #13 - no error!!!");
			} else {
				throw new Exception(error);
			}
		}
		return error == null;
	}
}
