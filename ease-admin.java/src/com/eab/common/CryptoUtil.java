package com.eab.common;

import com.eab.security.AES;
import com.eab.security.DESede;

public class CryptoUtil {

	public String encryptUserData(String userCode, String text) {
		DESede cipher = DESede.getInstance();
		String encryptedText = null;
		String salt = userCode; // Hash original by salt (must be 16 chars = 128bit)

		try {
			if (text != null && text.length() > 0) {
				salt = fixKeyLength(salt);

				encryptedText = AES.encrypt(text, salt); // Phase 1: Encrypt original text by User Code using AES (128-bit)
				encryptedText = cipher.encrypt(encryptedText); // Phase 2: Encrypt AES output by TripleDES
			}
		} catch (Exception e) {
			Log.error(e);
			encryptedText = null;
		}

		return encryptedText;
	}

	public String decryptUserData(String userCode, String encryptedText) {
		DESede cipher = DESede.getInstance();
		String decryptedText = null;
		String salt = userCode;

		try {
			if (encryptedText != null && encryptedText.length() > 0 && userCode != null && userCode.length() > 0) {
				salt = fixKeyLength(salt);
				if (salt != null && salt.length() > 0) {
					decryptedText = cipher.decrypt(encryptedText); // Phase 1: Decrypt encrypted text by TripleDES
					decryptedText = AES.decrypt(decryptedText, salt); // Phase 2: Decrypt TripleDES output by AES (same Salt is required)
				}
			}
		} catch (Exception e) {
			Log.error(e);
			encryptedText = null;
		}

		return decryptedText;
	}

	public String fixKeyLength(String keyString) {
		String result = keyString;

		if (result != null && result.length() != Constants.CRYPTO_KEY_LENGTH) {
			while (result.length() != Constants.CRYPTO_KEY_LENGTH) {
				if (result.length() > Constants.CRYPTO_KEY_LENGTH) {
					result = result.substring(0, result.length() - 2);
				} else {
					result = result + "*";
				}
			}
		} else {
			result = Function.GenerateRandomASCII(Constants.CRYPTO_KEY_LENGTH);
		}

		return result;
	}

}
