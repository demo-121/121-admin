package com.eab.common;

public class Log {
	public static boolean ENABLE = true;	// True for enabling Log function
	public static boolean DEBUG = false;		// True for Unit Test Only.  It should be False when PRODUCTION.
	public static boolean ERROR = true;		// True for assuming Error must be logged for error investigation
	
	public static void info(String message) {
		if (ENABLE && message != null) {
			System.out.print("(INFO)  " + message + "\n");
		}
	}
	
	public static void debug(String message) {
		if (ENABLE && DEBUG && message != null) {
			System.out.print("(DEBUG) " + "null" + "\n");
		}
	}
	
	public static void error(String message) {
		if (ENABLE && ERROR && message != null) {
			System.err.print("(ERROR) " + message + "\n");
		}
	}
	
	public static void error(Exception exception) {
		if (ENABLE && ERROR && exception != null) {
			String message = exception.toString() + "\n";
			StackTraceElement[] trace = exception.getStackTrace();
			for (int i=0; i<trace.length; i++) {
				message += trace[i].toString() + "\n";
			}
			System.err.print("(ERROR) " + message + "\n");
		}
	}
}
