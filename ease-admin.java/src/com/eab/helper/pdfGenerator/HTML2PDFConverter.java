package com.eab.helper.pdfGenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class HTML2PDFConverter {

    public static FileInputStream convertHTML2PDF(String wkhtmltopdfPath, String inputHTMLPath, String outputPDFPath) throws IOException, InterruptedException {
        // This magic config is used in Windows/Linux only, Mac version will generate wrong result        
        ProcessBuilder pb = new ProcessBuilder(wkhtmltopdfPath, "--page-size", "A4", "--dpi", "300", "--zoom", "0.9999", "--disable-smart-shrinking", "-T", "0", "-B", "0", "-L", "0", "-R", "0", "--encoding", "UTF-8", "--quiet", inputHTMLPath, outputPDFPath);
        Process proc = pb.start();
        InputStream stderr = proc.getErrorStream();
        InputStreamReader reader = new InputStreamReader(stderr);
        BufferedReader br = new BufferedReader(reader);
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = br.readLine()) != null) {
            sb.append(line).append("\n");
        }
        if (sb.length() > 0) {
            System.out.println(sb.toString());
        }
        int exitVal = proc.waitFor();
        if (exitVal == 0) {
            File pdf = new File(outputPDFPath);
            if (pdf.exists()) {
                return new FileInputStream(pdf);
            }
        }
        return null;
    }

}
