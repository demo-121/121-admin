package com.eab.helper.pdfGenerator;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class JsonUtil {

    private static Gson gson = null;
    private static Gson prettyGson = null;

    public static Gson sharedInstance() {
        if (JsonUtil.gson == null) {
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Double.class, new JsonNumericSerializer());
            JsonUtil.gson = builder.create();
        }
        return JsonUtil.gson;
    }

    public static Gson sharedPrettyInstance() {
        if (JsonUtil.prettyGson == null) {
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Double.class, new JsonNumericSerializer());
            builder.setPrettyPrinting();
            JsonUtil.prettyGson = builder.create();
        }
        return JsonUtil.prettyGson;
    }

    @SuppressWarnings("unchecked")
    public static <T> T getData(JsonElement object, String key, Class<T> type) {
        if (object == null || object.isJsonNull() || !(object instanceof JsonObject)) {
            return null;
        }
        JsonElement ele = object.getAsJsonObject().get(key);
        try {
            if (null != ele) {
                if (type == JsonObject.class) {
                    return (T) ele.getAsJsonObject();
                } else if (type == JsonArray.class) {
                    return (T) ele.getAsJsonArray();
                } else if (type == String.class) {
                    return (T) ele.getAsString();
                } else if (type == Double.class) {
                    return (T) new Double(ele.getAsDouble());
                } else if (type == Integer.class) {
                    return (T) new Integer(ele.getAsInt());
                } else if (type == Long.class) {
                    return (T) new Long(ele.getAsLong());
                } else if (type == Boolean.class) {
                    return (T) new Boolean(ele.getAsBoolean());
                } else if (type == Date.class) {
                    Object date = null;
                    try {
                        date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(object.getAsString());
                    } catch (Exception e) {
                        date = new SimpleDateFormat("yyyy-MM-dd").parse(object.getAsString());
                    }
                    return (T) date;
                }
            }
        } catch (Exception ex) {
            // Do nothing
        }
        return null;
    }

    public static JsonObject getAsJsonObject(JsonElement object) {
        return object != null ? (JsonObject) object : null;
    }

    public static JsonObject getAsJsonObject(JsonElement object, String key) {
        return JsonUtil.getData(object, key, JsonObject.class);
    }

    public static JsonObject getAsJsonObject(JsonElement object, int i) {
        if (object != null && object instanceof JsonArray) {
            JsonArray arr = (JsonArray) object;
            return (i >= 0 && i < arr.size()) ? JsonUtil.getAsJsonObject(arr.get(i)) : null;
        }
        return null;
    }

    public static JsonArray getAsJsonArray(JsonElement object) {
        return object != null ? (JsonArray) object : null;
    }

    public static JsonArray getAsJsonArray(JsonElement object, String key) {
        return JsonUtil.getData(object, key, JsonArray.class);
    }

    public static String getAsString(JsonElement object, String key) {
        String ret = JsonUtil.getData(object, key, String.class);
        return ret == null ? "" : ret;
    }

    public static double getAsDouble(JsonElement object, String key) {
        Double obj = JsonUtil.getData(object, key, Double.class);
        return obj == null ? 0 : obj.doubleValue();
    }

    public static int getAsInt(JsonElement object, String key) {
        Integer obj = JsonUtil.getData(object, key, Integer.class);
        return obj == null ? 0 : obj.intValue();
    }

    public static long getAsLong(JsonElement object, String key) {
        Long obj = JsonUtil.getData(object, key, Long.class);
        return obj == null ? 0 : obj.longValue();
    }

    public static boolean getAsBoolean(JsonElement object, String key) {
        Boolean obj = JsonUtil.getData(object, key, Boolean.class);
        return obj == null ? false : obj.booleanValue();
    }

    public static Date getAsDate(JsonElement object, String key) {
        return JsonUtil.getData(object, key, Date.class);
    }
}
