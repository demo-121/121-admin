package com.eab.helper.pdfGenerator;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.eab.helper.pdfGenerator.JsonUtil;

public class XSLTTransformer {
    
    private Gson gson = JsonUtil.sharedInstance();
    
    public String prepareReportHtmlByTemplateJson(String commonTemplateJson, String reportTemplateJson, String data) {
        String retVal = "";
        JsonObject commonTemplate = JsonUtil.getAsJsonObject(gson.fromJson(commonTemplateJson, JsonElement.class));
        JsonObject reportTemplate = JsonUtil.getAsJsonObject(gson.fromJson(reportTemplateJson, JsonElement.class));
        JsonObject commonHeaderFooter = commonTemplate.getAsJsonObject("headerFooter");
        JsonObject reportHeaderFooter = reportTemplate.getAsJsonObject("headerFooter");
        if (commonHeaderFooter == null) {
            System.out.println("Invalid template: Missing header/footer section in common template");
        }
        if (reportHeaderFooter == null) {
            System.out.println("Invalid template: Missing header/footer section in report template");
        }
        StringBuilder sb = new StringBuilder();
        sb.append(JsonUtil.getAsString(commonTemplate, "style"));
        sb.append(JsonUtil.getAsString(reportTemplate, "style"));
        String styles = sb.toString();
        String documentHeader = JsonUtil.getAsString(commonHeaderFooter, "documentHeader");
        documentHeader = String.format(documentHeader, styles);
        String documentFooter = JsonUtil.getAsString(commonHeaderFooter, "documentFooter");
        String pageHeader = JsonUtil.getAsString(commonHeaderFooter, "pageHeader");
        String pageFooter = JsonUtil.getAsString(commonHeaderFooter, "pageFooter");
        String contentHeader = JsonUtil.getAsString(commonHeaderFooter, "contentHeader");
        String contentFooter = JsonUtil.getAsString(commonHeaderFooter, "contentFooter");
        String xsltHeader = JsonUtil.getAsString(commonHeaderFooter, "xsltHeader");
        String xsltFooter = JsonUtil.getAsString(commonHeaderFooter, "xsltFooter");
        String htmlHeader = JsonUtil.getAsString(reportHeaderFooter, "header");
        String htmlFooter = JsonUtil.getAsString(reportHeaderFooter, "footer");
        sb = new StringBuilder();
        sb.append(documentHeader);
        JsonArray templates = reportTemplate.getAsJsonArray("templates");
        StringBuilder pagesSb = new StringBuilder();
        Boolean isFirstPage = true;
        for (int i = 0; i < templates.size(); i++) {
            String template = templates.get(i).getAsString();
            pagesSb.append(preparePage(template, !isFirstPage));
            isFirstPage = false;
        }
        String transformedPages = transformXml(pagesSb.toString(), data, xsltHeader, xsltFooter);
        htmlHeader = transformXml(htmlHeader, data, xsltHeader, xsltFooter);
        htmlFooter = transformXml(htmlFooter, data, xsltHeader, xsltFooter);
        pageHeader = String.format(pageHeader, htmlHeader);
        pageFooter = String.format(pageFooter, htmlFooter);
        sb.append(preparePages(transformedPages, pageHeader, pageFooter, contentHeader, contentFooter));
        
        sb.append(documentFooter);
        String report = sb.toString();
        int totalPage = 0;
        int lastIndex = 0;
        String findStr = "[[@TOTAL_PAGE]]";
        while (lastIndex != -1) {
            lastIndex = report.indexOf(findStr, lastIndex);
            if (lastIndex != -1) {
                totalPage++;
                lastIndex += findStr.length();
            }
        }
        retVal = report;
        retVal = report.replaceAll("\\[\\[@TOTAL_PAGE\\]\\]", String.valueOf(totalPage));
        for (int pageNo = 1; pageNo <= totalPage; pageNo++) {
            retVal = retVal.replaceFirst("\\[\\[@PAGE_NO\\]\\]", String.valueOf(pageNo));
        }
        return retVal;
    }

    public String preparePage(String template, Boolean willAppendPageBreak) {
        StringBuilder sb = new StringBuilder();
        if (willAppendPageBreak) {
            sb.append("[[@PAGE_BREAK]]");
        }
        sb.append(template);
        return sb.toString();
    }

    public String preparePages(String allPages, String pageHeader, String pageFooter, String contentHeader, String contentFooter) {
        StringBuilder sb = new StringBuilder();
        String[] pages = allPages.split("\\[\\[@PAGE_BREAK\\]\\]");
        for (String page : pages) {
            sb.append(pageHeader);
            sb.append(contentHeader);
            if (page == null || page.length() == 0) {
                sb.append("&nbsp;");
            } else {
                sb.append(page);
            }
            sb.append(contentFooter);
            sb.append(pageFooter);
        }
        return sb.toString();
    }

    public String transformXml(String template, String xml, String xsltHeader, String xsltFooter) {
        StringBuilder sb = new StringBuilder();
        sb.append(xsltHeader);
        sb.append(template);
        sb.append(xsltFooter);
        return transformXml(xml, sb.toString());
    }

    public String transformXml(String xml, String xslt) {
        String retVal = "";
        try {
            StringReader reader = new StringReader(xml);
            StringWriter writer = new StringWriter();
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(new ByteArrayInputStream(xslt.getBytes("UTF-8"))));
            transformer.transform(new StreamSource(reader), new StreamResult(writer));
            return writer.toString();
        } catch (Exception ex) {
            System.out.println("Exception in transformXml:" + ex.getMessage());
        }
        return retVal;
    }

}
