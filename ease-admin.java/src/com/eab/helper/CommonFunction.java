package com.eab.helper;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;


public class CommonFunction {
	
	private boolean DEBUG = false;
		
	/**
	 *  example:
	 *  map = {"firstLayer":{"secordLayer":{"thirdLayer":"here"}}}
	 *  key = "firstLayer.secordLayer.thirdLayer
	 *  returnValue = "here"
	 * @param map
	 * @param key
	 * @return
	 */
	public static Object getKey(Map<String, Object> map, String key){
		String[] keys;
		// use "\\." to split "." 
		keys = key.split("\\.");
		if(keys.length > 1){
			try{
				return getKey((HashMap<String, Object>)map.get(keys[0]), key.substring(keys[0].length() + 1));
			}catch(Exception e){
				System.out.println("Error in getting object(" + keys[0] + ") in HashMap");
			}
		}else{
			try{
				return map.get(keys[0]);
			}catch(Exception e){
				System.out.println("Error in getting object(" + keys[0] + ") in HashMap");
			}
		}
		return null;
	}

	
	public static String readFile(String path){

			/*String result = "";
		 
			ClassLoader classLoader = getClass().getClassLoader();
			try {
				
			    result = IOUtils.toString(classLoader.getResourceAsStream(path));
			} catch (Exception e) {
				e.printStackTrace();
			}
		 
			return result;*/
			String result = null;
			try{
				result = FileUtils.readFileToString(new File(path), "UTF-8");
			}catch(Exception e){
				e.printStackTrace();
			}
	        return result; 
    }
	
	public static ArrayList<String> splitByString(String source, String pattern){
		boolean pageEnd = false;
		String tempString = source;
		ArrayList<String> result = new ArrayList<String>();
		do{
			
			
			int pageIndex = tempString.indexOf(pattern);
			if(pageIndex == -1){
				pageEnd = true;
				result.add(tempString);
			}else{
				result.add(tempString.substring(0, pageIndex));
				tempString = tempString.substring(pageIndex + pattern.length());
			}
		}while(!pageEnd);
		return result;
	}
	
	public static String doubleToPriceString(double price){
		DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
		return decimalFormat.format(price);
	}
	
	public static HashMap<String, Object> toDictionary(String json) {
        HashMap<String,Object> map = new HashMap<String,Object>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            map = mapper.readValue(json, new TypeReference<HashMap<String, Object>>() {
            });
            return map;
        } catch (Exception e){
        	System.out.println("Error in parsing json to HashMap");
        }

        return null;
    }

    public static String toJsonString(HashMap<String, Object> dic) {
        JSONObject jsonObj = new JSONObject(dic);

        return jsonObj.toString();
    }
    
    public static String unescapeJavaString(String st) {
        StringBuilder sb = new StringBuilder(st.length());

        for (int i = 0; i < st.length(); i++) {
            char ch = st.charAt(i);
            if (ch == '\\') {
                char nextChar = (i == st.length() - 1) ? '\\' : st
                        .charAt(i + 1);
                // Octal escape?
                if (nextChar >= '0' && nextChar <= '7') {
                    String code = "" + nextChar;
                    i++;
                    if ((i < st.length() - 1) && st.charAt(i + 1) >= '0'
                            && st.charAt(i + 1) <= '7') {
                        code += st.charAt(i + 1);
                        i++;
                        if ((i < st.length() - 1) && st.charAt(i + 1) >= '0'
                                && st.charAt(i + 1) <= '7') {
                            code += st.charAt(i + 1);
                            i++;
                        }
                    }
                    sb.append((char) Integer.parseInt(code, 8));
                    continue;
                }
                switch (nextChar) {
                    case '\\':
                        ch = '\\';
                        break;
                    case 'b':
                        ch = '\b';
                        break;
                    case 'f':
                        ch = '\f';
                        break;
                    case 'n':
                        ch = '\n';
                        break;
                    case 'r':
                        ch = '\r';
                        break;
                    case 't':
                        ch = '\t';
                        break;
                    case '\"':
                        ch = '\"';
                        break;
                    case '\'':
                        ch = '\'';
                        break;
                    // Hex Unicode: u????
                    case 'u':
                        if (i >= st.length() - 5) {
                            ch = 'u';
                            break;
                        }
                        int code = Integer.parseInt(
                                "" + st.charAt(i + 2) + st.charAt(i + 3)
                                        + st.charAt(i + 4) + st.charAt(i + 5), 16);
                        sb.append(Character.toChars(code));
                        i += 5;
                        continue;
                }
                i++;
            }
            sb.append(ch);
        }
        return sb.toString();

    }

    
}
