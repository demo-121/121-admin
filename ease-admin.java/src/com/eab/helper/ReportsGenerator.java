package com.eab.helper;

import java.io.File;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.eab.helper.pdfGenerator.HTML2PDFConverter;
import com.eab.helper.CommonFunction;

public class ReportsGenerator {

	private String wkhtmltopdfPath;;
	private String resourceFolder;
	public String errorMsg;
	public boolean error;
	
	public ReportsGenerator(HttpServletRequest request){
		String pathToGraphsDir = request.getRealPath("/");
		wkhtmltopdfPath = pathToGraphsDir + "WEB-INF/classes/wkhtmltopdf/bin/wkhtmltopdf/";
        resourceFolder = pathToGraphsDir + "resources/";
        error = false;
		errorMsg = "";
	}
	
	
	

    public String proposalHtmlWithQuotation(String planDetail, String xmlData)
    {	
    	error = false;
        String reportTemplate = "";
        try {
        	HashMap<String, Object> basePlanDetail = (HashMap<String, Object>) CommonFunction.toDictionary(planDetail);
        	HashMap<String, Object> reports = (HashMap<String, Object>)CommonFunction.getKey(basePlanDetail, "reportTemplate");
        	if(reports == null){
        		errorMsg = "Failed to generate";
        		error = true;
                return "#";
        	}
            reportTemplate = CommonFunction.toJsonString(reports);
            //System.out.println("reportTemplate : " + reportTemplate);
            
        }catch(Exception e)
        {
        	errorMsg = "Failed to generate";
        	error = true;
            return "#";
        }
        
        String commonTemplate = CommonFunction.readFile("json/commonTemplate.json");
        if(commonTemplate == null || commonTemplate.trim().equals("")){
        	errorMsg = "Failed to generate";
        	error = true;
        	return "#";
        }
        if(xmlData == null || xmlData.trim().equals("")){
        	errorMsg = "Failed to generate";
        	error = true;
        	return "#";
        }
        
    	
        return generateHtml(commonTemplate, reportTemplate, xmlData);
        

    }
    
    public String generateHtml(String commonTemplate, String reportTemplate, String xmlData){
    	error = false;
    	File htmlFile = null;
    	try{
	        PDFHelper pdfHelper = new PDFHelper();
	        File tempFile = new File(resourceFolder, "temp");
	        if(!tempFile.exists())
	        	tempFile.mkdir();
	        htmlFile = new File(tempFile, "sampleHtml.html");
	        if (htmlFile.exists()) {
	    		htmlFile.delete();
	        }
	        FileUtils.writeStringToFile(htmlFile, pdfHelper.prepareReportHtml(commonTemplate, reportTemplate, xmlData), "UTF-8");
	        return pdfHelper.prepareReportHtml(commonTemplate, reportTemplate, xmlData);
        }catch(Exception e){
        	errorMsg = "Failed to generate";
        	error = true;
        	return null;
        }
    	
    	
        //return htmlFile.getName();
    }
    
    public String pdfWithHtml(String reportHtml){
    	if(reportHtml == null || reportHtml.trim().equals("")){
    		errorMsg = "Failed to generate";
    		error = true;
        	return "#";
        }
    	File htmlFile = new File(resourceFolder, "temp/" + reportHtml);
        System.out.println("---- Test for generating PDF by HTML [START] ----");
        File pdfFile = null;
        try {
        	File outputFile = new File(resourceFolder, "output");
        	if(!outputFile.exists())
        		outputFile.mkdir();
        	pdfFile = new File(outputFile, "samplePdf.pdf"); 
        	if (pdfFile.exists()) {
        		pdfFile.delete();
            }
            
			HTML2PDFConverter
			        .convertHTML2PDF(wkhtmltopdfPath, htmlFile.getAbsolutePath(), pdfFile.getAbsolutePath());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			errorMsg = "Failed to generate";
			error = true;
			return "#";
		}
        System.out.println("---- Test for generating PDF by HTML [ END ] ----");
        return pdfFile.getName();
    }
    



    
}
