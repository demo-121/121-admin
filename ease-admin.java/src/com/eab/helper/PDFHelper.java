package com.eab.helper;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONObject;

import com.eab.helper.pdfGenerator.XSLTTransformer;
import com.eab.helper.CommonFunction;


public class PDFHelper {
	
	private boolean DEBUG = false;
	
	public String prepareReportXLT(String _commonTemplateJson, String _reportTemplateJson, String xmlData)
    {
        HashMap<String, Object> commonTemplateJson = CommonFunction.toDictionary(_commonTemplateJson);
        HashMap<String, Object> reportTemplateJson = CommonFunction.toDictionary(_reportTemplateJson);
        if(DEBUG){
        	System.out.println("commonTemplateJson : " + new JSONObject(commonTemplateJson).toString());
        	System.out.println("reportTemplateJson : " + new JSONObject(reportTemplateJson).toString());
        }
        
        if (CommonFunction.getKey(commonTemplateJson, "headerFooter") == null || CommonFunction.getKey(commonTemplateJson, "headerFooter").toString().trim().equals(""))
            System.out.println("Error : the headerFooter of commonTemplate is missing");
        if (CommonFunction.getKey(reportTemplateJson, "headerFooter") == null || CommonFunction.getKey(reportTemplateJson, "headerFooter").toString().trim().equals(""))
        	System.out.println("Error : the headerFooter of reportTemplateJson is missing");


        String styles = "";
        styles = styles + CommonFunction.getKey(commonTemplateJson, "style");
        styles = styles + CommonFunction.getKey(reportTemplateJson, "style");
        String documentHeader = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.documentHeader");
        documentHeader = documentHeader.replace("%s", styles);
        String documentFooter = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.documentFooter");
        String pageHeader = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.pageHeader");
        String pageFooter = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.pageFooter");
        String contentHeader = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.contentHeader");
        String contentFooter = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.contentFooter");
        String xsltHeader = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.xsltHeader");
        String xsltFooter = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.xsltFooter");
        String htmlHeader = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.header");
        String htmlFooter = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.footer");
        
        if(DEBUG){
        	System.out.println("documentHeader : " + documentHeader);
        	System.out.println("documentFooter : " + documentFooter);
        	System.out.println("pageHeader : " + pageHeader);
        	System.out.println("pageFooter : " + pageFooter);
        	System.out.println("contentHeader : " + contentHeader);
        	System.out.println("contentFooter : " + contentFooter);
        	System.out.println("xsltHeader : " + xsltHeader);
        	System.out.println("xsltFooter : " + xsltFooter);
        	System.out.println("htmlHeader : " + htmlHeader);
        	System.out.println("htmlFooter : " + htmlFooter);
        }
        
        String report = "";
        report = documentHeader;
        ArrayList<String> templatesMap = (ArrayList<String>) CommonFunction.getKey(reportTemplateJson, "templates");
        int count = templatesMap.size();
        String[] templates = new String[count];
        for(int i = 0 ; i < count ; i++){
        	templates[i] = templatesMap.get(i);
        	if(DEBUG)
        		System.out.println("template[" + i + "] : " + templates[i]);
        }
        
        String pages = "";
        boolean isFirstPage = true;
        int index = 0;//for debug
        for(String template : templates)
        {
            pages = pages + preparePage(template, !isFirstPage);
            if(DEBUG)
            	System.out.println("index : " + index++ + " , isFirstPage " + isFirstPage + " , " + pages);
            isFirstPage = false;
            
            
        }

        pageHeader = pageHeader.replace("%s", htmlHeader);
        if(DEBUG)
        	System.out.println("pageHeader : " + pageHeader);
        pageFooter = pageFooter.replace("%s", htmlFooter);
        if(DEBUG)
        	System.out.println("htmlFooter : " + htmlFooter);
        report = report + preparePage(pages, pageHeader, pageFooter, contentHeader, contentFooter);
        report = report + documentFooter;
        String xslt = xsltHeader + report + xsltFooter;
        if(DEBUG)
        	System.out.println("xslt : " + xslt);

        return xslt;

    }
	

    public String prepareReportHtml(String _commonTemplateJson, String _reportTemplateJson, String xmlData)
    {
    	HashMap<String, Object> commonTemplateJson = CommonFunction.toDictionary(_commonTemplateJson);
    	HashMap<String, Object> reportTemplateJson = CommonFunction.toDictionary(_reportTemplateJson);
        if(DEBUG){
        	System.out.println("commonTemplateJson : " + new JSONObject(commonTemplateJson).toString());
        	System.out.println("reportTemplateJson : " + new JSONObject(reportTemplateJson).toString());
        }

        if (CommonFunction.getKey(commonTemplateJson, "headerFooter") == null || CommonFunction.getKey(commonTemplateJson, "headerFooter").toString().trim().equals(""))
            System.out.println("Error : the headerFooter of commonTemplate is missing");
        if (CommonFunction.getKey(reportTemplateJson, "headerFooter") == null || CommonFunction.getKey(reportTemplateJson, "headerFooter").toString().trim().equals(""))
        	System.out.println("Error : the headerFooter of reportTemplateJson is missing");

        String styles = "";
        styles = styles + CommonFunction.getKey(commonTemplateJson, "style");
        styles = styles + CommonFunction.getKey(reportTemplateJson, "style");
        String documentHeader = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.documentHeader");
        documentHeader = documentHeader.replace("%s", styles);
        String documentFooter = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.documentFooter");
        String pageHeader = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.pageHeader"); 
        String pageFooter = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.pageFooter"); 
        String contentHeader = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.contentHeader"); 
        String contentFooter = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.contentFooter"); 
        String xsltHeader = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.xsltHeader"); 
        String xsltFooter = (String) CommonFunction.getKey(commonTemplateJson, "headerFooter.xsltFooter");
        String htmlHeader = (String) CommonFunction.getKey(reportTemplateJson, "headerFooter.header"); 
        String htmlFooter = (String) CommonFunction.getKey(reportTemplateJson, "headerFooter.footer"); 
        String report = "";
        
        if(DEBUG){
        	System.out.println("documentHeader : " + documentHeader);
        	System.out.println("documentFooter : " + documentFooter);
        	System.out.println("pageHeader : " + pageHeader);
        	System.out.println("pageFooter : " + pageFooter);
        	System.out.println("contentHeader : " + contentHeader);
        	System.out.println("contentFooter : " + contentFooter);
        	System.out.println("xsltHeader : " + xsltHeader);
        	System.out.println("xsltFooter : " + xsltFooter);
        	System.out.println("htmlHeader : " + htmlHeader);
        	System.out.println("htmlFooter : " + htmlFooter);
        }
        
        report = documentHeader;
        ArrayList <String>templatesMap = (ArrayList<String>) CommonFunction.getKey(reportTemplateJson, "templates");
        int count = templatesMap.size();
        String[] templates = new String[count];
        for(int i = 0 ; i < count ; i++){
        	templates[i] = templatesMap.get(i);
        	System.out.println("template[" + i + "] : " + templates[i]);
        }

        String pages = "";
        boolean isFirstPage = true;
        int index = 0 ;//for debug
        for(String template : templates)
        {
            pages = pages + preparePage(template, !isFirstPage);
            if(DEBUG)
            	System.out.println("index : " + index++ + " , isFirstPage : " + isFirstPage + " , template : " + pages);
            isFirstPage = false;
        }
        String transformedPages = transformXml(pages, xmlData, xsltHeader, xsltFooter);
        htmlHeader = transformXml(htmlHeader, xmlData, xsltHeader, xsltFooter);
        htmlFooter = transformXml(htmlFooter, xmlData, xsltHeader, xsltFooter);
        pageHeader = pageHeader.replace("%s", htmlHeader);
        pageFooter = pageFooter.replace("%s", htmlFooter);
        if(DEBUG){
        	System.out.println("transformedPages : " + transformedPages);
        	System.out.println("htmlHeader : " + htmlHeader);
        	System.out.println("htmlFooter : " + htmlFooter);
        	System.out.println("pageHeader : " + pageHeader);
        	System.out.println("pageFooter : " + pageFooter);
        }

        report = report + preparePage(transformedPages, pageHeader, pageFooter, contentHeader, contentFooter);
        report = report + documentFooter;

        int totalPage = CommonFunction.splitByString(report, "<div class=\"pageContainer\">").size() - 1;
        report = report.replace("[[@TOTAL_PAGE]]", Integer.toString(totalPage));
        ArrayList<String> splitReports = CommonFunction.splitByString(report, "[[@PAGE_NO]]");
        if(splitReports.size() > 1){
        	report = splitReports.get(0);
        	for(int i = 1 ; i < splitReports.size() ; i++){
            	report = report + i + splitReports.get(i);
            }
        }else
        	System.out.println("Error in spliting report");
        
		if(DEBUG)
			System.out.println(report);

        return report;

    }



    private String preparePage(String template, boolean willAppendPageBreak)
    {
        String result = "";
        if (willAppendPageBreak)
            result = "[[@PAGE_BREAK]]";
        result = result + template;
        return result;
    }

    private String transformXml(String template, String xmlData, String xsltHeader, String xsltFooter)
    {
        String xslt = xsltHeader + template + xsltFooter;
        System.out.println("xslt : " + xslt);
        xslt = CommonFunction.unescapeJavaString(xslt);
        return transformXml(xmlData, xslt);
    }
    
    
    public String transformXml(String xml, String xslt)
    {
    	String output = "";
        XSLTTransformer transformer = new XSLTTransformer();
        output = transformer.transformXml(xml, xslt);
        if(DEBUG)
        	System.out.println("output : " + output);

        return output;
    }

    private String preparePage(String transformedPages, String pageHeader, String pageFooter, String contentHeader, String contentFooter)
    {
        String retVal = "";
        ArrayList<String> pages = CommonFunction.splitByString(transformedPages, "[[@PAGE_BREAK]]");
        for(String page : pages)
        {
        	
            if(page != null || page.trim().length() > 0)
            {
                retVal = retVal + pageHeader;
                retVal = retVal + contentHeader;
                retVal = retVal + page.trim();
                retVal = retVal + contentFooter;
                retVal = retVal + pageFooter;
            }


        }
        return retVal;            
    }


    public String generatePdfInVC(String commonTemplate, String reportTemplate, String xml)
    {
        String reportHtml = prepareReportHtml(commonTemplate, reportTemplate, xml);
        return reportHtml;
    }

    public void generatePdfInVC(String reportHtml)
    {
        try
        {
            //QuotationModel quotation = new QuotationModel();
            //success(quotation);
        }
        catch (Exception e)
        {
            //failure();
        }
    }
}
